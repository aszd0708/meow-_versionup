# 집사 해줄 고양!?(가제)

## 소개
평화로운 마을에 귀여운 동물들이 있어요

구석 구석 숨겨져있는 동물들을 찾아서 편안한 집에 데려다 줄까요?

## 플레이 영상
[![YoutubeGamePlayLink](https://img.youtube.com/vi/vdve4aJZDgg/sddefault.jpg)](https://www.youtube.com/watch?v=vdve4aJZDgg)

## 게임 방법

[![YoutubeTutorialLink](https://img.youtube.com/vi/xKOjjsiz298/sddefault.jpg)](https://www.youtube.com/watch?v=xKOjjsiz298)

## 사용한 라이브러리
[DoTween](http://dotween.demigiant.com/) : 움직임을 간단하게 구현 할 때 사용

[GoogleGamePlayService](https://developers.google.com/games/services/integration) : 구글플레이와 연동

[Newtonsoft.Json](https://github.com/JamesNK/Newtonsoft.Json/releases) : Json 연동

[MobileTouchCamera](https://assetstore.unity.com/packages/tools/camera/mobile-touch-camera-43960) : 카메라 이동

## 직접 제작한 유틸리티
[UnityManagers](https://github.com/aszd0708/UnityGameManagers) : 저장, 풀링, 팝업등 사용

## 제작
### 기간
2018년 ~ 미정

### 인원
기획 1명

디자인 1명

프로그래머 1명
