﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateSakura : MonoBehaviour
{
    public Transform[] createPos = new Transform[2];
    public Transform[] endPos = new Transform[2];

    public GameObject sakuraPrefab;

    public Sprite[] sprite;

    private void Start()
    {
        SakuraCreater();
    }

    private void SakuraCreater()
    {
        StartCoroutine(_SakuraCreater());
    }

    private IEnumerator _SakuraCreater()
    {
        WaitForSeconds createTime = new WaitForSeconds(00.5f);
        while (true)
        {
            for(int i = 0; i < Random.Range(0,5); i++)
            {
                CreateLeaf();
            }
            yield return createTime;
        }
    }

    private void CreateLeaf()
    {
        int random = Random.Range(0, createPos.Length);
        GameObject sakura = PoolingManager.Instance.GetPool("Sakura");
        if (sakura == null)
            sakura = Instantiate(sakuraPrefab);
        sakura.GetComponent<SpriteRenderer>().sprite = sprite[Random.Range(0, sprite.Length)];
        Vector3 pos = new Vector3(Random.Range(createPos[0].position.x, createPos[1].position.x), createPos[0].position.y, 0);
        sakura.transform.position = pos;
        float time = Random.Range(10f, 20f);
        Vector3 endPos = new Vector3(pos.x - 5, -7.5f, 0);
        sakura.transform.DOMove(endPos, time).SetEase(Ease.Linear);
        PoolingManager.Instance.SetPool(sakura, "Sakura", time);
    }
}
