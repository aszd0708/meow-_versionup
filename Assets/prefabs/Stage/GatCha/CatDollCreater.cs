﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatDollCreater : MonoBehaviour
{
    public int count;
    public List<CatSaveLoad.JsonCat> catJson;
    public GameObject catObj;
    public Transform objP;
    public float minX, maxX;
    public float minY, maxY;
    public List<int> colNum = new List<int>();
    public Sprite[] catSprite;
    [HideInInspector]
    public int randomSprite;

    // 스테이지 변수
    private int stageNumber;
    // 값을 지정??
    private List<int> collectNum = new List<int>();

    // 값을 지정하는 걸로
    public int[] stageCats = new int[5];
    // 대신 튜토리얼 고양이 때문에 0부터 시작이 아니고 1부터 시작
    void Awake()
    {
        // 대충 불러온다는 명령어
        // playerpref 로 불러오기

        stageCats[0] = 0;
    }
    // Start is called before the first frame update
    void Start()
    {
        // 여기 부분을 바꾸기, 크레인 모션 부분 변경
        catJson = GetComponent<CatSaveLoad>().LoadCats();
        catSprite = Resources.LoadAll<Sprite>("Cats'House/Animals/Cats");

        // 위에 있는 이걸 밑에 있는걸 지우고 선언 하면 스테이지 별로 불러올 수 있음.
        SetStageCats();

        //for (int i = 0; i < catJson.Count; i++)
        //    if (catJson[i].Collect)
        //        colNum.Add(catJson[i].No);

        CreateDoll();
    }

    // 스테이지 별로 인형 갖고오는 함수
    private void SetStageCats()
    {
        stageNumber = PlayerPrefs.GetInt("GatchaRoomNumber");

        for (int i = stageCats[stageNumber - 1]; i < stageCats[stageNumber]; i++)
            if (catJson[i].Collect)
            {
                Debug.Log(catJson[i].Name);
                colNum.Add(catJson[i].No);
            }
    }

    // 여기 스테이지별 인덱스값 리턴
    private int SendStageDoll(int index)
    {
        int stageStartIndex = 0;

        switch(index)
        {
            case 1:
                stageStartIndex = 2;
                break;
            case 2:
                stageStartIndex = 7;
                break;
        }

        return stageStartIndex;
    }

    public Sprite SendSprite()
    {
        int index = Random.Range(0, colNum.Count);
        return catSprite[colNum[index]-1];
    }

    private void CreateDoll()
    {
        for(int a = 0; a < count; a++)
        {
            int random = Random.Range(0, colNum.Count);
            GameObject catDoll = Instantiate(catObj);
            catDoll.transform.SetParent(objP);
            catDoll.transform.position = new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY), 0);
            catDoll.GetComponent<SpriteRenderer>().sprite = SendSprite();
            catDoll.transform.rotation = Quaternion.Euler(0, 0, Random.Range(-180, 180));
            float rScale = Random.Range(0.8f, 1.0f);
            catDoll.transform.localScale = new Vector3(rScale, rScale, 0);
        }
    }

    public void CreatUnColDoll()
    {

    }
}
