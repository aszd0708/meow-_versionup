﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundTest : MonoBehaviour
{
    public AudioClip clips;

    [Range(-1, 5)]
    public float pitch;
    public void PlaySound()
    {
        AudioManager.Instance.PlaySound(clips, transform.position, null, pitch);
    }
}
