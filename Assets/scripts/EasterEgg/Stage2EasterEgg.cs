﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2EasterEgg : MonoBehaviour
{
    [SerializeField]
    private int[] easterEggChecker = new int[5];
    private bool fail = false;
    private int checkerPointer = 0;
    private float easterEggOriginPos;

    public RectTransform easterEgg;

    public void EasterEggCheck(int value)
    {
        if (fail) return;

        if (easterEggChecker[checkerPointer] == value) checkerPointer++;
        else
        {
            fail = true;
            return;
        }

        if (easterEggChecker.Length - 1 == checkerPointer)
        {
            EasterEggStart();
            return;
        }
    }

    private void EasterEggStart()
    {
        easterEggOriginPos = easterEgg.localPosition.y;

        easterEgg.DOLocalMoveY(0, 1.0f);
    }

    public void EasterEggGoHome()
    {
        easterEgg.DOLocalMoveY(easterEggOriginPos, 1.0f);
    }
}
