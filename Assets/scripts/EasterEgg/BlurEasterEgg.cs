﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlurEasterEgg : MonoBehaviour
{
    public Material blur;
    public Image photo;
    // Start is called before the first frame update\

    // Update is called once per frame\
    public void OnClick()
    {
        photo.material = blur;
    }
}
