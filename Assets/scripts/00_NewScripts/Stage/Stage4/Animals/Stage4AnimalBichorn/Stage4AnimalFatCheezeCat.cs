﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage4AnimalFatCheezeCat : StageAnimalObjectBase
{
    #region Body
    [SerializeField]
    private SpriteRenderer bodyRenderer;
    [SerializeField]
    private Sprite idleBodySprite;
    [SerializeField]
    private Sprite[] moveBodySprites;
    [SerializeField]
    private float movementMotionTime;
    #endregion

    #region Tail
    [SerializeField]
    private Transform tailTransform;

    [SerializeField]
    private Transform sitTailTransform;

    [SerializeField]
    private float[] tailAngelValue = new float[2];
    [SerializeField]
    private float tailMotionTime;
    
    private Coroutine tailCor;
    #endregion

    #region Movement
    [SerializeField]
    private Transform movementPose;
    [SerializeField]
    private float movementTime;
    #endregion

    [SerializeField]
    private GameObject sitObject;
    [SerializeField]
    private GameObject movementObject;

    [SerializeField]
    private SpriteRenderer itemSprite;

    public override void StartItemEvent(GameObject itemObj)
    {
        if (endItemMotion)
        {
            PoolingManager.Instance.SetPool(itemObj, "ItemDummy");
            return;
        }
        base.StartItemEvent(itemObj);
        itemObj.SetActive(false);
        MovementMotion();
    }

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if(endItemMotion)
        {
            SetCollect();
            AudioManager.Instance.PlaySound("fat_cat", transform.position);
        }
        return false;
    }

    private void Start()
    {
        sitObject.SetActive(true);
        movementObject.SetActive(false);
        StartCoroutine(_TailMotion(sitTailTransform));
    }

    private void MovementMotion()
    {
        StartCoroutine(_MovementMotion());
    }

    private IEnumerator _MovementMotion()
    {
        sitObject.SetActive(false);
        movementObject.SetActive(true);
        transform.DOMove(movementPose.position, movementTime);
        itemSprite.enabled = true;
        WaitForSeconds wait = new WaitForSeconds(movementMotionTime);
        int count = (int)(movementTime / movementMotionTime);
        int index = 0;
        for (int i = 0; i < count; i++)
        {
            bodyRenderer.sprite = moveBodySprites[index++];
            index %= moveBodySprites.Length;
            yield return wait;
        }
        bodyRenderer.sprite = idleBodySprite;
        sitObject.SetActive(true);
        movementObject.SetActive(false);
        itemSprite.enabled = false;
         yield break;
    }

    private IEnumerator _TailMotion(Transform tail)
    {
        int index = 0;
        WaitForSeconds wait = new WaitForSeconds(tailMotionTime);
        while(true)
        {
            tail.DORotate(Vector3.forward * tailAngelValue[index++], tailMotionTime).SetEase(Ease.OutCirc);
            index %= tailAngelValue.Length;
            yield return wait;
        }
    }
}
