﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage4AnimalOgongCatMovingCloud : MonoBehaviour
{
    [SerializeField]
    private Transform[] movePoses;

    [SerializeField]
    private float moveToCatTime;

    [SerializeField]
    private float moveToOutsideTime;

    [SerializeField]
    private Transform ogongCat;

    private void Start()
    {
        Move();
    }

    private void Move()
    {
        StartCoroutine(_Move());
    }

    private IEnumerator _Move()
    {
        transform.DOMove(movePoses[0].position, moveToCatTime).SetEase(Ease.OutCirc);
        yield return new WaitForSeconds(1.5f);
        ogongCat.SetParent(transform);
        transform.DOMove(movePoses[1].position, moveToOutsideTime).SetEase(Ease.InCirc);

        yield break;
    }
}
