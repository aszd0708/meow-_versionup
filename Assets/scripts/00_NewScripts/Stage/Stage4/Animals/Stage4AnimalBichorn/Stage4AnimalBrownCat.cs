﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage4AnimalBrownCat : StageAnimalObjectBase
{
    [SerializeField]
    private SpriteRenderer bodyRenderer;
    [SerializeField]
    private SpriteRenderer eyeRenderer;

    [SerializeField]
    private Sprite[] eyeBodySprites;
    [SerializeField]
    private Sprite[] nonEyeBodySprites;
    [SerializeField]
    private Sprite standingBodySprite;
    [SerializeField]
    private Sprite[] eyeSprites;

    [SerializeField]
    private Sprite supriseEyeSprite;

    private int nowTouchCount = 0;

    [SerializeField]
    private int eventTouchCount;

    private bool canTouch = true;

    [SerializeField]
    private Stage4AnimalBrownCatUI ui;


    public override void StartItemEvent(GameObject itemObj)
    {
        base.StartItemEvent(itemObj);
        StartCoroutine(_TouchMotion());
    }

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if(!canTouch)
        {
            return false;
        }
        if(!endItemMotion)
        {
            ui.gameObject.SetActive(true);
            gameObject.SetActive(false);
        }
        else
        {
            SetCollect();
        }
        AudioManager.Instance.PlaySound("cat_7", transform.position);
        return false;
    }

    private IEnumerator _TouchMotion()
    {
        canTouch = true;
        StartCoroutine(_FailMotion());
        yield break;
    }

    private IEnumerator _FailMotion()
    {
        int index = 0;
        eyeRenderer.enabled = false;
        while (true)
        {
            bodyRenderer.sprite = nonEyeBodySprites[index++];
            index %= nonEyeBodySprites.Length;
            yield return new WaitForSeconds(3.0f);
        }
    }
}
