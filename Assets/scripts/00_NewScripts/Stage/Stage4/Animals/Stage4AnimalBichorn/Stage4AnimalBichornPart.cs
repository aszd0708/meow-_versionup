﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage4AnimalBichornPart : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer spriteRenderer;

    [SerializeField]
    private Sprite[] movementSprites;

    [SerializeField]
    private Sprite[] eventSprite;

    [Header("도착 Transform")]
    [SerializeField]
    private Transform destinationTransform;

    [Header("도착하는데 걸리는 시간")]
    [SerializeField]
    private float moveTime;

    private Coroutine cor;

    public void StartEvent()
    {
        StartCoroutine(_Movement());
    }

    private IEnumerator _ChangeMotion(Sprite[] sprites, float motionTime)
    {
        int num = 0;
        WaitForSeconds wait = new WaitForSeconds(motionTime);

        while (true)
        {
            spriteRenderer.sprite = sprites[num];
            num++;
            num %= sprites.Length;
            yield return wait;
        }
    }

    private IEnumerator _Movement()
    {
        cor = StartCoroutine(_ChangeMotion(movementSprites, 0.25f));
        transform.DOMove(destinationTransform.position, moveTime);
        yield return new WaitForSeconds(moveTime);
        Boobies();
        yield break;
    }

    public void Boobies()
    {
        StopCoroutine(cor);
        cor = StartCoroutine(_ChangeMotion(eventSprite, 0.25f));
    }
}
