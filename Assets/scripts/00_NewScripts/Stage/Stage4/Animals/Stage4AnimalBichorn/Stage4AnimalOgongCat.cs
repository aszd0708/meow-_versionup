﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage4AnimalOgongCat : StageAnimalObjectBase
{
    private bool bCanCollect = false;

    public bool BCanCollect { get => bCanCollect; set => bCanCollect = value; }

    [SerializeField]
    private Transform dogHeadTransform;

    [SerializeField]
    private Transform[] moveTransforms;
    [SerializeField]
    private float moveTime = 0;

    [SerializeField]
    private bool bIsFirstCollect = false;
    
    private bool bCanTouch = false;

    [SerializeField]
    private GameObject bong;

    [SerializeField]
    private GameObject light;

    public void FirstItem()
    {
        if (bIsFirstCollect)
        {
            StartCoroutine(_Motion());
            bIsFirstCollect = false;
        }
    }

    public override void StartItemEvent(GameObject itemObj)
    {
        if (endItemMotion)
        {
            PoolingManager.Instance.SetPool(itemObj, "ItemDummy");
            return;
        }
        if (BCanCollect == true)
        {
            base.StartItemEvent(itemObj);
            AudioManager.Instance.PlaySound("king", transform.position);
            PoolingManager.Instance.SetPool(itemObj, "ItemDummy");
            // 봉 돌림
            if (bong.activeSelf == false)
            {
                bong.SetActive(true);
            }
            bCanTouch = true;
            GetComponent<SpriteRenderer>().sortingOrder = 1;
        }
    }

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if (bCanTouch)
        {
            if (Info.IsCollectVirtual)
            {
                StartCoroutine(_Motion());
            }
            else
            {
                bIsFirstCollect = true;
                SetCollect();
            }
            bCanTouch = false;
        }
        return false;
    }
   
    public void FiveTime()
    {
        dogHeadTransform.DOMove(new Vector3(-200, -200, 0), 5.0f).SetEase(Ease.Linear);
        dogHeadTransform.DORotate(new Vector3(0, 0, 359), 5.0f);
        PoolingManager.Instance.SetPool(dogHeadTransform.gameObject, "Trash", 5.0f);
        BCanCollect = true;
        light.SetActive(true);
        PoolingManager.Instance.SetPool(light, "Trash", 0.2f);
    }

    private IEnumerator _Motion()
    {
        WaitForSeconds wait = new WaitForSeconds(moveTime);
        bCanTouch = false;
        for (int i = 0; i < moveTransforms.Length; i++)
        {
            //transform.LookAt(moveTransforms[i]);
            //transform.rotation = Quaternion.Euler( new Vector3(0, 0, transform.rotation.eulerAngles.z));
            transform.DOMove(moveTransforms[i].position, moveTime).SetEase(Ease.Linear);
            yield return wait;
        }
        yield break;
    }
}
