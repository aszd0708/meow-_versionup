﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage4AnimalGomsoon : StageAnimalObjectBase
{
    [System.Serializable]
    public struct MotionSprite
    {
        public Sprite bodySprite;
        public Sprite clothSprite;
        public Sprite snoodSprite;
    }

    [SerializeField]
    private MotionSprite idleMotion;

    [SerializeField]
    private MotionSprite[] runMotion;
    [SerializeField]
    private float runMotionSpriteChangeTime;

    [SerializeField]
    private Transform[] runPositions;

    [Header("움직일때 걸리는 시간 적으면 적을수록 늦게감")]
    [SerializeField]
    private float runningTime;

    [SerializeField]
    private SpriteRenderer bodyRenderer;

    [SerializeField]
    private SpriteRenderer clothRenderer;

    [SerializeField]
    private SpriteRenderer snoodRenderer;

    [SerializeField]
    private bool isRun;

    private int runTransformIndex = 0;

    private bool bIsFirstCollect;

    public bool IsRun
    {
        get
        {
            return isRun;
        }
        set 
        { 
            isRun = value; 
            if(isRun)
            {
                if(runCoroutine == null)
                {
                    runCoroutine = StartCoroutine(_Run());
                }
            }

            else
            {
                if(runCoroutine != null)
                {
                    StopCoroutine(runCoroutine);
                    runCoroutine = null;
                }
            }
        }
    }

    private Coroutine runCoroutine;

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if (Info.IsCollectVirtual)
        {
            IsRun = true;
        }
        if (endItemMotion)
        {
            SetCollect();
        }
        AudioManager.Instance.PlaySound("dog_1", transform.position);
        return false;
    }

    public override void StartItemEvent(GameObject itemObj)
    {
        if (endItemMotion)
        {
            PoolingManager.Instance.SetPool(itemObj, "ItemDummy");
            return;
        }
        base.StartItemEvent(itemObj);

        PoolingManager.Instance.SetPool(itemObj, "ItemDummy");
        SetSpriteLayer(transform, "2");
        endItemMotion = true;
        clothRenderer.enabled = true;
        snoodRenderer.enabled = true;
        IsRun = false;
        transform.DOPause();
        if (Info.IsCollectVirtual)
        {
            IsRun = false;
        }
        else
        {
            bIsFirstCollect = true;
        }
    }

    private void SetSpriteLayer(Transform obj, string layerName)
    {
        if(obj.GetComponent<SpriteRenderer>() != null)
        {
            obj.GetComponent<SpriteRenderer>().sortingLayerName = layerName;
        }

        for(int i = 0; i < obj.childCount; i++)
        {
            SetSpriteLayer(obj.GetChild(i), layerName);
        }
    }

    public void FirstItem()
    {
        if(bIsFirstCollect)
        {
            IsRun = true;

            StartCoroutine(_PlayRunMotion());
            bIsFirstCollect = false;
        }
    }

    private void Start()
    {
        clothRenderer.enabled = false;
        snoodRenderer.enabled = false;

        IsRun = true;
    }

    private IEnumerator _PlayRunMotion()
    {
        WaitForSeconds wait = new WaitForSeconds(runMotionSpriteChangeTime);

        int index = 0;
        while(IsRun)
        {
            bodyRenderer.sprite = runMotion[index].bodySprite;
            clothRenderer.sprite = runMotion[index].clothSprite;
            snoodRenderer.sprite = runMotion[index].snoodSprite;
            index++;
            index %= runMotion.Length;
            yield return wait;
        }
        bodyRenderer.sprite = idleMotion.bodySprite;
        clothRenderer.sprite = idleMotion.clothSprite;
        snoodRenderer.sprite = idleMotion.snoodSprite;

        yield break;
    }

    private IEnumerator _Run()
    {
        StartCoroutine(_PlayRunMotion());
        while(IsRun)
        {
            float currentRunTime = Vector2.Distance(runPositions[runTransformIndex].position, transform.position) * runningTime;
            float yDirection = transform.position.x - runPositions[runTransformIndex].position.x;

            if(yDirection < 0)
            {
                transform.rotation = Quaternion.Euler(Vector2.up * 180);
            }
            else
            {
                transform.rotation = Quaternion.Euler(Vector2.zero);
            }

            transform.DOMove(runPositions[runTransformIndex].position, currentRunTime).SetEase(Ease.Linear);
            yield return new WaitForSeconds(currentRunTime);
            runTransformIndex++;
            runTransformIndex %= runPositions.Length;
        }
        yield break;
    }
}
