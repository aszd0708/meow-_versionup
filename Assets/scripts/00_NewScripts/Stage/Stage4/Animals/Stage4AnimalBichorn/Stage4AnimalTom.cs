﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage4AnimalTom : StageAnimalObjectBase
{
    [SerializeField]
    private SpriteRenderer spriteRenderer;

    [SerializeField]
    private Sprite[] frontMotionSprites;
    [SerializeField]
    private int frontMotionCount;
    [SerializeField]
    private float frontMotionTime;

    [SerializeField]
    private Sprite[] backMotionSprites;
    [SerializeField]
    private int backMotionCount;
    [SerializeField]
    private float backMotionTime;

    [SerializeField]
    private float restMotionTime;

    private void Start()
    {
        StartCoroutine(_IdleMotion());
    }

    public override bool DoTouch(Vector2 touchPose = default)
    {
        SetCollect();
        AudioManager.Instance.PlaySound("cat_5", transform.position);
        return false;
    }

    private IEnumerator _Motion(Sprite[] motionSprites, int motionCount, float motionTime)
    {
        WaitForSeconds wait = new WaitForSeconds(motionTime);

        for(int count = 0; count < motionCount; count++)
        {
            for (int i = 0; i < motionSprites.Length; i++)
            {
                spriteRenderer.sprite = motionSprites[i];
                yield return wait;
            }
        }
        yield break;
    }

    private IEnumerator _IdleMotion()
    {
        WaitForSeconds wait = new WaitForSeconds(restMotionTime);
        while (true)
        {
            yield return StartCoroutine(_Motion(backMotionSprites, backMotionCount, backMotionTime));
            yield return wait;
            yield return StartCoroutine(_Motion(frontMotionSprites, frontMotionCount, frontMotionTime));
            yield return wait;
        }
    }
}
