﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage4AnimalCerberus : StageAnimalObjectBase
{
    [SerializeField]
    private SpriteRenderer leftBodyRender;
    [SerializeField]
    private Sprite[] leftBodySprites;
    [SerializeField]
    private Sprite leftIdleSprite;
    [SerializeField]
    private SpriteRenderer rightBodyRender;
    [SerializeField]
    private Sprite[] rightBodySprites;
    [SerializeField]
    private Sprite rightIdleSprite;
    [SerializeField]
    private SpriteRenderer centerBodyRender;
    [SerializeField]
    private Sprite[] centerBodySprites;
    [SerializeField]
    private Sprite centerIdleSprite;

    [SerializeField]
    private Transform[] movePose;

    [SerializeField]
    private float restTime;

    [SerializeField]
    private SpriteRenderer leftFaceRender;
    [SerializeField]
    private Sprite[] leftFaceSprites;
    [SerializeField]
    private SpriteRenderer rightFaceRender;
    [SerializeField]
    private Sprite[] rightFaceSprites;
    [SerializeField]
    private SpriteRenderer centerFaceRender;
    [SerializeField]
    private Sprite[] centerFaceSprites;

    private Coroutine moveCor;
    private Coroutine bodySpriteCor;
    private Coroutine faceSpriteCor;

    private void Start()
    {
        moveCor = StartCoroutine(_Move());
        bodySpriteCor = StartCoroutine(_MoveSprite());
        faceSpriteCor = StartCoroutine(_FaceSprite());
    }

    public override bool DoTouch(Vector2 touchPose = default)
    {
        StopCoroutine(moveCor);
        StopCoroutine(bodySpriteCor);

        transform.DOKill();

        leftBodyRender.sprite = leftIdleSprite;
        rightBodyRender.sprite = rightIdleSprite;
        centerBodyRender.sprite = centerIdleSprite;

        SetCollect();
        AudioManager.Instance.PlaySound("dog_6", transform.position);
        return false;
    }

    private IEnumerator _Move()
    {
        int index = 0;
        WaitForSeconds wait = new WaitForSeconds(restTime);
        while(true)
        {
            float distance = Vector3.Distance(transform.position, movePose[index].position);
            transform.DOMove(movePose[index].position, distance / 2).SetEase(Ease.Linear);
            index++;
            index %= movePose.Length;
            yield return new WaitForSeconds(distance / 2);
            yield return wait;
        }
    }

    private IEnumerator _MoveSprite()
    {
        int index = 0;
        WaitForSeconds wait=  new WaitForSeconds(0.5f);
        while(true)
        {
            leftBodyRender.sprite = leftBodySprites[index];
            rightBodyRender.sprite = rightBodySprites[index];
            centerBodyRender.sprite = centerBodySprites[index];
            index++;
            index %= centerBodySprites.Length;
            yield return wait;
        }
    }

    private IEnumerator _FaceSprite()
    {
        int index = 0;
        WaitForSeconds wait = new WaitForSeconds(0.5f);
        while (true)
        {
            leftFaceRender.sprite = leftFaceSprites[index];
            rightFaceRender.sprite = rightFaceSprites[index];
            centerFaceRender.sprite = centerFaceSprites[index];
            index++;
            index %= centerFaceSprites.Length;
            yield return wait;
        }
    }
}
