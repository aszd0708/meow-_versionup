﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage4AnimalTiger : StageAnimalObjectBase
{
    [SerializeField]
    private GameObject[] walkingObject;

    [SerializeField]
    private Transform walkingDest;

    [SerializeField]
    private float walkingTime;

    [SerializeField]
    private GameObject standingObject;

    [SerializeField]
    private SpriteRenderer[] faceRenderers;

    [SerializeField]
    private Sprite[] faceSprites;

    [SerializeField]
    private SpriteRenderer[] dduckRenderer;

    [SerializeField]
    private float faceChangeTime;

    private bool bCanTouch = false;

    [SerializeField]
    private SpriteRenderer groundDduck;

    private void Start()
    {
    }

    public override void StartItemEvent(GameObject itemObj)
    {
        if (endItemMotion)
        {
            PoolingManager.Instance.SetPool(itemObj, "ItemDummy");
            return;
        }

        base.StartItemEvent(itemObj);

        StartCoroutine(_GoToDestination());
        StartCoroutine(_FaceSpriteChanger());
        groundDduck.enabled = true;
        itemObj.SetActive(false);
    }
    public override bool DoTouch(Vector2 touchPose = default)
    {
        if(bCanTouch == true)
        {
            SetCollect();
            AudioManager.Instance.PlaySound("cat_9", transform.position);
        }
        return false;
    }

    private IEnumerator _GoToDestination()
    {
        transform.DOMove(walkingDest.position, walkingTime).SetEase(Ease.Linear);

        int index = 0;
        WaitForSeconds wait = new WaitForSeconds(walkingTime / 6);
        for(int i = 0; i < 6; i++)
        {
            walkingObject[index].SetActive(true);
            yield return wait;
            walkingObject[index].SetActive(false);
            index++;
            index %= walkingObject.Length;
        }

        groundDduck.enabled = false;
        //yield return new WaitForSeconds(walkingTime);
        for (int i = 0; i < dduckRenderer.Length; i++)
        {
            dduckRenderer[i].enabled = true;
        }
        standingObject.SetActive(true);
        bCanTouch = true;
        yield break;
    }

    private IEnumerator _FaceSpriteChanger()
    {
        WaitForSeconds wait = new WaitForSeconds(faceChangeTime);
        int index = 0;
        while(true)
        {
            for(int i = 0; i < faceRenderers.Length; i++)
            {
                if(faceRenderers[i].gameObject.activeSelf)
                {
                    faceRenderers[i].sprite = faceSprites[index++];
                }
                index %= faceSprites.Length;
            }
            yield return wait;
        }
    }
}
