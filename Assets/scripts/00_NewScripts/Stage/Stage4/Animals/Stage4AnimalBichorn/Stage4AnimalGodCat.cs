﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using BitBenderGames;

public class Stage4AnimalGodCat : StageAnimalObjectBase
{
    #region 모션
    [SerializeField]
    private Transform armTransform;


    [System.Serializable]
    public struct Pose
    {
        public Transform startPose;
        public Transform middlePose;
        public Transform endPose;
    }
    [SerializeField]
    private Pose[] motionPose;
    #endregion 

    [SerializeField]
    private Stage4ObjectLightAndTree[] tree;

    #region Camera
    [SerializeField]
    private MobileTouchCamera cameraSetting;
    [SerializeField]
    private Transform cameraTransform;
    [SerializeField]
    private float zoomSize;
    #endregion

    private int motionPointer = 0;

    private bool bIsFirstCollect = false;

    private bool bCanTouch = false;

    [SerializeField]
    private Stage4AnimalOgongCat ogong;

    [SerializeField]
    private Transform ogongStart, ogongMiddle, ogongEnd;

    #region
    [SerializeField]
    private Transform poolMaskTransform;

    [SerializeField]
    private SpriteMask mask;

    [SerializeField]
    private float poolMotionTime = 3.0f;

    [SerializeField]
    private Ease poolMotionEase = Ease.OutBack;
    #endregion

    private bool bDoingMotion = false;

    public override void StartItemEvent(GameObject itemObj)
    {
        base.StartItemEvent(itemObj);
        if (bDoingMotion) { return; }

        if (motionPointer <= tree.Length)
        {
            PoolingManager.Instance.SetPool(itemObj, "ItemDummy");

            StartCoroutine(_PoolStartMotion());
        }
        else
        {
            PoolingManager.Instance.SetPool(itemObj, "ItemDummy");
            return;
        }
    }
    public override bool DoTouch(Vector2 touchPose = default)
    {
        if (bCanTouch)
        {
            if (Info.IsCollectVirtual)
            {
                if (motionPointer < tree.Length)
                {
                    StartCoroutine(_MoveToMiddle(transform.position, motionPose[motionPointer].middlePose.position, false));
                }
                else
                {
                    StartCoroutine(_MoveToMiddle(transform.position, ogongMiddle.position, true));
                }
            }
            else
            {
                SetCollect();
            }
            AudioManager.Instance.PlaySound("god", transform.position);
        }
        return false;
    }

    #region Backup
    /*    
    public override void StartItemEvent(GameObject itemObj)
    {
        if(bDoingMotion)
        {
            return;
        }

        base.StartItemEvent(itemObj);
        if(motionPointer < tree.Length)
        {
            PoolingManager.Instance.SetPool(itemObj, "ItemDummy");
            StartCoroutine(_MoveToMiddle(motionPose[motionPointer].startPose.position, motionPose[motionPointer].middlePose.position));
            bCanTouch = true;
        }
        else if (motionPointer == tree.Length)
        {
            StartCoroutine(_MoveToMiddle(ogongStart.position, ogongMiddle.position));
            bCanTouch = true;
        }
        else
        {
            return;
        }
    }
     */

    /*
    public override bool DoTouch(Vector2 touchPose = default)
    {
        if(bCanTouch)
        {
            if (Info.IsCollectVirtual)
            {
                if (motionPointer < tree.Length)
                {
                    StartCoroutine(_StartMotion());
                }
                else if (motionPointer == tree.Length)
                {
                    StartCoroutine(_SpecialMotion());
                }
            }
            else
            {
                SetCollect();
                //bCanTouch = true;
                bIsFirstCollect = true;
            }
            bCanTouch = false;
        }
        return false;
    }
    */
    #endregion

    private IEnumerator _PoolStartMotion()
    {
        bCanTouch = false;
        transform.position = poolMaskTransform.position;
        float moveY = transform.position.y + 4.5f;
        transform.DOMoveY(moveY, poolMotionTime).SetEase(poolMotionEase);
        yield return new WaitForSeconds(poolMotionTime);
        bCanTouch = true;
        yield break;
    }

    public void FirstItem()
    {
        if (bIsFirstCollect)
        {
            StartCoroutine(_StartMotion());
            bIsFirstCollect = false;
        }
    }

    private IEnumerator _SpecialMotion()
    {
        Vector3 pose = ogongMiddle.position;
        StartCoroutine(_CameraMove(pose));
        yield return new WaitForSeconds(2.0f);
        armTransform.DORotate(new Vector3(0, 0, -15), 0.5f);
        yield return new WaitForSeconds(0.5f);
        ogong.gameObject.SetActive(true);
        ogong.BCanCollect = true;
        ogong.FiveTime();
        armTransform.DORotate(new Vector3(0, 0, 15), 0.5f);
        yield return new WaitForSeconds(0.5f);
        armTransform.DORotate(new Vector3(0, 0, 0), 0.5f);
        transform.DOMove(ogongEnd.position, 2.0f);
        motionPointer++;
        bDoingMotion = false;
        yield break;
    }

    private IEnumerator _MoveToMiddle(Vector3 startPose, Vector3 middlePose, bool bIsOgong)
    {
        bDoingMotion = true;
        transform.position = startPose;
        transform.DOMove(middlePose, 2.0f);
        Vector3 pose = middlePose;        
        yield return StartCoroutine(_CameraMove(pose));

        if (bIsOgong == true)
        {
            StartCoroutine(_SpecialMotion());
        }
        else
        {
            StartCoroutine(_StartMotion());
        }
        yield break;
    }

    private IEnumerator _StartMotion()
    {
        armTransform.DORotate(new Vector3(0, 0, -15), 1.0f);
        yield return new WaitForSeconds(1.0f);
        tree[motionPointer].Light();
        armTransform.DORotate(new Vector3(0, 0, 15), 0.5f);
        yield return new WaitForSeconds(0.5f);
        armTransform.DORotate(new Vector3(0, 0, 0), 0.5f);
        transform.DOMove(motionPose[motionPointer].endPose.position, 2.0f);
        motionPointer++;
        bDoingMotion = false;
        bCanTouch = false;
        yield break;
    }

    private IEnumerator _CameraMove(Vector3 pose)
    {
        float min = cameraSetting.CamZoomMax;
        StageTouchManagerBase.Instance.ForbitTouch(false);
        cameraTransform.DOMove(new Vector3(pose.x, pose.y, -10), 1.25f).SetEase(Ease.Linear);
        //StartCoroutine(ZoomOut(min, 10, 1.0f));
        //StartCoroutine(ZoomOut(10, min, 1.0f));
        CameraZoomManager.Instance.Zoom(cameraSetting.CamZoom, 10, 1.0f);
        yield return new WaitForSeconds(1.0f);
        cameraSetting.CamZoomMax = min;
        cameraSetting.ResetCameraBoundaries();
        StageTouchManagerBase.Instance.ForbitTouch(true);
        yield break;
    }

    private IEnumerator ZoomOut(float max, float min, float animeTime)
    {
        float zoomMax = max, zoomMin = min;
        float time = 0.0f;

        while (zoomMax > zoomMin)
        {
            time += Time.deltaTime / animeTime;

            zoomMin = Mathf.Lerp(min, max, time);

            cameraSetting.CamZoomMax = zoomMin;
            cameraSetting.ResetCameraBoundaries();
            yield return null;
        }
        yield break;
    }

    private IEnumerator ZoomIn(float max, float min, float animeTime)
    {
        float zoomMax = max, zoomMin = min;
        float time = 0.0f;

        while (zoomMax > zoomMin)
        {
            time += Time.deltaTime / animeTime;

            zoomMin = Mathf.Lerp(min, max, time);

            cameraSetting.CamZoomMax = zoomMin;
            cameraSetting.ResetCameraBoundaries();
            yield return null;
        }
        yield break;
    }
}
