﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage4AnimalOgongCatYeouibong : MonoBehaviour
{
    [SerializeField]
    [Header("날개 돌아가는 속도")]
    private float wingSpinSpeed;

    private float nowSpinSpeed = 0;

    private float speed = 0;

    [SerializeField]
    private bool isSpin = true;

    [SerializeField]
    private float maxSpeed = 2000f;

    void Update()
    {
        if (isSpin)
        {
            nowSpinSpeed += wingSpinSpeed;
        }

        else
        {
            nowSpinSpeed -= wingSpinSpeed * 5;
            if (nowSpinSpeed <= 0)
            {
                nowSpinSpeed = 0;
            }
        }
        speed += Time.deltaTime * nowSpinSpeed;
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, speed);
        nowSpinSpeed = Mathf.Clamp(nowSpinSpeed, 0, maxSpeed);
    }
}
