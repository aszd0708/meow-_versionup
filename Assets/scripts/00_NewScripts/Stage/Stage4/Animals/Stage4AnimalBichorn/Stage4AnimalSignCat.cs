﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage4AnimalSignCat : StageAnimalObjectBase
{
    [SerializeField]
    private SpriteRenderer bodyRenderer;

    [SerializeField]
    private Sprite[] bodySprites;

    [SerializeField]
    private SpriteRenderer tailRenderer;

    [SerializeField]
    private Sprite questionTailSprites;
    [SerializeField]
    private Sprite exclamationTailSprites;

    [SerializeField]
    private Transform tailTransform;

    [SerializeField]
    private float tailMotionTime;

    public enum State
    {
        QUESTION, EXCLAMATION
    }

    [SerializeField]
    private State nowState;

    private bool canTouch = false;

    Coroutine tailCor;

    private void Start()
    {
        bodyRenderer.sprite = bodySprites[1];
        tailCor = StartCoroutine(_QuestionTailMotion());
    }

    public override void StartItemEvent(GameObject itemObj)
    {
        if (endItemMotion)
        {
            PoolingManager.Instance.SetPool(itemObj, "ItemDummy");
            return;
        }
        base.StartItemEvent(itemObj);
        StopCoroutine(tailCor);
        bodyRenderer.sprite = bodySprites[0];
        tailTransform.DOPause();
        StartCoroutine(_ExclamationTailMotion());
    }

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if(!canTouch)
        {
            switch(nowState)
            {
                case State.EXCLAMATION:
                    tailRenderer.sprite = questionTailSprites;
                    nowState = State.QUESTION;
                    break;
                case State.QUESTION:
                    tailRenderer.sprite = exclamationTailSprites;
                    nowState = State.EXCLAMATION;
                    break;
            }
            return false;
        }
        else
        {
            SetCollect();
        }
        AudioManager.Instance.PlaySound("cat_11", transform.position);
        return false;
    }

    private IEnumerator _QuestionTailMotion()
    {
        WaitForSeconds wait = new WaitForSeconds(1.0f);
        while(true)
        {
            tailTransform.DORotate(Vector3.forward * 15, 1.0f);
            yield return wait;
            tailTransform.DORotate(Vector3.forward * -15, 1.0f);
            yield return wait;
        }
    }

    private IEnumerator _ExclamationTailMotion()
    {
        tailRenderer.sprite = exclamationTailSprites;
        tailTransform.rotation = Quaternion.Euler(Vector3.forward * 20);
        tailTransform.DORotate(Vector3.zero, 0.5f).SetEase(Ease.OutElastic);
        yield return new WaitForSeconds(0.5f);
        nowState = State.QUESTION;
        canTouch = true;
        yield break;
    }
}
