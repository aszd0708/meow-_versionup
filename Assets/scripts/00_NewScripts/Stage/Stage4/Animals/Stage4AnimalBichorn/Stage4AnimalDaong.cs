﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage4AnimalDaong : StageAnimalObjectBase
{
    [SerializeField]
    private SpriteRenderer spriteRenderer;

    [SerializeField]
    private Sprite[] idleSprites;
    [SerializeField]
    private float idleMotionTime;

    [SerializeField]
    private Sprite readySprite;

    [SerializeField]
    private Sprite jumpSprite;

    [SerializeField]
    private Transform[] jumpToGroundPose;
    [SerializeField]
    private Transform jumpToUpPose;

    private int tranformIndex = 0;

    [SerializeField]
    private float jumpPower;
    [SerializeField]
    private float jumpTime;

    private Coroutine idleCor;

    public enum State
    {
        IDLE,
        ON_JAR,
        CAN_CAPTURE
    }

    public State currentState;

    [SerializeField]
    private Ease jumpEase;

    private bool canTouch = true;

    private void Start()
    {
        idleCor = StartCoroutine(_IdleMotion());
    }

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if(!canTouch)
        {
            return false;
        }
        switch(currentState)
        {
            case State.IDLE:
                if(Info.IsCollectVirtual)
                {
                    currentState = State.ON_JAR;
                }
                else
                {
                    currentState = State.CAN_CAPTURE;
                }
                StartCoroutine(_PlayCoroutine());
                break;
            case State.CAN_CAPTURE:
                SetCollect();
                currentState = State.ON_JAR;
                break;
            case State.ON_JAR:
                currentState = State.IDLE;
                StartCoroutine(_PlayCoroutine());
                break;
        }
        AudioManager.Instance.PlaySound("cat_4", transform.position);
        return false;
    }

    private IEnumerator _PlayCoroutine()
    {
        canTouch = false;
        StopCoroutine(idleCor);
        switch(currentState)
        {
            case State.IDLE:
                yield return StartCoroutine(_jumpToGround());
                break;
            case State.CAN_CAPTURE:
            case State.ON_JAR:
                yield return StartCoroutine(_JumpToJar());
                break;
               
        }
        idleCor = StartCoroutine(_IdleMotion());
        canTouch = true;
        yield break;
    }

    private IEnumerator _IdleMotion()
    {
        WaitForSeconds wait = new WaitForSeconds(idleMotionTime);
        int index = 0;
        while(true)
        {
            spriteRenderer.sprite = idleSprites[index];
            index++;
            index %= idleSprites.Length;
            yield return wait;
        }
    }

    private IEnumerator _JumpToJar()
    {
        spriteRenderer.sprite = jumpSprite;
        SetRotation(jumpToUpPose.position);
        transform.DOJump(jumpToUpPose.position, jumpPower, 1, jumpTime).SetEase(Ease.Linear);
        transform.DORotate(new Vector3(0, transform.rotation.eulerAngles.y, -30), jumpTime).SetEase(jumpEase);
        yield return new WaitForSeconds(jumpTime);
        spriteRenderer.sprite = readySprite;
        transform.rotation = Quaternion.identity;
        yield break;
    }

    private IEnumerator _jumpToGround()
    {
        spriteRenderer.sprite = jumpSprite;
        SetRotation(jumpToGroundPose[tranformIndex].position);
        transform.DOJump(jumpToGroundPose[tranformIndex].position, jumpPower, 1, jumpTime).SetEase(Ease.Linear);
        transform.DORotate(new Vector3(0, transform.rotation.eulerAngles.y, -60), jumpTime).SetEase(jumpEase);
        yield return new WaitForSeconds(jumpTime);
        spriteRenderer.sprite = readySprite;
        transform.rotation = Quaternion.identity;
        tranformIndex++;
        tranformIndex %= jumpToGroundPose.Length;
        yield break;
    }

    private void SetRotation(in Vector2 pose)
    {
        float temp = transform.position.x - pose.x;

        if(temp > 0)
        {
            transform.rotation = Quaternion.Euler(Vector3.up * 180);
        }

        else
        {
            transform.rotation = Quaternion.Euler(Vector3.zero);
        }
    }
}
