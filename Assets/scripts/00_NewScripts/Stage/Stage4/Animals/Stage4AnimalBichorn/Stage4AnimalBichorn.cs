﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage4AnimalBichorn : StageAnimalObjectBase
{
    [SerializeField]
    private Stage4AnimalBichornPart[] parts;

    [SerializeField]
    private Stage4AnimalBichornYarn[] yarn;

    private bool canTouch = false;

    private bool doItemEvent = false;

    [SerializeField]
    private SpriteRenderer woolRender;

    public override void StartItemEvent(GameObject itemObj)
    {
        if (endItemMotion)
        {
            PoolingManager.Instance.SetPool(itemObj, "ItemDummy");
            return;
        }

        base.StartItemEvent(itemObj);
        itemObj.SetActive(false);
        woolRender.enabled = true;
        if (!doItemEvent)
        {
            for (int i = 0; i < parts.Length; i++)
            {
                parts[i].StartEvent();
            }
            for (int i = 0; i < parts.Length; i++)
            {
                yarn[i].SetYarnLine();
            }
            doItemEvent = true;
            canTouch = true;
        }
    }

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if(!canTouch)
        {
            return false;
        }
        SetCollect();
        AudioManager.Instance.PlaySound("dog_3", transform.position);

        return false;
    }
}