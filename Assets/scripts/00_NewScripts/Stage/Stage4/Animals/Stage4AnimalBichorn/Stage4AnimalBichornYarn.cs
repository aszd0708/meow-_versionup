﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage4AnimalBichornYarn : MonoBehaviour
{
    [SerializeField]
    private LineRenderer yarn;

    [SerializeField]
    private float yarnTime;

    private Vector3[] linePositions;

    private void Start()
    {
        SetLinePositions();
    }

    private void SetLinePositions()
    {
        if(yarn)
        {
            linePositions = new Vector3[yarn.positionCount];

            for (int i = 0; i < linePositions.Length; i++)
            {
                linePositions[i] = yarn.GetPosition(i);
            }
        }

        yarn.enabled = false;
    }

    public void SetYarnLine()
    {
        if(yarn)
        {
            StartCoroutine(_SetYarnPositions());
        }
    }

    private IEnumerator _SetYarnPositions()
    {
        int index = 0;
        WaitForSeconds wait = new WaitForSeconds(0.25f);

        yarn.enabled = true;
        while (true)
        {
            for (int i = 0; i < yarn.positionCount; i++)
            {
                if(i < index)
                {
                    yarn.SetPosition(i, linePositions[i]);
                }
                else
                {
                    yarn.SetPosition(i, linePositions[index]);
                }
            }
            index++;
            index %= yarn.positionCount;
            yield return wait;
        }
    }
}
