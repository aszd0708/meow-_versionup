﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class Stage4AnimalBrownCatUI : MonoBehaviour
{
    [SerializeField]
    private RectTransform rect;

    private Vector3 startPose;

    [SerializeField]
    private Vector3 settingButtonTransform;
    [SerializeField]
    private Button settingBtn;

    [SerializeField]
    private GameObject sitObj;
    [SerializeField]
    private GameObject standObj;

    [SerializeField]
    private Stage4AnimalBrownCat cat;

    private void Awake()
    {
        startPose = rect.localPosition;
    }

    private void OnEnable()
    {
        sitObj.SetActive(true);
        StartCoroutine(_Motion());
    }

    private void SetOffActive()
    {
        cat.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }

    private IEnumerator _Motion()
    {
        yield return new WaitForSeconds(1.0f);
        sitObj.SetActive(false);
        standObj.SetActive(true);
        rect.DOLocalJump(settingButtonTransform, 20f, 1, 1.0f).SetEase(Ease.Linear);
        yield return new WaitForSeconds(1.0f);
        settingBtn.onClick.Invoke();
        yield return new WaitForSeconds(0.5f);
        standObj.SetActive(false);
        rect.localPosition = startPose;
        SetOffActive();
        yield break;
    }
}
