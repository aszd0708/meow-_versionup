﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;

public class Stage4ObjectHelicopter : MonoBehaviour, TouchableObj
{
    [SerializeField]
    private Transform waterHoleTransform;

    [SerializeField]
    private GameObject water;

    [SerializeField]
    private int waterCount = 5;

    [SerializeField]
    private float waterDistance;

    [SerializeField]
    private Transform startPose;

    [SerializeField]
    private Transform destPose;

    [SerializeField]
    private float speed;

    [SerializeField]
    private Transform tree;

    private bool putOffFire;

    [SerializeField]
    private UnityEvent afterPutOffEvent;

    private void OnEnable()
    {
        putOffFire = false;
    }

    public void CallHelicopter()
    {
        transform.position = startPose.position;
        StartCoroutine(_HelicopterSountPlaye());
    }

    private IEnumerator _HelicopterSountPlaye()
    {
        for(int i = 0; i < 4; i++)
        {
            AudioManager.Instance.PlaySound("helicopter", transform.position);
            yield return new WaitForSeconds(0.5f);
        }
        yield break;
    }

    public void OnDisable()
    {
        transform.position = Vector2.zero;
    }

    private void Update()
    {
        Vector2 destination = (destPose.position - transform.position).normalized;

        transform.Translate(destination * speed * Time.deltaTime);

        if(Mathf.Abs(tree.position.x - transform.position.x) < waterDistance)
        {
            if(putOffFire == false)
            {
                StartCoroutine(_PutOffFire());
                putOffFire = true;
            }
        }

        if(Vector3.Distance(destPose.position, transform.position) <= 0.1f)
        {
            gameObject.SetActive(false);
        }
    }

    private IEnumerator _PutOffFire()
    {
        WaitForSeconds wait = new WaitForSeconds(0.01f);

        for (int i = 0; i < waterCount; i++)
        {
            GameObject effect = GetWaterObj();
            effect.transform.position = waterHoleTransform.position;
            effect.GetComponent<SpriteRenderer>().DOFade(0, 2.5f).SetEase(Ease.Linear);
            effect.transform.DOMoveY(effect.transform.position.y - 16, 2.5f);
            PoolingManager.Instance.SetPool(effect, "WaterEffect", 2.5f);
            yield return wait;
        }
        afterPutOffEvent.Invoke();
        yield break;
    }

    private GameObject GetWaterObj()
    {
        GameObject waterObj = PoolingManager.Instance.GetPool("WaterEffect");
        if(waterObj == null)
        {
            waterObj = Instantiate(water);
        }
        waterObj.GetComponent<SpriteRenderer>().color = Color.white;
        return waterObj;
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        return false;
    }
}
