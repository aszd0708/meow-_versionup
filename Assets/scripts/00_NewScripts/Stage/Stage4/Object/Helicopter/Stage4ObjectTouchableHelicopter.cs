﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage4ObjectTouchableHelicopter : MonoBehaviour, TouchableObj
{
    [SerializeField]
    private Stage4ObjectHelicopterWing[] wings;

    [SerializeField]
    private Transform landingTransform;

    [SerializeField]
    private Transform[] movePose;

    [SerializeField]
    private float moveAngle;
    [SerializeField]
    private float angleTime = 1.5f;

    [SerializeField]
    private float flyingTime;

    [SerializeField]
    private float moveSpeed;

    [SerializeField]
    private float landingTime;

    private bool bCanTouch;

    public bool DoTouch(Vector2 touchPose = default)
    {
        if(bCanTouch)
        {
            StartCoroutine(PlayHelicopterSound());
            StartCoroutine(_Motion());
        }
        return false;
    }

    private IEnumerator PlayHelicopterSound()
    {
        WaitForSeconds wait = new WaitForSeconds(0.5f);
        for(int i = 0; i < 4; i++)
        {
            AudioManager.Instance.PlaySound("helicopter", transform.position);
            yield return wait;
        }
        yield break;
    }

    private void Start()
    {
        for (int i = 0; i < wings.Length; i++)
        {
            wings[i].IsSpin = false;
        }
        bCanTouch = true;
    }

    private IEnumerator _Motion()
    {
        bCanTouch = false;
        for (int i = 0; i < wings.Length; i++)
        {
            wings[i].IsSpin = true;
        }
        yield return new WaitForSeconds(3);

        int temp = Random.Range(0, movePose.Length);
        Vector3 firstMovePose = movePose[temp].position;
        temp++;
        temp %= movePose.Length;
        Vector3 secondMovePose = movePose[temp].position;

        transform.DOMoveY(firstMovePose.y, flyingTime).SetEase(Ease.InOutCubic);
        yield return new WaitForSeconds(flyingTime);

        float distance = Vector3.Distance(transform.position, firstMovePose);
        transform.DOMoveX(firstMovePose.x, distance / moveSpeed).SetEase(Ease.InQuint);
        Flip(firstMovePose.x);
        yield return new WaitForSeconds(distance / moveSpeed);

        transform.position = secondMovePose;

        distance = Mathf.Abs(transform.position.x - landingTransform.position.x);
        transform.DOMoveX(landingTransform.position.x, distance / moveSpeed).SetEase(Ease.OutQuint);
        yield return new WaitForSeconds((distance / moveSpeed));

        transform.DOMoveY(landingTransform.position.y, landingTime).SetEase(Ease.OutQuint);
        Flip(transform.position.x - 1);
        yield return new WaitForSeconds(landingTime);

        for (int i = 0; i < wings.Length; i++)
        {
            wings[i].IsSpin = false;
        }
        bCanTouch = true;
        yield break;
    }

    private void Flip(float valueX)
    {
        float temp = transform.position.x - valueX;

        if(temp > 0)
        {
            transform.rotation = Quaternion.identity;
        }
        else
        {
            transform.rotation = Quaternion.Euler(Vector3.up * 180);
        }
    }
}
