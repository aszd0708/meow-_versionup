﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage4ObjectHelicopterWing : MonoBehaviour
{
    [SerializeField]
    [Header("날개 돌아가는 속도")]
    private float wingSpinSpeed;

    private float nowSpinSpeed = 0;

    private float speed = 0;

    [SerializeField]
    private bool isSpin = false;

    [SerializeField]
    private float maxSpeed = 2000f;

    public bool IsSpin { get => isSpin; set => isSpin = value; }

    // Update is called once per frame
    void Update()
    {
        if(IsSpin)
        {
            nowSpinSpeed += wingSpinSpeed;
        }

        else
        {
            nowSpinSpeed -= wingSpinSpeed * 5;
            if(nowSpinSpeed <= 0)
            {
                nowSpinSpeed = 0;
            }   
        }
        speed += Time.deltaTime * nowSpinSpeed;
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, speed);
        nowSpinSpeed = Mathf.Clamp(nowSpinSpeed, 0, maxSpeed);
    }
}
