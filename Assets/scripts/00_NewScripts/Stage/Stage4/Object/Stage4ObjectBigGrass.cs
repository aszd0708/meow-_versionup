﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage4ObjectBigGrass : MonoBehaviour, TouchableObj
{
    [SerializeField]
    private SpriteRenderer grassRender;

    [SerializeField]
    private SpriteRenderer topRender;
    [SerializeField]
    private Transform topTransform;

    [SerializeField]
    private SpriteRenderer bottomRender;

    [SerializeField]
    private float animationTime = 1.5f;

    [SerializeField]
    private float moveDistance = 5.0f;

    private bool canTouch = true;

    public bool DoTouch(Vector2 touchPose = default)
    {
        if (canTouch == false) { return false; }

        TouchEvent();

        return false;
    }

    private void TouchEvent()
    {
        bool right = IntRandom.StateRandomInt();

        if(right == false)
        {
            moveDistance *= -1;
        }

        float movePoseX = topTransform.position.x + moveDistance;

        grassRender.enabled = false;
        topRender.enabled = true;
        bottomRender.enabled = true;

        topTransform.DOMoveX(movePoseX, animationTime);
        topRender.DOFade(0, animationTime);

        canTouch = false;
    }
}
