﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage4ObjectSmallStonTower : MonoBehaviour, TouchableObj
{
    [Header("작은 돌")]
    [SerializeField]
    private Collider2D smallStoneCollider;

    [SerializeField]
    private Rigidbody2D smallStoneRigid;

    [SerializeField]
    private GameObject[] collders;

    private bool isTouch = false;

    public bool DoTouch(Vector2 touchPose = default)
    {
        if(!isTouch)
        {
            StartCoroutine(_DropStone());
        }
        return false;
    }

    private IEnumerator _DropStone()
    {
        isTouch = true;
        smallStoneCollider.enabled = true;
        smallStoneRigid.bodyType = RigidbodyType2D.Dynamic;

        for(int i = 0; i < collders.Length; i++)
        {
            collders[i].SetActive(true);
        }
        yield break;
    }
}
