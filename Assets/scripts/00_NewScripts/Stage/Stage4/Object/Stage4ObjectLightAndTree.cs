﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;

public class Stage4ObjectLightAndTree : MonoBehaviour
{
    [SerializeField]
    private GameObject lightning;

    [SerializeField]
    private SpriteRenderer beforeTree;

    [SerializeField]
    private SpriteRenderer[] brokentTreeSprite;

    [SerializeField]
    private Transform brokenTree;
    [SerializeField]
    private float breakTime;

    [SerializeField]
    private Ease brokentEase = Ease.OutBounce;

    [SerializeField]
    private UnityEvent endEvent;

    private bool bIsBroken = false;

    public void Light()
    {
        StartCoroutine(_Light());
    }

    private IEnumerator _Light()
    {
        WaitForSeconds wait = new WaitForSeconds(0.05f);
        for(int i = 0; i < 3; i++)
        {
            lightning.SetActive(true);
            AudioManager.Instance.PlaySound("thunder", lightning.transform.position);
            yield return wait;
            yield return wait;
        }
        lightning.SetActive(false);
        if(bIsBroken == false)
        {
            StartCoroutine(_BrakeTree());
            bIsBroken = true;
        }
        else
        {
            endEvent.Invoke();
        }
        yield break;
    }

    private IEnumerator _BrakeTree()
    {
        beforeTree.enabled = false;
        for(int i = 0; i < brokentTreeSprite.Length; i++)
        {
            brokentTreeSprite[i].enabled = true;
        }
        brokenTree.DORotate(Vector3.forward * 100, breakTime).SetEase(brokentEase);
        yield return new WaitForSeconds(breakTime);
        endEvent.Invoke();
        yield break;
    }
}
