﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

/*
 * 이 오브젝트는 좀 특이하게 작동
 * 카메라의 거리에 따라 움직임
 * 카메라랑 가까워 이 오브젝트와 카메라가 가까히 있을경우 움직임
 * 카메라랑 멀 경우 움직임을 멈춤
 */

public class Stage4ObjectBirdCrane : MonoBehaviour, TouchableObj
{
    [Header("카메라 위치")]
    [SerializeField]
    private Transform cameraTransform;

    [SerializeField]
    private float stopDistance;
    [SerializeField]
    private float checkTime = 0.5f;

    private float currentDistance;

    [SerializeField]
    private Transform[] moveTransform;
    [SerializeField]
    private float moveTime;

    [SerializeField]
    private SpriteRenderer spriteRenderer;

    [SerializeField]
    private Sprite[] motionSprite;
    int spriteIndex = 0;

    private void Awake()
    {
        if(!cameraTransform)
        {
            cameraTransform = Camera.main.transform;
        }
    }

    private void Update()
    {
        if(cameraTransform)
        {
            currentDistance = Vector2.Distance(transform.position, cameraTransform.position);
        }
    }

    private void Start()
    {
        StartCoroutine(_Motion());
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        spriteRenderer.sprite = motionSprite[spriteIndex++];
        spriteIndex %= motionSprite.Length;
        return false;
    }

    private IEnumerator _Motion()
    {
        WaitForSeconds wait = new WaitForSeconds(checkTime);
        int index = 0;
        while(true)
        {
            if(currentDistance > stopDistance)
            {
                if(transform.position.x - moveTransform[index].position.x > 0)
                {
                    spriteRenderer.flipX = false;
                }
                else
                {
                    spriteRenderer.flipX = true;
                }
                transform.DOMove(moveTransform[index++].position, checkTime).SetEase(Ease.Linear);
                index %= moveTransform.Length;
            }
            yield return wait;
        }
    }
}
