﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage4ObjectSparrow : MonoBehaviour, TouchableObj
{
    [SerializeField]
    private SpriteRenderer spriteRenderer;

    [SerializeField]
    private Sprite idleSprite;
    [SerializeField]
    private Sprite[] flyingSprites;
    [SerializeField]
    private float flyingSpriteChangeSecond;

    [SerializeField]
    private float flyingTime = 10.0f;
    private float currentFlyingTime = 0;

    private bool canTouch = true;

    private Vector2 startPose;

    private Coroutine flyingMotionCor;

    [SerializeField]
    private float flyingSpeed;
    [SerializeField]
    private Vector2[] flyingPosition;

    private Vector2 currentFlyingPosition;

    private float currentCoolTime;

    [SerializeField]
    private float coolTime;

    [SerializeField]
    private Vector2[] poseRange;

    private Vector2[] clampRange = new Vector2[2];

    public enum State
    {
        STANDING, FLYING, COME
    }

    [SerializeField]
    private State currentState;

    private float jumpCoolTime;
    private float currentJumpCoolTime;

    private void Start()
    {
        startPose = transform.position;

        SetRandomPose();
    }

    private void OnEnable()
    {
        SetPose();
        clampRange[0] = SetMinRange();
        clampRange[1] = SetMaxRange();
        jumpCoolTime = Random.Range(1.0f, 5.0f);
    }

    private void SetPose()
    {
        Vector2 localPose = transform.localPosition;
        Vector2 randomPose = new Vector3(localPose.x + Random.Range(poseRange[0].x, poseRange[1].y), localPose.y + Random.Range(poseRange[0].y, poseRange[1].y));

        transform.localPosition = randomPose;
    }

    private Vector2 GetRandomPose()
    {
        Vector2 localPose = transform.localPosition;
        Vector2 randomPose = new Vector3(localPose.x + Random.Range(-0.1f, 0.1f), localPose.y + Random.Range(-0.1f, 0.1f));

        return randomPose;
    }

    private Vector2 SetMinRange()
    {
        Vector2 localPose = transform.localPosition;
        float minX = (poseRange[0].x < poseRange[1].x) ? poseRange[0].x : poseRange[1].x;
        float minY = (poseRange[0].y < poseRange[1].y) ? poseRange[0].y : poseRange[1].y;
        return new Vector2(localPose.x + minX, localPose.y + minY);
    }

    private Vector2 SetMaxRange()
    {
        Vector2 localPose = transform.localPosition;
        float maxX = (poseRange[0].x > poseRange[1].x) ? poseRange[0].x : poseRange[1].x;
        float maxY = (poseRange[0].y > poseRange[1].y) ? poseRange[0].y : poseRange[1].y;
        return new Vector2(localPose.x + maxX, localPose.y + maxY);
    }

    private void Update()
    {
        switch (currentState)
        {
            case State.STANDING:
                spriteRenderer.sprite = idleSprite;
                currentJumpCoolTime += Time.deltaTime;
                if (currentJumpCoolTime >= jumpCoolTime)
                {
                    Vector2 randomPose = GetRandomPose();
                    if (randomPose.x > transform.localPosition.x)
                    {
                        transform.rotation = Quaternion.Euler(Vector2.zero);
                    }
                    else
                    {
                        transform.rotation = Quaternion.Euler(Vector2.up * 180);
                    }
                    transform.DOLocalJump(GetRandomPose(), 0.1f, Random.Range(1, 3), 0.2f).SetEase(Ease.Linear);
                    currentJumpCoolTime = 0;
                    jumpCoolTime = Random.Range(1.0f, 5.0f);


                    transform.localPosition = new Vector2(
                            Mathf.Clamp(transform.localPosition.x, clampRange[0].x, clampRange[1].x),
                            Mathf.Clamp(transform.localPosition.y, clampRange[0].y, clampRange[1].y)
                    );
                }
                break;

            case State.FLYING:
                Flying();
                break;

            case State.COME:
                Come();
                break;
        }
    }

    private void SetRandomPose()
    {
        bool isFlip = IntRandom.StateRandomInt();
        spriteRenderer.flipX = isFlip;
    }

    private void Flying()
    {
        canTouch = false;
        if (flyingMotionCor == null)
        {
            flyingMotionCor = StartCoroutine(_FlyingSpriteChange());
        }

        if (currentFlyingPosition == Vector2.zero)
        {
            currentFlyingPosition = GetFlyingPosition();
            if (currentFlyingPosition.x < 0)
            {
                spriteRenderer.flipX = false;
            }
            else
            {
                spriteRenderer.flipX = true;
            }
        }
        else
        {
            transform.Translate(currentFlyingPosition * flyingSpeed * Time.deltaTime);
            currentFlyingTime += Time.deltaTime;
        }

        if (currentFlyingTime >= flyingTime)
        {
            currentState = State.COME;
            currentFlyingTime = 0;
        }
    }

    private void Come()
    {
        spriteRenderer.enabled = false;
        currentCoolTime += Time.deltaTime;
        if (currentCoolTime >= coolTime)
        {
            spriteRenderer.enabled = true;
            transform.position = Vector2.MoveTowards(transform.position, startPose, flyingSpeed * Time.deltaTime);
            if (startPose.x < transform.position.x)
            {
                spriteRenderer.flipX = false;
            }
            else
            {
                spriteRenderer.flipX = true;
            }

            if (Vector2.Distance(transform.position, startPose) <= 0.5f)
            {
                currentCoolTime = 0;
                currentState = State.STANDING;
                StopCoroutine(flyingMotionCor);
                flyingMotionCor = null;
                canTouch = true;
            }
        }
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        if (!canTouch)
        {
            return false;
        }

        currentState = State.FLYING;

        return false;
    }

    private IEnumerator _FlyingSpriteChange()
    {
        int index = 0;
        WaitForSeconds wait = new WaitForSeconds(flyingSpriteChangeSecond);
        canTouch = true;
        while (true)
        {
            spriteRenderer.sprite = flyingSprites[index++];
            index %= flyingSprites.Length;
            yield return wait;
        }
    }

    private Vector2 GetFlyingPosition()
    {
        Vector2 newVector = new Vector2(Random.Range(flyingPosition[0].x, flyingPosition[1].x), Random.Range(flyingPosition[0].y, flyingPosition[1].y));
        return newVector.normalized;
    }
}

