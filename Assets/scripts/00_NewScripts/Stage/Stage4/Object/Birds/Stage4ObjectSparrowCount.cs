﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage4ObjectSparrowCount : MonoBehaviour
{
    [SerializeField]
    private Stage4ObjectSparrow[] sparrows;

    private void Awake()
    {
        int random = Random.Range(1, 4);

        for(int i = 0; i < random; i++)
        {
            sparrows[i].gameObject.SetActive(true);
        }
    }
}
