﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage4ObjectBeacon : MonoBehaviour, TouchableObj
{
    private bool isBurn;

    [SerializeField]
    private Stage4ObjectBeaconFire fire;

    [SerializeField]
    private GameObject smoke;

    [SerializeField]
    private Transform smokeCreatePose;

    [SerializeField]
    private float smokeCooltime = 2.0f;

    private float currentSmokeTime = 0;

    private void Start()
    {
        isBurn = true;
        fire.TurnMotion(isBurn);
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        isBurn = !isBurn;
        fire.TurnMotion(isBurn);
        return false;
    }

    private void Update()
    {
        if(isBurn)
        {
            currentSmokeTime += Time.deltaTime;

            if(currentSmokeTime >= smokeCooltime)
            {
                // 연기 생성
                CreateSmkoe();
                currentSmokeTime = 0.0f;
            }
        }
    }

    private void CreateSmkoe()
    {
        if(smokeCreatePose)
        {
            if(smoke)
            {
                GameObject createSmoke = PoolingManager.Instance.GetPool("BeaconSmoke");
                if (!createSmoke)
                {
                    createSmoke = Instantiate(smoke);
                }

                createSmoke.transform.SetParent(smokeCreatePose);
                createSmoke.transform.localPosition = Vector2.zero;
                createSmoke.GetComponent<Stage4ObjectBeaconSmoke>().PlayMotion();
            }
        }
    }
}
