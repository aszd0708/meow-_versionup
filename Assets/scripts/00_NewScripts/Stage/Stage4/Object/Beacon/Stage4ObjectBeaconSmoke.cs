﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage4ObjectBeaconSmoke : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer smokeRenderer;
    [Header("움직이는 거리")]
    [SerializeField]
    private float flyingDistance = 1.5f;

    [SerializeField]
    private float flyingTime;

    [SerializeField]
    private float startSize = 0.5f;
    [SerializeField]
    private float endSize = 1.5f;

    public void PlayMotion()
    {
        transform.localScale = Vector2.one * startSize;
        smokeRenderer.color = Color.white;
        transform.DOLocalMoveY(flyingDistance, flyingTime).SetEase(Ease.Linear);
        transform.DOScale(Vector2.one * endSize, flyingTime).SetEase(Ease.Linear);
        smokeRenderer.DOFade(0.0f, flyingTime).SetEase(Ease.Linear);
        PoolingManager.Instance.SetPool(gameObject, "BeaconSmoke", flyingTime);
    }
}
