﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage4ObjectBeaconFire : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer fireRenderer;

    [SerializeField]
    private Sprite[] fireSprites;

    [SerializeField]
    private float fireChangeTime;

    private Coroutine motionCor;

    public void TurnMotion(bool isOn)
    {
        if(isOn)
        {
            motionCor = StartCoroutine(_Motion());
        }
        else
        {
            if(motionCor != null)
            {
                StopCoroutine(motionCor);
            }
            motionCor = null;
        }
    }

    private IEnumerator _Motion()
    {
        int index = 0;
        WaitForSeconds wait = new WaitForSeconds(fireChangeTime);
        while(true)
        {
            fireRenderer.sprite = fireSprites[index];
            index++;
            index %= fireSprites.Length;
            yield return wait;
        }
    }
}
