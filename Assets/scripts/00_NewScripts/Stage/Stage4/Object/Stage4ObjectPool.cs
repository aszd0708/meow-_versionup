﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage4ObjectPool : MonoBehaviour, TouchableObj
{
    [SerializeField]
    private Transform leftTreeTransforms;
    [SerializeField]
    private Transform rightTreeTransforms;

    [SerializeField]
    private GameObject itemTrigger;

    private bool bIsOpen = false;

    private bool bCanTouch = true;

    public bool DoTouch(Vector2 touchPose = default)
    {
        if (bCanTouch == false) { return false; }

        Open();

        return false;
    }

    private void Open()
    {
        if (bIsOpen == true)
        {
            StartCoroutine(_TreeAnimation(180));
            itemTrigger.SetActive(false);
        }
        else
        {
            StartCoroutine(_TreeAnimation(0));
            itemTrigger.SetActive(true);
        }

        bIsOpen = !bIsOpen;
    }

    private IEnumerator _TreeAnimation(float yValue)
    {
        bCanTouch = false;

        rightTreeTransforms.DORotate(Vector3.up * yValue, 1.5f);
        leftTreeTransforms.DORotate(Vector3.up * (yValue - 180), 1.5f);
        yield return new WaitForSeconds(1.5f);
        bCanTouch = true;

        yield break;
    }
}

