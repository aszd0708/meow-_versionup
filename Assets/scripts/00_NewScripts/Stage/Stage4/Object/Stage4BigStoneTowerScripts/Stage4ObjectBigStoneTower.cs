﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Stage4ObjectBigStoneTower : MonoBehaviour, TouchableObj
{
    [SerializeField]
    private Stage4ObjectCatGhostsController catGhost;
    [SerializeField]
    private Stage4ObjectEggGhost eggGhost;

    private enum Ghost
    {
        EGG, CAT
    }
    private Ghost currentGhost;

    #region Sprite Variables
    [SerializeField]
    private Sprite[] sprites;

    [SerializeField]
    private GameObject render;
    #endregion

    private bool canTouch = true;

    [SerializeField]
    private bool isEventObj = false;


    [SerializeField]
    private Transform jumpPose;

    [SerializeField]
    private UnityEvent specialTouchEvent;

    public bool DoTouch(Vector2 touchPose = default)
    {
        if(isEventObj == true)
        {
            specialTouchEvent.Invoke();
            isEventObj = false;
            return false;
        }

        if(canTouch)
        {
            RandomEvent();
        }
        else
        {
            switch(currentGhost)
            {
                case Ghost.CAT:
                    catGhost.EndEvent();
                    break;
                case Ghost.EGG:
                    eggGhost.EndEvent();
                    break;
            }
            canTouch = true;
        }
        return false;
    }

    private void RandomEvent()
    {
        int randomValue = Random.Range(0, 100);

        if(randomValue < 50)
        {
            CreateRandomSprite();
        }
        else if(randomValue < 75)
        {
            catGhost.StartEvent();
            currentGhost = Ghost.CAT;
            canTouch = false;
        }
        else if(randomValue < 100)
        {
            eggGhost.StartEvent();
            currentGhost = Ghost.EGG;
            canTouch = false;
        }
    }


    private void CreateRandomSprite()
    {
        GameObject createObj = PoolingManager.Instance.GetPool("BigStoneTowerTrash");
        if (createObj == null)
        {
            createObj = Instantiate(render);
        }

        createObj.transform.position = render.transform.position;
        createObj.GetComponent<SpriteRenderer>().sprite = sprites[Random.Range(0, sprites.Length)];
        createObj.GetComponent<StageJumpToPoseEvent>().JumpPose = jumpPose;
        createObj.GetComponent<StageJumpToPoseEvent>().JumpEvent();
        PoolingManager.Instance.SetPool(createObj, "BigStoneTowerTrash", 5.0f);
    }
}
