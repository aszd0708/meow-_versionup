﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage4ObjectEggGhost : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer[] ghostRender;

    [Header("처음에 나타나고 없어지는 시간")]
    [SerializeField]
    private float fadeTime;
    [SerializeField]
    private float motionChangeTime;
    [SerializeField]
    private float idleTime;

    private void Start()
    {
        for(int i = 0; i < ghostRender.Length; i++)
        {
            ghostRender[i].color = new Color(1, 1, 1, 0);
        }
    }

    public void StartEvent()
    {
        StartCoroutine(_AppearGhost());
    }

    private IEnumerator _IdleMotion()
    {
        int index = 0;
        WaitForSeconds wait = new WaitForSeconds(motionChangeTime + idleTime);
        while(true)
        {
            ghostRender[index].DOFade(0, motionChangeTime);
            index++;
            index %= ghostRender.Length;
            ghostRender[index].DOFade(1, motionChangeTime);
            yield return wait;
        }
    }

    private IEnumerator _AppearGhost()
    {
        SpriteRenderer render = ghostRender[0];
        render.DOKill();
        render.color = new Color(1, 1, 1, 0);
        render.DOFade(1, fadeTime);
        yield return new WaitForSeconds(fadeTime);
        StartCoroutine(_IdleMotion());
        yield break;
    }

    public void EndEvent()
    {
        StopAllCoroutines();
        for(int i = 0; i < ghostRender.Length; i++)
        {
            ghostRender[i].DOKill();
            ghostRender[i].DOFade(0, 1.0f);
        }
    }
}
