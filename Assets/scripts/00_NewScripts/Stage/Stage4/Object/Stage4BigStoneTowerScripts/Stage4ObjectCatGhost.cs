﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage4ObjectCatGhost : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer spriteRenderer;
    [SerializeField]
    private Sprite[] ghostSprites;

    [SerializeField]
    private float[] ghostSize;

    [SerializeField]
    private float[] ghostRotation;

    [SerializeField]
    private float[] changeTime;

    [SerializeField]
    private Vector3 endScale;
    [SerializeField]
    private Quaternion endRotation;
    private void Start()
    {
        transform.localScale = Vector3.zero;
    }

    public void EventMotion()
    {
        StartCoroutine(_EventMotion());
    }

    private IEnumerator _EventMotion()
    {
        transform.localScale = Vector3.zero;
        spriteRenderer.DOKill();
        spriteRenderer.color = Color.white;
        for (int i = 0; i < ghostSprites.Length; i++)
        {
            transform.DOScale(ghostSize[i], changeTime[i]).SetEase(Ease.Linear);
            transform.DORotate(Vector3.forward * ghostRotation[i], changeTime[i]).SetEase(Ease.Linear);
            yield return new WaitForSeconds(changeTime[i]);
            spriteRenderer.sprite = ghostSprites[i];
            transform.localScale = Vector3.one;
            transform.rotation = Quaternion.identity;
        }
        transform.localScale = endScale;
        transform.rotation = endRotation;
        yield break;
    }

    public void EndEvent()
    {
        StopAllCoroutines();
        transform.DOKill();
        spriteRenderer.DOKill();
        spriteRenderer.DOFade(0, 2.0f);
    }
}
