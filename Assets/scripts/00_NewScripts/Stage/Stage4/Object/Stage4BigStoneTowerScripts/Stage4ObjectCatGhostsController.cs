﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage4ObjectCatGhostsController : MonoBehaviour
{
    [SerializeField]
    private Stage4ObjectCatGhost[] ghosts;

    public void StartEvent()
    {
        for(int i = 0; i < ghosts.Length; i++)
        {
            ghosts[i].EventMotion();
        }
    }

    public void EndEvent()
    {
        for(int i = 0; i < ghosts.Length; i++)
        {
            ghosts[i].EndEvent();
        }
    }
}
