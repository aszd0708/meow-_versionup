﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage4ObjectJumpingFish : MonoBehaviour
{
    [SerializeField]
    private float[] jumpingCoolTime;

    [SerializeField]
    private Transform fishTransform;

    #region Jump

    [SerializeField]
    private Transform fishStartTransform;
    [SerializeField]
    private Transform fishEndTransform;
    [SerializeField]
    private float jumpPower;
    [SerializeField]
    private float jumpDuration;

    #endregion

    [SerializeField]
    private Transform[] createTransforms;

    [SerializeField]
    private SpriteRenderer fishSprite;

    [SerializeField]
    private SpriteMask spriteMask;

    WaitForSeconds wait;

    private void Start()
    {
        wait = new WaitForSeconds(jumpDuration);

        StartCoroutine(_SetPosition());
    }

    private IEnumerator _SetPosition()
    {
        while(true)
        {
            float randomTime = Random.Range(jumpingCoolTime[0], jumpingCoolTime[1]);
            Transform randomTransform = createTransforms[Random.Range(0, createTransforms.Length)];

            transform.position = randomTransform.position;
            Turn(true);

            yield return StartCoroutine(_Motion());

            fishTransform.position = fishStartTransform.position;
            fishTransform.rotation = Quaternion.identity;
            Turn(false);

            yield return new WaitForSeconds(randomTime);
        }
    }

    private IEnumerator _Motion()
    {
        fishTransform.position = fishStartTransform.position;
        fishTransform.DOJump(fishEndTransform.position, jumpPower, 1, jumpDuration).SetEase(Ease.Linear);
        fishTransform.DORotate(Vector3.forward * 90, jumpDuration).SetEase(Ease.InQuint);
        yield return wait;
        yield break;
    }

    private void Turn(bool isOn)
    {
        fishSprite.enabled = isOn;
        spriteMask.enabled = isOn;
    }
}
