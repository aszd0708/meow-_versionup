﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage4ObjectFountainRainbowController : MonoBehaviour
{
    [SerializeField]
    private int onCount;

    private int currentCount;
    public int CurrentCount { get => currentCount; set => currentCount = value; }

    [SerializeField]
    private SpriteRenderer[] rainbowRenderers;

    [SerializeField]
    private float fadeSpeed;

    private bool turnOn = false;

    private void Start()
    {
        for (int i = 0; i < rainbowRenderers.Length; i++)
        {
            rainbowRenderers[i].color = new Color(1,1,1,0);
        }
    }

    private void Update()
    {
        float temp = fadeSpeed * Time.deltaTime;
        turnOn = CurrentCount >= onCount;
        if (!turnOn)
        {
            temp *= -1;
        }

        for (int i = 0; i < rainbowRenderers.Length; i++)
        {
            Color tempColor = rainbowRenderers[i].color;
            tempColor = new Color(tempColor.r, tempColor.g, tempColor.b, Mathf.Clamp(tempColor.a += temp, 0, 1));
            rainbowRenderers[i].color = tempColor;
        }
    }
}
