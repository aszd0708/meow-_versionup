﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage4ObjectFountainPipeController : MonoBehaviour, TouchableObj
{
    [SerializeField]
    private SpriteRenderer waterRenderer;
    [SerializeField]
    private StageObjectSpriteChangeByTime waterMotionRenderer;
    [SerializeField]
    private Stage4ObjectFountainRainbowController rainbowConteroller;

    private bool currentOn = false;

    private void Start()
    {
        currentOn = false;
        Motion();
        rainbowConteroller.CurrentCount = 0;
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        currentOn = !currentOn;
        Motion();
        return false;
    }

    private void Motion()
    {
        if(currentOn)
        {
            waterMotionRenderer.SpriteChange();
            rainbowConteroller.CurrentCount++;
        }
        else
        {
            waterMotionRenderer.StopMotion();
            rainbowConteroller.CurrentCount--;
        }
        waterRenderer.enabled = currentOn;
    }
}
