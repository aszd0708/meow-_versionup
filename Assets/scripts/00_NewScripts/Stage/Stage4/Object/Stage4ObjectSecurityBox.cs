﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage4ObjectSecurityBox : MonoBehaviour
{
    [SerializeField]
    private Transform insidePerson;

    [SerializeField]
    private Transform outsidePerson;

    [SerializeField]
    private float[] times;

    private void Start()
    {
        PlayPersonMotion();
    }

    void PlayPersonMotion()
    {
        if(insidePerson)
        {
            StartCoroutine(_PersonMotion(insidePerson, false));
        }

        if(outsidePerson)
        {
            StartCoroutine(_PersonMotion(outsidePerson, true));
        }
    }

    private IEnumerator _PersonMotion(Transform movePerson, bool isFlip)
    {
        while(true)
        {
            float randomTime = Random.Range(times[0],times[1]);

            if(isFlip)
            {
                movePerson.DORotate(Vector2.up * 180, 0.5f);
            }

            else
            {
                movePerson.DORotate(Vector2.zero, 0.5f);
            }

            isFlip = !isFlip;

            yield return new WaitForSeconds(randomTime);
        }
    }
}
