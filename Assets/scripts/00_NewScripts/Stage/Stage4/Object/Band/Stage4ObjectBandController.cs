﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Stage4ObjectBandController : MonoBehaviour
{
    [SerializeField]
    private int guitarMusicIndex;
    [SerializeField]
    private int ukuleleMusicIndex;
    [SerializeField]
    private int cajonMusicIndex;

    private int currentGuitarMusicIndex;
    private int currentUkuleleMusicIndex;
    private int currentCajonMusicIndex;

    [SerializeField]
    private UnityEvent completeEvent;
    [SerializeField]
    private UnityEvent notCompleteEvent;

    public void SetGuitarMusicIndex(StageObjectBandBase guitar)
    {
        currentGuitarMusicIndex = guitar.CurrentMusicIndex;
        CheckIndex();
    }

    public void SetUkuleleMusicIndex(StageObjectBandBase ukulele)
    {
        currentUkuleleMusicIndex = ukulele.CurrentMusicIndex;
        CheckIndex();
    }

    public void SetCajonMusicIndex(StageObjectBandBase cajon)
    {
        currentCajonMusicIndex = cajon.CurrentMusicIndex;
        CheckIndex();
    }

    private void CheckIndex()
    {
        if(guitarMusicIndex != currentGuitarMusicIndex)
        {
            notCompleteEvent.Invoke();
            return;
        }

        if(ukuleleMusicIndex != currentUkuleleMusicIndex)
        {
            notCompleteEvent.Invoke();
            return;
        }

        if(cajonMusicIndex != currentCajonMusicIndex)
        {
            notCompleteEvent.Invoke();
            return;
        }

        completeEvent.Invoke();
    }
}
