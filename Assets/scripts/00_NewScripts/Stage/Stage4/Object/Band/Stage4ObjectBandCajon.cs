﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage4ObjectBandCajon : StageObjectBandBase
{
    [SerializeField]
    private Transform leftArmTransform;
    [SerializeField]
    private float leftArmAngle;
    [SerializeField]
    private float[] leftArmMotionTime;

    [SerializeField]
    private Transform rightArmTransform;
    [SerializeField]
    private float rightArmAngle;
    [SerializeField]
    private float[] rightArmMotionTime;

    [SerializeField]
    private Ease[] eases;
    protected override IEnumerator _Motion()
    {
        WaitForSeconds[] leftWait = { new WaitForSeconds(leftArmMotionTime[0]), new WaitForSeconds(leftArmMotionTime[1]) };
        WaitForSeconds[] rightWait = { new WaitForSeconds(rightArmMotionTime[0]), new WaitForSeconds(rightArmMotionTime[1]) };

        bool isRight = false;
        while(true)
        {
            if(isRight == true)
            {
                rightArmTransform.DORotate(Vector3.forward * rightArmAngle, rightArmMotionTime[0]).SetEase(eases[0]);
                yield return rightWait[0];

                rightArmTransform.DORotate(Vector3.zero, rightArmMotionTime[1]).SetEase(eases[1]);
                yield return rightWait[1];
            }
            else
            {
                leftArmTransform.DORotate(Vector3.forward * leftArmAngle, leftArmMotionTime[0]).SetEase(eases[0]);
                yield return leftWait[0];

                leftArmTransform.DORotate(Vector3.zero, leftArmMotionTime[1]).SetEase(eases[1]);
                yield return leftWait[1];
            }
            isRight = !isRight;
        }
    }

    protected override void StopMotion()
    {
        base.StopMotion();
        rightArmTransform.DOKill();
        leftArmTransform.DOKill();
        rightArmTransform.rotation = Quaternion.identity;
        leftArmTransform.rotation = Quaternion.identity;
    }
}
