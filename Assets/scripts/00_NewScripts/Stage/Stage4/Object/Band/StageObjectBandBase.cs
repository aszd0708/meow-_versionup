﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class StageObjectBandBase : MonoBehaviour, TouchableObj
{
    [SerializeField]
    protected UnityEvent touchEvent;

    private int currentMusicIndex = 0;
    public int CurrentMusicIndex { get => currentMusicIndex; set => currentMusicIndex = value; }

    private bool isPlayingMusic = false;
    public bool IsPlayingMusic { get => isPlayingMusic; set => isPlayingMusic = value; }

    [SerializeField]
    private AudioClip[] musicClips;

    [SerializeField]
    private AudioSource audioSource;

    protected Coroutine motionCor;

    public virtual bool DoTouch(Vector2 touchPose = default)
    {
        PlayMusic();
        touchEvent.Invoke();
        return false;
    }

    protected virtual void PlayMotion()
    {
        if (motionCor == null)
        {
            motionCor = StartCoroutine(_Motion());
        }
    }

    protected virtual void StopMotion()
    {
        if (motionCor != null)
        {
            StopCoroutine(motionCor);
            motionCor = null;
        }
    }

    protected abstract IEnumerator _Motion();

    protected void PlayMusic()
    {
        CurrentMusicIndex++;
        CurrentMusicIndex %= musicClips.Length + 1;

        int musicIndex = CurrentMusicIndex - 1;

        if(musicIndex < 0)
        {
            StopMotion();

            audioSource.Stop();
        }

        else
        {
            PlayMotion();

            float volume = PlayerPrefs.GetFloat("EffectSound", 1.0f);
            audioSource.clip = musicClips[musicIndex];
            audioSource.volume = volume;
            audioSource.Play();
        }
    }
}
