﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage4ObjectBandGuitar : StageObjectBandBase
{
    [SerializeField]
    private Transform armTransform;

    [SerializeField]
    private float[] angle;

    [SerializeField]
    private float motionTime;

    [SerializeField]
    private Ease[] eases;

    protected override IEnumerator _Motion()
    {
        WaitForSeconds wait = new WaitForSeconds(motionTime);

        int index = 0;
        while(true)
        {
            armTransform.DORotate(Vector3.forward * angle[index], motionTime).SetEase(eases[index]);
            index++;
            index %= angle.Length;
            yield return wait;
        }
    }

    protected override void StopMotion()
    {
        base.StopMotion();
        armTransform.DOKill();
        armTransform.rotation = Quaternion.identity;
    }
}
