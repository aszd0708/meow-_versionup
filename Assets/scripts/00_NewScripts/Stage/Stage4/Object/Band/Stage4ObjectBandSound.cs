﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage4ObjectBandSound : MonoBehaviour, TouchableObj
{
    [SerializeField]
    [Header("자기 자신 오디오")]
    private AudioSource audioSource;

    private bool isOn = true;

    private void Start()
    {
        SetVolume();
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        SetVolume();
        return false;
    }

    private void SetVolume()
    {
        if(isOn)
        {
            audioSource.Stop();
            
            audioSource.volume = 0f;
        }
        else
        {
            audioSource.Play();
            audioSource.volume = 1f;
        }
        isOn = !isOn;
    }
}
