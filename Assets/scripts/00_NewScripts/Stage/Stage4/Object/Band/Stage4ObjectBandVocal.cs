﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage4ObjectBandVocal : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer headRender;
    [SerializeField]
    private Sprite[] headSprites;

    [SerializeField]
    private float motionTime;

    private Coroutine motionCor;

    public void PlayMotion(bool playMotion)
    {
        if(playMotion)
        {
            if(motionCor == null)
            {
                motionCor = StartCoroutine(_Motion());
            }
        }

        else
        {
            if(motionCor != null)
            {
                StopCoroutine(motionCor);
            }
        }
    }

    private IEnumerator _Motion()
    {
        int index = 0;
        WaitForSeconds wait = new WaitForSeconds(motionTime);
        while(true)
        {
            headRender.sprite = headSprites[index++];
            index %= headSprites.Length;
            yield return wait;
        }
    }
}
