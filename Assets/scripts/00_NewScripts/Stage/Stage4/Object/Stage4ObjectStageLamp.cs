﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage4ObjectStageLamp : MonoBehaviour, TouchableObj
{
    [SerializeField]
    private SpriteRenderer lightRender;

    [SerializeField]
    private float fadeTime;

    private float lightPower;

    private void Start()
    {
        lightPower = (float)Random.Range((int)0, (int)2);
        DoTouch();
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        lightRender.DOKill();
        lightRender.DOFade(lightPower, fadeTime);
        lightPower += 1;
        lightPower %= 2;
        return false;
    }
}
