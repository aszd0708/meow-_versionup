﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage4ObjectPhonePersonMotion : MonoBehaviour, TouchableObj
{
    [SerializeField]
    private Transform armTransform;

    [Header("움직이는 각도")]
    [SerializeField]
    private float armAngle = 100f;

    [SerializeField]
    private float armMotionTime;

    private bool canTouch = true;

    [SerializeField]
    private GameObject phoneLight;

    private void Start()
    {
        phoneLight.SetActive(false);
        armTransform.rotation = Quaternion.Euler(Vector3.forward * armAngle);
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        if(canTouch)
        {
            StartCoroutine(_ArmMotion());
        }
        return false;
    }

    private IEnumerator _ArmMotion()
    {
        canTouch = false;
        float temp = armTransform.rotation.z;
        armTransform.DORotate(Vector3.forward * temp, armMotionTime);
        yield return new WaitForSeconds(armMotionTime);
        // 여기 대충 소리 나오는 곳 찰칽
        bool isLighting = IntRandom.StateRandomInt(100);

        if(isLighting)
        {
            phoneLight.SetActive(true);
            yield return new WaitForSeconds(0.1f);
            phoneLight.SetActive(false);
        }
        yield return new WaitForSeconds(1.0f);

        armTransform.DORotate(Vector3.forward * armAngle, armMotionTime);
        yield return new WaitForSeconds(armMotionTime);

        canTouch = true;
        yield break;
    }
}
