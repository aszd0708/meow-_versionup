﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage4ObjectPhonePersonSpriteController : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer headRenderer;

    [SerializeField]
    private Sprite[] headSprites;

    private void Start()
    {
        SetSprites();
    }

    private void SetSprites()
    {
        int randomValue = Random.Range(0, headSprites.Length);
        headRenderer.sprite = headSprites[randomValue];
    }
}
