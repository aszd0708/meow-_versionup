﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage4ObjectSitPeopleController : MonoBehaviour
{
    [SerializeField]
    private int percent;
    [SerializeField]
    private GameObject[] personObjects;

    [SerializeField]
    private Transform[] peopleCreatePose;
    // Start is called before the first frame update
    void Start()
    {
        SetPeople();
    }

    private void SetPeople()
    {
        for (int i = 0; i < peopleCreatePose.Length; i++)
        {
            bool isVisible = IntRandom.StateRandomInt(percent);
            if(isVisible)
            {
                GameObject person = Instantiate(personObjects[Random.Range(0, personObjects.Length)]);
                person.transform.position = peopleCreatePose[i].position;
            }
        }
    }
}
