﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage4ObjectWalkingPersonLayerController : MonoBehaviour
{
    [SerializeField]
    private GameObject[] walkingPerson;

    [SerializeField]
    private int createCount;

    private void Awake()
    {
        CreateWalkingPerson();
    }

    private void CreateWalkingPerson()
    {
        for(int i = 0; i < createCount; i++)
        {
            GameObject g = Instantiate(walkingPerson[Random.Range(0, walkingPerson.Length)]);
            if(g.GetComponent<Stage4ObjectWalklingPersonSpriteController>() != null)
            {
                g.GetComponent<Stage4ObjectWalklingPersonSpriteController>().SetLayerNumber(i);
            }

            //if(g.GetComponent<Stage4ObjectWalkingPersonPathFind>() != null)
            //{
                //g.GetComponent<Stage4ObjectWalkingPersonPathFind>().ObjectStart();
            //}
            g.SetActive(true);
        }
    }
}
 