﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage4ObjectWalkingPersonMotion : MonoBehaviour
{
    [SerializeField]
    private Transform armTransform;
    [SerializeField]
    private float[] armMoveAngle;

    [SerializeField]
    private Transform headTransform;
    [SerializeField]
    private float[] headMoveAngle;
    [SerializeField]
    private float headMotionTime;

    [SerializeField]
    private Transform leftLegTransform;
    [SerializeField]
    private float[] leftLegMoveAnlge;
    [SerializeField]
    private Transform rightLegTransform;
    [SerializeField]
    private float[] rightLegMoveAngle;

    [SerializeField]
    private float motionTime;

    private Coroutine walingCor;
    private Coroutine headCor;

    private Quaternion idleHeadRotation;
    private Quaternion idleArmRotation;
    private Quaternion[] idleLegsRotation = new Quaternion[2];

    private void Start()
    {
        SetIdleValues();
    }

    private void SetIdleValues()
    {
        idleHeadRotation = headTransform.localRotation;
        idleArmRotation = armTransform.localRotation;
        idleLegsRotation[0] = leftLegTransform.localRotation;
        idleLegsRotation[1] = leftLegTransform.localRotation;
    }

    public virtual void StartMotion()
    {
        walingCor = StartCoroutine(_WalkingMotion());
        headCor = StartCoroutine(_HeadMotion());
    }

    private IEnumerator _WalkingMotion()
    {
        int motionIndex = 0;
        WaitForSeconds wait = new WaitForSeconds(motionTime);
        while (true)
        {
            armTransform.DOLocalRotate(Vector3.forward * armMoveAngle[motionIndex], motionTime).SetEase(Ease.Linear);
            leftLegTransform.DOLocalRotate(Vector3.forward * leftLegMoveAnlge[motionIndex], motionTime).SetEase(Ease.Linear);
            rightLegTransform.DOLocalRotate(Vector3.forward * rightLegMoveAngle[motionIndex], motionTime).SetEase(Ease.Linear);

            motionIndex++;
            motionIndex %= rightLegMoveAngle.Length;
            yield return wait;
        }
    }

    private IEnumerator _HeadMotion()
    {
        int motionIndex = 0;
        WaitForSeconds wait = new WaitForSeconds(headMotionTime);
        while (true)
        {
            headTransform.DOLocalRotate(Vector3.forward * headMoveAngle[motionIndex], headMotionTime).SetEase(Ease.Linear);
            motionIndex++;
            motionIndex %= headMoveAngle.Length;
            yield return wait;
        }
    }

    public void SetIdleMotion()
    {
        StopCoroutine(headCor);
        StopCoroutine(walingCor);

        headTransform.localRotation = idleHeadRotation;
        armTransform.localRotation = idleArmRotation;
        leftLegTransform.localRotation = idleLegsRotation[0];
        leftLegTransform.localRotation = idleLegsRotation[1];
    }
}
