﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage4ObjectWalkingPersonPathFind : MonoBehaviour
{
    [SerializeField]
    private MovableNode[] movableNodes;
    private int moveCount;
    private List<Transform> moveTransform = new List<Transform>();

    private int nextStartIndex = 0;

    [SerializeField]
    private float restTime;

    [SerializeField]
    private float[] moveSpeedSize = new float[2];

    [Header("거리에 따라 시간 곱")]
    private float moveTime;

    [SerializeField]
    private Stage4ObjectWalkingPersonMotion motion;

    private void FindPath(int startIndex, int moveCount)
    {
        bool[] isVisited = new bool[movableNodes.Length];
        MovableNode currentMoveNode = movableNodes[startIndex];
        isVisited[startIndex] = true;
        for (int i = 0; i < moveCount; i++)
        {
            isVisited[currentMoveNode.index] = true;
            if (currentMoveNode.nodes.Length == 1)
            {
                currentMoveNode = currentMoveNode.nodes[0];
                moveTransform.Add(currentMoveNode.transform);
            }

            else
            {
                int visitedCount = 0;
                for (int j = 0; j <currentMoveNode.nodes.Length; j++)
                {
                    if(!isVisited[currentMoveNode.nodes[j].index] && IntRandom.StateRandomInt(100 / currentMoveNode.nodes.Length))
                    {
                        currentMoveNode = currentMoveNode.nodes[j];
                        moveTransform.Add(currentMoveNode.transform);
                        break;
                    }
                    else/* if(isVisited[currentMoveNode.nodes[i].index])*/
                    {
                        visitedCount++;
                    }
                }
                if(visitedCount == currentMoveNode.nodes.Length)
                {
                    int randomValue = Random.Range(0, currentMoveNode.nodes.Length);

                    currentMoveNode = currentMoveNode.nodes[randomValue];
                    moveTransform.Add(currentMoveNode.transform);
                }
            }
        }
        nextStartIndex = currentMoveNode.index;
    }
    private void OnEnable()
    {
        nextStartIndex = Random.Range(0, movableNodes.Length);
        StartCoroutine(_Move());
    }

    private IEnumerator _Move()
    {
        WaitForSeconds wait = new WaitForSeconds(Random.Range(restTime, restTime * 1.5f));

        while(true)
        {
            moveTransform.Clear();
            moveCount = Random.Range(0, movableNodes.Length);

            SetSpeed();
            FindPath(nextStartIndex, moveCount);
            transform.position = moveTransform[0].transform.position;
            motion.StartMotion();
            for (int i = 1; i < moveTransform.Count; i++)
            {
                float distance = Vector3.Distance(transform.position, moveTransform[i].position);

                transform.DOMove(moveTransform[i].position, distance * moveTime).SetEase(Ease.Linear);

                if (moveTransform[i].position.x < transform.position.x)
                {
                    transform.rotation = Quaternion.Euler(Vector3.up * 180);
                }

                else
                {
                    transform.rotation = Quaternion.Euler(Vector3.zero);
                }

                yield return new WaitForSeconds(distance * moveTime);
            }
            motion.SetIdleMotion();
            yield return wait;
        }
    }

    private void SetSpeed()
    {
        moveTime = Random.Range(moveSpeedSize[0], moveSpeedSize[1]);
    }
}
