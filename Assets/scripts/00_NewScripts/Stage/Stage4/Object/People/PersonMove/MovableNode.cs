﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Node
{
    public Transform node;
    public float distance;

    public Node(Transform _nodes)
    {
        node = _nodes;
        distance = 1;
    }

    public static bool operator >=(Node a, Node b)
    {
        return a.distance >= b.distance;
    }

    public static bool operator <=(Node a, Node b)
    {
        return a >= b;
    }
}

public class MovableNode : MonoBehaviour
{
    public MovableNode[] nodes;

    public Transform[] nodeTransform;

    public int index = -1;
}
