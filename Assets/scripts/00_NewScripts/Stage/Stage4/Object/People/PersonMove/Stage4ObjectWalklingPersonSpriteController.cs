﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage4ObjectWalklingPersonSpriteController : MonoBehaviour
{
    [System.Serializable]
    public struct SpriteLayer
    {
        public float[] yRange;
        public string layerName;
    }

    [SerializeField]
    private SpriteLayer[] layers;

    private SpriteLayer currentLayers;

    [SerializeField]
    private SpriteRenderer headRenderer;
    [SerializeField]
    private Sprite[] headSprites;

    private List<SpriteRenderer> renderers = new List<SpriteRenderer>();

    private void OnEnable()
    {
        ChangeHead();
    }

    private void Start()
    {
        StartCoroutine(CheckLayer());
    }

    private IEnumerator CheckLayer()
    {
        WaitForSeconds wait = new WaitForSeconds(1.0f);
        while(true)
        {
            for(int i = 0; i < layers.Length; i++)
            {
                if(layers[i].yRange[0] <= transform.position.y && layers[i].yRange[1] >= transform.position.y)
                {
                    if(layers[i].layerName != currentLayers.layerName)
                    {
                        SetLayer(layers[i].layerName);
                        currentLayers = layers[i];
                    }
                }
            }
            yield return wait;
        }
    }

    private void ChangeHead()
    {
        headRenderer.sprite = headSprites[Random.Range(0, headSprites.Length)];
    }

    private void SetRenderers(Transform t)
    {
        if(t.GetComponent<SpriteRenderer>() != null)
        {
            renderers.Add(t.GetComponent<SpriteRenderer>());
        }

        for(int i = 0; i < t.childCount; i++)
        {
            SetRenderers(t.GetChild(i));
        }
    }

    private void SetLayer(string layerName)
    {
        for(int i = 0; i < renderers.Count; i++)
        {
            renderers[i].sortingLayerName = layerName;
        }
    }

    public void SetLayerNumber(int num)
    {
        SetRenderers(transform);
        for (int i = 0; i < renderers.Count; i++)
        {
            renderers[i].sortingOrder += num;
        }
    }
}
