﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableNodeIndexSetter : MonoBehaviour
{
    [SerializeField]
    private MovableNode[] nodes;
    private void Awake()
    {
        for (int i = 0; i < nodes.Length; i++)
        {
            nodes[i].index = i;
        }
    }
}
