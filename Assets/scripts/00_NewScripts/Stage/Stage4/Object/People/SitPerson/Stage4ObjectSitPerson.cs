﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage4ObjectSitPerson : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer headRenderer;

    [Header("등산복 2명 제외하고 머리는 여러개")]
    [SerializeField]
    private Sprite[] headSprites;

    [SerializeField]
    private SpriteRenderer bodyRenderer;
    [SerializeField]
    private Sprite bodySprite;

    [SerializeField]
    private SpriteRenderer armRenderer;
    [SerializeField]
    private Sprite armSprite;

    private void Start()
    {
        SetSprite();
    }

    private void SetSprite()
    {
        headRenderer.sprite = headSprites[Random.Range(0, headSprites.Length)];

        bodyRenderer.sprite = bodySprite;
        armRenderer.sprite = armSprite;
    }
}
