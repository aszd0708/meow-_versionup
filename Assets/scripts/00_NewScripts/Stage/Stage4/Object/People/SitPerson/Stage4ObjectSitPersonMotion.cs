﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage4ObjectSitPersonMotion : MonoBehaviour, TouchableObj
{
    public enum Kind
    {
        Drink, Eat
    }

    [SerializeField]
    private float[] headAngles;
    [SerializeField]
    private Transform headTransform;

    private float headOriginAngle;

    [SerializeField]
    private float[] handAngles;
    [SerializeField]
    private Transform handTransform;

    private float handOriginAngle;

    [SerializeField]
    private Kind kind;

    private bool playingMotion = false;

    [SerializeField]
    private float[] motionCoolTime;

    private void OnEnable()
    {
        headOriginAngle = headTransform.rotation.eulerAngles.z;
        handOriginAngle = handTransform.rotation.eulerAngles.z;
    }

    private void Start()
    {
        StartCoroutine(_Motion());
    }
    public bool DoTouch(Vector2 touchPose = default)
    {
        PlayMotion();
        return false;
    }

    private IEnumerator _Motion()
    {
        while(true)
        {
            float time = Random.Range(motionCoolTime[0], motionCoolTime[1]);
            PlayMotion();
            yield return new WaitForSeconds(time);
        }
    }

    private void PlayMotion()
    {
        if(playingMotion)
        {
            return;
        }

        switch(kind)
        {
            case Kind.Drink:
                StartCoroutine(_DrinkingMotion());
                break;
            case Kind.Eat:
                StartCoroutine(_EatMotion());
                break;
        }
    }

    private IEnumerator _EatMotion()
    {
        playingMotion = true;

        for(int j = 0; j < 3; j++)
        {
            for (int i = 0; i < headAngles.Length; i++)
            {
                handTransform.DORotate(Vector3.forward * handAngles[i], 0.2f).SetEase(Ease.Linear);
                headTransform.DORotate(Vector3.forward * headAngles[i], 0.2f).SetEase(Ease.Linear);
                yield return new WaitForSeconds(0.2f);
            }
        }

        handTransform.DORotate(Vector3.forward * handOriginAngle, 0.2f).SetEase(Ease.Linear);
        headTransform.DORotate(Vector3.forward * headOriginAngle, 0.2f).SetEase(Ease.Linear);
        yield return new WaitForSeconds(0.2f);
        playingMotion = false;
        yield break;
    }

    private IEnumerator _DrinkingMotion()
    {
        playingMotion = true;
        for (int i = 0; i < handAngles.Length; i++)
        {
            handTransform.DORotate(Vector3.forward * handAngles[i], 0.6f).SetEase(Ease.Linear);
            yield return new WaitForSeconds(0.6f);
        }
        handTransform.DORotate(Vector3.forward * handOriginAngle, 0.6f).SetEase(Ease.Linear);
        yield return new WaitForSeconds(0.6f);
        playingMotion = false;
        yield break;
    }
}
