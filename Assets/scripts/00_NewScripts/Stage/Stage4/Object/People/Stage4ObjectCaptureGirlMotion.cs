﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage4ObjectCaptureGirlMotion : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer flashRender;
    [SerializeField]
    private float flashingTime;
    [SerializeField]
    private float cameraWaitingTime;

    [SerializeField]
    private int[] randomCounts = new int[2];

    private float totalTime;

    private Coroutine flashCor;

    private WaitForSeconds startWait;
    private WaitForSeconds flashTimeWait;

    public float TotalTime { get => totalTime; }

    private void Start()
    {
        flashTimeWait = new WaitForSeconds(flashingTime);
        startWait = new WaitForSeconds(cameraWaitingTime);
    }

    private void OnEnable()
    {
        flashCor = StartCoroutine(_Flash());
    }

    private void OnDisable()
    {
        flashRender.enabled = false;
        StopCoroutine(flashCor);
    }

    private IEnumerator _Flash()
    {
        flashRender.enabled = false;
        //yield return startWait;
        int randomValue = Random.Range(randomCounts[0], randomCounts[1]);
        totalTime = randomValue * flashingTime;
        for (int i = 0; i < randomValue; i++)
        {
            yield return flashTimeWait;
            flashRender.enabled = true;
            yield return flashTimeWait;
            flashRender.enabled = false;
        }
        yield break;
    }
}
