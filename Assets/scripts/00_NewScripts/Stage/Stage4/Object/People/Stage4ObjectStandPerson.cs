﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage4ObjectStandPerson : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer headRender;

    [SerializeField]
    private Sprite[] headSprites;

    [SerializeField]
    private SpriteRenderer armRender;
    [SerializeField]
    private Sprite[] armSprites;

    [SerializeField]
    private SpriteRenderer bodyRender;
    [SerializeField]
    private Sprite[] bodySprites;

    [SerializeField]
    private SpriteRenderer leftLegRender;
    [SerializeField]
    private Sprite[] leftLegSprite;
    [SerializeField]
    private SpriteRenderer rightLegRender;
    [SerializeField]
    private Sprite[] rightLegSprite;

    private void Start()
    {
        RandomRender(ref headRender, ref headSprites);
        RandomRender(ref armRender, ref armSprites);
        RandomRender(ref bodyRender, ref bodySprites);
        int randomIndex = Random.Range(0, leftLegSprite.Length);
        RandomRender(ref headRender, ref headSprites, randomIndex);
        RandomRender(ref headRender, ref headSprites, randomIndex);
    }

    private void RandomRender(ref SpriteRenderer render, ref Sprite[] sprites, int index = -1)
    {
        if(index == -1)
        {
            render.sprite = sprites[Random.Range(0, sprites.Length)];
        }

        else
        {
            render.sprite = sprites[index];
        }
    }
}
