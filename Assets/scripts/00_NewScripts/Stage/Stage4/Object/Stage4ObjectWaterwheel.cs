﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage4ObjectWaterwheel : MonoBehaviour, TouchableObj
{
    [SerializeField]
    private SpriteRenderer wheelRenderer;

    [SerializeField]
    private Sprite[] wheelSprites;

    [SerializeField]
    private float wheelMotionTime;

    [SerializeField]
    private SpriteRenderer waterRenderer;

    [SerializeField]
    private Sprite[] waterSprites;

    [SerializeField]
    private float waterMotionTime;

    [SerializeField]
    private Transform movingBoard;

    [SerializeField]
    private float boardMotionTime;

    private Coroutine wheelMotionCor;

    private Coroutine boardMotionCor;

    private Coroutine waterMotionCor;

    private bool isTurnOn;

    private void Start()
    {
        isTurnOn = true;
        Motion(isTurnOn);
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        Motion(isTurnOn);
        isTurnOn = !isTurnOn;
        return false;
    }

    private void Motion(bool turnOn)
    {
        if(turnOn)
        {
            wheelMotionCor = StartCoroutine(_wheelMotion());
            boardMotionCor = StartCoroutine(_BoardMotion());
            waterMotionCor = StartCoroutine(_WaterMotion());
        }

        else
        {
            StopMotion();
        }
    }

    private void StopMotion()
    {
        if(wheelMotionCor != null)
        {
            StopCoroutine(wheelMotionCor);
            wheelMotionCor = null;
        }

        if (boardMotionCor != null)
        {
            StopCoroutine(boardMotionCor);
            boardMotionCor = null;

            movingBoard.DOPause();
            movingBoard.rotation = Quaternion.Euler(Vector2.zero);
        }

        if (waterMotionCor != null)
        {
            StopCoroutine(waterMotionCor);
            waterMotionCor = null;
        }
    }

    private IEnumerator _wheelMotion()
    {
        int index = 0;
        WaitForSeconds wait = new WaitForSeconds(wheelMotionTime);
        while(true)
        {
            wheelRenderer.sprite = wheelSprites[index++];
            index %= wheelSprites.Length;
            yield return wait;
        }
    }

    private IEnumerator _BoardMotion()
    {
        while(true)
        {
            movingBoard.DORotate(Vector3.forward * -20, boardMotionTime).SetEase(Ease.OutCubic);
            yield return new WaitForSeconds(boardMotionTime);
            movingBoard.DORotate(Vector3.zero, 0.5f).SetEase(Ease.OutElastic);
            yield return new WaitForSeconds(0.5f);
        }
    }

    private IEnumerator _WaterMotion()
    {
        int index = 0;
        WaitForSeconds wait = new WaitForSeconds(waterMotionTime);
        while (true)
        {
            waterRenderer.sprite = waterSprites[index++];
            index %= waterSprites.Length;
            yield return wait;
        }
    }
}
