﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage4ItemNote : StageItemBase
{
    [SerializeField]
    private Transform startPose;

    [SerializeField]
    private Transform destinationPose;

    [SerializeField]
    private float speed;

    [SerializeField]
    private float degree;

    private float y;
    private float firstY;

    private bool bCanFly;

    void Start()
    {
        firstY = transform.position.y;
        y = transform.position.y;
    }

    private void Update()
    {
        if(bCanFly == false) { return; }

        Vector3 direction = destinationPose.position - transform.position;

        y -= Time.deltaTime;
        transform.position = new Vector3
            (
            transform.position.x + direction.y * Time.deltaTime * speed,
            transform.position.y + (Mathf.Sin(y) * degree) + direction.y * speed * Time.deltaTime, 
            transform.position.z
            );

        float distance = Vector2.Distance(transform.position, destinationPose.position);
        if(distance <= 2)
        {
            gameObject.SetActive(false);
        }
    }

    public void StartMotion()
    {
        if(bCanFly == true || gameObject.activeSelf == false || SaveDataManager.Instance.ItemDiction[ItemName] == true)
        { return; }
        transform.position = new Vector3(startPose.position.x, startPose.position.y, transform.position.z);
        bCanFly = true;

        col.enabled = true;
        spriteRenderer.enabled = true;
    }

    public override bool DoTouch(Vector2 touchPose = default)
    {
        bCanFly = false;
        return base.DoTouch(touchPose);
    }
}
