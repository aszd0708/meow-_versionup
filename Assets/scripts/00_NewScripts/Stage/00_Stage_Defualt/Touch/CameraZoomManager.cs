﻿using BitBenderGames;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoomManager : Singleton<CameraZoomManager>
{
    [SerializeField]
    private Transform cameraTransform;
    [SerializeField]
    private MobileTouchCamera MTC;

    protected override void Awake()
    {
        base.Awake();
        if(cameraTransform == null)
        {
            cameraTransform = Camera.main.transform;
        }

        if(MTC == null)
        {
            Debug.LogError("Not Found MTC");
        }
    }

    /// <summary>
    /// 줌 하는 함수
    /// </summary>
    /// <param name="currentValue">현재나 원하는 줌값</param>
    /// <param name="destValue">마지막으로 될 줌값</param>
    /// <param name="during">줌되는 시간</param>
    public void Zoom(float currentValue, float destValue, float during)
    {
        StartCoroutine(_Zoom(currentValue, destValue, during));
    }

    private IEnumerator _Zoom(float currentValue, float destValue, float time)
    {
        MTC.CamZoom = currentValue;
        float temp = 0.0f;
        float value = currentValue;
        while (Mathf.Abs(value - destValue) > 0.1f)
        {
            Debug.Log("Value : " + value);
            temp += Time.deltaTime / time;

            value = Mathf.Lerp(currentValue, destValue, temp);

            MTC.CamZoom = value;
            yield return null;
        }
        yield break;
    }
}
