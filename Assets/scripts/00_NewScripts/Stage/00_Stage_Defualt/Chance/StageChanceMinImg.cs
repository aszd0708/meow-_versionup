﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 찬스 마이너스 효과 주는 클래스
/// </summary>
public class StageChanceMinImg : MonoBehaviour
{
    [Header("찬스 -1 오브젝트")]
    public GameObject chanceMinusObj;

    [Header("오브젝트가 나오는 곳")]
    public RectTransform createRect;

    /// <summary>
    /// 이펙트 출력
    /// </summary>
    public void CreateMinusEffect()
    {
        GameObject effect = PoolingManager.Instance.GetPool("MinusEffect");

        if (effect == null)
            effect = Instantiate(chanceMinusObj);
        effect.transform.SetParent(createRect.transform);
        effect.GetComponent<StageMinusEffect>().StartEffect(createRect);
    }
}
