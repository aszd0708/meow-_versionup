﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 찬스 타이머
/// </summary>
public class StageChanceTimer : MonoBehaviour
{
    public SaveDataManager dataManager;
    public StageChanceManager chanceManager;

    [Header("얼마 뒤에 충전될지")]
    public int rewardTime;

    [Header("얼마나 남았는지 알려주는 텍스트")]
    public Text timeText;

    [Header("타이머 코루틴")]
    private Coroutine timerCor;

    /// <summary>
    /// 타이머 코루틴 시작
    /// </summary>
    public void StartTimer()
    {
        timerCor = StartCoroutine(_StartTimer());
    }

    private IEnumerator _StartTimer()
    {
        WaitForSeconds wait = new WaitForSeconds(1.0f);
        yield return wait;
        while (true)
        {
            CheckTime();
            yield return wait;
        }
    }

    /// <summary>
    /// 매초마다 시간 체크해줌
    /// </summary>
    private void CheckTime()
    {
        DateTime nowTime = DateTime.Now;
        DateTime saveTime = DateTime.FromBinary(dataManager.Data.Player.ChanceTime);
        DateTime rewardDeltaTime = saveTime.AddHours(rewardTime);

        TimeSpan dif =  rewardDeltaTime - nowTime;

        timeText.text = string.Format("{0}:{1} 후에 충전", dif.Minutes, dif.Seconds);

        if (dif.Hours >= rewardTime)
        {
            GetReward();
        }
        else
        {
            return;
        }
    }

    private void GetReward()
    {
        chanceManager.GiveChance();
    }
}
