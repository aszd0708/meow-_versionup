﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 터치 횟수 및 찬스가 차는 시간 관리해주는 클래스
/// </summary>
public class StageChanceManager : Singleton<StageChanceManager>
{
    [Header("데이터 매니저")]
    public SaveDataManager dataManager;

    [Header("지금 찬스 카운트 텍스트")]
    public StageChanceImg chanceText;

    [Header("Max 터치 이미지")]
    public StageChanceImg maxText;

    [Header("마이너스 이펙트")]
    public StageChanceMinImg minImg;

    [Header("타이머")]
    public StageChanceTimer timer;

    [Header("최대 찬스")]
    public int maxChanceCount;

    private int nowChanceCount = 0;

    /// <summary>
    /// 현재 찬스 프로퍼티
    /// </summary>
    public int NowChanceCount 
    { 
        get => nowChanceCount; 
        set
        {
            int prevNum = nowChanceCount;
            nowChanceCount = value;

            if (nowChanceCount > maxChanceCount)
                nowChanceCount = maxChanceCount;
            else if (nowChanceCount < 0)
                nowChanceCount = 0;

            // 마이너스 될때만 이펙트 출력
            else if(prevNum > nowChanceCount)
            {
                minImg.CreateMinusEffect();
            }

            if (nowChanceCount != 0)
            {
                timer.timeText.enabled = false;
                dataManager.EditTouchCount(nowChanceCount);
            }
            else
                timer.timeText.enabled = true;

            chanceText.ChangeNumber(nowChanceCount);
            // 여기 나중에 진동 넣기
        }
    }

    private void OnEnable()
    {
        maxText.ChangeNumber(maxChanceCount);
    }

    private void Start()
    {
        nowChanceCount = dataManager.Data.Player.TouchCount;
        chanceText.ChangeNumber(nowChanceCount);
        timer.StartTimer();

        //StartCoroutine(_Test());
    }

    /// <summary>
    /// 원하는 수 더해주는 함수
    /// </summary>
    /// <param name="index"></param>
    public void SetNowChaneCount(int index)
    {
        NowChanceCount += index;
    }

    /// <summary>
    /// 기회 Max로 채워줌
    /// </summary>
    public void GiveChance()
    {
        SetNowChaneCount(maxChanceCount);
        dataManager.EditChanceTime(DateTime.Now);
    }

    /// <summary>
    /// 매초마다 카운트 1씩 줄어드는 함수
    /// 테스트용으로 제작됨
    /// </summary>
    /// <returns></returns>
    private IEnumerator _Test()
    {
        while(true)
        {
            yield return new WaitForSeconds(1.0f);
            SetNowChaneCount(-1);
        }
    }
}
