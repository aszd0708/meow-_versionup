﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 숫자를 나눠 이미지로 표현하는 클래스
/// </summary>
public class StageChanceImg : MonoBehaviour
{
    [Header("숫자 이미지")]
    public Sprite[] numberImg;

    [Header("십의자리 이미지")]
    public Image tensImg;
    [Header("일의자리 이미지")]
    public Image unitsImg;

    /// <summary>
    /// 받은 숫자 변환
    /// </summary>
    /// <param name="number"></param>
    public void ChangeNumber(int number)
    {
        SetImg(SplitNum(number));
    }

    /// <summary>
    /// 숫자를 일의자리 십의자리로 나눠 반환
    /// index 0 -> 십의자리 1 -> 일의자리
    /// </summary>
    /// <param name="num"></param>
    /// <returns></returns>
    private int[] SplitNum(int num)
    {
        int[] splitNum = new int[2];

        splitNum[0] = num / 10;
        splitNum[1] = num % 10;

        return splitNum;
    }

    /// <summary>
    /// 받은 숫자를 이미지로 변환
    /// </summary>
    /// <param name="num"></param>
    private void SetImg(int[] num)
    {
        tensImg.sprite = numberImg[num[0]];
        unitsImg.sprite = numberImg[num[1]];
    }
}
