﻿using BitBenderGames;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// 사진찍는 스크립트 다시 만들고있음
/// </summary>
public class StagePhotoShotManager : Singleton<StagePhotoShotManager>
{
    [SerializeField]
    [Header("사진 RectTransform")]
    private RectTransform photoRect;

    #region NewLogo Classes
    [SerializeField]
    [Header("NewLogo Rect")]
    private RectTransform newLogoRect;
    [SerializeField]
    [Header("NewLogo JellyMotion")]
    private JellyMotion newLogoMotion;
    #endregion

    [Space(20)]

    #region LeftRight JellyMotion
    [SerializeField]
    [Header("JellyEffects (Left, Right)")]
    private JellyMotion[] jelltMotions = new JellyMotion[2];
    #endregion

    [Space(20)]

    #region Camera
    [SerializeField]
    [Header("터치 막기위한 터치매니져")]
    private StageTouchManagerBase touchManager;

    [SerializeField]
    [Header("메인 카메라 Transform")]
    private Transform mainCamTrasnform;

    [SerializeField]
    [Header("모바일 터치 카메라(MainCamera에 있음)")]
    private MobileTouchCamera MTC;
    #endregion

    [Space(20)]

    #region TakePickture Effect
    [SerializeField]
    [Header("찍을때 하얀색 이펙트")]
    private ShotEffect shotWhiteEffect;

    [SerializeField]
    [Header("찍을때 안에 동물 색 이펙트")]
    private TakeShotEffect takeShotEffect;

    public GameObject shotEffect;
    #endregion

    [Space(20)]

    #region Pickture Texts
    [SerializeField]
    [Header("동물 설명 텍스트 컴포넌트")]
    private Text animalText;
    [SerializeField]
    [Header("동물 이름 텍스트 컴포넌트")]
    private Text animalName;

    [SerializeField]
    [Header("하이퍼 텍스트")]
    private EasyHyperText hyperText;

    [SerializeField]
    [Header("텍스트 페이드 인 아웃")]
    private FadeInLetterByLetter fadeText;
    #endregion

    [Space(20)]

    #region 시간들
    [SerializeField]
    [Header("줌하는데 걸리는 시간")]
    private float zoomTime;
    #endregion

    [Space(20)]

    #region Canvases
    [SerializeField]
    [Header("게임 플레이 캔버스와 메뉴 캔버스")]
    private Canvas gameCanvas;
    [SerializeField]
    private Canvas menuCanvas;
    #endregion

    [Space(20)]

    [SerializeField]
    [Header("이 사진의 원하는 스케일 default = 3")]
    private float scale = 3;

    [SerializeField]
    [Header("원하는 속도(언제 쓰는지는 나중에 씀)")]
    private float speed = 2;

    [SerializeField]
    [Header("젤리 모션 시간")]
    private float jellyMotionTime;
    // 이건 필요할지 모르겠음
    public BlurControllor blur;

    [Space(20)]

    #region 초기 값들 저장하는 변수들
    [Header("처음 사진의 포지션(아마 액티브로 꺼줘서 필요가 없어질듯)")]
    private Vector2[] photoPos = new Vector2[2];
    [Header("처음 사진의 크기(필요함)")]
    private Vector2 originScale;
    [Header("MobileTouchCamera 에 있는 Min~~ 값")]
    private float originZoom;
    [Header("동물의 초기 Layer이름값")]
    private string catLayer;
    #endregion

    [Header("스킵 버튼에 관한 변수들")]
    public RectTransform skipButton;
    private bool skipCreate = false;

    // 광고양이 일때만 사용하는 변수 만약 이런 것돌이 많아지면 옵저버 모델 사용해서 재구성하기
    private CoinCatMotion madCat;

    private Vector2 originZoomVectorMax, originZoomVectorMin;

    public enum State
    {
        picture, text, end, idle
    }

    private State nowState;

    private RectTransform rect;

    [Header("테스트용 오브젝트")]
    public GameObject testObj;

    private bool endPickture = false;

    /// <summary>
    /// 이 사진 찍는 이펙트가 끝났는지 표현해주는 프로퍼티
    /// </summary>
    public bool EndPickture { get => endPickture; set => endPickture = value; }

    /// <summary>
    /// 현재 상태(텍스트인지 사진인지등...) 알려주는 enum변수
    /// 만들긴 했는데 실질적인 사용은 할지 안할지 모르겠다.
    /// </summary>
    public State NowState 
    { 
        get => nowState;
        set
        {
            nowState = value;

            switch(nowState)
            {
                case State.picture:
                    break;
                case State.text:
                    break;
                case State.end:
                    break;
                case State.idle:
                    break;
                default:
                    break;
            }
        }
    }

    [Header("사진 다 찍고 할 이벤트들")]
    public UnityEvent endPictureBehavior;


    void Start()
    {
        SetInitialData();
    }

    /// <summary>
    /// 초기 데이터 설정(크기와 위치등)
    /// </summary>
    private void SetInitialData()
    {
        photoPos[0] = photoRect.offsetMax;
        photoPos[1] = photoRect.offsetMin;
        originScale = photoRect.localScale;
        skipButton.localScale = Vector2.zero;
    }

    void Update()
    {
        StateController();
    }

    /// <summary>
    /// 반복문 안에서 상태가 언제 변하는지 체크해서 바뀌는 함수
    /// </summary>
    private void StateController()
    {
        switch (NowState)
        {
            case State.picture:
                break;
            case State.text:
                if (Input.GetMouseButtonUp(0))
                {
                    animalText.GetComponent<TypingText>().state = TypingText.State.end;
                    animalText.GetComponent<FadeInLetterByLetter>().state = FadeInLetterByLetter.State.end;
                    animalText.GetComponent<EasyHyperText>().enabled = true;
                    NowState = State.end;
                }
                break;
            case State.end:
                if (!skipCreate)
                {
                    skipCreate = true;
                    skipButton.transform.DOScale(new Vector2(-1, 1), 0.5f).SetEase(Ease.OutElastic);
                }
                break;
            case State.idle:
                break;
        }
    }

    /// <summary>
    /// 사진을 찍음
    /// </summary>
    /// <param name="animal"></param>
    public void StartTakePickture(GameObject animal)
    {
        EndPickture = false;
        ReadyTakePickture();
        StartCoroutine(_StartTakePictureEvent(animal));
    }

    /// <summary>
    /// 사진 찍기 전 준비
    /// </summary>
    private void ReadyTakePickture()
    {
        NowState = State.picture;

        menuCanvas.enabled = false;
        gameCanvas.enabled = false;
        originZoomVectorMin = MTC.BoundaryMin;
        originZoomVectorMax = MTC.BoundaryMax;
        MTC.BoundaryMax *= 2;
        MTC.BoundaryMin *= 2;
        MTC.ResetCameraBoundaries();
        originZoom = MTC.CamZoomMax;
        if(touchManager != null)
        {
            touchManager.ForbitTouch(false);
        }
    }

    private IEnumerator Zoom(float max, float min, float animeTime)
    {
        MobileTouchCamera touchCam = MTC;
        float zoomMax = max, zoomMin = min;
        float time = 0.0f;

        while (zoomMax > zoomMin)
        {
            time += Time.deltaTime / animeTime;

            zoomMax = Mathf.Lerp(max, min, time);

            touchCam.CamZoomMax = zoomMax;
            touchCam.ResetCameraBoundaries();
            yield return null;
        }
        yield break;
    }

    public void Backbutton()
    {
        if (madCat != null)
        {
            madCat.SendMessage("GoPositionStarter", SendMessageOptions.DontRequireReceiver);
            madCat.MCC.SendMessage("MadCatTouch", SendMessageOptions.DontRequireReceiver);
            madCat = null;
        }

        EndTakePictureEvent();
    }

    /// <summary>
    /// 순차대로 사진 찍는 코루틴
    /// </summary>
    /// <param name="selectCat"></param>
    /// <returns></returns>
    private IEnumerator _StartTakePictureEvent(GameObject selectCat)
    {
        #region 사진찍기 전 기본적인 세팅
        if (StageTouchManagerBase.Instance != null)
        {
            StageTouchManagerBase.Instance.ForbitTouch(false);
        }
        SetBeforeTakePickture(selectCat);
        #endregion

        #region 하이퍼텍스트 false -> 카메라 이동 하면서 줌(코루틴)
        hyperText.enabled = false;
        mainCamTrasnform.DOMove(
            new Vector3(selectCat.transform.position.x, selectCat.transform.position.y, mainCamTrasnform.position.z), 
            zoomTime).SetEase(Ease.Linear);
        yield return StartCoroutine(Zoom(MTC.CamZoomMax, MTC.CamZoomMin, zoomTime));
        #endregion

        #region 사진 찍고 사진안에 택스트와 블러 표현 및 크기 효과 스테이트 변경
        AudioManager.Instance.PlaySound("cheer_1", mainCamTrasnform.position);

        shotWhiteEffect.Shot();
        blur.On(new Vector3(mainCamTrasnform.position.x, mainCamTrasnform.position.y, 0));
        fadeText.Fade();
        photoRect.DOScale(originScale, speed);

        NowState = State.text;
        yield return new WaitForSeconds(speed);
        #endregion

        #region 사진 찍는 하얀색 이펙트 끝냄 안에 동물 색 효과, 젤리모션 3개 줌

        shotWhiteEffect.EndShot();

        // 이 밑에 이프문은 어캐해야 할지 아직 감이 안잡힘 일단 냅둠
        // 레이어 저장
        Transform selectAnimal = selectCat.transform;
        if (selectAnimal.GetComponent<SpriteRenderer>() != null)
        {
            catLayer = selectAnimal.GetComponent<SpriteRenderer>().sortingLayerName;
        }
        else if(selectAnimal.childCount > 0)
        {
            catLayer = selectAnimal.GetChild(0).GetComponent<SpriteRenderer>().sortingLayerName;
        }
        else
        {
            catLayer = "8";
        }

        takeShotEffect.Shot(selectCat);
        newLogoMotion.Motion(jellyMotionTime);
        jelltMotions[0].Motion(jellyMotionTime);
        jelltMotions[1].Motion(jellyMotionTime);
        yield return new WaitForSeconds(jellyMotionTime);
        yield break;

        #endregion
    }

    /// <summary>
    /// 기본적인 텍스트와 RectTransform에 관한 값들 수정
    /// 사진의 텍스트와 하이퍼링크 수정
    /// </summary>
    /// <param name="animalObj"></param>
    private void SetBeforeTakePickture(GameObject animalObj)
    {
        StageAnimalObjectBase animalInfo = animalObj.GetComponent<StageAnimalObjectBase>();
        photoRect.offsetMax = Vector2.zero;
        photoRect.offsetMin = Vector2.zero;
        photoRect.sizeDelta = new Vector2(0, 500);
        photoRect.localScale = Vector2.one * scale;
        animalName.text = animalInfo.Info.Name;
        animalText.text = animalInfo.Info.PhotoText;
        StringBuilder stringB = new StringBuilder();
        stringB.Append("https://www.instagram.com/");
        stringB.Append(animalInfo.Info.InstarURL);
        hyperText.hyperlinkString = stringB.ToString();
        fadeText.SetText(animalInfo.Info.PhotoText);
    }

    /// <summary>
    /// 사진 찍는 이벤트 종료하는 코루틴을 실행시키는 함수
    /// </summary>
    public void EndTakePictureEvent()
    {
        StartCoroutine(_EndTakePictureEvent());
    }

    /// <summary>
    /// 사진 찍는 이벤트 종료하는 코루틴
    /// </summary>
    /// <returns></returns>
    private IEnumerator _EndTakePictureEvent()
    {
        #region 검은색 블러 끄고 크기 0으로 줄여주고 이펙트 꺼줌
        blur.Off();
        photoRect.DOScale(0.0f, 0.3f).SetEase(Ease.InOutQuint);
        shotWhiteEffect.EndShot();
        yield return new WaitForSeconds(0.3f);
        #endregion

        #region 캔버스 켜주고 이펙트 정리 줌 원상태 색 정리함
        takeShotEffect.LayerSetting(catLayer);
        takeShotEffect.ListReset();
        if(touchManager)
        {
            touchManager.ForbitTouch(true);
        }
        menuCanvas.enabled = true;
        gameCanvas.enabled = true;

        //MTC.BoundaryMax = new Vector2(MTC.BoundaryMax.x / 2, MTC.BoundaryMax.y / 2);
        //MTC.BoundaryMin = new Vector2(MTC.BoundaryMin.x / 2, MTC.BoundaryMin.y / 2);
        MTC.BoundaryMax = originZoomVectorMax;
        MTC.BoundaryMin = originZoomVectorMin;
        MTC.CamZoomMax = originZoom;
        MTC.ResetCameraBoundaries();

        photoRect.offsetMax = photoPos[0];
        photoRect.offsetMin = photoPos[1];
        takeShotEffect.Img.color = takeShotEffect.firstColor;
        newLogoRect.localScale = new Vector2(0, 0);

        EndPickture = true;

        //SkipButtonAction();
        if (StageTouchManagerBase.Instance)
        {
            StageTouchManagerBase.Instance.ForbitTouch(true);
        }
        yield break;
        #endregion
    }

    /// <summary>
    /// 여기서 부터는 스킵 버튼이 나오는 함수 및 스킵버튼이 하는 일들 정리
    /// </summary>
    public void SkipButtonAction()
    {
        Backbutton();
        NowState = State.idle;
        skipCreate = false;
        skipButton.transform.DOScale(new Vector2(0, 0), 0.5f).SetEase(Ease.Linear);
        StartEndPictureBehavior();
    }

    /// <summary>
    /// 사진 찍고 난 뒤에 실행되는 함수
    /// </summary>
    public virtual void StartEndPictureBehavior()
    {
        endPictureBehavior.Invoke();
    }
}