﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// 스테이지 동물들에게 필요한 인포메이션
/// 번호만 받으면 알아서 갖고옴
/// </summary>
[Serializable]
public class StageAnimalInfo
{
    [SerializeField]
    [Header("번호")]
    private int no;
    [Header("이름")]
    private string name;
    [Header("스토리")]
    private string instarURL;
    [Header("해시태그")]
    private string photoText;
    [Header("인스타 url")]
    private string url;
    [Header("수집이 됐냐 안됐냐(이건 입력안받고 저장데이터 사용)")]
    private bool isCollect;
    [Header("가상 수집이 됐냐 안됐냐(이건 입력안받고 저장데이터 사용)")]
    private bool isCollectVirtual;
    
    /// <summary>
    /// 이 동물의 인덱스값(튜토리얼 고양이가 0)
    /// </summary>
    public int No { get => no; set => no = value; }
    /// <summary>
    /// 이 동물의 이름 프로퍼티
    /// </summary>
    public string Name { get => name; set => name = value; }

    /// <summary>
    /// 사진에 나올 텍스트중 인스타 링크
    /// </summary>
    public string InstarURL { get => instarURL; set => instarURL = value; }

    /// <summary>
    /// 사진에 나올 텍스트중 메인 텍스트
    /// </summary>
    public string PhotoText { get => photoText; set => photoText = value; }

    /// <summary>
    /// 이 동물이 수집이 돼있는지 확인하는 변수
    /// </summary>
    public bool IsCollect { get => isCollect; set => isCollect = value; }

    /// <summary>
    /// 세이브파일 삭제를 위한 변수
    /// </summary>
    public bool IsCollectVirtual { get => isCollectVirtual; set => isCollectVirtual = value; }

    /// <summary>
    /// 생성자 모두 이 동물에 관한 정보들을 갖고있음
    /// 번호만 입력받으면 세이브 데이터에서 갖고옴
    /// </summary>
    /// <param name="_no">번호</param>
    public StageAnimalInfo(int _no)
    {
        No = _no;
        AnimalData thisAnimalData = SaveDataManager.Instance.Data.Animals[No];
        AnimalEx thisAnimalEx = AnimalExJsonData.Instance.JsonData[No];

        Name = thisAnimalEx.Name;
        InstarURL = thisAnimalEx.Instargram;
        PhotoText = thisAnimalEx.HashTag;
        IsCollect = thisAnimalData.IsCollect;
        IsCollectVirtual = thisAnimalData.IsCollectVirtual;
    }
}

public abstract class StageAnimalObjectBase : MonoBehaviour, TouchableObj
{
    [Header("이 동물 번호값(첫번째가 0), 0보다 적을 시 무시")]
    public int animalNumber;

    [Header("이 동물에 관한 정보(위에 번호만 있으면 입력할 일 없음)")]
    private StageAnimalInfo info;

    protected bool endItemMotion;

    /// <summary>
    /// 동물이 갖고있는 정보 프로퍼티
    /// </summary>
    public StageAnimalInfo Info { get => info; set => info = value; }

    [SerializeField]
    [Header("숨겨진 동물이면 체크 하기! 버튼 언락 막아줌")]
    private bool isHidden = false;

    protected virtual void OnEnable()
    {
        if(animalNumber < 0)
        {
            return;
        }
        if(Info == null)
        {
            Info = new StageAnimalInfo(animalNumber);
        }
    }

    /// <summary>
    /// 만들때 SetCollect() 함수 무조건 포함하기
    /// </summary>
    /// <param name="touchPose">터치한 좌표</param>
    /// <returns>false 면 기회차감 ㄴㄴ true면 기회 차감함</returns>
    public abstract bool DoTouch(Vector2 touchPose = default);

    /// <summary>
    /// 아이템 놨을때 나오는 행동
    /// 무조건 안에 있는 if문 복붙하기
    /// </summary>
    /// <param name="itemObj">아이템 오브젝트</param>
    public virtual void StartItemEvent(GameObject itemObj)
    {
        if (endItemMotion)
        {
            PoolingManager.Instance.SetPool(itemObj, "ItemDummy");
            return;
        }
        endItemMotion = true;
    }

    /// <summary>
    /// 이 동물의 수집이 됐는지 확인 한 뒤 동물 번호로 저장하고 사진찍는 이펙트 나옴
    /// 수집아 안됐으면 함수 종료
    /// </summary>
    protected void SetCollect()
    {
        if (!Info.IsCollect)
        {
            // 세이브 변경 및 사진 찍는거 등등 여러가지 들어감
            if (SaveDataManager.Instance != null)
            {
                SaveDataManager.Instance.EditAnimalCollect(Info.No);
            }

            if (StagePhotoShotManager.Instance != null)
            {
                StagePhotoShotManager.Instance.StartTakePickture(gameObject);
            }

            if (StageUINewMarkController.Instance != null)
            {
                StageUINewMarkController.Instance.SetAnimalHouseNewMark(true);
            }

            if (!isHidden && StageAnimalButtonCreater.Instance != null)
            {
                StageAnimalButtonCreater.Instance.SetButtonCollect(animalNumber + 1);
            }

            if(NextStagePanelManager.Instance != null)
            {
                NextStagePanelManager.Instance.SetCollectCount();
            }

            Info.IsCollect = true;
            Info.IsCollectVirtual = true;
        }

        else if (!Info.IsCollectVirtual)
        {
            // 세이브 변경 및 사진 찍는거 등등 여러가지 들어감
            if (SaveDataManager.Instance != null)
            {
                SaveDataManager.Instance.EditAnimalCollect(Info.No);
            }

            if (StagePhotoShotManager.Instance != null)
            {
                StagePhotoShotManager.Instance.StartTakePickture(gameObject);
            }

            if (!isHidden && StageAnimalButtonCreater.Instance != null)
            {
                StageAnimalButtonCreater.Instance.SetButtonCollect(animalNumber + 1);
            }

            Info.IsCollectVirtual = true;
        }

        else
        {
            return;
        }
    }
}
