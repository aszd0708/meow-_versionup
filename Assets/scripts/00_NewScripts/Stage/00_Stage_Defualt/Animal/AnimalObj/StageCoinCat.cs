﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 스테이지 내에 있는 광고양이에 관한 클래스
/// </summary>
public class StageCoinCat : StageAnimalObjectBase
{
    #region 움직임에 관한 변수들
    [SerializeField]
    [Header("움직이는 좌표(2개)")]
    [Tooltip("이 좌표 안에서 랜덤으로 움직임")]
    private Vector3[] pos = new Vector3[2];

    [SerializeField]
    [Header("날아가는 시간")]
    private float flyTime;
    #endregion

    #region 모션에 관한 변수들
    [SerializeField]
    [Header("움직일 Transform")]
    private Transform coinCatTransform;

    [SerializeField]
    [Header("바꿀 스프라이트 랜더러")]
    private SpriteRenderer sprite;

    [SerializeField]
    [Header("모션 스프라이트")]
    private Sprite[] motionSprite = new Sprite[2];

    [SerializeField]
    [Header("이동시 몇번 움직일지 정함 최대 최소")]
    private int[] motionCountIndex = new int[2];

    #endregion

    #region
    [SerializeField]
    [Header("흩뿌려질 동전 오브젝트")]
    [Tooltip("풀링으로 처리함")]
    private GameObject coin;

    [SerializeField]
    [Header("동전 겟수")]
    private int count = 30;

    private GameObject[] createCoin;
    #endregion

    #region 여기서 사용하는 코루틴들
    private Coroutine catMoveCor;
    private Coroutine catMotion;
    #endregion

    #region WaitForSeconds들
    private WaitForSeconds goPositionWait;
    #endregion

    /// <summary>
    /// 터치 했을때 터치 막아주는 부울값
    /// </summary>
    private bool isTouch = true;

    /// <summary>
    /// 처음 시작할때 포지션
    /// </summary>
    private Vector3 originePose;

    // Start is called before the first frame update
    void Start()
    {
        originePose = transform.position;
        goPositionWait = new WaitForSeconds(1.0f);
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        MotionStarter();
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if (isTouch)
            return false;

        if (!Info.IsCollect)
        {
            // 여기에 이제 사진 촬영
            StopCoroutine(catMoveCor);
            StartCoroutine(_StartCoinMotion());
            StagePhotoShotManager.Instance.StartTakePickture(gameObject);

            StartCoroutine(_EndTakePickture());
        }        

        else
        {
            StartCoroutine(_GoEndEvent());
        }

        SetCollect();
        SaveDataManager.Instance.EditCoinCount(SaveDataManager.Instance.Data.Player.CoinCount +1);
        AudioManager.Instance.PlaySound("cat_5", transform.position);
        return false;
    }

    /// <summary>
    /// 사진 찍는게 끝날때까지 기다렸다가 가는 코루틴
    /// </summary>
    /// <returns></returns>
    private IEnumerator _EndTakePickture()
    {
        yield return new WaitUntil(() => StagePhotoShotManager.Instance.EndPickture);
        StartCoroutine(_GoEndEvent());
        yield break;
    }

    /// <summary>
    /// 가야할 포지션 갖고오는 함수
    /// </summary>
    /// <returns></returns>
    private Vector3 GetRandomPose()
    {
        return new Vector3(Random.Range(pos[0].x, pos[1].x), Random.Range(pos[0].y, pos[1].y), transform.position.z);
    }

    /// <summary>
    /// 고양이 모션 시작 함수
    /// </summary>
    public void MotionStarter()
    {
        isTouch = false;
        coinCatTransform.position = GetRandomPose();
        catMoveCor = StartCoroutine(_MoveCat());
    }

    /// <summary>
    /// 동전 흩뿌려주는 함수
    /// </summary>
    /// <returns></returns>
    public IEnumerator _StartCoinMotion()
    {
        coinCatTransform.DOPause();
        createCoin = new GameObject[count];
        for (int i = 0; i < createCoin.Length; i++)
        {
            createCoin[i] = PoolingManager.Instance.GetPool("Coin");
            if (createCoin[i] == null)
                createCoin[i] = Instantiate(coin);

            createCoin[i].transform.position = coinCatTransform.position;
            Vector3 currentPose = createCoin[i].transform.position;
            createCoin[i].transform.DOJump(new Vector3(currentPose.x+Random.Range(-2f, 2f), currentPose.y+Random.Range(-0.5f, 0.5f)), Random.Range(1, 2), Random.Range(1, 4), Random.Range(0.5f, 1.0f));
            createCoin[i].SetActive(true);
            createCoin[i].GetComponent<StageCoinCatCoinMotion>().CoinAnimation();
        }

        AudioManager.Instance.PlaySound("coin_1", transform.position);
        yield break;
    }

    private IEnumerator _GoEndEvent()
    {
        StopCoroutine(catMoveCor);
        yield return StartCoroutine(_GoOriginePose());
        StageCoinCatManager.Instance.TurnOffCoinCat();
        isTouch = true;
        yield break;
    }

    private IEnumerator _GoOriginePose()
    {
        coinCatTransform.DORotate(new Vector3(0, 0, GetAngle(originePose) - 90), 3.0f);
        StartCoroutine(_StartCoinMotion());
        yield return goPositionWait;
        coinCatTransform.DOMove(originePose, 0.5f);

        isTouch = false;
        yield break;
    }

    private IEnumerator _MoveCat()
    {
        while (true)
        {
            Vector3 randomPos = GetRandomPose();
            float randomTime = Random.Range(1.0f, 3.0f);
            coinCatTransform.DOMove(randomPos, flyTime);
            catMotion = StartCoroutine(_PlayMotion(flyTime, randomPos));
            yield return new WaitForSeconds(randomTime + flyTime);
        }
    }

    /// <summary>
    /// 모션 바꾸는 함수
    /// </summary>
    /// <param name="randomTime">바꾸는 총 시간</param>
    /// <param name="randomPos">목적지</param>
    /// <returns></returns>
    private IEnumerator _PlayMotion(float randomTime, Vector2 randomPos)
    {
        coinCatTransform.DORotate(new Vector3(0, 0, GetAngle(randomPos) - 90), 0.1f);
        yield return new WaitForSeconds(0.2f);
        int randomCount = Random.Range(motionCountIndex[0], motionCountIndex[1]);
        float time = randomTime / randomCount;

        int index = 0;
        bool flip = false;
        float yAxis = 0;

        WaitForSeconds timeWait = new WaitForSeconds(time);

        for (int i = 0; i < randomCount; i++)
        {
            sprite.sprite = motionSprite[index];
            sprite.flipX = flip;
            index++;
            index %= motionSprite.Length;
            yAxis += 180;
            yAxis %= 180;
            flip = !flip;
            yield return timeWait;
        }
        yield break;
    }

    private float GetAngle(Vector2 randomPos)
    {
        float angle;

        angle = Mathf.Atan2(randomPos.y - transform.position.y,
                            randomPos.x - transform.position.x) * 180 / Mathf.PI;
        return angle;
    }
}
