﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageCoinCatManager : Singleton<StageCoinCatManager>
{
    // TODO : 두개 배열 랜덤으로 
    [SerializeField]
    [Header("코인 고양이가 나오는 초")]
    private int[] coinCatAppearSecond = new int[2];

    [SerializeField]
    [Header("코인 고양이 컴포넌트")]
    private GameObject coinCat;

    private int nowSceond;

    private WaitForSeconds waitSeconds;

    protected void Start()
    {
        TurnOffCoinCat();
    }

    public void StartTimer()
    {
        StartCoroutine(_StartTimer());
    }

    /// <summary>
    /// 타이머 시작해서 끝나면 고양이 나옴
    /// </summary>
    /// <returns></returns>
    private IEnumerator _StartTimer()
    {
        float time = Random.Range(coinCatAppearSecond[0], coinCatAppearSecond[1]);
        waitSeconds = new WaitForSeconds(time);
        yield return waitSeconds;
        TurnOnCoinCat();
        yield break;
    }

    private void TurnOnCoinCat()
    {
        coinCat.SetActive(true);
    }

    public void TurnOffCoinCat()
    {
        coinCat.SetActive(false);
        StartTimer();
    }
}
