﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageCoinCatCoinMotion : MonoBehaviour
{
    private SpriteRenderer coinSprite;

    public void CoinAnimation()
    {
        if(coinSprite == null)
        {
            coinSprite = GetComponent<SpriteRenderer>();
        }
        StartCoroutine(_EndCoinAction());
    }

    public IEnumerator _EndCoinAction()
    {
        int count = 15;
        // 코인이 사라지기 기다리는 중....
        yield return new WaitForSeconds(1.0f);

        WaitForSeconds coinWait = new WaitForSeconds(0.1f);

        // 동전이 깜빡깜빡 거리다가 사라지기
        for (int i = 0; i < count; i++)
        {
            coinSprite.enabled = false;
            yield return coinWait;
            coinSprite.enabled = true;
            yield return coinWait;
        }
        //coin.transform.SetParent(null);
        PoolingManager.Instance.SetPool(gameObject, "Coin");
        yield break;
    }
}
