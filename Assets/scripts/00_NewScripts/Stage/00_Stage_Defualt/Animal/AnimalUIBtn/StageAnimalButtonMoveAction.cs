﻿using DanielLochner.Assets.SimpleScrollSnap;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI.Extensions;

/// <summary>
/// 버튼 눌렀을시 스크롤 패널 옮겨주는 클래스
/// </summary>
public class StageAnimalButtonMoveAction : MonoBehaviour
{
    [Header("스크롤 스냅")]
    public SimpleScrollSnap sss;

    [SerializeField]
    [Header("랙트")]
    private RectTransform rect;

    // 현재 인덱스 
    private int nowIndex;

    /// <summary>
    /// 스크롤 스냅 러프하게 움직임
    /// </summary>
    /// <param name="_moveYPose"></param>
    public void MoveCenterSnap(int index)
    {
        /*
        //if (nowIndex == 0)
            scrollSnap.UpdateLayout();
        nowIndex = index;
        float y = scrollSnap._childSize * index;
        scrollSnap._lerp_target = new Vector3(0, y, 0);
        scrollSnap._lerp = true;
        */
        sss.GoToPanel(index);


    }
}
