﻿using DanielLochner.Assets.SimpleScrollSnap;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI.Extensions;

/// <summary>
/// 버튼 통합
/// </summary>
public class StageAnimalButtonActionSetting : MonoBehaviour
{
    [Header("스크롤 움직이게 하는 컴포넌트")]
    public StageAnimalButtonMoveAction moveAction;
    [Header("힌트 움직이게 하는 컴포넌트")]
    public StageAnimalHintSetting hintAction;

    [Header("스크롤 스냅")]
    public SimpleScrollSnap sss;

    private void Start()
    {
        Invoke("MovePanel", 0.05f);
    }

    private void MovePanel()
    {
        sss.GoToPanel(sss.Panels.Length - 1);
    }

    /// <summary>
    /// 버튼 액션
    /// Y좌표 움직일것, 힌트 텍스트
    /// </summary>
    /// <param name="_moveYPose"></param>
    /// <param name="_hintText"></param>
    public void PlayButtonAction(int index, string _hintText)
    {
        sss.GoToPanel(index);
        //moveAction.MoveCenterSnap(index);
        hintAction.SetHintText(_hintText);
        hintAction.ResetCoolTime();
    }
}
