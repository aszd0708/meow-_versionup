﻿using DanielLochner.Assets.SimpleScrollSnap;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 동물 힌트 세팅 클래스
/// </summary>
public class StageAnimalHintSetting : MonoBehaviour
{
    [Header("힌트 오브젝트")]
    public GameObject hintObj;

    [Header("힌트 텍스트")]
    public Text hintText;

    [Header("사라지는 쿨타임")]
    public float closedCoolTime;

    [Header("스크롤 뷰")]
    public SimpleScrollSnap sss;

    [Header("콘텐츠 부모")]
    public RectTransform contentsRect;
    private GameObject[] contents;

    // 현재의 쿨타임
    private float nowCoolTime;

    private bool turn;

    // 각 버튼의 Y크기
    private float sizeY;

    private void Start()
    {
        nowCoolTime = closedCoolTime;
        turn = true;
    }

    private void Update()
    {
        CheckCoolTime();   
    }

    /// <summary>
    /// 쿨타임 계산해서 패널 숨기고 다시 켜주는 함수
    /// 업데이트문에서 항상 사용
    /// </summary>
    private void CheckCoolTime()
    {
        if (nowCoolTime >= closedCoolTime && turn)
        {
            hintObj.SetActive(false);
            turn = false;
            return;
        }

        else if (nowCoolTime < closedCoolTime)
        {
            turn = true;
            hintObj.SetActive(true);
            nowCoolTime += Time.deltaTime;
            SetHintText();
        }

        else return;
    }

    /// <summary>
    /// 텍스트 세팅
    /// </summary>
    /// <param name="_text"></param>
    public void SetHintText(string _text)
    {
        hintText.text = _text;
    }

    public void ResetCoolTime()
    {
        nowCoolTime = 0;
    }

    public void TurnOffPanel()
    {
        nowCoolTime = closedCoolTime;
    }

    public void SetHintText()
    {
        if (contents == null)
            contents = sss.Panels;
        if(sizeY == 0)
            sizeY = sss.size.y + sss.size.y * sss.automaticLayoutSpacing;

        StageAnimalButtonSetting selectPanel = sss.Panels[CalGap()].GetComponent<StageAnimalButtonSetting>();
        hintText.text = selectPanel.HintString;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private int CalGap()
    {
        float currentAnchorY = contentsRect.anchoredPosition.y - sizeY * 0.5f;

        if (currentAnchorY > 0)
            currentAnchorY = 0;

        return (int)Mathf.Abs(currentAnchorY / sizeY);
    }
}
