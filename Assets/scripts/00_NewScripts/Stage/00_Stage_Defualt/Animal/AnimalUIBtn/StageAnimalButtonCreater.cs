﻿using DanielLochner.Assets.SimpleScrollSnap;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI.Extensions;

/// <summary>
/// 보턴 만드는 클래스
/// </summary>
public class StageAnimalButtonCreater : Singleton<StageAnimalButtonCreater>
{
    [Header("만들 오브젝트")]
    public GameObject btn;

    [Header("만들 부모")]
    public RectTransform createRect;

    [Header("사베 다타")]
    public SaveDataManager saveData;

    [Header("동물 인덱스")]
    public int startIndex;
    public int endIndex;

    [Header("히든 동물 인덱스")]
    public int[] hiddenAnimalIndexes;

    [Header("위에 인덱스 차이 +1 만큼 크기 설정")]
    [Header("만들 동물 스프라이트")]
    public Sprite[] animalSprites;

    [Header("동물 설명 Json")]
    public AnimalExJsonData animalExJsonData;

    [Header("시작할때 위로 가게")]
    public StageAnimalButtonMoveAction buttonMove;

    [SerializeField]
    [Header("SimpleScrollSnap 에 있는 Margin top bottom 합한값 넗기")]
    private float marginSize;

    [Header("고양이 패널에 넣을 SimpleScrollSnap")]
    public SimpleScrollSnap sss;
    
    private Dictionary<int, StageAnimalButtonSetting> createButtonDiction = new Dictionary<int, StageAnimalButtonSetting>();

    /// <summary>
    /// 수집 했을때 찾아서 켜줌
    /// 따라서 들어가는 버튼은 수집이 안된 버튼만 들어감
    /// </summary>
    public Dictionary<int, StageAnimalButtonSetting> CreateButtonDiction { get => createButtonDiction; set => createButtonDiction = value; }

    private void Start()
    {
        CreateAnimalButton();
    }

    /// <summary>
    /// 동물 버튼 생성 함수
    /// </summary>
    private void CreateAnimalButton()
    {
        int count = endIndex - startIndex + 1;
        GameObject buttonObj = Instantiate(btn);
        int hiddenCount = 0;
        //Vector2 vec = new Vector2(0, marginSize);
        for (int i = 0; i < count; i++)
        {
            if(CheckHiddenAnimal(startIndex +i))
            {
                hiddenCount++;
                continue;
            }
            bool isCollect = saveData.Data.Animals[startIndex + i].IsCollectVirtual;

            GameObject obj = sss.AddToFrontAndReturnObject(buttonObj);
            StageAnimalButtonSetting setting = obj.GetComponent<StageAnimalButtonSetting>();//hiddenCount * 

            setting.SetButton(animalSprites[i - hiddenCount], animalExJsonData.JsonData[startIndex + i].Hint, isCollect, count - i - 2 + (hiddenCount * hiddenAnimalIndexes.Length));
            RectTransform rect = obj.GetComponent<RectTransform>();

            if (!isCollect)
            {
                CreateButtonDiction.Add(animalExJsonData.JsonData[startIndex + i].No, setting);
            }

            obj.SetActive(true);
            StartCoroutine(_SetButtonSetting(rect, Vector2.up * rect.sizeDelta.y * sss.automaticLayoutSpacing));
        }
        buttonObj.SetActive(false);
        //PoolingManager.Instance.SetPool(btn, "Trash");
        sss.GoToPanel(count - 1);
    }

    private bool CheckHiddenAnimal(int index)
    {
        bool isHidden = false;
        for(int i = 0; i < hiddenAnimalIndexes.Length; i++)
        {
            if(index == hiddenAnimalIndexes[i])
            {
                isHidden = true;
                break;
            }
        }
        return isHidden;
    }

    private IEnumerator _SetButtonSetting(RectTransform rect, Vector2 vec)
    {
        yield return new WaitForSeconds(0.1f);
        rect.sizeDelta += vec;
        yield break;
    }

    public void SetButtonCollect(int animalNo)
    {
        CreateButtonDiction[animalNo].SetButtonCollect();
        CreateButtonDiction.Remove(animalNo);
    }
}
