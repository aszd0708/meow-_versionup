﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 각 동물 버튼에 정보들 넣는 클래스
/// </summary>
public class StageAnimalButtonSetting : MonoBehaviour
{
    [Header("이미지 컴포넌트")]
    public Image img;

    [Header("도움말 string")]
    private string hintString;

    /// <summary>
    /// 힌트 문장 프로퍼티
    /// </summary>
    public string HintString { get => hintString; set => hintString = value; }

    [SerializeField]
    [Header("Contents에 있는 Layoutgroup Space값")]
    private float spaceValue;

    [Header("버튼 액선 컴포넌트 Awake에서 Fine~ 로 찾음")]
    private StageAnimalButtonActionSetting buttonAction;

    [SerializeField]
    [Header("렉트")]
    private RectTransform rect;

    [SerializeField]
    [Header("이 버튼의 인덱스값")]
    private int index;
    public int Index { get => index; set => index = value; }

    private void Awake()
    {
        buttonAction = FindObjectOfType<StageAnimalButtonActionSetting>();
    }

    /// <summary>
    /// 이 버튼 세팅
    /// 그림 힌트문장 콜렉트 유무
    /// </summary>
    /// <param name="_sprite"></param>
    /// <param name="_hintString"></param>
    /// <param name="isCollect"></param>
    public void SetButton(Sprite _sprite, string _hintString, bool isCollect, int _index)
    {
        img.sprite = _sprite;
        HintString = _hintString;

        if (isCollect) img.color = Color.white;
        else img.color = Color.black;

        Index = _index;
    }

    /// <summary>
    /// 이 동물을 수집했을때 이미지 컬러로 만듦
    /// </summary>
    public void SetButtonCollect()
    {
        img.color = Color.white;
    }

    /// <summary>
    /// 버튼 눌렀을때 액션
    /// </summary>
    public void DoButtonAction()
    {
        buttonAction.PlayButtonAction(Index, HintString);
    }
}
