﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageObjectSpriteChangeByTime : MonoBehaviour
{
    [Header("바꿀 스프라이트 랜더러 안넣으면 이 오브젝트 랜더러가 들어감")]
    [SerializeField]
    private SpriteRenderer spriteRenderer;

    [SerializeField]
    private Sprite[] changeSprites;

    [Header("처음 시작할때 스프라이트를 랜덤으로 줄지")]
    [SerializeField]
    private bool randomStart = false;

    [Header("한개만 넣으면 랜덤이 아니고 그 시간에만 움직임")]
    [SerializeField]
    private float[] changeTime;

    private Coroutine motionCor;

    [SerializeField]
    private bool startCorountineStart = true;

    private void Start()
    {
        if(spriteRenderer == null)
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

        if(startCorountineStart)
        {
            SpriteChange();
        }
    }

    public void SpriteChange()
    {
        if (spriteRenderer == null)
        {
            Debug.Log("Renderer is null");
            return;
        }

        if (changeTime.Length == 0)
        {
            Debug.Log("changeTime is null");
            return;
        }

        if (changeSprites.Length == 0)
        {
            Debug.Log("changeSprites is null");
            return;
        }
        
        if (randomStart)
        {
            spriteRenderer.sprite = changeSprites[Random.Range(0, changeSprites.Length)];
        }

        motionCor = StartCoroutine(_SpriteChange());
        Debug.Log(gameObject.name + "Coroutine Start");
    }

    private IEnumerator _SpriteChange()
    {
        int spriteIndex = 0;
        spriteRenderer.enabled = true;
        while (true)
        {
            WaitForSeconds waitTime;
            if (changeTime.Length == 1)
            {
                waitTime = new WaitForSeconds(changeTime[0]);
            }
            else
            {
                waitTime = new WaitForSeconds(Random.Range(changeTime[0], changeTime[1]));
            }
            spriteRenderer.sprite = changeSprites[spriteIndex++];
            spriteIndex %= changeSprites.Length;
            yield return waitTime;
        }
    }

    public void StopMotion()
    {
        if(motionCor != null)
        {
            StopCoroutine(motionCor);
        }
        spriteRenderer.enabled = false;
    }
}
