﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformLocationController : MonoBehaviour
{
    [SerializeField]
    private Transform changeTransform;

    [SerializeField]
    private Vector3[] changeVector3;

    public void ChangePosition(int index)
    {
        if (index >= changeVector3.Length)
            return;

        changeTransform.position = changeVector3[index];
    }
}
