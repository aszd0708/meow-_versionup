﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 필드에 있는 편지 오브젝트 터치 했을때 들어있어야 할 인포메이션
/// </summary>
public class StageLetterInfo : MonoBehaviour, TouchableObj
{
    public enum LetterKind
    {
        RANDOM, FIXED
    }

    [SerializeField]
    [Header("이 편지의 종류(랜덤인지 고정인지)")]
    private LetterKind kind;

    /// <summary>
    /// 편지의 종류 갖고오는 프로퍼티
    /// </summary>
    public LetterKind Kind { get => kind; set => kind = value; }

    [SerializeField]
    [Header("고정된 편지에만 처음에 값을 불러옴")]
    private int letterID;
    public int LetterID 
    { 
        get => letterID;
        set
        {
            letterID = value;
            letterInfo = LetterJsonData.Instance.DataDiction[letterID];
        }
    }

    private void Start()
    {
        if(Kind == LetterKind.FIXED)
        {
            LetterID = letterID;
        }    
    }

    [Header("편지 인포메이션")]
    private LetterJson letterInfo;

    public bool DoTouch(Vector2 touchPose = default)
    {
        LetterUIMotion.Instance.StartLetter(letterInfo.Text);

        bool isCollected = SaveDataManager.Instance.Data.Letters[LetterID].IsCollect;

        if(isCollected == false)
        {
            PlayerPrefs.SetInt("NewCollectLetter", 1);
        }

        SaveDataManager.Instance.EditLetterCollect(LetterID);

        //StageLetterManager.Instance.SetRandomLetter(Kind);

        //StageLetterSubtitleButtonInfo subtitle = StageLetterTitleDivision.Instance.GetSubtitleButtonInfo(LetterID);
        //if (subtitle)
        //{
        //    subtitle.SetCollect();
        //}
        return false;
    }
}
