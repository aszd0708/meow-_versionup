﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageLetterManager : Singleton<StageLetterManager>
{
    [SerializeField]
    [Header("편지들")]
    private StageLetterInfo[] letters;

    private List<StageLetterInfo> randomLetters = new List<StageLetterInfo>();
    private List<int> randomLetterID = new List<int>();

    private int letterTextPointer;

    /// <summary>
    /// 편지 텍스트 어디인지 알려주는 프로퍼티
    /// </summary>
    public int LetterTextPointer 
    { 
        get => letterTextPointer;
        set
        {
            if (value >= randomLetterID.Count)
                value = 0;
            letterTextPointer = value;
        }
    }


    private int letterPositionPointer;

    /// <summary>
    /// 편지 위치 포인터
    /// </summary>
    public int LetterPositionPointer 
    { 
        get => letterPositionPointer;
        set
        {
            if (value >= randomLetters.Count)
                value = 0;
            letterPositionPointer = value;
        }
    }

    private void Start()
    {
        // TODO :
        //SetRandomLetters();

    }

    /// <summary>
    /// 분류 해서 Random인것만 갖고옴
    /// 그리고 난 후 그 중에 하나만 열어줌
    /// </summary>
    private void SetRandomLetters()
    {
        for (int i = 0, randomCount = 0; i < letters.Length; i++)
        {
            if (letters[i].Kind == StageLetterInfo.LetterKind.RANDOM)
            {
                randomLetters.Add(letters[i]);
                randomLetterID.Add(letters[i].LetterID);
                randomLetters[randomCount].gameObject.SetActive(false);
                randomCount++;
            }
        }

        letterTextPointer = Random.Range(0, randomLetterID.Count);
        letterPositionPointer = Random.Range(0, randomLetters.Count);

        ResetRandomLetter();
    }

    /// <summary>
    /// 랜덤 인덱스 이용해서 편지 ID 변경 및 오브젝트 꺼줌
    /// </summary>
    private void ResetRandomLetter()
    {
        randomLetters[LetterPositionPointer++].gameObject.SetActive(false);
        randomLetters[LetterPositionPointer].gameObject.SetActive(true);
        LetterTextPointer++;
        randomLetters[LetterPositionPointer].LetterID = randomLetterID[LetterTextPointer];
    }

    /// <summary>
    /// 편지들 클릭했을 시 종류에 따라서 재할당 받음
    /// </summary>
    /// <param name="_kind">Random일 경우에만 재할당</param>
    public void SetRandomLetter(StageLetterInfo.LetterKind _kind)
    {
        if (_kind == StageLetterInfo.LetterKind.RANDOM)
        {
            ResetRandomLetter();
        }

        else return;
    }
}
