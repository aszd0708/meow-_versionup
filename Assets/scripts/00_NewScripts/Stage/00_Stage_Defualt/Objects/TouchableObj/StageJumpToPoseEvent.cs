﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class StageJumpToPoseEvent : MonoBehaviour
{
    [SerializeField]
    private Transform jumpObj;
    
    [SerializeField]
    private Transform jumpPose;

    [SerializeField]
    private float jumpTime;

    [SerializeField]
    private float jumpPow;

    [SerializeField]
    private int jumpCount;

    [SerializeField]
    private Ease jumpEase = Ease.Linear;

    public Transform JumpPose { get => jumpPose; set => jumpPose = value; }

    public void JumpEvent()
    {
        if(jumpObj.GetComponent<StageItemBase>() != null)
        {
            if(SaveDataManager.Instance.ItemDiction[jumpObj.GetComponent<StageItemBase>().ItemName] == true)
            {
                return;
            }
        }

        jumpObj.DOJump(jumpPose.position, jumpPow, jumpCount, jumpTime).SetEase(jumpEase);
    }
}
