﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class StageInfinitySpriteObject : StageInfinityTouchObjectBase
{
    #region 스프라이트 랜더러
    [SerializeField]
    [Header("바꿀 스프라이트 랜더러")]
    private SpriteRenderer spriteRenderer;

    [SerializeField]
    [Header("바꿀 스프라이트들")]
    private Sprite[] changeSprites;

    [SerializeField]
    [Header("시작했을때 랜덤 스프라이트로 나오는지")]
    [Space(30)]
    private bool isStartRandom = true;

    [SerializeField]
    [Header("무제한으로 돌리냐?")]
    private bool isInfinity = true;
    private int spritePointer = 0;
    #endregion

    [SerializeField]
    [Header("랜덤으로 180도 돌릴까")]
    private bool isRotate;


    #region 이벤트 오브젝트
    [SerializeField]
    [Header("일정횟수")]
    private int eventTouchCount;

    [SerializeField]
    [Header("일정 횟수 터치시 나올 오브젝트")]
    protected Transform eventItem;

    private int nowTouchCount = 0;
    #endregion

    [SerializeField]
    private UnityEvent touchEvent;

    [SerializeField]
    private string audioName;

    private void Awake()
    {        
        if(!spriteRenderer)
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
        }
    }

    private void Start()
    {
        if(isStartRandom)
        {
            spritePointer = Random.Range(0, changeSprites.Length);
        }

        if(isRotate)
        {
            int sight = Random.Range(0, 2);
            if (sight == 0)
            {
                transform.rotation = Quaternion.Euler(0, 180, 0);
            }
            else
            {
                transform.rotation = Quaternion.Euler(0, 0, 0);
            }
        }

        if (eventItem)
        {
            spritePointer = 0;
        }

        ChangeSprite();
    }

    public override bool DoTouch(Vector2 touchPose = default)
    {
        nowTouchCount++;
        ChangeSprite();
        
        if(string.IsNullOrEmpty(audioName) == false)
        {
            AudioManager.Instance.PlaySound(audioName, transform.position);
        }

        if (eventItem)
        {
            if (eventTouchCount == nowTouchCount)
            {
                StartEvent();
            }
        }

        return false;
    }

    private void ChangeSprite()
    {
        if (changeSprites.Length == 0)
        {
            return;
        }
        else
        {
            spriteRenderer.sprite = changeSprites[spritePointer++];
            touchEvent.Invoke();
        }
        if (isInfinity)
        {
            spritePointer %= changeSprites.Length;
        }

        else
        {
            if(spritePointer >= changeSprites.Length)
            {
                spritePointer = changeSprites.Length - 1;
            }
        }
    }

    protected virtual void StartEvent()
    {
        //Debug.Log("이벤트 발생");
        eventItem.position = transform.position;
        eventItem.gameObject.SetActive(true);
    }
}
