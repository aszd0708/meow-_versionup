﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class StageObjectBasicEvent : MonoBehaviour, TouchableObj
{
    [SerializeField]
    [Header("터치 했을때 이벤트")]
    private UnityEvent touchEvent;

    [SerializeField]
    [Header("기회 차감 할까? (true면 차감 false면 안차감)")]
    private bool minChance;

    [SerializeField]
    private string audioName;

    public bool DoTouch(Vector2 touchPose = default)
    {
        touchEvent.Invoke();

        if(string.IsNullOrEmpty(audioName) == false)
        {
            AudioManager.Instance.PlaySound(audioName, transform.position);
        }

        return minChance;
    }
}
