﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StageInfinityTouchObjectBase : MonoBehaviour, TouchableObj
{
    public abstract bool DoTouch(Vector2 touchPose = default);
}
