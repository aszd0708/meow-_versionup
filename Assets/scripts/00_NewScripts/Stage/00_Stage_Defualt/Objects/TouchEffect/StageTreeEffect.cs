﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 나무 터치 시 나오는 이펙트
/// </summary>
public class StageTreeEffect : MonoBehaviour, TouchableObj
{
    [SerializeField]
    [Header("나무 터치했을 시 나오는 이펙트")]
    private GameObject treeEffect;

    [SerializeField]
    private GameObject otherEffect;

    public bool DoTouch(Vector2 touchPose = default)
    {
        string poolingName;
        bool temp = IntRandom.StateRandomInt();
        if (temp)
        {
            poolingName = "TreeTouchOtherEffect";
        }
        else
        {
            poolingName = "TreeEffect";
        }

        GameObject effect = PoolingManager.Instance.GetPool(poolingName);
        if (!effect)
        {
            if(temp)
            {
                effect = Instantiate(otherEffect);
            }
            else
            {
                effect = Instantiate(treeEffect);
            }
        }
        effect.transform.position = touchPose;


        AudioManager.Instance.PlaySound("in_1", transform.position);

        return true;
    }
}
