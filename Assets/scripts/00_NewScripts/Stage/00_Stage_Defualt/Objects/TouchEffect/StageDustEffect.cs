﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 먼지 나와서 이펙트 나오고 사라지는 클래스
/// </summary>
public class StageDustEffect : MonoBehaviour
{
    [SerializeField]
    [Header("먼지 스프라이트들")]
    private Sprite[] dustSprite;

    [SerializeField]
    [Header("스프라이트 한개당 나오는 시간")]
    private float spriteTime = 0.03f;

    [SerializeField]
    [Header("자신 스프라이트 랜더러")]
    private SpriteRenderer spriteRenderer;

    private void OnEnable()
    {
        StartEffect();
    }

    /// <summary>
    /// 이 오브젝트가 Active(false)가 되면 진행하고있던 코루틴 꺼줌
    /// </summary>
    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void StartEffect()
    {
        StartCoroutine(_StartEffect());
    }

    /// <summary>
    /// 스프라이트 한개씩 바꾸는 스프라이트
    /// </summary>
    /// <returns></returns>
    private IEnumerator _StartEffect()
    {
        WaitForSeconds wait = new WaitForSeconds(spriteTime);
        spriteRenderer.enabled = true;
        for (int i = 0; i < dustSprite.Length; i++)
        {
            spriteRenderer.sprite = dustSprite[i];
            yield return wait;
        }
        spriteRenderer.enabled = false;
        PoolingManager.Instance.SetPool(gameObject, "Dust");
        yield break;
    }
}
