﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface TouchableObj
{
    /// <summary>
    /// 터치 했을시 명령어 사용
    /// 반환값에 따라 횟수 차감되는지 확인(StageTouchManagerBase 에서 사용)
    /// 반드시 이걸 사용했을 시 레이어 TouchableObj로 변경할것
    /// </summary>
    /// <returns>true면 기회 차감 false면 기회 안차감</returns>
    bool DoTouch(Vector2 touchPose = default);
}

public interface TouchDownableObj
{
    /// <summary>
    /// 슬라이드가 가능한 오브젝트만 사용
    /// 터치 했을때 좌표 넘겨줌
    /// 반드시 이걸 사용했을 시 레이어 SlideObj로 변경할것
    /// </summary>
    /// <param name="touchPose"></param>
    /// <returns>true면 기회 차감 false면 기회 안차감</returns>
    bool DoTouchDown(Vector2 touchPose = default);
}

public interface ITouchManger
{
    void ForbitTouch(bool canTouch);
}