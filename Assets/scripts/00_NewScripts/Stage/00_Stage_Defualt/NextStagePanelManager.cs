﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextStagePanelManager : Singleton<NextStagePanelManager>
{
    [SerializeField]
    private int nextStageOpenAnimalCount = -1;

    [SerializeField]
    private int currentCount = 0;

    [SerializeField]
    private int[] animalIndex;

    [SerializeField]
    private RectTransform popupRect;

    private void Start()
    {
        SetCount();
    }

    private void SetCount()
    {
        if(nextStageOpenAnimalCount == -1)
        { return; }
        for(int i = animalIndex[0]; i <= animalIndex[1]; i++)
        {
            if(SaveDataManager.Instance.Data.Animals[i].IsCollect)
            {
                currentCount++;
            }
        }
    }

    public void SetCollectCount()
    {
        currentCount++;
        SetPanel();
    }

    private void SetPanel()
    {
        if(nextStageOpenAnimalCount == currentCount)
        {
            PopupManager.Instance.OpenPopup(popupRect);
        }
    }
}
