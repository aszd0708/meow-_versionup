﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class StageItemUIDragAndDrop : MonoBehaviour, IDragHandler, IEndDragHandler, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField]
    [Header("아이템 버튼 RectTransform")]
    private RectTransform rect;

    [Header("위치해야 할 Transform")]
    private Transform triggerTransform;
    /// <summary>
    /// 체크할 Transform
    /// </summary>
    public Transform TriggerTransform { get => triggerTransform; set => triggerTransform = value; }

    [SerializeField]
    [Header("트리거 ")]
    public float triggerDistance;

    public StageAnimalObjectBase targetAnimal;

    [SerializeField]
    [Header("동물한테 줄때 사용할 오브젝트(default)")]
    protected GameObject itemObj;

    [SerializeField]
    [Header("아이템 스프라이트(없으면 이 이미지의 스프라이트 갖고옴)")]
    protected Sprite itemSprite;

    [SerializeField]
    [Header("이미지 컴포넌트")]
    protected Image img;

    [SerializeField]
    [Header("아이템 이미지 RectTransform")]
    private RectTransform imageRect;

    private float size;

    [SerializeField]
    private UnityEvent onDragEvent;
    [SerializeField]
    private UnityEvent onDragEndEvent;

    [SerializeField]
    private UnityEvent animalEventEnd;

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = new Vector3(Input.mousePosition.x, Input.mousePosition.y + size, Input.mousePosition.z);
    }

    public virtual void OnEndDrag(PointerEventData eventData)
    {
        Vector2 worldVector = Camera.main.ScreenToWorldPoint(transform.position);

        if(TriggerTransform.gameObject.activeSelf == false)
        {
            Debug.Log("아이템 트리거 꺼져있음");
            return;
        }

        if (Vector2.Distance(TriggerTransform.position, worldVector) <= triggerDistance)
        {
            GameObject item = Instantiate(itemObj);
            if (!itemSprite)
            {
                itemSprite = img.sprite;
            }
            item.GetComponent<SpriteRenderer>().sprite = itemSprite;
            targetAnimal.StartItemEvent(item);
            animalEventEnd.Invoke();
        }
        else
        {

            AudioManager.Instance.PlaySound("er_3", transform.position);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        size = rect.sizeDelta.y * 1.5f;
        transform.position = new Vector3(Input.mousePosition.x, Input.mousePosition.y + size, Input.mousePosition.z);
        onDragEvent.Invoke();
    }

    /// <summary>
    /// 포지션 재설정
    /// </summary>
    private void ResetPosition()
    {
        imageRect.offsetMin = Vector2.zero;
        imageRect.offsetMax = Vector2.zero;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Invoke("ResetPosition", 0.05f);
        onDragEndEvent.Invoke();
    }
}
