﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageItemUISetting : MonoBehaviour
{
    [Header("아이템 이미지 컴포넌트")]
    public Image itemImg;

    [SerializeField]
    [Header("Json에 표기된 이름이랑 같은것 사용해야 함")]
    [Header("아이템 이름(검사할때 사용)")]
    private string itemName;

    /// <summary>
    /// 아이템 이름
    /// 혹시 몰라서 만들었음
    /// </summary>
    public string ItemName { get => itemName; set => itemName = value; }

    private Transform targetTransform;
    /// <summary>
    /// 아이템을 놨을때 검사할 타겟 트랜스폼
    /// </summary>
    public Transform TargetTransform { get => targetTransform; set => targetTransform = value; }

    [SerializeField]
    [Header("아이템 드래그 앤 드롭")]
    private StageItemUIDragAndDrop dragAndDrop;

    public StageAnimalObjectBase targetAnimal;

    /// <summary>
    /// 이건 아이템 오브젝트에 있어야 할거 같음.
    /// 밑에 일 다 끝내면 옮김
    /// </summary>
    public void CheckItemCollect()
    {
        bool isCollect = SaveDataManager.Instance.ItemDiction[ItemName];

        if(isCollect)
        {
            // 여기에 아이템 
        }
    }

    /// <summary>
    /// 아이템 버튼 세팅하는 스크립트
    /// </summary>
    /// <param name="_itemSprite">Image에 넣을 스프라이트</param>
    /// <param name="_itemName">이 컴포넌트에 넣을 아이템 이름</param>
    /// <param name="_targetTransform">타겟이 될 트랜스폼</param>
    public void SetItemUIButton(Sprite _itemSprite, string _itemName, Transform _targetTransform, StageAnimalObjectBase _targetAnimal, float distance)
    {
        itemImg.sprite = _itemSprite;
        ItemName = _itemName;
        if(_targetTransform)
        {
            TargetTransform = _targetTransform;
            dragAndDrop.TriggerTransform = TargetTransform;
        }

        targetAnimal = _targetAnimal;
        dragAndDrop.targetAnimal = _targetAnimal;
        dragAndDrop.triggerDistance = distance;
    }
}
