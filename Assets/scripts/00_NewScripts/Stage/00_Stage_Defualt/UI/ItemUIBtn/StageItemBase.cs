﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

/// <summary>
/// 필드에 있는 아이템 오브젝트 스크립트
/// 터치했을때나 아니면 모션등 여러가지 담당할 예정
/// 하나하나 다 넣어준 다음에 사용하는게 좋을듯
/// </summary>
public class StageItemBase : MonoBehaviour, TouchableObj
{
    #region 터치했을때 나오는 애니메이션에 관한 변수들
    [Header("가방 Rect(RectPose를 일반 Vector로 변환해서 사용)")]
    public RectTransform bagRectTransform;
    [Header("터치했을때 나올 이펙트(동그란 원)")]
    public GameObject whiteCircleObj;
    [Header("이펙트가 나오는 시간")]
    public float effectTime;
    [Header("가방에 들어가는 시간")]
    public float moveTime;
    #endregion

    #region UI에 필요한 변수들
    [Header("아이템 UI버튼")]
    public GameObject itemUIButton;
    [Header("아이템 스프라이트(안넣으면 이 오브젝트 스프라이트 들어감)")]
    public Sprite itemSprite;
    [Header("아이템 이름")]
    public string ItemName;
    [Header("이 아이템을 드래그 할때 검사할 타겟 Transform")]
    public Transform targetTransform;

    [Header("버튼을 어디 자식으로 두는지")]
    public RectTransform itemParent;

    [Header("알아서 크기 조정 및 넣어주는 함수 사용하기 위해 사용")]
    public FoldUI buttonsP;

    [Header("스테이지 동물")]
    public StageAnimalObjectBase targetAnimal;
    #endregion

    [SerializeField]
    [Header("터치에 관한 콜라이더(자신)")]
    protected Collider2D col;
    [SerializeField]
    [Header("스프라이트 랜더러(자신)")]
    protected SpriteRenderer spriteRenderer;

    [SerializeField]
    private float distance = 5.0f;

    private bool bIsCollected = false;

    // 아마 가방쪽은 Start에서 길이 조절하면 될듯.
    protected virtual void OnEnable()
    {
        if(bIsCollected == true)
        { 
            gameObject.SetActive(false);
            Debug.Log(ItemName + "수집됨");
            return;
        }
        Debug.Log(ItemName + "안수집됨");

        if (itemSprite == null) itemSprite = spriteRenderer.sprite;
        CheckCollect();
    }

    public virtual bool DoTouch(Vector2 touchPose = default)
    {
        DoTouchAction();
        SaveDataManager.Instance.EditItemCollect(ItemName);
        return false;
    }

    /// <summary>
    /// 수집 했는지 확인 한 뒤 아이템에 넣음
    /// </summary>
    private void CheckCollect()
    {
        bool isCollect = SaveDataManager.Instance.ItemDiction[ItemName];
        bIsCollected = isCollect;
        if (isCollect)
        {
            col.enabled = false;
            spriteRenderer.enabled = false;
            CreateItemBtn();
        }
        else return;
    }

    /// <summary>
    /// 터치했때 나올 액션
    /// 나중에 오브젝트마다 설정해야 하면 바꾸기 위해 가상함수로 만듦
    /// </summary>
    public virtual void DoTouchAction()
    {
        StartCoroutine(_CreateItemBtnWithEffect());
    }

    /// <summary>
    /// 이펙트 후 아이템 버튼 만드는 함수
    /// </summary>
    /// <returns></returns>
    private IEnumerator _CreateItemBtnWithEffect()
    {
        col.enabled = false;

        AudioManager.Instance.PlaySound("find", transform.position);
        yield return StartCoroutine(_DoTouchAction());

        EndButtonEvent();
        CreateItemBtn();
        yield break;
    }

    protected virtual void EndButtonEvent()
    {
        Debug.Log("버튼 끝남");
    }

    /// <summary>
    /// 아이템 버튼 만드는 함수
    /// </summary>
    private void CreateItemBtn()
    {
        GameObject btn = Instantiate(itemUIButton);
        btn.SetActive(true);
        StageItemUISetting btnSetting = btn.GetComponent<StageItemUISetting>();
        btnSetting.SetItemUIButton(itemSprite, ItemName, targetTransform, targetAnimal , distance);
        buttonsP.SetSize(btn);
        buttonsP.CollectItem();

        spriteRenderer.enabled = false;

        if (PoolingManager.Instance != null)
        {
            PoolingManager.Instance.SetPool(gameObject, "Trash", 0.5f);
        }
        else
        {
            gameObject.SetActive(false);
        }
        //FoldUI.Instance.OpenUI();

        
    }

    /// <summary>
    /// 터치하면 나오는 이펙트
    /// 동그란 원 나오고 몇초 뒤 가방쪽으로 가는 애니메이션
    /// </summary>
    /// <returns></returns>
    private IEnumerator _DoTouchAction()
    {
        Vector3 bagPose = Camera.main.ScreenToWorldPoint(bagRectTransform.position);
        transform.DOPause();
        SetCollectSpriteRenderer(ref spriteRenderer, 5);
        GameObject whiteCircle = PoolingManager.Instance.GetPool("WhiteCircle");
        if (whiteCircle == null)
            whiteCircle = Instantiate(whiteCircleObj);

        SetCollectSpriteRenderer(ref whiteCircle, 4);
        whiteCircle.transform.position = transform.position;

        yield return new WaitForSeconds(effectTime);

        PoolingManager.Instance.SetPool(whiteCircle, "WhiteCircle");
        transform.DOJump(bagPose, 5.0f, 1, moveTime);

        yield return new WaitForSeconds(moveTime);

        Debug.Log("가방 안으로 들어갔음");
        StageUINewMarkController.Instance.SetBagNewMark(false);
        yield break;
    }

    /// <summary>
    /// 오브젝트 레이어 변경 및 소팅 오더 변경
    /// </summary>
    /// <param name="obj">바꿀 오브젝트</param>
    /// <param name="order">원하는 소팅 오더</param>
    private void SetCollectSpriteRenderer(ref GameObject obj, int order)
    {
        SpriteRenderer renderer = obj.GetComponent<SpriteRenderer>();
        renderer.sortingLayerName = "ItemCollectLayer";
        renderer.sortingOrder = order;
    }
    
    /// <summary>
     /// 오브젝트 레이어 변경 및 소팅 오더 변경
     /// </summary>
     /// <param name="obj">바꿀 오브젝트의 스프라이트 랜더러</param>
     /// <param name="order">원하는 소팅 오더</param>
    private void SetCollectSpriteRenderer(ref SpriteRenderer renderer, int order)
    {
        renderer.sortingLayerName = "ItemCollectLayer";
        renderer.sortingOrder = order;
    }
}
