﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageUINewMarkController : Singleton<StageUINewMarkController>
{
    [SerializeField]
    [Header("가방 new 마크")]
    private GameObject bagNewMark;
    [SerializeField]
    [Header("동물집 new 마크")]
    private GameObject animalHouseNewMark;

    private void Start()
    {
        bagNewMark.SetActive(false);
        animalHouseNewMark.SetActive(false);
    }

    /// <summary>
    /// 가방 new 마크 컨트롤
    /// </summary>
    /// <param name="isActive"></param>
    public void SetBagNewMark(bool isActive)
    {
        bagNewMark.SetActive(isActive);
    }
    
    /// <summary>
    /// 동물집 new 마크 컨트롤
    /// </summary>
    /// <param name="isActive"></param>
    public void SetAnimalHouseNewMark(bool isActive)
    {
        animalHouseNewMark.SetActive(isActive);
    }
}
