﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageLetterSubtitleButtonInfo : MonoBehaviour
{
    [SerializeField]
    private RectTransform rect;

    [Header("제목 Text 컴포넌트")]
    public Text subTitleText;

    [Header("언락돼있는 상태에서 보여줄 이름")]
    public string unlockSubtitleText;

    private LetterSubtitleInfo info;

    [SerializeField]
    [Header("부제 버튼 오브젝트의 버튼 컴포넌트")]
    private Button button;

    [SerializeField]
    [Header("부제 버튼 오브젝트의 이미지 컴포넌트")]
    private Image img;

    [SerializeField]
    private Image letterImg;

    /// <summary>
    /// id, 제목, 내용등
    /// </summary>
    public LetterSubtitleInfo Info { get => info; set => info = value; }

    private void OnEnable()
    {
        rect.localScale = Vector3.one;
    }

    public void SetButton(LetterSubtitleInfo _info)
    {
        Info = _info;

        bool isCol = SaveDataManager.Instance.letterDataManager.DataDiction[Info.SubtitleID].IsCollect;
        //isCol = true;
        SetSubtitleButton(isCol);
        if(!isCol)
        {
            StageLetterTitleDivision.Instance.SubtitleDiction.Add(info.SubtitleID, this);
        }
        gameObject.SetActive(false);
    }

    /// <summary>
    /// 버튼 액션 넘겨줌
    /// </summary>
    public void PlayButtonAction()
    {
        Debug.Log(Info.Contents);
        LetterUIMotion.Instance.StartLetter(Info.Contents);
    }

    public void SetCollect()
    {
        SetSubtitleButton(true);
    }

    private void SetSubtitleButton(bool isCollect)
    {
        if (isCollect)
        {
            img.color = Color.white;
            subTitleText.text = Info.Subtitle;
        }

        else
        {
            img.color /= 2;
            subTitleText.text = unlockSubtitleText;
        }

        letterImg.enabled = isCollect;
        button.enabled = isCollect;
    }
}
