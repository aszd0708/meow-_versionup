﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageLetterTitleButtonInfo : MonoBehaviour
{
    [Header("제목 텍스트 컴포넌트")]
    public Text titleText;


    private LetterInfo info;

    public LetterInfo Info { get => info; set => info = value; }

    private List<GameObject> subtitleButtons = new List<GameObject>();

    private GameObject buttonObj;
    private Transform buttonParent;

    public void SetInfo(LetterInfo _info, GameObject _btnObj, Transform _btnParent)
    {
        Info = _info;
        buttonObj = _btnObj;
        buttonParent = _btnParent;


        titleText.text = Info.Title;
        CreateSubtitleButtons();
    }

    /// <summary>
    /// 켜주고 꺼주는 함수
    /// </summary>
    /// <param name="isOn"></param>
    public void PlayerButtonAction(bool isOn)
    {
        if (isOn)
        {
            if (subtitleButtons.Count <= 0)
                CreateSubtitleButtons();
            else TurnSubtotleButtons(isOn);
        }
        else TurnSubtotleButtons(isOn);
    }

    /// <summary>
    /// 한번도 생성한 적 없으면 생성하고 켜줌
    /// </summary>
    private void CreateSubtitleButtons()
    {
        for (int i = 0; i < Info.Subtitles.Count; i++)
        {
            GameObject createButton = Instantiate(buttonObj);
            createButton.SetActive(true);
            createButton.GetComponent<StageLetterSubtitleButtonInfo>().SetButton(Info.Subtitles[i]);
            createButton.transform.SetParent(buttonParent);
            subtitleButtons.Add(createButton);
        }
    }

    /// <summary>
    /// 이미 생성한적 있다면 끄거나 켜주는 함수
    /// </summary>
    /// <param name="isTurn"></param>
    private void TurnSubtotleButtons(bool isTurn)
    {
        for (int i = 0; i < subtitleButtons.Count; i++)
        {
            subtitleButtons[i].SetActive(isTurn);
        }
    }

    
}
