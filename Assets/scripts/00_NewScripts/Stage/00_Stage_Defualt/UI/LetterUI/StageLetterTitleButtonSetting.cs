﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageLetterTitleButtonSetting : MonoBehaviour
{
    [Header("타이틀 분리해서 놓은 클래스")]
    public StageLetterTitleDivision titles;

    [Header("타이틀 버튼 오브젝트")]
    public GameObject titleButton;
    [Header("타이틀 버튼 부모")]
    public Transform buttonParent;

    [Header("서브 타이틀 오브젝트")]
    public GameObject subtitleButton;
    [Header("서브 타이틀 부모")]
    public Transform subtitleParent;

    private List<StageLetterTitleButtonInfo> titleInfos = new List<StageLetterTitleButtonInfo>();

    private void Start()
    {
        CreateButtons();
    }

    private void CreateButtons()
    {
        for (int i = 0; i < titles.Letters.Count; i++)
        {
            CreateButton(i);
        }
    }

    /// <summary>
    /// 버튼 만드는 함수
    /// </summary>
    private void CreateButton(int index)
    {
        GameObject button = Instantiate(titleButton);
        button.SetActive(true);
        button.transform.SetParent(buttonParent);
        button.GetComponent<RectTransform>().localScale = Vector3.one;
        StageLetterTitleButtonInfo titleInfo = button.GetComponent<StageLetterTitleButtonInfo>();
        titleInfo.SetInfo(titles.Letters[index], subtitleButton, subtitleParent);
        titleInfos.Add(titleInfo);
        if(index == 0)
        {
            PlayTitleButtonAction(titleInfo);
        }
    }

    /// <summary>
    /// 버튼 누를때 모든 소제목 꺼주고 누른것만 켜줌
    /// </summary>
    /// <param name="pressButton"></param>
    public void PlayTitleButtonAction(StageLetterTitleButtonInfo pressButton)
    {
        for (int i = 0; i < titleInfos.Count; i++)
            titleInfos[i].PlayerButtonAction(false);
        pressButton.PlayerButtonAction(true);
    }
}
