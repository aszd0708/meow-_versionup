﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// 편지 인포들 (타이틀별로 모아둠)
/// </summary>
[Serializable]
public class LetterInfo
{
    private int titleID;
    private string title;
    private List<LetterSubtitleInfo> subtitles = new List<LetterSubtitleInfo>();

    /// <summary>
    /// 타이틀 ID 첫번째 기준
    /// </summary>
    public int TitleID { get => titleID; set => titleID = value; }

    /// <summary>
    /// 타이틀 이름
    /// </summary>
    public string Title { get => title; set => title = value; }

    /// <summary>
    /// 이 타이틀 안에 있는 소제목들
    /// </summary>
    public List<LetterSubtitleInfo> Subtitles { get => subtitles; set => subtitles = value; }

    /// <summary>
    /// 생성자
    /// </summary>
    /// <param name="_titleID"></param>
    /// <param name="_subtitles"></param>
    public LetterInfo(int _titleID, string _title, List<LetterSubtitleInfo> _subtitles)
    {
        TitleID = _titleID;
        Title = _title;
        Subtitles = _subtitles;
    }
}

/// <summary>
/// 큰 제목 안에 소제목들과 소제목 id
/// </summary>
[Serializable]
public class LetterSubtitleInfo
{
    private int subtitleID;
    private string subtitle;
    private string contents;

    /// <summary>
    /// 소제목 ID
    /// </summary>
    public int SubtitleID { get => subtitleID; set => subtitleID = value; }

    /// <summary>
    /// 소제목 이름
    /// </summary>
    public string Subtitle { get => subtitle; set => subtitle = value; }

    /// <summary>
    /// 소제목 안에 내용
    /// </summary>
    public string Contents 
    { 
        get => contents;
        set => contents = value; 
    }

    /// <summary>
    /// 안에 요소들 설정 내용은 알아서 들어감
    /// </summary>
    /// <param name="_subtitleID">소제목 ID</param>
    /// <param name="_subtitle">소제목 이름</param>
    public LetterSubtitleInfo(int _subtitleID, string _subtitle)
    {
        SubtitleID = _subtitleID;
        Subtitle = _subtitle;
        Contents = LetterJsonData.Instance.DataDiction[SubtitleID].Text;
    }
}

public class StageLetterTitleDivision : Singleton<StageLetterTitleDivision>
{
    private List<LetterInfo> letters = new List<LetterInfo>();

    /// <summary>
    /// 타이틀 별로 정리해 놓은 리스트
    /// </summary>
    public List<LetterInfo> Letters { get => letters; set => letters = value; }

    private Dictionary<int, StageLetterSubtitleButtonInfo> subtitleDiction = new Dictionary<int, StageLetterSubtitleButtonInfo>();

    /// <summary>
    /// 소제목 인포만 모아놓은 딕션
    /// </summary>
    public Dictionary<int, StageLetterSubtitleButtonInfo> SubtitleDiction { get => subtitleDiction; set => subtitleDiction = value; }


    [SerializeField]
    private RectTransform letterPanel;
    private bool bIsSetLetters = false;

    private void OnEnable()
    {
        if(bIsSetLetters == false)
        {
            SetLetters();
            bIsSetLetters = true;
            letterPanel.localScale = Vector3.zero;
            letterPanel.gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Json 데이터 검사해서 같은 제목이면 모아서 넣는 함수
    /// </summary>
    /// 
    //private void SetLetters()
    //{
    //    List<LetterJson> json = LetterJsonData.Instance.JsonData;

    //    List<int> letterIdIndex = new List<int>();
    //    Dictionary<string, List<int>> letterDic = new Dictionary<string, List<int>>();
    //    List<string> keys = new List<string>();
    //    string prevString = null;
    //    for (int i = 0; i < json.Count; i++)
    //    {
    //        if (letterDic[json[i].Title] == null)
    //        {
    //            letterDic[json[i].Title] = new List<int>();
    //            keys.Add(json[i].Title);
    //        }
    //        letterDic[json[i].Title].Add(json[i].Id);
    //    }

    //    for (int i = 0; i < keys.Count; i++)
    //    {
    //        Letters.Add(GetLetter(letterIdIndex));
    //    }
    //}

    private void SetLetters()
    {
        List<LetterJson> json = LetterJsonData.Instance.JsonData;

        List<int> letterIdIndex = new List<int>();
        string prevString = null;
        for (int i = 0; i < json.Count; i++)
        {
            if (string.IsNullOrEmpty(prevString))
            {
                prevString = json[i].Title;
                letterIdIndex.Add(json[i].Id);
                continue;
            }
            else
            {
                if (string.Equals(prevString, json[i].Title))
                {
                    letterIdIndex.Add(json[i].Id);
                    continue;
                }

                else
                {
                    Letters.Add(GetLetter(letterIdIndex));
                    letterIdIndex.Clear();

                    prevString = json[i].Title;
                    letterIdIndex.Add(json[i].Id);
                    continue;
                }
            }
        }

        Letters.Add(GetLetter(letterIdIndex));
    }

    /// <summary>
    /// 아이디를 검색해서 그거에 맞는 부제목을 갖고오는 함수
    /// </summary>
    /// <param name="letterIDs"></param>
    /// <returns></returns>
    private LetterInfo GetLetter(List<int> letterIDs)
    {
        List<LetterSubtitleInfo> infos = new List<LetterSubtitleInfo>();
        for(int i = 0; i < letterIDs.Count; i++)
        {
            string subtitle = LetterJsonData.Instance.DataDiction[letterIDs[i]].SubTitle;
            LetterSubtitleInfo info = new LetterSubtitleInfo(letterIDs[i], subtitle);
            infos.Add(info);
        }

        string title = LetterJsonData.Instance.DataDiction[letterIDs[0]].Title;
        LetterInfo letter = new LetterInfo(letterIDs[0], title, infos);
        return letter;
    }

    /// <summary>
    /// 서브 타이틀의 버튼의 수집 값을 갖고오고 켜줌
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public StageLetterSubtitleButtonInfo GetSubtitleButtonInfo(int id)
    {
        if (SubtitleDiction[id] != null)
            return SubtitleDiction[id];
        else return null;
    }
}
