﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage3ItemBone : StageItemBase
{
    [SerializeField]
    private Transform dropPose;

    private bool canTouch = false;

    private void Start()
    {
        spriteRenderer.enabled = false;
        col.enabled = false;
    }

    public void DropEvent()
    {
        if(SaveDataManager.Instance.ItemDiction[ItemName] == true) { return; }
        spriteRenderer.enabled = true;
        col.enabled = true;
        spriteRenderer.sortingOrder = 4;
        transform.DOJump(dropPose.position, 2.0f, 1, 1.0f);
        canTouch = true;
    }

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if (SaveDataManager.Instance.ItemDiction[ItemName] == true) { return false; }
        if (canTouch)
        {
            base.DoTouch(touchPose);
            return false;
        }
        return false;
    }
}
