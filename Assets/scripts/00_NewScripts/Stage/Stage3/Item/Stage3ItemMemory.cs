﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage3ItemMemory : StageItemBase
{
    private bool canTouch;

    [SerializeField]
    [Header("점프 할 위치")]
    private Transform jumpTargetTransform;

    [Space(20)]
    [Header("점프에 관한 변수들")]

    [SerializeField]
    private float jumpPower = 0.2f;
    [SerializeField]
    private float jumpTime = 0.5f;

    private void Start()
    {
        spriteRenderer.enabled = false;
        canTouch = false;
    }
    public override bool DoTouch(Vector2 touchPose = default)
    {
        if(canTouch)
        {
            return base.DoTouch(touchPose);
        }
        else
        {
            return false;
        }
    }

    public void JumpToOutside()
    {
        if(!canTouch)
        {
            if(jumpTargetTransform != null)
            {
                StartCoroutine(_JumpToOutside());
            }
        }
    }

    private IEnumerator _JumpToOutside()
    {
        spriteRenderer.enabled = true;
        transform.DOJump(jumpTargetTransform.position, jumpPower, 1, jumpTime);
        yield return new WaitForSeconds(jumpTime);
        canTouch = true;
        yield break;
    }
}
