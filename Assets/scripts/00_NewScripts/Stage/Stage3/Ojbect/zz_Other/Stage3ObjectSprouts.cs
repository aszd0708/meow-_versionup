﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-04-09
 * 콩나물 대가리
 * 예아 대가리 예아
 */

public class Stage3ObjectSprouts : MonoBehaviour, TouchableObj
{
    [Header("각 부분별 SpriteRenderer")]
    public SpriteRenderer head, stem, vinyl;

    [Header("각 부분별 Sprites")]
    public Sprite[] headSprites, stemSprites;

    [Header("대가리 Transform")]
    public Transform headTransform;

    [Header("대가리 위치들")]
    public Vector3[] headPoses;

    public enum State { CAP, FIRST, SECOND, THIRD, FOURTH, FIFTH }

    private State STATE;
    private int headIndex = 0, stemIndex = 0, headPosIndex = 0;

    public int HeadIndex 
    { 
        get => headIndex;
        set
        {
            if (value >= headSprites.Length)
                value = headSprites.Length - 1;
            headIndex = value; 
        }
    }

    public int StemIndex
    {
        get => stemIndex;
        set
        {
            if (value >= stemSprites.Length)
                value = stemSprites.Length - 1;
            stemIndex = value;
        }
    }

    public int HeadPosIndex
    {
        get => headPosIndex;
        set
        {
            if (value >= headPoses.Length)
                value = headPoses.Length - 1;
            headPosIndex = value;
        }
    }

    public void TouchEvent()
    {
        switch (STATE)
        {
            case State.CAP:
                vinyl.enabled = false;
                STATE++;
                break;

            case State.FIRST:
                head.sprite = headSprites[HeadIndex++];
                STATE++;
                break;

            case State.SECOND:
            case State.THIRD:
                head.sprite = headSprites[HeadIndex++];
                headTransform.localPosition = headPoses[HeadPosIndex++];

                stem.enabled = true;
                stem.sprite = stemSprites[StemIndex++];

                STATE++;
                break;

            case State.FOURTH:
            case State.FIFTH:
                headTransform.localPosition = headPoses[HeadPosIndex++];

                stem.transform.localScale = new Vector3(1, 1.5f, 0);

                STATE++;
                break;

            default:
                return;
        }
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        TouchEvent();
        return false;
    }
}
