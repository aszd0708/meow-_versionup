﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage3ObjectEnvironmentalcleanser : MonoBehaviour, TouchableObj
{
    [SerializeField]
    [Header("움직이는 속도")]
    private float moveSpeed;

    [SerializeField]
    [Header("도착 X축")]
    private float destinationXPose;

    [SerializeField]
    [Header("바퀴 Transform")]
    private Transform wheelTransform;
    [SerializeField]
    [Header("바퀴 굴러가는 속도")]
    private float wheelRotateSpeed;

    private float nowWheelRotateSpeed;

    [Header("처음 시작과 끝의 Transform")]
    private Transform startPose;
    private Transform endPose;

    [SerializeField]
    [Header("쓰레기 봉투들")]
    private GameObject[] trashObjects;

    public bool DoTouch(Vector2 touchPose = default)
    {
        return false;
    }

    private void Start()
    {
        SetTrashes();
    }

    private void Update()
    {
        Movement();
    }

    public void SetPose(Transform _startPose, Transform _endPose)
    {
        startPose = _startPose;
        endPose = _endPose;
        SetMovement();
    }

    private void SetMovement()
    {
        float distance = startPose.position.x - endPose.position.x;
        // 왼쪽 -> 오른쪽 +
        // 오른쪽 -> 왼쪽 -

        if(distance < 0)
        {
            transform.rotation = Quaternion.Euler(Vector3.zero * 180);
            if(wheelRotateSpeed >= 0)
            {
                wheelRotateSpeed *= -1;
            }

            if(moveSpeed <= 0)
            {
                moveSpeed *= -1;
            }
        }

        else
        {
            transform.rotation = Quaternion.Euler(Vector3.up * 180);
            if (wheelRotateSpeed <= 0)
            {
                wheelRotateSpeed *= -1;
            }

            if (moveSpeed >= 0)
            {
                moveSpeed *= -1;
            }
        }

        transform.position = startPose.position;
    }

    private void Movement()
    {
        nowWheelRotateSpeed -= wheelRotateSpeed * Time.deltaTime;
        nowWheelRotateSpeed %= 360;
        wheelTransform.rotation = Quaternion.Euler(0, 0, nowWheelRotateSpeed);

        transform.position += Vector3.right * moveSpeed * Time.deltaTime;

        float distance = Vector3.Distance(transform.position, endPose.position);
        if(distance <= 10.0f)
        {
            PoolingManager.Instance.SetPool(gameObject, "Environmentalcleanser");
        }
    }

    private void SetTrashes()
    {
        for(int i = 0; i < trashObjects.Length; i++)
        {
            int randomNumber = Random.Range(0, 2);
            if(randomNumber == 1)
            {
                trashObjects[i].SetActive(true);
            }

            else
            {
                trashObjects[i].SetActive(false);
            }
        }
    }

    public void SetSpriteLayer(Transform parentTransform, string layer)
    {
        SpriteRenderer sprite = parentTransform.GetComponent<SpriteRenderer>();
        if(sprite != null)
        {
            sprite.sortingLayerName = layer;
        }
        if(parentTransform.childCount != 0)
        {
            for (int i = 0; i < parentTransform.childCount; i++)
            {
                SetSpriteLayer(parentTransform.GetChild(i), layer);
            }
        }
    }
}
