﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage3ObjectEnvironmentalcleanserCreater : MonoBehaviour
{
    [System.Serializable]
    public struct MovementInfo
    {
        public Transform startPose;
        public Transform endPose;
        public string layerName;
    }

    [SerializeField]
    [Header("생성할 오브젝트")]
    private GameObject cleanserObject;

    [SerializeField]
    [Header("시작 끝 Transform")]
    private MovementInfo[] movementInfos;

    [SerializeField]
    [Header("나오는 시간(사이 시간을 랜덤으로 돌릴것)")]
    private float[] createTime = new float[2];

    public void Start()
    {
        CreateCleanser();
    }

    private IEnumerator _CreateCleanser(MovementInfo movementInfo)
    {
        while(true)
        {
            GameObject createObject = PoolingManager.Instance.GetPool("Environmentalcleanser");
            if(createObject == null)
            {
                createObject = Instantiate(cleanserObject);
            }
            if(movementInfo.startPose != null && movementInfo.endPose != null)
            {
                Stage3ObjectEnvironmentalcleanser obj = createObject.GetComponent<Stage3ObjectEnvironmentalcleanser>();
                obj.SetPose(movementInfo.startPose, movementInfo.endPose);
                obj.SetSpriteLayer(obj.transform, movementInfo.layerName);
            }
            yield return new WaitForSeconds(Random.Range(createTime[0], createTime[1]));
        }
    }

    private void CreateCleanser()
    {
        for(int i = 0; i < movementInfos.Length; i++)
        {
            StartCoroutine(_CreateCleanser(movementInfos[i]));
        }
    }
}
