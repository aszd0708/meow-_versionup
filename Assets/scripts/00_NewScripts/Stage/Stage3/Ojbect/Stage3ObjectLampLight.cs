﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage3ObjectLampLight : MonoBehaviour, TouchableObj
{
    [SerializeField]
    [Header("라이트")]
    private HardLight2D lampLight;

    [Header("밝기 수치")]
    [SerializeField]
    private float minLight;
    [SerializeField]
    private float maxLight;

    [SerializeField]
    [Header("껐다 키는데 시간")]
    private float offTime;
    [SerializeField]
    private float onTime;

    [SerializeField]
    [Header("체크하면 시작할때 랜덤으로 켜줌")]
    private bool isRandomLight;

    [SerializeField]
    [Header("체크하면 깜빡깜빡 하면서 켜짐")]
    private bool isBroken;

    private bool canTouch = true;

    private bool isLighting;

    public virtual bool DoTouch(Vector2 touchPose = default)
    {
        if (canTouch)
        {
            PlayTouchEvent();
        }
        return false;
    }

    private void Start()
    {
        if (isRandomLight)
        {
            int randomIndex = Random.Range(0, 2);
            switch (randomIndex)
            {
                case 0:
                    lampLight.Intensity = minLight;
                    isLighting = false;
                    break;
                case 1:
                    lampLight.Intensity = maxLight;
                    isLighting = true;
                    break;
                default:
                    break;
            }
        }
    }

    private void PlayTouchEvent()
    {
        StartCoroutine(_PlayTouchEvent());
    }

    private IEnumerator _PlayTouchEvent()
    {
        canTouch = false;
        if(isBroken)
        {
            if(isLighting)
            {
                yield return StartCoroutine(_SetBrokenLightPower(maxLight, minLight));
            }
            else
            {
                yield return StartCoroutine(_SetBrokenLightPower(minLight, maxLight));
            }
        }
        else
        {
            if (isLighting)
            {
                yield return StartCoroutine(_SetLightPower(maxLight, minLight, offTime));
            }
            else
            {
                yield return StartCoroutine(_SetLightPower(minLight, maxLight, onTime));
            }
        }
        canTouch = true;
        isLighting = !isLighting;
        yield break;
    }

    private IEnumerator _SetLightPower(float startValue, float endValue, float duringTime)
    {
        float currentValue = startValue;
        float interval = (endValue - startValue) / duringTime;
        float currentTime = 0;
        while (currentTime < duringTime)
        {
            currentTime += Time.deltaTime;
            currentValue += interval * Time.deltaTime;
            lampLight.Intensity = currentValue;
            yield return null;
        }
        lampLight.Intensity = endValue;
        yield break;
    }

    private IEnumerator _SetBrokenLightPower(float startValue, float endValue)
    {
        WaitForSeconds wait = new WaitForSeconds(Random.Range(0.05f, 0.1f));
        int randomCount = Random.Range(1, 4);
        for (int i = 0; i < randomCount; i++)
        {
            lampLight.Intensity = endValue;
            yield return wait;
            lampLight.Intensity = startValue;
            yield return wait;
        }
        yield return wait;
        float randomWait = Random.Range(1f, 2f);
        lampLight.Intensity = endValue;

        yield break;
    }
}
