﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

/*
 * 2020-03-11
 * 어항 안에서 물고기가 움직이는 스크립트
 * 모든 움직이는 것들은 localPosition을 사용해서 움직임
 * 움직였다 정지했다 움직였다 정지했다 반복
 * 한 방향으로만 가다가 섰다가 반복을 할까
 * 아니면 그냥 아예 랜덤으로 할까
 */

public class Stage3ObjectFishMovement : MonoBehaviour
{
    [Header("어항 위 아래 양 옆 트랜스")]
    public Transform leftUp, rightDown;
    [Header("최대 움직이는 거리 x, y축")]
    public float maxX, minX, maxY, minY;
    [Header("최대소 움직이는 속도")]
    public float maxTime, minTime;
    [Header("최대소 쉬는 시간")]
    public float maxRestTime, minRestTime;
    [Header("헤엄치는 모션을 갖고 있는지 체크")]
    public bool haveMotion = false;

    private float upPos, downPos, leftPos, rightPos;

    private Vector3 movePos;

    private bool right = false;

    private Coroutine motionCor;

    private float defaultMaxTime, defaultMinTime, defaultMaxRestTime, defaultMinRestTime;

    public Coroutine MotionCor { get => motionCor; set => motionCor = value; }
    
    public void StartScript()
    {
        StartSetting();
        MotionCor = StartCoroutine(Move());
        SetDefaultSetting();
    }

    // 현재 LocalPosition에서 랜덤으로 위치를 뽑아주는 함수
    private Vector3 RandomPosition()
    {
        Vector3 nowPos = transform.localPosition;

        float xPos = RandomMinMax(minX, maxX);
        float yPos = RandomMinMax(minY, maxY);

        float xValue = nowPos.x + xPos;
        float yValue = nowPos.y + yPos;

        Vector3 randomPos = new Vector2(xValue, yValue);

        return randomPos;
    }

    private IEnumerator RandomPositionCor()
    {
        Vector3 randomPos = Vector3.zero;

        bool end = true;

        while (end)
        {
            randomPos = RandomPosition();

            if (randomPos.x > leftPos && randomPos.x < rightPos && randomPos.y > downPos && randomPos.y < upPos)
                end = false;
            yield return new WaitForSeconds(0.1f);
        }
        movePos = randomPos;
        yield return new WaitForSeconds(0.1f);
        yield break;
    }

    private IEnumerator Move()
    {
        while (true)
        {
            yield return StartCoroutine(RandomPositionCor());

            if (DoFlip(movePos)) transform.DORotate(new Vector3(0, 180, 0), 0.3f);
            else transform.DORotate(new Vector3(0, 0, 0), 0.3f);

            float moveTime = Random.Range(minTime, maxTime);
            transform.DOLocalMove(movePos, moveTime).SetEase(Ease.OutQuart);
            yield return new WaitForSeconds(moveTime);
            yield return new WaitForSeconds(Random.Range(minRestTime, maxRestTime));
        }
    }

    // 움직였을때 왼쪽을 보면 F 오른쪽을 보면 T
    private bool DoFlip(Vector3 movePos)
    {
        bool flip = true;

        float goPosGap = movePos.x - transform.localPosition.x;

        if (goPosGap < 0) flip = false;
        else flip = true;

        return flip;
    }

    private float RandomMinMax(float min, float max)
    {
        float value = Random.Range(min, max);
        int random = Random.Range(0, 2);
        if (random == 0) return -value;
        else return value;
    }

    private void StartSetting()
    {
        upPos = leftUp.localPosition.y;
        downPos = rightDown.localPosition.y;
        leftPos = leftUp.localPosition.x;
        rightPos = rightDown.localPosition.x;
    }

    public void MotionCoroutineController(bool start)
    {
        if (start) MotionCor = StartCoroutine(Move());
        else
        {
            StopCoroutine(MotionCor);
            transform.DOPause();
        }
    }

    public void SetTimes(float _minTime, float _maxTime, float _minRestTime, float _maxRestTime)
    {
        minTime = _minTime;
        maxTime = _maxTime;
        minRestTime = _minRestTime;
        maxRestTime = _maxRestTime;
    }

    private void SetDefaultSetting()
    {
        defaultMinTime = minTime;
        defaultMaxTime = maxTime;
        defaultMinRestTime = minRestTime;
        defaultMaxRestTime = maxRestTime;
    }

    public void ReturnDefaultSetting()
    {
        minTime = defaultMinTime;
        maxTime = defaultMaxTime;
        minRestTime = defaultMinRestTime;
        maxRestTime = defaultMaxRestTime;
    }
}
