﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-03-12
 * 움직이는 일반 물꼬기
 * 터치했을 때 움직이는 시간 퐉 줄여서 정신없이 움직이게 만들어주기
 * 근디 이거 어항마다로 할까 아니면 물고기마다로 할까??
 * 어항마다로 하면 cafish 도 손 봐야 할거 같음
 */

public class Stage3ObjectFishMove : MonoBehaviour, TouchableObj
{
    [Header("움직이는 시간(빠르게!!!)")]
    public float moveTime;
    [Header("움직이고 쉬는 시간(빠르게!!!!)")]
    public float restTime;
    [Header("터치 모션 유지 시간")]
    public float motionTime;

    private Stage3ObjectFishMovement move;

    private void Awake()
    {
        move = GetComponent<Stage3ObjectFishMovement>();
    }

    private IEnumerator SetTime()
    {
        move.SetTimes(moveTime, moveTime, restTime, restTime);
        yield return new WaitForSeconds(motionTime);
        move.ReturnDefaultSetting();
        yield break;
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        StartCoroutine(SetTime());
        return false;
    }
}
