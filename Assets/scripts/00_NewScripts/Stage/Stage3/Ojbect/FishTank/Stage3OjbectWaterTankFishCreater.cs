﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-03-12
 * 수조에 물꼬기를 풀어놓는 함수
 * 물꼬기를 풀어라!!!!
 */

public class Stage3OjbectWaterTankFishCreater : MonoBehaviour
{
    [System.Serializable]
    public struct Fish
    {
        [Header("물꼬기 프리팹")]
        public GameObject fishPrefeb;
        [Header("물꼬기를 몇마리씩 랜덤으로 만들것인지")]
        public int minCount, maxCount;
        [Header("움직이는 물꼬기냐 아니냐")]
        public bool isMove;
    }
    [Header("물꼬기 인포 리스트")]
    public List<Fish> fishInfo = new List<Fish>();
    [Header("물꼬기 만들었을때 이동 반경(움직이는 물꼬기만 줌)")]
    public Transform leftUp, rightDown;
    [Header("물꼬기가 생성되는 위치(여기서 랜덤으로 뿌려줌)")]
    public Transform createPos;
    [Header("물꼬기 부모")]
    public Transform fishParent;
    [Header("스프라이트 랜더러 컨트롤 해줄 변수")]
    public SpriteMaskInteraction maskKind;
    public string layerName;

    private void Start()
    {
        Create(fishInfo[Random.Range(0, fishInfo.Count)]);
    }

    private void Create(Fish fish)
    {
        List<GameObject> fishList = new List<GameObject>();
        int randomCount = Random.Range(fish.minCount, fish.maxCount + 1);
        for(int i = 0; i < randomCount; i++)
        {
            fishList.Add(Instantiate(fish.fishPrefeb));
            fishList[i].transform.SetParent(fishParent);
            fishList[i].transform.localPosition = RandomCreatePos();

            if(fish.isMove)
            {
                Stage3ObjectFishMovement fishMove = fishList[i].GetComponent<Stage3ObjectFishMovement>();
                fishMove.leftUp = leftUp;
                fishMove.rightDown = rightDown;
                fishMove.StartScript();
            }

            SpriteRenderer fishSprite = fishList[i].GetComponent<SpriteRenderer>();
            fishSprite.maskInteraction = maskKind;
            fishSprite.sortingLayerName = layerName;
        }
        GetComponent<SpriteRenderer>().sortingLayerName = layerName;
    }

    private Vector3 RandomCreatePos()
    {
        Vector3 position = createPos.localPosition;

        float x = Random.Range(position.x - 1, position.x + 1);
        float y = Random.Range(position.y - 0.5f, position.y + 0.5f);

        return new Vector3(x, y, 0);
    }
}
