﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage3ObjectWaterTankTouch : MonoBehaviour, TouchableObj
{
    public Transform fishes;

    [Header("이거의 값은 안에 물고기 바꿀때 같이 바꿔줘야 함")]
    public float motionTime = 3;

    private List<TouchableObj> moveFishes = new List<TouchableObj>();

    private bool canTouch = true;


    private void Start()
    {
        Invoke("SettingList", 0.1f);
    }

    private void SettingList()
    {
        for (int i = 0; i < fishes.childCount; i++)
            moveFishes.Add(fishes.GetChild(i).GetComponent<TouchableObj>());
    }

    private IEnumerator Motion()
    {
        canTouch = false;
        for (int i = 0; i < moveFishes.Count; i++)
            moveFishes[i].DoTouch();
        yield return new WaitForSeconds(motionTime);
        canTouch = true;
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        if (canTouch)
            StartCoroutine(Motion());

        return false;
    }
}
