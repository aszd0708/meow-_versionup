﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class StageObjectInTrashCan : MonoBehaviour
{
    [SerializeField]
    private Sprite[] trashSprites;
    [SerializeField]
    private SpriteRenderer spriteRenderer;

    [SerializeField]
    private Transform dropPose;

    public void DropObject()
    {
        spriteRenderer.enabled = true;
        spriteRenderer.sprite = trashSprites[Random.Range(0, trashSprites.Length)];
        spriteRenderer.sortingOrder = 4;
        transform.DOJump(dropPose.position, 2.0f, 1, 1.0f);

        PoolingManager.Instance.SetPool(gameObject, "TrashInTrashCan", 5.0f);
    }
}
