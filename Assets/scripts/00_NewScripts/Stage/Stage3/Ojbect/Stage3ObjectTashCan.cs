﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Stage3ObjectTashCan : MonoBehaviour, TouchableObj
{
    private int nowTouchCount;
    [SerializeField]
    private int maxTouchCount = 3;

    [Header("뚜껑이 사라지면 나오는 이벤트")]
    public UnityEvent capEndEvent;

    [SerializeField]
    private Transform capTransform;

    [SerializeField]
    private Transform capDropPose;

    [SerializeField]
    private float capShakeTime;

    private bool canTouch = true;

    private void Start()
    {
        canTouch = true;
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        if(canTouch)
        {
            TouchEvnet();
        }
        return false;
    }

    private void TouchEvnet()
    {
        nowTouchCount++;
        if (nowTouchCount >= maxTouchCount)
        {
            StartCoroutine(_DropCap());
        }
        else
        {
            StartCoroutine(_ShakeCap());
        }
    }

    private IEnumerator _ShakeCap()
    {
        canTouch = false;
        capTransform.DOShakePosition(capShakeTime, 0.1f, 15, 30);
        yield return new WaitForSeconds(capShakeTime);
        canTouch = true;
        yield break;
    }

    private IEnumerator _DropCap()
    {
        canTouch = false;
        capTransform.DOJump(capDropPose.position, 2.0f, 1, 1.0f);
        yield return new WaitForSeconds(0.1f);
        capEndEvent.Invoke();
        yield return new WaitForSeconds(4.0f);
        capTransform.gameObject.SetActive(false);
        yield break;
    }
}
