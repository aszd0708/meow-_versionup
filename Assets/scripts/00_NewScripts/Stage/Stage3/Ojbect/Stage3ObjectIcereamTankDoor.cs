﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage3ObjectIcereamTankDoor : MonoBehaviour, TouchDownableObj
{
    public enum position
    { left, right }

    public position pos;
    public bool touch;
    public Transform other;
    private Collider2D otherCollider;

    private Vector2 originPos;
    private Vector2 nowPos;
    private float posZ;
    private Vector3 downPos;
    private Vector3 nPos;

    private BoxCollider2D objCollider;
    private Vector3 firstTouchPose;

    [SerializeField]
    private float xRange;

    [SerializeField]
    private int sortingOrder;

    [SerializeField]
    private SpriteRenderer render;

    private void Awake()
    {
        objCollider = GetComponent<BoxCollider2D>();
        otherCollider = other.GetComponent<Collider2D>();
    }

    void Start()
    {
        render.sortingOrder = 4;
        if (pos == position.left)
            posZ = -2;
        else if (pos == position.right)
            posZ = -1;

        nowPos = transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        DoSlide();
    }

    private void DoSlide()
    {
        if (touch)
        {
            if (Input.GetMouseButton(0))
            {
                if (other.localPosition.x < -xRange)
                {
                    objCollider.enabled = false;
                }
                else if (other.localPosition.x >= -xRange)
                {
                    otherCollider.enabled = true;
                    //nPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    nPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    if (pos == position.left)
                    {
                        transform.localPosition = new Vector3(
                            transform.localPosition.x + (downPos.x - nPos.x), 
                            transform.localPosition.y, posZ);
                    }
                    else if (pos == position.right)
                    {
                        transform.localPosition = new Vector3(
                            transform.localPosition.x -(downPos.x - nPos.x), 
                            transform.localPosition.y, posZ);
                    }
                    downPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                }
            }

            else if (Input.GetMouseButtonUp(0))
            {
                touch = false;
                StageTouchManagerBase.Instance.ForbitTouch(true);
            }
            transform.localPosition = new Vector3(Mathf.Clamp(transform.localPosition.x, originPos.x - xRange, originPos.x), nowPos.y, posZ);
        }
    }

    public bool DoTouchDown(Vector2 touchPose = default)
    {
        StageTouchManagerBase.Instance.ForbitTouch(false);
        firstTouchPose = touchPose;
        touch = true;
        nPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        return false;
    }
}
