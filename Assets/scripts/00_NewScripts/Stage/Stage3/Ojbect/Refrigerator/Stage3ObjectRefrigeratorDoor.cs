﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/*
 * 2020-04-28
 * 보턴을 누를때 마다 안에 내용물의 갯수가 바뀌는 스크립트
 */

public class Stage3ObjectRefrigeratorDoor : MonoBehaviour, TouchableObj
{
    [Header("RefrigeratorFishMeatCreater 스크립트의 변수를 잘 보고 값 넣기")]
    public int refrigeratorIndex;
    [Header("랭장고 따꿍")]
    public List<Sprite> sprite = new List<Sprite>();

    private Stage3ObjectRefrigeratorFishMeatCreater creater;
    private SpriteRenderer spriteRenderer;
    private bool open = false;
    private int index = 1;

    [SerializeField]
    private UnityEvent openEvent;
    [SerializeField]
    private UnityEvent closeEvent;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        creater = transform.parent.GetComponent<Stage3ObjectRefrigeratorFishMeatCreater>();
    }

    public void PlayOpenEvent()
    {
        spriteRenderer.sprite = sprite[index++];
        index %= sprite.Count;
        open = !open;
        if (open)
        {
            if(refrigeratorIndex != -10)
            {
                creater.SetInsideObjs(refrigeratorIndex);
            }
            openEvent.Invoke();
        }
        else
        {
            closeEvent.Invoke();
        }
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        PlayOpenEvent();
        return false;
    }
}
