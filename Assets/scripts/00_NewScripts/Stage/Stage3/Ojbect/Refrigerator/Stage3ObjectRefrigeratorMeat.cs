﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage3ObjectRefrigeratorMeat : MonoBehaviour, TouchableObj
{
    [Header("고기(아이템) 콜라이더")]
    public Collider2D meatCollider;

    [Header("랭장고 따꿍")]
    public List<Sprite> sprite = new List<Sprite>();

    private SpriteRenderer spriteRenderer;
    private bool open = false;
    private int index = 1;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void PlayOpenEvent()
    {
        spriteRenderer.sprite = sprite[index++];
        index %= sprite.Count;
        open = !open;
        if (open)
        {
            meatCollider.enabled = true;
        }
        else
        {
            meatCollider.enabled = false;
        }
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        PlayOpenEvent();
        return false;
    }
}
