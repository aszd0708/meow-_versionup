﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-04-23
 * 냉장고에 따라서 생선을 넣을지 고기를 넣을지 정해서 만들어주는 함수
 * 
 */

public class Stage3ObjectRefrigeratorFishMeatCreater : MonoBehaviour
{
    public enum Kind { MEAT, FISH }
    [System.Serializable]
    public struct InRefigerator
    {
        public Sprite sprite;
    }

    [System.Serializable]
    public struct Refigerator
    {
        public Kind KIND;
        public Transform transform;
    }

    [Header("각 음식별 스프라트들")]
    public InRefigerator[] fishSpires;
    public InRefigerator[] meatSprites;

    [Header("각 위치별 어떰 음식을 넣을건지에 관한 리스트")]
    public List<Refigerator> refrigerators = new List<Refigerator>();

    [Header("안에 있는 내용물 전부 살아남는 확률")]
    public int persent;
    [Header("안에 있는 내용물 개별 살아남는 확률")]
    public int objPersent;

    private void Start()
    {
        for (int i = 0; i < refrigerators.Count; i++)
        {
            bool del = IntRandom.StateRandomInt(100 - persent);

            if (del) DelVariable(refrigerators[i]);
            else SetSprite(refrigerators[i]);
        }
    }

    private void SetSprite(Refigerator refigerator)
    {
        switch (refigerator.KIND)
        {
            case Kind.FISH:
                for (int i = 0; i < refigerator.transform.childCount; i++)
                {
                    bool objLive = IntRandom.StateRandomInt(objPersent);
                    SpriteRenderer spriteRenderer = refigerator.transform.GetChild(i).GetComponent<SpriteRenderer>();
                    
                    spriteRenderer.enabled = objLive;
                    if (objLive)
                        spriteRenderer.sprite = fishSpires[Random.Range(0, fishSpires.Length)].sprite;
                }
                break;
            case Kind.MEAT:
                for (int i = 0; i < refigerator.transform.childCount; i++)
                {
                    bool objLive = IntRandom.StateRandomInt(objPersent);
                    SpriteRenderer spriteRenderer = refigerator.transform.GetChild(i).GetComponent<SpriteRenderer>();
                    spriteRenderer.enabled = objLive;
                    if (objLive)
                        spriteRenderer.sprite = meatSprites[Random.Range(0, meatSprites.Length)].sprite;
                }
                break;
        }
    }

    private void DelVariable(Refigerator refigerator)
    {
        for (int i = 0; i < refigerator.transform.childCount; i++)
            refigerator.transform.GetChild(i).GetComponent<SpriteRenderer>().enabled = false;
    }

    public void SetInsideObjs(int index)
    {
        bool del = IntRandom.StateRandomInt(100 - persent);

        if (del) DelVariable(refrigerators[index]);
        else SetSprite(refrigerators[index]);
    }
}
