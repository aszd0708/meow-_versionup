﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-04-22
 * 세로로 긴 랭장고에 과일이나 채소 만들어내는 그런 스크립트
 */

public class Stage3ObjectRefrigeratorFoodCreater : MonoBehaviour
{
    [System.Serializable]
    public struct FruitSprite
    {
        public Sprite[] sprites;
    }

    [Header("랭장고에 넣을 과일 스프라이트들")]
    public FruitSprite[] fruits;

    [Header("각 층 Transform")]
    public List<Transform> floors = new List<Transform>();

    [Header("각 층이 살아남을 확률")]
    public int persent;
    [Header("각 오브젝트가 살아남을 확률")]
    public int fruitPercent;

    public bool rot;

    private void Start()
    {
        for (int i = 0; i < floors.Count; i++)
        {
            bool del = IntRandom.StateRandomInt(100 - persent);

            if (del)
                DelSpriteRenderer(i);
            else
                ChangeSprite(fruits[Random.Range(0, fruits.Length)], i);
        }
    }

    // 트렌스폼 리스트에서 살아남은 애들끼리 돌리는 스프라이트
    private void ChangeSprite(FruitSprite fruit, int index)
    {
        for (int i = 0; i < floors[index].childCount; i++)
        {
            bool fruitLive = IntRandom.StateRandomInt(100 - fruitPercent);
            SpriteRenderer objSpriteRenderer = floors[index].GetChild(i).GetComponent<SpriteRenderer>();
            objSpriteRenderer.enabled = !fruitLive;
            if (!fruitLive)
            {
                int randomSpriteIndex = Random.Range(0, fruit.sprites.Length);
                GameObject fruitPtr = floors[index].GetChild(i).gameObject;
                if (rot)
                    fruitPtr.transform.rotation = Quaternion.Euler(0, 0, Random.Range(0f, 360f));
                objSpriteRenderer.sprite = fruit.sprites[randomSpriteIndex];
            }
        }
    }
    private void DelSpriteRenderer(int index)
    {
        for (int i = 0; i < floors[index].childCount; i++)
        {
            SpriteRenderer fruitPtr = floors[index].GetChild(i).GetComponent<SpriteRenderer>();
            fruitPtr.enabled = false;
        }
    }

    public void SetInsideObjs(int index)
    {
        bool del = IntRandom.StateRandomInt(100 - persent);

        if (del)
            DelSpriteRenderer(index);
        else
            ChangeSprite(fruits[Random.Range(0, fruits.Length)], index);
    }
}
