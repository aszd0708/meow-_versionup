﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage3ObjectRadishes : MonoBehaviour, TouchableObj
{
    [SerializeField]
    [Header("무 순서대로 넣기 0번째가 처음으로")]
    private List<GameObject> radishes = new List<GameObject>();

    public bool DoTouch(Vector2 touchPose = default)
    {
        PlayTouchEvent();
        return false;
    }

    private void PlayTouchEvent()
    {
        GameObject obj = radishes[0];
        radishes.RemoveAt(0);

        PoolingManager.Instance.SetPool(obj, "RadishesTrash");

        if(radishes.Count == 0)
        {
            radishes.Clear();
            gameObject.SetActive(false);
        }
    }
}
