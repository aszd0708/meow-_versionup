﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage3ObjectCabages : MonoBehaviour, TouchableObj
{    
    [SerializeField]
    [Header("각 층 부모 0번째가 1층")]
    private Transform[] floors;

    private int pointer = 0;
    public bool DoTouch(Vector2 touchPose = default)
    {
        PlayTouchEvent();
        return false;
    }

    private void PlayTouchEvent()
    {
        GameObject cabage = floors[pointer].GetChild(Random.Range(0, floors[pointer].childCount)).gameObject;
        PoolingManager.Instance.SetPool(cabage, "cabage");
        if(floors[pointer].childCount == 0)
        {
            pointer++;
            if (pointer >= floors.Length)
            {
                gameObject.SetActive(false);
            }
        }
    }
}
