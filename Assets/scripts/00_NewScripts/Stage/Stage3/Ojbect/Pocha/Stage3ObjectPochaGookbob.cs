﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage3ObjectPochaGookbob : MonoBehaviour
{
    public Stage3ObjectPochaFire fire;

    private void Start()
    {
        bool random = RandomBool();
        if (!random && fire.play)
            fire.OnClick();
        gameObject.SetActive(random);

        if(gameObject.activeSelf)
            StartCoroutine(Motion());
    }

    private IEnumerator Motion()
    {
        while (true)
        {
            if (!fire.play)
            {
                transform.DOShakeRotation(0.25f, 10f, 3);
            }

            yield return new WaitForSeconds(0.25f);
        }
    }

    private bool RandomBool()
    {
        int random = Random.Range(0, 2);
        if (random == 0) return false;
        else return true;
    }
}
