﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-04-27
 * 사람 스프라이트 랜더러 레이어 이름 셋팅 해주는 스크립트
 * 이름을 null로 설정하면 아무것도 안함
 */

public class Stage3ObjectPochaPersonSortingLayer : MonoBehaviour
{
    public List<SpriteRenderer> sprites = new List<SpriteRenderer>();

    [Header("이름을 null로 설정하면 안바꿈")]
    public string layerName = "null";

    private void Start()
    {
        if (layerName == "null")
            return;
        SettingLayerName(layerName);
    }

    private void SettingLayerName(string layerName)
    {
        for (int i = 0; i < sprites.Count; i++)
            sprites[i].sortingLayerName = layerName;
    }
}
