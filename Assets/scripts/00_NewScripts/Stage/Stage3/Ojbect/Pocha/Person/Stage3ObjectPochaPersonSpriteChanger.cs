﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-02-24 
 * 스테이지3에 있는 사람들의 얼굴과 몸을 랜덤으로 스프라이트를 변경해주는 스크립트
 */

public class Stage3ObjectPochaPersonSpriteChanger : MonoBehaviour
{
    public SpriteRenderer headSpriteRenderer;
    public SpriteRenderer armSpriteRenderer;
    public SpriteRenderer bodySpriteRenderer;

    public List<Sprite> headSprite = new List<Sprite>();

    [Header("Arm과 Head의 갯수와 위치를 같게 맞춰야 함")]
    public List<Sprite> armSprite = new List<Sprite>();
    public List<Sprite> bodySprite = new List<Sprite>();
    public List<Sprite> noArmBodySprite = new List<Sprite>();

    // Start is called before the first frame update
    void Start()
    {
        if (armSprite.Count != bodySprite.Count)
        {
            Debug.LogError("에러에러 숫자가 안맞음");
            return;
        }

        bool haveArm = IntRandom.StateRandomInt();
        
        if (armSprite.Count == 0)
            haveArm = false;

        if (haveArm)
        {
            // 여기 팔 있는거
            int random = Random.Range(0, bodySprite.Count);
            RandomSprite(armSpriteRenderer, armSprite, random);
            RandomSprite(bodySpriteRenderer, bodySprite, random);
        }

        else
        {
            // 여기 팔 없는거 생성
            RandomSprite(bodySpriteRenderer, noArmBodySprite);
            armSpriteRenderer.gameObject.SetActive(false);
        }

        RandomSprite(headSpriteRenderer, headSprite);
        SendMessage("SetHaveArm", haveArm);
    }

    private void RandomSprite(SpriteRenderer spriteComponent, List<Sprite> sprites)
    {
        int random = Random.Range(0, sprites.Count);
        spriteComponent.sprite = sprites[random];
    }

    private void RandomSprite(SpriteRenderer spriteComponent, List<Sprite> sprites, int random)
    {
        spriteComponent.sprite = sprites[random];
    }
}
