﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-03-05
 * 위치 별 오브젝트를 만들어서 레이어 설정 해줌
 * 랜덤으로 0~2개중 만들것을 정해서 만듦
 * 모든 만들어지는 사람들은 머리 -> 몸 -> 팔 순임
 * 머리 -> 몸 -> 팔
 */
public class Stage3ObjectPochaPeopleCreater : MonoBehaviour
{
    public enum Position
    {
        UP, DOWN, LEFT, RIGH
    }
    public Position POS;

    [Header("오른쪽 사람은 Y축 180 회전")]
    [Header("사람 프리팹 순서는 머리 -> 몸 -> 팔 순서")]
    public GameObject upPerson;
    public GameObject sidePerson;
    public GameObject downPerson;

    [Header("사람이 있는 포지션 설정 뒤 -> 앞 순")]
    public Transform[] personPosition = new Transform[2];

    [Header("레이어 넘버 설정")]
    public int layer;

    private void Start()
    {
        PositionSpriteSetting();
    }

    private void PositionSpriteSetting()
    {
        switch (POS)
        {
            case Position.UP: UpPositionSpriteSetting(); break;
            case Position.DOWN: DownPositionSpriteSetting(); break;
            case Position.LEFT: LeftPositionSpriteSetting(); break;
            case Position.RIGH: RightPositionSpriteSetting(); break;
            default: break;
        }
    }

    // 위에 있는 사람을 생성 및 포지션 설정 해주는 함수
    private void UpPositionSpriteSetting()
    {
        List<GameObject> person = new List<GameObject>();
        person.Add(ObjCreater(upPerson)); person.Add(ObjCreater(upPerson));

        for (int i = 0; i < person.Count; i++)
        {
            if (person[i] == null)
                continue;
            else
            {
                SettingLayerOrder(layer, person[i]);
                   // Transform 설정
                   person[i].transform.parent = transform;
                person[i].transform.position = personPosition[i].position;

                // 스프라이트 설정
                List<SpriteRenderer> personSprite = GetSpriteRenderers(person[i].transform);
                personSprite[0].sortingOrder = 1; // 머리
                personSprite[1].sortingOrder = 0; // 몸
            }
        }
    }

    private void DownPositionSpriteSetting()
    {
        GameObject left = ObjCreater(downPerson);
        GameObject right = ObjCreater(downPerson);

        if (left != null)
        {
            SettingTransform(left.transform, personPosition[0]);
        }
        if (right != null)
        {
            SettingTransform(right.transform, personPosition[1]);
        }

        SettingPosition(left, 17);
        SettingPosition(right, 14);
    }

    private void LeftPositionSpriteSetting()
    {
        GameObject front = ObjCreater(sidePerson);
        GameObject back = ObjCreater(sidePerson);

        if (front != null)
        {
            List<SpriteRenderer> personSprite = GetSpriteRenderers(front.transform);
            SettingTransform(front.transform, personPosition[1]);

            personSprite[0].sortingOrder = 12;
            personSprite[1].sortingOrder = 11;
            personSprite[2].sortingOrder = 13;
        }

        if (back != null)
        {
            List<SpriteRenderer> personSprite = GetSpriteRenderers(back.transform);
            SettingTransform(back.transform, personPosition[0]);

            personSprite[0].sortingOrder = 9;
            personSprite[1].sortingOrder = 8;
            personSprite[2].sortingOrder = 10;
        }
    }

    private void RightPositionSpriteSetting()
    {
        GameObject front = ObjCreater(sidePerson);
        GameObject back = ObjCreater(sidePerson);

        if (front != null)
        {
            SettingTransform(front.transform, personPosition[1]);
            front.transform.rotation = Quaternion.Euler(0, 180, 0);
            List<SpriteRenderer> personSprite = GetSpriteRenderers(front.transform);

            personSprite[0].sortingOrder = 9;
            personSprite[1].sortingOrder = 4;
            if (personSprite[2] != null)
                personSprite[2].sortingOrder = 10;
        }

        if (back != null)
        {
            SettingTransform(back.transform, personPosition[0]);
            back.transform.rotation = Quaternion.Euler(0, 180, 0);
            List<SpriteRenderer> personSprite = GetSpriteRenderers(back.transform);

            personSprite[0].sortingOrder = 4;
            personSprite[1].sortingOrder = 3;
            if (personSprite[2] != null)
                personSprite[2].sortingOrder = 7;
        }
    }

    private void SettingPosition(GameObject obj, int index)
    {
        if (obj != null)
        {
            List<SpriteRenderer> personSprite = GetSpriteRenderers(obj.transform);
            personSprite[0].sortingOrder = index + 2;
            personSprite[1].sortingOrder = index + 1;
            if (personSprite[2] != null)
                personSprite[2].sortingOrder = index;
        }
    }

    private List<SpriteRenderer> GetSpriteRenderers(Transform obj)
    {
        List<SpriteRenderer> spriteRenderer = new List<SpriteRenderer>();

        for (int i = 0; i < obj.childCount; i++)
            spriteRenderer.Add(obj.GetChild(i).GetComponent<SpriteRenderer>());

        return spriteRenderer;
    }

    private GameObject ObjCreater(GameObject obj)
    {
        if (IntToBool()) return Instantiate(obj);
        else return null;
    }

    private bool IntToBool()
    {
        int random = Random.Range(0, 2);

        if (random == 0) return false;
        else return true;
    }

    private void SettingLayerOrder(int num, GameObject obj)
    {
        List<SpriteRenderer> sprite = new List<SpriteRenderer>();
            GetSprite(obj.transform,sprite);
        for (int i = 0; i < sprite.Count; i++)
            sprite[i].sortingLayerName = num + "";
    }

    private void GetSprite(Transform obj, List<SpriteRenderer> sprite)
    {
        for(int i = 0; i < obj.childCount; i++)
        {
            if (obj.childCount > 0)
                GetSprite(obj.GetChild(i), sprite);
            sprite.Add(obj.GetChild(i).GetComponent<SpriteRenderer>());
        }
    }

    private void SettingTransform(Transform obj, Transform index)
    {
        SettingLayerOrder(layer, obj.gameObject);
        obj.parent = transform;
        obj.position = index.position;
    }
}
