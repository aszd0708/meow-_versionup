﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage3ObjectPochaMasterMovement : MonoBehaviour, TouchableObj
{
    [Header("움직이는 시간")]
    public float moveTime;
    [Header("움직이고 기다리는 시간")]
    public float stopTime;
    [Header("움직이는 거리")]
    public float[] movePos = new float[2];
    [Header("터치했을 때 움직이는 크기")]
    public float touchMovePos = 0.2f;

    private bool touch = false;

    void Start()
    {
        StartCoroutine(MoveMotion());
    }

    public void OnClick()
    {
        if (touch) return;
        StartCoroutine(TouchMotion());
    }

    private IEnumerator TouchMotion()
    {
        touch = true;
        transform.DOLocalMoveY(transform.localPosition.y + touchMovePos, 0.1f);
        yield return new WaitForSeconds(0.1f);
        transform.DOLocalMoveY(transform.localPosition.y - touchMovePos, 0.1f);
        yield return new WaitForSeconds(0.1f);
        touch = false;
        yield break;

    }

    private IEnumerator MoveMotion()
    {
        int index = 0;
        bool flip = false;
        while(true)
        {
            if (flip)
                transform.DOLocalRotate(new Vector3(0, 180, 0), 0.5f);
            else
                transform.DOLocalRotate(new Vector3(0, 0, 0), 0.5f);
            flip = !flip;

            float random = Random.Range(1.0f, 2.0f);

            transform.DOLocalMoveX(movePos[index++], random);
            index %= movePos.Length;
            yield return new WaitForSeconds(random + stopTime);
        }
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        OnClick();
        return false;
    }
}
