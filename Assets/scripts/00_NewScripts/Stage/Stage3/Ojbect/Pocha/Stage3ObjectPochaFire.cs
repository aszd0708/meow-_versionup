﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-02-03
 * 스테이지3에 있는 불 모션
 * 계속 두개의 스프라이트 변경
 * 터치하면 꺼지고 다시 터치하면 켜짐
 */

public class Stage3ObjectPochaFire : MonoBehaviour, TouchableObj
{
    public List<Sprite> motionSprite = new List<Sprite>();
    public float[] time = new float[2];

    private SpriteRenderer sprite;
    private Coroutine motionCor;

    public bool play;

    private void Awake()
    {
        sprite = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        int random = Random.Range(0, 2);

        if (random == 0) play = true;
        else play = false;

        OnClick();
    }

    private IEnumerator Motion()
    {
        int index = Random.Range(0, 1);
        int count = motionSprite.Count;
        while (true)
        {
            sprite.sprite = motionSprite[index++];
            index %= count;
            yield return new WaitForSeconds(Random.Range(time[0], time[1]));
        }
    }

    public void OnClick()
    {
        motionCor = StartCoroutine(Motion());

        if (!play)
            StopCoroutine(motionCor);

        sprite.enabled = play;
        play = !play;
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        OnClick();
        return false;
    }
}
