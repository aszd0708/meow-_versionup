﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-02-21
 * 가운데 불 있는 그 포차
 * 가운데 왼쪽 오른쪽 음식 생성해주는 함수
 * 왼쪽 오른쪽은 모르겠다 일단 해보자
 * 학원 갔다와서 토요일이나 금요일에 하기 잠만 끄고 일본어 시험 준비할거 감 ㅃㅃ
 * 아마 이거부터는 다음 컴퓨터에서 작업할듯
 */

public class Stage3ObjectPochaFoodCreater : MonoBehaviour
{
    public GameObject food;

    public List<Sprite> foodSprite = new List<Sprite>();

    public Transform centerTransform;
    public Transform leftTransform;
    public Transform rightTransform;

    public Transform foodTransform;

    public Quaternion centerRot;
    public Quaternion leftRot;
    public Quaternion rightRot;

    [Header("스프라이트 랜더러 레이어")]
    public string layer;

    private List<Vector3> centerPoses = new List<Vector3>();
    private List<Vector3> leftPoses = new List<Vector3>();
    private List<Vector3> rightPoses = new List<Vector3>();

    [Header("각 포지션별로 몇개를 만들까에 관한 변수")]
    public int center, left, right;

    private enum State
    {
        CENTER, LEFT, RIGHT
    }

    void Awake()
    {
        centerPoses = SetTransform(centerTransform);
        rightPoses = SetTransform(rightTransform);
        leftPoses = SetTransform(leftTransform);
    }

    // Start is called before the first frame update
    void Start()
    {
        int index = StateRandomCount(State.CENTER);

        CreateFood(State.CENTER, foodSprite[Random.Range(0, foodSprite.Count)]);
        CreateFood(State.RIGHT, foodSprite[Random.Range(0, foodSprite.Count)]);
        CreateFood(State.LEFT, foodSprite[Random.Range(0, foodSprite.Count)]);
    }

    private void CreateFood(State state, Sprite sprite)
    {
        int randomCount = StateRandomCount(state);
        for (int i = 0; i < randomCount+1; i++)
        {
            GameObject obj = Instantiate(food);
            SpriteRenderer objSprite = obj.GetComponent<SpriteRenderer>();
            objSprite.sortingLayerName = layer;
            objSprite.sprite = sprite;
            objSprite.sortingOrder = 6;
            SetPosition(state, obj);
        }
    }

    private int StateRandomCount(State state)
    {
        switch(state)
        {
            case State.CENTER: return Random.Range(0, centerPoses.Count);
            case State.RIGHT: return Random.Range(0, rightPoses.Count);
            case State.LEFT: return Random.Range(0, leftPoses.Count);
            default: return 0;
        }
    }

    private void SetPosition(State state, GameObject obj)
    {
        int random = StateRandomCount(state);
        switch (state)
        {
            case State.CENTER:
                obj.transform.rotation = centerRot;
                obj.transform.position = centerPoses[random];
                break;

            case State.RIGHT:
                obj.transform.rotation = rightRot;
                obj.transform.position = rightPoses[random];
                break;

            case State.LEFT:
                obj.transform.rotation = rightRot;
                obj.transform.position = leftPoses[random];
                break;
        }
        obj.transform.SetParent(foodTransform);
    }

    private List<Vector3> SetTransform(Transform t)
    {
        List<Vector3> vector = new List<Vector3>();

        for (int i = 0; i < t.childCount; i++)
            vector.Add(t.GetChild(i).position);

        return vector;
    }
}
