﻿using DG.Tweening;
using System.Collections;
using UnityEngine;

public class Stage3AnimalWhiteCat : StageAnimalObjectBase
{
    [Header("부분별 스프라이트 랜더러")]
    public SpriteRenderer bodySprite, tailSprite;

    [Header("각 부분 스프라이트")]
    public Sprite[] bodySprites, tailSprites;
    public Sprite standingSprite, catchSprite;

    [Header("몸 부분 스프라이트 변경 시간")]
    public float bodyChangeTime;

    [Header("목표, 착지 트랜스폼")]
    public Transform targetTrasnform, laningTransform;
    [Header("꼬리 트랜스폼")]
    public Transform tailTransform;


    [Header("먹이에게 가기 전에 흔드는 횟수")]
    public int shakingCount;
    public float shakingTime;

    [Header("꼬리가 움직이는 앵글")]
    public float tailAngle;

    [Header("날아가는 시간")]
    public float catchTime, laningTime;

    [Header("점프 파워")]
    public float jumpPower = 2.0f;


    private bool touch = false;
    public bool Touch { get => touch; set => touch = value; }

    private bool bItemEvent = false;
    private void Start()
    {
        bodySprite.sprite = standingSprite;
    }

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if (!Touch)
        {
            return false;
        }
        SetCollect();
        StartCoroutine(_BodyMotion());
        AudioManager.Instance.PlaySound("cat_5", transform.position);
        return false;
    }

    public override void StartItemEvent(GameObject itemObj)
    {
        base.StartItemEvent(itemObj);
        if(bItemEvent)
        {
            return;
        }
        bItemEvent = true;
        StopAllCoroutines();
        itemObj.transform.position = laningTransform.position;
        StartCoroutine(_FindTarget());
    }

    private IEnumerator _FindTarget()
    {
        yield return StartCoroutine(_BodyMotion());
        tailTransform.Rotate(Vector3.zero);
        bodySprite.sprite = catchSprite;
        transform.DOJump(laningTransform.position, jumpPower, 1, catchTime + laningTime);
        tailSprite.sprite = tailSprites[1];
        yield return new WaitForSeconds(catchTime + laningTime);
        bodySprite.sprite = standingSprite;
        tailSprite.sprite = tailSprites[0];
        Touch = true;
        yield break;
    }

    private IEnumerator _BodyMotion()
    {
        int index = 0;
        WaitForSeconds shakingWait = new WaitForSeconds(shakingTime);
        StartCoroutine(_TailMotion(shakingCount * 3 * shakingTime));
        for (int i = 0; i < shakingCount * 3; i++)
        {
            bodySprite.sprite = bodySprites[index++];
            index %= bodySprites.Length;
            yield return shakingWait;
        }
        yield break;
    }

    private IEnumerator _TailMotion(float time)
    {
        WaitForSeconds waitTime = new WaitForSeconds(time / 5);
        Vector3[] angle = { new Vector3(0, 0, tailAngle), new Vector3(0, 0, -tailAngle) };
        int index = 0;
        for (int i = 0; i < 5; i++)
        {
            tailTransform.DORotate(angle[index++], time / 5);
            index %= angle.Length;
            yield return waitTime;
        }
        yield break;
    }
}
