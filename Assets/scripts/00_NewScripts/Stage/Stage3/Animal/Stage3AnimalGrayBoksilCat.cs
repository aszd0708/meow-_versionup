﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage3AnimalGrayBoksilCat : StageAnimalObjectBase
{
    [Header("각 부분들 SpriteRenderer")]
    public SpriteRenderer bodySprite, eyeSprite, tailSprite;
    [Header("각 부분들 Sprite")]
    public Sprite sitBodySprite;
    public Sprite[] walkingBodySprites, eyeSprites;

    [Header("각 부분들 Transform")]
    public Transform eyeTransform, mouthTransform, tailTransform;

    [Header("꼬리 흔드는 시간")]
    public float tailTime;

    [Header("눈 깜빡이는 시간")]
    public float blinkTime;
    [Header("걸을 때 스프라이트가 바뀌는 시간")]
    public float walkingChangeTime;
    [Header("총 걷는 시간")]
    public float walkingTime;

    [Header("도착 지점")]
    public SpriteRenderer item;

    private Coroutine tailCor;
    private Coroutine eyeCor;

    private bool canTouch = false;
    public bool CanTouch { get => canTouch; set => canTouch = value; }

    private void Start()
    {
        tailCor = StartCoroutine(_TailMotion());
        eyeCor = StartCoroutine(_EyeMotion());
    }

    public void OnClick()
    {
        if (CanTouch)
            return;

        StartCoroutine(_GoDestination());
    }

    private IEnumerator _EyeMotion()
    {
        int randomCount;
        int eyeIndex = 0;
        WaitForSeconds blinkWait = new WaitForSeconds(blinkTime);
        WaitForSeconds waitTime = new WaitForSeconds(5.0f);
        while (true)
        {
            randomCount = Random.Range(0, 4);

            for (int i = 0; i < randomCount * 2; i++)
            {
                eyeSprite.sprite = eyeSprites[eyeIndex++];
                eyeIndex %= eyeSprites.Length;
                yield return blinkWait;
            }
            yield return waitTime;
        }
    }

    private IEnumerator _WalkingMotion()
    {
        int index = 0;
        WaitForSeconds spriteChangeWait = new WaitForSeconds(walkingChangeTime);

        while(true)
        {
            bodySprite.sprite = walkingBodySprites[index++];
            index %= walkingBodySprites.Length;
            yield return spriteChangeWait;
        }
    }

    private IEnumerator _TailMotion()
    {
        int index = 0;
        Vector3[] rotationAngle = { new Vector3(0, 0, 20), new Vector3(0, 0, -20) };
        WaitForSeconds tailWait = new WaitForSeconds(tailTime);

        while(true)
        {
            tailTransform.DORotate(rotationAngle[index++], tailTime).SetEase(Ease.OutCubic);
            index %= rotationAngle.Length;
            yield return tailWait;
        }
    }

    private IEnumerator _GoDestination()
    {
        CanTouch = false;
        SetWalking();
        WaitForSeconds goWait = new WaitForSeconds(walkingTime);
        Coroutine walkingMotionCor = StartCoroutine(_WalkingMotion());
        transform.DOMove(item.transform.position, walkingTime).SetEase(Ease.Linear);
        yield return goWait;
        StopCoroutine(walkingMotionCor);
        SetSitting();
        CanTouch = true;
        yield break;
    }

    private void SetSitting()
    {
        bodySprite.sprite = sitBodySprite;
        tailCor = StartCoroutine(_TailMotion());
    }

    private void SetWalking()
    {
        StopCoroutine(tailCor);
        tailTransform.Rotate(Vector3.zero);
    }

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if(!CanTouch)
        {
            return false;
        }
        SetCollect();
        AudioManager.Instance.PlaySound("fat_cat", transform.position);

        return false;
    }

    public override void StartItemEvent(GameObject itemObj)
    {
        if (endItemMotion)
        {
            return;
        }
        base.StartItemEvent(itemObj);
        itemObj.transform.position = item.transform.position;
        OnClick();
    }
}
