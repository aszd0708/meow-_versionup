﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage3AnimalBearPuppy : StageAnimalObjectBase
{
    [Header("각 부분들 스프라이트 랜더러")]
    public SpriteRenderer bodySprite, frontFaceSprite, sideFaceSprite, movementEyeSprite;

    [Header("각 부분들 스프라이트들")]
    public Sprite[] bodySprites, frontFaceSprites, sideFaceSprites;

    [Header("각 부분들 Trasnform")]
    public Transform bodyTransform, faceTransform, tailTrasnform;

    [Header("(아이템 주기 전)꼬리 흔드는 시간")]
    public float nonItemTailShakeTime;
    [Header("(아이템 주기 전)꼬리 흔드는 시간")]
    public float itemTailShakeTime;

    [SerializeField]
    private Transform destinationTransform;

    [SerializeField]
    private Sprite[] movementBodySprites;

    [SerializeField]
    private Sprite idleSprite;

    [SerializeField]
    private Sprite happySprite;

    private Coroutine tailCor;

    private bool canTouch;

    private bool isItem;

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if (!canTouch)
        {
            return false;
        }
        SetCollect();
        AudioManager.Instance.PlaySound("dog_1", transform.position);
        return false;
    }

    private IEnumerator _TailMotion(float shakeTime)
    {
        WaitForSeconds shakeWait = new WaitForSeconds(shakeTime);
        Vector3[] shakeRot = { new Vector3(0, 0, 20), new Vector3(0, 0, -20) };
        int index = 0;
        while (true)
        {
            tailTrasnform.DOLocalRotate(shakeRot[index++], shakeTime);
            index %= shakeRot.Length;
            yield return shakeWait;
        }
    }

    public override void StartItemEvent(GameObject itemObj)
    {
        base.StartItemEvent(itemObj);

        Debug.Log("BearDog");
        if(isItem)
        {
            return;
        }

        destinationTransform.GetComponent<SpriteRenderer>().enabled = true;
        StartCoroutine(_MotionStart());
    }

    private IEnumerator _MotionStart()
    {
        isItem = true;
        transform.DOMove(destinationTransform.position, 2.0f).SetEase(Ease.Linear);
        int index = 0;
        frontFaceSprite.enabled = false;
        sideFaceSprite.enabled = true;
        for (int i = 0; i < 8; i++)
        {
            bodySprite.sprite = movementBodySprites[index++];
            index %= movementBodySprites.Length;
            yield return new WaitForSeconds(2.0f / 8f);
        }
        bodySprite.sprite = happySprite;
        frontFaceSprite.enabled = true;
        sideFaceSprite.enabled = false;
        tailCor = StartCoroutine(_TailMotion(itemTailShakeTime));
        canTouch = true;
        yield break;
    }
}
