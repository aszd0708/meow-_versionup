﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage3AnimalMycarDog : StageAnimalObjectBase
{
    [SerializeField]
    private Transform windowTransform;
    [SerializeField]
    private GameObject windowMask;

    [SerializeField]
    private Transform[] wheels = new Transform[2];

    [SerializeField]
    private SpriteRenderer headRenderer;

    [SerializeField]
    private Sprite[] headSprites = new Sprite[2];

    [Header("움직일 좌표들?")]
    [SerializeField]
    private Transform[] movePoses = new Transform[4];

    [Header("바퀴 가속도")]
    [SerializeField]
    private float speed;

    [Header("최대 속도")]
    [SerializeField]
    private float maxSpeed;

    #region 처음 나올때 필요한 변수들

    [Header("이 오브젝트에 있는 스프라이트 랜더러들 (창문 제외)")]
    [SerializeField]
    private SpriteRenderer[] spriteRenderers;

    [Header("창문 스프라이트 랜더러")]
    [SerializeField]
    private SpriteRenderer windowSpriteRenderer;

    [Header("창문 내릴때 필요한 스프라이트 마스크")]
    [SerializeField]
    private SpriteMask spriteMask;

    [Header("차고 앞")]
    [SerializeField]
    private Transform frontOfGarbage;

    [Header("차고 문짝")]
    [SerializeField]
    private Transform slideDoor;

    #endregion

    private bool isMove = false;

    private bool isRollingWheele = false;

    private float nowSpeed = 0, rot = 0;

    private Coroutine headCor;

    private bool canCollect = false;

    private bool photo = false;

    private bool useItem = false;

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if(!canCollect)
        {
            return false;
        }
        SetCollect();
        AudioManager.Instance.PlaySound("car", transform.position);
        return false;
    }

    public override void StartItemEvent(GameObject itemObj)
    {
        base.StartItemEvent(itemObj);
        if(useItem)
        {
            return;
        }
        useItem = true;
        StartCoroutine(_ExitGarbage());
        PoolingManager.Instance.SetPool(itemObj, "ItemDummy");
    }

    private void Update()
    {
        if (isMove)
        {
            Accelate();
        }

        else
        {
            Stop();
        }

        if(isRollingWheele)
        {
            rot += Time.deltaTime * nowSpeed;
        }

        else
        {
            rot -= Time.deltaTime * 10;
            if(rot <= 0)
            {
                rot = 0;
            }
        }
        RotateWheels();
        nowSpeed = Mathf.Clamp(nowSpeed, 0, maxSpeed);
    }

    private void RotateWheels()
    {
        for(int i = 0; i < wheels.Length; i++)
        {
            wheels[i].rotation = Quaternion.Euler(0, 0, rot);
        }
    }

    private void Accelate()
    {
        nowSpeed += speed;
    }

    private void Stop()
    {
        if (nowSpeed <= 0)
            return;
        nowSpeed -= speed;
        if (rot <= 0)
            rot = 0;
    }

    public void StartUpCar()
    {
        if(photo)
        {
            StartCoroutine(_StartUpCar());
        }
    }

    private IEnumerator _StartUpCar()
    {
        windowTransform.gameObject.SetActive(false);
        windowMask.SetActive(false);
        isMove = true;
        transform.DOMove(movePoses[0].position, 4.0f).SetEase(Ease.InCirc);
        headCor = StartCoroutine(_PlayHeadMotion(8.0f, 10));
        yield return new WaitForSeconds(5.0f);
        transform.position = movePoses[1].position;
        transform.DOMove(movePoses[2].position, 4.0f).SetEase(Ease.OutCirc);
        yield return new WaitForSeconds(3.0f);
        isMove = false;
        gameObject.SetActive(false);
        yield break;
    }

    private IEnumerator _PlayHeadMotion(float time, int count)
    {
        WaitForSeconds wait = new WaitForSeconds(time / count);

        int index = 0;
        for(int i = 0; i < count; i++)
        {
            headRenderer.sprite = headSprites[index++];
            index %= headSprites.Length;
            yield return wait;
        }
        yield break;
    }

    private IEnumerator _ExitGarbage()
    {
        slideDoor.DOLocalMoveY(3.0f, 1.0f).SetEase(Ease.OutElastic);
        yield return new WaitForSeconds(1.5f);
        // 처음 마스크 설정 -> 이동 -> 다시 마스크 초기 설정으로 -> 창문 마스크 설정
        for (int i = 0; i < spriteRenderers.Length; i++)
        {
            spriteRenderers[i].maskInteraction = SpriteMaskInteraction.VisibleOutsideMask;
        }

        transform.DOMove(frontOfGarbage.position, 2.0f);
        isRollingWheele = true;
        yield return new WaitForSeconds(2.0f);
        isRollingWheele = false;
        for (int i = 0; i < spriteRenderers.Length; i++)
        {
            spriteRenderers[i].maskInteraction = SpriteMaskInteraction.None;
        }

        spriteMask.enabled = true;

        transform.DOShakePosition(1.0f, 0.1f, 15, 90);
        yield return new WaitForSeconds(1.0f);
        windowTransform.DOLocalMoveY(-0.65f, 1.0f).SetEase(Ease.Linear);
        photo = true;
        canCollect = true;

        if(Info.IsCollectVirtual)
        {
            yield return new WaitForSeconds(1.5f);
            StartUpCar();
        }

        yield break;
    }
}
