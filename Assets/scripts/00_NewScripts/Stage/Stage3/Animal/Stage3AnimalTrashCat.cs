﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

/*
 * 2020-04-14
 * 쓰래기통 고양이 모션 및 쓰래기통 따꿍 모션 
 * 처음 몇번(랜덤) 눌렀을 때 따꿍 떨어지고
 * 그 다음에 다시 누르면 흔들리다가
 * 고양이 나옴
 */

public class Stage3AnimalTrashCat : StageAnimalObjectBase
{
    [Header("몇번 터치 했을 때 따꿍 날아가는지")]
    public int minCount, maxCount;
    [Header("따꿍 착지 위치")]
    public Transform capPos;
    [Header("흔들릴때 스프라이트")]
    public Transform shakeCap;
    [Header("날아갈때 스프라이트")]
    public Transform jumpingCap;

    [Header("고양이에 관한 변수들")]
    [Header("고양이 Transoform")]
    public Transform catTransform;
    [Header("고양이 SpriteRenderer")]
    public SpriteRenderer eyeSprite, bodySprite;
    [Header("고양이 Sprites")]
    public Sprite[] eyeSprites, bodySprites;
    [Header("눈 감빡이는 시간")]
    public float blinkTime;
    [Header("모션 움직이는 시간")]
    public float motionTime;
    [Header("위로 올라오는 시간")]
    public float upTime;
    private Coroutine eyeCor, motionCor;

    [Header("쓰레기 시작 포인트")]
    [SerializeField]
    private Transform trashStartPose;
    [SerializeField]
    private Transform trashEndPose;


    public enum State { CAP, NONCAP, INSIDE, OUTSIDE }
    private State STATE = State.CAP;

    private bool canTouch = false;
    public bool CanTouch { get => canTouch; set => canTouch = value; }
    private bool endTrashMotion = true;

    private SpriteRenderer jumpingCapSprite;
    private SpriteRenderer shakeCapSprite;

    public SpriteRenderer ShakeCapSprite { get => shakeCapSprite; set => shakeCapSprite = value; }
    public SpriteRenderer JumpingCapSprite { get => jumpingCapSprite; set => jumpingCapSprite = value; }

    Quaternion origincapRotation;

    private bool canCollect = false;

    public void Awake()
    {
        JumpingCapSprite = jumpingCap.GetComponent<SpriteRenderer>();
        ShakeCapSprite = shakeCap.GetComponent<SpriteRenderer>();
    }

    public void Start()
    {
        JumpingCapSprite.enabled = false;
        ShakeCapSprite.enabled = true;

        origincapRotation = capPos.rotation;
    }

    private IEnumerator _CapMotion(GameObject trash, bool isFish)
    {
        if(isFish)
        {
            canCollect = true;
            Debug.Log("ㄱ");
            yield return StartCoroutine(_ShakeCap());
            yield return StartCoroutine(_CatCreateMotion());
        }

        else
        {
            endTrashMotion = false;
            yield return StartCoroutine(_ShakeCap());
            yield return StartCoroutine(_ThrowTrash(trash));
            endTrashMotion = true;
        }
        yield break;
    }

    private IEnumerator _ShakeCap()
    {
        shakeCap.DOPause();
        capPos.rotation = Quaternion.identity;
        WaitForSeconds wait = new WaitForSeconds(0.5f);
        for (int i = 0; i < 3; i++)
        {
            shakeCap.DOShakeRotation(0.5f, 50, 10, 45);
            yield return wait;
        }
        yield break;
    }

    private IEnumerator _jumpCap()
    {
        JumpingCapSprite.enabled = true;
        ShakeCapSprite.enabled = false;
        jumpingCap.DORotate(new Vector3(0, 0, -180), 0.5f);
        jumpingCap.DOLocalJump(capPos.localPosition, 1, 1, 0.75f);
        yield return new WaitForSeconds(0.75f);
        STATE = State.INSIDE;
        shakeCap.SetParent(null);
        jumpingCap.SetParent(null);
        endTrashMotion = true;
        yield break;
    }

    private IEnumerator _ThrowTrash(GameObject trash)
    {
        shakeCap.DORotate(Vector3.forward * 15f, 0.5f);
        yield return new WaitForSeconds(0.5f);
        trash.SetActive(true);
        trash.transform.position = trashStartPose.position;
        trash.transform.DOJump(trashEndPose.position, 1, 1, 0.3f);
        yield return new WaitForSeconds(0.2f);
        shakeCap.DORotate(Vector3.zero * 15f, 0.5f);
        PoolingManager.Instance.SetPool(trash, "ItemDummy", 3.0f);
        yield break;
    }
    // 여기서 부터는 고양이에 관한 코루틴 및 모션 함수들

    private IEnumerator _EyeMotion()
    {
        int randomCount;
        int eyeIndex = 0;
        WaitForSeconds blinkWait = new WaitForSeconds(blinkTime);
        WaitForSeconds waitTime = new WaitForSeconds(5.0f);

        while (true)
        {
            randomCount = Random.Range(0, 4);

            for (int i = 0; i < randomCount * 2; i++)
            {
                eyeSprite.sprite = eyeSprites[eyeIndex++];
                eyeIndex %= eyeSprites.Length;
                yield return blinkWait;
            }
            yield return waitTime;
        }
    }

    private IEnumerator _BodyMotion()
    {
        int bodyIndex = 0;
        WaitForSeconds bodyChangeWait = new WaitForSeconds(motionTime);
        WaitForSeconds waitTime = new WaitForSeconds(5.0f);

        while (true)
        {
            int randomCount = Random.Range(0, 4);
            for (int i = 0; i < randomCount; i++)
            {
                bodySprite.sprite = bodySprites[bodyIndex++];
                bodyIndex %= eyeSprites.Length;
                yield return bodyChangeWait;
            }
            yield return waitTime;
        }
    }

    private IEnumerator _CatCreateMotion()
    {
        WaitForSeconds waitShake = new WaitForSeconds(0.5f);
        for (int i = 0; i < 4; i++)
        {
            transform.DOShakeRotation(0.5f, 50, 10, 45);
            yield return waitShake;
        }
        yield return StartCoroutine(_jumpCap());
        catTransform.DOLocalMoveY(1, upTime);
        yield return new WaitForSeconds(upTime);
        eyeCor = StartCoroutine(_EyeMotion());
        motionCor = StartCoroutine(_BodyMotion());
        STATE = State.OUTSIDE;
        CanTouch = true;
        yield break;
    }

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if (!CanTouch)
        {
            StartCoroutine(_ShakeCap());
            return false;
        }
        SetCollect();
        AudioManager.Instance.PlaySound("cat_6", transform.position);
        return false;
    }

    public override void StartItemEvent(GameObject itemObj)
    {
        Debug.Log("뼈");
        base.StartItemEvent(itemObj);
        if(canCollect)
        {
            return;
        }
        PoolingManager.Instance.SetPool(itemObj, "ItemDummy");
        StartCoroutine(_CapMotion(null, true));
    }

    public void OtherItemEvent(GameObject otherItem)
    { 
        if(!endTrashMotion || CanTouch)
        {
            PoolingManager.Instance.SetPool(otherItem, "ItemDummy");
            return;
        }
        Debug.Log("안뼈");
        if(!canCollect)
        {
            StartCoroutine(_CapMotion(otherItem, false));
        }
    }
}
