﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage3AnimalWhiteBoksilCat : StageAnimalObjectBase
{
    [Header("각 부분들 스프라이트랜더러")]
    public SpriteRenderer bodySprite, eyeSprite, mouthSprite, tailSprite;

    [Header("각 부분 스프라이트들")]
    public Sprite[] bodySprites, eyeSprites, tailSprites;

    [Header("바뀌는데 시간")]
    public float changeTime;
    [Header("화나있는 시간")]
    public float angryTime;
    [Header("눈 감빡이는 시간")]
    public float blinkTime;

    [Header("흔드든데 필요한 변수들")]
    public float vibePower;
    public int vibrator;
       
    private bool touch = false;
    private bool Touch { get => touch; set => touch = value; }

    private Coroutine eyeCor;

    public void OnClick()
    {
        if (Touch)
            return;
        StartCoroutine(_AngryMotion());
        AudioManager.Instance.PlaySound("cat_10", transform.position);
    }

    private void Start()
    {
        eyeCor = StartCoroutine(_EyeMotion());
    }

    public override bool DoTouch(Vector2 touchPose = default)
    {
        SetCollect();
        OnClick();
        return false;
    }

    private IEnumerator _EyeMotion()
    {
        int randomCount;
        int eyeIndex = 0;
        WaitForSeconds blinkWait = new WaitForSeconds(blinkTime);
        WaitForSeconds waitTime = new WaitForSeconds(5.0f);
        while (true)
        {
            randomCount = Random.Range(0, 4);

            for (int i = 0; i < randomCount * 2; i++)
            {
                eyeSprite.sprite = eyeSprites[eyeIndex++];
                eyeIndex %= eyeSprites.Length;
                yield return blinkWait;
            }
            yield return waitTime;
        }
    }

    private IEnumerator _AngryMotion()
    {
        SetStanding();
        Touch = true;
        WaitForSeconds angryWait = new WaitForSeconds(angryTime);
        WaitForSeconds changeWait = new WaitForSeconds(changeTime);

        for(int i = 1; i < bodySprites.Length; i++)
        {
            bodySprite.sprite = bodySprites[i];
            yield return changeWait;
        }

        transform.DOShakePosition(angryTime, vibePower, vibrator);
        yield return angryWait;

        for (int i = bodySprites.Length - 1; i > 0; i--)
        {
            bodySprite.sprite = bodySprites[i];
            yield return changeTime;
        }
        SetSitting();
        Touch = false;
        yield break;
    }

    private void SetSitting()
    {
        bodySprite.sprite = bodySprites[0];
        tailSprite.sprite = tailSprites[0];
        eyeSprite.enabled = true;
        mouthSprite.enabled = true;
        eyeCor = StartCoroutine(_EyeMotion());
    }

    private void SetStanding()
    {
        StopCoroutine(eyeCor);
        eyeSprite.enabled = false;
        mouthSprite.enabled = false;
    }

}
