﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage3AnimalMemoryCat : StageAnimalObjectBase
{
    private bool canTouch;
    public bool CanTouch { get => canTouch; set => canTouch = value; }

    [Header("팔과 마이크 트랜스폼")]
    public Transform armTransform, micTransform;
    [Header("얼굴 스프라이트 랜더러")]
    public SpriteRenderer faceSprite, bodySprite;
    [Header("얼굴 스프라이트")]
    public Sprite[] faceSprites, bodySprites;
    public Sprite restfaceSprite;

    [Header("스포트라이트 스프라이트랜더러")]
    public SpriteRenderer spotlightSprite;

    [Header("각 파트 별 움직이는 시간")]
    public float[] time;

    [Header("전체 랜더러들")]
    [SerializeField]
    private SpriteRenderer[] spriteRenderers;

    public bool itenFound = false;

    [SerializeField]
    private Transform movePose;

    private bool canCollect;

    private void Start()
    {
        SetRendererColor(false);
        spotlightSprite.enabled = false;
        CanTouch = false;
    }

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if(CanTouch == false)
        {
            return false;
        }
        SetCollect();
        return false;
    }

    public override void StartItemEvent(GameObject itemObj)
    {
        if (endItemMotion)
        {
            PoolingManager.Instance.SetPool(itemObj, "ItemDummy");
            return;
        }
        base.StartItemEvent(itemObj);
        spotlightSprite.enabled = true;
        SetRendererColor(true);
        CanTouch = true;
        itenFound = true;
        OnClick();

        transform.position = new Vector3(transform.position.x, transform.position.y, -2);
    }

    private IEnumerator _MoveToMic()
    {
        transform.DOMove(movePose.position, 0.75f).SetEase(Ease.Linear);
        yield return new WaitForSeconds(0.75f);
        yield break;
    }

    public void OnClick()
    {
        if (!CanTouch)
        {
            return;
        }
        CanTouch = false;
        StartCoroutine(_SingingMotion());
        AudioManager.Instance.PlaySound("memory", transform.position);
    }

    private void SetRendererColor(bool isOn)
    {
        if(isOn)
        {
            for(int i = 0; i < spriteRenderers.Length; i++)
            {
                spriteRenderers[i].color = Color.white;
            }
        }

        else
        {
            for(int i = 0; i < spriteRenderers.Length; i++)
            {
                spriteRenderers[i].color = Color.black;
            }
        }
    }

    private IEnumerator _SingingMotion()
    {
        CanTouch = false;
        yield return StartCoroutine(_MoveToMic());
        CanTouch = true;
        Vector3 micPos = micTransform.position;
        int index = 0;
        float rot = 180;
        WaitForSeconds settingWait = new WaitForSeconds(time[index] / 2);
        WaitForSeconds danceWait = new WaitForSeconds(time[1]);
        WaitForSeconds flipWait = new WaitForSeconds(0.2f);
        armTransform.DORotate(Vector3.zero, time[index]).SetEase(Ease.OutBack);
        spotlightSprite.transform.SetParent(null);
        yield return settingWait;
        micTransform.DORotate(Vector3.zero, time[index] / 2);
        yield return settingWait;
        micTransform.SetParent(armTransform);
        spotlightSprite.enabled = true;

        for (int i = 0; i < 6; i++)
        {
            int bodyIndex = 0;
            int armIndex = 0;
            for (int a = 0; a < 2; a++)
            {
                bodySprite.sprite = bodySprites[bodyIndex];
                faceSprite.sprite = faceSprites[bodyIndex++];
                armTransform.DOLocalRotate(new Vector3(0, 0, armIndex), time[1]);
                if (armIndex == 0) armIndex = 10; else armIndex = 0;
                bodyIndex %= bodySprites.Length;
                yield return danceWait;
            }
            transform.DORotate(new Vector3(0, rot, 0), 0.2f);
            if (rot == 0) rot = 180; else rot = 0;
            yield return flipWait;
        }

        micTransform.SetParent(transform);
        faceSprite.sprite = restfaceSprite;
        armTransform.DORotate(new Vector3(0, 0, -50), time[index]).SetEase(Ease.OutBack);
        micTransform.position = micPos;
        micTransform.DORotate(new Vector3(0, 0, -15), time[index] / 2);
        yield return new WaitForSeconds(time[index]);
        //SetRendererColor(false);
        spotlightSprite.transform.SetParent(transform);
        spotlightSprite.enabled = false;
        CanTouch = true;
        yield break;
    }
}
