﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage3AnimalSleepingDog : StageAnimalObjectBase
{
    [Header("몸, 방울 스프라이트 랜더러")]
    public SpriteRenderer bodySprite;
    public SpriteRenderer bubbleSprite;

    [Header("몸, 방울 스프라이트들")]
    public Sprite[] bodySprites = new Sprite[2];
    public Sprite[] bubbleSprites = new Sprite[4];

    [Header("숨 한번 쉬는 시간")]
    public float breathTime;

    [Header("가비지 컬렉터 눈치보여서 전역으로 선언함")]
    WaitForSeconds waitBreath;

    private int bodySpriteIndex = 0;

    private Coroutine BodyCor, BubbleCor;

    private bool collect;
    public bool Collect { get => collect; set => collect = value; }

    private bool bubbleBomb = true;
    private void Start()
    {
        waitBreath = new WaitForSeconds(breathTime);
        BodyCor = StartCoroutine(_BodyMotion());
    }

    private IEnumerator _BodyMotion()
    {
        bodySpriteIndex = 0;
        while(true)
        {
            bodySprite.sprite = bodySprites[bodySpriteIndex++];
            bodySpriteIndex %= bodySprites.Length;
            if(bubbleBomb)
            {
                bubbleBomb = false;
                BubbleCor = StartCoroutine(_BubbleMotion());
            }

            yield return waitBreath;
        }
    }

    private IEnumerator _BubbleMotion()
    {
        WaitForSeconds waitBubble = new WaitForSeconds(breathTime / 2);
        bubbleSprite.sprite = bubbleSprites[0];
        yield return waitBreath;
        while(true)
        {
            bubbleSprite.sprite = bubbleSprites[bodySpriteIndex + 1];
            yield return waitBubble;
        }
    }
    
    private IEnumerator _BubbleTouch()
    {
        WaitForSeconds touchWait = new WaitForSeconds(0.25f);
        StopCoroutine(BubbleCor);
        bubbleSprite.sprite = bubbleSprites[3];
        yield return touchWait;
        bubbleSprite.sprite = bubbleSprites[4];
        yield return touchWait;
        bubbleSprite.sprite = null;
        yield return waitBreath;
        bubbleBomb = true;
        yield break;
    }

    public void OnClick()
    {
        StartCoroutine(_BubbleTouch());
    }

    public override bool DoTouch(Vector2 touchPose = default)
    {
        SetCollect();
        OnClick();
        AudioManager.Instance.PlaySound("dog_4", transform.position);
        return false;
    }
}
