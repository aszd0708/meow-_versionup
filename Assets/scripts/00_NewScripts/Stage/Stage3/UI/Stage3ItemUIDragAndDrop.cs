﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Stage3ItemUIDragAndDrop : StageItemUIDragAndDrop
{
    [Header("쓰레기통냥")]
    [SerializeField]
    private Stage3AnimalTrashCat trashCat;

    private void Start()
    {
        trashCat = FindObjectOfType<Stage3AnimalTrashCat>();
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        base.OnEndDrag(eventData);

        Debug.Log("진입"); 
        Vector2 worldVector = Camera.main.ScreenToWorldPoint(transform.position);

        if (trashCat != targetAnimal)
        {
            if (trashCat.gameObject.GetInstanceID() == targetAnimal.GetInstanceID())
            {
                base.OnEndDrag(eventData);
                return;
            }
            Debug.Log("있음");

            if (Vector2.Distance(trashCat.transform.position, worldVector) <= triggerDistance)
            {
                Debug.Log("만듦");
                GameObject item = Instantiate(itemObj);
                if (!itemSprite)
                {
                    itemSprite = img.sprite;
                }
                item.GetComponent<SpriteRenderer>().sprite = itemSprite;
                trashCat.OtherItemEvent(item);
            }
        }
    }
}
