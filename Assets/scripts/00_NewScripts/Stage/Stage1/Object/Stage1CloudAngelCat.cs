﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage1CloudAngelCat : MonoBehaviour, TouchableObj
{
    [SerializeField]
    [Header("트랜스폼(자신)")]
    private Transform animalTransform;
    [SerializeField]
    [Header("콜라이더(자신)")]
    private Collider2D animalCol;

    [SerializeField]
    [Header("움직이는 좌표(z축은 회전값으로 들어감)")]
    private Vector3[] movePoses = new Vector3[4];

    [SerializeField]
    [Header("움직이는 모션 시간")]
    private float motionTime;

    [SerializeField]
    [Header("멈춰있는 시간")]
    private float stopTime;

    WaitForSeconds motionWait;

    [SerializeField]
    [Header("진짜 천사냥 Transform")]
    private Transform anlgelTransform;
    [SerializeField]
    [Header("진짜 천사냥")]
    private Stage1AnimalAngelCat angel;

    [SerializeField]
    [Header("날아가는 시간")]
    private float flyingTime;

    private bool canTouch = false;

    private bool bIsFlying = false;

    private void Start()
    {
        motionWait = new WaitForSeconds(motionTime);
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        if (canTouch)
            MoveToOriginAngelCat();
        return false;
    }

    /// <summary>
    /// 빼꼼할때 터치하면 원래 천사냥 있는 곳으로 올라감
    /// </summary>
    public void MoveToOriginAngelCat()
    {
        if(bIsFlying)
        {
            return;
        }
        bIsFlying = false;
        StopAllCoroutines();
        animalTransform.DOPause();
        StartCoroutine(_MoveToOriginAngelCat());
    }

    private IEnumerator _MoveToOriginAngelCat()
    {
        animalTransform.DOMove(anlgelTransform.position, flyingTime);
        yield return new WaitForSeconds(flyingTime);
        angel.gameObject.SetActive(true);
        angel.StartMotions();
        gameObject.SetActive(false);
        yield break;
    }

    public void PlayMotionCloudTouch()
    {
        if(bCanTouch == true)
            StartCoroutine(_StartCloudTouchMotion());
    }

    bool bCanTouch = true;
    /// <summary>
    /// 구름 터치했을때 빼꼼하는 모션
    /// </summary>
    /// <returns></returns>
    private IEnumerator _StartCloudTouchMotion()
    {
        bCanTouch = false;
        animalCol.enabled = true;
        Vector3 originPose = animalTransform.localPosition;
        Quaternion originRotation = animalTransform.localRotation;
        MoveToRandomPose();
        yield return motionWait;
        canTouch = true;
        yield return new WaitForSeconds(stopTime);

        animalTransform.DOLocalMove(originPose, motionTime);
        yield return motionWait;

        animalTransform.localRotation = originRotation;
        animalCol.enabled = false;
        canTouch = false;
        bCanTouch = true;
        yield break;
    }

    private void MoveToRandomPose()
    {
        int index = Random.Range(0, movePoses.Length);
        Vector3 movePose = new Vector3(movePoses[index].x, movePoses[index].y, animalTransform.position.z);
        Quaternion randomRotation = Quaternion.Euler(0, 0, movePoses[index].z);
        animalTransform.DOLocalMove(movePose, motionTime).SetEase(Ease.Linear);
        animalTransform.localRotation = randomRotation;
    }
}
