﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage1FlyingLightBird : MonoBehaviour
{
    public float x1, x2;

    [SerializeField]
    [Header("날아가는 y축")]
    private float y = 17;

    [SerializeField]
    [Header("총 날아가는 시간")]
    private float flyingTime;

    [SerializeField]
    [Header("애니메이터")]
    private Animator animator;
    // Start is called before the first frame update

    private void OnEnable()
    {
        StartCoroutine(_Fly());
    }

    private IEnumerator _Fly()
    {
        float x;
        yield return new WaitForSeconds(1.0f);
        animator.enabled = true;
        x = Random.Range(x1, x2);
        if (x >= -3)
            transform.rotation = Quaternion.Euler(0, 180, 0);
        transform.DOMove(new Vector3(x, y), flyingTime).SetEase(Ease.Linear);
        yield return new WaitForSeconds(flyingTime);
        animator.enabled = false;
        PoolingManager.Instance.SetPool(gameObject, "LightFlyingBird");
        yield break;
    }
}
