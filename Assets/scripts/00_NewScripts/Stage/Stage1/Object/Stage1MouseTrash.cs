﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage1MouseTrash : StageInfinitySpriteObject
{
    [SerializeField]
    private Collider2D eventItemCollider;
    [SerializeField]
    private SpriteRenderer eventItemSpriteRender;
    protected override void StartEvent()
    {
        if(eventItem.GetComponent<StageItemBase>() != null)
        {
            string name = eventItem.GetComponent<StageItemBase>().ItemName;
            bool isCollect = SaveDataManager.Instance.ItemDiction[name];
            if (isCollect == true)
            {
                return;
            }
        }
        else
        {
            return;
        }

        eventItemCollider.enabled = true;
        eventItemSpriteRender.enabled = true;

        eventItem.DOMoveX(transform.position.x + 0.5f, 0.75f).SetEase(Ease.Linear);
    }
}
