﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage1CloudMovement : MonoBehaviour
{
    private enum State
    {
        LEFT, RIGHT
    }
    
    [SerializeField]
    [Header("위치 시작 X좌표")]
    private float leftStartXPose = 35.0f;
    [SerializeField]
    private float rightStartXPose = -40.0f;

    [SerializeField]
    [Header("구름 속도 범위")]
    private float[] cloudSpeedRange = new float[2];
    [SerializeField]
    [Header("구름 크기 범위")]
    private float[] cloudScaleRange = new float[2];
 
    private State nowState;
    private float nowSpeed;

    [SerializeField]
    [Header("시작할때 랜덤 포지션 할지 아니면 그 상태로 시작할지 정하는 부울값")]
    private bool startRandom;

    protected virtual void Start()
    {
        if(startRandom)
        {
            SetRandomValues();
            SetRandomPose();
        }

        else
        {
            SetRandomValues();
        }
    }

    protected virtual void Update()
    {
        MoveCloud();
    }

    private void MoveCloud()
    {
        switch (nowState)
        {
            case State.LEFT:
                transform.Translate(Vector2.left * Time.deltaTime * nowSpeed);
                break;
            case State.RIGHT:
                transform.Translate(Vector2.right * Time.deltaTime * nowSpeed);
                break;
        }

        if (transform.position.x <= rightStartXPose || transform.position.x >= leftStartXPose)
        {
            SetRandomValues();
            SetRandomPose();
        }
    }

    private void SetRandomValues()
    {
        int sight = Random.Range(0, 2);
        nowSpeed = Random.Range(0.75f, 2.0f);
        nowState = (State)Random.Range((float)State.LEFT, (float)State.RIGHT + 1);
        float randomScale = Random.Range(cloudScaleRange[0], cloudScaleRange[1]);

        transform.localScale = Vector2.one * randomScale;

        if (sight == 0)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
        else
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
    }

    private void SetRandomPose()
    {
        switch (nowState)
        {
            case State.LEFT:
                transform.position = new Vector2(leftStartXPose, Random.Range(10f, 13f));
                break;
            case State.RIGHT:
                transform.position = new Vector2(rightStartXPose, Random.Range(10f, 13f));
                break;
        }
    }
}
