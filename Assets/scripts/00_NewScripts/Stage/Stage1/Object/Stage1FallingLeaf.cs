﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage1FallingLeaf : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer spriteRenderer;

    [SerializeField]
    private Sprite[] leafSprites;

    [SerializeField]
    private float motionTime;

    [SerializeField]
    private float moveSpeed;

    private float currentTime = 0;

    private void OnEnable()
    {
        spriteRenderer.sprite = leafSprites[Random.Range(0, leafSprites.Length)];
        transform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));
    }

    private void Update()
    {
        currentTime += Time.deltaTime;
        MoveTransform();
        SetRenderAlpha();

        if(currentTime >= motionTime)
        {
            PoolingManager.Instance.SetPool(gameObject, "Leaf");
        }
    }

    private void MoveTransform()
    {
        Vector2 movePosition = transform.position;
        movePosition.y -= moveSpeed * Time.deltaTime;
        transform.position = movePosition;
    }

    private void SetRenderAlpha()
    {
        Color newColor = spriteRenderer.color;
        newColor.a -= Time.deltaTime / motionTime;
        spriteRenderer.color = newColor;
    }

    private void OnDisable()
    {
        spriteRenderer.color = Color.white;
        currentTime = 0;
    }

    public void SetRandomPosition(in float xValue1, in float xValue2, in float yValue1, in float yValue2)
    {
        float randomValueX = Random.Range(xValue1, xValue2);
        float randomValueY = Random.Range(yValue1, yValue2);
        Vector3 randomPosition = new Vector3(randomValueX, randomValueY);
        transform.position = randomPosition;
    }
}
