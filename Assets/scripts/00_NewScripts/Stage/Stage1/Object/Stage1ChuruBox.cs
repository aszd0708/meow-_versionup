﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage1ChuruBox : StageInfinitySpriteObject
{
    [SerializeField]
    private Collider2D eventItemCollider;
    [SerializeField]
    private SpriteRenderer eventItemSpriteRender;
    protected override void StartEvent()
    {
        if(eventItemCollider.gameObject.activeSelf == false) { return; }
        eventItemCollider.enabled = true;
        eventItemSpriteRender.enabled = true;

        eventItem.DOJump(new Vector3(eventItem.position.x + 2, eventItem.position.y, eventItem.position.z), 1.5f, 1, 1.0f);

        eventItem = null;
    }
}
