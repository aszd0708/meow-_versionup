﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage1LightCreateBird : MonoBehaviour, TouchableObj
{
    [SerializeField]
    [Header("생성할 새")]
    private GameObject bird;

    public Vector3[] XY = new Vector3[6];

    public bool DoTouch(Vector2 touchPose = default)
    {
        Vector3 location = XY[Random.Range(0, 6)];

        GameObject createBird = PoolingManager.Instance.GetPool("LightFlyingBird");
        if (!createBird)
            createBird = Instantiate(bird);

        createBird.transform.SetParent(transform);
        createBird.transform.localPosition = location;

        AudioManager.Instance.PlaySound("slide_4", transform.position);
        return false;
    }
}
