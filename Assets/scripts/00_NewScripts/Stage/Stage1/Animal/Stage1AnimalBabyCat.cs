﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage1AnimalBabyCat : StageAnimalObjectBase
{
    private bool canTouch = true;

    public float shakeAmount_X;
    public float shakeAmount_Z;

    public float stopTime;
    public float vibeTime;

    public Sprite firstBody;
    public Sprite firstEye;

    public GameObject collectEffect;

    [SerializeField]
    [Header("아이템이 있을 자리")]
    private Transform itemPose;

    [SerializeField]
    private string sound;

    [SerializeField]
    private int leavesCountMin, leavesCountMax;

    [SerializeField]
    private GameObject leafObj;

    [SerializeField]
    private Vector2 leafCreatePosition1, leafCreatePosition2;

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if(endItemMotion)
        {
            if (canTouch)
            {
                StartCoroutine(_PlayMotion());
            }

            SetCollect();
        }

        else
        {
            StartCoroutine(CreateLeaves());
        }

        AudioManager.Instance.PlaySound("baby_cat", transform.position);

        return false;
    }

    public override void StartItemEvent(GameObject itemObj)
    {
        if (endItemMotion)
        {
            itemObj.SetActive(false);
            return;
        }
        base.StartItemEvent(itemObj);

        for (int i = 0; i < gameObject.transform.childCount; i++)
            transform.GetChild(i).GetComponent<SpriteRenderer>().sortingOrder = 2;

        itemObj.transform.position = itemPose.position;

        StartCoroutine(_PlayIdleMotion());
    }

    private IEnumerator _PlayMotion()
    {
        canTouch = false;
        VibeBody();
        StartCoroutine(CreateLeaves());
        yield return new WaitForSeconds(vibeTime);
        canTouch = true;
        yield break;
    }

    /// <summary>
    /// 떨때 생성될 풀들
    /// </summary>
    private IEnumerator CreateLeaves()
    {
        WaitForSeconds wait = new WaitForSeconds(Random.Range(0.1f, 0.2f));
        int createCount = Random.Range(leavesCountMin, leavesCountMax);
        for(int i = 0; i < createCount; i++)
        {
            Vector3 createPosition = new Vector3(Random.Range(leafCreatePosition1.x, leafCreatePosition2.x),
    Random.Range(leafCreatePosition1.y, leafCreatePosition2.y));

            GameObject leaf = PoolingManager.Instance.GetPool("Leaf");
            if(leaf == null)
            {
                leaf = Instantiate(leafObj);
            }
            leaf.transform.position = createPosition;

            yield return wait;
        }
        yield break;
    }

    /// <summary>
    /// 터치했을 때 부들부들 떠는 함수
    /// </summary>
    private void VibeBody()
    {
        iTween.ShakePosition(gameObject, iTween.Hash(
                    "x", shakeAmount_X,
                    "time", vibeTime));

        iTween.ShakeRotation(gameObject, iTween.Hash(
            "z", shakeAmount_Z,
            "time", vibeTime));
    }

    /// <summary>
    /// 가만히 있는 모션
    /// </summary>
    /// <returns></returns>
    private IEnumerator _PlayIdleMotion()
    {
        WaitForSeconds vibeWait = new WaitForSeconds(vibeTime);
        WaitForSeconds stopWait = new WaitForSeconds(stopTime);
        while (true)
        {
            transform.GetChild(2).GetComponent<SpriteRenderer>().enabled = true;
            transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = firstBody;
            transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = firstEye;
            VibeBody();
            yield return vibeWait;
            transform.GetChild(2).GetComponent<SpriteRenderer>().enabled = false;
            yield return stopWait;
        }
    }
}
