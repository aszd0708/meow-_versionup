﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage1AnimalYellowCat : StageAnimalObjectBase
{
    /// <summary>
    /// 몸(0) 눈(1) 꼬리(2) 순
    /// </summary>
    [SerializeField]
    [Header("각 부분들 스프라이트 랜더러")]
    private SpriteRenderer bodySpriteRenderer;
    [SerializeField]
    private SpriteRenderer eyeSpriteRenderer, tailSpriteRenderer;

    [Header("각 부분들 Sprite들(동작에 필요함)")]
    public Sprite[] moveBodySprites = new Sprite[2];
    public Sprite catchMouse;
    public Sprite[] eyeSprites = new Sprite[2];

    [SerializeField]
    [Header("자신 애니메이터")]
    private Animator animator;

    [Header("모션에 필요한 좌표 및 속도")]
    public float speed;
    public Vector2 eyePos;
    private Vector3 originPos;

    [SerializeField]
    [Header("아이템이 나오는 곳")]
    private Transform hitBox;

    private bool canTouch = false;

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if(canTouch)
        {
            SetCollect();
            AudioManager.Instance.PlaySound("cat_11", transform.position);
        }
        return false;
    }

    public override void StartItemEvent(GameObject itemObj)
    {
        if (endItemMotion)
        {
            itemObj.SetActive(false);
            return;
        }
        base.StartItemEvent(itemObj);
        itemObj.transform.position = hitBox.position;

        StartCoroutine(_StartMotion(itemObj.transform));
    }

    private IEnumerator _StartMotion(Transform item)
    {
        bodySpriteRenderer.sortingLayerName = "2";
        eyeSpriteRenderer.sortingLayerName = "2";
        tailSpriteRenderer.sortingLayerName = "2";

        animator.enabled = false;

        #region 몸 움직이는 부분
        transform.DOMove(new Vector3(item.position.x - 1, item.position.y, item.position.z), speed).SetEase(Ease.Linear);
        for (float i = 0; i < speed;)
        {
            if (bodySpriteRenderer.sprite == moveBodySprites[0])
                bodySpriteRenderer.sprite = moveBodySprites[1];
            else if (bodySpriteRenderer.sprite == moveBodySprites[1])
                bodySpriteRenderer.sprite = moveBodySprites[0];
            i += speed / 4;
            yield return new WaitForSeconds(speed / 4);
        }
        #endregion

        #region 눈 깜빡이고 쥐를 잡는 부분
        eyeSpriteRenderer.sprite = eyeSprites[0];
        yield return new WaitForSeconds(1.0f);
        bodySpriteRenderer.sprite = catchMouse;
        eyeSpriteRenderer.sprite = eyeSprites[1];
        originPos = eyeSpriteRenderer.transform.position;
        eyeSpriteRenderer.transform.position = new Vector3(eyeSpriteRenderer.transform.position.x, 
                                                           eyeSpriteRenderer.transform.position.y - 0.2f,
                                                           eyeSpriteRenderer.transform.position.z);
        item.gameObject.SetActive(false);
        AudioManager.Instance.PlaySound("cat_11", transform.position);
        yield return new WaitForSeconds(0.25f);
        #endregion
        animator.enabled = true;
        canTouch = true;
        yield break;
    }
}
