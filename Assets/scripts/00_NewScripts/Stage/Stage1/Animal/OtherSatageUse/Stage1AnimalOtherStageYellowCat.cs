﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage1AnimalOtherStageYellowCat : MonoBehaviour, TouchableObj
{
    /// <summary>
    /// 몸(0) 눈(1)
    /// </summary>
    [SerializeField]
    [Header("각 부분들 스프라이트 랜더러")]
    private SpriteRenderer bodySpriteRenderer;
    [SerializeField]
    private SpriteRenderer eyeSpriteRenderer;

    [Header("각 부분들 Sprite들(동작에 필요함)")]
    public Sprite catchMouse;
    public Sprite idleBodySprite;
    public Sprite[] eyeSprites = new Sprite[2];

    [SerializeField]
    [Header("자신 애니메이터")]
    private Animator animator;

    [Header("모션에 필요한 좌표 및 속도")]
    public float speed;
    public Vector2 eyePos;
    private Vector3 originPos;

    private bool canTouch = true;

    public bool DoTouch(Vector2 touchPose = default)
    {
        if (canTouch)
        {
            StartCoroutine(_StartMotion());
        }
        return false;
    }

    private IEnumerator _StartMotion()
    {
        canTouch = false;
        animator.enabled = false;
        originPos = eyeSpriteRenderer.transform.position;
        yield return new WaitForSeconds(0.5f);
        bodySpriteRenderer.sprite = catchMouse;
        eyeSpriteRenderer.sprite = eyeSprites[1];
        originPos = eyeSpriteRenderer.transform.position;
        eyeSpriteRenderer.transform.position = new Vector3(eyeSpriteRenderer.transform.position.x,
                                                           eyeSpriteRenderer.transform.position.y - 0.2f,
                                                           eyeSpriteRenderer.transform.position.z);
        yield return new WaitForSeconds(1.0f);
        bodySpriteRenderer.sprite = idleBodySprite;
        eyeSpriteRenderer.transform.position = originPos;
        eyeSpriteRenderer.sprite = eyeSprites[0];
        animator.enabled = true;
        canTouch = true;
        yield break;
    }
}
