﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Stage1AnimalOtherStageBabyCat : MonoBehaviour, TouchableObj
{
    [SerializeField]
    [Header("아기고양이 RectTrasnform")]
    public RectTransform babyCatUIRect;

    [Header("입 부분 img 컴포넌트")]
    public Image mouse;
    [Header("입 스프라이트들")]
    public Sprite[] mouseSprite = new Sprite[2];
    private Vector2 originPos;
    private bool canTouch = true;

    void Start()
    {
        originPos = babyCatUIRect.anchoredPosition;
        babyCatUIRect.localScale = new Vector3(0, 0, 1);
    }

    private IEnumerator _PlayMouthMotion()
    {
        WaitForSeconds halfSecondWait = new WaitForSeconds(0.5f);
        int index = 0;
        for(int i = 0; i < 5; i++)
        { 
            index++;
            index %= mouseSprite.Length;
            mouse.sprite = mouseSprite[index];
            yield return halfSecondWait;
        }
        yield break;
    }

    private IEnumerator _MoveUI()
    {
        babyCatUIRect.localScale = new Vector3(1, 1, 1);
        canTouch = false;
        babyCatUIRect.DOAnchorPosY(0f, 0.15f);
        AudioManager.Instance.PlaySound("baby_cat2", transform.position);
        yield return new WaitForSeconds(2.0f);
        babyCatUIRect.DOAnchorPosY(originPos.y, 0.5f);
        yield return new WaitForSeconds(0.5f);
        babyCatUIRect.localScale = new Vector3(0, 0, 1);
        canTouch = true;
        yield break;
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        if(!canTouch)
        {
            return false;
        }

        StartCoroutine(_MoveUI());
        StartCoroutine(_PlayMouthMotion());
        return false;
    }
}
