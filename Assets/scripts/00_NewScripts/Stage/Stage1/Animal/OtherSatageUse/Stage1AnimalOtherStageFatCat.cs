﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage1AnimalOtherStageFatCat : MonoBehaviour, TouchableObj
{
    public SpriteRenderer body;
    public SpriteRenderer eye;
    public SpriteRenderer tail;
    public Sprite[] catSprite = new Sprite[3];
    public Sprite[] eyeSprite = new Sprite[2];
    private bool touch;
    private Coroutine cor = null;
    // Start is called before the first frame update

    private void Start()
    {
        touch = true;
        catSprite[0] = body.GetComponent<SpriteRenderer>().sprite;
        eyeSprite[0] = eye.GetComponent<SpriteRenderer>().sprite;
        cor = StartCoroutine(IdleMotion());
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        if (touch)
        {
            StopCoroutine(cor);
            StartCoroutine(RollingMotion());
            return false;
        }
        return false;
    }

    public void OhterStageAnimalOnClick()
    {
        
    }

    private IEnumerator IdleMotion()
    {
        body.sprite = catSprite[0];
        eye.sprite = eyeSprite[0];
        yield return new WaitForSeconds(1.0f);
        eye.sprite = eyeSprite[1];
        body.sprite = catSprite[1];
        yield return new WaitForSeconds(0.5f);
        StopCoroutine(cor);
        yield return cor = StartCoroutine(IdleMotion());
    }

    private IEnumerator RollingMotion()
    {
        StopCoroutine(cor);
        touch = false;
        eye.enabled = false;
        tail.enabled = false;
        transform.DORotate(new Vector3(0, 0, -60.0f), 0.5f);
        body.sprite = catSprite[2];
        yield return new WaitForSeconds(0.5f);
        transform.DORotate(new Vector3(0, 0, 60.0f), 1.0f);
        yield return new WaitForSeconds(1.0f);
        transform.DORotate(new Vector3(0, 0, 0), 0.5f);
        yield return new WaitForSeconds(0.5f);
        body.sprite = catSprite[0];
        eye.enabled = true;
        touch = true;
        tail.enabled = true;
        yield return cor = StartCoroutine(IdleMotion());
    }
}
