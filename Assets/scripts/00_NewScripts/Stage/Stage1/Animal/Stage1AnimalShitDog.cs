﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage1AnimalShitDog : StageAnimalObjectBase
{
    [SerializeField]
    [Header("각 눈 스프라이트 랜더러")]
    private SpriteRenderer leftEyeSpriteRenderer;
    [SerializeField]
    private SpriteRenderer rightEyeSpriteRenderer;

    [Header("각 눈 스프라이트")]
    public Sprite[] leftEye = new Sprite[2];
    public Sprite[] rightEye = new Sprite[2];

    [Header("흔들때 필요한 변수")]
    public float shakeAmount_X = 1;
    public float shakeAmount_Z = 20;
    public float shakeDuration = 1;
    public float motionTime = 3;

    [Header("만들 똥")]
    public GameObject shitObj;
    [Header("똥 만드는 곳")]
    public Transform createTransform;
    [Header("똥이 사라지는 시간")]
    public float shitDeleteTime = 10;

    private bool canTouch = true;

    WaitForSeconds wait;
    private void Start()
    {
        wait = new WaitForSeconds(shakeDuration);
    }

    public override bool DoTouch(Vector2 touchPose = default)
    {
        SetCollect();

        if (canTouch)
        {
            StartCoroutine(_StartMotion());
            AudioManager.Instance.PlaySound("dog_2", transform.position);
        }

        return false;
    }

    private IEnumerator _StartMotion()
    {
        canTouch = false;
        iTween.ShakePosition(gameObject, iTween.Hash(
                        "x", shakeAmount_X,
                        "y", 0f,
                        "delay", 0f,
                        "time", shakeDuration));
        iTween.ShakeRotation(gameObject, iTween.Hash(
                        "x", 0,
                        "z", shakeAmount_Z,
                        "delay", 0f,
                        "time", shakeDuration));
        leftEyeSpriteRenderer.sprite = leftEye[1];
        rightEyeSpriteRenderer.sprite = rightEye[1];
        yield return wait;
        leftEyeSpriteRenderer.sprite = leftEye[0];
        rightEyeSpriteRenderer.sprite = rightEye[0];
        CreateShit();
        canTouch = true;
        yield break;
    }

    void CreateShit()
    {
        GameObject shit;
        Vector3 createPose = createTransform.position;

        shit = PoolingManager.Instance.GetPool("Shit");
        if (shit == null)
            shit = Instantiate(shitObj);
        shit.transform.position = createPose;
        shit.transform.rotation = Quaternion.identity;
        PoolingManager.Instance.SetPool(shit, "Shit", shitDeleteTime);
    }
}
