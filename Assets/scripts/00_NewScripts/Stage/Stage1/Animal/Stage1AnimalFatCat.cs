﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage1AnimalFatCat : StageAnimalObjectBase
{
    [SerializeField]
    [Header("츄르 Transform")]
    private Transform chureTransform;

    [SerializeField]
    [Header("각 부분 SpriteRenderer")]
    private SpriteRenderer bodySpriteRenderer;
    [SerializeField]
    private SpriteRenderer eyeSpriteRenderer;

    [SerializeField]
    private SpriteRenderer tailRender;

    [SerializeField]
    [Header("굴러오는 속도")]
    private float speed;

    [SerializeField]
    [Header("모션에 필요한 Sprite")]
    private Sprite movingSprite;
    [SerializeField]
    private Sprite idleSprite;

    [SerializeField]
    [Header("Animaltor(자신)")]
    private Animator animator;

    private bool canTouch = false;

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if (canTouch)
        {
            SetCollect();
            AudioManager.Instance.PlaySound("fat_cat", transform.position);
        }
        return false;
    }

    public override void StartItemEvent(GameObject itemObj)
    {
        if (endItemMotion)
        {
            itemObj.SetActive(false);
            return;
        }
        base.StartItemEvent(itemObj);
        itemObj.transform.position = chureTransform.position;
        StartCoroutine(_StartMotion(itemObj));
    }

    private IEnumerator _StartMotion(GameObject item)
    {
        animator.enabled = false;
        tailRender.enabled = false;
        bodySpriteRenderer.sprite = movingSprite;
        eyeSpriteRenderer.enabled = false;
        transform.DORotate(new Vector3(0, 0, 720), speed, RotateMode.FastBeyond360).SetEase(Ease.Linear);
        transform.DOMove(chureTransform.position, speed).SetEase(Ease.Linear);
        yield return new WaitForSeconds(speed);
        AudioManager.Instance.PlaySound("fat_cat", transform.position);
        item.SetActive(false);
        bodySpriteRenderer.sprite = idleSprite;
        eyeSpriteRenderer.enabled = true;
        animator.enabled = true;
        tailRender.enabled = true;
        canTouch = true;
        yield break;
    }
}
