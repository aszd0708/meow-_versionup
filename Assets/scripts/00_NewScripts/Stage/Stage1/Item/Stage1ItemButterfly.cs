﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 나비 움직임
/// 상속 받아서 움직임 구현
/// </summary>
public class Stage1ItemButterfly : StageItemBase
{
    public enum State
    {
        left, right, idle
    }
    public GameObject trash;
    public GameObject line;
    public float lineTime = 0.3f;
    public float length;
    public float speed;
    public State state;
    private float y;
    private float firstY;

    private bool isTouch;

    private bool isCollect;

    public override bool DoTouch(Vector2 touchPose = default)
    {
        isTouch = true;
        DoTouchAction();
        SaveDataManager.Instance.EditItemCollect(ItemName);
        return false;
    }

    void Start()
    {
        firstY = transform.position.y;
        y = transform.position.y;
        state = State.left;

        isCollect = SaveDataManager.Instance.ItemDiction[ItemName];
    }

    private void Update()
    {
        if (isTouch || isCollect)
        {
            return;
        }

        y -= Time.deltaTime;

        switch (state)
        {
            case State.right:
                transform.position = new Vector3(transform.position.x + Time.deltaTime * speed, (Mathf.Sin(y) * length) + firstY, transform.position.z);
                break;
            case State.left:
                transform.position = new Vector3(transform.position.x - Time.deltaTime * speed, (Mathf.Sin(y) * length) + firstY, transform.position.z);
                break;
            case State.idle:
                return;
        }

        if (transform.position.x <= -19)
        {
            state = State.right;
            transform.rotation = new Quaternion(0, transform.rotation.y - 180, 0, 0);
        }

        else if (transform.position.x >= 19)
        {
            state = State.left;
            transform.rotation = new Quaternion(0, 0, 0, 0);
        }

        Vector3 nowPos = new Vector3(transform.position.x, transform.position.y, line.transform.position.z);

        StartCoroutine(_MakeLine(nowPos));
    }

    private IEnumerator _MakeLine(Vector3 position)
    {
        yield return new WaitForSeconds(lineTime);
        GameObject createLine = PoolingManager.Instance.GetPool("Line");
        if (createLine == null)
        {
            createLine = Instantiate(line);
        }

        createLine.transform.SetParent(trash.transform);
        createLine.transform.position = position;
        yield break;
    }
}
