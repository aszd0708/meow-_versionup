﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage0TutorialItemButtonController : MonoBehaviour
{
    [SerializeField]
    private StageItemUIDragAndDrop itemDrag;

    private void OnEnable()
    {
        Stage0TutorialManager.Instance.ItemButton = this;
        SetDrag(false);
    }

    public void SetDrag(bool bCanDrag)
    {
        itemDrag.enabled = bCanDrag;
    }
}
