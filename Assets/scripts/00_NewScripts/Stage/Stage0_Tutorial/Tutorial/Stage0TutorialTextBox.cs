﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Stage0TutorialTextBox : MonoBehaviour
{
    public RectTransform scriptBox;
    private Vector2 originScale;
    private Vector2 originScale_T;
    public Text tutorialText;
    public RectTransform tutorialTextRect;
    public float motionTime;

    private void OnEnable()
    {
        originScale = scriptBox.sizeDelta;
        originScale_T = tutorialTextRect.sizeDelta;

        scriptBox.sizeDelta = Vector2.zero;
        tutorialTextRect.sizeDelta = Vector2.zero;
    }

    public void AppearTextBox(Vector2 movePose, bool isFlip)
    {
        if (isFlip)
        {
            scriptBox.localRotation = Quaternion.Euler(0, 0, 180);
            tutorialTextRect.localRotation = Quaternion.Euler(0, 0, 180);
        }

        else
        {
            scriptBox.localRotation = Quaternion.identity;
            tutorialTextRect.localRotation = Quaternion.identity;
        }

        StartCoroutine(_AppearTextBox(movePose));
    }

    public void SetRotationTextBox(Vector3 rotation)
    {
        scriptBox.localRotation = Quaternion.Euler(rotation.x, rotation.y, rotation.z);
        tutorialTextRect.localRotation = Quaternion.Euler(rotation.x, rotation.y, rotation.z);
    }

    public void DisappearTextbox()
    {
        StartCoroutine(_DisappearTextbox());
    }

    public void SetText(string script)
    {
        tutorialText.text = script;
    }

    private IEnumerator _AppearTextBox(Vector2 movePose) // 텍스트 박스 나오게 하는 함수
    {
        tutorialText.enabled = true;
        scriptBox.anchoredPosition = movePose;
        //boxTransform.sizeDelta = new Vector2(0, 0);
        scriptBox.DOSizeDelta(originScale, motionTime).SetEase(Ease.OutElastic);
        tutorialTextRect.DOSizeDelta(originScale_T, motionTime).SetEase(Ease.OutElastic);
        yield return new WaitForSeconds(motionTime);
    }

    private IEnumerator _DisappearTextbox() // 텍스트 박스 사라지게 하는 함수
    {
        tutorialText.enabled = false;
        scriptBox.sizeDelta = originScale;
        scriptBox.DOSizeDelta(Vector2.zero, motionTime);
        tutorialTextRect.sizeDelta = originScale_T;
        tutorialTextRect.DOSizeDelta(Vector2.zero, motionTime);
        yield return new WaitForSeconds(motionTime);
    }
}
