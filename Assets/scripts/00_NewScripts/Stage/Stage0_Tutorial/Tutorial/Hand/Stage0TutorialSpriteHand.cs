﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage0TutorialSpriteHand : MonoBehaviour
{
    public float distance;
    public float time;
    public float pickTime;
    public float size;
    public enum State { left, right, up, down, pick, idle }
    public Vector3[] handPosition;
    public Sprite[] pickSpirte = new Sprite[2];

    private Sprite handSprite;
    private SpriteRenderer handRenderer;

    private Coroutine motionCor;
    // Start is called before the first frame update
    void Awake()
    {
        handRenderer = GetComponent<SpriteRenderer>();
        handSprite = handRenderer.sprite;
    }

    void Start()
    {
        StopMotion();
    }

    public void MoveToPose(State state, Vector3 movePos)
    {
        transform.position = movePos;
        handRenderer.enabled = true;
        motionCor = StartCoroutine(_PlayMotion(state));
    }

    public void StopMotion()
    {
        transform.DOPause();
        transform.localScale = Vector3.one;
        handRenderer.enabled = false;
        if(motionCor != null)
        {
            StopCoroutine(motionCor);
        }
    }

    private IEnumerator _PlayMotion(State state)
    {
        WaitForSeconds wait = new WaitForSeconds(time);
        while (true)
        {
            switch (state)
            {
                case State.left:
                    handRenderer.sprite = handSprite;
                    transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 90);
                    transform.DOScale(new Vector2(transform.localScale.x + size, transform.localScale.y - size), time);
                    yield return wait;
                    transform.DOLocalMoveX(transform.localPosition.x - distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size * 2, transform.localScale.y + size * 2), time);
                    yield return wait;
                    transform.DOScale(new Vector2(transform.localScale.x + size * 2, transform.localScale.y - size * 2), time);
                    yield return wait;
                    transform.DOLocalMoveX(transform.localPosition.x + distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size, transform.localScale.y + size), time);
                    yield return wait;
                    yield return wait;
                    break;
                case State.right:
                    handRenderer.sprite = handSprite;
                    transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 270);
                    transform.DOScale(new Vector2(transform.localScale.x + size, transform.localScale.y - size), time);
                    yield return wait;
                    transform.DOLocalMoveX(transform.localPosition.x + distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size * 2, transform.localScale.y + size * 2), time);
                    yield return wait;
                    transform.DOScale(new Vector2(transform.localScale.x + size * 2, transform.localScale.y - size * 2), time);
                    yield return wait;
                    transform.DOLocalMoveX(transform.localPosition.x - distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size, transform.localScale.y + size), time);
                    yield return wait;
                    yield return wait;
                    break;
                case State.up:
                    handRenderer.sprite = handSprite;
                    transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 0);
                    transform.DOScale(new Vector2(transform.localScale.x + size, transform.localScale.y - size), time);
                    yield return wait;
                    transform.DOLocalMoveY(transform.localPosition.y - distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size * 2, transform.localScale.y + size * 2), time);
                    yield return wait;
                    transform.DOScale(new Vector2(transform.localScale.x + size * 2, transform.localScale.y - size * 2), time);
                    yield return wait;
                    transform.DOLocalMoveY(transform.localPosition.y + distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size, transform.localScale.y + size), time);
                    yield return wait;
                    yield return wait;
                    break;
                case State.down:
                    handRenderer.sprite = handSprite;
                    transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 180);
                    transform.DOScale(new Vector2(transform.localScale.x + size, transform.localScale.y - size), time);
                    yield return wait;
                    transform.DOLocalMoveY(transform.localPosition.y - distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size * 2, transform.localScale.y + size * 2), time);
                    yield return wait;
                    transform.DOScale(new Vector2(transform.localScale.x + size * 2, transform.localScale.y - size * 2), time);
                    yield return wait;
                    transform.DOLocalMoveY(transform.localPosition.y + distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size, transform.localScale.y + size), time);
                    yield return wait;
                    yield return wait;
                    break;
                case State.pick:
                    handRenderer.sprite = pickSpirte[1];
                    yield break;
                case State.idle:

                    yield break;
            }
        }
    }
}
