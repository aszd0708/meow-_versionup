﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Stage0TutorialUIHand : MonoBehaviour
{
    public float distance;
    public float time;
    public float size;

    public enum State { left, right, up, down, pick, idle }

    public enum Location { AnimalBTN, ItemBTN }

    public Sprite[] handSprite = new Sprite[2];

    private Vector2 originSize;

    private new Image renderer;

    private RectTransform rect;

    private Coroutine motionCor;
    void Awake()
    {
        renderer = GetComponent<Image>();
        rect = GetComponent<RectTransform>();

        originSize = rect.localScale ;
    }

    public void StopMotion()
    {
        if(motionCor != null)
        {
            StopCoroutine(motionCor);
        }
        renderer.enabled = false;
        rect.localScale = originSize;
    }

    public void MoveToPose(State state, Vector2 movePose)
    {
        renderer.enabled = true;
        rect.anchoredPosition = movePose;
        renderer.sprite = handSprite[1];
        rect.localScale = originSize;
        //rect.localScale = Vector2.zero;

        switch (state)
        {
            case State.left:
                transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 90);
                break;
            case State.right:
                transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 270);
                break;
            case State.up:
                transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 0);
                break;
            case State.down:
                transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 180);
                break;
            case State.pick:
                transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 0);
                break;

        }

        motionCor = StartCoroutine(_PlayMotion(state));
    }

    private IEnumerator _PlayMotion(State state)
    {
        WaitForSeconds wait = new WaitForSeconds(time);
        while (true)
        {
            switch (state)
            {
                case State.left:
                    renderer.sprite = handSprite[0];
                    transform.DOScale(new Vector2(transform.localScale.x + size, transform.localScale.y - size), time);
                    yield return wait;
                    transform.DOLocalMoveX(transform.localPosition.x - distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size * 2, transform.localScale.y + size * 2), time);
                    yield return wait;
                    transform.DOScale(new Vector2(transform.localScale.x + size * 2, transform.localScale.y - size * 2), time);
                    yield return wait;
                    transform.DOLocalMoveX(transform.localPosition.x + distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size, transform.localScale.y + size), time);
                    yield return wait;
                    break;
                case State.right:
                    renderer.sprite = handSprite[0];
                    transform.DOScale(new Vector2(transform.localScale.x + size, transform.localScale.y - size), time);
                    yield return wait;
                    transform.DOLocalMoveX(transform.localPosition.x + distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size * 2, transform.localScale.y + size * 2), time);
                    yield return wait;
                    transform.DOScale(new Vector2(transform.localScale.x + size * 2, transform.localScale.y - size * 2), time);
                    yield return wait;
                    transform.DOLocalMoveX(transform.localPosition.x - distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size, transform.localScale.y + size), time);
                    yield return wait;
                    break;
                case State.up:
                    renderer.sprite = handSprite[0];
                    transform.DOScale(new Vector2(transform.localScale.x + size, transform.localScale.y - size), time);
                    yield return wait;
                    transform.DOLocalMoveY(transform.localPosition.y - distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size * 2, transform.localScale.y + size * 2), time);
                    yield return wait;
                    transform.DOScale(new Vector2(transform.localScale.x + size * 2, transform.localScale.y - size * 2), time);
                    yield return wait;
                    transform.DOLocalMoveY(transform.localPosition.y + distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size, transform.localScale.y + size), time);
                    yield return wait;
                    break;
                case State.down:
                    renderer.sprite = handSprite[0];
                    transform.DOScale(new Vector2(transform.localScale.x + size, transform.localScale.y - size), time);
                    yield return wait;
                    transform.DOLocalMoveY(transform.localPosition.y - distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size * 2, transform.localScale.y + size * 2), time);
                    yield return wait;
                    transform.DOScale(new Vector2(transform.localScale.x + size * 2, transform.localScale.y - size * 2), time);
                    yield return wait;
                    transform.DOLocalMoveY(transform.localPosition.y + distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size, transform.localScale.y + size), time);
                    yield return wait;
                    break;
                case State.pick:
                    renderer.sprite = handSprite[1];
                    yield break;
                case State.idle:
                    yield return null;
                    break;
            }
        }
    }
}
