﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StageReset : MonoBehaviour
{
    [SerializeField]
    private int[] stageAnimaIndex;

    [SerializeField]
    private string[] stageItemStrings;

    [SerializeField]
    private string currentStageName;

    public string CurrentStageName { get => currentStageName; }

    private void Start()
    {
        if(string.IsNullOrEmpty(currentStageName))
         {
            currentStageName = SceneManager.GetActiveScene().name;
        }
    }

    public void ResetAndRestartStage()
    {
        ResetStageData();
        RestartStege();
    }

    public void ResetStageData()
    {
        SaveDataManager.Instance.ResetData(stageAnimaIndex, stageItemStrings);
    }

    public void RestartStege()
    {
        if (string.IsNullOrEmpty(currentStageName))
        {
            return;
        }
        SceneChangeManager.Instance.ChangeScene(CurrentStageName);
    }
}
