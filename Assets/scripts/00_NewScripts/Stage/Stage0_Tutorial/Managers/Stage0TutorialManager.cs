﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

[Serializable]
/// <summary>
/// 튜토리얼에 필요한 좌표 및 텍스트 구조체
/// </summary>
public struct Stage0TutorialValues
{
    public Vector2[] handUIPoses;
    
    public Vector2[] handSpritePoses;

    public Vector2[] tutorialTextBoxPoses;

    public string[] tutorialScripts;
}

public class Stage0TutorialManager : Singleton<Stage0TutorialManager>
{
    public enum TutorialPrograss
    {
        Start,

        BeforeAnimalButton,
        AfterAnimalButton,
        
        BeforeNonItemAnimal,
        AfterNonItemAnimal,

        BeforeBackground,
        AfterBackground,

        BeforeItem,
        AfterItem,

        BeforeItemEvent,
        AfterItemEvent,

        AfterAnimalCollect,

        End
    }


    private Coroutine nowCor;

    [SerializeField]
    private float textTime = 2.0f;
    [SerializeField]
    private float textBoxTime = 2.0f;

    [SerializeField]
    private TutorialPrograss nowPrograss = TutorialPrograss.Start;
    public TutorialPrograss NowPrograss { get => nowPrograss; set => nowPrograss = value; }
    public bool CanNextPrograss 
    { 
        get => canNextPrograss;
        set
        {
            canNextPrograss = value;
            StageTouchManagerBase.Instance.ForbitTouch(canNextPrograss);
        }
    }


    private bool canNextPrograss = true;

    [Header("손들")]
    [SerializeField]
    private Stage0TutorialUIHand handUI;
    [SerializeField]
    private Stage0TutorialSpriteHand handSprite;

    [SerializeField]
    private Stage0TutorialTextBox textBox;

    [SerializeField]
    [Header("튜토리얼에 필요한 좌표 및 텍스트(텍스트는 안넣어도 알아서 들감)")]
    public Stage0TutorialValues values;

    public UnityEvent LastEvent;

    private Stage0TutorialItemButtonController itemButton;
    public Stage0TutorialItemButtonController ItemButton { get => itemButton; set => itemButton = value; }
    protected override void Awake()
    {
        base.Awake();
        SetTutorialText(ref values);
    }

    private void Start()
    {
        nowCor = StartCoroutine(_PlayPrograss(++NowPrograss));
    }

    public bool PlayPrograss(TutorialPrograss prograss)
    {
        if(!CanNextPrograss || (NowPrograss != prograss))
        {
            return false;
        }

        nowCor = StartCoroutine(_PlayPrograss(prograss));
        return true;
    }

    public bool CanPlayPrograss(TutorialPrograss prograss)
    {
        if (!CanNextPrograss || (NowPrograss != prograss))
        {
            return false;
        }
        else
        {
            return true;
        }
    }


    private IEnumerator _PlayPrograss(TutorialPrograss prograss)
    {
        switch(prograss)
        {
            case TutorialPrograss.Start:
                break;

            #region 고양이 버튼
            case TutorialPrograss.BeforeAnimalButton:
                StageTouchManagerBase.Instance.ForbitTouch(false);
                handUI.MoveToPose(Stage0TutorialUIHand.State.right,values.handUIPoses[0]);
                break;

            case TutorialPrograss.AfterAnimalButton:
                StageTouchManagerBase.Instance.ForbitTouch(true);
                CanNextPrograss = false;
                handUI.StopMotion();
                textBox.AppearTextBox(values.tutorialTextBoxPoses[0], false);
                textBox.SetText(values.tutorialScripts[0]);
                yield return new WaitForSeconds(textBoxTime);
                textBox.DisappearTextbox();
                nowCor = StartCoroutine(_PlayPrograss(TutorialPrograss.BeforeNonItemAnimal));
                CanNextPrograss = true;
                break;
            #endregion

            #region 아이템 안한 고양이
            case TutorialPrograss.BeforeNonItemAnimal:
                handSprite.MoveToPose(Stage0TutorialSpriteHand.State.down, values.handSpritePoses[0]);
                break;

            case TutorialPrograss.AfterNonItemAnimal:
                CanNextPrograss = false;
                handSprite.StopMotion();
                textBox.AppearTextBox(values.tutorialTextBoxPoses[1], false);
                textBox.SetText(values.tutorialScripts[1]);
                yield return new WaitForSeconds(textBoxTime);
                textBox.DisappearTextbox();
                nowCor = StartCoroutine(_PlayPrograss(TutorialPrograss.BeforeBackground));
                CanNextPrograss = true;
                break;
            #endregion

            #region 허공 터치
            case TutorialPrograss.BeforeBackground:
                handSprite.MoveToPose(Stage0TutorialSpriteHand.State.up, values.handSpritePoses[1]);
                break;

            case TutorialPrograss.AfterBackground:
                CanNextPrograss = false;
                handSprite.StopMotion();
                textBox.AppearTextBox(values.tutorialTextBoxPoses[2], true);
                textBox.SetText(values.tutorialScripts[2]);
                yield return new WaitForSeconds(textTime);
                textBox.SetText(values.tutorialScripts[3]);
                yield return new WaitForSeconds(textBoxTime);
                textBox.DisappearTextbox();
                nowCor = StartCoroutine(_PlayPrograss(TutorialPrograss.BeforeItem));
                CanNextPrograss = true;
                break;
            #endregion

            #region 아이템 터치
            case TutorialPrograss.BeforeItem:
                handSprite.MoveToPose(Stage0TutorialSpriteHand.State.up, values.handSpritePoses[2]);
                break;

            case TutorialPrograss.AfterItem:
                CanNextPrograss = false;
                handSprite.StopMotion();
                textBox.AppearTextBox(values.tutorialTextBoxPoses[3],false);
                textBox.SetText(values.tutorialScripts[4]);
                yield return new WaitForSeconds(textTime);
                textBox.SetText(values.tutorialScripts[5]);
                yield return new WaitForSeconds(textTime);
                textBox.SetText(values.tutorialScripts[6]);
                yield return new WaitForSeconds(textBoxTime);
                textBox.DisappearTextbox();
                nowCor = StartCoroutine(_PlayPrograss(TutorialPrograss.BeforeItemEvent));
                CanNextPrograss = true;
                break;
            #endregion

            #region 아이템 끌어놓기
            case TutorialPrograss.BeforeItemEvent:
                ItemButton.SetDrag(true);
                handUI.MoveToPose(Stage0TutorialUIHand.State.pick, values.handUIPoses[1]);
                break;

            case TutorialPrograss.AfterItemEvent:
                handUI.StopMotion();
                handSprite.MoveToPose(Stage0TutorialSpriteHand.State.down, values.handSpritePoses[3]);
                break;
            #endregion

            #region 동물 수집
            case TutorialPrograss.AfterAnimalCollect:
                CanNextPrograss = false;
                handSprite.StopMotion();
                textBox.AppearTextBox(values.tutorialTextBoxPoses[4], true);
                textBox.SetRotationTextBox(new Vector3(0, 180, 0));
                textBox.SetText(values.tutorialScripts[7]);
                yield return new WaitForSeconds(textBoxTime);
                textBox.DisappearTextbox();
                nowCor = StartCoroutine(_PlayPrograss(TutorialPrograss.End));
                CanNextPrograss = true;
                Debug.Log("끗");
                break;
            #endregion

            case TutorialPrograss.End:
                //끗
                //PlayerPrefs.SetInt("IsTutorialEnd", 1);
                LastEvent.Invoke();
                break;
        }
        NowPrograss++;
        yield break;
    }

    private void SetTutorialText(ref Stage0TutorialValues v)
    {
        v.tutorialScripts = new string[8];

        v.tutorialScripts[0] = "찾아야 할 동물에 대한 정보를\n볼 수 있어요.";

        v.tutorialScripts[1] = "어떤 동물은 좋아하는 것으로\n유인을 해야만 발견 할 수 있어요.";

        v.tutorialScripts[2] = "엉뚱한 곳을 눌렀을 경우\n터치 횟수가 차감 됩니다.";
        v.tutorialScripts[3] = "일정 시간이 지나거나 광고를 보면\n 터치 횟수는 다시 회복 됩니다.";

        v.tutorialScripts[4] = "획득한 아이템은\n가방에 들어갑니다.";
        v.tutorialScripts[5] = "가방을 눌러 고양이나 강아지가\n있을 것 같은 장소에";
        v.tutorialScripts[6] = "아이템을 끌어다 놓아봐요.";

        v.tutorialScripts[7] = "동물을 찾았습니다.\n찾은 동물은 하우스에 입주하게 됩니다.";
    }
    
    public void SetTutorialAnimalBTN()
    {
        PlayPrograss(TutorialPrograss.AfterAnimalButton);
    }

    public void SetTutorialAfterAnimalCollect()
    {
        PlayPrograss(TutorialPrograss.AfterAnimalCollect);
    }

    public void SetTutorialEndPlayerPreference()
    {
        PlayerPrefs.SetInt("IsTutorialEnd", 1);
    }
}
