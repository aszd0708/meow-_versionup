﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage0TouchManager : StageTouchManagerBase
{
    [Header("마이너스 이펙트")]
    public StageChanceMinImg minImg;
    [Header("지금 찬스 카운트 텍스트")]
    public StageChanceImg chanceText;

    int nowChanceCount;

    protected override void Awake()
    {
        base.Awake();
        nowChanceCount = 40;
        chanceText.ChangeNumber(nowChanceCount);
    }

    protected override void DoStageTouch()
    {
        base.DoStageTouch();

    }

    protected override bool MinusChance()
    {
        bool playProgress = Stage0TutorialManager.Instance.PlayPrograss(Stage0TutorialManager.TutorialPrograss.AfterBackground);
        if(playProgress)
        {
            minImg.CreateMinusEffect();
            nowChanceCount--;
            chanceText.ChangeNumber(nowChanceCount);
        }
        return true;
    }
}
