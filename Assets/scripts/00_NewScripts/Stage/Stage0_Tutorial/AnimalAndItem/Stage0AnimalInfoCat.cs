﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage0AnimalInfoCat : StageAnimalObjectBase
{
    public Sprite walkMotion1;
    public Sprite walkMotion2;
    private Sprite origin;

    public SpriteRenderer bodyRenderer, eyeRenderer, tailRenderer;

    [SerializeField]
    [Header("목적지 Transform")]
    private Transform destinationTransform;

    private bool canTouch;
    void Start()
    {
        origin = bodyRenderer.sprite;
    }

    public override bool DoTouch(Vector2 touchPose = default)
    {

        if(!canTouch)
        {
            Stage0TutorialManager.Instance.PlayPrograss(Stage0TutorialManager.TutorialPrograss.AfterNonItemAnimal);
            return false;
        }

        else
        {
            AudioManager.Instance.PlaySound("cat_5", transform.position);
        }

        if (!Info.IsCollect)
        {
            // 세이브 변경 및 사진 찍는거 등등 여러가지 들어감
            SaveDataManager.Instance.EditAnimalCollect(Info.No);
            StageUINewMarkController.Instance.SetAnimalHouseNewMark(true);
            Info.IsCollect = true;
        }
        StagePhotoShotManager.Instance.StartTakePickture(gameObject);

        return false;
    }

    public override void StartItemEvent(GameObject itemObj)
    {
        if (endItemMotion)
        {
            PoolingManager.Instance.SetPool(itemObj, "ItemDummy");
            return;
        }
        base.StartItemEvent(itemObj);
        StartCoroutine(_MoveToPose());
        itemObj.transform.position = destinationTransform.position;
    }

    private IEnumerator _MoveToPose()
    {
        yield return StartCoroutine(_MoveMotion());
        Stage0TutorialManager.Instance.PlayPrograss(Stage0TutorialManager.TutorialPrograss.AfterItemEvent);
        yield break;
    }

    private IEnumerator _MoveMotion()
    {
        transform.DOMove(new Vector3(destinationTransform.position.x, destinationTransform.position.y, 0), 1.0f);
        eyeRenderer.enabled = false;
        tailRenderer.enabled = false;
        for (int i = 0; i < 2; i++)
        {
            bodyRenderer.sprite = walkMotion1;
            yield return new WaitForSeconds(0.25f);
            bodyRenderer.sprite = walkMotion2;
            yield return new WaitForSeconds(0.25f);
        }
        eyeRenderer.enabled = true;
        tailRenderer.enabled = true;
        bodyRenderer.sprite = origin;
        canTouch = true;
    }
}
