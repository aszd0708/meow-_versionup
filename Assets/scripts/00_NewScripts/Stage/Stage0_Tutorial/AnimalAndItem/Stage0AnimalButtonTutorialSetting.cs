﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Stage0AnimalButtonTutorialSetting : MonoBehaviour
{
    public Image buttonAnimalImgComponent;
    private void OnEnable()
    {
        buttonAnimalImgComponent.color = Color.black;
    }
}
