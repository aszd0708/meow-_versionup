﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage0ItemBowl : StageItemBase
{
    protected override void OnEnable()
    {
        if (itemSprite == null) itemSprite = spriteRenderer.sprite;
    }

    protected override void EndButtonEvent()
    {
        bool playProgress = Stage0TutorialManager.Instance.PlayPrograss(Stage0TutorialManager.TutorialPrograss.AfterItem);
        if(playProgress)
        {
            base.EndButtonEvent();
        }
    }

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if(Stage0TutorialManager.Instance.CanPlayPrograss(Stage0TutorialManager.TutorialPrograss.AfterItem))
        {
            base.DoTouch(touchPose);
        }
        return false;
    }
}
