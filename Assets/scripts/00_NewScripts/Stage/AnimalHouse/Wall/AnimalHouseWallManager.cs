﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalHouseWallManager : Singleton<AnimalHouseWallManager>
{
    [SerializeField]
    [Header("벽들")]
    private AnimalHouseWallColor[] walls;

    private Vector3[] wallPoses;

    [SerializeField]
    [Header("벽이 움직이는 위치")]
    private Transform moveWallPose;

    [SerializeField]
    [Header("움직이는 시간")]
    private float moveTime;

    protected void OnEnable()
    {
        wallPoses = new Vector3[walls.Length];

        for(int i = 0; i < walls.Length; i++)
        {
            wallPoses[i] = walls[i].transform.position;
        }
    }

    public float MoveToCenterWalls()
    {
        StartCoroutine(_MoveToCenterWalls());
        return moveTime * walls.Length;
    }

    private IEnumerator _MoveToCenterWalls()
    {
        for (int i = 0; i < walls.Length; i++)
        {
            walls[i].ChangeColor();
            walls[i].transform.DOMove(moveWallPose.position, moveTime);
            yield return new WaitForSeconds(moveTime);
        }
        yield break;
    }

    public void MoveToOutsideWalls()
    {
        for (int i = 0; i < walls.Length; i++)
        {
            walls[i].transform.DOMove(wallPoses[i], moveTime);
        }
    }
}
