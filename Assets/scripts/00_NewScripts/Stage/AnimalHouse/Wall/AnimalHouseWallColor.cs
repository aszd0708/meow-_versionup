﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static ColorData;

public class AnimalHouseWallColor : MonoBehaviour
{
    public ColorData color;

    [SerializeField]
    [Header("자신의 스프라이트")]
    private SpriteRenderer wallSprite;

    public enum WallPos
    {
        Left, Right, Bottom
    }

    public WallPos wallPos;

    public void ChangeColor()
    {
        int colorNum = color.random + (int)wallPos;
        
        if(colorNum >= color.PastelRGB.Length)
            colorNum = 0 + (int)wallPos;

        wallSprite.color = new Color(color.PastelRGB[colorNum].R, color.PastelRGB[colorNum].G, color.PastelRGB[colorNum].B);
    }
}
