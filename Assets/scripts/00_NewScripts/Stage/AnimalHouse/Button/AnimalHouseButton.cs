﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimalHouseButton : MonoBehaviour
{
    [Header("자기 자신한테 있는 컴포넌트")]
    [SerializeField]
    private Image animalImg;
    [SerializeField]
    private Button button;

    [Header("여기서 부터는 다른 클래스에서 받아와야 할것들")]
    public GameObject animal;
    public AnimalEx animalExplanation;
    public GameObject item;
    public string catName;
    public Sprite itemImg;
    public bool hasSpecialAnim;
    public bool isHiddenAnimal;

    public Vector3 pointPos;

    [Space(20)]

    [Header("직접 넣어줘야 할것들")]
    public Button back;
    public Reset reset;

    [Header("이름 설명")]
    public Text nameExplanation;
    [Header("각 설명칸")]
    public Text[] explanation = new Text[3]; // 0 - point 1 - hobby 2 - story
    public RectTransform nameBox;
    public RectTransform explanationBox;

    public RectTransform tipBox;
    public Text tipBoxText;

    public InstarButton instarURL;
    public Image hiddenMark;
    public RectTransform hiddenMarkRect;

    public RectTransform chartRect;
    public AnimalStageRadarChartMotion raderChart;

    public SpriteRenderer background;
    public Canvas mainCanvas;

    private float upSpeed;
    private float downSpeed;

    private GameObject createAnimalObj;

    public void InsertElement(GameObject _animal, Sprite animalSprite, AnimalEx _animalExplanation, GameObject _item, string _catName, Sprite _itemImg,bool isCollect,bool _hasSpecialAnim = true, bool _isHiddenAnimal = false)
    {
        animal = _animal;
        animalImg.sprite = animalSprite;
        animalExplanation = _animalExplanation;
        item = _item;
        catName = _catName;
        itemImg = _itemImg;
        hasSpecialAnim = _hasSpecialAnim;
        isHiddenAnimal = _isHiddenAnimal;

        if(isCollect)
        {
            animalImg.color = Color.white;
            button.enabled = true;
        }

        else
        {
            animalImg.color = Color.black;
            button.enabled = false;
        }
    }

    void Start()
    {
        upSpeed = 1.5f;
        downSpeed = 0.2f;
    }
    private void SetBoxText()
    {
        nameExplanation.text = animalExplanation.Name;
        explanation[2].text = animalExplanation.Charactor;
        explanation[1].text = animalExplanation.Favorite;
        explanation[0].text = animalExplanation.Story;
    }

    private IEnumerator _MoveExplaneBoxes()
    {
        SetBoxText();
        explanationBox.anchoredPosition = Vector2.up * 800;
        nameBox.DOAnchorPosY(0, upSpeed / 2).SetEase(Ease.OutBounce);
        explanationBox.DOAnchorPosY(0, upSpeed / 2).SetEase(Ease.OutBounce);
        yield break;
    }

    private void SetInstarURL()
    {
        if (animalExplanation.Instargram == "none")
        {
            instarURL.GetComponent<Image>().color = Color.white / 2;
            instarURL.GetComponent<Button>().enabled = false;
        }
        else
        {
            instarURL.GetComponent<InstarButton>().instarURL = "https://www.instagram.com/" + animalExplanation.Instargram;
            instarURL.GetComponent<Image>().color = Color.white;
            instarURL.GetComponent<Button>().enabled = true;
        }
    }

    private GameObject CreateAnimal()
    {
        GameObject animalCreate = PoolingManager.Instance.GetPool(animalExplanation.Name);
        if(animalCreate == null)
        {
            animalCreate = Instantiate(animal);
        }
        return animalCreate;
    }

    private void SetItem(bool showItem)
    {
        if (showItem)
        {
            item.GetComponent<SpriteRenderer>().sprite = itemImg;
            item.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        }
        else
        {
            item.GetComponent<SpriteRenderer>().sprite = null;
        }
    }

    private void SetOthers()
    {
        tipBoxText.text = animalExplanation.Tip;
        background.enabled = true;
        nameBox.anchoredPosition = Vector2.up * 400;
    }

    public void StartRoomChange()
    {
        SetBoxText();
        SetOthers();
        StartCoroutine(_StartRoomChange());
    }

    private IEnumerator _StartRoomChange()
    {
        mainCanvas.enabled = false;

        float wallTime = AnimalHouseWallManager.Instance.MoveToCenterWalls();
        yield return new WaitForSeconds(wallTime);

        createAnimalObj = CreateAnimal();
        createAnimalObj.transform.position = new Vector3(pointPos.x, pointPos.y -2.5f, -2);
        createAnimalObj.transform.localScale = new Vector2(0, 0);
        createAnimalObj.transform.DOScale(2, upSpeed / 2).SetEase(Ease.OutElastic);

        bool showItem = true;
        CatHouseCat animalInfo = createAnimalObj.GetComponent<CatHouseCat>();
        if(animalInfo != null)
        {
            showItem = animalInfo.ShowItem;
            SetItem(showItem);
        }
        else
        {
            SetItem(false);
        }

        SetInstarURL();

        instarURL.GetComponent<RectTransform>().DOScale(1, upSpeed).SetEase(Ease.OutElastic);

        StartCoroutine(_MoveExplaneBoxes());

        chartRect.DOScale(1, upSpeed).SetEase(Ease.OutElastic);
        raderChart.OnClick();
        item.transform.DOScale(2, upSpeed / 2).SetEase(Ease.OutElastic);
        tipBox.DOScale(1, upSpeed / 2).SetEase(Ease.OutElastic);

        yield return new WaitForSeconds(upSpeed / 2);

        if(isHiddenAnimal)
        {
            hiddenMarkRect.DOScale(Vector2.one, upSpeed / 2).SetEase(Ease.OutElastic);
        }
        back.onClick.AddListener(ClickClear);
        yield return new WaitForSeconds(upSpeed / 2);

        reset.catButton = this;
        reset.room = true;
        yield break;
    }

    public void ClickClear()
    {
        back.onClick.RemoveAllListeners();
            AnimalHouseSpecialAnimAnimal specialAnimal = createAnimalObj.GetComponent<AnimalHouseSpecialAnimAnimal>();
            if (specialAnimal != null)
            {
                StartCoroutine(WaitChange(specialAnimal.GetGoBackAnimTime()));
                Debug.Log("!null");
            }
            else
            {
                Debug.Log("null");
                StartCoroutine(BackRoomMotion());
            }
    }

    public IEnumerator WaitChange(float time)
    {
        back.enabled = false;
        yield return new WaitForSeconds(time);
        back.enabled = true;
        StartCoroutine(BackRoomMotion());
        yield break;
    }

    public IEnumerator BackRoomMotion()
    {
        createAnimalObj.transform.DOScale(0, downSpeed);

        if (isHiddenAnimal)
        {
            hiddenMarkRect.DOScale(Vector2.zero, upSpeed / 2).SetEase(Ease.Linear);
        }

        item.transform.DOScale(0, downSpeed);
        item.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;

        nameBox.DOAnchorPosY(400, upSpeed / 2).SetEase(Ease.InQuint);
        explanationBox.DOAnchorPosY(800, upSpeed / 2).SetEase(Ease.InQuint);
        yield return new WaitForSeconds(upSpeed / 2);

        AnimalHouseWallManager.Instance.MoveToOutsideWalls();

        instarURL.GetComponent<RectTransform>().DOScale(0, downSpeed);
        chartRect.DOScale(0, downSpeed);
        tipBox.DOScale(0, downSpeed);
        yield return new WaitForSeconds(downSpeed + 0.1f);

        PoolingManager.Instance.SetPool(createAnimalObj, animalExplanation.Name);
        background.enabled = true;

        mainCanvas.enabled = true;
        reset.room = false;
        yield break;
    }
}
