﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class AnimalHouseButtonCreater : MonoBehaviour
{
    [SerializeField]
    [Header("눌렀을때 나오는 동물 오브젝트")]
    private GameObject button;

    [SerializeField]
    [Header("버튼 부모")]
    private RectTransform buttonParent;

    [SerializeField]
    [Header("눌렀을때 나오는 동물 오브젝트")]
    private GameObject[] animalObjs;

    [SerializeField]
    [Header("동물 스프라이트")]
    private Sprite[] animalSprites;

    [SerializeField]
    [Header("아이템")]
    private Sprite[] itemSprites;

    [SerializeField]
    [Header("아이템 오브젝트(공중에 떠다니는)")]
    private GameObject item;

    [SerializeField]
    [Header("특이한 애니메이션 갖고있는 고양이(Ex에 있는 No로 결정)")]
    private int[] specialAnimIndex;

    [SerializeField]
    private AnimalExJsonData animalExJsonDataManger;
    [SerializeField]
    private AnimalChartJsonData animalChartJsonDataManager;
    [SerializeField]
    private AnimalJsonData animalJsonData;

    [SerializeField]
    private ItemDataManager itemDataManager;

    [Header("히든 동물 인덱스들(Ex에 있는 No로 결정)")]
    [SerializeField]
    private int[] hiddenAnimalIndex;

    [SerializeField]
    private ScrollRectSnap SRS;

    [SerializeField]
    [Header("간격")]
    private float buttonDistance;

    List<AnimalData> animalDatas;
    List<AnimalEx> animalEx;
    List<AnimalChart> animalChart;

    Dictionary<string, Sprite> itemSpriteDiction = new Dictionary<string, Sprite>();
    Dictionary<string, Sprite> animalSpriteDiction = new Dictionary<string, Sprite>();

    private void OnEnable()
    {
        SetItemSpriteDiction();

    }

    private void Start()
    {
        CreateButton();
    }

    private void CreateButton()
    {
        animalDatas = SaveDataManager.Instance.Data.Animals;
        animalEx = animalExJsonDataManger.JsonData;
        animalChart = animalChartJsonDataManager.JsonData;

        if (animalObjs.Length != animalDatas.Count)
        {
            return;
        }

        SortAnimalData(ref animalDatas, ref animalEx, ref animalChart,ref animalObjs, 0, animalDatas.Count-1);

        for(int i = 0; i < animalDatas.Count; i++)
        {
            GameObject createButton = Instantiate(button);
            createButton.SetActive(true);
            AnimalHouseButton createButtonInfo = createButton.GetComponent<AnimalHouseButton>();

            if (!itemSpriteDiction.TryGetValue(animalDatas[i].AnimalName, out Sprite itemSprite))
            {
                itemSprite = null;
            }

            if (!animalSpriteDiction.TryGetValue(animalDatas[i].AnimalName, out Sprite animalSprite))
            {
                animalSprite = null;
            }

            createButtonInfo.InsertElement(animalObjs[i], animalSprite, animalEx[i], item, animalEx[i].Name, itemSprite, animalDatas[i].IsCollect, GetSpecialAnim(animalEx[i].No), IsHiddenAnimal(animalEx[i].No));
            createButtonInfo.raderChart.chartInfo = animalChart[i];
            createButton.transform.SetParent(buttonParent);
            createButton.GetComponent<RectTransform>().anchoredPosition = new Vector3(i * buttonDistance, 0, 0);
            createButton.GetComponent<CenterSize>().isCollect = animalDatas[i].IsCollect;
            SRS.GetCatScroll(createButton.GetComponent<Button>(), i);
        }
        SRS.SetSnap();
    }

    private bool IsHiddenAnimal(int index)
    {
        bool isHidden = false;
        for(int i = 0; i < hiddenAnimalIndex.Length; i++)
        {
            if(hiddenAnimalIndex[i] == index)
            {
                isHidden = true;
            }
        }

        return isHidden;
    }

    private void SortAnimalData(ref List<AnimalData> animalData, ref List<AnimalEx> animalEx, ref List<AnimalChart> animalCharts, ref GameObject[] animalArray, int startIndex, int endIndex)
    {
        Divide(ref animalData, ref animalEx, ref animalCharts, ref animalArray, startIndex, endIndex);
    }
    private void Divide(ref List<AnimalData> animalData, ref List<AnimalEx> animalEx, ref List<AnimalChart> animalCharts, ref GameObject[] animalArray, int startIndex, int endIndex)
    {
        int middleIndex = (startIndex + endIndex) / 2;

        if (startIndex == endIndex)
        {
            return;
        }

        else
        {
            Divide(ref animalData, ref animalEx, ref animalCharts, ref animalArray, startIndex, middleIndex);
            Divide(ref animalData, ref animalEx, ref animalCharts, ref animalArray, middleIndex +1, endIndex);
            Merge(ref animalData, ref animalEx, ref animalCharts, ref animalArray, startIndex, endIndex);
        }
    }

    private void Merge(ref List<AnimalData> animalData, ref List<AnimalEx> animalEx, ref List<AnimalChart> animalCharts, ref GameObject[] animalArray, int startIndex, int endIndex)
    {
        int start = startIndex;
        int middleIndex = (startIndex + endIndex) / 2 + 1;

        List<AnimalData> dataTemp = new List<AnimalData>();
        List<AnimalEx> exTemp = new List<AnimalEx>();
        List<AnimalChart> chartTemp = new List<AnimalChart>();
        GameObject[] animalTemp = new GameObject[endIndex - startIndex + 1];

        int arrayPointer = 0;
        while (start <= (startIndex + endIndex) / 2 && middleIndex <= endIndex)
        {
            if (!animalData[start].IsCollect && animalData[middleIndex].IsCollect)
            {
                dataTemp.Add(animalData[middleIndex]);
                exTemp.Add(animalEx[middleIndex]);
                chartTemp.Add(animalCharts[middleIndex]);
                animalTemp[arrayPointer] = animalArray[middleIndex];
                middleIndex++;
                arrayPointer++;
            }

            else
            {
                dataTemp.Add(animalData[start]);
                exTemp.Add(animalEx[start]);
                chartTemp.Add(animalCharts[start]);
                animalTemp[arrayPointer] = animalArray[start];
                start++;
                arrayPointer++;
            }
        }

        if (middleIndex >= endIndex)
        {
            for (int i = start; i <= (startIndex + endIndex) / 2; i++)
            {
                dataTemp.Add(animalData[i]);
                exTemp.Add(animalEx[i]);
                chartTemp.Add(animalCharts[i]);
                animalTemp[arrayPointer++] = animalArray[i];
            }
        }

        if (start > (startIndex + endIndex) / 2)
        {
            for (int i = middleIndex; i < endIndex; i++)
            {
                dataTemp.Add(animalData[i]);
                exTemp.Add(animalEx[i]);
                chartTemp.Add(animalCharts[i]);
                animalTemp[arrayPointer++] = animalArray[i];
            }
        }

        for (int i = startIndex, j = 0; j < dataTemp.Count; i++, j++)
        {
            animalData[i] = dataTemp[j];
            animalEx[i] = exTemp[j];
            animalCharts[i] = chartTemp[j];
            animalArray[i] = animalTemp[j];
        }
    }

    private bool GetSpecialAnim(int index)
    {
        bool isSpecial = false;
        for(int i = 0; i < specialAnimIndex.Length; i++)
        {
            if(specialAnimIndex[i] == index)
            {
                isSpecial = true;
                break;
            }
        }
        return isSpecial;
    }

    private void SetItemSpriteDiction()
    {
        string none = "none";
        for (int i = 0, itemIndex = 0; i < animalJsonData.JsonData.Count; i++)
        {
            if (!string.Equals(animalJsonData.JsonData[i].ItemName, none))
            {
                itemSpriteDiction.Add(animalJsonData.JsonData[i].CatPath, itemSprites[itemIndex]);
                itemIndex++;
            }

            Debug.Log("동물 이름 : " + animalJsonData.JsonData[i].CatPath);
            animalSpriteDiction.Add(animalJsonData.JsonData[i].CatPath, animalSprites[i]);
        }
    }
}
