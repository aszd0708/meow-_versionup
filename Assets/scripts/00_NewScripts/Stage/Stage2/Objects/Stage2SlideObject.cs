﻿using BitBenderGames;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2SlideObject : MonoBehaviour, TouchDownableObj
{
    public enum position
    { left, right}

    public position pos;
    public bool touch;
    public TouchInputController camTouch;
    public Transform other;
    private Collider2D otherCollider;

    private Vector2 originPos;
    private Vector2 nowPos;
    private float posZ;
    private Vector3 downPos;
    private Vector3 nPos;

    private BoxCollider2D objCollider;
    private Vector3 firstTouchPose;

    private bool bPlaySound = false;
    private void Awake()
    {
        objCollider = GetComponent<BoxCollider2D>();
        otherCollider = other.GetComponent<Collider2D>();
    }

    void Start()
    {
        transform.parent.transform.GetChild(2).GetComponent<SpriteRenderer>().sortingOrder = 4;
        GetComponent<SpriteRenderer>().sortingOrder = 4;
        if (pos == position.left)
            posZ = -2;
        else if (pos == position.right)
            posZ = -1;
    }
    
    // Update is called once per frame
    void Update()
    {
        DoSlide();
    }

    private void DoSlide()
    {
        if (touch)
        {
            if (Input.GetMouseButton(0))
            {
                if (other.localPosition.x < -0.3f)
                {
                    objCollider.enabled = false;
                }
                else if (other.localPosition.x >= -0.3f)
                {
                    otherCollider.enabled = true;
                    //nPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    nPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    if (pos == position.left)
                    {
                        transform.localPosition = new Vector3((transform.localPosition.x) +
                            (downPos.x - nPos.x), transform.localPosition.y, posZ);
                    }
                    else if (pos == position.right)
                    {
                        transform.localPosition = new Vector3((transform.localPosition.x) -
                            (downPos.x - nPos.x), transform.localPosition.y, posZ);
                    }
                    float distance = Mathf.Abs(downPos.x - nPos.x);
                    downPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                    Debug.Log("Distance : " + distance);
                    if(distance >= 0.3f && bPlaySound == false)
                    {
                        AudioManager.Instance.PlaySound("window", transform.position);
                        bPlaySound = true;
                    }
                }
            }

            else if (Input.GetMouseButtonUp(0))
            {
                touch = false;
                StageTouchManagerBase.Instance.ForbitTouch(true);
                bPlaySound = false;
            }
            transform.localPosition = new Vector3(Mathf.Clamp(transform.localPosition.x, originPos.x - 3.5f, originPos.x), Mathf.Clamp(nowPos.y, originPos.y, originPos.y), posZ);
        }
    }

    public bool DoTouchDown(Vector2 touchPose = default)
    {
        StageTouchManagerBase.Instance.ForbitTouch(false);
        firstTouchPose = touchPose;
        touch = true;
        nPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        return false;
    }
}
