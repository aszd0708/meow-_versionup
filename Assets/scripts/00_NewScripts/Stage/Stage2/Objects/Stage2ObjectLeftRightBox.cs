﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2ObjectLeftRightBox : MonoBehaviour, TouchableObj
{
    [SerializeField]
    [Header("닫혀있는 따꿍")]
    private SpriteRenderer boxUplockCap;

    [SerializeField]
    [Header("양옆 날개")]
    private SpriteRenderer[] lids;

    [SerializeField]
    [Header("열렸을 때 나오는 오브젝트")]
    [Tooltip("두개 이상일 경우 랜덤으로 나옴")]
    private GameObject[] eventItem;

    [SerializeField]
    [Header("씬에 있는 오브젝트 일경우 켜주기만 함")]
    private bool inScene;

    [SerializeField]
    [Header("이벤트 아이템이 나오면 이 오브젝트를 꺼줄까?")]
    private bool isDestroy;

    private bool isOpen;

    private void Start()
    {
        if(inScene)
        {
            for(int i = 0; i < eventItem.Length; i++)
            {
                eventItem[i].SetActive(false);
            }
        }
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        OpenBox();
        return false;
    }

    private void OpenBox()
    {
        if (!isOpen)
        {
            boxUplockCap.enabled = false;
            for (int i = 0; i < lids.Length; i++)
            {
                lids[i].enabled = true;
            }
            isOpen = true;
            CreateItem();
        }
    }

    /// <summary>
    /// 아이템 생성
    /// 만약 씬 안에 존재하면 켜주기만 함
    /// 아니면 새로 생성한 뒤 10초뒤에 풀링
    /// </summary>
    private void CreateItem()
    {
        if (eventItem.Length == 0)
            return;

        int randomIndex = Random.Range(0, eventItem.Length);

        if(inScene)
        {
            eventItem[randomIndex].SetActive(true);
            eventItem[randomIndex].transform.position = transform.position;
        }

        else
        {
            GameObject createItem = Instantiate(eventItem[randomIndex]);
            createItem.transform.position = transform.position;
            PoolingManager.Instance.SetPool(createItem, "DInABox");
        }

        if(isDestroy)
        {
            gameObject.SetActive(false);
        }
    }
}
