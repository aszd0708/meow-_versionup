﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2ObjectReed : MonoBehaviour
{
    private float firstPos;

    private bool bMove = true;

    private bool bChange = false;

    private Coroutine idleMotionCor, touchMotionCor;

    private float moveSpeed;

    public enum State
    {
        LEFT,
        RIGHT
    }
    private State currentState;

    private void Start()
    {
        this.enabled = false;
        transform.localRotation = Quaternion.Euler(Vector3.forward * Random.Range(20f, 0f));
        firstPos = transform.localRotation.z;
        bMove = true;
    }

    private void Update()
    {
        if(bMove == false)        { return; }

        Vector3 localRotation = transform.localRotation.eulerAngles;
        if (transform.localRotation.z > firstPos - 0.06f)
        {
            currentState = State.LEFT;
            bChange = false;
            Debug.Log("왼쪽으로 바뀜");
        }
        else if (transform.localRotation.z > firstPos + 0.06f)
        {
            currentState = State.RIGHT;
            bChange = false;
            Debug.Log("오른쪽으로 바뀜");
        }

        if(bChange == false)
        {
            switch(currentState)
            {
                case State.LEFT:
                    moveSpeed = Random.Range(1.0f, 1.5f);
                    moveSpeed *= -10f;
                    break;
                case State.RIGHT:
                    moveSpeed = Random.Range(1.0f, 1.5f);
                    moveSpeed *= 10f;
                    break;
            }
            bChange = true;
        }

        //transform.DOLocalRotate(Vector3.forward * movePose, randomTime);

        transform.localRotation = Quaternion.Euler(localRotation.x, localRotation.y, localRotation.z + moveSpeed * Time.deltaTime);
    }

    public void StartTouchMotion()
    {
        transform.DOPause();
        bMove = false;
        if (touchMotionCor != null)
        {
            StopCoroutine(touchMotionCor);
            touchMotionCor = null;
        }
        touchMotionCor = StartCoroutine(_PlayTouchMotion());
    }

    /// <summary>
    /// 터치 했을때 나오는 모션
    /// </summary>
    /// <returns></returns>
    private IEnumerator _PlayTouchMotion()
    {
        float randomRange = Random.Range(-15.0f, 0.0f);

        if (transform.rotation.z < -20)
            transform.DOLocalRotate(new Vector3(0, 0, 0), 0.5f);
        else
        {
            float movePose = transform.rotation.z + randomRange;
            transform.DOLocalRotate(Vector3.forward * movePose, 0.3f);
        }
        yield return new WaitForSeconds(0.5f);
        bMove = true;
        yield break;
    }
}
