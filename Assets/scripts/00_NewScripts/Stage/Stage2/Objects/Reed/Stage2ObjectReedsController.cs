﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2ObjectReedsController : MonoBehaviour, TouchableObj
{
    [SerializeField]
    [Header("움직일 갈대들")]
    private Stage2ObjectReed[] reed;
    private bool touch;

    public bool DoTouch(Vector2 touchPose = default)
    {
        AudioManager.Instance.PlaySound("slide_4", transform.position);
        //StartCoroutine(Motion());
        return false;
    }

    private IEnumerator Motion()
    {
        touch = true;
        int count = 0;
        int randomNum = Random.Range(0, reed.Length);
        for (int i = 0; i < reed.Length; i++)
        {

            float randomRange = Random.Range(-15.0f, 0.0f);
            int a = Random.Range(0, 2);
            if (a == 0)
                continue;
            reed[i].StartTouchMotion();

            if (count == randomNum)
                break;
        }
        yield return new WaitForSeconds(0.5f);
        touch = false;
        yield break;
    }
}
