﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2ObjectManhole : MonoBehaviour, TouchableObj
{
    [SerializeField]
    [Header("맨홀 따꿍 Transform")]
    private Transform manholeCapTransform;

    [SerializeField]
    [Header("만들 쓰렉이")] 
    private GameObject trashObj;
    [SerializeField]
    private Sprite[] trashSprites;

    [SerializeField]
    [Header("특별한 아이템 한번만 나옴(없으면 아무일 ㄴㄴ)")]
    private GameObject specialObj;
    [SerializeField]
    [Header("특별한 아이템이 나올 터치 횟수")]
    private int specialObjCreateTouchCount;

    [SerializeField]
    [Header("닫히는 터치 카운트")]
    private int closedCount = 5;

    private float randomPose;

    /// <summary>
    /// 지금 터치 가능한지 확인하는 부울값
    /// </summary>
    private bool canTouch = true;

    /// <summary>
    /// 열려있는지 확인하는 부울값
    /// </summary>
    private bool isOpen;

    /// <summary>
    /// 현재 터치 한 횟수
    /// </summary>
    private int currentTouchCount;

    private WaitForSeconds waitHalfSecond;

    private List<GameObject> craeteTrashes = new List<GameObject>();

    private void Start()
    {
        waitHalfSecond = new WaitForSeconds(0.5f);
        if(specialObj)
        {
            specialObj.SetActive(false);
        }
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        if(canTouch)
        {
            StartEvent();
        }
        return false;
    }

    private void SetRandomPose()
    {
        int random = Random.Range(0, 2);
        switch (random)
        {
            case 0: randomPose = -1.2f; break;
            case 1: randomPose = 1.2f; break;
        }
    }

    private void StartEvent()
    {
        canTouch = false;
        currentTouchCount++;
        #region 열려있을때
        if (isOpen)
        {
            StartCoroutine(_StartOpenEvent());
        }
        #endregion

        #region 닫혀있을 때
        else
        {
            SetRandomPose();
            StartCoroutine(_StartOpeningEvent());
        }
        #endregion
    }

    private IEnumerator _StartOpenEvent()
    {
        if(isOpen && currentTouchCount == specialObjCreateTouchCount && specialObj)
        {
            specialObj.transform.position = transform.position;
            specialObj.SetActive(true);
        }

        else if(currentTouchCount % closedCount <= 0)
        {
            // 닫히는곳 
            manholeCapTransform.DOLocalMove(Vector3.zero, 0.5f);
            isOpen = false;
            ClearTrashes();
            AudioManager.Instance.PlaySound("slide_4", transform.position);
            yield return waitHalfSecond;
        }

        else
        {
            GameObject trash = CreateTrash();
            trash.transform.DOLocalJump(Vector3.left * randomPose, 0.5f, 2, 0.5f);
        }

        canTouch = true;
        yield break;
    }

    /// <summary>
    /// 쓰레기 만들고 스프라이트 붙이고 리스트에 넣고 리턴
    /// </summary>
    /// <returns></returns>
    private GameObject CreateTrash()
    {
        GameObject createTrash = PoolingManager.Instance.GetPool("ManholeTrash");
        if (!createTrash)
        {
            createTrash = Instantiate(trashObj);
            createTrash.SetActive(true);
        }
        createTrash.transform.SetParent(transform);
        createTrash.transform.localPosition = Vector3.zero;
        SpriteRenderer trashSpriteRenderer = createTrash.GetComponent<SpriteRenderer>();
        trashSpriteRenderer.sprite = trashSprites[Random.Range(0, trashSprites.Length)];
        trashSpriteRenderer.sortingOrder = currentTouchCount % closedCount;
        craeteTrashes.Add(createTrash);
        return createTrash;
    }

    /// <summary>
    /// 맨홀 따꿍 여는 이벤트
    /// </summary>
    /// <returns></returns>
    private IEnumerator _StartOpeningEvent()
    {
        AudioManager.Instance.PlaySound("slide_4", transform.position);
        manholeCapTransform.DOLocalMove(new Vector3(randomPose, 0.6f, 0), 0.5f);
        isOpen = true;
        yield return waitHalfSecond;
        canTouch = true;
        yield break;
    }

    private void ClearTrashes()
    {
        for(int i = 0; i < craeteTrashes.Count; i++)
        {
            PoolingManager.Instance.SetPool(craeteTrashes[i], "ManholeTrash");
        }

        craeteTrashes.Clear();
    }
}
