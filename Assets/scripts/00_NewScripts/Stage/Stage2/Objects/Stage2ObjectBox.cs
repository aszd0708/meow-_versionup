﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Stage2ObjectBox : MonoBehaviour, TouchableObj
{
    public SpriteRenderer open;
    public SpriteRenderer[] close;
    private bool openBool;

    [SerializeField]
    private UnityEvent openEvent;

    private bool bPlayEvent = false;

    [SerializeField]
    private UnityEvent touchEvent;

    private void Start()
    {
        openBool = false;
    }

    public void Open()
    {
        if(!openBool)
        {
            open.enabled = false;
            close[0].enabled = true;
            close[1].enabled = true;
            openBool = true;
        }

        else if(openBool)
        {
            open.enabled = true;
            close[0].enabled = false;
            close[1].enabled = false;
            openBool = false;
        }
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        Open();
        if(bPlayEvent == false)
        {
            openEvent.Invoke();
            bPlayEvent = true;
        }
        touchEvent.Invoke();
        return false;
    }
}
