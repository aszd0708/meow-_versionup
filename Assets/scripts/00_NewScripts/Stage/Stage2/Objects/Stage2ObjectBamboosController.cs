﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2ObjectBamboosController : MonoBehaviour, TouchableObj
{
    [SerializeField]
    [Header("대나무들")]
    private Stage2ObjectBamboo[] bamboos;

    [SerializeField]
    [Header("콜라이더(자신)")]
    private Collider2D col;
    private int count;

    private void Awake()
    {
        if(!col)
        {
            col = GetComponent<Collider2D>();
        }    
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        for (int i = 0; i < bamboos.Length; i++)
            bamboos[i].BigCut();
        count++;
        if (count >= 2)
            col.enabled = false;

        return false;
    }
}
