﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2ObjectAutoDoor : MonoBehaviour, TouchableObj
{
    [Header("움직이는 거리")]
    public float movingDis;
    [Header("움직이는 시간")]
    public float movingTime;

    [Header("왼쪽 문")]
    public Transform leftDoor;
    [Header("오른쪽쪽 문")]
    public Transform rightDoor;

    [SerializeField]
    [Header("콜라이더(자신)")]
    private BoxCollider2D allDoorCollider;

    private bool open = false;
    private bool touch = true;

    private WaitForSeconds moveWait;

    [SerializeField]
    [Header("콜라이더 사이즈")]
    private float openColliderSize = 5;
    [SerializeField]
    private float closedColliderSize = 10;

    private void Start()
    {
        moveWait = new WaitForSeconds(movingTime);
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        if(touch)
        {
            StartCoroutine(_MoveDoor(open));
        }

        return false;
    }

    private IEnumerator _MoveDoor(bool isOpen)
    {
        touch = false;
        float moveDistance = movingDis;
        if(isOpen)
        {
            moveDistance *= -1;
        }

        leftDoor.DOMoveX(leftDoor.transform.position.x - moveDistance, movingTime).SetEase(Ease.OutSine);
        rightDoor.DOMoveX(rightDoor.transform.position.x + moveDistance, movingTime).SetEase(Ease.OutSine);
        yield return moveWait;
        float collierSize = closedColliderSize;
        if (isOpen)
            collierSize = openColliderSize;
        allDoorCollider.size = new Vector2(collierSize, allDoorCollider.size.y);
        open = !isOpen;
        touch = true;
        yield break;
    }
}
