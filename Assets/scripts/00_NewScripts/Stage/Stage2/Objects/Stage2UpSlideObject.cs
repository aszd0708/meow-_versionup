﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2UpSlideObject : MonoBehaviour, TouchDownableObj
{
    [Header("최대 Y축")]
    public float maximumY;
    [Header("z Pose")]
    public float posZ;

    private bool touch;
    private Vector2 originPos;
    private Vector3 downPos;
    private Vector3 nPos;

    public bool DoTouchDown(Vector2 touchPose = default)
    {
        downPos = touchPose;
        touch = true;
        StageTouchManagerBase.Instance.ForbitTouch(false);
        return false;
    }

    // Update is called once per frame
    void Update()
    {
        DoUpSlide();
    }

    private void DoUpSlide()
    {
        if (Input.GetMouseButtonDown(0))
        {
            downPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }

        if (touch)
        {
            if (Input.GetMouseButton(0))
            {
                nPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                transform.localPosition = new Vector3(transform.localPosition.x, (transform.localPosition.y) - (downPos.y - nPos.y), posZ);

                downPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            }

            else if (Input.GetMouseButtonUp(0))
            {
                StageTouchManagerBase.Instance.ForbitTouch(true);
                touch = false;
            }
            transform.localPosition = new Vector3(Mathf.Clamp(transform.localPosition.x, transform.localPosition.x, transform.localPosition.x),
                Mathf.Clamp(transform.localPosition.y, originPos.y - 0.2f, originPos.y + maximumY), posZ);
        }
    }
}
