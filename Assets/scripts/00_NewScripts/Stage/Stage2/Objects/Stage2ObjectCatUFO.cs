﻿using BitBenderGames;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2ObjectCatUFO : MonoBehaviour,TouchableObj
{
    [SerializeField]
    [Header("타치 매니져")]
    private StageTouchManagerBase touchManager;
    [SerializeField]
    [Header("줌 하기 위해 사용")]
    private MobileTouchCamera mobileTouchCamera;
    [SerializeField]
    [Header("MainCamera")]
    private Transform mainCamera;
    [Header("빔 마스크 Transform")]
    public Transform beemMaskTransform;
    [Header("우주선 양쪽에 있는 고양이들")]
    public Transform[] catilien = new Transform[2];
    [Header("나올 고양이")]
    public Transform boxCat;
    [Header("CatUFO의 몸체")]
    public Transform catFOBody;
    [Header("목적지 Transform")]
    public Transform destination;
    [Header("빔 랜더러")]
    public SpriteRenderer beemRenderer;
    public Vector3[] movePos = new Vector3[2];

    [Header("히트박스 Transform")]
    public Transform hitBoxTransform;

    [Header("움직이는 시간")]
    public float moveTime;
    [Header("아이템 위치")]
    public Transform itemTransform;
    
    [SerializeField]
    [Header("고먐미")]
    private Stage2AnimalGomaymmee cat;

    [Header("지금 이 애니메션이 끝났나?")]
    private bool end = false;

    private bool canTouch = false;

    private Vector3 originMaskPose;
    private Vector3 originPose;

    private Coroutine miniCatCor;
    // 고양이 Y + 8

    private void Start()
    {
        end = false;
        originPose = transform.position;
        originMaskPose = beemMaskTransform.localPosition;
        beemRenderer.enabled = false;
    }

    public void CallCatUFO()
    {
        StartCoroutine(_ComeUFO());
    }

    /// <summary>
    /// UFO 부름
    /// </summary>
    public void ReturnCatUFO()
    {
        StartCoroutine(_ComeUFO());
    }

    /// <summary>
    /// 백스 고양이 밖으로 내보내는 코루틴
    /// </summary>
    /// <returns></returns>
    private IEnumerator _OutDoorBoxCat()
    {
        Vector3 pos = hitBoxTransform.position;
        miniCatCor = StartCoroutine(_miniCatMotion());
        yield return new WaitForSeconds(0.75f);
        beemRenderer.enabled = true;
        beemMaskTransform.GetComponent<SpriteMask>().enabled = true;
        boxCat.position = destination.position;
        boxCat.localScale = Vector3.zero;
        beemMaskTransform.DOLocalMoveY(-1.45f, 1.5f);
        yield return new WaitForSeconds(1.5f);
        boxCat.DOLocalRotate(Vector3.forward * 1800f, 3.0f, RotateMode.FastBeyond360).SetEase(Ease.Linear);
        boxCat.DOScale(new Vector3(1.5f, 1.5f, 0), 3.0f).SetEase(Ease.Linear);
        boxCat.DOMove(new Vector3(pos.x, pos.y + 1, 0), 3.0f).SetEase(Ease.Linear);
        yield return new WaitForSeconds(3.0f);
        cat.StartMotion();
        touchManager.ForbitTouch(true);
        canTouch = true;
        yield break;
    }

    /// <summary>
    /// 다시 돌아가는 코루틴
    /// </summary>
    /// <returns></returns>
    private IEnumerator _GoToHome()
    {
        float min = mobileTouchCamera.CamZoomMin;
        touchManager.ForbitTouch(false);
        mainCamera.transform.DOMove(new Vector3(originPose.x, originPose.y + 3, mainCamera.transform.position.z), 0.5f);
        AudioManager.Instance.PlaySound("ufo_1", transform.position);
        StartCoroutine(ZoomOut(mobileTouchCamera.CamZoomMax, mobileTouchCamera.CamZoomMin, 0.5f));
        yield return new WaitForSeconds(0.5f);
        mobileTouchCamera.CamZoomMin = min;
        end = false;
        if(miniCatCor != null)
        {
            StopCoroutine(miniCatCor);
        }
        catilien[0].DOLocalMove(Vector3.zero, 0.75f);
        catilien[1].DOLocalMove(Vector3.zero, 0.75f);
        yield return new WaitForSeconds(0.75f);
        itemTransform.DOMove(destination.position, 0.75f);
        itemTransform.DORotate(Vector3.forward * 1800f, 0.75f, RotateMode.FastBeyond360);
        itemTransform.DOScale(Vector3.one, 0.75f);
        yield return new WaitForSeconds(0.75f);
        itemTransform.gameObject.SetActive(false);
        beemMaskTransform.DOLocalMoveY(1.7f, 1.5f);
        yield return new WaitForSeconds(1.5f);
        beemRenderer.enabled = false;
        beemMaskTransform.GetComponent<SpriteMask>().enabled = false;
        transform.DOScale(new Vector3(10, 10, 0), moveTime / 1.5f);
        transform.DOMove(movePos[1], moveTime / 3).SetEase(Ease.OutCubic);
        yield return new WaitForSeconds(moveTime / 3);
        transform.DOMove(movePos[0], moveTime / 3).SetEase(Ease.InCubic);
        yield return new WaitForSeconds(moveTime / 3);

        touchManager.ForbitTouch(true);

        gameObject.SetActive(false);
        yield break;
    }

    /// <summary>
    /// 작은 고양이들 모션
    /// </summary>
    /// <returns></returns>
    private IEnumerator _miniCatMotion()
    {
        catilien[0].DOLocalMoveX(-1.2f, 0.75f);
        catilien[0].DOLocalMoveY(0.1f, 0.75f).SetEase(Ease.InOutBounce);
        catilien[1].DOLocalMoveX(1.2f, 0.75f);
        catilien[1].DOLocalMoveY(-0.1f, 0.75f).SetEase(Ease.InOutBounce);
        yield return new WaitForSeconds(0.75f);
        // 둥실둥실
        WaitForSeconds wait = new WaitForSeconds(1.5f);
        while (true)
        {
            float num = 0.15f;
            for (int i = 0; i < 2; i++)
            {
                catilien[i].DOLocalMoveY(catilien[i].localPosition.y - num, 1.5f);
                num *= -1;
            }
            yield return wait;
            for (int i = 0; i < 2; i++)
            {
                catilien[i].DOLocalMoveY(catilien[i].localPosition.y + num, 1.5f);
                num *= -1;
            }
            yield return wait;
        }
    }

    /// <summary>
    /// 줌인 줌아웃
    /// </summary>
    /// <param name="max"></param>
    /// <param name="min"></param>
    /// <param name="animeTime"></param>
    /// <returns></returns>
    private IEnumerator ZoomOut(float max, float min, float animeTime)
    {
        float zoomMax = max, zoomMin = min;
        float time = 0.0f;

        while (zoomMax > zoomMin)
        {
            time += Time.deltaTime / animeTime;

            zoomMin = Mathf.Lerp(min, max, time);

            mobileTouchCamera.CamZoomMin = zoomMin;
            mobileTouchCamera.ResetCameraBoundaries();
            yield return null;
        }
        yield break;
    }

    /// <summary>
    /// 맵 안으로 오는 코루틴
    /// </summary>
    /// <returns></returns>
    private IEnumerator _ComeUFO()
    {
        float min = mobileTouchCamera.CamZoomMin;
        originPose = hitBoxTransform.position;
        touchManager.ForbitTouch(false);
        mainCamera.transform.DOMove(new Vector3(originPose.x, originPose.y + 3, mainCamera.transform.position.z), 0.5f);
        yield return StartCoroutine(ZoomOut(mobileTouchCamera.CamZoomMax, mobileTouchCamera.CamZoomMin, 0.5f));        
        mobileTouchCamera.CamZoomMin = min;
        transform.localScale = new Vector3(10, 10, 0);
        transform.position = movePos[0];
        transform.DOScale(new Vector3(3, 3, 0), moveTime / 1.5f);
        transform.DOMove(movePos[1], moveTime / 3).SetEase(Ease.OutCubic);
        AudioManager.Instance.PlaySound("ufo_1", transform.position);
        yield return new WaitForSeconds(moveTime / 3);
        transform.DOMove(new Vector3(originPose.x, originPose.y + 10, 0), moveTime / 2).SetEase(Ease.InCubic);
        yield return new WaitForSeconds(moveTime / 3);
        StartCoroutine(_OutDoorBoxCat());
        yield break;
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        if(!canTouch)
        {
            return false;
        }
        canTouch = false;
        StartCoroutine(_GoToHome());
        return false;
    }
}
