﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Stage2ObjectVentilator : MonoBehaviour, TouchableObj
{
    [Space(20)]
    [SerializeField]
    [Header("스프라이트 랜더러(자신)")]
    private SpriteRenderer spriteRenderer;

    #region Step1
    [SerializeField]
    [Header("처음 터치했을때 움직일 각도")]
    [Header("Step1")]
    private float rotateAngleZ = 20.0f;

    [Space(20)]
    [SerializeField]
    [Header("움직이는 시간")]
    private float rotateTime = 0.3f;
    #endregion

    #region Step2
    [Header("흔드는 변수들")]
    [SerializeField]
    private float shakePowX = 0.1f;
    [SerializeField]
    private float shakePowZ = 10.0f;
    [SerializeField]
    private float shakeTime = 0.5f;

    [SerializeField]
    [Header("떨어지는 변수들")]
    private float fallingPoseY = -17;
    [SerializeField]
    private float fallingTime = 1.0f;
    #endregion

    [SerializeField]
    [Header("안에 있는 오브젝트(안넣으면 아무일 X)")]
    private Stage2ObjectInVentilator objInVentilator;

    [Header("현재 터치 가능한지")]
    private bool touch = true;

    [Header("지금 현재 상황")]
    private int nowStep = 0;

    [Header("실외기 떨어지고 난 뒤에 하는 이벤트")]
    public UnityEvent endEvent;

    private void Awake()
    {
        if(!spriteRenderer)
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
        }
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        if(touch)
        {
            StartCoroutine(_StartMotion());
        }
        return false;
    }

    private IEnumerator _StartMotion()
    {
        touch = false;
        switch (nowStep)
        {
            case 0:
                int a = Random.Range(0, 2);
                switch (a)
                {
                    case 0:
                        transform.DORotate(Vector3.forward * rotateAngleZ, rotateTime);
                        break;
                    case 1:
                        transform.DORotate(Vector3.forward * -rotateAngleZ, rotateTime);
                        break;
                }
                yield return new WaitForSeconds(rotateTime);
                break;
            case 1:
                iTween.ShakePosition(gameObject, iTween.Hash(
                        "x", shakePowX,
                        "time", shakeTime));

                iTween.ShakeRotation(gameObject, iTween.Hash(
                    "z", shakePowZ,
                    "time", shakeTime));
                yield return new WaitForSeconds(shakeTime);
                StartEndEvent();
                spriteRenderer.sortingOrder = 5;
                transform.DOLocalMoveY(fallingPoseY, fallingTime);
                yield return new WaitForSeconds(fallingTime);
                PoolingManager.Instance.SetPool(gameObject, "Ventilator");
                break;
            default:
                break;
        }
        nowStep++;
        touch = true;

        yield break;
    }

    /// <summary>
    /// 실외기 떨어지고 난 뒤에 하는 이벤트
    /// </summary>
    private void StartEndEvent()
    {
        endEvent.Invoke();
        if (objInVentilator)
        {
            objInVentilator.gameObject.SetActive(true);
            objInVentilator.transform.position = transform.position;
            objInVentilator.StartEvent();
        }
    }
}
