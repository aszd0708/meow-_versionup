﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2ObjectInVentilator : MonoBehaviour
{
    /// <summary>
    /// 환풍기 떨어지면 나오는 이벤트
    /// </summary>
    public virtual void StartEvent()
    {

    }

    public void StartDefaultEndEvent(Transform setPose)
    {
        transform.position = setPose.position;
    }
}
