﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2ObjectBugInVetilator : Stage2ObjectInVentilator
{
    [SerializeField]
    [Header("벌레 속도 곱할 값")]
    private float divSize;

    [SerializeField]
    [Header("벌레 움직이는 컴포넌트들")]
    private LaserMove[] bugMoves;

    [SerializeField]
    [Header("총 모션 타임")]
    private float motionTime;

    void Start()
    {
        //for(int i = 0; i < bugMoves.Length; i++)
        //{
        //    bugMoves[i].enabled = false;
        //}
    }

    public override void StartEvent()
    {
        for (int i = 0; i < bugMoves.Length; i++)
        {
            bugMoves[i].enabled = true;
            bugMoves[i].MulSize = 0.5f;
        }
        StartCoroutine(_StartMotion());
    }

    private IEnumerator _StartMotion()
    {
        yield return new WaitForSeconds(motionTime);
        PoolingManager.Instance.SetPool(gameObject, "Bugs");
        yield break;
    }
}
