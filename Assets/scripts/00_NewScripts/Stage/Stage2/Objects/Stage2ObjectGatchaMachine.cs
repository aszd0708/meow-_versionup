﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2ObjectGatchaMachine : MonoBehaviour, TouchableObj
{
    public GameObject gatcha;
    public Transform[] createPos = new Transform[2];
    public Sprite[] trashSprite = new Sprite[2];
    public Vector2[] trashPos = new Vector2[4];

    public bool DoTouch(Vector2 touchPose = default)
    {
        PlayGatchaMotion();
        return false;
    }

    private void PlayGatchaMotion()
    {
        int randomNum = Random.Range(0, 10);

        if (randomNum < 9)
        {
            SetGatchaObj(CreateGatcha());
        }
        else
        {
            for (int i = 0; i < Random.Range(2, 6); i++)
            {
                SetGatchaObj(CreateGatcha());
            }
        }
    }

    private void DoGatchaMotion(GameObject trash, int random)
    {
        switch(random)
        {
            case 0:
                trash.transform.DOLocalMove(new Vector3(
                    Random.Range(trashPos[0].x, trashPos[1].x), 
                    Random.Range(trashPos[0].y, trashPos[1].y), 0), 0.5f);
                break;
            case 1:
                trash.transform.DOLocalMove(new Vector3(
                    Random.Range(trashPos[2].x, trashPos[3].x), 
                    Random.Range(trashPos[2].y, trashPos[3].y), 0), 0.5f);
                break;
        }
        PoolingManager.Instance.SetPool(trash, "Gatcha", 4.0f);
    }

    private void SetGatchaObj(GameObject obj)
    {
        int randomPos = Random.Range(0, createPos.Length);
        obj.transform.rotation = Quaternion.Euler(0, 0, Random.Range(0f, 360f));
        obj.transform.SetParent(createPos[randomPos]);
        obj.transform.position = createPos[randomPos].position;
        obj.GetComponent<SpriteRenderer>().sprite = trashSprite[randomPos];
        DoGatchaMotion(obj, randomPos);
    }

    private GameObject CreateGatcha()
    {
        GameObject createGatcha = PoolingManager.Instance.GetPool("Gatcha");
        if(createGatcha == null)
        {
            createGatcha = Instantiate(gatcha);
        }
        return createGatcha;
    }
}
