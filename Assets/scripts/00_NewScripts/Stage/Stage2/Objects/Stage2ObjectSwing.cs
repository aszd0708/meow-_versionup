﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2ObjectSwing : MonoBehaviour, TouchableObj
{
    [Header("그네 양쪽")]
    public Transform[] swingParts;
    [Header("흔드는 시간")]
    public float swingTime;

    private WaitForSeconds swingWait;

    private bool canTouch = true;

    private void Start()
    {
        swingWait = new WaitForSeconds(swingTime);
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        if(canTouch)
        {
            AudioManager.Instance.PlaySound("slide_4", transform.position);
            StartCoroutine(_StartMotion());
        }
        return false;
    }

    private IEnumerator _StartMotion()
    {
        int num = Random.Range(0, 3);
        canTouch = false;
        switch (num)
        {
            case 0:
                for (int i = 0; i < 6; i++)
                {
                    int swingAngle = 10 - (i * 2);
                    if (i % 2 != 0)
                        swingAngle *= -1;
                    for (int j = 0; j < swingParts.Length; j++)
                    {
                        swingParts[j].DOLocalRotate(Vector3.forward * swingAngle, swingTime);
                    }
                    yield return swingWait;
                }
                break;

            case 1:
            case 2:
                for (int i = 0; i < 6; i++)
                {
                    int swingAngle = 10 - (i * 2);
                    if (i % 2 != 0)
                        swingAngle *= -1;
                    DoSwing(swingParts[num - 1], swingAngle);
                }
                break;
        }
        canTouch = true;
       yield break;
    }

    private void DoSwing(Transform swing, float angle)
    {
        swing.DOLocalRotate(Vector3.forward * angle, swingTime);
    }
}
