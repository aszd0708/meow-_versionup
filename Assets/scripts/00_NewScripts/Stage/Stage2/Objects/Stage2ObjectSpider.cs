﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2ObjectSpider : MonoBehaviour, TouchableObj
{
    [SerializeField]
    [Header("거미 아이템Transform")]
    private Transform itemSpider;

    [SerializeField]
    [Header("거미 아이템 베이스")]
    private Stage2ItemSpider spiderInfo;

    [SerializeField]
    [Header("거미가 있어야할 곳")]
    private Transform spiderPose;

    [SerializeField]
    [Header("떨어지는 시간")]
    private float fallingTime;


    /// <summary>
    /// 현재 어떤 모션까지 갔나 Idle -> 대롱 -> 가만히
    /// </summary>
    private int motionStep;

    private bool canTouch = true;

    private Coroutine motionCor;
    private Coroutine spiderPoseCor;

    public bool DoTouch(Vector2 touchPose = default)
    {
        if (!canTouch)
            return false;

        TouchEvent();

        return false;
    }

    private void TouchEvent()
    {
        switch(motionStep)
        {
            case 0:
                motionCor = StartCoroutine(_PlayMotion());
                break;
            case 1:
                StopCoroutine(motionCor);
                StopCoroutine(spiderPoseCor);
                transform.DOLocalRotate(Vector3.zero, 0.3f);
                spiderInfo.DoTouch();
                break;
            default: return;
        }
        motionStep++;
    }

    private IEnumerator _PlayMotion()
    {
        int zPos = 10;
        canTouch = false;
        transform.DOMoveY(-12.5f, fallingTime);
        yield return new WaitForSeconds(fallingTime);
        spiderPoseCor = StartCoroutine(_SetSpiderPose());
        spiderPose.transform.DOLocalMoveY(-1.0f, fallingTime);
        yield return new WaitForSeconds(fallingTime);
        spiderInfo.CanTouch = true;
        transform.DOLocalRotate(Vector3.forward * -10, 0.5f);
        yield return new WaitForSeconds(0.5f);
        WaitForSeconds wait = new WaitForSeconds(1.0f);
        canTouch = true;
        while (true)
        {
            transform.DOLocalRotate(Vector3.forward * zPos, 1.0f);
            yield return wait;
            zPos *= -1;
        }
    }

    private IEnumerator _SetSpiderPose()
    {
        WaitForEndOfFrame frameWait = new WaitForEndOfFrame();
        while (true)
        {
            itemSpider.position = new Vector3(spiderPose.position.x, spiderPose.position.y, itemSpider.position.z);
            yield return frameWait;
        }
    }
}
