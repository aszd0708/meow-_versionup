﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2ObjectWalletPerson : StageObjectBasicEvent
{
    [SerializeField]
    private Stage2ItemWallet wallet;
    [SerializeField]
    private Transform tempWallet;
    public void WalletEvent()
    {
        wallet.gameObject.SetActive(true);
        wallet.PlayFallingToGround(tempWallet);
    }
}
