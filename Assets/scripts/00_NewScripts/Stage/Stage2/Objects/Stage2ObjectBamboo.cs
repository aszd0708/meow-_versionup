﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class Stage2ObjectBamboo : MonoBehaviour, TouchableObj
{
    [Header("잘랐을때 나올 파츠")]
    public GameObject[] cuttingPart;
    [Header("잘랐을때 변할 스프라이트")]
    public Sprite[] cuttingBamboo;
    [Header("흐려지는 시간")]
    public float fadeOutTime;
    [Header("처음 잘린 위치")]
    public Transform firstPos;
    [Header("두번째 잘린 위치")]
    public Transform secondPos;

    private Sprite nowBamboo;
    private Sprite nextBamboo;
    private int state;
    private float scale;

    private BoxCollider2D boxCollider;

    public enum State
    {
        RIGHT, LEFT, UPRIGHT, DOWNLEFT
    }

    private void Awake()
    {
        boxCollider = GetComponent<BoxCollider2D>();
    }

    private void OnEnable()
    {
        scale = Random.Range(3.75f, 4.0f);
        gameObject.transform.localScale = new Vector3(scale, scale, 1);
        state = 0;
        nowBamboo = cuttingBamboo[0];
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        PlayCutMotion();
        return false;
    }

    public void BigCut()
    {
        if (state == 0)
        {
            nextBamboo = cuttingBamboo[1];
            GameObject cuttingParts = SetCuttingPart(state);
            cuttingParts.transform.SetParent(gameObject.transform);
            cuttingParts.transform.localPosition = firstPos.localPosition;
            StartCoroutine(_CutParts(cuttingParts, state));
            state++;
            AudioManager.Instance.PlaySound("slide_4", transform.position);
        }

        else if (state == 1)
        {
            nextBamboo = cuttingBamboo[2];
            GameObject cuttingParts = SetCuttingPart(state);
            cuttingParts.transform.SetParent(gameObject.transform);
            cuttingParts.transform.localPosition = secondPos.localPosition;
            StartCoroutine(_CutParts(cuttingParts, state));
            state++;
            AudioManager.Instance.PlaySound("slide_4", transform.position);
        }

        else
        {
            return;
        }
    }

    private void PlayCutMotion()
    {
        if (state == 0)
        {
            nextBamboo = cuttingBamboo[1];
            GameObject cuttingParts = SetCuttingPart(state);
            cuttingParts.transform.SetParent(gameObject.transform);
            cuttingParts.transform.localPosition = firstPos.localPosition;
            StartCoroutine(_CutParts(cuttingParts, state));
            state++;
            boxCollider.offset = new Vector2(boxCollider.offset.x, 0.4f);
            boxCollider.size = new Vector2(boxCollider.size.x, 0.75f);
        }

        else if (state == 1)
        {
            nextBamboo = cuttingBamboo[2];
            GameObject cuttingParts = SetCuttingPart(state);
            cuttingParts.transform.SetParent(gameObject.transform);
            cuttingParts.transform.localPosition = secondPos.localPosition;
            StartCoroutine(_CutParts(cuttingParts, state));
            state++;
            boxCollider.enabled = false;
        }

        else
            return;
    }

    private GameObject SetCuttingPart(int num)
    {
        GameObject part;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.Append("BambooPart");
        stringBuilder.Append(num);
        part = PoolingManager.Instance.GetPool(stringBuilder.ToString());
        if (part == null)
            part = Instantiate(cuttingPart[num]);
        return part;
    }

    private IEnumerator _CutParts(GameObject cuttingPart, int num)
    {
        State state = (State)Random.Range((int)State.RIGHT, (int)State.DOWNLEFT + 1);

        cuttingPart.transform.localScale = new Vector3(scale - 3.0f, scale - 3.0f, 1);
        switch (state)
        {
            case State.RIGHT:
                cuttingPart.transform.DOLocalMoveX(firstPos.localPosition.x + 1.0f, fadeOutTime);
                break;
            case State.LEFT:
                cuttingPart.transform.DOLocalMoveX(firstPos.localPosition.x - 1.0f, fadeOutTime);
                break;
            case State.UPRIGHT:
                cuttingPart.transform.DOLocalMove(new Vector3(firstPos.localPosition.x + 1, firstPos.localPosition.y + 0.5f, cuttingPart.transform.position.z), fadeOutTime);
                break;
            case State.DOWNLEFT:
                cuttingPart.transform.DOLocalMove(new Vector3(firstPos.localPosition.x - 1, firstPos.localPosition.y - 0.5f, cuttingPart.transform.position.z), fadeOutTime);
                break;
        }

        cuttingPart.transform.DOLocalMoveX(firstPos.localPosition.x + 1.0f, fadeOutTime);
        GetComponent<SpriteRenderer>().sprite = nextBamboo;
        SpriteRenderer sr = cuttingPart.GetComponent<SpriteRenderer>();

        if (GetComponent<SpriteRenderer>().flipX)
            sr.flipX = true;

        Color tempColor = sr.color;
        WaitForEndOfFrame waitFrame = new WaitForEndOfFrame();
        while (tempColor.a > 0f)
        {
            tempColor.a -= Time.deltaTime / fadeOutTime;
            sr.color = tempColor;

            if (tempColor.a <= 0f) tempColor.a = 0f;

            yield return waitFrame;
        }
        sr.color = tempColor;
        yield return new WaitForSeconds(fadeOutTime + 0.5f);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.Append("BambooPart");
        stringBuilder.Append(num);
        PoolingManager.Instance.SetPool(cuttingPart, stringBuilder.ToString());
        yield break;
    }
}
