﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2AnimalBlackCatNero : StageAnimalObjectBase
{
    [Header("각 부분 Renderer")]
    public SpriteRenderer body;
    public SpriteRenderer tail;
    public SpriteRenderer eye;

    [Header("각 부분 Sprite들")]
    public Sprite[] bodySprite = new Sprite[2];
    public Sprite[] tailSprite = new Sprite[2];
    public Sprite[] eyeSprite = new Sprite[2];

    [Header("문!")]
    public Stage2ObjectSuperDoor door;

    [Header("움직이는 목적지 및 아이템 위치")]
    public Transform dest;

    /// <summary>
    /// 아이템 사용 후 터치 가능하게
    /// </summary>
    private bool canTouch = false;

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if(canTouch)
        {
            SetCollect();
            AudioManager.Instance.PlaySound("cat_4", transform.position);
        }
        return false;
    }

    public override void StartItemEvent(GameObject itemObj)
    {
        if (endItemMotion)
        {
            itemObj.SetActive(false);
            return;
        }
        base.StartItemEvent(itemObj);

        StartCoroutine(_PlayWalkMotion());
        StartCoroutine(_PlayWalkEyeMotion());
        StartCoroutine(_MoveToDestination());

        itemObj.transform.position = dest.position;
    }

    void Start()
    {
            body.enabled = false;
            tail.enabled = false;
            eye.enabled = false;
    }

    private IEnumerator _PlayWalkMotion()
    {
        WaitForSeconds wait = new WaitForSeconds(0.5f);
        while (true)
        {
            for (int i = 0; i < 2; i++)
            {
                body.sprite = bodySprite[i];
                tail.sprite = tailSprite[i];
                yield return wait;
            }
        }
    }

    private IEnumerator _PlayWalkEyeMotion()
    {
        WaitForSeconds wait = new WaitForSeconds(0.5f);
        while (true)
        {
            for (int i = 0; i < 2; i++)
            {
                eye.sprite = eyeSprite[i];
                yield return wait;
            }
        }
    }

    private IEnumerator _PlayIdleEyeMotion()
    {
        WaitForSeconds waitSecond = new WaitForSeconds(1.0f);
        WaitForSeconds waitSecondPointFive = new WaitForSeconds(1.5f);
        while (true)
        {
            eye.enabled = true;
            yield return waitSecond;
            eye.enabled = false;
            yield return waitSecondPointFive;
        }
    }

    private IEnumerator _MoveToDestination()
    {
        float doorTime = door.OpenDoorAndMoveCat();
        yield return new WaitForSeconds(doorTime - 0.5f);

        body.enabled = true;
        tail.enabled = true;
        eye.enabled = true;
        yield return new WaitForSeconds(1.5f);

        transform.DOMove(dest.position, 1.5f);
        yield return new WaitForSeconds(1.5f);

        canTouch = true;
        yield break;
    }
}
