﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2AnimalGomaymmee : StageAnimalObjectBase
{
    [SerializeField]
    [Header("UFO 갖고오기")]
    private Stage2ObjectCatUFO catUFO;

    [SerializeField]
    [Header("진짜 필드에 있는 아이템")]
    private GameObject realItem;

    private bool canTouch;

    public bool CanTouch { get => canTouch; set => canTouch = value; }

    [Header("각 부분 Transform")]
    public Transform[] legTransforms = new Transform[4];
    public Transform tailTrasnform;
    public Transform headTransform;

    public void StartMotion()
    {
        canTouch = true;
        StartCoroutine(_PlayWalkMotion());
    }

    private IEnumerator _PlayWalkMotion()
    {
        int a = 1;
        WaitForSeconds wait = new WaitForSeconds(0.5f);
        while (true)
        {
            headTransform.DORotate(Vector3.forward * 10 * a, 0.5f);
            tailTrasnform.DORotate(Vector3.forward * 10 * a, 0.5f);
            for (int i = 0; i < 4; i++)
            {
                legTransforms[i].DORotate(Vector3.forward * 20 * a, 0.5f);
                a *= -1;
            }
            a *= -1;
            yield return wait;
        }
    }

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if(!CanTouch)
        {
            return false;
        }
        SetCollect();
        AudioManager.Instance.PlaySound("ufo_2", transform.position);
        return false;
    }

    public override void StartItemEvent(GameObject itemObj)
    {

        if (endItemMotion)
        {
            itemObj.SetActive(false);
            return;
        }
        base.StartItemEvent(itemObj);
        itemObj.SetActive(false);
        realItem.SetActive(true);
        catUFO.itemTransform = realItem.transform;
        catUFO.ReturnCatUFO();
    }

    private IEnumerator startMo()
    {
        yield return new WaitForSeconds(3.0f);
        realItem.SetActive(true);
        catUFO.itemTransform = realItem.transform;
        catUFO.ReturnCatUFO();

    }
}
