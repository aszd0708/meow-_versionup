﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2AnimalWhiteCat : StageAnimalObjectBase
{
    [Header("각 부분 Renderer")]
    public SpriteRenderer bodyRenderer;
    public SpriteRenderer eyeRenderer;
    [Header("각 부분에 사용할 Sprites")]
    public Sprite sitBodySprite;
    public Sprite[] walkBodySprites = new Sprite[2];
    public Sprite[] eyeSprites = new Sprite[2];
    [Header("꼬리 Transform")]
    public Transform tailTransform;
    [Header("ItemSpriteRenderer")]
    public SpriteRenderer item;

    private bool canTouch = false;
    void Start()
    {
        StartCoroutine(_PlayEyeMotion());
        StartCoroutine(_PlayTailMotion());
    }

    private IEnumerator _PlayEyeMotion()
    {
        WaitForSeconds wait = new WaitForSeconds(0.3f);
        int index = 0;
        while (true)
        {
            eyeRenderer.sprite = eyeSprites[index];
            yield return wait;
            index++;
            index %= eyeSprites.Length;
        }
    }

    private IEnumerator _PlayWalkMotion()
    {
        item.enabled = true;
        transform.DOMove(item.transform.position, 1.5f);
        for (int i = 0; i < 5; i++)
        {
            bodyRenderer.sprite = walkBodySprites[i % 2];
            yield return new WaitForSeconds(0.3f);
        }
        bodyRenderer.sprite = sitBodySprite;
        item.enabled = false;
        canTouch = true;
        yield break;
    }

    private IEnumerator _PlayTailMotion()
    {
        WaitForSeconds waitHalfSecond = new WaitForSeconds(0.5f);
        float rotationAngle = 20;
        while (true)
        {
            tailTransform.DORotate(Vector3.forward * rotationAngle, 0.5f);
            yield return waitHalfSecond;
            rotationAngle *= -1;
        }
    }

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if(!canTouch)
        {
            return false;
        }
        SetCollect();
        AudioManager.Instance.PlaySound("baby_cat", transform.position);
        return false;
    }

    public override void StartItemEvent(GameObject itemObj)
    {

        if (endItemMotion)
        {
            itemObj.SetActive(false);
            return;
        }
        base.StartItemEvent(itemObj);

        StartCoroutine(_PlayWalkMotion());
    }
}
