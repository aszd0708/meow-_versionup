﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2AnimalSwagCat : StageAnimalObjectBase
{
    [Header("입 랜더러")]
    public SpriteRenderer mouseRenderer;
    [Header("입 스프라이트들")]
    public Sprite mouse;
    public Sprite originMouse;

    [Header("각 부분 Transform")]
    public Transform armTransform;
    public Transform headTransform;
    
    [Header("아이템 스프라이트")]
    public SpriteRenderer itemSprite;

    

    [Header("움직이는 포지션")]
    public Transform movePose;

    private bool canTouch;

    void Start()
    {
        StartCoroutine(_PlayMotion());
    }

    private IEnumerator _PlayMotion()
    {
        WaitForSeconds twoWait = new WaitForSeconds(0.2f);
        WaitForSeconds oneWait = new WaitForSeconds(1.0f);
        while (true)
        {
            headTransform.transform.DORotate(Vector3.forward * -5, 0.2f);
            yield return twoWait;
            mouseRenderer.sprite = mouse;
            for (int i = 0; i < 3; i++)
            {
                headTransform.transform.DORotate(Vector3.forward * -3, 0.2f);
                armTransform.transform.DORotate(Vector3.forward * 10, 0.2f);
                yield return twoWait;
                headTransform.transform.DORotate(Vector3.forward * -5, 0.2f);
                armTransform.transform.DORotate(Vector3.zero, 0.2f);
                yield return twoWait;
            }
            headTransform.transform.DORotate(Vector3.zero, 0.2f);
            mouseRenderer.sprite = mouse;
            yield return twoWait;
            mouseRenderer.sprite = originMouse;
            yield return oneWait;
        }
    }

    public void MoveMotion()
    {
        itemSprite.enabled = true;
        transform.DOMove(movePose.position, 0.5f);
        canTouch = true;
        //transform.DOMove(new Vector3(-39.81f, 41.71f, -1), 0.5f);
    }

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if(canTouch)
        {
            SetCollect();
            AudioManager.Instance.PlaySound("cat_10", transform.position);
        }
        return false;
    }

    public override void StartItemEvent(GameObject itemObj)
    {

        if (endItemMotion)
        {
            itemObj.SetActive(false);
            return;
        }
        base.StartItemEvent(itemObj);

        itemObj.SetActive(false);
        MoveMotion();
    }
}
