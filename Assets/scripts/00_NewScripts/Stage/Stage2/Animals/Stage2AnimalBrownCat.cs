﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2AnimalBrownCat : StageAnimalObjectBase
{
    [Header("거미가 생성될 위치")]
    public Transform hitBox;

    [Header("거미 오브젝트")]
    public Transform spider;

    [Header("머리 꼬리 Transform")]
    public Transform headTransform;
    public Transform tailTransform;

    [Header("머리 꼬리 Transform")]
    public Transform leftArmTransform;
    public Transform rightArmTransform;

    [Header("각 부분 SpriteRenderer")]
    public SpriteRenderer headRenderer;
    public SpriteRenderer tailRenderer;
    public SpriteRenderer walkRenderer;

    public SpriteRenderer sitBodyRenderer;
    public SpriteRenderer leftArmRenderer;
    public SpriteRenderer rightArmRenderer;

    [Header("움직일때 사용할 SpriteS")]
    public Sprite[] walkSprite = new Sprite[2];

    [Header("콜라이더(자신)")]
    [SerializeField]
    private BoxCollider2D animalCollider;

    [SerializeField]
    [Header("거미 움직일 포지션(안정하면 알아서 결정됨)")]
    private Vector3 spiderLeftPos;
    [SerializeField]
    private Vector3 spiderRightPos;

    private bool canTouch;
    void Start()
    {
        SetPosition();
        sitBodyRenderer.enabled = false;
        leftArmRenderer.enabled = false;
        rightArmRenderer.enabled = false;

        if (spiderLeftPos == Vector3.zero)
        {
            spiderLeftPos = new Vector3(-0.198f, -0.681f, 0);
        }

        if(spiderRightPos == Vector3.zero)
        {
            spiderRightPos = new Vector3(0.199f, -0.704f, 0);
        }
    }
    public override bool DoTouch(Vector2 touchPose = default)
    {
        if(canTouch)
        {
            SetCollect();
            AudioManager.Instance.PlaySound("cat_4", transform.position);
        }
        return false;
    }

    public override void StartItemEvent(GameObject itemObj)
    {
        if (endItemMotion)
        {
            itemObj.SetActive(false);
            return;
        }
        base.StartItemEvent(itemObj);

        StartCoroutine(_StartMotion(itemObj));
    }

    /// <summary>
    /// 처음 머리하고 꼬리 포지션 설정
    /// </summary>
    void SetPosition()
    {
        headTransform.localPosition = new Vector3(-0.016f, 0.629f, 0);
        tailTransform.localPosition = new Vector3(1.41f, 0.53f, 0);
    }

    private IEnumerator _StartMotion(GameObject item)
    {
        headRenderer.enabled = true;
        walkRenderer.enabled = true;
        tailRenderer.enabled = true;
        yield return StartCoroutine(WalkMotion(3));
        PlayWithSpider(item);
        yield break;
    }

    private IEnumerator WalkMotion(int loopCount)
    {
        transform.DOMove(hitBox.position, 1.5f);
        WaitForSeconds halfSecondWait = new WaitForSeconds(0.5f);
        for (int i = 0; i < loopCount; i++)
        {
            walkRenderer.sprite = walkSprite[i % walkSprite.Length];
            yield return halfSecondWait;
        }
        yield break;
    }

    private void PlayWithSpider(GameObject item)
    {
        headTransform.localPosition = new Vector3(-0.078f, 0.761f, 0);
        tailTransform.localPosition = new Vector3(0.515f, 0.422f, 0);

        animalCollider.offset = Vector2.up * animalCollider.offset.y;
        animalCollider.size = new Vector2(animalCollider.size.x, 2);

        spider.SetParent(leftArmTransform);
        spider.localPosition = spiderLeftPos;

        walkRenderer.enabled = false;
        sitBodyRenderer.enabled = true;
        leftArmRenderer.enabled = true;
        rightArmRenderer.enabled = true;
        spider.gameObject.SetActive(true);
        transform.rotation = Quaternion.identity;

        canTouch = true;
        item.SetActive(false);
        StartCoroutine(_PlayWithSpider());
    }

    private IEnumerator _PlayWithSpider()
    {
        WaitForSeconds threeWait = new WaitForSeconds(0.3f);
        WaitForSeconds twoWait = new WaitForSeconds(0.2f);
        WaitForSeconds halfWait = new WaitForSeconds(0.5f);
        int i = 0;
        float negative = -1;
        Transform armParent = null;
        Transform moveArm = null;
        Vector3 rotation = default;
        Vector3 spiderPose = default;
        while (true)
        {
            if (i % 2 == 0)
            {
                moveArm = leftArmTransform;
                armParent = rightArmTransform;
                negative = -1;
                rotation = Vector3.forward * 360;
                spiderPose = spiderRightPos;
            }

            if (i % 2 != 0)
            {
                moveArm = rightArmTransform;
                armParent = leftArmTransform;
                negative = 1;
                rotation = Vector3.zero;
                spiderPose = spiderLeftPos;
            }

            moveArm.DORotate(Vector3.forward * negative  * 20, 0.3f);
            yield return threeWait;

            moveArm.DORotate(Vector3.forward * -negative * 10, 0.3f);
            yield return twoWait;

            moveArm.DORotate(Vector3.zero, 0.1f);
            spider.SetParent(armParent);
            spider.DOLocalMove(spiderPose, 0.5f);
            spider.DOLocalRotate(rotation, 0.5f, RotateMode.FastBeyond360).SetEase(Ease.OutSine);
            yield return halfWait;
            i++;
        }
    }
}
