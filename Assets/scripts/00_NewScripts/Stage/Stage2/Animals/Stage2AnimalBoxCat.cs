﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2AnimalBoxCat : StageAnimalObjectBase
{
    [SerializeField]
    [Header("빡스 안에 있는 고양이들")]
    private BoxCatMotion boxCats;

    protected override void OnEnable()
    {
        base.OnEnable();

        boxCats.StartMotion();
    }

    public override bool DoTouch(Vector2 touchPose = default)
    {
        SetCollect();
        AudioManager.Instance.PlaySound("baby_cat", transform.position);
        return false;
    }
}
