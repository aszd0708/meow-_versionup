﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2AnimalJelly : StageAnimalObjectBase
{
    [SerializeField]
    [Header("각 부분 Renderer")]
    private SpriteRenderer body;
    public SpriteRenderer sock;

    [Header("각 부분 Sprite들")]
    public Sprite standBodySprite;
    public Sprite biteBodySprite;
    public Sprite[] walkSprite;

    [SerializeField]
    [Header("이 오브젝트의 콜라이더")]
    private Collider touchCollider;

    /// <summary>
    /// 터치 할 수 있는지
    /// </summary>
    private bool canTouch = false;

    // Start is called before the first frame update
    void Start()
    {
        if(!body)
        {
            body = GetComponent<SpriteRenderer>();
        }
        sock.enabled = false;
        body.sprite = standBodySprite;
    }

    private IEnumerator _PlayMotion()
    {
        WaitForSeconds waitHalfSecond = new WaitForSeconds(0.5f);
        body.sortingOrder = 1;
        sock.sortingOrder = 2;
        body.sprite = biteBodySprite;
        yield return waitHalfSecond;
        body.sprite = standBodySprite;
        sock.enabled = true;
        yield return new WaitForSeconds(1.0f);
        sock.enabled = false;
        // 가는곳 설정해서 가기
        for (int i = 0; i < 4; i++)
        {
            body.sprite = walkSprite[0];
            yield return waitHalfSecond;
            body.sprite = walkSprite[1];
            yield return waitHalfSecond;
        }
        body.sprite = standBodySprite;
        sock.enabled = true;
        canTouch = true;
        yield break;
    }

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if(!canTouch)
        {
            return false;
        }

        SetCollect();
        AudioManager.Instance.PlaySound("dog_1", transform.position);
        return false;
    }

    public override void StartItemEvent(GameObject itemObj)
    {

        if (endItemMotion)
        {
            itemObj.SetActive(false);
            return;
        }

        canTouch = true;
        base.StartItemEvent(itemObj);
        itemObj.SetActive(false);
        StartCoroutine(_PlayMotion());
    }
}
