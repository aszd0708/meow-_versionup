﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2AnimalSpottedCat : StageAnimalObjectBase
{
    [Header("각 부분 Renderer")]
    public SpriteRenderer bodyRenderer;
    public SpriteRenderer tailRenderer;
    [Header("몸 움직임 Sprites")]
    public Sprite[] motion = new Sprite[2];

    [Header("레이져!")]
    public GameObject laser;

    [Header("아이템 놓았을때 이동하는 목적지")]
    public Transform hitbox;

    [SerializeField]
    [Header("꼬리 움직이는 시간")]
    private float tailTime = 1.0f;

    private Coroutine tailMoveCor;

    private bool canTouch = false;
    void Start()
    {
        tailMoveCor = StartCoroutine(_PlayTailMotion());
    }

    private IEnumerator _PlayTailMotion()
    {
        WaitForSeconds wait = new WaitForSeconds(tailTime);
        while (true)
        {
            tailRenderer.flipX = !tailRenderer.flipX;
            yield return wait;
        }
    }

    private IEnumerator _StartMotion()
    {

        laser.SetActive(true);
        yield return new WaitForSeconds(1.0f);

        transform.DOJump(new Vector3(hitbox.position.x, transform.position.y, transform.position.z), 0.5f, 1, 0.5f);

        canTouch = true;

        StopCoroutine(tailMoveCor);
        tailTime = 0.5f;
        StartCoroutine(_PlayTailMotion());

        bool flip = false;
        WaitForSeconds waitHalfSecond = new WaitForSeconds(0.5f);

        while (true)
        {
            for (int j = 0; j < Random.Range(1, 4); j++)
            {
                for (int i = 0; i < 2; i++)
                {
                    //transform.position
                    bodyRenderer.sprite = motion[0];
                    yield return waitHalfSecond;

                    bodyRenderer.sprite = motion[1];
                    yield return waitHalfSecond;
                }
            }

            if (!flip)
                transform.DOLocalRotate(Vector3.up * 180, 0.5f);
            else
                transform.DOLocalRotate(Vector3.zero, 0.5f);
            yield return waitHalfSecond;
            flip = !flip;
        }
    }
    public override bool DoTouch(Vector2 touchPose = default)
    {
        if(!canTouch)
        {
            return false;
        }

        SetCollect();
        AudioManager.Instance.PlaySound("cat_4", transform.position);
        return false;
    }

    public override void StartItemEvent(GameObject itemObj)
    {
        if (endItemMotion)
        {
            PoolingManager.Instance.SetPool(itemObj, "ItemDummy");
            return;
        }

        base.StartItemEvent(itemObj);
        itemObj.SetActive(false);
        StartCoroutine(_StartMotion());
    }
}
