﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2AnimalPanda : StageAnimalObjectBase
{
    [Header("왼쪽 팔 Transform")]
    public Transform leftArmTrasnform;
    [Header("풀때기 Renderer")]
    public SpriteRenderer leafRenderer;
    [Header("입 Renderer")]
    public SpriteRenderer mouseRenderer;

    private Vector3 originPos;

    void Start()
    {
        originPos = leftArmTrasnform.localPosition;
        StartCoroutine(_PlayMotion());
    }

    public override bool DoTouch(Vector2 touchPose = default)
    {
        SetCollect();
        AudioManager.Instance.PlaySound("slide_3", transform.position);
        return false;
    }

    private IEnumerator _PlayMotion()
    {
        WaitForSeconds zeroPfive = new WaitForSeconds(0.5f);
        WaitForSeconds zeroPtwo = new WaitForSeconds(0.2f);
        WaitForSeconds one = new WaitForSeconds(1.0f);

        Vector3 movePos = new Vector3(-0.578f, 0.051f, leftArmTrasnform.transform.position.z);

        while (true)
        {
            leafRenderer.enabled = true;
            yield return zeroPfive;
            mouseRenderer.enabled = true;
            leftArmTrasnform.DOLocalMove(movePos, 0.5f);
            yield return zeroPfive;
            leafRenderer.enabled = false;
            for (int i = 0; i < 3; i++)
            {
                mouseRenderer.enabled = false;
                yield return zeroPtwo;
                mouseRenderer.enabled = true;
                yield return zeroPtwo;
            }
            leftArmTrasnform.DOLocalMove(originPos, 0.5f);
            mouseRenderer.enabled = false;
            yield return one;
        }
    }
}
