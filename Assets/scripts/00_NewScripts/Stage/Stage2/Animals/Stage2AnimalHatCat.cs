﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2AnimalHatCat : StageAnimalObjectBase
{
    [Header("각 부분 SpriteRenderer")]
    public SpriteRenderer handRenderer;
    public SpriteRenderer headRenderer;
    public SpriteRenderer eyeRenderer;
    public SpriteRenderer[] lightEffectRenderers = new SpriteRenderer[2];

    [Space(20)]

    [Header("각 부분 Sprite")]
    public Sprite[] headSprite = new Sprite[2];
    public Sprite[] effect = new Sprite[4];

    [SerializeField]
    [Header("모자가 떨어질 포지션")]
    private Transform hatDropPose;

    private bool isDrop = false;

    /// <summary>
    /// 두번때 터치일때 수집 할 수 있음
    /// </summary>
    private bool canCollect = false;
    public override bool DoTouch(Vector2 touchPose = default)
    {
        if (canCollect)
        {
            SetCollect();
            AudioManager.Instance.PlaySound("cat_5", transform.position);
        }
        return false;
    }
    void Start()
    {
        handRenderer.enabled = false;
        headRenderer.enabled = false;
        eyeRenderer.enabled = false;
        lightEffectRenderers[0].enabled = false;
        lightEffectRenderers[1].enabled = false;
    }

    private void StartMotion()
    {
        StartCoroutine(_StartMotion());
    }
    private IEnumerator _PlayEffectMotion()
    {
        int random;
        WaitForSeconds wait = new WaitForSeconds(0.5f);
        while (true)
        {
            random = Random.Range(0, 4);
            lightEffectRenderers[0].sprite = effect[random];
            lightEffectRenderers[1].sprite = effect[random];
            yield return wait;
        }
    }

    private IEnumerator _PlayHeadMotion()
    {
        int index = 0;
        while (true)
        {
            headRenderer.sprite = headSprite[index];
            yield return new WaitForSeconds(Random.Range(0.5f, 1.5f));
            index++;
            index %= headSprite.Length;
        }
    }

    private IEnumerator _StartMotion()
    {
        yield return new WaitForSeconds(0.3f);
        handRenderer.enabled = true;
        yield return new WaitForSeconds(0.3f);
        headRenderer.enabled = true;
        eyeRenderer.enabled = true;
        lightEffectRenderers[0].enabled = true;
        lightEffectRenderers[1].enabled = true;

        StartCoroutine(_PlayEffectMotion());
        StartCoroutine(_PlayHeadMotion());

        canCollect = true;
        yield break;
    }

    public void DropHat()
    {
        StartCoroutine(_DropHat());
    }

    private IEnumerator _DropHat()
    {
        transform.DOMove(hatDropPose.position, 1.0f);
        yield return new WaitForSeconds(0.5f);
        transform.DORotate(Vector3.zero,0.5f);
        yield return new WaitForSeconds(0.5f);
        StartMotion();
        yield break;
    }
}
