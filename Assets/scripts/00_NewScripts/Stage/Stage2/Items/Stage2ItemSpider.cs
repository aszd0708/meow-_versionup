﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2ItemSpider : StageItemBase
{
    private bool canTouch = false;

    public bool CanTouch { get => canTouch; set => canTouch = value; }

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if(CanTouch)
        {
            return base.DoTouch(touchPose);
        }

        else
        {
            return false;
        }
    }
}
