﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2ItemWallet : StageItemBase
{
    [SerializeField]
    [Header("떨어지는 포지션")]
    private Transform dropPose;
    
    private bool canCollct = false;

    private Coroutine animCor;

    public bool CanCollct { get => canCollct; set => canCollct = value; }

    protected override void OnEnable()
    {
        base.OnEnable();
        canCollct = false;
    }

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if(CanCollct)
        {
            return base.DoTouch(touchPose);
        }
        else
        {
            return false;
        }
    }
        
    public void PlayFallingToGround(Transform walletTransform)
    {
        if (animCor != null)
            return;
        animCor = StartCoroutine(_PlayFallenToGround(walletTransform));
    }

    private IEnumerator _PlayFallenToGround(Transform walletTransform)
    {
        spriteRenderer.enabled = true;
        transform.position = walletTransform.position;
        transform.rotation = walletTransform.rotation;
        transform.localScale = walletTransform.localScale;
        transform.SetParent(null);
        transform.rotation = Quaternion.Euler(Vector3.forward * 180);
        transform.DOMove(dropPose.position, 1.0f);
        transform.DOLocalRotate(Vector3.zero, 1.0f, RotateMode.FastBeyond360);
        yield return new WaitForSeconds(1.0f);
        transform.DOScale(new Vector3(1f, 1f, 1), 0.5f);
        canCollct = true;
        yield break;
    }
}
