﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2ItemCatAlien : StageItemBase
{
    [SerializeField]
    [Header("도착 좌표들")]
    private Vector2[] movePoses;
    private Coroutine flyingCor;

    private bool canTouch = true;

    private bool bTurnOn = true;

    protected override void OnEnable()
    {
        base.OnEnable();

        if(bTurnOn)
        {
            if(!SaveDataManager.Instance.ItemDiction[ItemName])
            {
                bTurnOn = false;
                gameObject.SetActive(false);
                return;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        flyingCor = StartCoroutine(_Fly());
    }

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if(canTouch)
        {
            StopCoroutine(flyingCor);
            transform.DOPause();
            return base.DoTouch(touchPose);
        }

        else
        {
            return false;
        }
    }

    private IEnumerator _Fly()
    {
        transform.localScale = new Vector3(2, 2, 0);
        transform.DOMove(new Vector3(movePoses[Random.Range(0, movePoses.Length)].x, movePoses[Random.Range(0, movePoses.Length)].y, -6f), 30.0f);
        transform.DOLocalRotate(new Vector3(0, 0, 1800), 120f, RotateMode.FastBeyond360);
        yield return new WaitForSeconds(20.0f);
        canTouch = false;
        transform.DOScale(Vector3.zero, 2.0f);
        transform.DOLocalRotate(Vector3.forward * 1800f, 3.0f, RotateMode.FastBeyond360).SetEase(Ease.Linear);
        yield return new WaitForSeconds(2.0f);
        gameObject.SetActive(false);
        yield break;
    }
}
