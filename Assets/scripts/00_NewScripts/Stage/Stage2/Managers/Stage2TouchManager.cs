﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2TouchManager : StageTouchManagerBase
{

    protected override bool DoStageTouchDown()
    {
        //base.DoStageTouchDown();

        Vector3 touchPose = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(touchPose, Vector2.zero, 0.0f, slideLayer);

        bool isChanceMin = true;

        if(hit.transform)
        {
            isChanceMin = hit.transform.GetComponent<TouchDownableObj>().DoTouchDown(touchPose);
        }

        return isChanceMin;
    }
}
