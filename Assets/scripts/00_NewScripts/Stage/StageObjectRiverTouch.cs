﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageObjectRiverTouch : MonoBehaviour, TouchableObj
{
    [SerializeField]
    [Header("꽝 먼지")]
    private GameObject touchDust;

    public bool DoTouch(Vector2 touchPose = default)
    { 
        AudioManager.Instance.PlaySound("water_2", transform.position);
        CreateDustEffect(touchPose);
        return true;
    }
    private void CreateDustEffect(Vector2 touchPose)
    {
        GameObject dust = PoolingManager.Instance.GetPool("Dust");
        if (dust == null)
            dust = Instantiate(touchDust);

        dust.transform.position = touchPose;
    }

}
