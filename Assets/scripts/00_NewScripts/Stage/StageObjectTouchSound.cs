﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageObjectTouchSound : MonoBehaviour, TouchableObj
{
    [SerializeField]
    private string audioName;

    public bool DoTouch(Vector2 touchPose = default)
    {
        AudioManager.Instance.PlaySound(audioName, transform.position);
        return false;
    }
}
