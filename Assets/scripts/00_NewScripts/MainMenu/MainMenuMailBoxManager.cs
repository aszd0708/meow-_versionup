﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MainMenuMailBoxManager : MonoBehaviour
{
    [SerializeField]
    private RectTransform capRect;
    [SerializeField]
    private RectTransform wingRect;

    [SerializeField]
    private float capRotation = -120;

    [SerializeField]
    private float wingRotation = -210;

    private void Start()
    {
        int isCollect = PlayerPrefs.GetInt("NewCollectLetter", 0);
        if(isCollect == 1)
        {
            SetNewLetterWing();
        }
    }

    private void SetNewLetterWing()
    {
        wingRect.DORotate(Vector3.zero, 0.5f).SetEase(Ease.OutElastic);
    }

    public void SetDownLetterWing()
    {
        wingRect.DOKill();
        wingRect.DORotate(Vector3.forward * wingRotation, 0.5f).SetEase(Ease.OutElastic);
    }

    public void OpenCap()
    {
        capRect.DOKill();
        capRect.DORotate(Vector3.right * capRotation, 1.0f).SetEase(Ease.OutElastic);
    }

    public void CloseCap()
    {
        capRect.DOKill();
        capRect.DORotate(Vector3.zero, 1.0f).SetEase(Ease.OutElastic);
    }

    public void SetPlayerPrefs()
    {
        PlayerPrefs.SetInt("NewCollectLetter", 0);
    }
}
