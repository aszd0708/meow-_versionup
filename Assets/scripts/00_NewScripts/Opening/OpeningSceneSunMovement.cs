﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class OpeningSceneSunMovement : MonoBehaviour
{
    [SerializeField]
    private Transform lensFlareTransform;

    private Vector3 startPose;
    private Vector3 endPose;

    private void OnEnable()
    {
        startPose = Camera.main.ViewportToWorldPoint(Vector3.up);
        startPose = new Vector3(startPose.x-0.1f, startPose.y-0.1f, 0);
        endPose = Camera.main.ViewportToWorldPoint(new Vector3(0.24f, 1));
        endPose = new Vector3(endPose.x, endPose.y, 0);

        StartCoroutine(_Move());
    }

    private IEnumerator _Move()
    {
        yield return new WaitForSeconds(3f);
        lensFlareTransform.position = startPose;
        lensFlareTransform.DOMoveX(endPose.x, 1.5f).SetEase(Ease.OutSine);
        yield return new WaitForSeconds(1.0f);
        lensFlareTransform.DOMoveY(endPose.y, 0.5f).SetEase(Ease.OutQuint);
        yield break;
    }    
}
