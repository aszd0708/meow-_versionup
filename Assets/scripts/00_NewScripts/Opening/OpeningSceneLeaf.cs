﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpeningSceneLeaf : MonoBehaviour
{
    [SerializeField]
    private bool xRot, yRot, zRot;

    [SerializeField]
    [Range(0, 270)]
    private float xRotValue, yRotValue, zRotValue;

    [SerializeField]
    private bool isRandomAxis;

    private Vector3 moveVector;

    [SerializeField]
    private float[] moveVectorX;
    [SerializeField]
    private float[] moveVectorY;

    private Vector3 rotationAxis;

    private Vector3 outside;

    private void OnEnable()
    {
        outside = Camera.main.ViewportToWorldPoint(Vector3.right);
        if (isRandomAxis)
        {
            xRot = RandomAxis();
            yRot = RandomAxis();
            zRot = RandomAxis();
        }
        SetMoveVector();
    }

    private void Update()
    {
        SetRotation();
        SetMove();
    }

    private bool RandomAxis()
    {
        bool axis;
        int randomValue = Random.Range(0, 2);
        if(randomValue == 0)
        {
            axis = false;
        }

        else
        {
            axis = true;
        }
        return axis;
    }

    private void SetRotationAxis()
    {
        rotationAxis = Vector3.zero;
        if (xRot)
        {
            rotationAxis += Vector3.right * xRotValue;
        }

        if (yRot)
        {
            rotationAxis += Vector3.up * yRotValue;
        }

        if (zRot)
        {
            rotationAxis += Vector3.forward * zRotValue;
        }
    }

    private void SetRotation()
    {
        SetRotationAxis();
        transform.Rotate(rotationAxis * Time.deltaTime);
    }


    private void SetMove()
    {
        transform.position += moveVector * Time.deltaTime;

        if(transform.position.x >= outside.x)
        {
            PoolingManager.Instance.SetPool(gameObject, "Leaf");
        }
    }

    private void SetMoveVector()
    {
        moveVector = new Vector2(Random.Range(moveVectorX[0], moveVectorX[1]), Random.Range(moveVectorY[0], moveVectorY[1]));
    }
}
