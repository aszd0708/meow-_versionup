﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpeningSceneLeafCreater : MonoBehaviour
{
    public Camera cam;
    public GameObject leaf;

    [SerializeField]
    private Sprite[] leafSprites;

    [SerializeField]
    private float[] createPoseX, createPoseY;

    private void Start()
    {
        StartCoroutine(_CreateLeaf());
    }

    private Vector3 SetCreatePose()
    {
        Vector3 createPose = Camera.main.ViewportToWorldPoint(Vector3.up);
        createPose = new Vector3(createPose.x + Random.Range(createPoseX[0], createPoseX[1]), createPose.y + Random.Range(createPoseY[0], createPoseY[1]));
        return createPose;
    }

    private void CreateLeaf()
    {
        GameObject createLeaf = PoolingManager.Instance.GetPool("Leaf");
        if(createLeaf == null)
        {
            createLeaf = Instantiate(leaf);
        }
        createLeaf.transform.position = SetCreatePose();
        createLeaf.transform.SetParent(transform);
        createLeaf.transform.localScale = Vector3.one;
        createLeaf.GetComponent<SpriteRenderer>().sprite = leafSprites[Random.Range(0, leafSprites.Length)];
        createLeaf.SetActive(true);
    }

    private IEnumerator _CreateLeaf()
    {
        while(true)
        {
            CreateLeaf();
            yield return new WaitForSeconds(Random.Range(0.5f, 1.4f));
        }
    }
}
