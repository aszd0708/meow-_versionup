﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpeningSceneScaleFixxer : MonoBehaviour
{
    public SpriteRenderer sp;
    private void Awake()
    {
        float spriteX = sp.sprite.bounds.size.x;
        float spriteY = sp.sprite.bounds.size.y;

        float screenY = Camera.main.orthographicSize * 2;
        float screenX = screenY / Screen.height * Screen.width;

        transform.localScale *= new Vector2(Mathf.Ceil(screenX / spriteX), Mathf.Ceil(screenY / spriteY));
    }
}
