﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class OpeningSceneCatController : MonoBehaviour
{
    [Header("눈에 관련된 요소들")]
    [SerializeField]
    private SpriteRenderer eyeRenderer;
    [SerializeField]
    private Sprite[] eyeSprites;
    [Header("이 시간 만큼 눈을 움직임")]
    [SerializeField]
    private float[] eyeChangeTime;

   
    [Header("꼬리에 관한 요소들")]
    [SerializeField]
    [Space(20)]
    private Transform tailTransform;
    [SerializeField]
    private float[] tailAngle = new float[2];
    [SerializeField]
    private float tailSwingTime;

    private void Start()
    {
        StartCoroutine(_EyeMotion());
        StartCoroutine(_TailMotion());
    }

    private IEnumerator _EyeMotion()
    {
        int eyeIndex = 0;
        while(true)
        {
            for(int i = 0; i < eyeChangeTime.Length; i++)
            {
                eyeRenderer.sprite = eyeSprites[eyeIndex++];
                eyeIndex %= eyeSprites.Length;
                yield return new WaitForSeconds(eyeChangeTime[i]);
            }
            eyeRenderer.sprite = eyeSprites[0];
            yield return new WaitForSeconds(2.0f);
        }
    }

    private IEnumerator _TailMotion()
    {
        int angleIndex = 0;
        WaitForSeconds wait = new WaitForSeconds(tailSwingTime);
        while(true)
        {
            tailTransform.DORotate(Vector3.forward * tailAngle[angleIndex++], tailSwingTime).SetEase(Ease.OutQuad);
            angleIndex %= tailAngle.Length;
            yield return wait;
        }
    }
}
