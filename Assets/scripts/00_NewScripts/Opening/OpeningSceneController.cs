﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class OpeningSceneController : MonoBehaviour
{
    [SerializeField]
    private Image bg, logo;

    [SerializeField]
    private RectTransform logoRect;

    private Text startText;

    private bool isEnd = false;

    private bool isTimer;

    private float nowTime = 0.0f;

    private float waitTime = 1.0f;

    private float currentTime = 0.0f;

    [SerializeField]
    private float passTime = 5.0f;
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(_OpeningLogo());
    }

    private IEnumerator _OpeningLogo()
    {
        logoRect.localScale = Vector2.zero;
        yield return new WaitForSeconds(0.5f);
        logoRect.DOScale(Vector2.one, 1.0f).SetEase(Ease.OutElastic);
        AudioManager.Instance.PlaySound("logo", transform.position);
        yield return new WaitForSeconds(4.0f);
        bg.DOFade(0, 1.0f);
        logo.DOFade(0, 1.0f);
        yield return new WaitForSeconds(1.0f);
        bg.gameObject.SetActive(false);
        logo.gameObject.SetActive(false);
        isTimer = true;
        AudioManager.Instance.PlaySound("start", transform.position);
        yield break;
    }

    // Update is called once per frame
    void Update()
    {
        if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            return;

        if (isTimer)
        {
            nowTime += Time.deltaTime;
        }

        if(nowTime >= waitTime)
        {

            if (Input.GetMouseButtonDown(0))
            {
                if (isEnd)
                {
                    return;
                }
                ChangeScene();
            }
        }
        currentTime += Time.deltaTime;
        if(currentTime >= passTime)
        {
            ChangeScene();
        }
    }

    private void ChangeScene()
    {
        isEnd = true;
        startText.DOFade(0, 1.0f);

        int tutorialEnd = PlayerPrefs.GetInt("IsTutorialEnd", 0);
        Debug.LogError("Tutorial : " + tutorialEnd);
        if (tutorialEnd == 0)
        {
            SceneChangeManager.Instance.ChangeScene("Stage0 Tutorial");
        }

        else
        {
            SceneChangeManager.Instance.ChangeScene("MainMenu");
        }
        PlayerPrefs.SetInt("OpeningStart", 1);
    }
}
