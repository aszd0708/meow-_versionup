﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PriorityQueue
{
    public List<Node> nowList;

    public PriorityQueue()
    {
        nowList = new List<Node>();
    }

    public void Push(Node data)
    {
        int count = nowList.Count;
        int index = -1;

        if(count < 1)
        {
            nowList.Add(data);
            return;
        }

        for(int i = count; i > 1; i--)
        {
            if(nowList[i] <= data)
            {
                index = i + 1;
                break;
            }
        }
        if(index.Equals(-1))
        {
            index = 0;
        }

        nowList.Insert(index, data);
    }

    public Node Pop()
    {
        Node n = nowList[0];
        nowList.RemoveAt(0);
        return n;
    }

    public int Count
    {
        get
        {
            return nowList.Count;
        }
    }
}
