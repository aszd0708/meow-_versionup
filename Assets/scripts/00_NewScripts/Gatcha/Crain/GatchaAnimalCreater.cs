﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct GatchaAnimal
{
    public int stageIndex;
    public int gatchaCount;
    public float percent;
    public GameObject Animal;
}

public class GatchaAnimalCreater : MonoBehaviour
{
    [SerializeField]
    [Header("동물 인덱스")]
    private int[] gatchaAnimalIndexes;

    [Header("어떤 동물을 뽑아야 하는지에 따른 포인터")]
    private int pointer;

    private GatchaAnimal currentGatcha;

    public GatchaAnimal[] gatchaAnimals;

    public GatchaDollCreater dollCreater;

    private int gatchaCount;

    private int nowStageIndex;

    [SerializeField]
    private AudioClip[] bgmClips;

    [SerializeField]
    private AudioSource audioSource;

    private void Start()
    {
        SetPointer();
    }

    private void SetPointer()
    {
        int stageIndex = PlayerPrefs.GetInt("GatchaStageIndex", 1) - 1;

        nowStageIndex = stageIndex;
        pointer = gatchaAnimalIndexes[stageIndex];
        currentGatcha = gatchaAnimals[stageIndex];
        gatchaCount = GetGatchaCount(stageIndex);

        audioSource.clip = bgmClips[stageIndex];
        audioSource.Play();
    }

    private int GetGatchaCount(int stageIndex)
    {
        int count = SaveDataManager.Instance.Data.Player.GetGatchaCount(stageIndex);
        return count;
    }

    private GameObject GetAnimal()
    {
        GameObject animal = Instantiate(currentGatcha.Animal);
        return animal;
    }

    public GameObject ActiveAnimal(Transform activePose)
    {
        GameObject animal = GetAnimal();
        if (animal == null)
        {
            return null;
        }
        animal.transform.position = activePose.position;
        animal.SetActive(true);
        return animal;
    }

    public bool GetGatchaObj(Transform activePose, out GameObject obj)
    {
        bool isAnimal = false;
        StageAnimalObjectBase animalInfo = currentGatcha.Animal.GetComponent<StageAnimalObjectBase>();
        if (!animalInfo.Info.IsCollect)
        {
            if (GatchaPercentCalculater.GetPercent(gatchaAnimals[nowStageIndex].percent))
            {
                obj = GetAnimal();
                isAnimal = true;
            }
            else
            {
                if (gatchaCount >= gatchaAnimals[nowStageIndex].gatchaCount)
                {
                    obj = GetAnimal();
                    isAnimal = true;
                }
                else
                {
                    obj = dollCreater.CreateOneDoll(activePose);
                }
            }
            SaveDataManager.Instance.EditGatchaCount(nowStageIndex, ++gatchaCount);
        }

        else
        {
            obj = dollCreater.CreateOneDoll(activePose);
        }

        return isAnimal;
    }
}
