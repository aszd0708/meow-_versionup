﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct StageAnimalIndex
{
    public int stageNo;
    public int[] animalIndex;
}

public class GatchaDollCreater : MonoBehaviour
{
    [Header("스테이지에 따른 동물 인덱스")]
    public StageAnimalIndex[] stageAnimalInfo;

    public AnimalJsonData animalJson;

    [SerializeField]
    [Header("동물 스프라이트")]
    private Sprite[] animalSprites;

    public GameObject dollObj;

    [Header("생성 카운트 2개 값 랜덤으로 만들어서 반환")]
    public int[] createCountRange;

    [Header("생성 위치 (0,1에서 x축 랜덤 0 y축으로 생성)")]
    public Transform[] createPoses;

    private int nowStageIndex;

    [SerializeField]
    [Header("인형들 부모")]
    private Transform dolls;

    private void Start()
    {
        nowStageIndex = PlayerPrefs.GetInt("GatchaStageIndex", 1) -1;

        Create();
    }

    private int GetCreateCount(int[] range)
    {
        if (range is null)
        {
            throw new System.ArgumentNullException(nameof(range));
        }

        int createCount = Random.Range(createCountRange[0], createCountRange[1]);
        return createCount;
    }

    private void CreateDollInBox(List<int> collectDollindexes)
    {
        GameObject doll = Instantiate(dollObj);
        doll.GetComponent<SpriteRenderer>().sprite = animalSprites[collectDollindexes[Random.Range(0, collectDollindexes.Count)]];
        float randomX = Random.Range(createPoses[0].position.x, createPoses[1].position.x);
        doll.transform.position = new Vector3(randomX, createPoses[0].position.y + Random.Range(-5f, 1f), 0);
        float random = Random.Range(0.8f, 1.4f);
        doll.transform.localScale = Vector3.one * random;
        doll.transform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 360f));
        doll.transform.SetParent(dolls);
    }

    private GameObject CreateDoll(List<int> collectDollindexes)
    {
        GameObject doll = Instantiate(dollObj);
        doll.GetComponent<SpriteRenderer>().sprite = animalSprites[collectDollindexes[Random.Range(0, collectDollindexes.Count)]];
        return doll;
    }

    private List<int> GetCollectAnimalIndex()
    {
        List<int> collectIndex = new List<int>();
        for (int i = 0; i < stageAnimalInfo[nowStageIndex].animalIndex.Length; i++)
        {
            if (SaveDataManager.Instance.Data.Animals[stageAnimalInfo[nowStageIndex].animalIndex[i]].IsCollect)
            {
                collectIndex.Add(stageAnimalInfo[nowStageIndex].animalIndex[i]);
            }
        }
        return collectIndex;
    }

    private void Create()
    {
        StartCoroutine(_Create());
    }

    private IEnumerator _Create()
    {
        int randomCount = GetCreateCount(createCountRange);
        WaitForSeconds wait = new WaitForSeconds(0.05f);
        for (int i = 0; i < randomCount; i++)
        {
            CreateDollInBox(GetCollectAnimalIndex());
        }
        yield break;
    }

    public GameObject CreateOneDoll(Transform activeTransform)
    {
       GameObject doll = CreateDoll(GetCollectAnimalIndex());
        doll.transform.position = activeTransform.position;
        return doll;
    }
}
