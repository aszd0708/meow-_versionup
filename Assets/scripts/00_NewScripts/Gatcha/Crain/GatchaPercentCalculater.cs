﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GatchaPercentCalculater : MonoBehaviour
{
    /// <summary>
    /// 백분률로 계산
    /// </summary>
    /// <param name="percent">참이 되는 확률</param>
    public static bool GetPercent(float percent)
    {
        int randomValue = Random.Range(0, 100);
        bool isGet = false;
        if(randomValue <= percent)
        {
            isGet = true;
        }

        Debug.Log("뽑 : " + isGet);

        return isGet;
    }
}
