﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraInterMove : MonoBehaviour
{
    public float speed;
    public bool collect;
    public Transform target;
    // Start is called before the first frame update
    void Start()
    {
        collect = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(collect)
        {
            iTween.MoveTo(gameObject, iTween.Hash("x", target.position.x, "y", target.position.y,"speed", speed, "easeType", iTween.EaseType.easeOutCubic));
            collect = false;
        }
    }
}
