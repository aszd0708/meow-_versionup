﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeedingBirdFly : MonoBehaviour
{
    public Sprite feeding;
    public Sprite standing;
    private Vector2 snackPosition;
    private Vector2 flyPosition;
    public float flyingTime;
    
    // 1 스테이지는 -10, -20 y : 20
    // Start is called before the first frame update
    void OnEnable()
    {
        snackPosition = new Vector2(Random.Range(0.5f, 3.0f), Random.Range(-2.5f, -3.5f));
        StartCoroutine("Flying");
    }

    private void SetRandomPos(float[] xy)
    {
        flyPosition = new Vector2(Random.Range(xy[0], xy[1]), xy[2]);
    }

    // Update is called once per frame
    private IEnumerator Flying()
    {
        iTween.MoveTo(gameObject, iTween.Hash("x", snackPosition.x, "y", snackPosition.y, "time", flyingTime, "islocal", true, "movetopath", false, "easeType", iTween.EaseType.linear));
        yield return new WaitForSeconds(flyingTime);
        gameObject.GetComponent<Animator>().enabled = false;
        gameObject.GetComponent<SpriteRenderer>().enabled = false;

        for(int i = 0; i < 2; i++)
        {
            gameObject.transform.GetChild(i).GetComponent<SpriteRenderer>().enabled = true;
        }

        for (int j = 0; j < 3; j++)
        {
            for (int i = 0; i < 5; i++)
            {
                gameObject.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().sprite = feeding;
                yield return new WaitForSeconds(0.1f);
                gameObject.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().sprite = standing;
                yield return new WaitForSeconds(0.1f);
            }
            yield return new WaitForSeconds(0.5f);
        }

        for (int i = 0; i < 2; i++)
        {
            gameObject.transform.GetChild(i).GetComponent<SpriteRenderer>().enabled = false;
        }

        gameObject.GetComponent<Animator>().enabled = true;
        gameObject.GetComponent<SpriteRenderer>().enabled = true;
        iTween.MoveTo(gameObject, iTween.Hash("x", flyPosition.x, "y", flyPosition.y, "time", flyingTime, "islocal", true, "movetopath", false, "easeType", iTween.EaseType.linear));
        yield return new WaitForSeconds(flyingTime);
        //Destroy(gameObject);
        PoolingManager.Instance.SetPool(gameObject, "Bird");
    }
}
