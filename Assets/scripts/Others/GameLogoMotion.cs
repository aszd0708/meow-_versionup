﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameLogoMotion : MonoBehaviour
{
    public SpriteRenderer head;
    public SpriteRenderer hand;
    public Transform speech;
    public Transform text;
    public FadeInOut fade;

    public float motionTime;
    private Vector2[] origionScale = new Vector2[2];

    private void Start()
    {
        origionScale[0] = speech.localScale;
        origionScale[1] = text.localScale;
        head.enabled = false;
        hand.enabled = false;
        head.transform.rotation = Quaternion.Euler(0, 0, 30);
        speech.localScale = new Vector2(0, 0);
        text.localScale = new Vector2(0, 0);
        StartCoroutine("Motion");
    }

    private IEnumerator Motion()
    {
        yield return new WaitForSeconds(0.5f);
        hand.enabled = true;
        head.enabled = true;
        yield return new WaitForSeconds(0.5f);
        head.transform.DORotate(new Vector3(0,0,0), 0.5f);
        speech.DOScale(origionScale[0], 1f).SetEase(Ease.OutElastic);
        text.DOScale(origionScale[1], 1f).SetEase(Ease.OutElastic);
        yield return new WaitForSeconds(1.5f);
        fade.FadeOutF();
        yield return new WaitForSeconds(fade.animTime);
        PlayerPrefs.SetInt("TutorialEnd", 1);
        NextScene();
    }

    private void NextScene()
    {
        if (PlayerPrefs.HasKey("Tutorial"))
        {
            if (PlayerPrefs.GetInt("Tutorial") == 0)
            {
                SceneManager.LoadScene("Stage0 Tutorial");
            }
            // 튜토리얼
            else
            {
                SceneManager.LoadScene("MainMenu");
            } // 메인
        }

        else
        {
            PlayerPrefs.SetInt("Tutorial", 0);
            SceneManager.LoadScene("Stage0 Tutorial");
            // 튜토리얼
        }
    }
}
