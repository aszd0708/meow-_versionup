﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Collections;
using DG.Tweening;

public class TouchInterationCam : MonoBehaviour
{
    public enum Stage
    {
        TUTORIAL, STAGE1, STAGE2, STAGE3
    }
    public Stage STAGE;

    public new GameObject camera;
    public GameObject photo;
    public float checkTime = -0.5f;
    private float timeSpan = 0.5f;
    private Text ChanceNum;
    private int Chance;
    public ChanceNumImage chanceNum;
    public GameObject number;
    public bool cameraTouch = true;
    public BackgoundTouch backgound;
    public CatHint catHint;
    public CatSaveLoad jsonCat;
    public SoundManager soundManager;
    public CoinCatTimer coinCatCounter;
    public GameObject treeTouchEffect;
    public Sprite[] tree1 = new Sprite[2];
    public Sprite[] tree2 = new Sprite[2];
    public AudioClip[] catSound;

    [Header("일반 고양이 컨텐츠 판넬")]
    public Transform catPanelContent;
    [Header("히든 고양이 판넬")]
    public Transform hiddenCatContent;

    private GameObject interItem;
    private bool jelly;
    private Vector3 camPos;

    private readonly string[] touchSound = { "dgtouch1", "dgtouch2", "dgtouch3" };
    // 슬라이드 오브젝트
    private bool slide;

    private TouchCreateCoin touchCreateCoin;

    private bool noChance = false;
    public bool NoChance { get => noChance; set => noChance = value; }

    private int noChanceTouchCount;
    public int NoChanceTouchCount { get => noChanceTouchCount; set => noChanceTouchCount = value; }


    private void Awake()
    {
        touchCreateCoin = FindObjectOfType<TouchCreateCoin>();
    }

    // Start is called before the first frame update
    void Start()
    {
        jelly = true;
        timeSpan = Time.time;
        ChanceNum = GameObject.Find("ChanceNum").GetComponent<Text>();
        Chance = System.Convert.ToInt32(ChanceNum.text);
        if (Chance <= 0)
            NoChance = true;
        NoChanceTouchCount = -1;
    }

    // Update is called once per frame
    void Update()
    {
        if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            return;

        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                if (EventSystem.current.IsPointerOverGameObject(i))
                    return;
            }
        }

        if (!cameraTouch)
            return;

        if (NoChance)
        {
            NoChanceTouch();
        }

        switch (STAGE)
        {
            case Stage.TUTORIAL:
                Tutorial();
                break;
            case Stage.STAGE1:
                Stage1();
                break;
            case Stage.STAGE2:
                Stage2();
                break;
            case Stage.STAGE3:
                Stage3();
                break;
        }
    }

    private void Tutorial()
    {
        if (Input.GetMouseButtonDown(0))
        {
            timeSpan = Time.time;
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            catHint.GoOriginPos();
        }
        else if (Input.GetMouseButtonUp(0))
        {
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(touchPos, Vector2.zero, 0.0f);

            timeSpan -= Time.time;

            if (timeSpan >= checkTime)
            {
                if (hit.collider == null)
                {
                    TutorialManager helper = GameObject.Find("TutorialManager").GetComponent<TutorialManager>();
                    if (helper.i == 4)
                    {
                        chanceNum.GetComponent<ChanceNumImage>().MinusChance();
                        AudioManager.Instance.PlaySound(touchSound, touchPos, null, Random.Range(0.8f, 1.0f));
                        backgound.Touch();
                        VibratorControllor.TouchVib();
                        helper.i++;
                    }
                }

                else
                {
                    VibratorControllor.ObjTouchVib();
                    interItem = hit.collider.gameObject;
                    Debug.Log(interItem.name);
                    if (interItem.tag == "fieldCat")
                    {
                        CatCheck cat = interItem.GetComponent<CatCheck>();
                        StartCoroutine("Jelly", interItem);
                        if (cat.effectSound != null)
                            AudioManager.Instance.PlaySound(cat.effectSound, interItem.transform.position, null, Random.Range(0.8f, 1.0f));
                        if (interItem.name == "InfoCat")
                        {
                            TutorialManager helper = FindObjectOfType<TutorialManager>();
                            if (!interItem.GetComponent<InfoCatMotion>().collect)
                            {
                                if (helper.i == 2)
                                {
                                    helper.i++;
                                    return;
                                }
                                AudioManager.Instance.PlaySound(catSound[0], interItem.transform.position);
                                return;
                            }
                            else
                                if (helper.i == 12)
                                helper.i++;
                        }

                        if (!interItem.GetComponent<CatCheck>().loadCollect)
                        {
                            Debug.Log("사진");
                            photo.GetComponent<PhotoShot>().TakePicture(interItem);
                        }

                        List<CatSaveLoad.JsonCat> jsonCatData = jsonCat.LoadCats();
                        jsonCatData[interItem.GetComponent<CatCheck>().no - 1].Collect = true;
                        jsonCat.SaveCats(jsonCatData, jsonCat.key);

                        for (int i = 0; i < catPanelContent.childCount; i++)
                        {
                            SampleCatButton fieldCatScript = catPanelContent.GetChild(i).GetComponent<SampleCatButton>();
                            if (interItem == fieldCatScript.fieldCat)
                            {
                                fieldCatScript.OnClick();
                                interItem.GetComponent<CatCheck>().CollectObject();
                                interItem.GetComponent<CatCheck>().CheckCollect();
                                break;
                            }
                        }
                    }

                    else if (interItem.tag == "fieldObject")
                    {
                        interItem.GetComponent<SampleButtonScript>().OnClick();
                        interItem.GetComponent<TutorialItemTrigger>().OnClick();
                    }
                    StartCoroutine("Jelly", interItem);
                }
            }
        }
    }

    private void Stage1()
    {
        if (Input.GetMouseButtonDown(0))
        {
            timeSpan = Time.time;
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(touchPos, Vector2.zero, 0.0f);
            catHint.GoOriginPos();
            camPos = camera.transform.position;
        }

        else if (Input.GetMouseButtonUp(0))
        {
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(touchPos, Vector2.zero, 0.0f);

            timeSpan -= Time.time;
            if (Mathf.Abs(camPos.x - camera.transform.position.x) > 0.05f || Mathf.Abs(camPos.y - camera.transform.position.y) > 0.05f)
                return;

            if (timeSpan >= checkTime)
            {
                if (hit.collider == null)
                    CreateDust(touchPos);

                else if (hit.collider.tag == "Tree" || hit.collider.tag == "Tree2")
                    TouchTreeEffectCreate(hit.collider.tag, touchPos);

                else
                {
                    VibratorControllor.ObjTouchVib();
                    interItem = hit.collider.gameObject;
                    if (interItem.tag == "bird")
                        interItem.GetComponent<TouchBird>().Touch();

                    else if (interItem.tag == "fieldObject")
                    {
                        interItem.GetComponent<SampleButtonScript>().OnClick();
                        StartCoroutine(Jelly(interItem));
                    }

                    else if (interItem.tag == "fieldCat")
                    {
                        CatCheck cat = interItem.GetComponent<CatCheck>();
                        StartCoroutine(Jelly(interItem));
                        if (cat.effectSound != null)
                            AudioManager.Instance.PlaySound(cat.effectSound, interItem.transform.position, null, Random.Range(0.8f, 1.0f));
                        if (interItem.name == "BabyCat" && !interItem.GetComponent<BabyCat>().show)
                            return;

                        else if (interItem.name == "YellowCat" && !interItem.GetComponent<YellowCatMove>().huntingMouse)
                            return;


                        else if (interItem.name == "FatCat" && !interItem.GetComponent<FatCatMove>().findChuru)
                            return;

                        else if (interItem.name == "ShitDog")
                            interItem.GetComponent<DogTouch>().Touch();

                        if (!interItem.GetComponent<CatCheck>().loadCollect)
                        {
                            FindObjectOfType<CatHouseNewMark>().SetData(true);
                            photo.GetComponent<PhotoShot>().TakePicture(interItem);
                        }

                        List<CatSaveLoad.JsonCat> jsonCatData = jsonCat.LoadCats();
                        jsonCatData[interItem.GetComponent<CatCheck>().no - 1].Collect = true;
                        jsonCat.SaveCats(jsonCatData, jsonCat.key);

                        for (int i = 0; i < catPanelContent.childCount; i++)
                        {
                            SampleCatButton fieldCatScript = catPanelContent.GetChild(i).GetComponent<SampleCatButton>();
                            if (interItem == fieldCatScript.fieldCat)
                            {
                                fieldCatScript.OnClick();
                                interItem.GetComponent<CatCheck>().CollectObject();
                                interItem.GetComponent<CatCheck>().CheckCollect();
                                break;
                            }
                        }
                        return;
                    }

                    else if (interItem.tag == "hiddenCat")
                    {
                        CatCheck cat = interItem.GetComponent<CatCheck>();

                        if (cat.effectSound != null)
                            AudioManager.Instance.PlaySound(cat.effectSound, interItem.transform.position, null, Random.Range(0.8f, 1.0f));
                        if (!interItem.GetComponent<CatCheck>().loadCollect)
                        {
                            FindObjectOfType<CatHouseNewMark>().SetData(true);
                            photo.GetComponent<PhotoShot>().TakePicture(interItem);
                        }

                        List<CatSaveLoad.JsonCat> jsonCatData = jsonCat.LoadCats();
                        jsonCatData[interItem.GetComponent<CatCheck>().no - 1].Collect = true;
                        jsonCat.SaveCats(jsonCatData, jsonCat.key);

                        for (int i = 0; i < hiddenCatContent.childCount; i++)
                        {
                            SampleCatButton hiddenCatScript = hiddenCatContent.GetChild(0).GetComponent<SampleCatButton>();
                            if (interItem == hiddenCatScript.fieldCat)
                            {
                                hiddenCatScript.OnClick();
                                interItem.GetComponent<CatCheck>().CollectObject();
                                interItem.GetComponent<CatCheck>().CheckCollect();
                                break;
                            }
                        }
                        StartCoroutine("Jelly", interItem);
                        return;
                    }

                    // 이건 인테페이스로 묶으려고 하면 좀 많이 고쳐야 하므로 마지막에 최적화 할 때 고치는거

                    else if (interItem.tag == "NotColObject")
                        interItem.GetComponent<I_NonColObject>().OnClick();


                    else if (interItem.name == "BlurCat")
                        interItem.GetComponent<BlurEasterEgg>().OnClick();

                    else if (interItem.name == "MadCat")
                    {
                        interItem.GetComponent<CoinCatMotion>().OnClick();
                        if (!interItem.GetComponent<MadCatCheck>().collect)
                        {
                            Debug.Log("사진");
                            photo.GetComponent<PhotoShot>().TakePicture(interItem);
                        }
                        else
                            return;
                        List<CatSaveLoad.JsonCat> jsonCatData = jsonCat.LoadCats();
                        jsonCatData[interItem.GetComponent<MadCatCheck>().no - 1].Collect = true;
                        jsonCat.SaveCats(jsonCatData, jsonCat.key);
                    }

                    if (interItem.GetComponent<EffectSound>() != null && interItem.GetComponent<EffectSound>().effectSound != null)
                    {
                        EffectSound sound = interItem.GetComponent<EffectSound>();
                        AudioManager.Instance.PlaySound(sound.effectSound, interItem.transform.position, null, Random.Range(0.8f, 1.0f));
                    }
                    // 위에 코루틴은 다음에 하란 말 나오면 그때 하기.
                }
            }
        }
    }

    private void Stage2()
    {
        if (Input.GetMouseButtonDown(0))
        {
            timeSpan = Time.time;
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(touchPos, Vector2.zero, 0.0f);
            catHint.GoOriginPos();
            camPos = camera.transform.position;
            if (hit.collider != null)
            {
                interItem = hit.collider.gameObject;

                if (interItem.tag == "SlideObject")
                {
                    interItem.GetComponent<Stage2SlideObject>().touch = true;
                    slide = true;
                    return;
                }
                else if (interItem.tag == "UpSlide")
                {
                    interItem.GetComponent<Upslide>().touch = true;
                    slide = true;
                    return;
                }
            }
        }

        else if (Input.GetMouseButtonUp(0))
        {
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(touchPos, Vector2.zero, 0.0f);

            timeSpan -= Time.time;

            if (Mathf.Abs(camPos.x - camera.transform.position.x) > 0.05f || Mathf.Abs(camPos.y - camera.transform.position.y) > 0.05f)
                return;

            if (timeSpan >= checkTime)
            {
                if (slide)
                {
                    slide = false;
                    return;
                }

                if (hit.collider == null)
                {
                    CreateDust(touchPos);
                }

                else if (hit.collider.tag == "Car")
                {
                    chanceNum.GetComponent<ChanceNumImage>().MinusChance();
                    backgound.Touch();
                    VibratorControllor.TouchVib();
                    coinCatCounter.MinCounter();
                }

                else if (hit.collider.tag == "SlideObject" || hit.collider.tag == "UpSlide")
                    return;

                else if (hit.collider.tag == "Tree" || hit.collider.tag == "Tree2")
                    TouchTreeEffectCreate(hit.collider.tag, touchPos);

                else
                {
                    VibratorControllor.ObjTouchVib();
                    interItem = hit.collider.gameObject;
                    Debug.Log(interItem.name);
                    if (interItem.tag == "bird")
                    {
                        interItem.GetComponent<I_NonColObject>().OnClick();
                    }

                    else if (interItem.tag == "fieldObject")
                    {
                        if (interItem.name == "CatAilen")
                            interItem.transform.DOPause();
                        interItem.GetComponent<SampleButtonScript>().OnClick();
                        StartCoroutine("Jelly", interItem);
                        //if()
                    }

                    else if (interItem.tag == "fieldCat")
                    {
                        CatCheck cat = interItem.GetComponent<CatCheck>();
                        StartCoroutine("Jelly", interItem);
                        if (cat.effectSound != null)
                            AudioManager.Instance.PlaySound(cat.effectSound, interItem.transform.position, null, Random.Range(0.8f, 1.0f));
                        if (interItem.name == "BabyCat")
                        {
                            if (!interItem.GetComponent<BabyCat>().show)
                            {
                                return;
                            }
                        }

                        else if (interItem.name == "BoxCat")
                        {
                            if (!interItem.GetComponent<BoxCatBox>().open)
                            {
                                //interItem.GetComponent<BoxCatBox>().OnClick();
                                return;
                            }
                        }

                        else if (interItem.name == "SwagCat")
                        {
                            if (!interItem.GetComponent<SwagCatMotion>().collect)
                                return;
                        }

                        else if (interItem.name == "WhiteCat")
                        {
                            if (!interItem.GetComponent<CottonCatMotion>().col)
                                return;
                        }

                        else if (interItem.name == "SpottedCat")
                        {
                            if (!interItem.GetComponent<SpottedCatMotion>().col)
                                return;
                        }

                        else if (interItem.name == "BrownCat")
                        {
                            if (!interItem.GetComponent<BrownCatMotion>().col)
                                return;
                        }

                        if (!interItem.GetComponent<CatCheck>().loadCollect)
                        {
                            photo.GetComponent<PhotoShot>().TakePicture(interItem);
                        }

                        List<CatSaveLoad.JsonCat> jsonCatData = jsonCat.LoadCats();
                        jsonCatData[interItem.GetComponent<CatCheck>().no - 1].Collect = true;
                        jsonCat.SaveCats(jsonCatData, jsonCat.key);

                        for (int i = 0; i < catPanelContent.childCount; i++)
                        {
                            SampleCatButton fieldCatScript = catPanelContent.GetChild(i).GetComponent<SampleCatButton>();
                            if (interItem == fieldCatScript.fieldCat)
                            {
                                fieldCatScript.OnClick();
                                interItem.GetComponent<CatCheck>().CollectObject();
                                interItem.GetComponent<CatCheck>().CheckCollect();
                                break;
                            }
                        }
                        return;
                    }

                    else if (interItem.tag == "hiddenCat")
                    {
                        if (interItem.name == "Gomaymmee")
                        {
                            interItem.GetComponent<SquareCatMotion>().OnClick();
                        }

                        if (!interItem.GetComponent<CatCheck>().loadCollect)
                        {
                            photo.GetComponent<PhotoShot>().TakePicture(interItem);
                        }

                        List<CatSaveLoad.JsonCat> jsonCatData = jsonCat.LoadCats();
                        jsonCatData[interItem.GetComponent<CatCheck>().no - 1].Collect = true;
                        jsonCat.SaveCats(jsonCatData, jsonCat.key);

                        for (int i = 0; i < hiddenCatContent.childCount; i++)
                        {
                            SampleCatButton hiddenCatScript = hiddenCatContent.GetChild(0).GetComponent<SampleCatButton>();
                            if (interItem == hiddenCatScript.fieldCat)
                            {
                                hiddenCatScript.OnClick();
                                interItem.GetComponent<CatCheck>().CollectObject();
                                interItem.GetComponent<CatCheck>().CheckCollect();
                                break;
                            }
                        }
                        StartCoroutine("Jelly", interItem);
                    }

                    else if (interItem.tag == "otherCat")
                    {
                        if (interItem.name == "BabyCat")
                        {
                            interItem.GetComponent<BabyCat>().OnClick();
                            interItem.GetComponent<BabyCatUI>().OnClick();
                        }
                        else
                            interItem.GetComponent<I_OtherStageAnimal>().OhterStageAnimalOnClick();
                    }


                    // 인터페이스로 묶음
                    // 아마 인터페이스 명령어도 곧 한번 바꿔야 될거 같음
                    // 이름을 좀더 직관적이게
                    if (interItem.tag == "NotColObject")
                        interItem.GetComponent<I_NonColObject>().OnClick();

                    else if (interItem.name == "CatFO")
                    {
                        interItem.GetComponent<CatFOMotion>().OnClick();
                        StartCoroutine(Jelly(interItem.transform.GetChild(0).gameObject));
                    }
                    else if (interItem.name == "MadCat")
                    {
                        interItem.GetComponent<CoinCatMotion>().OnClick();
                        if (!interItem.GetComponent<MadCatCheck>().collect)
                        {
                            Debug.Log("사진");
                            photo.GetComponent<PhotoShot>().TakePicture(interItem);
                        }
                        else
                            return;
                        List<CatSaveLoad.JsonCat> jsonCatData = jsonCat.LoadCats();
                        jsonCatData[interItem.GetComponent<MadCatCheck>().no - 1].Collect = true;
                        jsonCat.SaveCats(jsonCatData, jsonCat.key);
                    }
                }
            }
        }
    }

    private void Stage3()
    {
        if (Input.GetMouseButtonDown(0))
        {
            timeSpan = Time.time;
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(touchPos, Vector2.zero, 0.0f);
            catHint.GoOriginPos();
            camPos = camera.transform.position;
            if (hit.collider != null)
            {
                interItem = hit.collider.gameObject;

                if (interItem.tag == "SlideObject")
                {
                    interItem.GetComponent<SlidObjectMK2>().touch = true;
                    slide = true;
                    return;
                }
                else if (interItem.tag == "UpSlide")
                {
                    interItem.GetComponent<Upslide>().touch = true;
                    slide = true;
                    return;
                }
            }
        }

        else if (Input.GetMouseButtonUp(0))
        {
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(touchPos, Vector2.zero, 0.0f);

            timeSpan -= Time.time;

            if (Mathf.Abs(camPos.x - camera.transform.position.x) > 0.05f || Mathf.Abs(camPos.y - camera.transform.position.y) > 0.05f)
                return;

            if (timeSpan >= checkTime)
            {
                if (slide)
                {
                    slide = false;
                    return;
                }

                if (hit.collider == null)
                {
                    Debug.Log("배경 선택됨");
                    chanceNum.GetComponent<ChanceNumImage>().MinusChance();
                    backgound.Touch();
                    AudioManager.Instance.PlaySound(touchSound, touchPos, null, Random.Range(0.8f, 1.0f));
                    VibratorControllor.TouchVib();
                    coinCatCounter.MinCounter();
                }

                else
                {
                    VibratorControllor.ObjTouchVib();
                    interItem = hit.collider.gameObject;
                    Debug.Log(interItem.name);

                    if (interItem.tag == "NotColObject")
                        interItem.GetComponent<I_NonColObject>().OnClick();
                }
            }
        }
    }

    private IEnumerator Jelly(GameObject hitObject)
    {
        if (jelly)
        {
            jelly = false;
            Vector3 origin = hitObject.transform.localScale;

            hitObject.transform.DOScale(new Vector3(origin.x - 0.1f, origin.y - 0.1f, origin.z), 0.1f);
            yield return new WaitForSeconds(0.1f);
            hitObject.transform.DOScale(new Vector3(origin.x, origin.y, origin.z), 0.8f).SetEase(Ease.OutElastic);
            yield return new WaitForSeconds(0.3f);
            jelly = true;
        }
        else
        {
            yield return null;
        }
    }

    private void TouchTreeEffectCreate(string tagName, Vector3 touchPos)
    {
        if (tagName == "Tree" || tagName == "Tree2")
        {
            chanceNum.MinusChance();
            VibratorControllor.TouchVib();
            coinCatCounter.MinCounter();
            GameObject treeEffect;

            treeEffect = PoolingManager.Instance.GetPool(treeTouchEffect.GetComponent<TreeTouchEffect>().poolingName);

            if (treeEffect == null)
                treeEffect = Instantiate(treeTouchEffect);
            treeEffect.transform.position = touchPos;
        }
    }

    private void CreateDust(Vector3 touchPos)
    {
        chanceNum.MinusChance();
        backgound.Touch();
        AudioManager.Instance.PlaySound(touchSound, touchPos, null, Random.Range(0.8f, 1.0f));
        VibratorControllor.TouchVib();
        coinCatCounter.MinCounter();

        touchCreateCoin.StartCreateCoin(touchPos);
    }

    private void NoChanceTouch()
    {
        if (Input.GetMouseButtonDown(0))
        {
            timeSpan = Time.time;
            catHint.GoOriginPos();
        }
        else if (Input.GetMouseButtonUp(0))
        {
            timeSpan -= Time.time;
            if (timeSpan >= checkTime)
            {
                if (NoChanceTouchCount == -1)
                    NoChanceTouchCount = 2;

                else if (NoChanceTouchCount == 1)
                {
                    FindObjectOfType<ChanceEmpty>().SendMessage("ShowPopup", true, SendMessageOptions.DontRequireReceiver);
                    NoChanceTouchCount = -1;
                }

                else NoChanceTouchCount--;
            }
        }
    }

    public void SetCamTouch()
    {
        cameraTouch = true;
    }
}
