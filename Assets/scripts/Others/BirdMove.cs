﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdMove : MonoBehaviour
{
    public Sprite feeding;
    public Sprite standing;
    public float distance_x;
    public float distance_y;
    public float move;
    public float time;
    public bool snack = false;

    private Direction dir;
    private Vector3 firstPosition;

    enum Direction
    {
        up, down, right, left, upRight, upLeft, downRight, downLeft, center, feeding
    };

    void Start()
    {
        firstPosition = gameObject.transform.localPosition;
        StartCoroutine("DisplayLog");
        //StartCoroutine("MovingDir");
    }

    private IEnumerator DisplayLog()
    {
        while (true)
        {
            gameObject.transform.GetChild(0).transform.rotation = gameObject.transform.GetChild(1).transform.rotation;
            dir = (Direction)Random.Range((float)Direction.up, (float)Direction.feeding + 1);
            if (!snack)
            {
                for (int i = Random.Range(0, 2); i < 3; i++)
                {
                    if (gameObject.transform.localPosition.x > firstPosition.x - distance_x
                        && gameObject.transform.localPosition.x < firstPosition.x + distance_x
                        && gameObject.transform.localPosition.y > firstPosition.y - distance_y
                        && gameObject.transform.localPosition.y < firstPosition.y + distance_y)
                    {
                        switch (dir)
                        {
                            case Direction.up:
                                iTween.MoveBy(gameObject, iTween.Hash("y", Random.Range(0.0f, 0.5f), "easeType", iTween.EaseType.linear));
                                break;

                            case Direction.down:
                                iTween.MoveBy(gameObject, iTween.Hash("y", Random.Range(0.0f, -0.5f), "easeType", iTween.EaseType.linear));
                                break;

                            case Direction.left:
                                iTween.MoveBy(gameObject, iTween.Hash("x", Random.Range(0.0f, -0.5f), "easeType", iTween.EaseType.linear));
                                if (gameObject.transform.GetChild(0).transform.rotation.y != 0)
                                {
                                    gameObject.transform.GetChild(0).transform.rotation = Quaternion.Euler(0, 0, 0);
                                    gameObject.transform.GetChild(1).transform.rotation = gameObject.transform.GetChild(0).transform.rotation;
                                }
                                break;

                            case Direction.right:
                                iTween.MoveBy(gameObject, iTween.Hash("x", Random.Range(0.0f, 0.5f), "easeType", iTween.EaseType.linear));
                                if (gameObject.transform.GetChild(0).transform.rotation.y != 180)
                                {
                                    gameObject.transform.GetChild(0).transform.rotation = Quaternion.Euler(0, 180, 0);
                                    gameObject.transform.GetChild(1).transform.rotation = gameObject.transform.GetChild(0).transform.rotation;
                                }
                                break;

                            case Direction.upRight:
                                iTween.MoveBy(gameObject, iTween.Hash("x", Random.Range(0.0f, 0.5f), "y", Random.Range(0.0f, 0.5f), "easeType", iTween.EaseType.linear));
                                if (gameObject.transform.GetChild(0).transform.rotation.y != 180)
                                {
                                    gameObject.transform.GetChild(0).transform.rotation = Quaternion.Euler(0, 180, 0);
                                    gameObject.transform.GetChild(1).transform.rotation = gameObject.transform.GetChild(0).transform.rotation;
                                }
                                break;

                            case Direction.upLeft:
                                iTween.MoveBy(gameObject, iTween.Hash("x", Random.Range(0.0f, -0.5f), "y", Random.Range(0.0f, 0.5f), "easeType", iTween.EaseType.linear)); if (gameObject.transform.rotation.y != 0)
                                    if (gameObject.transform.GetChild(0).transform.rotation.y != 0)
                                    {
                                        gameObject.transform.GetChild(0).transform.rotation = Quaternion.Euler(0, 0, 0);
                                        gameObject.transform.GetChild(1).transform.rotation = gameObject.transform.GetChild(0).transform.rotation;
                                    }
                                break;

                            case Direction.downRight:
                                iTween.MoveBy(gameObject, iTween.Hash("x", Random.Range(0.0f, 0.5f), "y", Random.Range(0.0f, -0.5f), "easeType", iTween.EaseType.linear));
                                if (gameObject.transform.GetChild(0).transform.rotation.y != 180)
                                {
                                    gameObject.transform.GetChild(0).transform.rotation = Quaternion.Euler(0, 180, 0);
                                    gameObject.transform.GetChild(1).transform.rotation = gameObject.transform.GetChild(0).transform.rotation;
                                }
                                break;

                            case Direction.downLeft:
                                iTween.MoveBy(gameObject, iTween.Hash("x", Random.Range(0.0f, -0.5f), "y", Random.Range(0.0f, -0.5f), "easeType", iTween.EaseType.linear)); if (gameObject.transform.rotation.y != 0)
                                    if (gameObject.transform.GetChild(0).transform.rotation.y != 0)
                                    {
                                        gameObject.transform.GetChild(0).transform.rotation = Quaternion.Euler(0, 0, 0);
                                        gameObject.transform.GetChild(1).transform.rotation = gameObject.transform.GetChild(0).transform.rotation;
                                    }
                                break;

                            case Direction.center:
                                break;

                            case Direction.feeding:
                                gameObject.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().sprite = feeding;
                                yield return new WaitForSeconds(0.1f);
                                gameObject.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().sprite = standing;
                                yield return new WaitForSeconds(0.1f);
                                break;
                                // 쪼는건 2~3회
                        }
                        yield return new WaitForSeconds(0.3f);
                    }

                    else
                    {
                        Vector3 backPosition = firstPosition;
                        if (gameObject.transform.localPosition.x > firstPosition.x)
                            backPosition.x = -backPosition.x;
                        if (gameObject.transform.localPosition.y > firstPosition.y)
                            backPosition.y = -backPosition.y;

                        //iTween.MoveTo(gameObject, iTween.Hash("x", ((firstPosition.x) + (transform.localPosition.x)) / 2, "y",
                        //    ((firstPosition.y) + (transform.localPosition.y)) / 2, "islocal", true, "movetopath", false, "easeType", iTween.EaseType.linear));
                        iTween.MoveAdd(gameObject, iTween.Hash("amount", new Vector3(firstPosition.x - gameObject.transform.localPosition.x, firstPosition.y - gameObject.transform.localPosition.y, 0.0f)
                            , "speed", 0.5f, "islocal", true, "movetopath", false, "easeType", iTween.EaseType.linear));
                        if ((firstPosition.x + transform.localPosition.x) / 2 < firstPosition.x)
                        {
                            if (gameObject.transform.GetChild(0).transform.rotation.y != 180)
                            {
                                gameObject.transform.GetChild(0).transform.rotation = Quaternion.Euler(0, 180, 0);
                                gameObject.transform.GetChild(1).transform.rotation = gameObject.transform.GetChild(0).transform.rotation;
                            }
                        }

                        else
                        {
                            if (gameObject.transform.GetChild(0).transform.rotation.y != 0)
                            {
                                gameObject.transform.GetChild(0).transform.rotation = Quaternion.Euler(0, 0, 0);
                                gameObject.transform.GetChild(1).transform.rotation = gameObject.transform.GetChild(0).transform.rotation;
                            }
                        }
                        yield return new WaitForSeconds(0.3f);
                    }

                    if (dir == Direction.feeding)
                        yield return new WaitForSeconds(time / 2);
                    else
                        yield return new WaitForSeconds(time);
                }
                yield return null;
            }
                

            else if(snack)
            {
                if (gameObject.transform.localPosition.x > 0.5f && gameObject.transform.localPosition.x < 3.0f && gameObject.transform.localPosition.y > -3.5f && gameObject.transform.localPosition.y < -2.5)
                {
                    yield return new WaitForSeconds(time);
                    for (int j = 0; j < 3; j++)
                    {
                        for (int i = 0; i < 5; i++)
                        {
                            gameObject.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().sprite = feeding;
                            yield return new WaitForSeconds(0.1f);
                            gameObject.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().sprite = standing;
                            yield return new WaitForSeconds(0.1f);
                        }
                    }
                    yield return new WaitForSeconds(time);
                    snack = false;
                }

                else
                {
                    Vector2 snackPosition = new Vector2(Random.Range(0.5f, 3.0f), Random.Range(-2.5f, -3.5f));
                    iTween.MoveAdd(gameObject, iTween.Hash("amount", new Vector3(snackPosition.x - gameObject.transform.localPosition.x, snackPosition.y - gameObject.transform.localPosition.y, 0.0f)
                            , "speed", 0.5f, "islocal", true, "movetopath", false, "easeType", iTween.EaseType.linear));
                    if (gameObject.transform.localPosition.x < snackPosition.x)
                    {
                        if (gameObject.transform.GetChild(0).transform.rotation.y != 180)
                        {
                            gameObject.transform.GetChild(0).transform.rotation = Quaternion.Euler(0, 180, 0);
                            gameObject.transform.GetChild(1).transform.rotation = gameObject.transform.GetChild(0).transform.rotation;
                        }
                    }

                    else
                    {
                        if (gameObject.transform.GetChild(0).transform.rotation.y != 0)
                        {
                            gameObject.transform.GetChild(0).transform.rotation = Quaternion.Euler(0, 0, 0);
                            gameObject.transform.GetChild(1).transform.rotation = gameObject.transform.GetChild(0).transform.rotation;
                        }
                    }
                    yield return new WaitForSeconds(time);
                }
                yield return new WaitForSeconds(time);
            }
            yield return new WaitForSeconds(time);
        }
    }
}
