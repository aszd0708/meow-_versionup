﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainCatSpeedControlloer : MonoBehaviour
{
    public Text speedText;
    public Button upBtn;
    public Button downBtn;

    public Transform parent;

    private float speed;
    private List<CatMove> maps = new List<CatMove>();

    // Start is called before the first frame update
    void Start()
    {
        Invoke("SetList", 0.5f);  
    }
    
    void SetList()
    {
        for(int i = 0; i < parent.childCount; i++)
            maps.Add(parent.GetChild(i).GetComponent<CatMove>());
        speedText.text = maps[0].speed + "";
        speed = maps[0].speed;
    }

    public void DownAction()
    {
        SetMapSpeed(speed - 0.1f);
    }

    public void UpAction()
    {
        SetMapSpeed(speed + 0.1f);
    }

    private void SetMapSpeed(float setSpeed)
    {
        if (setSpeed <= 0)
            return;

        for (int i = 0; i < maps.Count; i++)
            maps[i].speed = setSpeed;
        speedText.text = setSpeed +"";
        speed = setSpeed;
    }
}
