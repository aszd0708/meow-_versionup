﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgoundTouch : MonoBehaviour
{
    public GameObject effect1, effect2;
    public GameObject dust;
    public bool vibrator;
    public bool effect;

    public void Awake()
    {
        if (PlayerPrefs.GetInt("vibrator") == 1)
            vibrator = true;
        else
            vibrator = false;
        dust = effect1;
    }

    public void Touch()
    {
        Vector3 cameraVector = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        GameObject dust_Inst;

        dust_Inst = PoolingManager.Instance.GetPool("Dust");

        if(dust_Inst == null)
            dust_Inst = Instantiate(dust);

        dust_Inst.transform.position = new Vector3(cameraVector.x, cameraVector.y, 10);

        if (vibrator)
            Handheld.Vibrate();

        //Destroy(dust_Inst, 0.3f);
        PoolingManager.Instance.SetPool(dust_Inst, "Dust", 0.3f);
    }

    public void ChangeEffect()
    {
        if(effect)
        {
            effect = false;
            dust = effect1;
        }

        else
        {
            effect = true;
            dust = effect2;
        }
    }
}
