﻿using BitBenderGames;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraStart : MonoBehaviour
{
    public new GameObject camera;
    public float zoomSize;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("Zoom", 0.5f);
    }
    void Zoom()
    {
        camera.GetComponent<MobileTouchCamera>().CamZoomMin = zoomSize;
    }
}
