﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntRandom : MonoBehaviour
{
    static public bool StateRandomInt()
    {
        int random = Random.Range(0, 2);

        switch (random)
        {
            case 0: return false;
            case 1: return true;
            default: return false;
        }
    }

    
    static public bool StateRandomInt(int persent)
    {
        int random = Random.Range(0, 100);

        if (random < persent)
            return true;
        else return false;
    }
}
