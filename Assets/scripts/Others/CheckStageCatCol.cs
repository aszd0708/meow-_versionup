﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CheckStageCatCol : MonoBehaviour
{
    [Header("지금 하고있느 스테이지 넘버")]
    public int stageNumber;
    [Header("이 스테이지에서 몇마리를 찾아야 열리는지 정하는 변수")]
    public int stageMaxCount;

    public CatSaveLoad jsonCat;
    public int[] catNum;
    public RectTransform nextMenu;

    private bool[] collect;

    private StageUnlockData stageUnlockData;

    private bool collectChecker;

    void Awake()
    {
        stageUnlockData = GetComponent<StageUnlockData>();
    }

    // Start is called before the first frame update
    void Start()
    {
        collectChecker = stageUnlockData.GetStageComplete(stageNumber);

        collect = new bool[catNum.Length];
        InputCol();
    }

    public void InputCol()
    {
        List<CatSaveLoad.JsonCat> jsonCatData = jsonCat.LoadCats();
        for (int i = catNum[0], j = 0; i <= catNum[catNum.Length - 1]; i++, j++)
            collect[j] = jsonCatData[i].SendCollect();
    }

    public void CheckCol()
    {
        InputCol();
        
        // 스테이지가 튜토리얼이면 실행 ㄴㄴ
        if (stageNumber == 0) return;
        // 처음 이 스테이지에 진입 했을 때 완료가 됐었다면 실행 ㄴㄴ
        if (collectChecker) return;

        // 스테이지 별 col카운트가 정해진거 보다 높거나 같으면 
        if (stageMaxCount > CollectCount()) return;

        // 스테이지 언락 데이터를 true로 만들어 주고 메뉴를 띄어줌
        stageUnlockData.SetStageComplete(stageNumber, true);

        ShowNextMenu();
    }

    private int CollectCount()
    {
        int count = 0;
        for (int i = 0; i < collect.Length; i++)
            if (collect[i]) count++;

        return count;
    }

    public void ShowNextMenu()
    {
        PopupManager.Instance.OpenPopup(nextMenu);
    }

    public void HideNextMenu()
    {
        PopupManager.Instance.ClosedPopup(nextMenu);
    }
}
