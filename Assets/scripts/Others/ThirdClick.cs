﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdClick : MonoBehaviour, TouchableObj
{
    public List<Sprite> spriteList = new List<Sprite>();

    private SpriteRenderer itemImg;
    private int index = 0;

    void Awake()
    {
        itemImg = GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        int num = Random.Range(0, spriteList.Count);
        int sight = Random.Range(0, 2);
        if (sight == 0)
            transform.rotation = Quaternion.Euler(0, 180, 0);
        else
            transform.rotation = Quaternion.Euler(0, 0, 0);

        itemImg.sprite = spriteList[num];
    }

    public void OnClick()
    {
        itemImg.sprite = spriteList[index++];
        index %= spriteList.Count;
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        itemImg.sprite = spriteList[index++];
        index %= spriteList.Count;
        return false;
    }
}
