﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchBird : MonoBehaviour, I_NonColObject 
{
    private float num = 3.0f;
    public Sprite first;
    public Sprite second;
    public Sprite third;
    float time;
    void Start()
    {
        num = 3;
        time = 0.0f;
    }

    // Update is called once per frame
    public void Touch()
    {
        num--;
        Debug.Log("눌러짐");
        if (num == 2)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = first;
        }

        else if (num == 1)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = second;
        }

        else
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = third;
        }
    }

    void Update()
    {
        if (gameObject.GetComponent<SpriteRenderer>().sprite == third)
        {
            time += Time.deltaTime;
        }

        if (time >= 1)
        {
            Destroy(gameObject);
        }
    }

    public void OnClick()
    {
        num--;
        Debug.Log("눌러짐");
        if (num == 2)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = first;
        }

        else if (num == 1)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = second;
        }

        else
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = third;
        }
    }
}
