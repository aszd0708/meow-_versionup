﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * 2020-04-15
 * 스테이지 아닌 씬들(메인 뽑기방 등등)에서 BGM하고 밝기 조정해주는 스크립트
 */

public class NonStageAudioBrightSetting : MonoBehaviour
{
    public AudioSource BGMaudio;
    public Image bright;
    // Start is called before the first frame update
    void Start()
    {
        float backVol = PlayerPrefs.GetFloat("backVol", 1.0f);
        BGMaudio.volume = backVol;

        float backBri = PlayerPrefs.GetFloat("backBright", 1.0f);
        bright.color = new Vector4(bright.color.r, bright.color.g, bright.color.b, (255f - backBri * 255f) / (255f * 2f));
    }
}
