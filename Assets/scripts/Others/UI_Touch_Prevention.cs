﻿using BitBenderGames;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UI_Touch_Prevention : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            {
                gameObject.GetComponent<TouchInputController>().enabled = false;
            }
            if (Input.touchCount > 0)
            {
                for (int i = 0; i < Input.touchCount; i++)
                {
                    if (EventSystem.current.IsPointerOverGameObject(i))
                        gameObject.GetComponent<TouchInputController>().enabled = false;
                }
            }
        }

        if(Input.GetMouseButtonUp(0))
        {
                gameObject.GetComponent<TouchInputController>().enabled = true;
        }
    }
}
