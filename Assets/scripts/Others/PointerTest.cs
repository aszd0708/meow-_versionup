﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointerTest : MonoBehaviour
{
    public GameObject bag;
    private Vector3 pos;
    // Start is called before the first frame update
    void Start()
    {

        StartCoroutine("Move");
    }

    private IEnumerator Move()
    {
        yield return new WaitForSeconds(1.0f);
        pos = Camera.main.ScreenToWorldPoint(bag.transform.position);
        transform.DOMove(pos, 1.0f);

        yield return null;
    }
}
