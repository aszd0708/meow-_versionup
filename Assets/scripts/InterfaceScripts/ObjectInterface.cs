﻿// 아마 인터페이스 명령어도 곧 한번 바꿔야 될거 같음
// 이름을 좀더 직관적이게

// 수집이 안되지만 상호작용이 있는 오브젝트 인터페이스
interface I_NonColObject
{
    void OnClick();
}

// 수집이 되는 오브젝트 인터페이스
interface I_ColObject
{
    void OnClick(object obj); /* 터치 했을 시 상호작용 */
}

interface I_ItemUsedCat
{
    void UseItem(object obj);
    string CatName { get; set; }
}

// 수집은 안되지만 다른 스테이지에 있는 동물들 터치 모션
// 왜 위랑 같이 안했냐면 내가 처음부터 잘못짜서
// 아마 다시 짜게 된다면 매우 힘든 과정이 될 거 같아서 일단 이렇게 짬
interface I_OtherStageAnimal
{
    void OhterStageAnimalOnClick();
}

/*
 * 2020-03-10
 * 냥집에 있는 고양이들 상호작용 할때 사용할 인터페이스
 */
interface CatHouseCat
{
    void OnClick();
    bool Touch { get; set; }

    bool ShowItem { get; set; }
}

interface AnimalHouseSpecialAnimAnimal
{
    float GetGoBackAnimTime();
}

/*
 *2020-03-30
 * 냥집에 있는 오브젝트 드래그 할 때 사용할 인터페이스
 */
interface CatHouseDragObject
{
    bool Touch { get; set; }
}