﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldCat : MonoBehaviour
{
    private bool canCollect;
    /// <summary>
    /// 이 고양이(동물)을 터ㅣ해서 수집할 수 있는지에 따른 부울값(프로퍼티)
    /// </summary>
    public bool CanCollect
    {
        get
        {
            if (canCollect)
                PlayTouchMotion();

            return canCollect;
        }
        set => canCollect = value;
    }

    /// <summary>
    /// 터치 했을때 나오는 모션
    /// 몇몇 고양이만 갖고있을듯
    /// </summary>
    public virtual void PlayTouchMotion()
    {

    }
}
