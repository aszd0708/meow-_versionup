﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

/*
 * 2020-01-19
 * UI 이동하는 함수들
 * 자세한건 MadCatControllor.cs 확인
 */

public class MadCatUIMove : MonoBehaviour
{
    [Header("터치 관련 컴포넌트들")]
    //private TouchInterationCam touchCom;
    private TouchManager touchManager;
    [Header("UI 관련")]
    public GameObject adUI;
    public GameObject tokenUI;
    private RectTransform adUITransform;
    private RectTransform tokenUITransform;

    void Awake()
    {
        adUITransform = adUI.GetComponent<RectTransform>();
        tokenUITransform = tokenUI.GetComponent<RectTransform>();
        //touchCom = FindObjectOfType<TouchInterationCam>();
        touchManager = FindObjectOfType<TouchManager>();
    }

    public void StartSetting()
    {
        adUITransform.localScale = new Vector2(0, 0);
        tokenUITransform.localScale = new Vector2(0, 0);

        adUI.SetActive(false);
        tokenUI.SetActive(false);
    }

    public void CreateAdUI()
    {
        adUI.SetActive(true);
        TouchOnOffManager.Instance.TouchOnOff(false);
        MenuManager.Instance.SetState(true);
        // ADUI 나오는 애니메이션 추가하기
        adUITransform.DOScale(new Vector2(1, 1), 0.25f).SetEase(Ease.OutElastic);
    }

    public void DestroyAdUI()
    {
        adUITransform.DOScale(new Vector2(0, 0), 0.25f).SetEase(Ease.Linear);
        MenuManager.Instance.SetState(false);
        //touchCom.SetCamTouch();
        touchManager.SetCamTouch();

        TouchOnOffManager.Instance.TouchOnOff(true);
        adUI.SetActive(false);
    }

    public void CreateCoinUI()
    {
        tokenUI.SetActive(true);
        TouchOnOffManager.Instance.TouchOnOff(false);
        MenuManager.Instance.SetState(true);

        tokenUITransform.DOScale(new Vector2(1, 1), 0.25f).SetEase(Ease.OutQuad);
    }

    public void DestroyCoinUI()
    {
        tokenUITransform.DOScale(new Vector2(0, 0), 0.25f).SetEase(Ease.Linear);
        MenuManager.Instance.SetState(false);

        TouchOnOffManager.Instance.TouchOnOff(true);
        tokenUI.SetActive(false);
    }
}
