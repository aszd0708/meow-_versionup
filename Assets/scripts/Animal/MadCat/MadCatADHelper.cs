﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class MadCatADHelper : MonoBehaviour
{
    // 광고 본 후 토큰++;
    private MadCatControllor MCC;

    private const string android_game_id = "2813358";
    private const string ios_game_id = "2813357";

    private const string rewarded_video_id = "rewardedVideo";

    void Awake()
    {
        MCC = GetComponent<MadCatControllor>();
    }

    void Start()
    {
        Initialize();
    }

    private void Initialize()
    {
#if UNITY_ANDROID
        Advertisement.Initialize(android_game_id);
#elif UNITY_IOS
        Advertisement.Initialize(ios_game_id);
#endif
    }

    public void ShowRewardedAd()
    {
        if (Advertisement.IsReady(rewarded_video_id))
        {
            var options = new ShowOptions { resultCallback = HandleShowResult };

            Advertisement.Show(rewarded_video_id, options);
        }
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                {
                    Debug.Log("The ad was successfully shown.");
                    MCC.Reward(MadCatControllor.RewardState.FINISHED);
                    // to do ...
                    // 광고 시청이 완료되었을 때 처리
                    // 원하는 기능 넣기
                    break;
                }
            case ShowResult.Skipped:
                {
                    Debug.Log("The ad was skipped before reaching the end.");
                    MCC.Reward(MadCatControllor.RewardState.SKIPED);

                    // to do ...
                    // 광고가 스킵되었을 때 처리
                    // 원하는 기능 넣기
                    break;
                }
            case ShowResult.Failed:
                {
                    Debug.LogError("The ad failed to be shown.");
                    MCC.Reward(MadCatControllor.RewardState.FAILED);

                    // to do ...
                    // 광고 시청에 실패했을 때 처리
                    // 시청에 실패하면 빼액 거림
                    break;
                }
        }
    }
}
