﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * 2020-01-19
 * 터치 했을 시 토큰 늘어나는 컴포넌트
 * 자세한건 MadCatControllor.cs 확인
 */

public class MadCatTokenManager : MonoBehaviour
{
    [Header("CoinUI에 사용되는 컴포넌트")]
    public TokenCounter TC;
    public Text coinCountText;

    public void SetCount()
    {
        coinCountText.text = "X " + TC.TokenCount;
    }

    public void UpCount()
    {
        TC.PlusToken(1);
    }

    public void ShowAD()
    {

    }
}
