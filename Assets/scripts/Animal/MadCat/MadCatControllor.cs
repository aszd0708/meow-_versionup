﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using BitBenderGames;

/*
 * 2020-01-19
 * 광고양이 터치 시 광고양이에 관한 명령어들 실행
 * 예, 아니요를 눌렀을 때 실해되는 함수
 * 예 -> 광고를 보여주고, 동전을 획득하는 UI 표시 및 동전 카운트++ -> 이 동전에 관한 데이터는 여기서 변경
 * 
 * 아니요 -> 그냥 없어짐
 * 
 * 터치를 다 막아놓자! -> 싱글톤으로 만들어서 자유롭게 막아둠
 *  
 * 이 컴포넌트와 연결되는 컴포넌트
 * - 광고에 관한 컴포넌트
 * - coin 저장에 관한 컴포넌트
 * - UI 2개(광고, 획득)
 * 
 * 유한상태기계 비슷하게 만듦
 * MadCatUIMove.cs 와 MadCatTokenManager.cs 연결
 * MadCatUIMove.cs - UI 이동 담당
 * MadCatTokenManager.cs - 토큰 계산 담당
 */

public class MadCatControllor : MonoBehaviour
{
    [Header("UI이동, Token컨트롤")]
    private MadCatUIMove UIMove;
    private MadCatTokenManager tokenManager;

    //[Header("AdUI에 사용되는 컴포넌트")]
    //private MadCatADHelper AD;

    public enum RewardState
    {
        FINISHED, SKIPED, FAILED
    }

    void Awake()
    {
        UIMove = GetComponent<MadCatUIMove>();
        tokenManager = GetComponent<MadCatTokenManager>();
        //AD = GetComponent<MadCatADHelper>();
    }

    void Start()
    {
        UIMove.StartSetting();
    }

    public void MadCatTouch()
    {
        // 광고양이 터치
        UIMove.CreateAdUI();
        // 토큰 매니저 호출
    }

    // 이건 화면 터치 했을 때 코인 주는 함수
    // 광고 안보여주고 토큰을 바로 줌
    public void NonAdGiveCoin()
    {
        UIMove.CreateCoinUI();
        tokenManager.UpCount();
        tokenManager.SetCount();
    }

    public void AdUI(bool state)
    {
        if (state)
        {
            // 광고 보여주고
            UIMove.DestroyAdUI();
            tokenManager.UpCount();
            tokenManager.SetCount();
            UIMove.CreateCoinUI();
            //AD.ShowRewardedAd();
            // 토큰 한개++해주는 함수
            // 광고 실행
        }

        else
        {
            // 끄기
            UIMove.DestroyAdUI();
        }
    }

    public void Reward(RewardState REWARDSTATE)
    {
        switch (REWARDSTATE)
        {
            case RewardState.FAILED: UIMove.CreateAdUI(); break;
            case RewardState.FINISHED:
                tokenManager.UpCount();
                tokenManager.SetCount();
                UIMove.CreateCoinUI();
                break;
            case RewardState.SKIPED:
                AdUI(false);
                break;
            default: Debug.Log("에러에러"); break;
        }
    }

    public void CoinUIDestroy()
    {
        UIMove.DestroyCoinUI();
    }
}
