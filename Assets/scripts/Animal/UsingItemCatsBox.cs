﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UsingItemCatsBox : MonoBehaviour
{
    public UsingItemCat[] cats;

    public UsingItemCat GetUsingItemCats(string name)
    {
        UsingItemCat wantCat = null;
        for (int i = 0; i < cats.Length; i++)
        {
            if (string.Equals(name, cats[i].cat_Name))
            {
                wantCat = cats[i];
                break;
            }
        }
        return wantCat;
    }
}
