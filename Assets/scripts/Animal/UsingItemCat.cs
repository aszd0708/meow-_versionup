﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UsingItemCat : FieldCat
{
    public string cat_Name;

    /// <summary>
    /// 아이템 드래그해서 놓을때 이 함수 사용
    ///  item 매개변수를 안 써도 괜찮음
    /// </summary>
    /// <param name="item"></param>
    public abstract void UseItem(GameObject item);
}
