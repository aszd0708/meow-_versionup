﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;

public class StageGatchaTouchableDoll : StageAnimalObjectBase
{
    private bool bCanTouch;

    public bool BCanTouch { get => bCanTouch; set => bCanTouch = value; }

    private UnityEvent motionEvent;
    public UnityEvent MotionEvent { get => motionEvent; set => motionEvent = value; }

    [SerializeField]
    private float createTime = 0.5f;

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if (BCanTouch == false) { return false; }

        SetCollect();
        return false;
    }

    public void SetDollPosition(Vector3 createPosition)
    {
        transform.position = createPosition;
        transform.localScale = Vector3.zero;

        transform.DOScale(Vector3.one, createTime).SetEase(Ease.Linear);
        BCanTouch = true;

        PlayDollAnimation();
    }

    public virtual void PlayDollAnimation()
    {
        return;
    }
}
