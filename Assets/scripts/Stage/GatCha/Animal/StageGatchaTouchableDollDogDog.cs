﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageGatchaTouchableDollDogDog : StageGatchaTouchableDoll
{
    [SerializeField]
    private SpriteRenderer bodyRender;
    [SerializeField]
    private SpriteRenderer eyeRender;

    [SerializeField]
    private Sprite[] bodySprites;
    [SerializeField]
    private float bodyChangeTime;
    [SerializeField]
    private Sprite[] eyeSprites;
    [SerializeField]
    private float eyeChangeTime;
    [SerializeField]
    private Sprite suprieseEyeSprite;

    private Coroutine eyemotionCor;
    private Coroutine bodyMotionCor;

    protected override void OnEnable()
    {
        base.OnEnable();

        PlayDollAnimation();
    }

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if (BCanTouch == true)
        {
            SetCollect();

            if(eyemotionCor != null)
            {
                StopCoroutine(eyemotionCor);
            }

            eyeRender.sprite = suprieseEyeSprite;
            AudioManager.Instance.PlaySound("dog_3", transform.position);
        }
        return false;
    }

    private IEnumerator _RenderMotion(SpriteRenderer renderer, Sprite[] sprites, float time)
    {
        WaitForSeconds wait = new WaitForSeconds(time);
        int index = 0;
        while (true)
        {
            renderer.sprite = sprites[index++];
            index %= sprites.Length;
            yield return wait;
        }
    }
    public override void PlayDollAnimation()
    {
        base.PlayDollAnimation();

        BCanTouch = true;
        eyemotionCor = StartCoroutine(_RenderMotion(eyeRender, eyeSprites, eyeChangeTime));
        bodyMotionCor = StartCoroutine(_RenderMotion(bodyRender, bodySprites, bodyChangeTime));
    }
}
