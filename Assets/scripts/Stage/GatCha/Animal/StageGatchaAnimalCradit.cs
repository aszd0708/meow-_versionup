﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageGatchaAnimalCradit : StageGatchaAnimalBase
{
    private bool bCanTouch = false;

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if(bCanTouch == true)
        {
            SetCollect();
            AudioManager.Instance.PlaySound("cheer_1", transform.position);
            ResetRigid();
        }
        return false;
    }

    public override void PlayGatchaMotion()
    {
        base.PlayGatchaMotion();
        bCanTouch = true;
        return;
    }
}
