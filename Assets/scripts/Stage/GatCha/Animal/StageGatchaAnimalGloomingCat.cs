﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageGatchaAnimalGloomingCat : StageGatchaAnimalBase
{
    [Header("고양이 말고 개 들어있어도 놀라지 마세요 개에요")]

    [SerializeField]
    private SpriteRenderer bodyRender;
    [SerializeField]
    private Sprite[] bodySprites;
    [SerializeField]
    private float bodyChangeTime;

    [SerializeField]
    private SpriteRenderer eyeRender;
    [SerializeField]
    private Sprite[] eyeSprites;
    [SerializeField]
    private float eyeChangeTime;

    private bool bCanCollect = false;

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if(bCanCollect == true)
        {
            SetCollect();
            AudioManager.Instance.PlaySound("dog_2", transform.position);

            ResetRigid();
        }
        return false;
    }

    public override void PlayGatchaMotion()
    {
        base.PlayGatchaMotion();
        //bCanCollect = true;
        //StartCoroutine(_RenderMotion(bodyRender, bodySprites, bodyChangeTime));
        //StartCoroutine(_RenderMotion(eyeRender, eyeSprites, eyeChangeTime));
    }

    private IEnumerator _RenderMotion(SpriteRenderer renderer, Sprite[] sprites, float time)
    {
        WaitForSeconds wait = new WaitForSeconds(time);
        int index = 0;
        while (true)
        {
            renderer.sprite = sprites[index++];
            index %= sprites.Length;
            yield return wait;
        }
    }
}
