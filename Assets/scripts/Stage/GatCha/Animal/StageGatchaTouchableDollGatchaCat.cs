﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class StageGatchaTouchableDollGatchaCat : StageGatchaTouchableDoll
{
    [SerializeField]
    private SpriteRenderer bodyRender;
    [SerializeField]
    private SpriteRenderer tailRender;
    [SerializeField]
    private SpriteRenderer eyeRender;

    [SerializeField]
    private Sprite standBodySprite;
    [SerializeField]
    private Sprite[] longBodySprites;
    [SerializeField]
    private float bodyMotionTime;
    [SerializeField]
    private float shakeMotionTime;
    [SerializeField]
    private float shakePower;
    [SerializeField]
    private int shaderVibrato;
    [SerializeField]
    private Sprite[] eyeSprites;
    [SerializeField]
    private float eyeMotionTime;

    protected override void OnEnable()
    {
        base.OnEnable();

        PlayDollAnimation();
    }

    public override void PlayDollAnimation()
    {
        BCanTouch = true;
        StartCoroutine(_GatchaMotion());
        StartCoroutine(_EyeMotion());
    }

    private IEnumerator _GatchaMotion()
    {
        WaitForSeconds wait = new WaitForSeconds(bodyMotionTime);
        if(tailRender != null)
        {
            tailRender.enabled = false;
        }
        int index = 0;

        for (int i = 0; i < longBodySprites.Length; i++, index++)
        {
            bodyRender.sprite = longBodySprites[index];
            yield return wait;
        }
        transform.DOShakePosition(shakeMotionTime, shakePower, shaderVibrato);
        yield return new WaitForSeconds(shakeMotionTime);

        index = longBodySprites.Length - 1;

        for (int i = 0; i < longBodySprites.Length; i++, index--)
        {
            bodyRender.sprite = longBodySprites[index];
            yield return wait;
        }
        bodyRender.sprite = standBodySprite;
        tailRender.enabled = true;
        yield break;
    }

    private IEnumerator _EyeMotion()
    {
        int index = 0;
        WaitForSeconds wait = new WaitForSeconds(eyeMotionTime);
        while (true)
        {
            eyeRender.sprite = eyeSprites[index++];
            index %= eyeSprites.Length;
            yield return wait;
        }
    }
}
