﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StageGatchaAnimalBase : StageAnimalObjectBase
{
    [SerializeField]
    private Rigidbody2D[] rigid;

    [SerializeField]
    protected GameObject touchableDoll;

    [SerializeField]
    protected Transform createTransform;

    /// <summary>
    /// 뽑혔을때 모션
    /// </summary>
    public virtual void PlayGatchaMotion()
    {
        CreateTouchableDoll(createTransform.position);
    }

    protected void CreateTouchableDoll(Vector3 createPosition)
    {
        if (touchableDoll.activeSelf == true) { return; }

        touchableDoll.SetActive(true);

        StageGatchaTouchableDoll touchableScript = touchableDoll.GetComponent<StageGatchaTouchableDoll>();

        if(touchableScript != null)
        {
            touchableScript.SetDollPosition(createPosition);
        }

        gameObject.SetActive(false);
    }

    protected void ResetRigid()
    {
        for (int i = 0; i < rigid.Length; i++)
        {
            rigid[i].isKinematic = true;
        }
    }
}
