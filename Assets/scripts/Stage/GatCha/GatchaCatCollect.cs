﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GatchaCatCollect : MonoBehaviour
{
    private int catNum;
    public CatSaveLoad saveData;
    public PhotoShot photo;
    
    
    private void Awake()
    {
        catNum = GetComponent<CatCheck>().no;
    }

    public void OnClick()
    {
        // 제이슨 저장해주는 명령어
        List<CatSaveLoad.JsonCat> jsonCatData = saveData.LoadCats();
        jsonCatData[catNum - 1].Collect = true;
        saveData.SaveCats(jsonCatData, saveData.key);

        // 하고 여기에 사진 찍는 이펙트 추가하기
        if (GetComponent<CatCheck>().loadCollect)
        {
            Debug.Log("사진");
            photo.TakePicture(gameObject);
        }
    }
}
