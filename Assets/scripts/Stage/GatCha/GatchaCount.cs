﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class GatchaCount : MonoBehaviour
{
    // 시작 고양이는 1이 아니고 0
    [Serializable]
    public class GatchaCounter
    {
        public int catCount;

        public GatchaCounter(int count)
        {
            catCount = count;
        }
    }

    public List<int> basicCount = new List<int>();
    //[HideInInspector]
    public List<GatchaCounter> gatchaNum = new List<GatchaCounter>();

    private void Awake()
    {
        if (!System.IO.File.Exists(Application.persistentDataPath + "/" + "Counter" + "Info.dat"))
        {
            for (int i = 0; i < basicCount.Count; i++)
                gatchaNum.Add(new GatchaCounter(basicCount[i]));
            Save();
        }

        else
        {
            Load();
        }
    }

    private void Save()
    {
        Debug.Log("세이브!");
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/" + "Counter" + "Info.dat");
        List<GatchaCounter> saveInfo = new List<GatchaCounter>();
        saveInfo = gatchaNum;
        for (int i = 0; i < gatchaNum.Count; i++)
            Debug.Log(saveInfo[i].catCount);
        //    saveInfo.Add(gatchaNum[i]);
        bf.Serialize(file, saveInfo);
        file.Close();
    }

    private void Load()
    {
        Debug.Log("로드!");
        if (!System.IO.File.Exists(Application.persistentDataPath + "/" + "Counter" + "Info.dat"))
        {
            return;
        }

        else
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.OpenRead(Application.persistentDataPath + "/" + "Counter" + "Info.dat");
            if (file != null && file.Length > 0)
            {
                Debug.Log("담는중");
                List<GatchaCounter> saveInfo = (List<GatchaCounter>)bf.Deserialize(file);
                gatchaNum = saveInfo;
                file.Close();
            }
        }
    }

    public void DelSaveFile()
    {
        System.IO.File.Delete(Application.persistentDataPath + "/" + "Counter" + "Info.dat");
        Debug.Log("파일 삭제");
    }

    public void MinCount()
    {
        for (int i = 0; i < gatchaNum.Count; i++)
        {
            if (gatchaNum[i].catCount < 0)
                continue;
            else
                gatchaNum[i].catCount--;
        }
        Save();
    }

    public int Checking()
    {
        for (int i = 0; i < gatchaNum.Count; i++)
        {
            if (gatchaNum[i].catCount == 0)
            {
                gatchaNum[i].catCount = -1;
                return i;
            }
        }
        return -1;
    }

    public void PickDoll(int catNum)
    {
        for (int i = catNum; i < gatchaNum.Count; i++)
            gatchaNum[i].catCount -= gatchaNum[catNum].catCount;
        Save();
    }
}