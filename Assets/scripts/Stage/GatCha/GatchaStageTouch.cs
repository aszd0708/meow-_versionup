﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GatchaStageTouch : MonoBehaviour
{
    public float checkTime = -0.5f;
    private float timeSpan = 0.5f;

    private GameObject interItem;
    private bool touch;

    [SerializeField]
    private LayerMask touchableLayer;
    // Start is called before the first frame update
    void Start()
    {
        touch = true;
        timeSpan = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            return;

        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                if (EventSystem.current.IsPointerOverGameObject(i))
                    return;
            }
        }

        if (!touch)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            timeSpan = Time.time;
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(touchPos, Vector2.zero, 0.0f);
            if (hit.collider != null)
            {
                //interItem = hit.collider.gameObject;
            }

        }

        else if (Input.GetMouseButtonUp(0))
        {
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(touchPos, Vector2.zero, 0.0f, touchableLayer);

            timeSpan -= Time.time;

            if (timeSpan >= checkTime)
            {
                if(hit.transform != null)
                {
                    TouchableObj touchObj = hit.transform.GetComponent<TouchableObj>();
                    if (touchObj != null)
                    {
                        touchObj.DoTouch(touchPos);
                    }
                }
            }
        }
    }
}
