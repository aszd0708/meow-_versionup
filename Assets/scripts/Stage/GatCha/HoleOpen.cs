﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoleOpen : MonoBehaviour
{
    public SpriteRenderer cap;
    public Sprite openSprite, closeSprite;
    private bool open;

    private void Start()
    {
        open = false;
    }

    public void OnCilck()
    {
        if(open)
        {
            // 닫혀야댐
            cap.sortingLayerName = "3";
            cap.sprite = closeSprite;
            cap.transform.position = new Vector3(cap.transform.position.x, cap.transform.position.y, -5);
        }

        else
        {
            // 열려야 댐
            cap.sortingLayerName = "1";
            cap.sprite = openSprite;
            cap.transform.position = new Vector3(cap.transform.position.x, cap.transform.position.y, 1);
        }
        open = !open;
    }
}
