﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TokenTextSetPos : MonoBehaviour
{
    public Transform insertPos;
    public Text[] texts = new Text[3];
    public Image coin;
    public Text nothingT;

    private void Start()
    {
        SetPos();
    }

    public void SetPos()
    {
        Vector2 setPos = Camera.main.WorldToScreenPoint(new Vector3(insertPos.position.x, insertPos.position.y+1f, 0));
        transform.position = setPos;
    }

    public void ApearBox()
    {
        int count = SaveDataManager.Instance.Data.Player.CoinCount;
        if(count <= 0)
        {
            BoolText(false);
        }
        else
        {
            BoolText(true);
            texts[0].text = count.ToString();
        }
        transform.DOScale(new Vector3(1, 1, 0), 0.2f).SetEase(Ease.OutElastic);
    }

    public void DisApearBox()
    {
        transform.DOScale(new Vector3(0, 0, 0), 0.2f);
    }

    public void BoolText(bool appear)
    {
        for(int i = 0; i < texts.Length; i++)
            texts[i].enabled = appear;
        coin.enabled = appear;
        nothingT.enabled = !appear;
    }
}
