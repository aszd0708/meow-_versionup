﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class CrainMotion : MonoBehaviour
{
    public enum State
    {
        drop, nothing, pickup
    }

    public Transform startUpPos;
    public Transform startDownPos;
    public Transform leftPos;
    public Transform rightPos;
    public Transform originPos;
    
    public Transform tongL;
    public Transform tongR;
    public GameObject pointL, pointR;
    public GameObject tongs;
    public bool left, right;
    public float speedSet;
    public CoinAnimation coin;
    public Transform dolls;

    public bool play;

    public GatchaMachinePlay machine;
    
    public TokenTextSetPos tokenBox;

    private int catPointer = 0;
    private float speed;
    private Vector2 pos;
    private bool prevVec; // T - Right F - Left
    private float rotSpeed;
    private float downSpeed;
    private Vector3 startUpPosV;
    private Vector3 startDownPosV;
    private Vector3 leftPosV;
    private Vector3 rightPosV;
    private Vector3 originPosV;

    private float nowRotationSpeed;


    private GatchaMachinePlay manager;

    public GatchaAnimalCreater animalCreater;
    public Transform activePose;
    // Start is called before the first frame update

    //[SerializeField]
    //private GameObject testDoll;

    void Awake()
    {
        manager = GetComponent<GatchaMachinePlay>();
    }

    void Start()
    {
        startUpPosV = startUpPos.localPosition;
        startDownPosV = startDownPos.localPosition;
        leftPosV = leftPos.localPosition;
        rightPosV = rightPos.localPosition;
        originPosV = originPos.localPosition;

        transform.localPosition = startDownPosV;

        //StartCoroutine("Motion");
        play = false;
        //pos = tongsR.position;

        //StartCoroutine(CatPickup(testDoll));
    }

    private bool stop = true;
    // Update is called once per frame
    void Update()
    {
        if (play)
        {
            if (left)
            {
                if(transform.localPosition.x <= leftPosV.x)
                {
                    nowRotationSpeed -= Time.deltaTime * rotSpeed * 0.5f;
                    if (nowRotationSpeed <= 0)
                    {
                        nowRotationSpeed = 0;
                    }
                    tongs.transform.rotation = Quaternion.Euler(0, 0, transform.rotation.z + nowRotationSpeed);
                }
                else
                {
                    nowRotationSpeed = Time.deltaTime * rotSpeed * 0.5f;
                    tongs.transform.rotation = Quaternion.Euler(0, 0, transform.rotation.z + Time.deltaTime * rotSpeed);
                }
                transform.position = new Vector3(transform.position.x - Time.deltaTime * speed, transform.position.y, 0);
                rotSpeed = speed * 200;
                stop = true;
            }
            else if (right)
            {
                if (transform.localPosition.x >= rightPosV.x)
                {
                    nowRotationSpeed -= Time.deltaTime * rotSpeed;
                    if (nowRotationSpeed <= 0)
                    {
                        nowRotationSpeed = 0;
                    }
                    tongs.transform.rotation = Quaternion.Euler(0, 0, transform.rotation.z - nowRotationSpeed);
                }

                else
                {
                    nowRotationSpeed = Time.deltaTime * rotSpeed;
                    tongs.transform.rotation = Quaternion.Euler(0, 0, transform.rotation.z - Time.deltaTime * rotSpeed);
                }
                transform.position = new Vector3(transform.position.x + Time.deltaTime * speed, transform.position.y, 0);
                rotSpeed = speed * 200;
            }
            else if (right && left)
                return;

            transform.localPosition = new Vector3(Mathf.Clamp(transform.localPosition.x, leftPosV.x, rightPosV.x), transform.localPosition.y, 0);
            SpeedSetting();
        }
    }

    private void SpeedSetting()
    {
        if (left || right)
            speed += Time.deltaTime * speedSet;
        else
            speed -= Time.deltaTime * speedSet * 5;

        speed = Mathf.Clamp(speed, 0, speedSet);
    }

    public void StartL()
    {
        left = true;
        tongs.transform.DOPause();
    }

    public void StopL()
    {
        left = false;
        prevVec = false;
        downSpeed = -rotSpeed / 3;
        StopRot();
    }

    public void StartR()
    {
        right = true;
        tongs.transform.DOPause();
    }

    public void StopR()
    {
        right = false;
        prevVec = true;
        downSpeed = -rotSpeed / 3;
        StopRot();
    }

    private void StopRot()
    {
        tongs.transform.DORotate(new Vector3(0, 0, 0), 1.5f).SetEase(Ease.OutElastic);
        rotSpeed = 0;
    }

    public void MotionStarter()
    {
        if (play)
        {
            StartCoroutine(Motion());
        }
    }

    public void StartMotionStarter()
    {
        manager.SendMessage("CrainMotionState", true, SendMessageOptions.DontRequireReceiver);
        StartCoroutine(StartMotion());
    }
    
    private IEnumerator StartMotion()
    {
        float time = coin.MotionStarter();
        yield return new WaitForSeconds(time);
        transform.DOLocalMoveY(startUpPosV.y, 1.0f);
        yield return new WaitForSeconds(1.0f);
        transform.DOLocalMoveX(rightPosV.x, 0.5f);
        yield return new WaitForSeconds(0.5f);
        AudioManager.Instance.PlaySound("coin_2", transform.position);
        play = true;
        yield return null;
    }

    private IEnumerator Motion()
    {
        tongs.transform.DORotate(new Vector3(0, 0, 0), 0.1f).SetEase(Ease.OutElastic);
        play = false;
        tongL.DORotate(new Vector3(0, 0, 40), 1.0f);
        tongR.DORotate(new Vector3(0, 180, 40), 1.0f);
        yield return new WaitForSeconds(1.0f);
        transform.DOMoveY(-1.5f, 1.5f);

        AudioManager.Instance.PlaySound("coin_3", transform.position);
        yield return new WaitForSeconds(1.5f);
        tongL.DORotate(new Vector3(0, 0, 0), 0f);
        tongR.DORotate(new Vector3(0, 180, 0), 0f);
        for (int i = 0; i < 3; i++)
        {
            pointL.GetComponent<CircleCollider2D>().enabled = true;
            pointL.GetComponent<PointEffector2D>().enabled = true;
            pointR.GetComponent<CircleCollider2D>().enabled = true;
            pointR.GetComponent<PointEffector2D>().enabled = true;
            yield return new WaitForSeconds(0.1f);
            pointL.GetComponent<CircleCollider2D>().enabled = false;
            pointL.GetComponent<PointEffector2D>().enabled = false;
            pointR.GetComponent<CircleCollider2D>().enabled = false;
            pointR.GetComponent<PointEffector2D>().enabled = false;
            yield return new WaitForSeconds(0.1f);
        }
        yield return new WaitForSeconds(1.0f);
        // 만드는 스크립트
        GameObject doll;
        bool isAnimal = animalCreater.GetGatchaObj(activePose, out doll);
        Debug.Log("doll : " + doll.name);
        SaveDataManager.Instance.EditCoinCount(SaveDataManager.Instance.Data.Player.CoinCount - 1);
        State state = State.drop;
        if(isAnimal)
        {
            state = State.pickup;
        }
        else
        {
            int randomValue = Random.Range(0, 2);
            Debug.Log("randomValue : " + randomValue);
            state = (State)randomValue;
        }
        Debug.Log("STATE : " + state);
        switch (state)
        {
            case State.drop:
                //doll = dollGatcha.CreateDoll(true);
                StartCoroutine(DropCat(doll));
                Debug.Log("놓침");
                break;
            case State.nothing:
                //doll = dollGatcha.CreateDoll(true);
                StartCoroutine(NothiongPickup(doll));
                Debug.Log("아무것도 안걸림");
                break;
            case State.pickup:
                //doll = dollGatcha.CreateDoll(false);
                StartCoroutine(CatPickup(doll));
                Debug.Log("당첨됨");
                break;
        }
        machine.motion = false;
        yield break;
    }

    private IEnumerator FinalMotion(GameObject doll)
    {
        Debug.Log("FinalMotion(doll)");
        tongL.DORotate(new Vector3(0, 0, 0), 1.0f);
        tongR.DORotate(new Vector3(0, 180, 0), 1.0f);
        doll.GetComponent<CircleCollider2D>().enabled = false;
        yield return new WaitForSeconds(0.5f);
        doll.GetComponent<CircleCollider2D>().enabled = true;
        yield return new WaitForSeconds(0.5f);
        tongL.DORotate(new Vector3(0, 0, 40), 1.0f);
        tongR.DORotate(new Vector3(0, 180, 40), 1.0f);
        yield return new WaitForSeconds(1.5f);
        tongL.DORotate(new Vector3(0, 0, 0), 1.0f);
        tongR.DORotate(new Vector3(0, 180, 0), 1.0f);
        yield return new WaitForSeconds(1.0f);
        transform.DOLocalMoveY(startDownPosV.y, 0.75f);
        tokenBox.ApearBox();
        play = false;
        manager.SendMessage("CrainMotionState", false, SendMessageOptions.DontRequireReceiver);
        doll.transform.SetParent(null);
        yield break;
    }

    private IEnumerator FinalMotion()
    {
        Debug.Log("FinalMotion()");
        tongL.DORotate(new Vector3(0, 0, 0), 1.0f);
        tongR.DORotate(new Vector3(0, 180, 0), 1.0f);
        yield return new WaitForSeconds(1.0f);
        tongL.DORotate(new Vector3(0, 0, 40), 1.0f);
        tongR.DORotate(new Vector3(0, 180, 40), 1.0f);
        yield return new WaitForSeconds(1.5f);
        tongL.DORotate(new Vector3(0, 0, 0), 1.0f);
        tongR.DORotate(new Vector3(0, 180, 0), 1.0f);
        yield return new WaitForSeconds(1.0f);
        transform.DOLocalMoveY(startDownPosV.y, 0.75f);
        tokenBox.ApearBox();
        play = false;
        manager.SendMessage("CrainMotionState", false, SendMessageOptions.DontRequireReceiver);
        yield break;
    }

    private IEnumerator FinalMotionWithDoll(GameObject doll)
    {
        Debug.Log("FinalMotion()");
        tongL.DORotate(new Vector3(0, 0, 0), 1.0f);
        tongR.DORotate(new Vector3(0, 180, 0), 1.0f);
        yield return new WaitForSeconds(1.0f);
        tongL.DORotate(new Vector3(0, 0, 40), 1.0f);
        tongR.DORotate(new Vector3(0, 180, 40), 1.0f);
        yield return new WaitForSeconds(1.5f);
        tongL.DORotate(new Vector3(0, 0, 0), 1.0f);
        tongR.DORotate(new Vector3(0, 180, 0), 1.0f);
        yield return new WaitForSeconds(1.0f);
        transform.DOLocalMoveY(startDownPosV.y, 0.75f);
        tokenBox.ApearBox();
        play = false;
        manager.SendMessage("CrainMotionState", false, SendMessageOptions.DontRequireReceiver);


        if (doll != null)
        {
            if (doll.GetComponent<StageGatchaAnimalBase>() != null)
            { doll.GetComponent<StageGatchaAnimalBase>().PlayGatchaMotion(); }
        }
        yield break;
    }


    private IEnumerator DropCat(GameObject doll)
    {
        if(doll == null)
        {
            Debug.LogError("없다 인형");
        }
        doll.layer = 8;
        doll.transform.SetParent(dolls);
        doll.transform.position = activePose.position;
        transform.DOLocalMoveY(startUpPosV.y, 1.5f);
        AudioManager.Instance.PlaySound("coin_3", transform.position);
        yield return new WaitForSeconds(2.5f);
        transform.DOLocalMoveX(startUpPosV.x, 2.5f);
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(FinalMotion(doll));
        AudioManager.Instance.PlaySound("sigh", transform.position);
        yield break;
    }

    private IEnumerator NothiongPickup(GameObject doll)
    {
        Destroy(doll);
        transform.DOLocalMoveY(startUpPosV.y, 1.5f);
        AudioManager.Instance.PlaySound("coin_3", transform.position);
        yield return new WaitForSeconds(2.5f);
        transform.DOLocalMoveX(startUpPosV.x, 2.5f);
        yield return new WaitForSeconds(2.5f);
        StartCoroutine(FinalMotion());
        AudioManager.Instance.PlaySound("sigh", transform.position);
        yield break;
    }

    private IEnumerator CatPickup(GameObject doll)
    {
        doll.transform.SetParent(dolls);
        doll.transform.position = activePose.position;
        doll.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        transform.DOLocalMoveY(startUpPosV.y, 1.5f);
        AudioManager.Instance.PlaySound("coin_3", transform.position);
        yield return new WaitForSeconds(2.5f);
        transform.DOLocalMoveX(startUpPosV.x, 2.5f);
        yield return new WaitForSeconds(2.5f);
        StartCoroutine(FinalMotionWithDoll(doll));
        yield break;
    }
}
