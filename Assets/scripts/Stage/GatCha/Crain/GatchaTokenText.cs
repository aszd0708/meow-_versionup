﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GatchaTokenText : MonoBehaviour
{ 
    public Text tokenText;
    public Text tokenCountText;
    public TokenTextSetPos tokenBox;

    private Coroutine textMotion;

    // 토큰이 0개 이하인지 알려주는 부울과 프리퍼런스
    private bool haveToken;

    public bool HaveToken
    {
        set
        {
            int count = SaveDataManager.Instance.Data.Player.CoinCount;
            if (count <= 0) haveToken = false;
            else haveToken = true;
        }
        get
        {
            int count = SaveDataManager.Instance.Data.Player.CoinCount;
            if (count <= 0) haveToken = false;
            else haveToken = true;
            return haveToken;
        }
    }

    private void Start()
    {
        SetText();
    }

    public void SetText()
    {
        int count = SaveDataManager.Instance.Data.Player.CoinCount;
        Debug.Log("토큰 있음" + HaveToken);
        if (!HaveToken)
            tokenBox.BoolText(false);
        else
        {
            tokenBox.BoolText(true);
            tokenCountText.text = "x" + count;
        }
    }

    public void StartMotionStarter()
    {
        if (!HaveToken)
        {
            if (textMotion != null)
                StopCoroutine(textMotion);
            textMotion = StartCoroutine(TextMotion());
            // 대충 안된다는 문구
            // 아니면 되긴 되는데 광고 보면 토큰 주는 식?? 일단은 그건 나중에 하는걸로
        }

        else
        {
            tokenBox.DisApearBox();
        }
    }

    private IEnumerator TextMotion()
    {
        //tokenText.enabled = true;
        yield return new WaitForSeconds(3.0f);
        tokenText.enabled = false;
    }
    /*
     * 한번에 통합하여 따로따로 실행 시키게 만듦
     */

    public void MinCoin()
    {
        //token.MinToken();
        //int count = SaveDataManager.Instance.Data.Player.CoinCount;
        //SaveDataManager.Instance.EditCoinCount(count - 1);
    }
}
