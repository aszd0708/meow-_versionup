﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GatchaMachinePlay : MonoBehaviour
{
    public GameObject leftButton;
    public GameObject rightButton;
    public GameObject downButton;
    public GameObject insertButton;
    public HoleOpen hole;
    public CrainMotion crain;
    public bool motion;

    private bool touch;
    private bool leftTouch;
    private bool rightTouch;
    private bool one;
    private GameObject nowTouchButton;
    private Vector2 touchPos;
    private RaycastHit2D hit;

    private GatchaTokenText tokenText;

    [SerializeField]
    private LayerMask mask;

    private void Awake()
    {
        tokenText = GetComponent<GatchaTokenText>();
    }

    private void Start()
    {
        touch = true;
        motion = false;
    }

    private void Update()
    {
        // UI터치 방지
        if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            return;
        if (Input.GetMouseButtonDown(0))
        {
            if (touch)
            {
                touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                hit = Physics2D.Raycast(touchPos, Vector2.zero, 0.0f, mask);
                if (hit.collider == null)
                    return;

                Debug.Log(hit.collider.gameObject.name);

                if (hit.collider.gameObject == leftButton)
                {
                    leftTouch = true;
                    TouchMotion(hit.collider.gameObject);
                }

                else if (hit.collider.gameObject == rightButton)
                {
                    rightTouch = true;
                    TouchMotion(hit.collider.gameObject);
                }

                else if (hit.collider.gameObject == insertButton)
                {
                    TouchMotion(hit.collider.gameObject);
                }

                else if (hit.collider.gameObject == downButton)
                {
                    TouchMotion(hit.collider.gameObject);
                }

                touch = false;
                nowTouchButton = hit.collider.gameObject;
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (!touch)
            {
                AllFalse();
                touch = true;
                if (nowTouchButton == leftButton)
                    crain.StopL();
                else if (nowTouchButton == rightButton)
                    crain.StopR();
                NonTouchMotion(nowTouchButton);
                touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                hit = Physics2D.Raycast(touchPos, Vector2.zero, 0.0f);
                if(hit.collider != null)
                {
                    if (hit.collider.tag == "fieldCat")
                        return;
                    else if (hit.collider.gameObject == insertButton)
                    {
                        if (!motion)
                        {
                            motion = true;
                            tokenText.StartMotionStarter();
                            if (tokenText.HaveToken)
                                crain.StartMotionStarter();
                        }
                    }

                    else if (hit.collider.gameObject == downButton)
                    {
                        if (tokenText.HaveToken)
                        {
                            tokenText.MinCoin();
                            tokenText.SetText();
                            crain.MotionStarter();
                        }
                    }

                    else if (hit.collider.gameObject == hole.gameObject)
                    {
                        hole.OnCilck();
                    }
                }
            }
        }

        if (leftTouch)
        {
            if (crain.play)
                crain.StartL();
        }
        if (rightTouch)
        {
            if (crain.play)
                crain.StartR();
        }
    }

    private void AllFalse()
    {
        leftTouch = false;
        rightTouch = false;
    }

    private void TouchMotion(GameObject button)
    {
        Color dark = new Color(0.5f, 0.5f, 0.5f);
        SpriteRenderer sprite = button.GetComponent<SpriteRenderer>();

        sprite.DOPause();
        sprite.DOColor(dark, 0.25f);


        AudioManager.Instance.PlaySound("hyo_2", transform.position);
    }

    private void NonTouchMotion(GameObject button)
    {
        Color origin = new Color(1f, 1f, 1f);

        if (button != null)
        {
            SpriteRenderer sprite = button.GetComponent<SpriteRenderer>();
            sprite.DOPause();
            sprite.DOColor(origin, 0.25f);
        }
        nowTouchButton = null;
    }

    private void CrainMotionState(bool state)
    {
        motion = state;
        insertButton.GetComponent<Collider2D>().enabled = !state;
    }
}
