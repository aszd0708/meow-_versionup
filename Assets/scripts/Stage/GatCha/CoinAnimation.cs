﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAnimation : MonoBehaviour
{
    public Transform coin;
    public Transform startPos;
    public Transform endPos;
    public Transform pos;
    public float totlaTime;
    public float[] time = new float[2];

    [SerializeField]
    private GameObject spriteMask;

    private void Start()
    {
        coin.localPosition = pos.localPosition;
    }

    public float MotionStarter()
    {
        totlaTime = 0;
        StartCoroutine("Motion");
        for (int i = 0; i < time.Length; i++)
            totlaTime += time[i];
        return totlaTime;
    }

    private IEnumerator Motion()
    {
        spriteMask.SetActive(true);
        coin.gameObject.SetActive(true);
        coin.transform.localPosition = pos.localPosition;
        coin.DOLocalMoveY(startPos.localPosition.y, time[0]);
        yield return new WaitForSeconds(time[0] + 0.25f);
        coin.DOLocalMoveX(endPos.localPosition.x, time[1]);
        yield return new WaitForSeconds(time[1]);
        coin.gameObject.SetActive(false);
        spriteMask.SetActive(false);
        yield break;
    }
}
