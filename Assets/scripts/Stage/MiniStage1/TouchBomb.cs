﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TouchBomb : MonoBehaviour
{
    private bool touch;
    public float checkTime = -0.5f;
    public GameObject bomb;
    private float timeSpan = 0.5f;

    public AudioClip[] bombSoud;

    // Start is called before the first frame update
    void Start()
    {
        timeSpan = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            return;
        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                if (EventSystem.current.IsPointerOverGameObject(i))
                    return;
            }
        }
        
        if (Input.GetMouseButtonDown(0))
        {
            timeSpan = Time.time;
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(touchPos, Vector2.zero, 0.0f);
        }

        else if (Input.GetMouseButtonUp(0))
        {
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(touchPos, Vector2.zero, 0.0f);
            timeSpan -= Time.time;

            if (timeSpan >= checkTime)
            {
                if (hit.collider != null)
                {
                    // 이 에러는 잠깐동안 보류하는걸로

                    if (hit.collider.name == "Hole")
                    {
                        hit.collider.SendMessage("OnClick", SendMessageOptions.DontRequireReceiver);
                    }

                    else
                    {
                        GameObject createBomb = Instantiate(bomb);
                        createBomb.transform.position = touchPos;
                        Destroy(createBomb, 0.1f);
                    }
                }
                else
                {
                    GameObject createBomb = Instantiate(bomb);
                    createBomb.transform.position = touchPos;
                    Destroy(createBomb, 0.1f);
                }
                AudioManager.Instance.PlaySound(bombSoud, touchPos, null, Random.Range(0.3f, 1.0f));
            }
        }
    }
}
