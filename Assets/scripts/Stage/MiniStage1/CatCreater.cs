﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatCreater : MonoBehaviour
{
    public CatSaveLoad file;
    public List<CatSaveLoad.JsonCat> json = new List<CatSaveLoad.JsonCat>();
    public GameObject objCreater;
    public Vector3 createPos;
    public int count;

    private List<int> collectCat = new List<int>();
    void Start()
    {
        List<CatSaveLoad.JsonCat> jsonCat = file.LoadCats();
        for(int i = 0; i < jsonCat.Count; i++)
            if (jsonCat[i].Collect)
                collectCat.Add(jsonCat[i].No);
    }
}
