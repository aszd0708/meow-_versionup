﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BambooCreater : MonoBehaviour
{
    public float disX, disY;
    public int count;
    public float[] rangeX = new float[2];
    public float[] rangeY = new float[2];
    public List<GameObject> bamboo = new List<GameObject>();
    public Vector3[,] bambooPos;
    public MiniStageCatCreater creater;
    private float x, y;
    private Transform[] linePos = new Transform[8];
    // Start is called before the first frame update
    void Start()
    {
        x = 0;
        y = 0;
        for (int i = 0; i < transform.childCount; i++)
            linePos[i] = transform.GetChild(i).transform;
        bambooPos = new Vector3[linePos.Length, count];
        BamboolCreater();
    }

    private void BamboolCreater()
    {
        for(int i = 0; i < linePos.Length; i++)
        {
            for(int j = 0; j < count; j++)
            {
                GameObject createBamboo = Instantiate(bamboo[Random.Range(0,bamboo.Count)]);
                createBamboo.transform.SetParent(linePos[i]);
                createBamboo.transform.localPosition = new Vector3(x - Random.Range(0,0.5f), y - Random.Range(0,0.3f), 0);
                bambooPos[i, j] = createBamboo.transform.position;
                creater.createPos.Add(bambooPos[i, j]);
                x += disX;
            }
            x = 0;
            y -= disY;
        }
        creater.Create();
    }
}
