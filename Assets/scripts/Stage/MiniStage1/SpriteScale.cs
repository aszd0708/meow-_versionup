﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteScale : MonoBehaviour
{
    private Vector3 m_leftDown;
    private Vector3 m_rightUp;

    // Start is called before the first frame update
    void Start()
    {
        m_leftDown = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0));
        m_rightUp = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
        float weight = m_rightUp.x - m_leftDown.x;
        float height = m_rightUp.y - m_leftDown.y;
        transform.localScale = new Vector3(weight / GetComponent<SpriteRenderer>().bounds.size.x, height / GetComponent<SpriteRenderer>().bounds.size.y, 0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
