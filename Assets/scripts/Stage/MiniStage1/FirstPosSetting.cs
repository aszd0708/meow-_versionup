﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPosSetting : MonoBehaviour
{
    [Header("원하는 스케일")]
    [SerializeField]
    private float scale = 0.75f;
    private Vector3 m_leftDown;
    
    void Awake()
    {
        m_leftDown = Camera.main.ScreenToWorldPoint(Vector3.zero);
        transform.position = new Vector3(m_leftDown.x, m_leftDown.y, 0);
        float scaleValue = SettingScale(GetFactor());
        transform.localScale = new Vector3(scaleValue, scaleValue, 0);
        //transform.localScale *= scaleValue;
    }

    private float GetFactor()
    {
        float width = Screen.width, height = Screen.height;
        float max = 0, min = 0;
        if (width > height)
        {
            max = height;
            min = width;
        }
        else
        {
            max = width;
            min = height;
        }
        while(max % min != 0)
        {
            float temp = max % min;
            max = min;
            min = temp;
        }
        Debug.Log("Min : " + min);
        return max;
    }

    private float SettingScale(float factor)
    {
        float width = Screen.width / factor;
        float height = Screen.height / factor;
        float value = ((width / height) / (16f / 9f)) * scale;
        //float value = ((width / height)) * scale;
        Debug.Log("value : " + value);
        return value;
    }
}
