﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniStageCatCreater : MonoBehaviour
{
    public CatSaveLoad file;
    public List<CatSaveLoad.JsonCat> json = new List<CatSaveLoad.JsonCat>();
    public GameObject objCreater;
    public int count;
    public List<GameObject> cat = new List<GameObject>();
    public List<Vector3> createPos = new List<Vector3>();

    private List<int> collectCat = new List<int>();
    private List<Vector3> usedPos = new List<Vector3>();
    private List<int> usedCat = new List<int>();
    void Start()
    {
        List<CatSaveLoad.JsonCat> jsonCat = file.LoadCats();
        for (int i = 0; i < jsonCat.Count; i++)
            if (jsonCat[i].Collect)
                collectCat.Add(jsonCat[i].No);
    }

    private bool Compare(int num, Vector3 vec)
    {
        for (int i = 0; i < usedPos.Count; i++)
        {
            if (num == usedCat[i] || vec == usedPos[i])
                return false;
        }
        return true;
    }

    public void Create()
    {
        int catRandom = Random.Range(0, collectCat.Count);
        int posRandom = Random.Range(0, createPos.Count);

        for(int i = 0; i < count; i++)
        {
            while(!Compare(collectCat[catRandom], createPos[posRandom]))
            {
                catRandom = Random.Range(0, collectCat.Count);
                posRandom = Random.Range(0, createPos.Count);
            }
            GameObject createCat = Instantiate(cat[collectCat[catRandom]]);
            createCat.transform.position = createPos[posRandom];
        }
    }
}
