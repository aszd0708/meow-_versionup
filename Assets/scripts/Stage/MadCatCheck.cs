﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MadCatCheck : MonoBehaviour
{
    public int no;
    public bool collect;
    public CatSaveLoad jsonCat;
    public CatSaveLoad.JsonCat catData;

    void Start()
    {
        LoadCatData();
    }

    private void LoadCatData()
    {
        List<CatSaveLoad.JsonCat> allData = jsonCat.LoadCats();
        for(int i = 0; i < allData.Count; i++)
        {
            if (allData[i].No == no)
                catData = allData[i];
        }
        collect = catData.Collect;
    }
}
