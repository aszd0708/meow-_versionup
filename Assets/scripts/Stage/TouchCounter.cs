﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchCounter : MonoBehaviour
{
    public int basicCount;
    public GameObject coinCat;
    public Vector3[] createPos = new Vector3[2];
    private int touchCount;
    void Start()
    {
        if (!EncryptedPlayerPrefs.HasKey("touchCount"))
            SetCount(basicCount);

        else
            touchCount = EncryptedPlayerPrefs.GetInt("touchCount");
    }

    public bool MinCount()
    {
        int count = touchCount - 1;

        if (count == 0)
        {
            SetCount(basicCount);
            CreateCoinCat();
            return true;
        }

        else
        {
            SetCount(count);
            return false;
        }
    }

    private void SetCount(int count)
    {
        EncryptedPlayerPrefs.SetInt("touchCount", count);
        touchCount = count;
    }

    private void CreateCoinCat()
    {
        Vector3 create = new Vector3(Random.Range(createPos[0].x, createPos[1].y), Random.Range(createPos[0].y, createPos[1].y), 0);
        GameObject cat = Instantiate(coinCat);
        cat.transform.position = create;
    }
}
