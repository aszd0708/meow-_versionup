﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage3LightMotion : MonoBehaviour
{
    public float fadeTime;
    public float waitTime;

    private SpriteRenderer sprite;

    private void Awake()
    {
        sprite = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        StartCoroutine(LightMotion());
    }

    private IEnumerator LightMotion()
    {
        while(true)
        {
            sprite.DOFade(0.0f, fadeTime).SetEase(Ease.Linear);
            yield return new WaitForSeconds(fadeTime);

            sprite.DOFade(1, fadeTime).SetEase(Ease.Linear);
            yield return new WaitForSeconds(fadeTime + waitTime);
        }
    }
}
