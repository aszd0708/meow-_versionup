﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FirstZoomCam : MonoBehaviour
{
    public Image fingerImg;
    public Camera cam;
    public Sprite[] fingerSprite = new Sprite[2];

    // Start is called before the first frame update
    private void Start()
    {
        transform.DOScale(new Vector3(0, 0, 0),0).SetEase(Ease.OutBack);
        cam.GetComponent<SetSubCamSize>().enabled = false;
        cam.enabled = false;
    }

    public void MotionStarter()
    {
        cam.GetComponent<SetSubCamSize>().enabled = true;
        cam.enabled = true;
        StartCoroutine("Motion");
    }

    // Update is called once per frame
    public void Stop()
    {
        StopCoroutine("Motion");
        transform.DOScale(new Vector3(0, 0, 0), 0.5f).SetEase(Ease.OutBack);
        cam.GetComponent<SetSubCamSize>().enabled = false;
        cam.enabled = false;
    }

    private void Change(int num)
    {
        fingerImg.sprite = fingerSprite[num];
    }

    private IEnumerator Motion()
    {
        transform.DOScale(new Vector3(1, 1, 1), 0.5f).SetEase(Ease.OutElastic);
        while (true)
        {
            Change(0);
            yield return new WaitForSeconds(1.5f);
            Change(1);
            yield return new WaitForSeconds(1.5f);
        }
    }
}