﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FirstZoomOneSprite : MonoBehaviour
{
    public Image fingerImg;
    public Image zoomImg;
    public Sprite[] fingerSprite = new Sprite[2];
    public Sprite[] zoomSpirte = new Sprite[2];

    private Coroutine zoomCor;

    // Start is called before the first frame update
    public void Start()
    {
        transform.DOScale(new Vector3(0, 0, 1), 0.0f).SetEase(Ease.OutElastic);
    }

    public void MotionStarter()
    {
        zoomCor = StartCoroutine(Motion());
    }

    // Update is called once per frame
    public void Stop()
    {
        StopCoroutine(zoomCor);
        transform.DOScale(new Vector3(0, 0, 1), 0.5f).SetEase(Ease.OutElastic);
    }

    private void Change(int num)
    {
        fingerImg.sprite = fingerSprite[num];
        zoomImg.sprite = zoomSpirte[num];
    }

    private IEnumerator Motion()
    {
        transform.DOScale(new Vector3(1, 1, 1), 0.5f).SetEase(Ease.OutElastic);
        while (true)
        {
            Change(0);
            yield return new WaitForSeconds(0.5f);
            Change(1);
            yield return new WaitForSeconds(0.5f);
        }
    }
}
