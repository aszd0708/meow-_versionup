﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-04-03
 * 줌인 줌아웃 UI 구현
 * 이 변수는 털려도 큰 상관이 없기 때문에 PlayerPrefs로 만들었음
 */

public class FirstZoomTwoSprite : MonoBehaviour
{
    private RectTransform rect;
    // Start is called before the first frame update

    private void Awake()
    {
        rect = GetComponent<RectTransform>();
    }
    private void Start()
    {
        StartCoroutine(ShowPop());
    }

    private IEnumerator ShowPop()
    {
        yield return new WaitForSeconds(0.5f);
        int showTheUI = PlayerPrefs.GetInt("ShowTheZoom", 0);
        switch (showTheUI)
        {
            case 0:
                MotionStarter();
                break;
            case 1:
                rect.localScale = new Vector3(0, 0, 0);
                break;
        }
        yield break;
    }

    public void MotionStarter()
    {
        //MenuManager.Instance.SetState(true);
        PopupManager.Instance.OpenPopup(rect);
    }

    public void Stop()
    {
        // MenuManager.Instance.SetState(false);
        PopupManager.Instance.ClosedPopup(rect);
        PlayerPrefs.SetInt("ShowTheZoom", 1);
    }
}
