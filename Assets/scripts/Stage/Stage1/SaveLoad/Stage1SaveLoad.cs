﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class Stage1SaveLoad : MonoBehaviour
{
    public GameObject item;
    public GameObject hiddenCat;

    public int catCount = 0;
    public int itemCount = 0;

    public int hiidenCatStartNum;

    public string stage;

    [Serializable]
    public class FieldObject
    {
        public string itemName;
        public bool itemCollect;
        public string catName;
        public bool catCollect;
        public System.DateTime chanceTime;
        public int chanceum;
    }

    public List<FieldObject> info = new List<FieldObject>();

    private Stage1Objects obj;

    void Awake()
    {
        obj = GetComponent<Stage1Objects>();

        if (!System.IO.File.Exists(Application.persistentDataPath + "/" + stage + "Info.dat"))
        {
            int count;

            if (item.transform.childCount < obj.cat.Count)
                count = obj.cat.Count;
            else
                count = item.transform.childCount;

            for (int i = 0; i < count; i++)
            {
                FieldObject temp = InfoDataSetting(i);              
                info.Add(temp);
            }
            PlayerPrefs.SetInt("/" + stage + "CatCount", catCount);
            PlayerPrefs.SetInt("/" + stage + "ItemCount", itemCount);

            //int index = Mathf.Abs(obj.cat.Count - item.transform.childCount);
            //if (item.transform.childCount < obj.cat.Count)
            //{
            //    for(int i = index; i < obj.cat.Count; i++)
            //    {
            //        FieldObject temp = InfoDataSetting(i);
            //    }
            //}
                Save();
        }

        else
            Load();
    }

    // Start is called before the first frame update
    public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/" + stage + "Info.dat");

        List<FieldObject> saveInfo = new List<FieldObject>();

        saveInfo = info;

        bf.Serialize(file, saveInfo);
        file.Close();
    }

    public void Load()
    {
        if (!System.IO.File.Exists(Application.persistentDataPath + "/" + stage + "Info.dat"))
        {
            return;
        }

        else
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.OpenRead(Application.persistentDataPath + "/" + stage + "Info.dat");

            if (file != null && file.Length > 0)
            {
                // 파일 역직렬화하여 B에 담기
                List<FieldObject> saveInfo = (List<FieldObject>)bf.Deserialize(file);
                // B --> A에 할당
                info = saveInfo;
                file.Close();
            }
        }
    }

    public void ResetSave()
    {
        System.IO.File.Delete(Application.persistentDataPath + "/" + stage + "Info.dat");
        Debug.Log("파일 삭제");
    }

    public void DeleteData(int stageNumber)
    {
        System.IO.File.Delete(Application.persistentDataPath + "/" + "Stage" + stageNumber + "Info.dat");
    }

    public FieldObject InfoDataSetting(int index)
    {
        FieldObject temp = new FieldObject();
        Debug.Log("자식 갯수" + item.transform.childCount);
        Debug.Log("i의 값" + index);

        if (index < item.transform.childCount)
        {
            ItemCheck itemInfo = item.transform.GetChild(index).GetComponent<ItemCheck>();
            temp.itemName = itemInfo.name;
            temp.itemCollect = itemInfo.collect;
            itemCount++;
        }

        if (index < obj.cat.Count)
        {
            CatCheck catInfo = obj.cat[index].GetComponent<CatCheck>();
            temp.catName = catInfo.name;
            temp.catCollect = catInfo.collect;
            catCount++;
        }

        else
        {
            if (hiidenCatStartNum != 0)
            {
                CatCheck hiddenCatInfo = hiddenCat.transform.GetChild(hiidenCatStartNum - 1).GetComponent<CatCheck>();
                temp.catName = hiddenCatInfo.name;
                temp.catCollect = hiddenCatInfo.collect;
                catCount++;
                hiidenCatStartNum++;
            }
        }
        return temp;
    }
}
