﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage1Objects : MonoBehaviour
{
    public List<GameObject> item = new List<GameObject>();
    public List<GameObject> cat = new List<GameObject>();

    private Stage1SaveLoad saveLoad;

    void Awake()
    {
        saveLoad = GetComponent<Stage1SaveLoad>();
    }

    void Start()
    {
        saveLoad.Load();
        Debug.Log("세이브 로드 인포 카운트" + saveLoad.info.Count);
        Debug.Log("아이템 카운트" + item.Count);
        for (int i = 0; i < item.Count; i++)
            for (int j = 0; j < item.Count; j++)
                if (saveLoad.info[j].itemName == item[i].GetComponent<ItemCheck>().name)
                    item[i].GetComponent<ItemCheck>().CheckObject();

        for (int i = 0; i < cat.Count; i++)
        {
            for (int j = 0; j < cat.Count; j++)
            {
                if (cat[i].GetComponent<CatCheck>().name == saveLoad.info[j].catName)
                    cat[i].GetComponent<CatCheck>().CheckObject();
            }
        }
    }
}
