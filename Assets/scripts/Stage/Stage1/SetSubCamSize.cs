﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetSubCamSize : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("Motion");
    }

    private IEnumerator ZoomIn(float max, float min, float animeTime)
    {
        Camera touchCam = GetComponent<Camera>();
        float zoomMax = max, zoomMin = min;
        float time = 0.0f;

        while (zoomMax > zoomMin)
        {
            time += Time.deltaTime / animeTime;

            zoomMax = Mathf.Lerp(max, min, time);

            touchCam.orthographicSize = zoomMax;
            yield return null;
        }
    }

    private IEnumerator ZoomOut(float max, float min, float animeTime)
    {
        Camera touchCam = GetComponent<Camera>();
        float zoomMax = max, zoomMin = min;
        float time = 0.0f;

        while (zoomMax > zoomMin)
        {
            time += Time.deltaTime / animeTime;

            zoomMax = Mathf.Lerp(min, max, time);

            touchCam.orthographicSize = zoomMax;
            yield return null;
        }
    }

    private IEnumerator Motion()
    {
        while(true)
        {
            StartCoroutine(ZoomIn(5, 3, 1.5f));
            yield return new WaitForSeconds(1.5f);
            StartCoroutine(ZoomOut(5, 3, 1.5f));
            yield return new WaitForSeconds(1.5f);
        }
    }
}
