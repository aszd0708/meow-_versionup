﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage1GrandmaAndBird : MonoBehaviour, TouchableObj
{
    #region 할머니 움직임
    [Header("팔이 움직이는 시간")]
    public float time;
    [Header("팔 Transform")]
    public Transform armTransform;
    [Header("팔이 움직이는 각도")]
    public float moveAngle = 20;
    #endregion

    #region 과자
    [Header("비어있는 과자 오브젝트")]
    public GameObject emptySnack;
    [Header("과자 스프라이트들")]
    public Sprite[] snack = new Sprite [5];

    [Header("스낵 부모")]
    public Transform snackPose;
    [Header("스낵이 랜덤으로 뿌려질 Transform")]
    public Transform[] snackRandomPose;
    #endregion

    [Header("원래 있던 그냥 있는 새")]
    public BirdMove[] standingBirds;

    #region 날아오는 새
    [Header("날으는 새 오브젝트")]
    public GameObject flyingBirdObj;
    [Header("날으는 새 부모")]
    public Transform flyingBirdParent;
    [Header("나올 새 마릿수")]
    public int birdCount;
    [Header("비둘기 위치 랜덤으로 해주기 위한 XY좌표")]
    public float[] flyingBirdX = new float[2];
    public float flyingBirdY;
    [Header("비둘기가 다시 밖으로 나갈 때 XY 좌표")]
    public float[] exitPosXY = new float[3];
    #endregion

    /// <summary>
    /// 현재 터치가 된 상태인지
    /// </summary>
    private bool nowTouch = true;

    public bool DoTouch(Vector2 touchPose = default)
    {
        if(nowTouch)
        {
            AudioManager.Instance.PlaySound("slide_4", transform.position);
            StartCoroutine(_StartEvent());
        }
        return false;
    }

    private IEnumerator _StartEvent()
    {
        nowTouch = false;
        armTransform.DORotate(new Vector3(0, 0, moveAngle), time);
        StartCoroutine(_SpreadSnack());
        yield return new WaitForSeconds(time);
        armTransform.DORotate(new Vector3(0, 0, -moveAngle), time);
        nowTouch = true;
        yield break;
    }

    private IEnumerator _SpreadSnack()
    {
        yield return new WaitForSeconds(0.5f);
        WaitForSeconds waitSecond = new WaitForSeconds(0.1f);
        for (int i = 0; i< 8; i++)
        {
            int num = Random.Range(0, snack.Length);
            GameObject groundSnack  = PoolingManager.Instance.GetPool("Snack");
            if(groundSnack == null)
                groundSnack = Instantiate(emptySnack);
            SpriteRenderer snackSpriteRenderer = groundSnack.GetComponent<SpriteRenderer>();

            snackSpriteRenderer.sortingLayerName = "2";
            snackSpriteRenderer.sortingOrder = 4;
            snackSpriteRenderer.sprite = snack[num];

            groundSnack.transform.SetParent(snackPose);            
            groundSnack.transform.localScale = Vector3.one;
            groundSnack.transform.localPosition = Vector3.zero;
            groundSnack.transform.SetParent(null);

            groundSnack.transform.DOMove(new Vector3(
                Random.Range(snackRandomPose[0].position.x, snackRandomPose[1].position.x), 
                Random.Range(snackRandomPose[0].position.y, snackRandomPose[1].position.y), 0), 1)
                .SetEase(Ease.Linear);

            PoolingManager.Instance.SetPool(groundSnack, "Snack", 10);
            AudioManager.Instance.PlaySound("gogo_1", transform.position);
            yield return waitSecond;
        }

        for(int i = 0; i< standingBirds.Length; i++)
        {
            standingBirds[i].snack = true;
        }

        for(int i = 0; i < birdCount; i++)
        {
            GameObject flyBird;
            flyBird = PoolingManager.Instance.GetPoolRandom("Bird");
            if(flyBird == null)
                flyBird = Instantiate(flyingBirdObj);

            flyBird.transform.SetParent(gameObject.transform);
            flyBird.SendMessage("SetRandomPos", exitPosXY, SendMessageOptions.DontRequireReceiver);

            flyBird.transform.localScale = new Vector3(2.0f, 2.0f, 0);
            flyBird.transform.localPosition = new Vector3(Random.Range(8, 35), flyingBirdY, -1);
        }
        yield break;
    }
}
