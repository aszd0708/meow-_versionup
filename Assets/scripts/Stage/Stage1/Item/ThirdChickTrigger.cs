﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdChickTrigger : MonoBehaviour, I_NonColObject
{
    public TriggerMove moveObject;
    public Sprite[] changeSpirte;

    void Start()
    {
        GetComponent<SpriteRenderer>().sprite = changeSpirte[0];
    }

    public void OnClick()
    {
        if (GetComponent<SpriteRenderer>().sprite == changeSpirte[0])
            GetComponent<SpriteRenderer>().sprite = changeSpirte[1];
        else if (GetComponent<SpriteRenderer>().sprite == changeSpirte[1])
        {
            GetComponent<SpriteRenderer>().sprite = changeSpirte[2];
            if (moveObject == null)
                return;
            moveObject.Trigger();
        }
    }
}
