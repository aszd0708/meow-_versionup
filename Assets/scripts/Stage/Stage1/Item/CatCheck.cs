﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CatCheck : MonoBehaviour
{
    public new string name;
    public int no;
    [HideInInspector] public bool collect = false;
    public bool loadCollect;
    public GameObject saveLoad;
    public AudioClip effectSound;

    void Awake()
    {
        if(SceneManager.GetActiveScene().name != "Gatcha")
        {
            CheckObject();
            CheckCollect();
        }
    }

    public void CheckObject()
    {
        for (int i = 0; i < saveLoad.GetComponent<Stage1SaveLoad>().info.Count; i++)
        {
            if (saveLoad.GetComponent<Stage1SaveLoad>().info[i].catName == name && saveLoad.GetComponent<Stage1SaveLoad>().info[i].catCollect == true)
            {
                if (gameObject.tag == "hiddenCat")
                    GameObject.Find("HiddenCatInfo").transform.GetChild(0).GetComponent<SampleCatButton>().collect = true;
                else
                    GameObject.Find("Cat Panel").transform.GetChild(0).transform.GetChild(0).transform.GetChild(i).GetComponent<SampleCatButton>().OnClick();
            }
        }
    }

    public void CollectObject()
    {
        collect = true;
        CheckStageCatCol checker = FindObjectOfType<CheckStageCatCol>();
        Debug.Log(saveLoad.GetComponent<Stage1SaveLoad>().info.Count);
        for (int i = 0; i < saveLoad.GetComponent<Stage1SaveLoad>().info.Count; i++)
        {
            if (saveLoad.GetComponent<Stage1SaveLoad>().info[i].catName == name)
            {
                saveLoad.GetComponent<Stage1SaveLoad>().info[i].catCollect = collect;
                saveLoad.GetComponent<Stage1SaveLoad>().Save();
                Debug.Log(saveLoad.GetComponent<Stage1SaveLoad>().info[i].catCollect);
            }
        }
        checker.CheckCol();
    }

    public void CheckCollect()
    {
        saveLoad.GetComponent<Stage1SaveLoad>().Load();
        for (int i = 0; i < saveLoad.GetComponent<Stage1SaveLoad>().info.Count; i++)
        {
            if (saveLoad.GetComponent<Stage1SaveLoad>().info[i].catName == name)
            {
                loadCollect = saveLoad.GetComponent<Stage1SaveLoad>().info[i].catCollect;
            }
        }
    }
}
