﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemTriggerObject : MonoBehaviour
{
    public ChanceNumImage chance;
    [System.Serializable]
    public class TriggerObject
    {
        public string catName;
        public GameObject box;
    }
    public List<TriggerObject> trigger = new List<TriggerObject>();
}
