﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemCheck : MonoBehaviour
{
    public new string name;
    public bool collect = false;
    private Stage1SaveLoad saveLoad;

    void Awake()
    {
        saveLoad = FindObjectOfType<Stage1SaveLoad>();
        //CheckObject();
    }

    public void CheckObject()
    {
        for (int i = 0; i < saveLoad.info.Count; i++)
        {
            if (saveLoad.info[i].itemName == name && saveLoad.info[i].itemCollect)
            {
                print("수집 된 아이템 이름 : " + name);
                GetComponent<SampleButtonScript>().LoadGo();
                Destroy(gameObject);
            }
        }
    }

    public void ColletObject()
    {
        collect = true;

        for (int i = 0; i < saveLoad.info.Count; i++)
        {
            if (saveLoad.info[i].itemName == name)
            { 
                saveLoad.info[i].itemCollect = collect;
                saveLoad.Save();
            }
        }
    }
}
