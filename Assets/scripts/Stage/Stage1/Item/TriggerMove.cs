﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerMove : MonoBehaviour
{
    public enum State
    {
        mouse, churu, babyCat
    }
    public ThirdChickTrigger triggerObject;
    public State state;

    public Transform babyCatTree;

    public void Trigger()
    {
        switch (state)
        {
            case State.mouse:
                MouseMotion();
                return;

            case State.churu:
                ChuruMotion();
                return;

            case State.babyCat:
                BabyCatMove();
                return;
        }
    }

    private void MouseMotion()
    {
        GetComponent<CircleCollider2D>().enabled = true;
        //iTween.MoveTo(gameObject, iTween.Hash("x", triggerObject.gameObject.transform.position.x + 0.5, "easeType", iTween.EaseType.linear));
        transform.DOMoveX(triggerObject.transform.position.x + 0.5f, 0.75f).SetEase(Ease.Linear);
    }

    private void ChuruMotion()
    {
        transform.DOJump(new Vector3(transform.position.x + 2, transform.position.y, transform.position.z), 1.5f, 1, 1.0f);
    }

    private void BabyCatMove()
    {
        babyCatTree.position = new Vector3(babyCatTree.position.x, babyCatTree.position.y, -3.0f);
        GetComponent<BabyCat>().show = true;
    }

    void Up()
    {
        babyCatTree.position = new Vector3(babyCatTree.position.x, babyCatTree.position.y, 5);
        GetComponent<BabyCat>().show = true;
    }
}
