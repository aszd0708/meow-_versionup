﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectCheck : MonoBehaviour
{
    public new string name;
    public bool collect = false;
    private GameObject saveLoad;

    void Start()
    {
        saveLoad = GameObject.Find("SaveManager");
        CheckObject();
    }

    public void CheckObject()
    {
        if (collect)
            Destroy(gameObject);
    }

    public void ColletObject()
    {
        collect = true;

        
    }
}
