﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ObjectTrigger : MonoBehaviour
{
    public string catName;
    public float V = 0.5f;

    public Vector3 itemPosition;

    public string sortingLayer;

    private ChanceNumImage chance;
    private GameObject hitBox;
    private GameObject trigger;
    private GameObject UIItem;

    private UsingItemCatsBox catBox;

    private void OnEnable()
    {
        catBox = FindObjectOfType<UsingItemCatsBox>();
    }

    void Start()
    {
        ItemTriggerObject trigger = FindObjectOfType<ItemTriggerObject>();

        chance = trigger.chance;
        for (int i = 0; i < trigger.trigger.Count; i++)
        {
            if (trigger.trigger[i].catName == catName)
            {
                hitBox = trigger.trigger[i].box;
                break;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(Camera.main.ScreenToWorldPoint(UIItem.transform.position).x, Camera.main.ScreenToWorldPoint(UIItem.transform.position).y, -5);

        TouchOnOffManager.Instance.TouchOnOff(false);

        if (Input.GetMouseButtonUp(0))
        {
            if (SceneManager.GetActiveScene().name == "Stage0 Tutorial")
            {
                if (Vector2.Distance(hitBox.transform.position, gameObject.transform.position) <= V)
                {
                    GameObject createItem = Instantiate(gameObject);
                    SpriteRenderer itemSpriteRenderer = createItem.GetComponent<SpriteRenderer>();
                    itemSpriteRenderer.color = new Color(itemSpriteRenderer.color.r, itemSpriteRenderer.color.g, itemSpriteRenderer.color.b, 255);
                    createItem.transform.position = hitBox.transform.GetChild(hitBox.transform.childCount - 1).transform.position;
                    //hitBox.transform.position;
                    createItem.GetComponent<DragItem>().enabled = false;
                    createItem.GetComponent<ObjectTrigger>().enabled = false;

                    if (catName == "InfoCat")
                    {
                        InfoCatMotion infoCatMotion = FindObjectOfType<InfoCatMotion>();
                        if (!infoCatMotion.collect)
                            infoCatMotion.MotionStarter();
                    }
                    VibratorControllor.ObjTouchVib();
                }
                else
                {
                    //chance.MinusChance();
                    VibratorControllor.WrongObjTouch();
                }
            }

            else if (SceneManager.GetActiveScene().name == "Stage1 Park")
            {
                if (Vector2.Distance(hitBox.transform.position, gameObject.transform.position) <= V)
                {
                    GameObject createItem = Instantiate(gameObject);
                    SpriteRenderer itemSpriteRenderer = createItem.GetComponent<SpriteRenderer>();
                    itemSpriteRenderer.color = new Color(itemSpriteRenderer.color.r, itemSpriteRenderer.color.g, itemSpriteRenderer.color.b, 255);
                    createItem.transform.position = hitBox.transform.GetChild(hitBox.transform.childCount - 1).transform.position;
                    createItem.GetComponent<DragItem>().enabled = false;
                    createItem.GetComponent<ObjectTrigger>().enabled = false;

                    UsingItemCat cat = catBox.GetUsingItemCats(catName);

                    if (cat != null)
                    {
                        cat.UseItem(createItem);
                    }

                    // 이부분이었슈 - 1

                    VibratorControllor.ObjTouchVib();
                }
                else
                {
                    chance.MinusChance();
                    VibratorControllor.WrongObjTouch();
                }
            }

            else if (SceneManager.GetActiveScene().name == "Stage2 City")
            {
                if (Vector2.Distance(hitBox.transform.position, gameObject.transform.position) <= V)
                {
                    GameObject createItem = Instantiate(gameObject);
                    SpriteRenderer itemSpriteRenderer = createItem.GetComponent<SpriteRenderer>();
                    itemSpriteRenderer.color = new Color(itemSpriteRenderer.color.r, itemSpriteRenderer.color.g, itemSpriteRenderer.color.b, 255);
                    createItem.transform.position = hitBox.transform.position;
                    createItem.GetComponent<SpriteRenderer>().sortingLayerName = "1";
                    createItem.GetComponent<DragItem>().enabled = false;
                    createItem.GetComponent<ObjectTrigger>().enabled = false;

                    UsingItemCat cat = catBox.GetUsingItemCats(catName);

                    if (cat != null)
                    {
                        cat.UseItem(createItem);
                    }

                    // 이부분이었슈 - 2

                    VibratorControllor.ObjTouchVib();
                }

                else
                {
                    chance.MinusChance();
                    VibratorControllor.WrongObjTouch();
                }
            }
            TouchOnOffManager.Instance.TouchOnOff(true);
            Destroy(gameObject);
        }

    }

    public void SetObject(GameObject UITem)
    {
        UIItem = UITem;
    }


    /* 1
     * if (catName == "YellowCat")
    {
        GameObject.Find(catName).GetComponent<YellowCatMove>().Onclick(createItem);
    }
    
    else if (catName == "FatCat")
    {
        GameObject.Find(catName).GetComponent<FatCatMove>().OnTrigger(createItem);
    }
    
    else if (catName == "BabyCat")
    {
        GameObject.Find(catName).GetComponent<BabyCat>().Collect();
    }
     */

    /*
     * 2
     * if (catName == "SwagCat")
                    {
                        Destroy(createItem);
                        GameObject.Find(catName).GetComponent<SwagCatMotion>().MoveMotion();
                    }

                    else if (catName == "Gomaymmee")
                    {
                        createItem.GetComponent<SpriteRenderer>().sortingLayerName = "2";
                        GameObject.Find(catName).GetComponent<SquareCatMotion>().Trigger(createItem.transform);
                    }

                    else if (catName == "BlackCat")
                    {
                        GameObject.Find(catName).GetComponent<NeroMotion>().OnClick();
                    }

                    else if (catName == "WhiteCat")
                    {
                        Destroy(createItem);
                        GameObject.Find(catName).GetComponent<CottonCatMotion>().OnClick();
                    }

                    else if (catName == "Jelly")
                    {
                        GameObject.Find(catName).GetComponent<JellyDogMotion>().MotionStarter(createItem);
                    }
                    else if (catName == "SpottedCat")
                    {
                        GameObject.Find(catName).GetComponent<SpottedCatMotion>().MotionStarter(createItem);
                    }
                    else if (catName == "BrownCat")
                    {
                        GameObject.Find(catName).GetComponent<BrownCatMotion>().MotionStarter(createItem);
                    }
     */
}
