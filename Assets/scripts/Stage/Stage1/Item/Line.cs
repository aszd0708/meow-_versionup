﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Line : MonoBehaviour
{
    public float fadeTime;
    private SpriteRenderer sprite;

    public float speed = 1.0f;
    // Update is called once per frame
    public float time;

    void Awake()
    {
        sprite = GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        sprite.color = new Color(0, 0, 0, 1);
        StartCoroutine(FadeLine());
    }

    void OnEnable()
    {
        sprite.color = new Color(0, 0, 0, 1);
        StartCoroutine(FadeLine());
    }

    private IEnumerator FadeLine()
    {
        sprite.DOFade(0, fadeTime);
        yield return new WaitForSeconds(fadeTime);
        PoolingManager.Instance.SetPool(gameObject, "Line");
        yield break;
    }
}
