﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FirstZoom : MonoBehaviour
{
    public Image fingerImg;
    public Image zoomImg;
    public Sprite[] fingerSprite = new Sprite[2];
    public Sprite[] zoomSpirte = new Sprite[2];

    // Start is called before the first frame update
    void Start()
    {
        if (!PlayerPrefs.HasKey("Zoom"))
        {
            PlayerPrefs.SetInt("Zoom", 1);
        }
        else
        {
            //Destroy(gameObject);
            //return;
        }
        StartCoroutine("Motion");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            StopCoroutine("Motion");
            Destroy(gameObject);
        }
    }

    private void Change(int num)
    {
        fingerImg.sprite = fingerSprite[num];
        zoomImg.sprite = zoomSpirte[num];
    }

    private IEnumerator Motion()
    {
        transform.DOScale(new Vector3(1, 1, 1), 0.5f).SetEase(Ease.OutElastic);
        while (true)
        {
            Change(0);
            yield return new WaitForSeconds(0.5f);
            Change(1);
            yield return new WaitForSeconds(0.5f);
        }
    }
}
