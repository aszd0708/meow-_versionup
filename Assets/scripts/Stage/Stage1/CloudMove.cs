﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudMove : MonoBehaviour
{
    enum State
    {
        left, right
    }

    private State state;
    private float speed;

    void Start()
    {
        RandomPos();
    }
    
    void Update()
    {
        switch(state)
        {
            case State.left:
                transform.position = new Vector2(transform.position.x - Time.deltaTime * speed, transform.position.y);
                break;
            case State.right:
                transform.position = new Vector2(transform.position.x + Time.deltaTime * speed, transform.position.y);
                break;
        }

        if (transform.position.x <= -40.5 || transform.position.x >= 35.5)
        {
            RandomPos();
            switch(state)
            {
                case State.left:
                    transform.position = new Vector2(35f, transform.position.y);
                    break;
                case State.right:
                    transform.position = new Vector2(-40f, transform.position.y);
                    break;
            }
        }
    }

    private void RandomPos()
    {
        int sight = Random.Range(0, 3);
        speed = Random.Range(1.0f, 3.0f);
        state = (State)Random.Range((float)State.left, (float)State.right+1);
        transform.position = new Vector2(transform.position.x, Random.Range(10f, 18f));

        if (sight == 0)
            transform.rotation = Quaternion.Euler(0, 180, 0);
        else
            transform.rotation = Quaternion.Euler(0, 0, 0);
    }
}
