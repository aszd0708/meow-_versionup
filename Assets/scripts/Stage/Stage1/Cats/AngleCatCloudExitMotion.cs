﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AngleCatCloudExitMotion : MonoBehaviour, I_NonColObject
{
    public GameObject angelCat;
    public float flyingTime;
    // Start is called before the first frame update

    private CircleCollider2D angelCatCircleCol;
    private Stage1AnimalAngelCat angelCatMotion;

    private bool flying = false;

    void Awake()
    {
        angelCatCircleCol = angelCat.GetComponent<CircleCollider2D>();
        angelCatMotion = angelCat.GetComponent<Stage1AnimalAngelCat>();
    }

    void Update()
    {
        if(flying)
            transform.position = new Vector3(1000, 1000, 0);
    }

    public void OnClick()
    {
        StartCoroutine(AngelMotion());
    }

    private IEnumerator AngelMotion()
    {
        //angelCat.GetComponent<SpriteRenderer>().enabled = true;
        transform.DOMove(angelCat.transform.position, 0.5f);
        yield return new WaitForSeconds(0.6f);
        flying = true;
        angelCat.transform.DOMoveY(angelCat.transform.position.y - 9.0f, flyingTime);
        yield return new WaitForSeconds(flyingTime);
        angelCatCircleCol.enabled = true;
        PoolingManager.Instance.SetPool(gameObject, "AngelCatDummy");
        yield break;
    }
}
