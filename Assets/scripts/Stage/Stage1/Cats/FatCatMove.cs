﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FatCatMove : UsingItemCat
{
    public Vector3 churuPos;
    public SpriteRenderer body;
    public SpriteRenderer eye;
    public float speed;

    public Sprite movingSprite;
    private Sprite idleSprite;

    public GameObject destination;
    public bool findChuru;

    public GameObject collectEffect;

    void Start()
    {
        findChuru = false;
        idleSprite = transform.GetChild(0).GetComponent<SpriteRenderer>().sprite;
    }

    public override void UseItem(GameObject item = null)
    {
        if(!findChuru && item != null)
            StartCoroutine(Motion(item));
    }

    public void OnTrigger(GameObject item)
    {
        StartCoroutine("Motion", item);
    }

    private IEnumerator Motion(GameObject item)
    {
        findChuru = true;
        CanCollect = true;
        GetComponent<Animator>().enabled = false;
        body.sprite = movingSprite;
        eye.enabled = false;
        transform.DORotate(new Vector3(0,0, 720), speed, RotateMode.FastBeyond360).SetEase(Ease.Linear);
        transform.DOMove(item.transform.position, speed).SetEase(Ease.Linear);
        yield return new WaitForSeconds(speed);
        Destroy(item);
        body.sprite = idleSprite;
        eye.enabled = true;
        GetComponent<Animator>().enabled = true;
        GameObject effect = Instantiate(collectEffect);
        effect.transform.position = new Vector3(transform.position.x, transform.position.y - 1.3f, effect.transform.position.z);
        effect.GetComponent<EffectMotion>().Motion();
        yield return null;
    }
}
