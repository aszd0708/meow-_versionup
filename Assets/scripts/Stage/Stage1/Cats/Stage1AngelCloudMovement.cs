﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage1AngelCloudMovement : Stage1CloudMovement, TouchableObj
{
    [SerializeField]
    [Header("구름 속 안에 있는 천사냥이 움직이는 클래스")]
    private Stage1CloudAngelCat cloudAngelCat;

    public bool DoTouch(Vector2 touchPose = default)
    {
        cloudAngelCat.PlayMotionCloudTouch();
        return false;
    }
}
