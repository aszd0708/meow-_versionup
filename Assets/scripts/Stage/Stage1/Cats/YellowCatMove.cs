﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class YellowCatMove : UsingItemCat
{
    public Sprite[] move = new Sprite[2];
    public Sprite catchMouse;
    public Sprite[] eye = new Sprite[2];
    public GameObject collectEffect;

    public bool huntingMouse;

    public float speed;
    public Vector2 eyePos;
    public SoundManager soundManager;
    private bool col;
    private Vector3 originPos;
    private bool touch;

    private CatCheck check;
    
    public string CatName { get => cat_Name; set => cat_Name = value; }

    void Awake()
    {
        check = GetComponent<CatCheck>();
    }

    void Start()
    {
        touch = true;
        huntingMouse = false;
    }


    public override void UseItem(GameObject item)
    {
        if (!col && item != null)
            StartCoroutine(Motion(item));
    }

    public void Onclick(GameObject item)
    {
        if(!col)
            StartCoroutine(Motion(item));
    }

    public void OhterStageAnimalOnClick()
    {
        if(touch)
            StartCoroutine("CatchMotion");
    }

    private IEnumerator Motion(GameObject item)
    {
        col = true;
        for (int i = 0; i < transform.childCount; i++)
            transform.GetChild(i).GetComponent<SpriteRenderer>().sortingLayerName = "2";
        GetComponent<Animator>().enabled = false;
        transform.DOMove(new Vector3(item.transform.position.x-1, item.transform.position.y, item.transform.position.z), speed).SetEase(Ease.Linear);
        for (float i = 0; i < speed;)
        {
            if(transform.GetChild(0).GetComponent<SpriteRenderer>().sprite == move[0])
                transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = move[1];
            else if(transform.GetChild(0).GetComponent<SpriteRenderer>().sprite == move[1])
                transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = move[0];
            i += speed / 4;
            yield return new WaitForSeconds(speed / 4);
        }
        gameObject.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = eye[0];
        yield return new WaitForSeconds(1.0f);
        transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = catchMouse;
        gameObject.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = eye[1];
        originPos = transform.GetChild(1).transform.position;
        gameObject.transform.GetChild(1).transform.position = new Vector3(gameObject.transform.GetChild(1).transform.position.x, gameObject.transform.GetChild(1).transform.position.y - 0.2f, gameObject.transform.GetChild(1).transform.position.z);
        Destroy(item);
        AudioManager.Instance.PlaySound(check.effectSound, transform.position);
        yield return new WaitForSeconds(1.0f);
        GetComponent<Animator>().enabled = true;
        GameObject effect = Instantiate(collectEffect);
        effect.transform.position = new Vector3(transform.position.x + 0.3f, transform.position.y - 0.6f, effect.transform.position.z);
        effect.GetComponent<EffectMotion>().Motion();
        huntingMouse = true;
        CanCollect = true;
        yield break;
    }

    private IEnumerator CatchMotion()
    {
        touch = false;
        CatCheck check = GetComponent<CatCheck>();
        transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = catchMouse;
        gameObject.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = eye[1];
        originPos = transform.GetChild(1).transform.position;
        gameObject.transform.GetChild(1).transform.position = new Vector3(gameObject.transform.GetChild(1).transform.position.x, gameObject.transform.GetChild(1).transform.position.y - 0.2f, gameObject.transform.GetChild(1).transform.position.z);
        AudioManager.Instance.PlaySound(check.effectSound, transform.position);
        yield return new WaitForSeconds(1.0f);
        GetComponent<Animator>().enabled = true;
        transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = move[0];
        gameObject.transform.GetChild(1).transform.position = new Vector3(originPos.x, originPos.y, originPos.z);
        huntingMouse = true;
        CanCollect = true;
        touch = true;
        yield break;
    }
}