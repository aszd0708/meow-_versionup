﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BabyCat : UsingItemCat, I_OtherStageAnimal
{
    public float shakeAmount_X;
    public float shakeAmount_Z;

    public float stopTime;
    public float vibeTime;

    public Sprite firstBody;
    public Sprite firstEye;

    public float bigCatSpeed;
    public bool show;
    public ThirdClick trigger;
    public bool up;
    public GameObject collectEffect;
    // Start is called before the first frame update
    [Header("다른 스테이지에 사용할 컴포넌트")]
    public BabyCatUI babyCatUI;
    void Start()
    {
        up = false;
        StartCoroutine("Moving");
        show = false;
    }
    
    private void VibeBody()
    {
        iTween.ShakePosition(gameObject, iTween.Hash(
                    "x", shakeAmount_X,
                    "time", vibeTime));

        iTween.ShakeRotation(gameObject, iTween.Hash(
            "z", shakeAmount_Z,
            "time", vibeTime, "oncomplete", "PosZ"));
    }

    private void PosZ()
    {
        if(up)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, -3.0f);
            GameObject effect = Instantiate(collectEffect);
            effect.transform.position = new Vector3(transform.position.x - 0.1f, transform.position.y - 1f, effect.transform.position.z);
            effect.GetComponent<EffectMotion>().Motion();
            show = true;
            CanCollect = true;
        }
    }


    public override void UseItem(GameObject item)
    {
        for (int i = 0; i < gameObject.transform.childCount; i++)
            transform.GetChild(i).GetComponent<SpriteRenderer>().sortingOrder = 2;
        show = true;
        transform.position = new Vector3(transform.position.x, transform.position.y, 0);
    }

    public void Collect()
    {
        for (int i = 0; i < gameObject.transform.childCount; i++)
            transform.GetChild(i).GetComponent<SpriteRenderer>().sortingOrder = 2;
        show = true;
        transform.position = new Vector3(transform.position.x, transform.position.y, 0);
    }

    public void OnClick()
    {
        StartCoroutine(Gone());
    }

    private IEnumerator Gone()
    {
        GetComponent<CircleCollider2D>().enabled = false;
        for (int i = 0; i < transform.childCount; i++)
            transform.GetChild(i).GetComponent<SpriteRenderer>().enabled = false;
        yield return new WaitForSeconds(2.0f);
        GetComponent<CircleCollider2D>().enabled = true;
        for (int i = 0; i < transform.childCount; i++)
            transform.GetChild(i).GetComponent<SpriteRenderer>().enabled = true;
    }

    private IEnumerator Moving()
    {
        while (true)
        {
            transform.GetChild(2).GetComponent<SpriteRenderer>().enabled = true;
            gameObject.GetComponent<Animator>().enabled = false;
            transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = firstBody;
            transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = firstEye;
            VibeBody();
            yield return new WaitForSeconds(vibeTime);
            gameObject.GetComponent<Animator>().enabled = true;
            transform.GetChild(2).GetComponent<SpriteRenderer>().enabled = false;
            yield return new WaitForSeconds(stopTime);
        }
    }

    public void OhterStageAnimalOnClick()
    {
        OnClick();
        babyCatUI.OnClick();
    }
}
