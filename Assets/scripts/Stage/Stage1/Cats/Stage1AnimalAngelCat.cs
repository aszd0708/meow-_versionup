﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage1AnimalAngelCat : StageAnimalObjectBase
{
    [Header("왼쪽 날개")]
    public Transform Lwing;
    [Header("오른쪽 날개")]
    public Transform Rwing;

    [Header("날개 움직이는 시간")]
    public float wingMotionTime;
    [Header("몸이 위 아래로 움직이는 시간")]
    public float moveMotionTime = 1.5f;

    [Header("위 아래로 움직이는 거리")]
    public float moveRange = 1.0f;

    private bool canTouch = false;

    public override bool DoTouch(Vector2 touchPose = default)
    {
        if (canTouch)
            SetCollect();

        AudioManager.Instance.PlaySound("cat_6", transform.position);
        return false;
    }

    /// <summary>
    /// 모션 켜준 뒤 터치 할 수 있게 해줌
    /// </summary>
    public void StartMotions()
    {
        StartCoroutine(_StartMotions());
    }

    private IEnumerator _StartMotions()
    {
        StartCoroutine(_WingMotion());
        transform.DOMoveY(14, 1.0f).SetEase(Ease.OutCubic);
        yield return new WaitForSeconds(1.0f);
        canTouch = true;
        StartCoroutine(_MoveMotion());
        yield break;
    }

    private IEnumerator _WingMotion()
    {
        Vector3 downVector = new Vector3(0, 0, 10);
        Vector3 upVector3 = new Vector3(0, 0, 3);
        WaitForSeconds wingWait = new WaitForSeconds(wingMotionTime);

        while (true)
        {
            Lwing.transform.DORotate(downVector, wingMotionTime);
            Rwing.transform.DORotate(downVector * -1, wingMotionTime);
            yield return wingWait;

            Lwing.transform.DORotate(upVector3 * -1, wingMotionTime);
            Rwing.transform.DORotate(upVector3, wingMotionTime);
            yield return wingWait;
        }
    }

    private IEnumerator _MoveMotion()
    {
        Vector3 originPos = transform.localPosition;
        WaitForSeconds wait = new WaitForSeconds(moveMotionTime);
        while (true)
        {
            transform.DOLocalMoveY(transform.localPosition.y + moveRange, moveMotionTime).SetEase(Ease.OutQuad);
            yield return wait;
            transform.DOLocalMoveY(originPos.y, moveMotionTime).SetEase(Ease.OutQuad);
            yield return wait;
        }
    }
}
