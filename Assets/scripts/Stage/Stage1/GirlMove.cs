﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GirlMove : MonoBehaviour
{
    public enum State
    {
        walk, lookSky, lookGround, kickGround
    }

    public float walkTime;
    public float breakTime;
    public float distance;

    public State state;
    void Start()
    {
        StartCoroutine("DoSomthing");
    }

    private IEnumerator DoSomthing()
    {
        while (true)
        {
            state = (State)Random.Range((float)State.walk, (float)State.kickGround + 1);

            iTween.MoveBy(gameObject, iTween.Hash("x", distance, "time", walkTime, "islocal", true, "movetopath", false, "easeType", iTween.EaseType.linear));
            yield return new WaitForSeconds(walkTime);
            gameObject.GetComponent<Animator>().enabled = false;
            gameObject.transform.GetChild(2).gameObject.transform.localRotation = Quaternion.Euler(0, 0, 30);
            gameObject.transform.GetChild(3).gameObject.transform.localRotation = Quaternion.Euler(0, 0, -30);

            switch (state)
            {
                case State.lookSky:
                    iTween.RotateTo(gameObject.transform.GetChild(0).gameObject, iTween.Hash("z", -30, "time", breakTime / 3, "easeType", iTween.EaseType.linear));
                    yield return new WaitForSeconds(breakTime / 3);
                    yield return new WaitForSeconds(breakTime / 3);
                    iTween.RotateTo(gameObject.transform.GetChild(0).gameObject, iTween.Hash("z", 0, "time", breakTime / 3, "easeType", iTween.EaseType.linear));
                    yield return new WaitForSeconds(breakTime / 3);
                    break;

                case State.lookGround:
                    iTween.RotateTo(gameObject.transform.GetChild(0).gameObject, iTween.Hash("z", 30, "time", breakTime / 3, "easeType", iTween.EaseType.linear));
                    yield return new WaitForSeconds(breakTime / 3);
                    yield return new WaitForSeconds(breakTime / 3);
                    iTween.RotateTo(gameObject.transform.GetChild(0).gameObject, iTween.Hash("z", 0, "time", breakTime / 3, "easeType", iTween.EaseType.linear));
                    yield return new WaitForSeconds(breakTime / 3);
                    break;

                case State.kickGround:
                    yield return new WaitForSeconds(0.5f);
                    iTween.RotateTo(gameObject.transform.GetChild(2).gameObject, iTween.Hash("z", 45, "time", breakTime / 10, "easeType", iTween.EaseType.easeOutCubic));
                    yield return new WaitForSeconds(breakTime / 10);
                    yield return new WaitForSeconds(breakTime / 5);
                    iTween.RotateTo(gameObject.transform.GetChild(2).gameObject, iTween.Hash("z", -10, "time", (breakTime / 5), "easeType", iTween.EaseType.easeOutCubic));
                    yield return new WaitForSeconds((breakTime / 5));
                    iTween.RotateTo(gameObject.transform.GetChild(2).gameObject, iTween.Hash("z", 30, "time", (breakTime / 5) * 2, "easeType", iTween.EaseType.linear));
                    yield return new WaitForSeconds((breakTime / 5) * 2);
                    break;

            }
            yield return new WaitForSeconds(0.5f);
            gameObject.GetComponent<Animator>().enabled = true;
            if (gameObject.transform.rotation.y != 0)
            {
                //gameObject.transform.GetChild(2).transform.position = new Vector3(gameObject.transform.GetChild(2).transform.position.x, gameObject.transform.GetChild(2).transform.position.y, -1);
                gameObject.transform.rotation = Quaternion.Euler(0, 0, 0);
            }

            else if (gameObject.transform.rotation.y == 0)
            {
                //gameObject.transform.GetChild(2).transform.position = new Vector3(gameObject.transform.GetChild(2).transform.position.x, gameObject.transform.GetChild(2).transform.position.y, 1);
                gameObject.transform.rotation = Quaternion.Euler(0, 180, 0);
            }
        }
    }
}
