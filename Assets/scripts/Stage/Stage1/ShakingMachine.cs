﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ShakingMachine : MonoBehaviour, TouchableObj
{
    [SerializeField]
    [Header("움직일 앞부분")]
    private Transform front;
    private bool touch = true;

    [SerializeField]
    [Header("한번 움직이는 시간")]
    private float time;
    [SerializeField]
    [Header("총 몇번 움직이는지")]
    private int shakingCount;

    public bool DoTouch(Vector2 touchPose = default)
    {
        if(touch)
        {
            StartCoroutine(_ShakeMachine());
        }
        return false;
    }

    private IEnumerator _ShakeMachine()
    {
        touch = false;
        WaitForSeconds wait = new WaitForSeconds(time);
        float shakeAngle;
        float min = -1;
        for (int i = 0; i < shakingCount; i++)
        {
            shakeAngle = (shakingCount - i - 1) * 5 * min;
            min *= -1;
            front.DOLocalRotate(new Vector3(0, 0, shakeAngle), time).SetEase(Ease.InOutQuad);
            yield return wait;
        }
        touch = true;
        yield break;
    }

}
