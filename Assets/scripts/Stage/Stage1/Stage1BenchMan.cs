﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage1BenchMan : MonoBehaviour, TouchableObj
{
    public float speed = 0.1f;

    private bool touch = false;

    private WaitForSeconds wait;

    private void Start()
    {
        wait = new WaitForSeconds(speed);
    }

    private IEnumerator _PlayAction()
    {
        touch = true;
        AudioManager.Instance.PlaySound(new string[] { "hu_1", "hu_2", "hu_3" }, transform.position, null, Random.Range(0.9f, 1.0f));
        iTween.RotateTo(gameObject, iTween.Hash("z", -10, "time", speed, "easeType", iTween.EaseType.linear));
        iTween.RotateTo(gameObject, iTween.Hash("z", 0, "time", speed, "delay", speed, "easeType", iTween.EaseType.linear));
        yield return wait;
        touch = false;
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        if (!touch)
        {
            StartCoroutine(_PlayAction());
            AudioManager.Instance.PlaySound("hu_2", transform.position);
        }
        return false;
    }
}
