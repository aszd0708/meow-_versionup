﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchSpriteChange : MonoBehaviour, I_NonColObject
{
    public enum Kind { NORMAL, RANDOM }
    public Kind KIND;

    private SpriteRenderer spriteRenderer;

    public List<Sprite> sprite = new List<Sprite>();

    private int index = 0;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        switch(KIND)
        {
            case Kind.NORMAL: break;

            case Kind.RANDOM:
                int num = Random.Range(0, sprite.Count);
                int sight = Random.Range(0, 2);
                if (sight == 0)
                    transform.rotation = Quaternion.Euler(0, 180, 0);
                else
                    transform.rotation = Quaternion.Euler(0, 0, 0);

                spriteRenderer.sprite = sprite[num];
                break;

            default: break;
        }
        OnClick();
    }

    public void OnClick()
    {
        spriteRenderer.sprite = sprite[index++];
        index %= sprite.Count;
    }
}
