﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShotEffect : MonoBehaviour
{
    public float animTime = 1.0f;

    private readonly float outStart = 0.0f;
    private readonly float outEnd = 1.0f;
    private readonly float inStart = 1.0f;
    private readonly float inEnd = 0.0f;

    private float time = 0.0f;

    // Start is called before the first frame update
    private Image img;

    private void Awake()
    {
        img = GetComponent<Image>();
    }

    public void Shot()
    {
        img.enabled = true;
        StartCoroutine(FadeOut());
    }

    public void EndShot()
    {
        img.enabled = false;
    }

    private IEnumerator FadeIn()
    {
        Color color = GetComponent<Image>().color;
        time = 0.0f;
        color.a = Mathf.Lerp(inStart, inEnd, time);

        while (color.a > 0.0f)
        {
            time += Time.deltaTime / (animTime*1.5f);

            color.a = Mathf.Lerp(inStart, inEnd, time);

            GetComponent<Image>().color = color;
            yield return null;
        }
        GetComponent<Image>().enabled = false;
        yield return null;
    }

    private IEnumerator FadeOut()
    {
        GetComponent<Image>().enabled = true;
        Color color = GetComponent<Image>().color;
        time = 0.0f;
        color.a = Mathf.Lerp(outStart, outEnd, time);

        while (color.a < 1.0f)
        {
            time += Time.deltaTime / animTime;

            color.a = Mathf.Lerp(outStart, outEnd, time);

            GetComponent<Image>().color = color;
            yield return null;
        }
        StartCoroutine("FadeIn");
        yield return null;
    }
}
