﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TouchTimerChecker : MonoBehaviour
{
    public TouchInterationCam touch;
    public TouchManager touchManager;

    void Start()
    {
        GetComponent<Text>().text = (touch.checkTime * -1f).ToString();
    }

    public void UpTimer()
    {
        touch.checkTime -= 0.1f;
        touchManager.checkTime -= 0.1f;
        GetComponent<Text>().text = (touch.checkTime * -1f).ToString();
        GetComponent<Text>().text = (touchManager.checkTime * -1f).ToString();
    }

    public void DownTimer()
    {
        if (touch.checkTime >= -0.01f)
        {
            touch.checkTime = -0.01f;
            return;
        }
        //if(touch.checkTime >= -0.1)
        touch.checkTime += 0.1f;
        GetComponent<Text>().text = (touch.checkTime * -1f).ToString();
    }

    public void SmallUpTimer()
    {
        touch.checkTime -= 0.01f;
        GetComponent<Text>().text = (touch.checkTime * -1f).ToString();
    }

    public void SmallDownTimer()
    {
        if (touch.checkTime >= -0.01f)
        {
            touch.checkTime = -0.01f;
            return;
        }
        touch.checkTime += 0.01f;
        GetComponent<Text>().text = (touch.checkTime * -1f).ToString();
    }
}
