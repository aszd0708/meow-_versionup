﻿using BitBenderGames;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinCatMotion : MonoBehaviour
{
    public Vector3[] pos = new Vector3[2];
    public bool col;
    public GameObject coin;
    public JsonCatExplanation data;
    public CatSaveLoad catCol;

    [Header("날아가는 시간")]
    public float flyTime;

    // 여기서 부터 모션에 사용될 변수들
    private SpriteRenderer sprite;
    public Sprite[] motionSprite = new Sprite[2];
    [Header("이동시 몇번 움직일지 정함 최대 최소")]
    public int[] motionCountIndex = new int[2];

    [Header("동전들이 사용이 끝났을때 가게 되는 부모")]
    public Transform trash;

    [Header("클릭 했을 시 동전++ 해주는 컴포넌트")]
    public MadCatControllor MCC;


    // 코루틴 
    private Coroutine catMoveCor, coinMotion, goPosition, catMotion;
    // 여기까지

    private CatSaveLoad.JsonCat catLoad;
    private Vector3 originePos;
    private MadCatCheck check;

    private MobileTouchCamera mainCam;
    private float originZoom;

    void Awake()
    {
        mainCam = FindObjectOfType<MobileTouchCamera>();
        check = GetComponent<MadCatCheck>();
        sprite = GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        originZoom = mainCam.CamZoomMax;
        //col = false;
        if (!EncryptedPlayerPrefs.HasKey("CoinCatCol"))
            EncryptedPlayerPrefs.SetInt("CoinCatCol", 0);
        SetCol();

        originePos = transform.position;
        AnimalText text = GetComponent<AnimalText>();
        List<JsonCatExplanation.Explanation> explanations = data.LoadCats();
        text.name = explanations[check.no - 1].Name;
        text.photoText = explanations[check.no - 1].HashTagText();
        text.instarURL = explanations[check.no - 1].InstarURL();
    }

    public Vector2 RandomPos()
    {
        return new Vector2(Random.Range(pos[0].x, pos[1].x), Random.Range(pos[0].y, pos[1].y));
    }

    public void MotionStarter()
    {
        transform.position = RandomPos();
        catMoveCor = StartCoroutine(CatMove());
    }

    public void SetCol()
    {
        if (EncryptedPlayerPrefs.GetInt("CoinCatCol") == 0)
            col = false;
        else
            col = true;
    }

    public void SaveCol(int TF)
    {
        EncryptedPlayerPrefs.SetInt("CoinCatCol", TF);
    }

    public void OnClick()
    {
        if (!check.collect)
        {
            CoinMotionStart();
            SetCollect();
        }

        else
        {
            // 여기에 이제 그거 넣기 코인 한개 얻었다는 UI판넬
            StartAllMotion();
        }
    }

    private void StartAllMotion()
    {
        CoinMotionStart();
        StartCoroutine(_ZoomMotion());
    }

    private IEnumerator _ZoomMotion()
    {
        mainCam.transform.DOMove(new Vector3(transform.position.x, transform.position.y, mainCam.transform.position.z), 0.5f);
        yield return StartCoroutine(Zoom(mainCam.CamZoomMax, mainCam.CamZoomMin, 0.5f));
        yield return new WaitForSeconds(0.5f);
        MCC.MadCatTouch();

        if(catMotion != null)
            StopCoroutine(catMotion);
        if(catMoveCor != null)
            StopCoroutine(catMoveCor);
        if (goPosition != null)
            StopCoroutine(goPosition);

        for (int i = 0; i < createCoin.Length; i++)
            StartCoroutine(CoinDestroy(createCoin[i]));
        GoPositionStarter(); SetCol();
        mainCam.CamZoomMax = originZoom;
    }

    private IEnumerator Zoom(float max, float min, float animeTime)
    {
        MobileTouchCamera touchCam = mainCam;
        float zoomMax = max, zoomMin = min;
        float time = 0.0f;

        while (zoomMax > zoomMin)
        {
            time += Time.deltaTime / animeTime;

            zoomMax = Mathf.Lerp(max, min, time);

            touchCam.CamZoomMax = zoomMax;
            touchCam.ResetCameraBoundaries();
            yield return null;
        }
    }

    public void CoinMotionStart()
    {
        StartCoroutine(CoinMotion());
    }
    
    [Header("동전 겟수")]
    public int count = 30;
    private GameObject[] createCoin;

    public IEnumerator CoinMotion()
    {
        transform.DOPause();
        float anlge =  count;
        trash.position = transform.position;
        createCoin = new GameObject[count];
        for (int i = 0; i < createCoin.Length; i++)
        {
            createCoin[i] = Instantiate(coin);
            createCoin[i].transform.SetParent(transform);
            createCoin[i].transform.localPosition = Vector2.zero;
            createCoin[i].transform.DOLocalJump(new Vector3(Random.Range(-2f, 2f), Random.Range(-0.5f, 0.5f)), Random.Range(1, 2), Random.Range(1, 4), Random.Range(0.5f, 1.0f));
        }
        yield break;
    }

    public IEnumerator CoinDestroy(GameObject coin)
    {
        int count = 10;
        coin.transform.SetParent(trash);
        SpriteRenderer coinSprite = coin.GetComponent<SpriteRenderer>();
        // 코인이 사라지기 기다리는 중....
        yield return new WaitForSeconds(2.5f);
        // 동전이 깜빡깜빡 거리다가 사라지기
        for (int i = 0; i < count; i++)
        {
            coinSprite.enabled = false;
            yield return new WaitForSeconds(0.1f);
            coinSprite.enabled = true;
            yield return new WaitForSeconds(0.1f);
        }
        // 약 2초걸림
        coin.SetActive(false);
        yield break;
    }

    public void SetCollect()
    {
        col = true;
        SaveCol(1);
        SetCol();
    }

    private Vector2 AnlgeToDir(float angle)
    {
        Vector2 dir;
        float x = Mathf.Sin(angle);
        float y = Mathf.Cos(angle);
        y = Mathf.Abs(y);
        dir = new Vector2(x, y);
        return dir;
    }

    private void GoPositionStarter()
    {
        catMotion = StartCoroutine(GoPosition());
    }

    private IEnumerator GoPosition()
    {
        transform.DORotate(new Vector3(0, 0, GetAngle(originePos) - 90), 0.1f);
        yield return new WaitForSeconds(1.0f);
        transform.DOMove(originePos, 0.5f);
        yield break;
    }

    private IEnumerator CatMove()
    {
        while (!col)
        {
            Vector2 randomPos = RandomPos();
            float randomTime = Random.Range(1.0f, 3.0f);
            transform.DOMove(randomPos, flyTime);
            catMotion = StartCoroutine(CatMotion(flyTime, randomPos));
            yield return new WaitForSeconds(randomTime + flyTime);
        }
        yield break;
    }

    // 삼각함수 기울기를 이용해서 방향을 구하긴 하는데 어렵다.... 
    // 생각보다 금방 끝내서 놀랐다
    private float GetAngle(Vector2 randomPos)   
    {
        float angle;

        angle = Mathf.Atan2(randomPos.y - transform.position.y,
                            randomPos.x - transform.position.x) * 180 / Mathf.PI;
        return angle;
    }

    // 고양이 모션 바꿔주는 함수
    private IEnumerator CatMotion(float randomTime, Vector2 randomPos)
    {
        transform.DORotate(new Vector3(0, 0, GetAngle(randomPos) - 90), 0.1f);
        yield return new WaitForSeconds(0.2f);
        //StartCoroutine(LookAt_Object_2DHoming(0.1f, randomPos));
        int randomCount = Random.Range(motionCountIndex[0], motionCountIndex[1]);
        float time = randomTime / randomCount;

        int index = 0;
        bool flip = false;
        float yAxis = 0;
        for (int i = 0; i < randomCount; i++)
        {
            sprite.sprite = motionSprite[index];
            sprite.flipX = flip;
            //transform.rotation = Quaternion.Euler(new Vector3(0, yAxis, transform.rotation.z));
            index++;
            index %= motionSprite.Length;
            yAxis += 180;
            yAxis %= 180;
            flip = !flip;
            yield return new WaitForSeconds(time);
        }
        yield break;
    }

    // 여긴 sendMessage로 전달할 함수 과연 sendMessage로 전달해야 효율적일까??
}
