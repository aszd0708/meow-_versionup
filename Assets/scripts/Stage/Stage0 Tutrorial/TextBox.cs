﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextBox : MonoBehaviour
{
    public class BoxInfo
    {
        Vector2 boxPos;
        bool flip;

        BoxInfo(Vector2 _boxPos, bool _flip)
        {
            boxPos = _boxPos;
            flip = _flip;
        }
    }

    public List<BoxInfo> boxInfo = new List<BoxInfo>();

    void Start()
    {
        // 여기에 좌표하고 불값 넣기
    }
}
