﻿using BitBenderGames;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialHelper : MonoBehaviour
{
    public GameObject cameraFunc, touch;
    public GameObject hand, textBox;
    public GameObject animal, item;
    public string[] text1, text2, text3, text4, text5, text6;

    public enum State
    {
        one, two, three, four, five, six
    }

    public enum ObjectState
    {
        cat, item, bag, list, nullItem
    }

    public ObjectState objectState;
    public State nowState;
    public bool complete;
    private int num;
    public Text helpText;
    private int i;
    private string[] nowText;
    
    void Start()
    {
        complete = true;
        i = 0;
        num = 1;
        nowState = State.one;
        objectState = ObjectState.cat;
        SetObjectState();
        SetText();
        StartText();
    }

    // Update is called once per frame
    void Update()
    {
        if (complete)
            StopCamera();

        else if (!complete)
            StartCamera();

        switch (helpText.GetComponent<TextFade>().state)
        {
            case TextFade.State.typing:
                if (Input.GetMouseButtonDown(0))
                    helpText.GetComponent<TextFade>().state = TextFade.State.end;
                break;

            case TextFade.State.end:
                if (Input.GetMouseButtonDown(0))
                {
                    if(complete)
                        StartText();
                }
                else if (nowText.Length == i)
                {
                    // 텍스트 박스 안보이게 함.
                    helpText.GetComponent<TextFade>().state = TextFade.State.end;
                    i = 0;
                    SetText();
                    complete = false;
                    StartCamera();
                }
                break;

            case TextFade.State.idle:
                break;
        }
    }

    public void StartText()
    {
        helpText = textBox.transform.GetChild(0).gameObject.GetComponent<Text>();
        helpText.GetComponent<TextFade>().SetText(nowText[i]);
        helpText.GetComponent<TextFade>().Fade();
        helpText.GetComponent<TextFade>().state = TextFade.State.typing;
        i++;
    }

    public void SetObjectState()
    {
        switch(objectState)
        {
            case ObjectState.cat:
                animal.GetComponent<BoxCollider2D>().enabled = true;
                item.GetComponent<CircleCollider2D>().enabled = false;
                break;
            case ObjectState.item:
                animal.GetComponent<BoxCollider2D>().enabled = false;
                item.GetComponent<CircleCollider2D>().enabled = true;
                break;

        }
    }

    private void SetText()
    {
        switch (nowState)
        {
            case State.one:
                nowText = text1;
                nowState = State.two;
                break;
            case State.two:
                nowText = text2;
                nowState = State.three;
                break;
            case State.three:
                nowText = text3;
                nowState = State.four;
                break;
            case State.four:
                nowText = text4;
                nowState = State.five;
                break;
            case State.five:
                nowText = text5;
                nowState = State.six;
                break;
            case State.six:
                nowText = text6;
                break;
        }
    }

    private void StopCamera()
    {
        cameraFunc.GetComponent<TouchInputController>().enabled = false;
        touch.GetComponent<TouchInterationCam>().enabled = false;
    }

    private void StartCamera()
    {
        cameraFunc.GetComponent<TouchInputController>().enabled = true;
        touch.GetComponent<TouchInterationCam>().enabled = true;
    }

    private void TextBoxMotion(bool UDState)
    {
        if(UDState)
        {
            textBox.GetComponent<RectTransform>().DOMoveY(textBox.GetComponent<RectTransform>().localPosition.y - 10.0f, 0.5f);
        }

        else if(!UDState)
        {
            textBox.GetComponent<RectTransform>().DOMoveY(textBox.GetComponent<RectTransform>().localPosition.y + 10.0f, 0.5f);
        }
    }
}
