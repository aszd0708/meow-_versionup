﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialCatButton : MonoBehaviour
{
    private bool click;
    private TutorialManager tutorialManager;
    // Start is called before the first frame update
    void Start()
    {
        click = true;
        tutorialManager = GameObject.Find("TutorialManager").GetComponent<TutorialManager>();
    }

    public void Onclick()
    {
        if (click)
        {
            tutorialManager.i++;
            click = false;
        }
        else
            return;
    }
}
