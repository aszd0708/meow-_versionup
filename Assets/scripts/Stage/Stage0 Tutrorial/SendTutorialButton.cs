﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SendTutorialButton : MonoBehaviour
{
    private Button catButton;
    private Button hand;
    // Start is called before the first frame update
    void Start()
    {
        catButton = GetComponent<Button>();
        hand = GameObject.Find("HandUI").GetComponent<Button>();
        SendButton();
    }

    private void SendButton()
    {
        hand.onClick.AddListener(GetComponent<GoCenter>().Center);
        hand.onClick.AddListener(GetComponent<TutorialCatButton>().Onclick);
        hand.onClick.AddListener(GetComponent<AudioSource>().Play);
    }
}
