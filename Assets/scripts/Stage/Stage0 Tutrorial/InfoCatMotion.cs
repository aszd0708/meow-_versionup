﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoCatMotion : MonoBehaviour
{
    public GameObject trigger;
    public Sprite walkMotion1;
    public Sprite walkMotion2;
    public bool collect;
    public Transform grace;
    public TutorialManager tutorial;
    private Sprite origin;
    private bool arrive;
    private Vector3 desPos;
    // Start is called before the first frame update
    void Start()
    {
        origin = gameObject.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite;
        collect = false;
    }

    public void MotionStarter()
    {
        desPos = trigger.transform.position;
        StartCoroutine("Motion");
    }

    private IEnumerator Motion()
    {
        transform.DOMove(new Vector3(desPos.x - 1, desPos.y+0.5f, 0), 1.0f);
        transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
        transform.GetChild(2).GetComponent<SpriteRenderer>().enabled = false;
        collect = true;
        for (int i = 0; i < 2; i++)
        {
            transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = walkMotion1;
            yield return new WaitForSeconds(0.25f);
            transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = walkMotion2;
            yield return new WaitForSeconds(0.25f);
        }
        tutorial.i++;
        transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = true;
        transform.GetChild(2).GetComponent<SpriteRenderer>().enabled = true;
        transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = origin;
    }

    public void TouchMotionStarter()
    {
        StartCoroutine("TouchMotion");
    }

    private IEnumerator TouchMotion()
    {
        transform.DOMove(new Vector3(grace.position.x, grace.position.y+0.7f, 0), 1.0f);
        transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
        transform.GetChild(2).GetComponent<SpriteRenderer>().enabled = false;
        for (int i = 0; i < 2; i++)
        {
            transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = walkMotion1;
            yield return new WaitForSeconds(0.25f);
            transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = walkMotion2;
            yield return new WaitForSeconds(0.25f);
        }
        transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = true;
        transform.GetChild(2).GetComponent<SpriteRenderer>().enabled = true;
        transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = origin;
        collect = true;
    }
}
