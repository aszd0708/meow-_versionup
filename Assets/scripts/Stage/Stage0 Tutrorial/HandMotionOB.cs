﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandMotionOB : MonoBehaviour
{
    public float distance;
    public float time;
    public float pickTime;
    public float size;
    public enum State { left, right, up, down, pick, idle }
    public State state;
    public Vector3[] handPosition;
    public Sprite[] pickSpirte = new Sprite[2];

    private bool move;
    private Sprite handSprite;
    private new SpriteRenderer renderer;
    // Start is called before the first frame update
    void Awake()
    {
        handPosition[0] = new Vector3(Camera.main.ScreenToWorldPoint(new Vector2(61.1f, -126f)).x, Camera.main.ScreenToWorldPoint(new Vector2(61.1f, -126f)).y, -5);
        renderer = GetComponent<SpriteRenderer>();
        handSprite = renderer.sprite;
    }

    void Start()
    {
        StartCoroutine(Motion());
        transform.position = new Vector3(1000, 1000, 1000);
    }

    public void MovePosition(Vector3 movePos)
    {
        if(!move)
        {
            gameObject.transform.position = movePos;
            move = true;
        }
    }

    public void MoveAway()
    {
        //GetComponent<SpriteRenderer>().enabled = false;
        transform.position = new Vector3(1000, 1000, 1000);
        move = false;
        Debug.Log("손 위치 초기화");
    }

    private IEnumerator Motion()
    {
        WaitForSeconds wait = new WaitForSeconds(time);
        while (true)
        {
            switch(state)
            {
                case State.left:
                    renderer.sprite = handSprite;
                    transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 90);
                    transform.DOScale(new Vector2(transform.localScale.x + size, transform.localScale.y - size), time);
                    yield return wait;
                    transform.DOLocalMoveX(transform.localPosition.x - distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size * 2, transform.localScale.y + size * 2), time);
                    yield return wait;
                    transform.DOScale(new Vector2(transform.localScale.x + size * 2, transform.localScale.y - size * 2), time);
                    yield return wait;
                    transform.DOLocalMoveX(transform.localPosition.x + distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size, transform.localScale.y + size), time);
                    yield return wait;
                    yield return wait;
                    break;
                case State.right:
                    renderer.sprite = handSprite;
                    transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 270);
                    transform.DOScale(new Vector2(transform.localScale.x + size, transform.localScale.y - size), time);
                    yield return wait;
                    transform.DOLocalMoveX(transform.localPosition.x + distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size * 2, transform.localScale.y + size * 2), time);
                    yield return wait;
                    transform.DOScale(new Vector2(transform.localScale.x + size * 2, transform.localScale.y - size * 2), time);
                    yield return wait;
                    transform.DOLocalMoveX(transform.localPosition.x - distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size, transform.localScale.y + size), time);
                    yield return wait;
                    yield return wait;
                    break;
                case State.up:
                    renderer.sprite = handSprite;
                    transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 0);
                    transform.DOScale(new Vector2(transform.localScale.x + size, transform.localScale.y - size), time);
                    yield return wait;
                    transform.DOLocalMoveY(transform.localPosition.y - distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size * 2, transform.localScale.y + size * 2), time);
                    yield return wait;
                    transform.DOScale(new Vector2(transform.localScale.x + size * 2, transform.localScale.y - size * 2), time);
                    yield return wait;
                    transform.DOLocalMoveY(transform.localPosition.y + distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size, transform.localScale.y + size), time);
                    yield return wait;
                    yield return wait;
                    break;
                case State.down:
                    renderer.sprite = handSprite;
                    transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 180);
                    transform.DOScale(new Vector2(transform.localScale.x + size, transform.localScale.y - size), time);
                    yield return wait;
                    transform.DOLocalMoveY(transform.localPosition.y - distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size * 2, transform.localScale.y + size * 2), time);
                    yield return wait;
                    transform.DOScale(new Vector2(transform.localScale.x + size * 2, transform.localScale.y - size * 2), time);
                    yield return wait;
                    transform.DOLocalMoveY(transform.localPosition.y + distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size, transform.localScale.y + size), time);
                    yield return wait;
                    yield return wait;
                    break;
                case State.pick:
                    renderer.sprite = pickSpirte[1];
                    yield return new WaitForSeconds(pickTime);
                    break;
                case State.idle:
                    yield return null;
                    break;
            }
        }
    }
}
