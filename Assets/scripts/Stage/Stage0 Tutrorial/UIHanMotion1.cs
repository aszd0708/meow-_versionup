﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class U : MonoBehaviour
{
    public float distance;
    public float time;
    public float size;

    public enum State { left, right, up, down, pick, idle}

    public State state;
    public Sprite[] handSprite = new Sprite[2];
    public Vector2 movePos;

    [HideInInspector]
    public bool hide;

    private new Image renderer;
    private bool start;
    void Awake()
    {
        start = true;
        hide = true;
        renderer = GetComponent<Image>();
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("Motion");
    }

    void Update()
    {
        if(!start && hide)
        {
            if (Input.GetMouseButtonDown(0))
                renderer.enabled = false;
            else if (Input.GetMouseButtonUp(0))
                renderer.enabled = true;
        }
    }

    public void Stop()
    {
        StopCoroutine("Motion");
    }

    public void End()
    {
        gameObject.SetActive(false);
    }

    public void GoItemPos()
    {
        if (!start)
            return;
        start = false;
        renderer.enabled = true;
        GetComponent<RectTransform>().anchoredPosition = movePos;
        Debug.Log("시작함");
        GetComponent<RectTransform>().localScale = new Vector3(1, 1, 0);
        renderer.sprite = handSprite[1];
        transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 0);
        return;
    }

    private IEnumerator Motion()
    {
        WaitForSeconds wait = new WaitForSeconds(time);
        while (true)
        {
            switch(state)
            {
                case State.left:
                    renderer.sprite = handSprite[0];
                    transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 90);
                    transform.DOScale(new Vector2(transform.localScale.x + size, transform.localScale.y - size), time);
                    yield return wait;
                    transform.DOLocalMoveX(transform.localPosition.x - distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size * 2, transform.localScale.y + size * 2), time);
                    yield return wait;
                    transform.DOScale(new Vector2(transform.localScale.x + size * 2, transform.localScale.y - size * 2), time);
                    yield return wait;
                    transform.DOLocalMoveX(transform.localPosition.x + distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size, transform.localScale.y + size), time);
                    yield return wait;
                    break;
                case State.right:
                    renderer.sprite = handSprite[0];
                    transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 270);
                    transform.DOScale(new Vector2(transform.localScale.x + size, transform.localScale.y - size), time);
                    yield return wait;
                    transform.DOLocalMoveX(transform.localPosition.x + distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size * 2, transform.localScale.y + size * 2), time);
                    yield return wait;
                    transform.DOScale(new Vector2(transform.localScale.x + size * 2, transform.localScale.y - size * 2), time);
                    yield return wait;
                    transform.DOLocalMoveX(transform.localPosition.x - distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size, transform.localScale.y + size), time);
                    yield return wait;
                    break;
                case State.up:
                    renderer.sprite = handSprite[0];
                    transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 0);
                    transform.DOScale(new Vector2(transform.localScale.x + size, transform.localScale.y - size), time);
                    yield return wait;
                    transform.DOLocalMoveY(transform.localPosition.y - distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size * 2, transform.localScale.y + size * 2), time);
                    yield return wait;
                    transform.DOScale(new Vector2(transform.localScale.x + size * 2, transform.localScale.y - size * 2), time);
                    yield return wait;
                    transform.DOLocalMoveY(transform.localPosition.y + distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size, transform.localScale.y + size), time);
                    yield return wait;
                    break;
                case State.down:
                    renderer.sprite = handSprite[0];
                    transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 180);
                    transform.DOScale(new Vector2(transform.localScale.x + size, transform.localScale.y - size), time);
                    yield return wait;
                    transform.DOLocalMoveY(transform.localPosition.y - distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size * 2, transform.localScale.y + size * 2), time);
                    yield return wait;
                    transform.DOScale(new Vector2(transform.localScale.x + size * 2, transform.localScale.y - size * 2), time);
                    yield return wait;
                    transform.DOLocalMoveY(transform.localPosition.y + distance, time * 2);
                    transform.DOScale(new Vector2(transform.localScale.x - size, transform.localScale.y + size), time);
                    yield return wait;
                    break;
                case State.pick:
                    yield return null;
                    break;
                case State.idle:
                    yield return null;
                    break;
            }
            if (!start)
                break;
        }
    }

    private IEnumerator MoveLR()
    {
        transform.DOScale(new Vector2(GetComponent<RectTransform>().localScale.x + size, GetComponent<RectTransform>().localScale.y - size), time);
        yield return new WaitForSeconds(time);
        transform.DOLocalMoveY(GetComponent<RectTransform>().anchoredPosition.x - distance, time * 2);
        transform.DOScale(new Vector2(GetComponent<RectTransform>().localScale.x - size * 2, GetComponent<RectTransform>().localScale.y + size * 2), time);
        yield return new WaitForSeconds(time);
        transform.DOScale(new Vector2(GetComponent<RectTransform>().localScale.x + size * 2, GetComponent<RectTransform>().localScale.y - size * 2), time);
        yield return new WaitForSeconds(time);
        transform.DOLocalMoveY(GetComponent<RectTransform>().anchoredPosition.x + distance, time * 2);
        transform.DOScale(new Vector2(GetComponent<RectTransform>().localScale.x - size, GetComponent<RectTransform>().localScale.y + size), time);
        yield return new WaitForSeconds(time);
        yield return new WaitForSeconds(time);
    }

    private IEnumerator MoveUD()
    {
        transform.DOScale(new Vector2(GetComponent<RectTransform>().localScale.x + size, GetComponent<RectTransform>().localScale.y - size), time);
        yield return new WaitForSeconds(time);
        transform.DOLocalMoveY(GetComponent<RectTransform>().anchoredPosition.y - distance, time * 2);
        transform.DOScale(new Vector2(GetComponent<RectTransform>().localScale.x - size * 2, GetComponent<RectTransform>().localScale.y + size * 2), time);
        yield return new WaitForSeconds(time);
        transform.DOScale(new Vector2(GetComponent<RectTransform>().localScale.x + size * 2, GetComponent<RectTransform>().localScale.y - size * 2), time);
        yield return new WaitForSeconds(time);
        transform.DOLocalMoveY(GetComponent<RectTransform>().anchoredPosition.y + distance, time * 2);
        transform.DOScale(new Vector2(GetComponent<RectTransform>().localScale.x - size, GetComponent<RectTransform>().localScale.y + size), time);
        yield return new WaitForSeconds(time);
        yield return new WaitForSeconds(time);
    }
}
