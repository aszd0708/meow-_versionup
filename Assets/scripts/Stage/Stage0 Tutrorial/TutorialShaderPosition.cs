﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialShaderPosition : MonoBehaviour
{
    public Vector2[] anchoredPos_1;
    public Vector2[] sizeDelta_1;

    public Vector2[] anchoredPos_2;
    public Vector2[] sizeDelta_2;

    public Vector2[] anchoredPos_3;
    public Vector2[] sizeDelta_3;

    public Vector2[] anchoredPos_4;
    public Vector2[] sizeDelta_4;

    public Vector2[] anchoredPos_5;
    public Vector2[] sizeDelta_5;
}
