﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchMarkPosition : MonoBehaviour
{
    public Transform hand;

    void Update()
    {
        switch(hand.GetComponent<HandMotionOB>().state)
        {
            case HandMotionOB.State.left:
                transform.position = new Vector3(hand.position.x + 4, hand.position.y, hand.position.z);
                break;
            case HandMotionOB.State.right:
                transform.position = new Vector3(hand.position.x - 4, hand.position.y, hand.position.z);
                break;
            case HandMotionOB.State.up:
                transform.position = new Vector3(hand.position.x, hand.position.y - 2, hand.position.z);
                break;
            case HandMotionOB.State.down:
                transform.position = new Vector3(hand.position.x, hand.position.y + 2, hand.position.z);
                break;
            case HandMotionOB.State.idle:
                transform.position = new Vector3(hand.position.x, hand.position.y, hand.position.z);
                break;
        }
    }
}
