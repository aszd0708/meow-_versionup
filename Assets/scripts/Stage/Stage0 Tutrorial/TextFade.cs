﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextFade : MonoBehaviour
{
    public enum State
    {
        typing, end, idle
    }

    public State state;
    [SerializeField] private Text textToUse;
    [SerializeField] private bool useThisText = false;
    [TextAreaAttribute(4, 15)]
    [SerializeField] private string textToShow;
    [SerializeField] private float fadeSpeedMultiplier = 0.25f;
    private float colorFloat = 0.1f;
    private int colorInt;
    private int letterCounter = 0;
    private string shownText;

    private void Start()
    {
        if (useThisText)
        {
            textToUse = GetComponent<Text>();
        }
    }
    private IEnumerator FadeInText()
    {
        state = State.typing;
        while (letterCounter < textToShow.Length)
        {
            if (colorFloat < 1.0f)
            {
                colorFloat += Time.deltaTime * fadeSpeedMultiplier;
                colorInt = (int)(Mathf.Lerp(0.0f, 1.0f, colorFloat) * 255.0f);
                textToUse.text = shownText + "<color=\"#376189" + string.Format("{0:X}", colorInt) + "\">" + textToShow[letterCounter] + "</color>";
            }
            else
            {
                colorFloat = 0.1f;
                shownText += textToShow[letterCounter];
                letterCounter++;
            }

            if (state == State.end)
            {
                textToUse.text = textToShow;
                state = State.idle;
                break;
            }
            yield return null;
        }
        state = State.end;
        textToShow = "";
        shownText = "";
        letterCounter = 0;
    }

    public void SetText(string text)
    {
        textToShow = text;
    }

    public void Fade()
    {
        StartCoroutine(FadeInText());
    }
}
