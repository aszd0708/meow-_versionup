﻿using DG.Tweening;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using BitBenderGames;

public class TutorialManager : MonoBehaviour
{
    public TutorialShaderPosition shaderPos;
    public TouchInputController cam;
    public Stage1SaveLoad clear;
    public FadeInOut fade;
    public TouchInterationCam interCam;
    public GameObject handHelper;
    public GameObject scriptBox;
    public Text text;
    public float motionTime;
    public string[] script;
    public float breakTime;
    public GameObject handUI;
    public RectTransform lastButton;
    public GameObject cat;
    public GameObject item;

    // 처음 튜토리얼엔 기회 20으로 고정시키기
    public Timer chance;

    private HandMotionOB handMotion;
    private Vector2 originScale;
    private Vector2 originScale_T;
    private int num;
    private bool textMotion;
    private bool findObject;
    private bool shaderMove;
    private bool showLast;
    // shader 포지션
    private Vector2[] anchoredPos;
    private Vector2[] sizeDelta;

    private bool hanUIMotion;
    // Start is called before the first frame update

    // 튜토리얼 체크 해주는 코루틴과 i
    private Coroutine tutorialCoroutine;
    public int i = 0;

    [Header("SkipButton")]
    public GameObject skipObject;
    private Image skipImg;
    private Button skipBtn;
    public float skipCreateTime;

    void Awake()
    {
        clear.ResetSave();
        // 스킵버튼 변수 초기화
        skipImg = skipObject.GetComponent<Image>();
        skipBtn = skipObject.GetComponent<Button>();
    }

    void Start()
    {
        chance.GiveChance();
        InputText();
        shaderMove = false;
        findObject = false;
        showLast = false;
        hanUIMotion = true;
        clear.ResetSave();
        num = 0;
        handMotion = handHelper.GetComponent<HandMotionOB>();
        originScale = scriptBox.GetComponent<RectTransform>().sizeDelta;
        originScale_T = scriptBox.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta;
        scriptBox.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 0);
        lastButton.localScale = new Vector2(0, 0);
        textMotion = false;
        text.enabled = false;
        i = 0;
        StartCoroutine(SkipButtonState(false));
        Debug.Log("시작 함?");
        tutorialCoroutine = StartCoroutine(Tutorial());
    }

    private void Update()
    {
        cam.enabled = false;
    }

    //  터치해서 움직이는 아이템 변수
    private GameObject itemSprite = null;
    

    private IEnumerator Tutorial()
    {
        int num = 0;
        while (true)
        {
            cam.enabled = false;
            switch (i)
            {
                case 0:
                    // 처음 시작했을때 기다림
                    interCam.enabled = false;
                    handMotion.MoveAway();
                    handMotion.state = HandMotionOB.State.idle;
                    //handMotion.MovePosition(handMotion.handPosition[0]);
                    if(!shaderMove)
                    {
                        anchoredPos = shaderPos.anchoredPos_1;
                        sizeDelta = shaderPos.sizeDelta_1;
                        MoveShader();
                    }
                    break;
                case 1:
                    // 처음 고양이 버튼 터치 했을때 텍스트 박스
                    handUI.GetComponent<Image>().enabled = false;
                    handUI.GetComponent<UIHanMotion>().Stop();
                    handMotion.state = HandMotionOB.State.idle;
                    handMotion.MoveAway();

                    if (shaderMove)
                        GoAwayShaders();
                    yield return new WaitForSeconds(1.5f);
                    text.text = script[0];

                    if (!textMotion)
                    {
                        StartCoroutine(TextBoxAppear(new Vector2(450, 220), skipCreateTime));
                    }
                    break;

                case 2:
                    // 고양이 터치하라는 손짓
                    interCam.enabled = true;
                    handMotion.state = HandMotionOB.State.down;
                    handMotion.MovePosition(handMotion.handPosition[1]);
                    cat.GetComponent<BoxCollider2D>().enabled = true;
                    if (!shaderMove)
                    {
                        anchoredPos = shaderPos.anchoredPos_2;
                        sizeDelta = shaderPos.sizeDelta_2;
                        MoveShader();
                    }
                    break;

                case 3:
                    // 터치 후 말풍선 나옴
                    interCam.enabled = false;
                    handMotion.state = HandMotionOB.State.idle;
                    handMotion.MoveAway();
                    text.text = script[1];

                    if (shaderMove)
                        GoAwayShaders();

                    if (!textMotion)
                    {
                        StartCoroutine(TextBoxAppear(new Vector2(-168, 17), skipCreateTime));
                        // yield return new WaitForSeconds(breakTime);
                        // NextText();
                    }
                    break;

                case 4:
                    // 화면 아무대나 터치 하라는 손짓
                    interCam.enabled = true;
                    handMotion.state = HandMotionOB.State.up;
                    handMotion.MovePosition(handMotion.handPosition[2]);

                    if (!shaderMove)
                    {
                        anchoredPos = shaderPos.anchoredPos_3;
                        sizeDelta = shaderPos.sizeDelta_3;
                        MoveShader();
                    }
                    break;

                case 5:
                    // 5~6 터치 후 나오는 말풍선
                    interCam.enabled = false;
                    handMotion.state = HandMotionOB.State.idle;
                    handMotion.MoveAway();
                    text.text = script[2];

                    if (shaderMove)
                        GoAwayShaders();

                    if (!textMotion)
                    {
                        StartCoroutine(TextBoxAppear(new Vector2(-466, 183), skipCreateTime + breakTime));
                        SetSkipButtonPos(true);
                        scriptBox.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 180);
                        scriptBox.transform.GetChild(0).GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 180);
                        
                        yield return new WaitForSeconds(breakTime);
                        textMotion = false;
                        i++;
                    }
                    break;

                case 6:
                    text.text = script[3];
                    if (!textMotion)
                    {
                        Debug.Log("진입 함");
                        //StartCoroutine("TextBoxAppear", new Vector2(-548, 200));
                        textMotion = true;
                        // yield return new WaitForSeconds(breakTime);
                        // NextText();
                    }
                    break;

                case 7:
                    // 아이템 터치하는 손짓
                    interCam.enabled = true;
                    handMotion.state = HandMotionOB.State.up;
                    handMotion.MovePosition(handMotion.handPosition[3]);
                    item.GetComponent<BoxCollider2D>().enabled = true;
                    if (!shaderMove)
                    {
                        anchoredPos = shaderPos.anchoredPos_4;
                        sizeDelta = shaderPos.sizeDelta_4;
                        MoveShader();
                    }
                    Debug.Log(i);
                    break;
                case 8:
                    // 여기에 픽업 하는거 추가
                    // 8~10 말풍선
                    Debug.Log(i);
                    interCam.enabled = false;
                    handMotion.state = HandMotionOB.State.idle;
                    handMotion.MoveAway();
                    text.text = script[4];

                    if (shaderMove)
                    {
                        GoAwayShaders();
                    }
                    yield return null;
                    if (!textMotion)
                    {
                        yield return new WaitForSeconds(2.1f);
                        if (!findObject)
                        {
                            findObject = true;
                            itemSprite = GameObject.Find("itemSprite").gameObject;
                            itemSprite.GetComponent<UIItemDragHandler>().enabled = false;
                        }
                        StartCoroutine(TextBoxAppear(new Vector2(474, -154), skipCreateTime + breakTime * 2));
                        SetSkipButtonPos(false);
                        scriptBox.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
                        scriptBox.transform.GetChild(0).GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
                        yield return new WaitForSeconds(breakTime);
                        textMotion = false;
                        i++;
                    }
                    break;

                case 9:
                    text.text = script[5];
                    if (!textMotion)
                    {
                        //StartCoroutine(TextBoxAppear(new Vector2(474, -154), skipCreateTime));
                        yield return new WaitForSeconds(breakTime);
                        textMotion = false;
                        i++;
                    }
                    break;

                case 10:
                    text.text = script[6];
                    if (!textMotion)
                    {
                        //StartCoroutine(TextBoxAppear(new Vector2(474, -154), skipCreateTime));
                        // yield return new WaitForSeconds(breakTime);
                        // NextText();
                    }
                    break;
                case 11:
                    // 핸들러 온
                    itemSprite.GetComponent<UIItemDragHandler>().enabled = true;
                    if (hanUIMotion)
                    {
                        handUI.GetComponent<UIHanMotion>().GoItemPos();
                        hanUIMotion = false;
                    }
                    break;
                case 12:
                    handUI.GetComponent<UIHanMotion>().End();
                    interCam.enabled = true;
                    handMotion.state = HandMotionOB.State.down;
                    handMotion.MovePosition(handMotion.handPosition[4]);
                    itemSprite.GetComponent<UIItemDragHandler>().enabled = false;

                    if (!shaderMove)
                    {
                        anchoredPos = shaderPos.anchoredPos_5;
                        sizeDelta = shaderPos.sizeDelta_5;
                        MoveShader();
                    }
                    break;
                case 13:
                    interCam.enabled = false;
                    handMotion.state = HandMotionOB.State.idle;
                    handMotion.MoveAway();
                    if (shaderMove)
                        GoAwayShaders();
                    break;
                case 14:
                    text.text = script[7];
                    if (!textMotion)
                    {
                        RectTransform skipPos = skipObject.GetComponent<RectTransform>();
                        StartCoroutine(TextBoxAppear(new Vector2(-451, -139), skipCreateTime));
                        scriptBox.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 180, 0);
                        scriptBox.transform.GetChild(0).GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 180, 0);
                        skipPos.localRotation = Quaternion.Euler(180, 0, 0);
                        skipPos.localPosition = new Vector2(-140, -25);
                    }
                    break;
                case 15:
                    if(!showLast)
                    {
                        lastButton.DOScale(new Vector2(1, 1), motionTime).SetEase(Ease.OutElastic);
                        showLast = true;
                        PlayerPrefs.SetInt("Tutorial", 1);
                        Debug.Log(PlayerPrefs.GetInt("Tutorial"));
                        i++;
                    }
                    break;
                case 16:
                    StopCoroutine("Tutorial");
                    break;
            }
            cam.enabled = false;
            yield return new WaitForSeconds(0.05f);
            //yield break;
        }
    }   

    private void InputText()
    {
        script[0] = "찾아야 할 고양이에 대한 정보를\n볼 수 있어요.";
        script[1] = "어떤 동물은 좋아하는 것으로\n유인을 해야만 발견 할 수 있어요.";
        script[2] = "엉뚱한 곳을 눌렀을 경우\n터치 횟수가 차감 됩니다.";
        script[3] = "일정 시간이 지나거나 광고를 보면\n 터치 횟수는 다시 회복 됩니다.";
        script[4] = "획득한 아이템은\n가방에 들어갑니다.";
        script[5] = "가방을 눌러 고양이나 강아지가\n있을 것 같은 장소에";
        script[6] = "아이템을 끌어다 놓아봐요.";
        script[7] = "고양이를 찾았습니다.\n찾은 고양이는 고집에 입주하게 됩니다.";
    }

    private IEnumerator TextBoxAppear(Vector2 goPos, float skipTime) // 텍스트 박스 나오게 하는 함수
    {
        textMotion = true;
        RectTransform boxTransform = scriptBox.GetComponent<RectTransform>();
        text.enabled = true;
        boxTransform.anchoredPosition = goPos;
        //boxTransform.sizeDelta = new Vector2(0, 0);
        boxTransform.DOSizeDelta(originScale, motionTime).SetEase(Ease.OutElastic);
        boxTransform.transform.GetChild(0).GetComponent<RectTransform>().DOSizeDelta(originScale_T, motionTime).SetEase(Ease.OutElastic);
        StartCoroutine(SkipButtonState(true, skipTime));
        yield return new WaitForSeconds(motionTime);
    }

    private IEnumerator TextBoxDisappear() // 텍스트 박스 사라지게 하는 함수
    {
        textMotion = false;
        RectTransform boxTransform = scriptBox.GetComponent<RectTransform>();
        text.enabled = false;
        boxTransform.sizeDelta = originScale;
        boxTransform.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = originScale_T;
        boxTransform.DOSizeDelta(new Vector2(0, 0), motionTime);
        boxTransform.transform.GetChild(0).GetComponent<RectTransform>().DOSizeDelta(new Vector2(0, 0), motionTime);
        StartCoroutine(SkipButtonState(false));
        yield return new WaitForSeconds(motionTime);
    }

    private void MoveShader()
    {
        shaderMove = true;
        for (int i = 0; i < shaderPos.transform.childCount; i++)
        {
            shaderPos.transform.GetChild(i).GetComponent<RectTransform>().anchoredPosition = anchoredPos[i];
            shaderPos.transform.GetChild(i).GetComponent<RectTransform>().sizeDelta = sizeDelta[i];
        }
    }

    private void GoAwayShaders()
    {
        shaderMove = false;
        for(int i = 0; i < shaderPos.transform.childCount; i++)
        {
            shaderPos.transform.GetChild(i).GetComponent<RectTransform>().anchoredPosition = new Vector2(0,0);
            shaderPos.transform.GetChild(i).GetComponent<RectTransform>().sizeDelta = new Vector2(0,0);
        }
    }

    private void SetText() // 텍스트 설정 함수
    {
        text.text = script[num];
        num++;
    }

    private IEnumerator SkipButtonState(bool state, float createTime = 0)
    {
        yield return new WaitForSeconds(createTime);
        // 생성됨
        if (state)
            skipObject.transform.DOScale(new Vector2(1, 1), 0.5f).SetEase(Ease.OutElastic);

        // 제거됨
        else
            skipObject.transform.DOScale(new Vector2(0, 0), 0.5f);

        // 버튼 On/Off
        skipBtn.enabled = state;
        yield break;
    }

    public void Skip()
    {
        StopCoroutine(tutorialCoroutine);
        NextText();
        if (i == 11)
        {
            itemSprite.GetComponent<UIItemDragHandler>().enabled = true;
            handUI.GetComponent<UIHanMotion>().GoItemPos();
            hanUIMotion = false;
        }

        tutorialCoroutine = StartCoroutine("Tutorial");
    }

    public void NextText()
    {
        StartCoroutine("TextBoxDisappear");
        i++;
    }

    // rot이 T이면 
    private void SetSkipButtonPos(bool rot)
    {
        RectTransform skipPos = skipObject.GetComponent<RectTransform>();

        if(!rot)
        {
            skipPos.localPosition = new Vector2(150, -25);
            skipPos.localRotation = Quaternion.Euler(0, 0, 180);
        }

        else
        {
            skipPos.localPosition = new Vector2(-140, 75);
            skipPos.localRotation = Quaternion.Euler(0, 0, 0);
        }

        if(i == 14)
        {
            skipPos.localPosition = new Vector2(-140, -25);
            skipPos.localRotation = Quaternion.Euler(180, 0, 0);
        }
    }
}
