﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialItemTrigger : MonoBehaviour
{
    public TutorialManager TM;
    private bool col;

    void Start()
    {
        col = false;
    }

    void Update()
    {
        if (TM.i != 7)
            GetComponent<CircleCollider2D>().enabled = false;
        else
            GetComponent<CircleCollider2D>().enabled = true;
    }

    public void OnClick()
    {
        if(!col)
        {
            col = true; 
            TM.i++;
        }
    }
}
