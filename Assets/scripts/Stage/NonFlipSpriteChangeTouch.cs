﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-04-15
 * 플립 안하고 터치 하는 스크립트 필요해서 만듦
 * 아 일본어 조금 하고 집가야징~
 */

public class NonFlipSpriteChangeTouch : MonoBehaviour, I_NonColObject
{
    public List<Sprite> spriteList = new List<Sprite>();

    private SpriteRenderer itemImg;
    private int index = 0;

    void Awake()
    {
        itemImg = GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        int num = Random.Range(0, spriteList.Count);
        itemImg.sprite = spriteList[num];
    }

    public void OnClick()
    {
        itemImg.sprite = spriteList[index++];
        index %= spriteList.Count;
    }
}
