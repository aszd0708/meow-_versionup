﻿using BitBenderGames;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PhotoShot : MonoBehaviour
{
    public GameObject newLogo;
    public GameObject[] effect = new GameObject[2];
    //public TouchInterationCam cameraTouch;
    public TouchManager touchManager;
    public new GameObject camera;
    public GameObject whiteShot;
    public GameObject shotEffect;
    public Text animalText;
    public Text animalName;
    public float shotSpeed;
    public Canvas canvas;
    public float scale = 3;
    public float speed = 2;
    public float newMotionTime;
    public CheckStageCatCol check;
    public BlurControllor blur;
    private string layerID;
    private Vector2[] photoPos = new Vector2[2];
    private Vector2 originScale;
    private float originZoom;
    private string catLayer;

    [Header("스킵 버튼에 관한 변수들")]
    public GameObject skipButton;
    private bool skipCreate = false;

    // 광고양이 일때만 사용하는 변수 만약 이런 것돌이 많아지면 옵저버 모델 사용해서 재구성하기
    private CoinCatMotion madCat;

    public enum State
    {
        picture, text, end, idle
    }

    public State state;

    private RectTransform rect;

    private void Awake()
    {
        rect = GetComponent<RectTransform>();
    }
    // Start is called before the first frame update
    void Start()
    {
        photoPos[0] = rect.offsetMax;
        photoPos[1] = rect.offsetMin;
        originScale = rect.localScale;
        skipButton.GetComponent<RectTransform>().localScale = new Vector2(0, 0);
    }

    void Update()
    {
        if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            return;
        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                if (EventSystem.current.IsPointerOverGameObject(i))
                    return;
            }
        }
        switch (state)
        {
            case State.picture:
                break;
            case State.text:
                if (Input.GetMouseButtonUp(0))
                {
                    animalText.GetComponent<TypingText>().state = TypingText.State.end;
                    animalText.GetComponent<FadeInLetterByLetter>().state = FadeInLetterByLetter.State.end;
                    animalText.GetComponent<EasyHyperText>().enabled = true;
                    state = State.end;
                }
                break;
            case State.end:
                if(!skipCreate)
                {
                    skipCreate = true;
                    skipButton.transform.DOScale(new Vector2(-1, 1), 0.5f).SetEase(Ease.OutElastic);
                }
                // 밑에 주석들은 맨 아래에 있는 스킵 버튼으로 넘어갔음
                /*
                if (Input.GetMouseButtonUp(0))
                {
                    Backbutton();
                    state = State.idle;
                    if (SceneManager.GetActiveScene().name == "Stage0 Tutorial")
                        GameObject.Find("TutorialManager").GetComponent<TutorialManager>().i++;
                }
                */
                break;
            case State.idle:
                break;
        }
    }

    public void TakePicture(GameObject selectCat)
    {
        if (selectCat.GetComponent<AnimalText>().name == "광고양이")
            madCat = selectCat.GetComponent<CoinCatMotion>();

        state = State.picture;
        canvas.enabled = false;
        camera.GetComponent<MobileTouchCamera>().BoundaryMax = new Vector2(camera.GetComponent<MobileTouchCamera>().BoundaryMax.x * 2, camera.GetComponent<MobileTouchCamera>().BoundaryMax.y * 2);
        camera.GetComponent<MobileTouchCamera>().BoundaryMin = new Vector2(camera.GetComponent<MobileTouchCamera>().BoundaryMin.x * 2, camera.GetComponent<MobileTouchCamera>().BoundaryMin.y * 2);
        originZoom = camera.GetComponent<MobileTouchCamera>().CamZoomMax;
        //camera.GetComponent<MobileTouchCamera>().CamZoomMax = camera.GetComponent<MobileTouchCamera>().CamZoomMin;
        //camera.GetComponent<MobileTouchCamera>().ResetCameraBoundaries();
        StartCoroutine(TakePictureDelay(selectCat));
        //layerID = selectCat.GetComponent<SpriteRenderer>().sortingLayerName;
        camera.GetComponent<TouchInputController>().CanMove = false;
        //cameraTouch.cameraTouch = false;
        touchManager.CameraTouch = false;
        //ShotEffet();
    }

    private IEnumerator Zoom(float max, float min, float animeTime)
    {
        MobileTouchCamera touchCam = camera.GetComponent<MobileTouchCamera>();
        float zoomMax = max, zoomMin = min;
        float time = 0.0f;

        while (zoomMax > zoomMin)
        {
            time += Time.deltaTime / animeTime;

            zoomMax = Mathf.Lerp(max, min, time);

            touchCam.CamZoomMax = zoomMax;
            touchCam.ResetCameraBoundaries();
            yield return null;
        }
    }

    public void Backbutton()
    {
        if (madCat != null)
        {
            madCat.SendMessage("GoPositionStarter", SendMessageOptions.DontRequireReceiver);
            madCat.MCC.SendMessage("MadCatTouch", SendMessageOptions.DontRequireReceiver);
            madCat = null;
        }

        StartCoroutine("DisabledPicture");
    }

    private IEnumerator TakePictureDelay(GameObject selectCat)
    {
        //selectCat.GetComponent<SpriteRenderer>().sortingLayerName = "8";
        animalText.GetComponent<EasyHyperText>().enabled = false;
        iTween.MoveTo(camera, iTween.Hash("x", selectCat.transform.position.x, "y", selectCat.transform.position.y, "time", 0.5f, "easeType", iTween.EaseType.linear));
        StartCoroutine(Zoom(camera.GetComponent<MobileTouchCamera>().CamZoomMax, camera.GetComponent<MobileTouchCamera>().CamZoomMin, 0.5f));
        yield return new WaitForSeconds(0.5f);
        whiteShot.GetComponent<Image>().enabled = true;
        whiteShot.GetComponent<ShotEffect>().Shot();
        blur.On(new Vector3(camera.transform.position.x, camera.transform.position.y, 0));
        rect.offsetMax = new Vector2(0, 0);
        rect.offsetMin = new Vector2(0, 0);
        rect.sizeDelta = new Vector2(0, 500);
        rect.localScale = new Vector2(scale, scale);
        animalName.text = selectCat.GetComponent<AnimalText>().name;
        animalText.text = selectCat.GetComponent<AnimalText>().photoText;
        animalText.GetComponent<EasyHyperText>().hyperlinkString = "https://www.instagram.com/" + selectCat.GetComponent<AnimalText>().instarURL;
        //animalText.GetComponent<TypingText>().InputText();
        animalText.GetComponent<FadeInLetterByLetter>().SetText(selectCat.GetComponent<AnimalText>().photoText);
        animalText.GetComponent<FadeInLetterByLetter>().Fade();
        state = State.text;
        transform.DOScale(originScale, speed);
        yield return new WaitForSeconds(speed);
        shotEffect.GetComponent<Image>().enabled = true;
        if (selectCat.name != "MadCat")
            catLayer = selectCat.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingLayerName;
        else
            catLayer = selectCat.GetComponent<SpriteRenderer>().sortingLayerName;
        shotEffect.GetComponent<TakeShotEffect>().Shot(selectCat);
        newLogo.GetComponent<JellyMotion>().Motion(newMotionTime);
        effect[0].GetComponent<JellyMotion>().Motion(newMotionTime);
        effect[1].GetComponent<JellyMotion>().Motion(newMotionTime);
        yield return new WaitForSeconds(newMotionTime);
        yield return null;
    }

    private IEnumerator DisabledPicture()
    {
        blur.Off();
        transform.DOScale(0.0f, 0.3f).SetEase(Ease.InOutQuint);
        whiteShot.GetComponent<Image>().enabled = false;
        yield return new WaitForSeconds(0.3f);
        shotEffect.GetComponent<TakeShotEffect>().LayerSetting(catLayer);
        shotEffect.GetComponent<TakeShotEffect>().ListReset();
        camera.GetComponent<TouchInputController>().CanMove = true;
        //cameraTouch.cameraTouch = true;
        touchManager.CameraTouch = true;
        canvas.enabled = true;
        camera.GetComponent<MobileTouchCamera>().CamZoomMax = originZoom;
        camera.GetComponent<MobileTouchCamera>().BoundaryMax = new Vector2(camera.GetComponent<MobileTouchCamera>().BoundaryMax.x / 2, camera.GetComponent<MobileTouchCamera>().BoundaryMax.y / 2);
        camera.GetComponent<MobileTouchCamera>().BoundaryMin = new Vector2(camera.GetComponent<MobileTouchCamera>().BoundaryMin.x / 2, camera.GetComponent<MobileTouchCamera>().BoundaryMin.y / 2);
        camera.GetComponent<MobileTouchCamera>().ResetCameraBoundaries();
        GetComponent<RectTransform>().offsetMax = photoPos[0];
        GetComponent<RectTransform>().offsetMin = photoPos[1];
        shotEffect.GetComponent<Image>().color = shotEffect.GetComponent<TakeShotEffect>().firstColor;
        newLogo.GetComponent<RectTransform>().localScale = new Vector2(0, 0);
        yield return null;
    }

    // 여기서 부터는 스킵 버튼이 나오는 함수 및 스킵버튼이 하는 일들 정리
    public void SkipButtonAction()
    {
        Backbutton();
        state = State.idle;
        skipCreate = false;
        skipButton.transform.DOScale(new Vector2(0, 0), 0.5f).SetEase(Ease.Linear);
        if (SceneManager.GetActiveScene().name == "Stage0 Tutorial")
            GameObject.Find("TutorialManager").GetComponent<TutorialManager>().i++;
    }
}
