﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageSpeedController : MonoBehaviour
{
    public float speed = 1;

    public Text speedText;

    private void Start()
    {
        speedText.text = speed + "";
    }

    public void SetSpeed(float value)
    {
        speed += value;
        Time.timeScale = speed;
        speedText.text = speed +"";
    }
}
