﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TreeTouchOtherEffect : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer render;

    [SerializeField]
    private Sprite[] leafSprites;

    private void OnEnable()
    {
        render.color = Color.white;

        render.sprite = leafSprites[Random.Range(0, leafSprites.Length)];

        render.DOFade(0, 1.0f);
        PoolingManager.Instance.SetPool(gameObject, "TreeTouchOtherEffect", 1.0f);
    }
}
