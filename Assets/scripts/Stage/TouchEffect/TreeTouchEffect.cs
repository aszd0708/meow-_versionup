﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeTouchEffect : MonoBehaviour
{
    public GameObject effect;
    [Header("이펙트 이미지")]
    public List<Sprite> effectSprites = new List<Sprite>();

    public int[] randomValues = new int[2];

    [Header("나뭇잎 뿌릴때 어느 곳으로 뿌려질 지 X축만 정하는 랜덤 변수")]
    public float[] randomPosValues = new float[2];
    [Header("이펙트가 나올때 어디까지(Y축) 갈지 정함")]
    public float dropYValue;
    [Header("점프 힘 크기 랜덤 범위로 정함")]
    public float[] jumpPower = new float[2];
    [Header("점프 하는 시간")]
    public float jumpTime;
    [Header("점프 후 사라지는 시간")]
    public float fadeTime;

    [Header("Pooling할 오브젝트 이름")]
    public string poolingName;
    [Header("이펙트 Pooling 이름")]
    public string effectPoolingName;
    private int count;
    public int Count { get => count; set => count = value; }

    private void OnEnable()
    {
        Count = Random.Range(randomValues[0], randomValues[1] + 1);

        for (int i = 0; i < Count; i++)
            SpreadEffect();
    }

    // 나뭇잎 이펙트를 막 뿌리는 함수
    private void SpreadEffect()
    {
        GameObject createEffect = PoolingManager.Instance.GetPool(effectPoolingName);
        if (createEffect == null)
            createEffect = Instantiate(effect);
        createEffect.transform.eulerAngles = new Vector3(0, 0, Random.Range(0.0f, 360.0f));
        createEffect.transform.SetParent(transform);
        createEffect.transform.localPosition = Vector3.zero;

        StartCoroutine(DropEffect(createEffect));
    }

    private IEnumerator DropEffect(GameObject effect)
    {
        float randomX = Random.Range(randomPosValues[0], randomPosValues[1]);
        SpriteRenderer sprite = effect.GetComponent<SpriteRenderer>();
        SetRandomTreeEffect(sprite);

        effect.transform.DOLocalJump(new Vector3(randomX, dropYValue, 0), Random.Range(jumpPower[0], jumpPower[1]), 1, jumpTime + fadeTime);
        yield return new WaitForSeconds(jumpTime);
        sprite.DOColor(new Color(sprite.color.r, sprite.color.g, sprite.color.b, 0), fadeTime);
        yield return new WaitForSeconds(fadeTime);
        PoolingManager.Instance.SetPool(effect, effectPoolingName);
        PoolingManager.Instance.SetPool(gameObject, "TreeEffect", 0.1f);
        yield break;
    }

    private void SetRandomTreeEffect(SpriteRenderer sprite)
    {
        int random = Random.Range(0, effectSprites.Count);
        sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, 1);
        sprite.sprite = effectSprites[random];
    }
}
