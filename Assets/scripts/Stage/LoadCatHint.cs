﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadCatHint : MonoBehaviour
{
    public int[] num;
    public JsonCatExplanation catExplanation;

    public GameObject contents;
    public GameObject cat;
    public GameObject hiddenCat;
    public GameObject hiddenCatInfo;
    public int[] hiddenCatNum;

    void Start()
    {
        LoadHint();
    }

    private void LoadHint()
    {
        int temp=0;
        string[] name = new string[num.Length + hiddenCatNum.Length];
        string[] hint = new string[num.Length + hiddenCatNum.Length];
        string[] photoText = new string[num.Length + hiddenCatNum.Length];
        string[] instarURL = new string[num.Length + hiddenCatNum.Length];
        List<JsonCatExplanation.Explanation> explanations = catExplanation.LoadCats();

        for (int i = num[0] - 1; i < num[num.Length - 1] + hiddenCatNum.Length; i++, temp++)
        {
            name[temp] = explanations[i].Name;
            hint[temp] = explanations[i].Hint;
            photoText[temp] = explanations[i].HashTagText();
            instarURL[temp] = explanations[i].InstarURL();
            if (hint[temp] == "FALSE")
                hint[temp] = "안알랴줌";
        }

        for (int i = 0; i < contents.transform.childCount; i++)
        {
            AnimalText text = GetComponent<Stage1Objects>().cat[i].GetComponent<AnimalText>();
            contents.transform.GetChild(i).GetComponent<SampleCatButton>().text = hint[i];
            text.name = name[i];
            text.photoText = photoText[i];
            text.instarURL = instarURL[i];
        }

        for(int i = 0; i < hiddenCatInfo.transform.childCount; i++)
        {
            AnimalText hiddenCatAnimalText = hiddenCat.transform.GetChild(i).GetComponent<AnimalText>();
            hiddenCatInfo.transform.GetChild(i).GetComponent<SampleCatButton>().text = hint[i + num.Length];
            hiddenCatAnimalText.name = name[i + num.Length];
            hiddenCatAnimalText.photoText = photoText[i + num.Length];
            hiddenCatAnimalText.instarURL = instarURL[i + num.Length];
        }
    }
}
