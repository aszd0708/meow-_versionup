﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageRockTouch : StageInfinitySpriteObject
{
    [Header("먼지 오브젝트")]
    public GameObject dustObj;
    [Header("먼지가 변하는 시간")]
    public float changeTime;

    protected override void StartEvent()
    {
        StartCoroutine(_BeDust());
    }

    private IEnumerator _BeDust()
    {
        GameObject dust = PoolingManager.Instance.GetPool("Dust");
        if (dust == null)
            dust = Instantiate(dustObj);

        dust.transform.position = transform.position;

        PoolingManager.Instance.SetPool(dust, "Dust", changeTime);
        gameObject.SetActive(false);
        yield break;
    }
}
