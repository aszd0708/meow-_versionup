﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.UI;

public class CoinCatTimer : MonoBehaviour
{
    // 초기엔터치 120 시간랜덤 30~120
    // 그리고 1시간은 10이 아니고 100으로 설정해야 함 지금은 테스트 때문에 어쩔수 없이 함
    public GameObject coinCat;
    public Vector3[] randomPos = new Vector3[2];
    private System.DateTime resetTime;
    private System.DateTime saveTime;
    // Start is called before the first frame update
    
    private Coroutine hourTimer;
    private Coroutine minTimer;
    private Coroutine timerCheck;

    [Header("몇분 있다가 나올지 확인 총 천의자리까지 분분초초")]
    public int timer;
    void Start()
    {
        resetTime = new System.DateTime(1995, 11, 10, 18, 00, 00);

        if (!System.IO.File.Exists(Application.persistentDataPath + "/" + "CoinCatTimer.dat"))
        {
            saveTime = resetTime;
            TimerSave();
        }
        else
            TimerLoad();

        hourTimer = StartCoroutine(HourTimer());
        if (!EncryptedPlayerPrefs.HasKey("CoinCatTimer"))
            EncryptedPlayerPrefs.SetInt("CoinCatTimer", 10);
        if (!EncryptedPlayerPrefs.HasKey("CoinCatCounter"))
            EncryptedPlayerPrefs.SetInt("CoinCatCounter", Random.Range(0, 30));

        if(!coinCat.GetComponent<CoinCatMotion>().col)
        {
            minTimer = StartCoroutine(MinTimer());
            timerCheck = StartCoroutine(TimerChecker());
        }
    }

    private IEnumerator TimerChecker()
    {
        while(true)
        {
            //Debug.Log("시간 : " + EncryptedPlayerPrefs.GetInt("CoinCatTimer"));
            //Debug.Log("횟수 : " + EncryptedPlayerPrefs.GetInt("CoinCatCounter"));
            yield return new WaitForSeconds(1.0f);
        }
    }

    private void SetCounter(int counter)
    {
        EncryptedPlayerPrefs.SetInt("CoinCatCounter", counter);
    }

    private void Checking()
    {
        int timer = EncryptedPlayerPrefs.GetInt("CoinCatTimer");
        int counter = EncryptedPlayerPrefs.GetInt("CoinCatCounter");

        if (timer == 0 && counter <= 0)
        {
            // 고양이 생성 후 1시간 초기화 타이머 돌리기
            coinCat.GetComponent<CoinCatMotion>().pos = randomPos;
            coinCat.GetComponent<CoinCatMotion>().MotionStarter();
            saveTime = System.DateTime.Now;
            TimerSave();
            // 고양이 모션에 따른 값 전달해주기
            hourTimer = StartCoroutine("HourTimer");
            if(minTimer != null)
                StopCoroutine(minTimer);
        }
    }

    private IEnumerator HourTimer()
    {
        Debug.Log(saveTime);
        System.DateTime nowTime = System.DateTime.Now;
        if (saveTime == resetTime)
        {
            saveTime = nowTime;
            StartCoroutine(HourTimer());
            Checking();
            yield return null;
        }
        else
        {
            //Debug.Log(nowTime.ToString("yyMMdd"));
            //Debug.Log(saveTime.ToString("yyMMdd"));
            //Debug.Log(nowTime.ToString("yyMMdd") != saveTime.ToString("yyMMdd"));

            if (nowTime.ToString("yyMMdd") != saveTime.ToString("yyMMdd"))
            {
                ResetTimerCounter();
                minTimer = StartCoroutine("MinTimer");
                timerCheck = StartCoroutine("TimerChecker");
                yield return null;
            }
            else
            {
                while (true)
                {
                    nowTime = System.DateTime.Now;
                    Debug.Log("현재시간 : " + (int.Parse(nowTime.ToString("HHmm"))));
                    Debug.Log("저장시간 : " + int.Parse(saveTime.ToString("HHmm")));
                    if((int.Parse(nowTime.ToString("HHmm")) - int.Parse(saveTime.ToString("HHmm"))) >= timer)
                    {
                        ResetTimerCounter();
                        coinCat.GetComponent<CoinCatMotion>().col = false;
                        coinCat.GetComponent<CoinCatMotion>().SaveCol(0);
                        break;
                    }
                    yield return new WaitForSeconds(5.0f);
                }
            }
        }
        yield return null;
        StopCoroutine(hourTimer);
    }

    private IEnumerator MinTimer()
    {
        while(true)
        {
            Checking();
            EncryptedPlayerPrefs.SetInt("CoinCatTimer", EncryptedPlayerPrefs.GetInt("CoinCatTimer")-1);
            yield return new WaitForSeconds(1.0f);
        }
    }

    private void ResetTimerCounter()
    {
        EncryptedPlayerPrefs.SetInt("CoinCatTimer", 10);
        EncryptedPlayerPrefs.SetInt("CoinCatCounter", Random.Range(0, 5));
    }

    public void MinCounter()
    {
        int num = EncryptedPlayerPrefs.GetInt("CoinCatCounter") - 1;
        if (num > 0)
            SetCounter(num);
        else
            SetCounter(0);
    }

    public void TimerSave()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/" + "CoinCatTimer.dat");
        System.DateTime saveInfo;

        saveInfo = saveTime;
        bf.Serialize(file, saveInfo);
        file.Close();
    }

    public void TimerLoad()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.OpenRead(Application.persistentDataPath + "/" + "CoinCatTimer.dat");
        if (file != null && file.Length > 0)
        {
            System.DateTime loadInfo = (System.DateTime)bf.Deserialize(file);
            saveTime = loadInfo;
            file.Close();
        }
    }
}
