﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;

/*
 * 2020-02-24
 * 뒤로 돌아서 앉아있는 사람 모션 스크립트
 * 그냉 고개만 까딱까딲까딲???
 * 일단 만들어보고 이상하면 고치는걸로
 * 
 * 사람 한명한명 눌러도 되는게 있고
 * 만약 포장마차를 누르면 다 같이 건배 하는것도 괜찮을듯 함
 * 그건 일단 사람들 다 만든 다음에 포장마차에 넣으면서 생각하는걸로
 */

public class Stage3ObjectSittingPersonMotion : MonoBehaviour, TouchableObj
{
    public Transform headTrans;
    public Transform armTrans;

    // 머리
    [Header("움직이는 시간 간격")]
    public float motionTime;
    [Header("움직이고 난 뒤에 잠깐 쉬는 시간")]
    public float restTime;
    [Header("머리 움직이는 각도")]
    public float headMoveAngle = 15;

    // 팔
    [Header("팔 모션 움직이는 시간")]
    public float armMotionTime;
    [Header("팔이 위에 고정되있는 시간")]
    public float armRestTime;
    [Header("팔 움직이는 각도")]
    public float armMoveAngle = -50;

    // 머리 움직이는 코루틴 ON/OFF 용도로 만들었음
    private Coroutine headMotionCor;

    // 손을 움직일 수 있는 사람과 아닌 사람 구분 하기 위해 만듦
    public bool haveArm;

    public enum Position
    {
        BACK, FRONT, SIDE, CENTER
    }

    public Position POS;

    private bool touch = false;

    // Flip때문에 만들었는데 이거 두개의 변수로 컨트롤 함
    private bool headMove = false;

    // 이건 플립 해주는 사람인지 아닌지 확인하는 변수
    private bool flip = false;

    [SerializeField]
    [Header("모션 말고 다른 이벤트 넣을때 사용")]
    public UnityEvent touchEvent;

    void Start()
    {
        headMotionCor = StartCoroutine(HeadMotion());
        flip = SetFlip(POS);
        if (flip)
            StartCoroutine(FlipMotion());
    }

    private IEnumerator HeadMotion()
    {
        while (true)
        {
            headMove = true;
            headTrans.DOLocalRotate(new Vector3(0, 0, headMoveAngle), motionTime);
            headMoveAngle *= -1;
            yield return new WaitForSeconds(motionTime);
            headMove = false;
            yield return new WaitForSeconds(restTime);
        }
    }

    public void OnClick()
    {
        if (touch) return;

        if (haveArm) StartCoroutine(HandMotion());
        else StartCoroutine(NoArmPersonMotion());

    }

    private IEnumerator HandMotion()
    {
        StopCoroutine(headMotionCor);
        touch = true;
        headTrans.DOLocalRotate(new Vector3(0, 0, 0), 0.5f);
        armTrans.DOLocalRotate(new Vector3(0, armTrans.rotation.y, armMoveAngle), armMotionTime).SetEase(Ease.OutCirc);
        yield return new WaitForSeconds(armMotionTime + armRestTime);
        armTrans.DOLocalRotate(new Vector3(0, armTrans.rotation.y, 0), armMotionTime);
        headMotionCor = StartCoroutine(HeadMotion());
        touch = false;
        yield break;
    }

    private IEnumerator NoArmPersonMotion()
    {
        yield break;
    }

    private IEnumerator FlipMotion()
    {
        float flip = 1;
        while (true)
        {
            if (!touch && !headMove)
                transform.DORotate(new Vector3(0, flip * 180, 0), 0.5f).SetEase(Ease.Linear);
            yield return new WaitForSeconds(5.5f);
            flip++;
            flip %= 2;
        }
    }

    private void SetHaveArm(bool have)
    {
        haveArm = have;
    }

    private bool SetFlip(Position POS)
    {
        switch (POS)
        {
            case Position.BACK: 
            case Position.FRONT: 
                return true;

            default: return false;
        }
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        OnClick();
        touchEvent.Invoke();
        return false;
    }
}
