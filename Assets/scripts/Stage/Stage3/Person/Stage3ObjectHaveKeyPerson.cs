﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage3ObjectHaveKeyPerson : MonoBehaviour
{
    [SerializeField]
    private Collider2D itemCollider;
    [SerializeField]
    private SpriteRenderer itemSpriteRenderer;
    [SerializeField]
    private Transform itemTransform;

    [SerializeField]
    [Header("아이템 나오는 포지션")]
    private Transform itemPose;

    bool bIsColleted = false;

    private void Start()
    {
        if(itemCollider != null)
        {
            itemCollider.enabled = false;
        }
        if(itemSpriteRenderer != null)
        {
            //itemSpriteRenderer.enabled = false;
        }

        if (itemCollider.gameObject.activeSelf == false)
        {
            bIsColleted = true;
        }
    }

    public void JumpToDestination()
    {
        if(bIsColleted == true)
        {
            return;
        }
        if(itemTransform != null)
        {
            if (itemSpriteRenderer != null)
            {
                itemSpriteRenderer.enabled = true;
                itemSpriteRenderer = null;
            }
            itemTransform.DOJump(itemPose.position, 0.4f, 1, 0.5f);
            Invoke("SetItemOn", 0.4f);
        }
    }

    private void SetItemOn()
    {
        if (bIsColleted == true)
        {
            return;
        }
        if (itemCollider != null)
        {
            itemCollider.enabled = true;
            itemCollider = null;
        }
        itemTransform = null;
    }
}
