﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-03-09
 * 수산물 바구니 수산물 만드는 스크립트
 */

public class Stage3ObjectSeaMarketBasket : MonoBehaviour, TouchableObj
{
    private SpriteRenderer basketSprite;

    public List<GameObject> seaFoods = new List<GameObject>();

    private GameObject fish;

    private List<Transform> fishesTransform = new List<Transform>();

    private bool canTouch = true;

    private void Awake()
    {
        basketSprite = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        CreateFish();
    }

    private void CreateFish()
    {
        fish = Instantiate(seaFoods[Random.Range(0, seaFoods.Count)]);
        fish.transform.SetParent(transform);
        fish.transform.localPosition = Vector3.zero;

        for (int i = 0; i < fish.transform.childCount; i++)
        {
            fishesTransform.Add(fish.transform.GetChild(i));
            SpriteRenderer spriteRenderer = fishesTransform[i].GetComponent<SpriteRenderer>();
            spriteRenderer.sortingLayerName = basketSprite.sortingLayerName;
            spriteRenderer.sortingOrder += basketSprite.sortingOrder;
        }
    }

    private IEnumerator FishMotion()
    {
        int random = Random.Range(0, 100);
        canTouch = false;

        if (random >= 80 && random < 99)
        {
            // 여기 여러개 팔딱팔딱
            yield return StartCoroutine(Motion(fishesTransform));
        }

        else if (random == 99)
        {
            // 여기 한개만 팔딱팔딱
            int randomFish = Random.Range(0, fishesTransform.Count);
            yield return StartCoroutine(FireFish(fishesTransform[randomFish]));
        }


        else
        {
            // 여기 한개만 팔딱팔딱
            int randomFish = Random.Range(0, fishesTransform.Count);
            yield return StartCoroutine(Motion(fishesTransform[randomFish]));
        }

        canTouch = true;
    }

    private IEnumerator Motion(Transform fish)
    {
        float angle = 10;
        int random = Random.Range(1, 4);
        for(int i = 0; i < random; i++)
        {
            fish.DORotate(new Vector3(0, 0, angle), 0.1f);
            yield return new WaitForSeconds(0.1f);
            angle *= -1;
        }
        fish.DORotate(new Vector3(0, 0, 0), 0.05f);
        yield return new WaitForSeconds(0.05f);
        yield break;
    }

    private IEnumerator Motion(List<Transform> fishT)
    {
        float angle = 10;
        int random = Random.Range(1, 5);
        for (int i = 0; i < random; i++)
        {
            foreach (Transform fish in fishT)
                fish.DORotate(new Vector3(0, 0, angle), 0.1f);

            yield return new WaitForSeconds(0.1f);
            angle *= -1;
        }
        foreach (Transform fish in fishT)
            fish.DORotate(new Vector3(0, 0, 0), 0.05f);
        yield return new WaitForSeconds(0.05f);
        yield break;
    }

    private IEnumerator FireFish(Transform fish)
    {
        fish.DOShakePosition(3.0f, 0.5f, 30, 45);
        yield return new WaitForSeconds(3.0f);
        fish.DOLocalMoveY(50, 7f).SetEase(Ease.InQuint);
        yield return new WaitForSeconds(7);
        fishesTransform.Remove(fish);
        fish.gameObject.SetActive(false);
        yield break;
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        if(canTouch)
        {
            StartCoroutine(FishMotion());
        }
        return false;
    }
}
