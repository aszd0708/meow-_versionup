﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-03-02
 * 동전 나오는 프리팹에 들어갈 스크립트
 * TouchCreateCoin과 같은 것에 들어감
 */
public class CoinSpreadAction : MonoBehaviour
{
    public GameObject coin;

    public float poolingTime;

    public int[] randomValues = new int[2];
    public float angle = 30;
    [Header("Pooling할 오브젝트 이름")]
    public string poolingName;
    private int count;
    public int Count { get => count; set => count = value; }

    public float spreadDistance;

    private void OnEnable()
    {
        Count = Random.Range(randomValues[0], randomValues[1] + 1);

        for (int i = 0; i < Count; i++)
            SpreadCoin();
    }

    // 막 동전을 막 뿌려대는 함수
    private void SpreadCoin()
    {
        GameObject createCoin = PoolingManager.Instance.GetPool(poolingName);
        if (createCoin == null)
            createCoin = Instantiate(coin);
        createCoin.transform.SetParent(transform);
        createCoin.transform.localPosition = Vector3.zero;
        createCoin.transform.DOLocalJump(new Vector3(Random.Range(-spreadDistance, spreadDistance), Random.Range(-0.5f, 0.5f)), Random.Range(1, 2), Random.Range(1, 4), Random.Range(0.5f, 1.0f));
        PoolingManager.Instance.SetPool(createCoin, poolingName, poolingTime);
        PoolingManager.Instance.SetPool(gameObject, "SpreadCoin", poolingTime);
    }

    private Vector3 AnlgeToDir(float angle)
    {
        Vector3 dir;
        float x = Mathf.Sin(angle);
        float y = Mathf.Cos(angle);
        Quaternion qur = Quaternion.Euler(0, angle, 0);
        dir = new Vector2(x, y) * 1.5f;
        return dir;
    }
}
