﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PochaSpriteSetting : MonoBehaviour
{
    public List<SpriteRenderer> pochaObj = new List<SpriteRenderer>();

    private void Start()
    {
        for (int i = 0; i < pochaObj.Count; i++)
            pochaObj[i].enabled = RandomBool();
    }

    private bool RandomBool()
    {
        int random = Random.Range(0, 2);
        if (random == 0) return false;
        else return true;
    }
}
