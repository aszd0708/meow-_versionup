﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-02-04
 * 파라솔 접었다 폈다
 * 왜 한개 더 만들었냐하면 콜라이더를 두개를 써야 자연스러울거 같아서 그랬음
 * 처음에 시작할 때 랜덤으로 펼쳐져 있던지 없던지 하고 있음
 */

public class Stage3ObjectParasol : MonoBehaviour, TouchableObj
{
    private SpriteRenderer sprite;

    public List<Sprite> sprites = new List<Sprite>();

    private CapsuleCollider2D openCol;
    private BoxCollider2D closedCol;

    private int index = 0;
    private bool open;

    private void Awake()
    {
        sprite = GetComponent<SpriteRenderer>();
        openCol = GetComponent<CapsuleCollider2D>();
        closedCol = GetComponent<BoxCollider2D>();
    }

    private void Start()
    {
        index = Random.Range(0, sprites.Count);
        if (index == 0)
            open = false;
        else open = true;
        OnClick();
    }

    public void OnClick()
    {
        openCol.enabled = open;
        sprite.sprite = sprites[index++];
        index %= sprites.Count;
        open = !open;
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        OnClick();
        return false;
    }
}
