﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-02-21
 * 식탁에 바스켓들을 만들어주는 함수
 * 랜덤으로 랜덤위치 (정해진 포지션 중에)에 생성
 * 중복되지 않게 만들면 그 포지션은 리스트에서 제외됨
 */

public class DeskBasketCreate : MonoBehaviour
{
    public List<GameObject> baskets = new List<GameObject>();
    public Transform createParentTransform;

    public Transform createParent;

    [Header("최대 몇개를 만들지 정하는 변수")]
    public int createCount;

    public string layer;

    private List<Vector3> createPos = new List<Vector3>();

    private List<SpriteRenderer> spriteRenderers = new List<SpriteRenderer>();

    void Awake()
    {
        for (int i = 0; i < createParentTransform.childCount; i++)
        {
            if(createParentTransform.GetChild(i).gameObject.activeSelf)
            {
                createPos.Add(createParentTransform.GetChild(i).position);
            }
        }
    }

    // Start is called before the first frame update
    private void Start()
    {
        Invoke("Create", 0.1f);
    }

    void Create()
    {       
        if (createCount > createPos.Count)
            return;

        for (int i = 0; i < createCount; i++)
            CreateBasket();

    }

    void CreateBasket()
    {
        int index = Random.Range(0, baskets.Count);
        int posIndex = Random.Range(0, createPos.Count);
        GameObject create = Instantiate(baskets[index]);
        create.GetComponent<Stage3ObjectVegetableTouch>().Layer = layer;
        create.GetComponent<Stage3ObjectVegetableTouch>().SetLayer(2);
        create.transform.position = createPos[posIndex];
        create.transform.SetParent(createParent);

        createPos.Remove(createPos[posIndex]);
    }

    private void GetAllSpriteRenderer(Transform t, List<SpriteRenderer> sprites)
    {
        Debug.Log(t.childCount);
        for (int i = 0; i < t.childCount; i++)
        {
            if (t.childCount > 0)
                GetAllSpriteRenderer(t, sprites);
            if (t.GetChild(i).GetComponent<SpriteRenderer>() != null)
                sprites.Add(t.GetChild(i).GetComponent<SpriteRenderer>());
        }
    }

}
