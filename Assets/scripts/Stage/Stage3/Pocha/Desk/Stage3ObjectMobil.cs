﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-02-17
 * 모빌 누르면 스프라이트 바꾸고 다시 누르면 180도 회전 누르면 바꾸고 다시 180도 회전
 */

public class Stage3ObjectMobil : MonoBehaviour, TouchableObj
{
    [Header("0이 최소 최대 정하기 (배열이나 리스트 인덱스랑 같음)")]
    [Header("모빌은 4가 최대 이거 모빌만 쓰는 스크립트임")]
    public int maxMotionIndex;
    public List<Sprite> mobilSprite = new List<Sprite>();

    private SpriteRenderer sprite;
    private int motionIndex;

    public List<Quaternion> rot = new List<Quaternion>();
        
    [Header("프레임당 시간")]
    public float motionFrameTime;

    private bool touch = true;

    private void Awake()
    {
        sprite = GetComponent<SpriteRenderer>();
        
    }

    private void Start()
    {
        motionIndex = Random.Range(0, maxMotionIndex + 1);
        OnClick();
    }

    private IEnumerator MotionCor()
    {
        WaitForSeconds wait = new WaitForSeconds(motionFrameTime);
        while (!touch)
        {
            Motion();
            yield return wait;
        }
        yield break;
    }

    public void OnClick()
    {
        touch = !touch;
        StartCoroutine(MotionCor());
    }

    private void Motion()
    {
        switch (motionIndex)
        {
            case 0:
                sprite.sprite = mobilSprite[0];
                transform.rotation = rot[0];
                break;
            case 1:
                sprite.sprite = mobilSprite[1];
                transform.rotation = rot[0];
                break;
            case 2:
                sprite.sprite = mobilSprite[1];
                transform.rotation = rot[1];
                break;
            case 3:
                sprite.sprite = mobilSprite[0];
                transform.rotation = rot[1];
                break;
        }
        motionIndex++;
        motionIndex %= maxMotionIndex;
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        OnClick();
        return false;
    }
}
