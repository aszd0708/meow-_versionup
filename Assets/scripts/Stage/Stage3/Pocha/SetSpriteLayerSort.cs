﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetSpriteLayerSort : MonoBehaviour
{
    // Start is called before the first frame update
    public string layerName;

    private List<SpriteRenderer> sprite = new List<SpriteRenderer>();

    void Start()
    {
        GetAllSpriteRenderer(transform, sprite);
        SetAllSpriteRendererLayer();
    }

    private void GetAllSpriteRenderer(Transform parent, List<SpriteRenderer> list)
    {
        for(int i = 0; i < parent.childCount; i++)
        {
            if (parent.childCount > 0)
                GetAllSpriteRenderer(parent, list);
            sprite.Add(parent.GetChild(i).GetComponent<SpriteRenderer>());
        }
    }

    private void SetAllSpriteRendererLayer()
    {
        for (int i = 0; i < sprite.Count; i++)
            sprite[i].sortingLayerName = layerName;
    }
}
