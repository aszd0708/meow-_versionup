﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-02-04
 * 선풍기 돌아가는 스크립트
 */

public class Stage3ObjectFan : MonoBehaviour, TouchableObj
{
    [Header("가속도")]
    public float speed;
    [Header("최대 속도")]
    public float maxSpeed = 500f;

    [Header("돌릴 날개")]
    public Transform fan;
    
    private float nowSpeed = 0;
    public bool stop = true;
    private float rot = 0;

    private void Start()
    {
        stop = RandomState();
    }

    private void Update()
    {
        if (!stop)
            RatateFan();
        else
            StopFan();

        rot += Time.deltaTime * nowSpeed;
        fan.rotation = Quaternion.Euler(0, 0, rot);
        nowSpeed = Mathf.Clamp(nowSpeed, 0, maxSpeed);
    }

    private void RatateFan()
    {
        nowSpeed += speed;
    }

    private void StopFan()
    {
        if (nowSpeed <= 0)
            return;
        nowSpeed -= speed;
        if (rot <= 0)
            rot = 0;
    }

    public void OnClick()
    {
        if(!stop)
            rot %= 360;

        stop = !stop;
    }

    private bool RandomState()
    {
        int random = Random.Range(0, 2);

        switch(random)
        {
            case 0: return false;
            case 1: return true;
            default: return false;
        }
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        OnClick();
        return false;
    }
}
