﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 스테이지3에 있는 불빛 켜졌다 꺼졌다 해주는 함수
 * 
 */


public class Stage3ObjectFenceLight : MonoBehaviour, TouchableObj
{
    private SpriteRenderer lightOn;
    private SpriteRenderer lightOff;

    private bool canTouch = true;
    private bool on = true;

    private void Awake()
    {
        lightOn = transform.GetChild(0).GetComponent<SpriteRenderer>();
        lightOff = GetComponent<SpriteRenderer>();
    }

    public void OnClick()
    {
        if (canTouch) return;
        else StartCoroutine(Light());
    }

    private IEnumerator Light()
    {
        canTouch = false;

        if (on)
            lightOn.DOFade(0, 1.0f);

        else
            lightOn.DOFade(1, 1.0f);
        yield return new WaitForSeconds(1.0f);
        on = !on;
        canTouch = true;
        yield break;
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        if(canTouch)
        {
            StartCoroutine(Light());
        }
        return false;
    }
}
