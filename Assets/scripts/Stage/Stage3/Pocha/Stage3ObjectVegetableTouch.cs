﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-02-17
 * 바구니에 들어있는 채소 터치하면 풀링 해주는 스크립트
 * 인터페이스 사용 
 */

public class Stage3ObjectVegetableTouch : MonoBehaviour, TouchableObj
{
    public List<GameObject> vegetablePrefab = new List<GameObject>();
    public Transform createPos;
    public Vector3 vegetableScale = new Vector3(1, 1, 1);

    private List<GameObject> vegetables = new List<GameObject>();
    private GameObject vegetable;
    private List<SpriteRenderer> sprite = new List<SpriteRenderer>();
    public enum State { ORDER, RANDOM }
    public State STATE;

    [Header("앞에 껍데기 SpriteRenderer")]
    public SpriteRenderer frontBox;

    public enum Kind { DESTROY, MOVE }
    public Kind KIND;

    // 이 밑에부터 Move에서만 작동하는 변수
    private bool touch = false;
    private int moveIndex = 0;

    private string layer;

    public string Layer { get => layer; set => layer = value; }

    private void OnEnable()
    {
        int random = Random.Range(0, vegetablePrefab.Count);
        vegetable = Instantiate(vegetablePrefab[random]);
        if (vegetablePrefab[random].name == "Vegetable_Pumpkin")
            STATE = State.ORDER;

        if (createPos == null)
            vegetable.transform.localPosition = Vector3.zero;
        else
            vegetable.transform.position = createPos.position;

        vegetable.transform.localScale = vegetableScale;
        vegetable.transform.SetParent(gameObject.transform);

        for (int i = 0; i < vegetable.transform.childCount; i++)
            vegetables.Add(vegetable.transform.GetChild(i).gameObject);
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        OnClick();
        return false;
    }

    public void OnClick()
    {
        switch (KIND)
        {
            case Kind.DESTROY: DestKind(); break;
            case Kind.MOVE: MoveKind(); break;
            default: break;
        }
    }

    public void DestKind()
    {
        GameObject obj = null;
        if (vegetables.Count == 0)
        {
            // 여기는 바구니 없어지는 부분
            obj = gameObject;
            DestVegetable(obj);
            return;
        }

        switch (STATE)
        {
            case State.ORDER:
                int index = 0;
                obj = vegetables[index];
                break;
            case State.RANDOM:
                int random = Random.Range(0, vegetables.Count);
                obj = vegetables[random];
                break;
        }
        DestVegetable(obj);
    }

    public void MoveKind()
    {
        if (!touch)
        {
            int random = Random.Range(0, 10);

            if (random <= 7)
            {
                StartCoroutine(MoveCor(vegetables[moveIndex++].transform));
                moveIndex %= vegetables.Count;
            }

            else if (random > 7 && random <= 8)
            {
                for (int i = 0; i < vegetables.Count; i++)
                    StartCoroutine(MoveCor(vegetables[i].transform));
            }

            else StartCoroutine(WaleMove(vegetables));
        }
        else return;
    }

    private IEnumerator MoveCor(Transform obj)
    {
        touch = true;
        Vector3 originPos = obj.transform.localPosition;
        float random = Random.Range(0.25f, 0.7f);
        obj.DOLocalMoveY(originPos.y + random, 0.2f).SetEase(Ease.Linear);
        yield return new WaitForSeconds(0.2f);
        obj.DOLocalMoveY(originPos.y, 0.2f).SetEase(Ease.Linear);
        yield return new WaitForSeconds(0.25f);
        touch = false;
        yield break;
    }

    private IEnumerator WaleMove(List<GameObject> vegetable)
    {
        touch = true;
        for (int i = 0; i < vegetable.Count; i++)
        {
            StartCoroutine(WaleMove(vegetable[i].transform));
            yield return new WaitForSeconds(0.1f);
        }
        touch = false;
        yield break;
    }

    private IEnumerator WaleMove(Transform obj)
    {
        Vector3 originPos = obj.transform.localPosition;
        float random = Random.Range(0.5f, 1.0f);
        obj.DOLocalMoveY(originPos.y + random, 0.2f).SetEase(Ease.Linear);
        yield return new WaitForSeconds(0.2f);
        obj.DOLocalMoveY(originPos.y, 0.2f).SetEase(Ease.Linear);
        yield return new WaitForSeconds(0.2f);
        yield break;
    }

    private void DestVegetable(GameObject obj)
    {
        vegetables.Remove(obj);
        obj.SetActive(false);
    }

    public void SetLayer(int num)
    {
        if (frontBox != null)
        {
            frontBox.sortingLayerName = Layer;
            frontBox.sortingOrder += vegetables.Count;
        }

        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sortingLayerName = Layer;
        spriteRenderer.sortingOrder = num - 1;

        for (int i = 0; i < vegetables.Count; i++)
        {
            if (vegetables[i].GetComponent<SpriteRenderer>() != null)
            {   
                vegetables[i].GetComponent<SpriteRenderer>().sortingLayerName = Layer;
                vegetables[i].GetComponent<SpriteRenderer>().sortingOrder += num;
            }
        }
    }
}