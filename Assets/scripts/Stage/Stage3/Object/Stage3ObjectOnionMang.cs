﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage3ObjectOnionMang : MonoBehaviour, TouchableObj
{
    [Header("망 스프라이트들")]
    public Sprite[] mangsprites = new Sprite[3];
    public GameObject onion;

    [Header("양파들 컴포넌트")]
    public List<Collider2D> onionCols = new List<Collider2D>();
    public List<Rigidbody2D> onionRigids = new List<Rigidbody2D>();
    [Header("벽들 컴포넌트")]
    public List<Collider2D> wallCols = new List<Collider2D>();

    [Header("양파가 떨어질 때 필요한 변수들")]
    public float[] dropTime, dropPower;
    public int[] dropCount;

    public enum State
    {
        NEW, HALF, USED
    }

    private State STATE;

    private int index = 0;
    private SpriteRenderer sprite;

    public void PlayTouchEvent()
    {
        switch (STATE)
        {
            case State.NEW:
                sprite.sprite = mangsprites[++index];
                STATE = State.HALF;
                break;
            case State.HALF:
                sprite.sprite = mangsprites[++index];
                SpreadOnion();
                STATE = State.USED;
                break;
            case State.USED:
                break;
        }
    }

    private void Awake()
    {
        sprite = GetComponent<SpriteRenderer>();
    }

    private void SpreadOnion()
    {
        SetWalls(true);
        for (int i = 0; i < onionCols.Count; i++)
        {
            onionCols[i].enabled = true;
            onionRigids[i].bodyType = RigidbodyType2D.Dynamic;
        }
        Invoke("SetNothing", 1.5f);
    }

    private void SetNothing()
    {
        SetWalls(false);
        for (int i = 0; i < onionCols.Count; i++)
        {
            onionCols[i].enabled = false;
            onionRigids[i].bodyType = RigidbodyType2D.Static;
            onionRigids[i].simulated = false;
        }
    }

    private void SetWalls(bool data)
    {
        for(int i = 0; i < wallCols.Count; i++)
        {
            wallCols[i].enabled = data;
        }
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        PlayTouchEvent();
        return false;
    }
}
