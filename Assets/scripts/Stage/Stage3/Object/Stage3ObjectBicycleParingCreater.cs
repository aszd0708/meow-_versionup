﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-04-15
 * 자전거 주차 구역에 있는 자전거 랜덤으로 생성하는 스크립트
 * 꺄 적축으로 타이핑 하니까 넘나 좋은것
 */

public class Stage3ObjectBicycleParingCreater : MonoBehaviour
{
    [Header("자전거 오브젝트들")]
    public List<GameObject> bicycles = new List<GameObject>();
    [Header("최대 최소 몇개 만들까(랜덤으로 결정됨)")]
    public int min, max;

    private void Start()
    {
        SettingBicycles();
    }

    private void SettingBicycles()
    {
        if (max > bicycles.Count)
            max = bicycles.Count;

        int count = bicycles.Count - Random.Range(min, max + 1);
        int indexRandom = 0;

        for(int i = 0; i < count; i++)
        {
            indexRandom = Random.Range(0, bicycles.Count);

            bicycles[indexRandom].SetActive(false);
            bicycles.RemoveAt(indexRandom);
        }
    }
}
