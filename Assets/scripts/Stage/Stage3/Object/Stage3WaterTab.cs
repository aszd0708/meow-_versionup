﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage3WaterTab : MonoBehaviour, TouchableObj
{
    [Header("물 스프라이트 랜더러")]
    public SpriteRenderer water;
    [Header("물 스프라이트들")]
    public Sprite[] waterSprite = new Sprite[2];
    [Header("스프라이트 바뀌는 시간")]
    public float changeTime;

    private Coroutine motionCor;

    private bool turn;

    private void Start()
    {
        turn = IntRandom.StateRandomInt();
        OnClick();
    }

    public void OnClick()
    {
        switch (turn)
        {
            case true:
                if(motionCor != null)
                {
                    StopCoroutine(motionCor);
                }
                break;

            case false:
                motionCor = StartCoroutine(_WaterMotion());
                break;
        }
        turn = !turn;
        water.enabled = turn;
    }

    private IEnumerator _WaterMotion()
    {
        int index = 0;
        WaitForSeconds waitTime = new WaitForSeconds(changeTime);
        while (true)
        {
            water.sprite = waterSprite[index++];
            index %= waterSprite.Length;
            yield return waitTime;
        }
        yield break;
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        OnClick();
        return false;
    }
}
