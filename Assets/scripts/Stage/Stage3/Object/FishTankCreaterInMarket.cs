﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishTankCreaterInMarket : MonoBehaviour
{
    [Header("만들 오브젝트 프리팹")]
    public GameObject[] createPrefab;

    [Header("만들 위치")]
    public Transform createPos;

    [Header("상속 시킬 Transform")]
    public Transform parentTransform;

    [Header("레이어 이름")]
    public string layerName;

    private void Start()
    {
        CreateObj();
    }

    private void CreateObj()
    {
        int random = Random.Range(0, createPrefab.Length);

        GameObject createObj = Instantiate(createPrefab[random]);
        createObj.GetComponent<Stage3OjbectWaterTankFishCreater>().layerName = layerName;
        createObj.transform.position = createPos.position;
        createObj.transform.SetParent(parentTransform);
    }
}
