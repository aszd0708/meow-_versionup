﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage3ObjectBoxCap : MonoBehaviour, TouchableObj
{
    private bool touch;
    public bool Touch { get => touch; set => touch = value; }
    public float[] rots = new float[2];

    public float rotTime;
    public void OnClick()
    {
        if (Touch)
            return;
        StartCoroutine(_Motion());
    }

    private IEnumerator _Motion()
    {
        Touch = true;
        WaitForSeconds waitTime = new WaitForSeconds(rotTime);
        int index = 0;
        for(int i = 0; i < rots.Length * 2; i++)
        {
            transform.DORotate(new Vector3(0, 0, rots[index++]), rotTime);
            index %= rots.Length;
            yield return waitTime;
        }
        Touch = false;
        yield break;
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        OnClick();
        return false;
    }
}
