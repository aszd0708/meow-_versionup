﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-04-14
 * 자전거 스프라이트 변경해주는 스크립트*
 */

public class BicycleSpriteSetting : MonoBehaviour
{
    [Header("각 부분 SpriteRenderer")]
    public SpriteRenderer bodySprite, frontWheelSprite, behindeWheelSprite, basketSprite;
    [Header("각 부분 Sprites")]
    public Sprite[] bodySprites;
    [Header("뒤에서 부터 몇번째 자전거인지")]
    public int order;

    private void Start()
    {
        SettingSprites();
    }

    private void SettingSprites()
    {
        OrderSorting();
        int bodyRandomIndex = Random.Range(0, bodySprites.Length);
        bool randomInt = IntRandom.StateRandomInt(80);

        bodySprite.sprite = bodySprites[bodyRandomIndex];
        behindeWheelSprite.enabled = randomInt;
    }

    private void OrderSorting()
    {
        switch(order)
        {
            case 1:
                bodySprite.sortingOrder = 3;
                basketSprite.sortingOrder = 3;
                frontWheelSprite.sortingOrder = 4;
                behindeWheelSprite.sortingOrder = 4;
                break;
            case 2:
                bodySprite.sortingOrder = 6;
                basketSprite.sortingOrder = 6;
                frontWheelSprite.sortingOrder = 7;
                behindeWheelSprite.sortingOrder = 7;
                break;
            default:
                bodySprite.sortingOrder = 10;
                basketSprite.sortingOrder = 10;
                frontWheelSprite.sortingOrder = 11;
                behindeWheelSprite.sortingOrder = 11;
                break;
        }
    }
}
