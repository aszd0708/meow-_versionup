﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-03-12
 * 물고기 헤엄치는 프레임 조절
 * 초단위
 */
public class Stage3ObjectFishCat : MonoBehaviour, TouchableObj
{
    [Header("물고기가 움직일때 스프라이트 프레임 조절")]
    public float minTime, maxTime;
    public float changeTime;

    public List<Sprite> fishSprite = new List<Sprite>();

    public float flyingPos;

    private SpriteRenderer spriteRenderer;

    private Stage3ObjectFishMovement move;

    private bool canTouch = true;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        move = GetComponent<Stage3ObjectFishMovement>();
    }

    private void Start()
    {
        StartCoroutine(Motion());
    }

    private IEnumerator Motion()
    {
        int num = 0;
        while (true)
        {
            spriteRenderer.sprite = fishSprite[num++];
            yield return new WaitForSeconds(changeTime);
            num %= fishSprite.Count;
        }
    }

    public void SetTimer(float time)
    {
        StartCoroutine(SetTime(time));
    }

    private IEnumerator SetTime(float time)
    {
        float timeDiv = time / 10;
        changeTime = minTime;
        for (float i = 0; i < time; i += timeDiv)
        {
            changeTime += timeDiv;
            yield return new WaitForSeconds(timeDiv);
        }
        changeTime = maxTime;
        yield break;
    }

    private IEnumerator TouchMotion()
    {
        canTouch = false;
        move.MotionCoroutineController(false);

        Vector3 nowPos = transform.localPosition;
        transform.DOLocalMoveY(nowPos.y + flyingPos, 0.3f).SetEase(Ease.Linear);
        yield return new WaitForSeconds(0.3f);
        for (int i = 0; i < Random.Range(1, 4); i++)
        {
            transform.DORotate(new Vector3(0, 0, 360), 0.5f, RotateMode.FastBeyond360).SetEase(Ease.Linear);
            yield return new WaitForSeconds(0.5f);
        }
        transform.DOLocalMoveY(nowPos.y, 0.5f).SetEase(Ease.Linear);
        yield return new WaitForSeconds(0.5f);

        move.MotionCoroutineController(true);
        canTouch = true;

        yield break;
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        if(canTouch)
        {
            StartCoroutine(TouchMotion());
        }
        return false;
    }
}
