﻿using BitBenderGames;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlidObjectMK2 : MonoBehaviour
{
    public enum position
    { LEFT, RIGHT }

    public position pos;
    public bool touch;
    public Transform other;
    [Header("최대 얼마나 움직일 수 있는지 정하는 변수")]
    public float slideDistance;

    private Vector2 movePos;
    private Vector2 originPos;
    private Vector2 nowPos;
    private float posZ;
    private Vector3 firstT;
    private Vector3 downPos;
    private Vector3 nPos;

    private BoxCollider2D otherCollider;
    private SlidObjectMK2 otherComponenet;

    private TouchInputController camTouch;
    private void Awake()
    {
        otherCollider = other.GetComponent<BoxCollider2D>();
        otherComponenet = other.GetComponent<SlidObjectMK2>();
        camTouch = FindObjectOfType<TouchInputController>();
    }

    void Start()
    {
        if (pos == position.LEFT)
            posZ = -2;
        else if (pos == position.RIGHT)
            posZ = -1;

        originPos = transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            downPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }

        if (touch)
        {
            camTouch.enabled = false;
            if (Input.GetMouseButton(0))
            {
                if (other.localPosition.x < otherComponenet.originPos.x - 0.3f)
                {
                    GetComponent<BoxCollider2D>().enabled = false;
                }
                else if (other.localPosition.x >= otherComponenet.originPos.x - 0.3f)
                {
                    otherCollider.enabled = true;
                    nPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                    switch (pos)
                    {
                        case position.LEFT:
                            //transform.localPosition = new Vector3((originPos.x) - (downPos.x - nPos.x), originPos.y, posZ);
                            transform.position = new Vector3(nPos.x, originPos.y, 0);
                            gameObject.transform.localPosition = new Vector3(Mathf.Clamp(transform.localPosition.x, originPos.x, originPos.x + slideDistance),
                                                                            Mathf.Clamp(nowPos.y, originPos.y, originPos.y), posZ);
                            break;
                        case position.RIGHT:
                            //transform.localPosition = new Vector3((originPos.x) - (downPos.x - nPos.x), originPos.y, posZ);
                            transform.position = new Vector3(nPos.x, originPos.y, 0);
                            gameObject.transform.localPosition = new Vector3(Mathf.Clamp(transform.localPosition.x, originPos.x - slideDistance, originPos.x),
                                                                            Mathf.Clamp(nowPos.y, originPos.y, originPos.y), posZ);
                            break;
                    }

                    downPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                }
            }

            else if (Input.GetMouseButtonUp(0))
            {
                touch = false;
                camTouch.enabled = true;
            }
            switch (pos)
            {
                case position.LEFT:
                    break;
                case position.RIGHT:
                    break;
            }
        }
    }
}
