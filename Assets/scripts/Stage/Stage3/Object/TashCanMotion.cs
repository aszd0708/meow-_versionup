﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

/*
 * 2020-03-06
 * 쓰래기 통 터치 했을때 모션
 * 따꿍 흔들 몇번 후 따꿍 날아감
 */

public class TashCanMotion : MonoBehaviour, I_NonColObject
{
    [Header("몇번 터치 했을 때 따꿍 날아가는지")]
    public int minCount, maxCount;
    [Header("따꿍 착지 위치")]
    public Transform capPos;
    [Header("흔들릴때 스프라이트")]
    public Transform shakeCap;
    [Header("날아갈때 스프라이트")]
    public Transform jumpingCap;

    private bool touch = false;
    private bool end = false;

    private int touchCount = 0;
    private int count;

    private SpriteRenderer jumpingCapSprite;
    private SpriteRenderer shakeCapSprite;

    public SpriteRenderer ShakeCapSprite { get => shakeCapSprite; set => shakeCapSprite = value; }
    public SpriteRenderer JumpingCapSprite { get => jumpingCapSprite; set => jumpingCapSprite = value; }

    public void Awake()
    {
        JumpingCapSprite = jumpingCap.GetComponent<SpriteRenderer>();
        ShakeCapSprite = shakeCap.GetComponent<SpriteRenderer>();
    }

    public void Start()
    {
        JumpingCapSprite.enabled = false;
        ShakeCapSprite.enabled = true;

        count = Random.Range(minCount, maxCount + 1);
    }

    public void OnClick()
    {
        if (touch || end) return;
        StartCoroutine(Motion());
    }

    private IEnumerator Motion()
    {
        touch = true;
        touchCount++;

        if(touchCount >= count)
        {
            JumpingCapSprite.enabled = true;
            ShakeCapSprite.enabled = false;
            jumpingCap.DORotate(new Vector3(0, 0, -180), 0.5f);
            jumpingCap.DOLocalJump(capPos.localPosition, 1, 1, 0.75f);
            yield return new WaitForSeconds(0.75f);
            end = true;
        }

        else
        {
            shakeCap.DOShakeRotation(0.5f, 50, 10, 45);
            yield return new WaitForSeconds(0.5f);
        }
        touch = false;
        yield break;
    }
}
