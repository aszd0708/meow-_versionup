﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-04-26
 * 술집 오브젝트 썰것하고 칼 바꿔주는 스크립트
 */

public class NogaritJRObjChanger : MonoBehaviour
{
    public enum ObjKind
    {
        MEAT, FISH
    }

    [System.Serializable]
    public struct ObjValue
    {
        public Quaternion rot;
        public ObjKind KIND;
        public Sprite objSprite;
    }

    [Header("각 부분별 스프라이트랜더러")]
    public SpriteRenderer knife;
    public SpriteRenderer cuttingObj;

    [Header("각 종류별 오브젝트 값")]
    public ObjValue[] objs;
    public Sprite meatKnife, fishKnife;

    private void Start()
    {
        SetSprite();
    }

    private void SetSprite()
    {
        int randomValue = Random.Range(0, objs.Length);

        cuttingObj.sprite = objs[randomValue].objSprite;
        cuttingObj.transform.rotation = objs[randomValue].rot;
        switch (objs[randomValue].KIND)
        {
            case ObjKind.MEAT: knife.sprite = meatKnife; break;
            case ObjKind.FISH: knife.sprite = fishKnife; break;
        }
    }
}
