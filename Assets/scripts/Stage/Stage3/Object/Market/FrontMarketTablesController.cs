﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-04-27
 * 마켓 앞에 테이블들 사람 및 음식들 설치해주는 스크립트
 */

public class FrontMarketTablesController : MonoBehaviour
{
    [Header("사람들 오브젝트")]
    public GameObject[] people = new GameObject[2];

    [Header("의자 오브젝트")]
    public SpriteRenderer[] chairs = new SpriteRenderer[2]; 

    [Header("오브젝트들이 살아있을 확률")]
    public int chairLivePercent;
    public int personLivePercent, foodLivePercent;

    public enum FoodKind { ALCOHOL, FOOD, ELSE }

    [System.Serializable]
    public struct FoodSprite
    {
        public SpriteRenderer foodSpriteRenderer;
        public FoodKind KIND;
    }

    [Header("음식 SpriteRenderer")]
    public FoodSprite[] foods;
    [Header("음식 Sprites")]
    public Sprite[] alcoholSprites;
    public Sprite[] foodSprites, elseSprites;

    private bool[] personLive = new bool[2];

    private void Start()
    {
        SettingPersonObj(); 
        SettingFoods();
    }

    private void SettingPersonObj()
    {
        for (int i = 0; i < people.Length; i++)
        {
            bool live = IntRandom.StateRandomInt(personLivePercent);

            if (!live)
            {
                people[i].SetActive(false);
                personLive[i] = false;
            }
            else personLive[i] = true;
        }

        for(int i = 0; i < chairs.Length; i++)
        {
            if(!personLive[i])
            {
                bool live = IntRandom.StateRandomInt(chairLivePercent);
                if (!live)
                    chairs[i].enabled = false;
            }
        }
    }

    private void SettingFoods()
    {
        for(int i = 0; i < foods.Length; i++)
        {
            bool live = IntRandom.StateRandomInt(foodLivePercent);

            if (live)
                FoodKindSetting(foods[i]);
            else foods[i].foodSpriteRenderer.enabled = false;
        }
    }

    private void FoodKindSetting(FoodSprite food)
    {
        switch(food.KIND)
        {
            case FoodKind.ALCOHOL:
                food.foodSpriteRenderer.sprite = alcoholSprites[Random.Range(0, alcoholSprites.Length)];
                break;
            case FoodKind.FOOD:
                food.foodSpriteRenderer.sprite = foodSprites[Random.Range(0, foodSprites.Length)];
                break;
            case FoodKind.ELSE:
                food.foodSpriteRenderer.sprite = elseSprites[Random.Range(0, elseSprites.Length)];
                break;
        }
    }
}
