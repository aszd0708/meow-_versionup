﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-04-16
 * 상점들 판때기에 야채나 그런것들 만들어내는 스크립트
 * 생각보다 복잡하게 만들었다...
 */

public class MarketTableVegetableCteater : MonoBehaviour
{
    [Header("만들 갯수")]
    public int createCount;
    [Header("만들 오브젝트들")]
    public GameObject[] createBasketObj;
    public GameObject[] createNonBasketObj;
    [Header("만들 포지션")]
    public List<Transform> createTransforms = new List<Transform>();
    [Header("레이어 관란")]
    public string sortingLayerName;
    public int sortingLayerOrder;

    public enum Kind { BASKET, NONBASKET }
    public Kind KIND;

    [Header("만들때 SpriteRenderer 전부 갖고오는 리스트")]
    private List<SpriteRenderer> spriteRenderers = new List<SpriteRenderer>();

    private void Start()
    {
        CreateObj();
    }

    private Kind CheckingKind()
    {
        if (createBasketObj.Length == 0)
            return Kind.NONBASKET;
        else if (createNonBasketObj.Length == 0)
            return Kind.NONBASKET;

        else
        {
            Kind OBJKIND;

            bool basket = IntRandom.StateRandomInt();

            if (basket)
                OBJKIND = Kind.BASKET;
            else
                OBJKIND = Kind.NONBASKET;

            return OBJKIND;
        }
    }

    private void CreateObj()
    {
        if (createCount > createTransforms.Count)
            createCount = createTransforms.Count;

        for (int i = 0; i < createCount; i++)
        {
            if (!IntRandom.StateRandomInt(80))
                continue;
            else
            {
                int randomPositionIndex = Random.Range(0, createTransforms.Count);

                GameObject create;

                if(createNonBasketObj.Length <=0)
                {
                    create = Instantiate(createBasketObj[Random.Range(0, createBasketObj.Length)]);
                }
                else
                {
                    if (IntRandom.StateRandomInt())
                    {
                        create = Instantiate(createBasketObj[Random.Range(0, createBasketObj.Length)]);
                    }
                    else
                    {
                        if (CheckingKind() == Kind.BASKET)
                            return;
                        create = Instantiate(createNonBasketObj[Random.Range(0, createNonBasketObj.Length)]);
                    }
                }

                create.transform.position = createTransforms[randomPositionIndex].position;
                create.transform.SetParent(createTransforms[randomPositionIndex]);

                if (create.GetComponent<SpriteRenderer>() != null)
                {
                    create.GetComponent<SpriteRenderer>().sortingLayerName = sortingLayerName;
                    create.GetComponent<SpriteRenderer>().sortingOrder = 1;
                }

                switch (CheckingKind())
                {
                    case Kind.BASKET:
                        GetAllSpriteRenderer(create.transform);
                        SetSpritesInfo(sortingLayerName, sortingLayerOrder);
                        break;
                    case Kind.NONBASKET:
                        break;
                }

                if (create.GetComponent<Stage3ObjectVegetableTouch>() != null)
                {
                    create.GetComponent<Stage3ObjectVegetableTouch>().Layer = sortingLayerName;
                    create.GetComponent<Stage3ObjectVegetableTouch>().SetLayer(2);
                }
                createTransforms.RemoveAt(randomPositionIndex);
            }
        }
    }

    private void GetAllSpriteRenderer(Transform parent)
    {
        for (int i = 0; i < parent.childCount; i++)
        {
            if (parent.GetChild(i).childCount >= 0)
                GetAllSpriteRenderer(parent.GetChild(i));

            if (parent.GetChild(i).GetComponent<SpriteRenderer>() != null)
                spriteRenderers.Add(parent.GetChild(i).GetComponent<SpriteRenderer>());
        }
    }

    private void SetSpritesInfo(string layerName, int layerOrder)
    {
        for (int i = 0; i < spriteRenderers.Count; i++)
        {
            spriteRenderers[i].sortingLayerName = layerName;
            spriteRenderers[i].sortingOrder = layerOrder;
        }
        spriteRenderers.Clear();
    }
}
