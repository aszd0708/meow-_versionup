﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

/*
 * 2020-04-13
 * 손수레나 뭐 그런것들 움직이게 만드는 스크립트
 */

public class Stage3ObjectRoller : MonoBehaviour, TouchableObj
{
    [Header("바퀴들 Transform")]
    public Transform[] wheels = new Transform[2];

    [Header("움직이는 거리")]
    public float moveDistance;

    [Header("움직이는 시간")]
    public float moveTime;

    private bool right;

    private bool touch;
    public bool Touch { get => touch; set => touch = value; }

    private void Start()
    {
        RandomPos();
    }

    private void RandomPos()
    {
        right = IntRandom.StateRandomInt();

        if (right)
            transform.position = new Vector3(transform.position.x + moveDistance, transform.position.y);
    }

    public void OnClick()
    {
        if (Touch)
            return;
        StartCoroutine(_MovingRoller());
    }

    private IEnumerator _MovingRoller()
    {
        Touch = true;

        float movePos, boolNum;
        if (right) boolNum = -1;
        else boolNum = 1;
        movePos = transform.position.x + (moveDistance * boolNum);
        right = !right;

        transform.DOMoveX(movePos, moveTime).SetEase(Ease.OutCubic);
        for (int i = 0; i < wheels.Length; i++)
            wheels[i].DORotate(new Vector3(0, 0, 720f * -boolNum), moveTime, RotateMode.FastBeyond360).SetEase(Ease.OutCubic);
        yield return new WaitForSeconds(moveTime);

        Touch = false;
        yield break;
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        OnClick();
        return false;
    }
}
