﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartRandomRot : MonoBehaviour
{
    private SpriteRenderer sprite;

    void Awake()
    {
        sprite = GetComponent<SpriteRenderer>();
    }
    // Start is called before the first frame update
    void Start()
    {
        int random = Random.Range(0, 4);
        switch(random)
        {
            case 0:
                sprite.flipX = true;
                sprite.flipY = true;
                break;
            case 1:
                sprite.flipX = true;
                sprite.flipY = false;
                break;
            case 2:
                sprite.flipX = false;
                sprite.flipY = false;
                break;
            case 3:
                sprite.flipX = false;
                sprite.flipY = true;
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
