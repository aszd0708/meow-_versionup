﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NightMarketSpriteOnOff : MonoBehaviour
{
    public SpriteRenderer example;

    private bool turn;

    public bool Turn { get => turn; set => turn = value; }

    private void Start()
    {
        Turn = example.enabled;
    }

    private void TurnOnOff()
    {
        Turn = !Turn;
        example.enabled = Turn;
    }
}
