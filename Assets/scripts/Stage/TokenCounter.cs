﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TokenCounter : MonoBehaviour
{
    private int tokenCount;

    public int TokenCount
    {
        get { return tokenCount; }
        set { tokenCount = value; }
    }

    void Start()
    {
        TokenCount = EncryptedPlayerPrefs.GetInt("tokenCount", 1);
        Debug.Log("토큰 갯수" + TokenCount);
    }

    public void SetToken(int count)
    {
        if (count < 0)
            count = 0;
        EncryptedPlayerPrefs.SetInt("tokenCount", count);
        TokenCount = count;
    }

    public void MinToken()
    {
        SetToken(TokenCount - 1);
    }

    public void PlusToken(int num)
    {
        SetToken(TokenCount + num);
    }

    public int GetTokenCount()
    {
        return tokenCount;
    }
}
