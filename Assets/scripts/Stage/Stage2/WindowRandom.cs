﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowRandom : MonoBehaviour
{
    public GameObject[] windowObjects;
    private int num = 0;
    // Start is called before the first frame update
    void Start()
    {
        num = Random.Range(0, windowObjects.Length);
        for(int i = 0; i < windowObjects[num].transform.childCount; i++)
        {
            GameObject objects = Instantiate(windowObjects[num].transform.GetChild(i).gameObject);

            if(objects.transform.childCount > 0)
            {
                for(int a = 0; a < objects.transform.childCount; a++)
                    objects.transform.GetChild(a).GetComponent<SpriteRenderer>().sortingLayerName = GetComponent<SpriteRenderer>().sortingLayerName;
            }
            if(objects.GetComponent<SpriteRenderer>() != null)
                objects.GetComponent<SpriteRenderer>().sortingLayerName = GetComponent<SpriteRenderer>().sortingLayerName;

            objects.transform.SetParent(gameObject.transform);
            objects.transform.localPosition = windowObjects[num].transform.GetChild(i).gameObject.transform.localPosition;
            if(transform.parent.name == "Window_L")
            {
                objects.transform.localPosition = new Vector3(objects.transform.localPosition.x, -objects.transform.localPosition.y, objects.transform.localPosition.z);
                objects.transform.rotation = Quaternion.Euler(objects.transform.rotation.x, 180, objects.transform.rotation.z);
            }
        }
    }
}
