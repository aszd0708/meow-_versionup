﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarDrive : MonoBehaviour
{
    public enum State
    {
        left, right
    }
    public Transform endPos;
    public float speed;
    public GameObject front;
    public GameObject back;
    public float distance;
    public State state;

    private string poolingName;
    public string PoolingName { get => poolingName; set => poolingName = value; }

    private float driveSpeed = 0;


    void OnEnable()
    {
        driveSpeed = speed;
        driveSpeed = 300;
        StartCoroutine(Drive());
    }

    private IEnumerator Drive()
    {
        yield return new WaitForSeconds(0.05f);
        while (true)
        {
            driveSpeed = Mathf.Clamp(driveSpeed, 0, speed);
            switch (state)
            {
                case State.left:
                    LeftDrive();
                    break;

                case State.right:
                    RightDrive();   
                    break;
            }
            yield return new WaitForFixedUpdate();
        }
    }

    private void LeftDrive()
    {
        if (front == gameObject)
            UpSpeed();

        else if (front.transform.localPosition.x - gameObject.transform.localPosition.x < distance)
            DownSpeed();

        else
            UpSpeed();

        transform.position = new Vector3(transform.position.x + driveSpeed * Time.deltaTime, transform.position.y, transform.position.z);

        if (transform.position.x >= endPos.transform.position.x)
        {
            back.GetComponent<CarDrive>().front = back;
            PoolingManager.Instance.SetPool(gameObject, PoolingName);
        }
    }

    private void RightDrive()
    {
        if (front == gameObject)
            UpSpeed();

        else if (gameObject.transform.localPosition.x - front.transform.localPosition.x < distance)
            DownSpeed();

        else
            UpSpeed();

        transform.position = new Vector3(transform.position.x - driveSpeed * Time.deltaTime, transform.position.y, transform.position.z);

        if (transform.position.x <= endPos.transform.position.x)
        {
            back.GetComponent<CarDrive>().front = back;
            PoolingManager.Instance.SetPool(gameObject, PoolingName);
        }
    }

    private void UpSpeed()
    {
        driveSpeed = driveSpeed + 0.1f;
    }

    private void DownSpeed()
    {
        driveSpeed = driveSpeed - 0.3f;
    }
}
