﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2ObjectDuckMove : MonoBehaviour
{
    public Transform[] pos = new Transform[2];
    public float movingTime;

    public GameObject baby;
    public enum Dir
    {
        right, left, idle
    }

    void Start()
    {
        GameObject prevDuck = null;
        for(int i = 0; i < Random.Range(0,5); i++)
        {
            GameObject babyDuck = Instantiate(baby);
            babyDuck.transform.SetParent(transform.parent);
            if (i == 0)
            {
                babyDuck.transform.position = transform.GetChild(0).transform.position;
                babyDuck.GetComponent<BabyduckMove>().front = transform.GetChild(0);
            }

            else
            {
                babyDuck.transform.position = prevDuck.transform.GetChild(0).transform.position;
                babyDuck.GetComponent<BabyduckMove>().front = prevDuck.transform.GetChild(0);
            }
            
            babyDuck.transform.localScale = new Vector3(0.55f - i * 0.05f, 0.55f - i * 0.05f, 0);
            prevDuck = babyDuck;
        }
        StartCoroutine(Moving());
    }    

    private IEnumerator Moving()
    {
        while (true)
        {
            float randomMoveTime = Random.Range(0.3f, movingTime);
            Dir dir = (Dir)Random.Range((int)Dir.right, (int)Dir.idle + 1);

            switch(dir)
            {
                case Dir.right:
                    if (Mathf.Abs(pos[(int)dir].localPosition.x - transform.localPosition.x) > 1.0f)
                    {
                        //transform.DOLocalMoveX(transform.localPosition.x + Random.Range(0.3f, 1.0f), randomMoveTime);
                        transform.DOLocalMove(new Vector3(transform.localPosition.x + Random.Range(0.3f, 1.0f), transform.localPosition.y + Random.Range(-0.05f, 0.05f), transform.localPosition.z), randomMoveTime);
                        //GetComponent<SpriteRenderer>().flipX = true;
                        transform.rotation = Quaternion.Euler(0, 180, 0);
                        yield return new WaitForSeconds(randomMoveTime);
                    }
                    break;
                case Dir.left:
                    if (pos[(int)dir].localPosition.x - transform.localPosition.x < -1.0f)
                    {
                        //transform.DOLocalMoveX(transform.localPosition.x - Random.Range(0.3f, 1.0f), randomMoveTime);
                        transform.DOLocalMove(new Vector3(transform.localPosition.x - Random.Range(0.3f, 1.0f), transform.localPosition.y + Random.Range(-0.05f, 0.05f), transform.localPosition.z), randomMoveTime);
                        //GetComponent<SpriteRenderer>().flipX = false;

                        transform.rotation = Quaternion.Euler(0, 0, 0);
                        yield return new WaitForSeconds(randomMoveTime);
                    }
                    break;
                case Dir.idle:
                    yield return new WaitForSeconds(randomMoveTime);
                    break;
                default:
                    break;
            }
            yield return new WaitForSeconds(randomMoveTime);
        }
    }
}