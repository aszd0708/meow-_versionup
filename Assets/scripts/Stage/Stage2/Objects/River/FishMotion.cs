﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishMotion : MonoBehaviour
{
    public Sprite[] fishIdleSprite;
    public float motionTime;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("Motion");
    }
    
    private IEnumerator Motion()
    {
        while(true)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = fishIdleSprite[0];
            yield return new WaitForSeconds(motionTime);
            gameObject.GetComponent<SpriteRenderer>().sprite = fishIdleSprite[1];
            yield return new WaitForSeconds(motionTime);
        }
    }
}
