﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BabyduckMove : MonoBehaviour
{
    public Transform front;

    public float speed;
    public float distance;
    public Sprite[] duckSpirte = new Sprite[2];
    private float driveSpeed = 0;

    void Start()
    {
        GetComponent<SpriteRenderer>().sprite = duckSpirte[Random.Range(0, 2)];
        driveSpeed = speed;
    }

    void Update()
    {
        driveSpeed = Mathf.Clamp(driveSpeed, 0, speed);

        if(front.rotation.y == 1)
            transform.rotation = Quaternion.Euler(0, 180, 0);

        else if(front.rotation.y == 0)
            transform.rotation = Quaternion.Euler(0, 0, 0);

        if (front.position.x - gameObject.transform.position.x <= -distance)
            DownSpeed();
        else
            UpSpeed();

        if (Mathf.Abs(front.position.x - gameObject.transform.position.x) < 0.01f)
            driveSpeed = 0;

        transform.position = new Vector3(transform.position.x + driveSpeed * Time.deltaTime, front.position.y, transform.position.z);
    }
    
    private void UpSpeed()
    {
        driveSpeed = driveSpeed + 0.8f;
    }

    private void DownSpeed()
    {
        driveSpeed = driveSpeed - 1f;
    }
}
