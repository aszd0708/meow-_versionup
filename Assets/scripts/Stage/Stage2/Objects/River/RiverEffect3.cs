﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RiverEffect3 : MonoBehaviour
{
    public GameObject whale;
    public float motionTime;
    public GameObject effectPos;
    private Vector2[] effectPosV = new Vector2[11];
    private float distance = 0.25f;
    private int count;
    private GameObject[] createWale;
    // Start is called before the first frame update
    void Start()
    {
        int randomCount = Random.Range(3, 5);
        createWale = new GameObject[randomCount + 1];
        for(int i = 0; i < randomCount; i++)
        {
            GameObject whales = Instantiate(whale);
            whales.transform.SetParent(transform);
            whales.transform.localScale = new Vector3(1, 1, 0);
            whales.transform.localPosition = new Vector3(-distance + i * distance, 0, 0);
            createWale[i] = whales;
        }
        count = transform.childCount;

        for(int i = 0; i < effectPos.transform.childCount; i++)
            effectPosV[i] = effectPos.transform.GetChild(i).transform.localPosition;
        StartCoroutine(WhaleMove());
    }
    
    private IEnumerator WhaleMove()
    {
        while(true)
        {
            int random = Random.Range(0, effectPosV.Length);
            transform.localPosition = new Vector3(effectPosV[random].x, effectPosV[random].y, -2);
            for(int i = 0; i < count; i++)
            {
                StartCoroutine(WhaleMotion(createWale[i]));
                yield return new WaitForSeconds(motionTime / 2);
            }
            yield return new WaitForSeconds(motionTime * (count + 1));
        }
    }

    private IEnumerator WhaleMotion(GameObject whale)
    {
        whale.GetComponent<SpriteRenderer>().enabled = true;
        yield return new WaitForSeconds(motionTime);
        whale.GetComponent<SpriteRenderer>().enabled = false;
        yield break;
    }
}
