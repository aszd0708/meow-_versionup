﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RiverEffect : MonoBehaviour
{
    public Sprite[] effect = new Sprite[2];
    public GameObject effectPos;

    private Vector2[] effectPosV = new Vector2[11];
    void Start()
    {
        for (int i = 0; i < effectPos.transform.childCount; i++)
            effectPosV[i] = effectPos.transform.GetChild(i).transform.localPosition;
        StartCoroutine("Motion");
    }

    private IEnumerator Motion()
    {
        while (true)
        {
            int num = Random.Range(0, effectPos.transform.childCount);
            gameObject.transform.localPosition = new Vector3(effectPosV[num].x, effectPosV[num].y, -2);
            gameObject.GetComponent<SpriteRenderer>().enabled = true;
            for(int i = 0; i < 2; i++)
            {
                if (gameObject.GetComponent<SpriteRenderer>().sprite == effect[0])
                    gameObject.GetComponent<SpriteRenderer>().sprite = effect[1];

                else if (gameObject.GetComponent<SpriteRenderer>().sprite == effect[1])
                    gameObject.GetComponent<SpriteRenderer>().sprite = effect[0];

                yield return new WaitForSeconds(0.5f);
            }
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            yield return new WaitForSeconds(Random.Range(1.0f, 2.5f));
        }
    }
}
