﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2ObjekctLeafMove : MonoBehaviour, TouchableObj
{
    public List<Sprite> sprite = new List<Sprite>();
    public Transform[] pos = new Transform[2]; // 0이 왼쪽
    public List<GameObject> smallLeafs = new List<GameObject>();
    public List<Vector3> lPos = new List<Vector3>();
    private Vector3[] movePos = new Vector3[2];
    private bool touch;
    private bool left;

    [SerializeField]
    private bool bRandomPose = true;
    
    void Start()
    {
        int spriteNum = Random.Range(0, sprite.Count);
        for (int i = 0; i < smallLeafs.Count; i++)
            SetSmallLeaf(smallLeafs[i]);

        touch = false;
        movePos[0] = pos[0].position;
        movePos[1] = pos[1].position;

        if(bRandomPose == true)
        {
            int random = Random.Range(0, 2);
            transform.position = movePos[random];
            if (random == 0)
                left = true;
            else
                left = false;
        }
        else
        {
            int temp = 0;
            transform.position = movePos[temp];
            left = true;
        }
    }

    private void SetSmallLeaf(GameObject leaf)
    {
        Transform leafPos = leaf.transform;
        SpriteRenderer leafSprite = leaf.GetComponent<SpriteRenderer>();
        int random = Random.Range(0, lPos.Count);
        leafPos.localPosition = lPos[random];
        lPos.RemoveAt(random);
        leafSprite.sprite = sprite[Random.Range(0, sprite.Count)];
    }

    private IEnumerator Move()
    {
        touch = true;
        if(left)
            transform.DOMove(movePos[1], 1.5f).SetEase(Ease.OutCubic);
        else
            transform.DOMove(movePos[0], 1.5f).SetEase(Ease.OutCubic);
        left = !left;
        yield return new WaitForSeconds(1.5f);
        touch = false;
        yield break;
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        if (!touch)
        {
            AudioManager.Instance.PlaySound("slide_4", transform.position);
            StartCoroutine(Move());
        }
        return false;
    }

    public void MoveObject()
    {
        StartCoroutine(Move());
    }
}
