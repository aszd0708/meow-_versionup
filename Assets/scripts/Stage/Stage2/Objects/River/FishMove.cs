﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FishMove : MonoBehaviour
{
    [SerializeField]
    private Transform[] routes;

    private int routeToGo;

    private float tParm;

    private Vector2 fishPosition;

    private float speedModigier;

    private bool coroutineAllowed;

    private Vector2 direction;


    void Start()
    {
        routeToGo = 0;
        tParm = 0.0f;
        speedModigier = Random.Range(0.1f, 0.4f);
        coroutineAllowed = true;
    }

    void Update()
    {
        if (coroutineAllowed)
            StartCoroutine(GoByTheRoute(routeToGo));
    }

    private IEnumerator GoByTheRoute(int routeNumber)
    {
        coroutineAllowed = false;

        Vector2 p0 = routes[routeNumber].GetChild(0).position;
        Vector2 p1 = routes[routeNumber].GetChild(1).position;
        Vector2 p2 = routes[routeNumber].GetChild(2).position;
        Vector2 p3 = routes[routeNumber].GetChild(3).position;

        while(tParm < 1)
        {
            tParm += Time.deltaTime * speedModigier;

            fishPosition = Mathf.Pow(1 - tParm, 3) * p0 +
                3 * Mathf.Pow(1 - tParm, 2) * tParm * p1 +
                3 * (1 - tParm) * Mathf.Pow(tParm, 2) * p2 +
                Mathf.Pow(tParm, 3) * p3;
            direction = new Vector2(fishPosition.x - transform.position.x, fishPosition.y - transform.position.y);

            transform.up = direction;

            transform.position = fishPosition;

            

            yield return new WaitForEndOfFrame();
        }

        tParm = 0f;

        routeToGo += 1;

        if (routeToGo > routes.Length - 1)
            routeToGo = 0;
        coroutineAllowed = true;
    }
}
