﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BugMoveSwitch : MonoBehaviour
{
    private List<Transform> bug = new List<Transform>();

  
    void Start()
    {
        //Destroy(gameObject, 1.5f);
    }

    private void Update()
    {
        //for (int i = 0; i < bug.Count; i++)
        //    if (bug[i].localPosition.y >= 0.51f || bug[i].localPosition.y <= -0.50f)
        //    {
        //        bug[i].GetComponent<LaserMove>().enabled = false;
        //        bug[i].GetComponent<SpriteRenderer>().enabled = false;
        //    }
    }

    public void On()
    {
        for (int i = 0; i < transform.childCount; i++)
            bug.Add(transform.GetChild(i).gameObject.transform);

        for (int i = 0; i < bug.Count; i++)
        {
            bug[i].GetComponent<LaserMove>().enabled = true;
            bug[i].GetComponent<LaserMove>().speed[0] /= 1.2f;
            bug[i].GetComponent<LaserMove>().speed[1] /= 1.2f;
            bug[i].GetComponent<SpriteRenderer>().sortingOrder = 5;
        }

        PoolingManager.Instance.SetPool(gameObject, "Bug", 1.5f);
    }
}
