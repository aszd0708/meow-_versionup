﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2ObjectSuperDoor : MonoBehaviour, TouchableObj
{
    public GameObject[] door;
    public float motionTime;

    private Vector3[] doorPos = new Vector3[3];
    public bool isOpen;
    private bool canTouch;
    void Start()
    {
        isOpen = false;
        canTouch = true;
        for (int i = 0; i < 3; i++)
            doorPos[i] = door[i].transform.localPosition;
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        if(canTouch)
        {
            StartCoroutine(DoorMotion());
        }
        return false;
    }

    public void OpenDoor()
    {
        StartCoroutine(OpenDoorMotion());
    }

    private IEnumerator DoorMotion()
    {
        if(isOpen)
        {
            door[0].transform.DOLocalMoveX(doorPos[0].x, motionTime);
            door[1].transform.DOLocalMoveX(doorPos[1].x, motionTime);
            isOpen = false;
        }

        else if(!isOpen)
        {
            door[0].transform.DOLocalMoveX(doorPos[2].x, motionTime);
            door[1].transform.DOLocalMoveX(doorPos[2].x, motionTime);
            isOpen = true;
        }

        canTouch = false;
        yield return new WaitForSeconds(motionTime);
        canTouch = true;
        yield break;
    }

    private IEnumerator OpenDoorMotion()
    {
        canTouch = false;
        door[0].transform.DOLocalMoveX(doorPos[2].x, motionTime);
        door[1].transform.DOLocalMoveX(doorPos[2].x, motionTime);
        yield return new WaitForSeconds(motionTime);
        canTouch = true;
        yield break;
    }

    public float OpenDoorAndMoveCat()
    {
        StopAllCoroutines();
        if(!isOpen)
        {
            StartCoroutine(DoorMotion());
            return motionTime;
        }

        else
        {
            return 0;
        }
    }
}
