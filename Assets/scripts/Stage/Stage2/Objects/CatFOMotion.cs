﻿using BitBenderGames;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatFOMotion : MonoBehaviour
{
    public GameObject cam;
    public Transform beemMask;
    public Transform[] catilien = new Transform[2];
    public Transform boxCat;
    public Transform catFOBody;
    public Transform destination;
    public SpriteRenderer beem;
    public Vector3[] movePos = new Vector3[2];
    public float moveTime;
    //public TouchInterationCam touch;
    public TouchManager touchManager;
    public Transform item;
    private bool end;
    private Vector3 maskPos;
    private Vector3 pos;
    private MobileTouchCamera camMove;
    // 고양이 Y + 8

    private void Start()
    {
        end = false;
        pos = transform.position;
        maskPos = beemMask.localPosition;
        camMove = cam.GetComponent<MobileTouchCamera>();
        beem.enabled = false;
    }    

    public void OnClick()
    {
        if (end)
            StartCoroutine("GoHome");
    }

    public void Trigger()
    {
        StartCoroutine("UFOCome");
    }

    private IEnumerator OutDoor()
    {
        Vector3 pos = boxCat.GetComponent<SquareCatMotion>().hitBox.transform.position;
        StartCoroutine("Motion");
        yield return new WaitForSeconds(0.75f);
        beem.enabled = true;
        beemMask.GetComponent<SpriteMask>().enabled = true;
        boxCat.position = destination.position;
        boxCat.localScale = new Vector3(0, 0, 0);
        beemMask.DOLocalMoveY(-1.45f, 1.5f);
        yield return new WaitForSeconds(1.5f);
        boxCat.DOLocalRotate(new Vector3(0, 0, 1800f), 3.0f, RotateMode.FastBeyond360).SetEase(Ease.Linear);
        boxCat.DOScale(new Vector3(1.5f, 1.5f, 0), 3.0f).SetEase(Ease.Linear);
        boxCat.DOMove(new Vector3(pos.x, pos.y + 1, 0), 3.0f).SetEase(Ease.Linear);
        yield return new WaitForSeconds(3.0f);
        boxCat.GetComponent<SquareCatMotion>().StartMotion();
        //touch.cameraTouch = true;
        touchManager.CameraTouch = true;
        cam.GetComponent<TouchInputController>().CanMove = true;
        end = true;
        GetComponent<BoxCollider2D>().enabled = true;
    }

    private IEnumerator GoHome()
    {
        float min = camMove.CamZoomMin;
        //touch.cameraTouch = false;
        touchManager.CameraTouch = false;
        cam.GetComponent<TouchInputController>().CanMove = false;
        cam.transform.DOMove(new Vector3(pos.x, pos.y + 3, cam.transform.position.z), 0.5f);
        StartCoroutine(ZoomOut(camMove.CamZoomMax, camMove.CamZoomMin, 0.5f));
        yield return new WaitForSeconds(0.5f);
        camMove.CamZoomMin = min;
        end = false;
        StopCoroutine("Motion");
        catilien[0].DOLocalMove(new Vector3(0, 0, 0), 0.75f);
        catilien[1].DOLocalMove(new Vector3(0, 0, 0), 0.75f);
        yield return new WaitForSeconds(0.75f);
        item.DOMove(destination.position, 0.75f);
        item.DORotate(new Vector3(0,0,1800), 0.75f, RotateMode.FastBeyond360);
        item.DOScale(new Vector3(0, 0, 0), 0.75f);
        yield return new WaitForSeconds(0.75f);
        beemMask.DOLocalMoveY(1.7f, 1.5f);
        yield return new WaitForSeconds(1.5f);
        beem.enabled = false;
        beemMask.GetComponent<SpriteMask>().enabled = false;
        transform.DOScale(new Vector3(10, 10, 0), moveTime);
        transform.DOMove(movePos[1], moveTime / 2).SetEase(Ease.OutCubic);
        yield return new WaitForSeconds(moveTime / 2);
        transform.DOMove(movePos[0], moveTime / 2).SetEase(Ease.InCubic);
        yield return new WaitForSeconds(moveTime / 2);
        //touch.cameraTouch = true;
        touchManager.CameraTouch = true;
        cam.GetComponent<TouchInputController>().CanMove = true;
        //yield return new WaitForSeconds(10.0f);
        gameObject.SetActive(false);
    }

    private IEnumerator Motion()
    {
        catilien[0].DOLocalMoveX(-1.2f, 0.75f);
        catilien[0].DOLocalMoveY(0.1f, 0.75f).SetEase(Ease.InOutBounce);
        catilien[1].DOLocalMoveX(1.2f, 0.75f);
        catilien[1].DOLocalMoveY(-0.1f, 0.75f).SetEase(Ease.InOutBounce);
        yield return new WaitForSeconds(0.75f);
        // 둥실둥실
        while (true)
        {
            float num = 0.15f;
            for (int i = 0; i < 2; i++)
            {
                catilien[i].DOLocalMoveY(catilien[i].localPosition.y - num, 1.5f);
                num *= -1;
            }
            yield return new WaitForSeconds(1.5f);
            for (int i = 0; i < 2; i++)
            {
                catilien[i].DOLocalMoveY(catilien[i].localPosition.y + num, 1.5f);
                num *= -1;
            }
            yield return new WaitForSeconds(1.5f);
        }
    }

    private IEnumerator ZoomOut(float max, float min, float animeTime)
    {
        float zoomMax = max, zoomMin = min;
        float time = 0.0f;

        while (zoomMax > zoomMin)
        {
            time += Time.deltaTime / animeTime;

            zoomMin = Mathf.Lerp(min, max, time);

            camMove.CamZoomMin = zoomMin;
            camMove.ResetCameraBoundaries();
            yield return null;
        }
    }

    private IEnumerator UFOCome()
    {
        float min = camMove.CamZoomMin;
        pos = boxCat.GetComponent<SquareCatMotion>().hitBox.transform.position;
        //touch.cameraTouch = false;
        touchManager.CameraTouch = true;
        cam.GetComponent<TouchInputController>().CanMove = false;
        cam.transform.DOMove(new Vector3(pos.x, pos.y + 3, cam.transform.position.z), 0.5f);
        StartCoroutine(ZoomOut(camMove.CamZoomMax, camMove.CamZoomMin, 0.5f));
        yield return new WaitForSeconds(0.5f);
        camMove.CamZoomMin = min;
        transform.localScale = new Vector3(10, 10, 0);
        transform.position = movePos[0];
        transform.DOScale(new Vector3(3, 3, 0), moveTime);
        transform.DOMove(movePos[1], moveTime / 2).SetEase(Ease.OutCubic);
        yield return new WaitForSeconds(moveTime / 2);
        transform.DOMove(new Vector3(pos.x, pos.y + 10, 0), moveTime / 2).SetEase(Ease.InCubic);
        yield return new WaitForSeconds(moveTime / 2);
        StartCoroutine("OutDoor");
        // 지금 크기가 3
    }
}

