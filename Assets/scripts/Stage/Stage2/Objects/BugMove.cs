﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BugMove : MonoBehaviour
{
    public Transform van;
    public float speed;
    private Transform[] bug;
    private float[] y = new float[2];
    private bool start;
    // Start is called before the first frame update
    void Start()
    {
        start = true;
        bug = new Transform[transform.childCount];
        transform.position = van.position;
        for (int i = 0; i < transform.childCount; i++)
            bug[i] = transform.GetChild(i).transform;
    }

    private void Update()
    {
        if (start)
        {
            //if (van.GetComponent<Stage2ObjectVentilator>().end)
            //{
            //    for (int i = 0; i < bug.Length; i++)
            //    {
            //        bug[i].GetComponent<LaserMove>().enabled = true;
            //        bug[i].GetComponent<LaserMove>().speed[0] /= speed;
            //        bug[i].GetComponent<LaserMove>().speed[1] /= speed;
            //        bug[i].GetComponent<SpriteRenderer>().sortingOrder = 5;
            //    }
            //    start = false;
            //}
        }
        if (!start)
            for (int i = 0; i < bug.Length; i++)
                if (bug[i].localPosition.y >= 0.51f || bug[i].localPosition.y <= -0.50f)
                {
                    bug[i].GetComponent<LaserMove>().enabled = false;
                    bug[i].GetComponent<SpriteRenderer>().enabled = false;
                }
    }
}
