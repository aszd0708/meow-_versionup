﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VetilatorsPos : MonoBehaviour
{
    private GameObject[] Ventilator = new GameObject[2];
    public float[] x = new float[2];
    public float[] y = new float[4];
    // Start is called before the first frame update
    void Awake()
    {
        SetPos();
        GoPos();
    }

    private void SetPos()
    {
        Ventilator[0] = transform.GetChild(0).gameObject;
        Ventilator[1] = transform.GetChild(1).gameObject;
        x[0] = -36.5f;
        x[1] = -30.5f;
        y[0] = 2.6f;
        y[1] = -1.4f;
        y[2] = -5.4f;
        if(y.Length == 4)
            y[3] = -9.4f;
    }

    private void GoPos()
    {
        int a = Random.Range(0, y.Length);
        int b = Random.Range(0, 2);
        int c = Random.Range(0, y.Length);
        while (true)
        {
            if (a == c)
                c = Random.Range(0, 4);
            else
                break;
        }
        Ventilator[0].transform.localPosition = new Vector3(x[b], y[a], Ventilator[0].transform.localPosition.z);
        Ventilator[1].transform.localPosition = new Vector3(x[b], y[c], Ventilator[1].transform.localPosition.z);
    }
}
