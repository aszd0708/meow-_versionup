﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderMove : MonoBehaviour
{
    public GameObject replica;

    private SampleButtonScript itemSpider;

    void Awake()
    {
        itemSpider = GetComponent<SampleButtonScript>();
    }

    // Update is called once per frame
    void Update()
    {
        if (itemSpider.collect)
        {
            replica.SetActive(false);
            return;
        }           
        
        transform.position = replica.transform.position;
        transform.rotation = replica.transform.rotation;
    }
}
