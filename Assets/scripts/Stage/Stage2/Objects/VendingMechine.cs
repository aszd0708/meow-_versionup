﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VendingMechine : MonoBehaviour, TouchableObj
{
    public GameObject trash;
    public Transform createPos;
    public List<Sprite> canSprite = new List<Sprite>();
    public Vector2[] trashPos = new Vector2[2];

    private int randomEvent;

    public bool DoTouch(Vector2 touchPose = default)
    {
        OnClick();
        return false;
    }

    public void OnClick()
    {
        randomEvent = Random.Range(0, 10);
        if(randomEvent < 9)
        {
            GameObject createTrash = Instantiate(trash);
            createTrash.GetComponent<SpriteRenderer>().sprite = canSprite[Random.Range(0, canSprite.Count)];
            createTrash.transform.SetParent(gameObject.transform);
            createTrash.transform.localPosition = createPos.localPosition;
            createTrash.transform.rotation = Quaternion.Euler(0, 0, Random.Range(0f, 360f));
            StartCoroutine("Motion", createTrash);
        }

        else
        {
            int randomArr = Random.Range(2, 6);
            GameObject[] createTrash = new GameObject[randomArr];
            for(int i = 0; i < randomArr; i++)
            {
                createTrash[i] = Instantiate(trash);
                createTrash[i].GetComponent<SpriteRenderer>().sprite = canSprite[Random.Range(0, canSprite.Count)];
                createTrash[i].transform.SetParent(gameObject.transform);
                createTrash[i].transform.localPosition = createPos.localPosition;
                createTrash[i].transform.rotation = Quaternion.Euler(0, 0, Random.Range(0f, 360f));
            }
            StartCoroutine("Motion", createTrash);
        }
    }

    private IEnumerator Motion(GameObject trash)
    {
        trash.transform.DOLocalMove(new Vector3(Random.Range(trashPos[0].x, trashPos[1].x), Random.Range(trashPos[0].y, trashPos[1].y), 0), 0.5f);
        yield return new WaitForSeconds(4.0f);
        Destroy(trash);
    }

    private IEnumerator Motion(GameObject[] trash)
    {
        for(int i = 0; i < trash.Length; i++)
            trash[i].transform.DOLocalMove(new Vector3(Random.Range(trashPos[0].x, trashPos[1].x), Random.Range(trashPos[0].y, trashPos[1].y), 0), 0.5f);
        yield return new WaitForSeconds(4.0f);
        for(int i = 0; i < trash.Length; i++)
            Destroy(trash[i]);
    }
}
