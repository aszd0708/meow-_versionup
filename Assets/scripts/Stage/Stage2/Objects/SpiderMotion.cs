﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderMotion : MonoBehaviour, I_NonColObject
{
    public GameObject origin;
    public GameObject line;
    public GameObject spider;
    public float motionTime;
    public bool col;

    private Coroutine motion;

    void Start()
    {
        col = false;
    }

    void Update()
    {
        if (origin == null)
            Destroy(spider);
    }

    public void OnClick()
    {
        motion = StartCoroutine(Motion());
    }

    private IEnumerator Motion()
    {
        int zPos = 10;
        if (origin == null)
        {
            Debug.Log("거미 없당");
            StopCoroutine(motion);
        }
        GetComponent<BoxCollider2D>().enabled = false;
        transform.DOMoveY(-12.5f, motionTime);
        yield return new WaitForSeconds(motionTime);    
        spider.transform.DOLocalMoveY(-1.0f, motionTime);
        yield return new WaitForSeconds(motionTime);
        origin.GetComponent<CircleCollider2D>().enabled = true;
        transform.DOLocalRotate(new Vector3(0, 0, -10), 0.5f);
        yield return new WaitForSeconds(0.5f);
        while (true)
        {
            transform.DOLocalRotate(new Vector3(0, 0, zPos), 1.0f);
            yield return new WaitForSeconds(1.0f);
            zPos *= -1;
            if (origin == null)
            {
                Debug.Log("거미 없당");
                StopCoroutine(motion);
            }
        }
    }
}
