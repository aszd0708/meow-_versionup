﻿using BitBenderGames;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManholeMotion : MonoBehaviour, I_NonColObject
{
    public GameObject trash;
    public GameObject cap;
    public List<Sprite> sprite = new List<Sprite>();
    public GameObject bug;
    public GameObject catAilen;
    public bool ailen;
    public int count;

    private Vector3 downPos;
    private Vector3 nPos;
    private Vector3 originPos;
    private bool open;
    private bool nonOpen;
    private float randomDis;
    private int touchCount;
    private GameObject prevTrash;
    private bool touch;
    private void Start()
    {
        count = 0;
        int random = Random.Range(0, 2);
        switch (random)
        {
            case 0: randomDis = -1.2f; break;
            case 1: randomDis = 1.2f; break;
        }
        touch = true;
        open = false;
        nonOpen = true;
        touchCount = 0;
        originPos = transform.localPosition;
    }

    public void OnClick()
    {
        if (!touch)
            return;
        if (prevTrash != null)
        {
            prevTrash.SetActive(false);
        }
        Debug.Log("맨홀 클릭됨");
        if (touchCount >= 5)
        {
            int random = Random.Range(0, 2);
            switch (random)
            {
                case 0: randomDis = -1.2f; break;
                case 1: randomDis = 1.2f; break;
            }
            touchCount = 0;
            cap.transform.DOLocalMove(new Vector3(0, 0, 0), 0.5f);
            open = false;
        }

        else
            StartCoroutine(SpriteMotion(randomDis));
    }

    private IEnumerator SpriteMotion(float pos)
    {
        int random;
        count++;
        if (catAilen == null)
            ailen = false;

        if (!ailen)
            random = Random.Range(0, sprite.Count + 1);

        else
            random = Random.Range(0, sprite.Count);

        touch = false;

        if ((count == 5) && ailen)
        {
            catAilen.GetComponent<CatAilenMotion>().MotionStarter();
            count = 100;
            ailen = false;
        }

        else if (random == sprite.Count)
        {
            prevTrash = PlayBug();
        }

        else
        {
            GameObject createTrash;
            createTrash = PoolingManager.Instance.GetPool("Trash" + random);
            if(createTrash == null)
                createTrash = Instantiate(trash);
            SpriteRenderer trashSprite = createTrash.GetComponent<SpriteRenderer>();
            Transform trashPos = createTrash.transform;

            createTrash.transform.SetParent(gameObject.transform);
            createTrash.transform.localPosition = new Vector3(0, 0, 0);
            trashSprite.enabled = false;
            trashSprite.sprite = sprite[random];

            if (!open)
            {
                cap.transform.DOLocalMove(new Vector3(pos, 0.6f, 0), 0.5f);
                open = true;
                yield return new WaitForSeconds(0.5f);
            }

            trashSprite.enabled = true;

            if (random == 0)
                trashPos.DOLocalMoveX(-pos, 0.5f);

            else
                trashPos.DOLocalJump(new Vector3(-pos, 0.0f), 0.5f, 2, 0.5f);
            prevTrash = createTrash;
            //Destroy(createTrash, 10.0f);
            PoolingManager.Instance.SetPool(createTrash, "Trash" + random, 10);
            yield return new WaitForSeconds(0.5f);
        }
        touchCount++;
        touch = true;
        yield break;
    }

    private GameObject PlayBug()
    {
        GameObject createBug;
        createBug = PoolingManager.Instance.GetPool("Bug");
        if(createBug == null)
            createBug = Instantiate(bug);
        createBug.transform.SetParent(gameObject.transform);
        createBug.transform.localPosition = trash.transform.localPosition;
        createBug.GetComponent<BugMoveSwitch>().On();
        return createBug;
    }
}
