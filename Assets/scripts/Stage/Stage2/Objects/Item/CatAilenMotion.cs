﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CatAilenMotion : MonoBehaviour
{
    public Transform manhole;
    public ManholeMotion motion;
    public Transform[] movePosT = new Transform[2];

    private Vector3[] movePosV = new Vector3[2];
    // Start is called before the first frame update
    void Start()
    {
        transform.position = manhole.transform.position;
        transform.localScale = new Vector3(0, 0, 0);
        for (int i = 0; i < 2; i++)
            movePosV[i] = movePosT[i].position;
    }

    public void MotionStarter()
    {
        StartCoroutine("Motion");
    }

    private IEnumerator Motion()
    {
        transform.localScale = new Vector3(2, 2, 0);
        transform.DOJump(new Vector3(transform.position.x - 1f, transform.position.y, 0), 0.5f, 2, 0.5f);
        yield return new WaitForSeconds(0.5f);
        transform.DOMove(new Vector3(movePosV[0].x, movePosV[Random.Range(0, 2)].y, 0f), 10.0f);
        transform.DOLocalRotate(new Vector3(0, 0, 1800), 120f, RotateMode.FastBeyond360);
        yield return new WaitForSeconds(10.0f);
        transform.localScale = new Vector3(0, 0, 0);
        transform.position = manhole.transform.position;
        transform.rotation = Quaternion.Euler(0, 0, 0);
        motion.ailen = true;
        motion.count = 0;
        transform.DOPause();
    }
}
