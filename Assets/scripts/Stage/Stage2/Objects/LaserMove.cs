﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserMove : MonoBehaviour
{
    private int x = 0;
    private int y = 1;
    [SerializeField]
    public float[] angle = new float[2];
    public float[] center = new float[2];
    public float[] speed = new float[2];
    public float range;
    private float xPos;
    private float yPos;
    private float[] originPos = new float[2];

    [SerializeField]
    private float mulSize;

    /// <summary>
    /// 최종 속도에서 나눌 값
    /// </summary>
    public float MulSize { get => mulSize; set => mulSize = value; }

    void Start()
    {
        originPos[x] = transform.position.x;
        originPos[y] = transform.position.y;
    }

    void Update()
    {
        xPos = center[x] + Mathf.Sin(angle[x]) * range;
        yPos = center[y] + Mathf.Sin(angle[y]) * range;

        angle[x] += speed[x] * MulSize;
        angle[y] += speed[y] * MulSize;

        transform.position = new Vector3(originPos[x] + xPos, originPos[y] + yPos, transform.position.z);
    }
}
