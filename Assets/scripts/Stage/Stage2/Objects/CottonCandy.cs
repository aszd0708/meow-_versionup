﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CottonCandy : MonoBehaviour
{
    public Transform original;
    public SampleButtonScript itemCottonCandy;
    // Start is called before the first frame update
    void Start()
    {
        if (original == null)
            gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (itemCottonCandy.collect)
            gameObject.SetActive(false);
        else if (original == null)
            gameObject.SetActive(false);
        original.position = transform.position;
        original.rotation = transform.rotation;
    }
}
