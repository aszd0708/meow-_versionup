﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarCreater : MonoBehaviour
{
    public GameObject leftPos;
    public GameObject rightPos;

    public Transform leftCars;
    public Transform rightCars;

    public GameObject[] cars;
    public float waitTime;

    public float carDistance;

    public bool up;
    private GameObject leftPointer;
    private GameObject rightPointer;
    private int pos;

    [SerializeField]
    private string leftUpLayer = "2";
    [SerializeField]
    private string leftDownLayer = "3";

    [SerializeField]
    private string rightUpLayer = "4";
    [SerializeField]
    private string rightDownLayer = "5";

    void Start()
    {
        StartCoroutine("LeftCarCreater");
        StartCoroutine("RightCarCreater");
    }

    private IEnumerator LeftCarCreater()
    {
        while(true)
        {
            int carKinds = RandomCar();

            string poolingName;
            GameObject car;
            poolingName = cars[carKinds].name;
            car = PoolingManager.Instance.GetPool(poolingName);

            if (car == null)
                car = Instantiate(cars[carKinds]);

            car.transform.rotation = Quaternion.Euler(0, 0, 0);

            CarDrive carScript = car.GetComponent<CarDrive>();

            carScript.PoolingName = poolingName;

            car.transform.position = new Vector3(leftPos.transform.GetChild(0).transform.position.x, leftPos.transform.GetChild(0).transform.position.y, 0);
            car.transform.SetParent(leftCars.transform);
            car.transform.localScale = Vector3.one;
            carScript.endPos = leftPos.transform.GetChild(1).transform;
            carScript.speed = Random.Range(3.0f, 8.0f);
            carScript.distance = carDistance;
            carScript.state = CarDrive.State.left;

            if (up)
                car = SetLayer(car, leftUpLayer);

            else
                car = SetLayer(car, leftDownLayer);

            if (leftCars.childCount == 1)
            {
                carScript.front = car;
                carScript.back = car;
            }

            else
            {
                leftPointer.GetComponent<CarDrive>().back = car;
                carScript.front = leftPointer;
            }
            leftPointer = car;
            yield return new WaitForSeconds(Random.Range(waitTime,waitTime * 2));
        }
    }

    private IEnumerator RightCarCreater()
    {
        while (true)
        {
            int carKinds = RandomCar();
            string poolingName;
            GameObject car;
            poolingName = cars[carKinds].name;
            car = PoolingManager.Instance.GetPool(poolingName);

            if (car == null)
            {
                car = Instantiate(cars[carKinds]);
            }

            car.transform.rotation = Quaternion.Euler(0, 180, 0);
            CarDrive carScript = car.GetComponent<CarDrive>();
            carScript.PoolingName = poolingName;
            car.transform.position = new Vector3(rightPos.transform.GetChild(0).transform.position.x, rightPos.transform.GetChild(0).transform.position.y, -2);
            car.transform.SetParent(rightCars.transform);
            car.transform.localScale = Vector3.one;
            carScript.endPos = rightPos.transform.GetChild(1).transform;
            carScript.speed = Random.Range(3.0f, 8.0f);
            carScript.distance = carDistance;
            carScript.state = CarDrive.State.right;

            if (up)
                car = SetLayer(car, rightUpLayer);

            else
                car = SetLayer(car, rightDownLayer);

            if (leftCars.childCount == 1)
            {
                carScript.front = car;
                carScript.back = car;
            }

            else
            {
                rightPointer.GetComponent<CarDrive>().back = car;
                carScript.front = rightPointer;
            }
            rightPointer = car;
            yield return new WaitForSeconds(Random.Range(waitTime, waitTime * 2));
        }
    }

    private int RandomCar()
    {
        int carKinds = Random.Range(0, 101);

        if (carKinds >= 0 && carKinds < 12) return 0;
        else if (carKinds >= 12 && carKinds < 23) return 1;
        else if (carKinds >= 23 && carKinds < 34) return 2;
        else if (carKinds >= 34 && carKinds < 45) return 3;
        else if (carKinds >= 45 && carKinds < 56) return 4;
        else if (carKinds >= 56 && carKinds < 67) return 5;
        else if (carKinds >= 67 && carKinds < 78) return 6;
        else if (carKinds >= 78 && carKinds < 89) return 7;
        else if (carKinds >= 89 && carKinds < 99) return 8;
        else return 9;
    }

    private GameObject SetLayer(GameObject car,string layer)
    {
        for(int i = 0; i < car.transform.childCount; i++)
        {
            car.transform.GetChild(i).GetComponent<SpriteRenderer>().sortingLayerName = layer;
        }
        return car;
    }
}
