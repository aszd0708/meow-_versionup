﻿using DG.Tweening;
using System;
using System.Collections;
using UnityEngine;

public class SushiMoving : MonoBehaviour
{
    public float waitTime;
    [SerializeField]
    float fR = 2;
    float MAXRADIAN = 360;
    float MINRADIAN = 0;
    [SerializeField]
    float fRadian = 0;
    [SerializeField]
    float fSpeed = 1;
    [SerializeField]
    Vector3 v3MovePos = Vector3.zero;
    [SerializeField]
    bool bAutoMove = false;

    Transform transMe = null;

    // Use this for initialization
    void Start()
    {
        if (transMe == null)
            transMe = transform;
        GetComponent<SpriteRenderer>().enabled = false;
        StartCoroutine("Moving");
    }

    private IEnumerator Moving()
    {
        yield return new WaitForSeconds(waitTime);
        GetComponent<SpriteRenderer>().enabled = true;
        while (true)
        {
            if (bAutoMove)
            {
                if (fRadian > MAXRADIAN)
                    fRadian = MINRADIAN;

                fRadian += Time.deltaTime * fSpeed;
            }

            // 추가 시킨 각도의 Radian를 구한다.
            float deRad = fRadian * Mathf.Deg2Rad;
            //Radian값으로 Sin과 Cos 값을 구한다.
            float sinValue = Mathf.Sin(deRad);
            float cosValue = Mathf.Cos(deRad);

            //Debug.Log(fRadian + " Mathf.Sin :" + sinValue + " Mathf.Cos :" + cosValue + deRad);

            // 반지름을 곱해 포인트 x,y값을 구한다.
            float y = 0;
            float x = 0;
            y = sinValue * fR;
            x = cosValue * fR;

            //이동
            transform.localPosition = new Vector3(x, y / 4, 0) + v3MovePos;
            gameObject.transform.localPosition = new Vector3(Mathf.Clamp(gameObject.transform.localPosition.x, gameObject.transform.localPosition.x, gameObject.transform.localPosition.x), Mathf.Clamp(gameObject.transform.localPosition.y, -0.7f, 0.6f), -1);
            yield return null;
        }
    }
}
