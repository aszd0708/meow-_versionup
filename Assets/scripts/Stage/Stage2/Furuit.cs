﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Furuit : MonoBehaviour, I_NonColObject
{
    public void OnClick()
    {
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<CircleCollider2D>().enabled = false;
        Destroy(gameObject, 1.0f);
    }
}
