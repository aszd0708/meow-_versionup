﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorMotion : MonoBehaviour
{
    public Sprite open;
    public Sprite close;

    private SpriteRenderer spriteRednerer;

    private void Awake()
    {
        if(GetComponent<SpriteRenderer>() != null)
        {
            spriteRednerer = GetComponent<SpriteRenderer>();
            spriteRednerer.sprite = close;
        }
    }

    public void DoTouch()
    {
        if (spriteRednerer.sprite == open)
        {
            spriteRednerer.sprite = close;
        }
        else
        {
            spriteRednerer.sprite = open;
        }

    }
}
