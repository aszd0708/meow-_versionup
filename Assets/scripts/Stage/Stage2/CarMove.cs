﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarMove : MonoBehaviour, TouchableObj
{
    public GameObject body;
    public GameObject[] wheel;

    private float rotationSpeed;

    [SerializeField]
    private SpriteRenderer taxiCapRender;

    public enum Kind
    {
        car, Rickshaw
    }

    public Kind kind;

    void Start()
    {
        if (kind == Kind.Rickshaw)
            rotationSpeed = 50;
        else
            rotationSpeed = 500;


        if(taxiCapRender != null)
        {
            bool random = IntRandom.StateRandomInt(30);
            taxiCapRender.enabled = random;
        }
    }

    // Update is called once per frame
    void Update()
    {
        for(int i = 0; i < wheel.Length; i++)
        {
            wheel[i].transform.Rotate(Vector3.forward * -rotationSpeed * Time.deltaTime);
        }
    }

    public bool DoTouch(Vector2 touchPose = default)
    {
        AudioManager.Instance.PlaySound("road", transform.position);
        return false;
    }
}