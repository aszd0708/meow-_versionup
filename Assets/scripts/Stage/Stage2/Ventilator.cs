﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ventilator : MonoBehaviour
{
    public Sprite one;
    public Sprite two;
    public float delay;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<SpriteRenderer>().sprite = one;
        StartCoroutine("Motion");
    }

    private IEnumerator Motion()
    {
        bool a = true;
        while(true)
        {
            if (a)
            {
                GetComponent<SpriteRenderer>().sprite = one;
                a = false;
            }
            else if (!a)
            {
                GetComponent<SpriteRenderer>().sprite = two;
                a = true;
            }
            yield return new WaitForSeconds(delay);
        }
    }
}
