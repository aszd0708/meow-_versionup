﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SushiMovingPoint : MonoBehaviour
{
    public GameObject[] sushi;

    public Vector3[] point1;
    public Vector3[] point2;

    void Awake()
    {
        for(int i = 0; i < point1.Length; i++)
        {
            point2[i] = new Vector3(point1[i].x + 8.0f, point1[i].y, point1[i].z);
        }
    }
}
