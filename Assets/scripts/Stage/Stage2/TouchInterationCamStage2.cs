﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TouchInterationCamStage2 : MonoBehaviour
{
    public GameObject photo;
    public float checkTime = -0.5f;
    private float timeSpan = 0.5f;
    private Text ChanceNum;
    private int Chance;
    public GameObject chanceNum;
    public GameObject number;
    public bool touch = true;
    public BackgoundTouch backgound;
    public CatHint catHint;
    public CatSaveLoad jsonCat;

    public AudioClip[] catSound;
    public AudioClip[] manSound;

    private GameObject interItem;

    // Start is called before the first frame update
    void Start()
    {
        timeSpan = Time.time;
        ChanceNum = GameObject.Find("ChanceNum").GetComponent<Text>();
        Chance = Convert.ToInt32(ChanceNum.text);
    }

    // Update is called once per frame
    void Update()
    {
        if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            return;
        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                if (EventSystem.current.IsPointerOverGameObject(i))
                    return;
            }
        }

        if (!touch)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            timeSpan = Time.time;
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(touchPos, Vector2.zero, 0.0f);
            catHint.GoOriginPos();
            if(hit.collider != null)
            {
                interItem = hit.collider.gameObject;

                if (interItem.tag == "SlideObject")
                {
                    interItem.GetComponent<Stage2SlideObject>().touch = true;
                    return;
                }

                else if(interItem.tag == "UpSlide")
                {
                    interItem.GetComponent<Upslide>().touch = true;
                    return;
                }
            }
                
        }

        else if (Input.GetMouseButtonUp(0))
        {
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(touchPos, Vector2.zero, 0.0f);

            timeSpan -= Time.time;

            if (timeSpan >= checkTime)
            {
                if (hit.collider == null || hit.collider.gameObject.name == "park_Tree2")
                {
                    Debug.Log("배경 선택됨");
                    backgound.Touch();
                    Chance--;
                    ChanceNum.text = "" + (Convert.ToInt32(ChanceNum.text) - 1);
                    EncryptedPlayerPrefs.SetInt(number.GetComponent<ChanceEmpty>().StageNumChance, Convert.ToInt32(ChanceNum.text));
                    chanceNum.GetComponent<ChanceNumImage>().NumImg();
                }

                else
                {
                    interItem = hit.collider.gameObject;
                    if (interItem.tag == "bird")
                    {
                        interItem.GetComponent<TouchBird>().Touch();
                    }

                    else if (interItem.tag == "fieldObject")
                    {
                        interItem.GetComponent<SampleButtonScript>().OnClick();
                    }

                    else if (interItem.tag == "fieldCat")
                    {
                        if (interItem.name == "BabyCat")
                        {
                            if (!interItem.GetComponent<BabyCat>().show)
                            {
                                GetComponent<AudioSource>().clip = catSound[0];
                                GetComponent<AudioSource>().Play();
                                return;
                            }
                        }

                        else if (interItem.name == "YellowCat")
                        {
                            if (!interItem.GetComponent<YellowCatMove>().huntingMouse)
                            {
                                GetComponent<AudioSource>().clip = catSound[1];
                                GetComponent<AudioSource>().Play();
                                return;
                            }
                        }


                        else if (interItem.name == "FatCat")
                        {
                            if (!interItem.GetComponent<FatCatMove>().findChuru)
                            {
                                GetComponent<AudioSource>().clip = catSound[2];
                                GetComponent<AudioSource>().Play();
                                return;
                            }
                        }

                        else if (interItem.name == "ShitDog")
                        {
                            interItem.GetComponent<DogTouch>().Touch();
                        }

                        if (!interItem.GetComponent<CatCheck>().loadCollect)
                        {
                            Debug.Log("사진");
                            photo.GetComponent<PhotoShot>().TakePicture(interItem);
                        }

                        List<CatSaveLoad.JsonCat> jsonCatData = jsonCat.LoadCats();
                        jsonCatData[interItem.GetComponent<CatCheck>().no - 1].Collect = true;
                        jsonCat.SaveCats(jsonCatData, jsonCat.key);

                        for (int i = 0; i < 10; i++)
                        {
                            if (interItem == GameObject.Find("Cat Panel").transform.GetChild(0).transform.GetChild(0).transform.GetChild(i).GetComponent<SampleCatButton>().fieldCat)
                            {
                                GameObject.Find("Cat Panel").transform.GetChild(0).transform.GetChild(0).transform.GetChild(i).GetComponent<SampleCatButton>().OnClick();
                                interItem.GetComponent<CatCheck>().CollectObject();
                                interItem.GetComponent<CatCheck>().CheckCollect();
                                break;
                            }
                        }
                    }

                    else if (interItem.tag == "thirdClick")
                    {
                        interItem.GetComponent<ThirdClick>().OnClick();
                    }

                    else if (interItem.tag == "itemThirdClick")
                    {
                        interItem.GetComponent<ItemThirdTouch>().OnClick();
                    }

                    else if (interItem.tag == "itemThirdTrigger")
                        interItem.GetComponent<ThirdChickTrigger>().OnClick();
                    // 여기서부터 2스테이지
                    

                }
            }

        }
    }
}
