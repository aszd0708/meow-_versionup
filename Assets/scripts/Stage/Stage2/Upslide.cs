﻿using BitBenderGames;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Upslide : MonoBehaviour
{
    public bool touch;
    public float num;
    public float posZ;

    private Vector2 originPos;
    private Vector2 nowPos;
    private Vector3 downPos;
    private Vector3 nPos;

    private TouchInputController camTouch;

    private void Awake()
    {
        camTouch = FindObjectOfType<TouchInputController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            downPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }

        if (touch)
        {
            camTouch.enabled = false;
            if (Input.GetMouseButton(0))
            {
                nPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                gameObject.transform.localPosition = new Vector3(gameObject.transform.localPosition.x, (gameObject.transform.localPosition.y) - (downPos.y - nPos.y), posZ);

                downPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            }

            else if (Input.GetMouseButtonUp(0))
            {
                touch = false;
                camTouch.enabled = true;
            }
            gameObject.transform.localPosition = new Vector3(Mathf.Clamp(gameObject.transform.localPosition.x, gameObject.transform.localPosition.x, gameObject.transform.localPosition.x), Mathf.Clamp(gameObject.transform.localPosition.y, originPos.y-0.2f, originPos.y + num), posZ);
        }
    }
}
