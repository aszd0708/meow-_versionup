﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CottonCandyMan : MonoBehaviour
{
    public SpriteRenderer head;
    public Sprite[] headSprite;
    public float flipTime;

    private void Start()
    {
        head.sprite = headSprite[Random.Range(0, headSprite.Length)];
        StartCoroutine("FlipMotion");
    }

    private IEnumerator FlipMotion()
    {
        bool flip = false;
        while (true)
        {
            // 뒤집어짐
            if (!flip)
            {
                transform.DOLocalRotate(new Vector3(0, 180, 0), 0.5f);
            }
            else
            {
                transform.DOLocalRotate(new Vector3(0, 0, 0), 0.5f);
            }
            yield return new WaitForSeconds(Random.Range(5.0f, flipTime));
            flip = !flip;
        }
    }
}
