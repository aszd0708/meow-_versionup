﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowPoersonCreater : MonoBehaviour
{
    public Transform movePos;
    public List<GameObject> person = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        int create = Random.Range(0, 3);
        if (create == 0)
        {
            int randomNum = Random.Range(0, person.Count);
            GameObject createPerson = Instantiate(person[randomNum]);
            createPerson.transform.SetParent(gameObject.transform.parent.transform);
            if(randomNum == 2)
                createPerson.transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y + 0.05f, transform.localPosition.z);
            else if(randomNum == 3)
                createPerson.transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y + 0.025f, transform.localPosition.z);
            else
                createPerson.transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y - 0.05f, transform.localPosition.z);
            createPerson.GetComponent<SpriteRenderer>().sortingLayerName = transform.parent.GetComponent<SpriteRenderer>().sortingLayerName;
            for (int i = 0; i < createPerson.transform.childCount; i++)
                createPerson.transform.GetChild(i).GetComponent<SpriteRenderer>().sortingLayerName = transform.parent.GetComponent<SpriteRenderer>().sortingLayerName;
            createPerson.GetComponent<StandPersonMotion>().movePos[1] = movePos.localPosition;
        }
        else return;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
