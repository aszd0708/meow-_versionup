﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalletPerson : MonoBehaviour, I_NonColObject
{
    public Transform wallet;
    public Transform dropPos;
    public GameObject realWallet;
    void Start()
    {
        realWallet.GetComponent<CircleCollider2D>().enabled = false;
        realWallet.GetComponent<SpriteRenderer>().enabled = false;
    }

    public void OnClick()
    {
        StartCoroutine("WalletMotion");
    }

    private IEnumerator WalletMotion()
    {
        if(GameObject.Find("Wallet") != null)
        {
            wallet.transform.SetParent(null);
            wallet.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 180));
            wallet.transform.DOMove(dropPos.position, 1.0f);
            wallet.transform.DOLocalRotate(new Vector3(0, 0, 0), 1.0f, RotateMode.FastBeyond360);
            yield return new WaitForSeconds(1.0f);
            wallet.transform.DOScale(new Vector3(0.5f, 0.5f, 1), 0.5f);
            wallet.GetComponent<SpriteRenderer>().enabled = false;
            realWallet.GetComponent<CircleCollider2D>().enabled = true;
            realWallet.GetComponent<SpriteRenderer>().enabled = true;
            realWallet.transform.position = wallet.transform.position;
        }
    }
}
