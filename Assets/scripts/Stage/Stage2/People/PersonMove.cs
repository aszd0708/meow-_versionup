﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonMove : MonoBehaviour
{
    public GameObject[] leg;
    public GameObject[] arm;
    // 왼쪽 먼저
    public SpriteRenderer bodySprite;
    public SpriteRenderer headSprite;
    public Sprite[] legSpriteArr;
    public Sprite[] bodySpriteArr;
    public Sprite[] armSpriteArr;
    public Sprite[] headSpriteArr;
    public float motionTime;
    public bool right;
    public enum State
    {
        leg, arm, both
    }

    private Transform[] legPos = new Transform[2];
    private Transform[] armPos = new Transform[2];

    private SpriteRenderer[] legSprite = new SpriteRenderer[2];
    private SpriteRenderer[] armSprite = new SpriteRenderer[2];

    private void OnEnable()
    {
        for (int i = 0; i < leg.Length; i++)
        {
            legPos[i] = leg[i].GetComponent<Transform>();
            legSprite[i] = leg[i].GetComponent<SpriteRenderer>();
            armPos[i] = arm[i].GetComponent<Transform>();
            armSprite[i] = arm[i].GetComponent<SpriteRenderer>();
        }
        RandomPerson();
        StartCoroutine(LegMotion());
        StartCoroutine(ArmMotion());
    }

    public void MotionStarter(State state, bool right)
    {
        switch (state)
        {
            case State.leg:
                StartCoroutine("LegMotion");
                break;
            case State.arm:
                StartCoroutine("ArmMotion");
                break;
            case State.both:
                StartCoroutine("LegMotion");
                StartCoroutine("ArmMotion");
                break;
        }
    }

    public void MotionStoper(State state)
    {
        switch (state)
        {
            case State.leg:
                StopCoroutine("LegMotion");
                break;
            case State.arm:
                StopCoroutine("ArmMotion");
                break;
            case State.both:
                StopCoroutine("LegMotion");
                StopCoroutine("ArmMotion");
                break;
        }
    }

    private int EvenRandom(int start, int end)
    {
        int randomNum;
        while (true)
        {
            randomNum = Random.Range(start, end);
            if (randomNum % 2 == 0)
                break;
        }
        return randomNum;
    }

    private void RandomPerson()
    {
        int random = EvenRandom(0, legSpriteArr.Length);
        int randomArm = EvenRandom(0, armSpriteArr.Length);
        int randomBody = Random.Range(0, bodySpriteArr.Length);
        int randomHead = Random.Range(0, headSpriteArr.Length);

        headSprite.sprite = headSpriteArr[randomHead];
        bodySprite.sprite = bodySpriteArr[random / 2];

        for (int i = 0; i < 2; i++)
        {
            legSprite[i].sprite = legSpriteArr[random + i];
            armSprite[i].sprite = armSpriteArr[random + i];
        }
    }

    private IEnumerator LegMotion()
    {
        int num = -1;
        while (true)
        {
            for (int i = 0; i < legPos.Length; i++)
            {
                legPos[i].DOLocalRotate(new Vector3(0, legPos[i].localRotation.y, 40 * num), motionTime);
                num *= -1;
            }
            yield return new WaitForSeconds(motionTime);
            for (int i = 0; i < legPos.Length; i++)
            {
                legPos[i].DOLocalRotate(new Vector3(0, legPos[i].localRotation.y, 10 * -num), motionTime);
                num *= -1;
            }
            yield return new WaitForSeconds(motionTime);
        }
    }

    private IEnumerator ArmMotion()
    {
        int num = -1;
        while (true)
        {
            for (int i = 0; i < armPos.Length; i++)
            {
                armPos[i].DOLocalRotate(new Vector3(0, armPos[i].localRotation.y, 0), motionTime);
            }
            yield return new WaitForSeconds(motionTime);

            for (int i = 0; i < armPos.Length; i++)
            {
                armPos[i].DOLocalRotate(new Vector3(0, armPos[i].localRotation.y, 30 * num), motionTime);
                num *= -1;
            }
            yield return new WaitForSeconds(motionTime);
        }
    }

}
