﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecurityMotion : MonoBehaviour
{
    public Transform arm;
    public Transform body;
    public Transform head;
    public Transform leafs;
    public Transform pile;
    public float rotationTime;
    public float motionTime;
    public float restTime;
    public bool flip;
    private Vector3 leafsPos;
    private SpriteRenderer[] leaf;

    private LeafsMove leafsMove;

    private void Awake()
    {
        if (!leafsMove)
            leafsMove = GetComponent<LeafsMove>();
    }

    private void Start()
    {
        leaf = new SpriteRenderer[leafs.childCount];
        leafsPos = leafs.position;
        for (int i = 0; i < leafs.childCount; i++)
            leaf[i] = leafs.GetChild(i).GetComponent<SpriteRenderer>();
        StartCoroutine(Motion());
    }

    private void RandomLeaf()
    {
        while (true)
        {
            int random = Random.Range(0, leaf.Length);
            if (!leaf[random].enabled)
            {
                leaf[random].enabled = true;
                return;
            }
            else if (Check())
                return;
        }
    }

    private bool Check()
    {
        for (int i = 0; i < leaf.Length; i++)
        {
            if (!leaf[i].enabled)
                return false;
        }
        return true;
    }

    private void AllOffLeafs()
    {
        for (int i = 0; i < leaf.Length; i++)
            leaf[i].enabled = false;
        leafs.position = leafsPos;
        leafs.SetParent(arm);
    }

    private IEnumerator Motion()
    {
        flip = false;
        int count = 0;

        WaitForSeconds motionWait = new WaitForSeconds(motionTime);
        WaitForSeconds motionMoreWait = new WaitForSeconds(motionTime * 1.5f);
        WaitForSeconds restWait = new WaitForSeconds(restTime);

        while (true)
        {
            int random = Random.Range(0, 10);
            if (count > 3)
            {
                StartCoroutine(Flip());
                count = 0;
            }
            else if (random < 8)
            {
                if (!flip && Check())
                {
                    arm.DOLocalRotate(new Vector3(0, 0, 20), motionTime);
                    yield return motionWait;
                    arm.DOLocalRotate(new Vector3(0, 0, -20), motionTime * 1.5f);
                    leafs.SetParent(pile);
                    leafsMove.MotionStarter();
                    leafs.DOMove(pile.position, 0.5f);
                    yield return motionMoreWait;
                    arm.DOLocalRotate(new Vector3(0, 0, 0), motionTime);
                    yield return motionWait;
                    AllOffLeafs();
                }
                // 빗질
                else if (flip)
                {
                    arm.DOLocalRotate(new Vector3(0, 180, 20), motionTime);
                    yield return motionWait;
                    RandomLeaf();
                    arm.DOLocalRotate(new Vector3(0, 180, -20), motionTime * 1.5f);
                    leafsMove.MotionStarter();
                    yield return motionMoreWait;
                    arm.DOLocalRotate(new Vector3(0, 180, 0), motionTime);
                    yield return motionWait;
                }
                else
                {
                    arm.DOLocalRotate(new Vector3(0, 0, 20), motionTime);
                    yield return motionWait;
                    RandomLeaf();
                    arm.DOLocalRotate(new Vector3(0, 0, -20), motionTime * 1.5f);
                    leafsMove.MotionStarter();
                    yield return motionMoreWait;
                    arm.DOLocalRotate(new Vector3(0, 0, 0), motionTime);
                    yield return motionWait;
                }
                count++;
                yield return new WaitForEndOfFrame();
            }

            else if (random >= 8 && random < 10)
            {
                StartCoroutine(Flip());
            }

            yield return restWait;
        }
    }

    private IEnumerator Flip()
    {
        // 뒤집어짐
        if (!flip)
        {
            arm.DOLocalRotate(Vector3.up * 180, rotationTime);
            body.DOLocalRotate(Vector3.up * 180, rotationTime);
            head.DOLocalRotate(Vector3.up * 180, rotationTime);
        }
        else
        {
            arm.DOLocalRotate(Vector3.zero, rotationTime);
            body.DOLocalRotate(Vector3.zero, rotationTime);
            head.DOLocalRotate(Vector3.zero, rotationTime);
        }
        yield return new WaitForSeconds(rotationTime);
        flip = !flip;
    }
}
