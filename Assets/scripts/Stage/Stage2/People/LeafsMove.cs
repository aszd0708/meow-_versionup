﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeafsMove : MonoBehaviour
{
    public SecurityMotion security;
    public Transform leafs;
    public Vector2[] movePos = new Vector2[2];
    private Transform[] leaf;

    private void Start()
    {
        leaf = new Transform[leafs.childCount];
        for (int i = 0; i < leafs.childCount; i++)
            leaf[i] = leafs.GetChild(i);
    }

    public void MotionStarter()
    {
        StartCoroutine("MoveMotion");
    }

    private IEnumerator MoveMotion()
    {
        if (!security.flip)
            for (int i = 0; i < leaf.Length; i++)
                leaf[i].DOMove(new Vector3(leaf[i].position.x - Random.Range(movePos[0].x, movePos[1].x),
                    leaf[i].position.y + Random.Range(movePos[0].y, movePos[1].y), leaf[i].position.z),
                    Random.Range(0.1f, 0.5f));
        else if(security.flip)
            for (int i = 0; i < leaf.Length; i++)
                leaf[i].DOMove(new Vector3(leaf[i].position.x + Random.Range(movePos[0].x, movePos[1].x),
                    leaf[i].position.y + Random.Range(movePos[0].y, movePos[1].y), leaf[i].position.z),
                    Random.Range(0.1f, 0.5f));

        yield return new WaitForSeconds(0.5f);
    }
}
