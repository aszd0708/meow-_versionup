﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandPersonMotion : MonoBehaviour
{
    public List<Sprite> headSprite = new List<Sprite>();
    public Vector3[] movePos = new Vector3[2];
    public SpriteRenderer head;
    public bool flip;
    private float ro;
    void Start()
    {
        int randomNum = Random.Range(0, headSprite.Count);
        head.sprite = headSprite[randomNum];
        ro = 0;
        if (transform.parent.transform.parent.transform.localRotation.z == 0)
            flip = true;
        else if(transform.parent.transform.parent.transform.localRotation.z == 1)
        {
            flip = false;
            transform.localRotation = Quaternion.Euler(new Vector3(0, 0, -180));
            ro = 180;
        }
        movePos[0] = transform.localPosition;
        StartCoroutine("Motion");
    }

    private IEnumerator Motion()
    {
        float time;
        while (true)
        {
            for (int i = 0; i < 2; i++)
            {
                Flip();
                time = Random.Range(1.0f, 1.5f);
                transform.DOLocalMoveX(movePos[i].x, time);
                yield return new WaitForSeconds(time);
                yield return new WaitForSeconds(Random.Range(1.0f,3.0f));
            }
        }
    }

    private void Flip()
    {
        float flipSize;
        if (flip)
            flipSize = 0;
        else
            flipSize = 180;
        float rotationTime = 0.5f;

        transform.DOLocalRotate(new Vector3(0, flipSize, ro), rotationTime);
        flip = !flip;
    }
}
