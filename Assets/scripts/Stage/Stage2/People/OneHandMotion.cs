﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneHandMotion : MonoBehaviour
{
    public SpriteRenderer head;
    public Transform hand;
    public Transform[] legPos;

    public Sprite[] headSprite;
    public float motionTime;
    public float speed;

    private void OnEnable()
    {
        int a = Random.Range(0, headSprite.Length);
        if (a < 8)
            head.transform.localPosition = new Vector3(0.061f, head.transform.localPosition.y, head.transform.localPosition.z);
        head.sprite = headSprite[a];
        StartCoroutine("LegMotion");
        StartCoroutine("HandMotion");
    }

    private IEnumerator LegMotion()
    {
        int num = -1;
        while (true)
        {
            for (int i = 0; i < legPos.Length; i++)
            {
                legPos[i].DOLocalRotate(new Vector3(0, legPos[i].localRotation.y, 40 * num), motionTime);
                num *= -1;
            }
            yield return new WaitForSeconds(motionTime);
            for (int i = 0; i < legPos.Length; i++)
            {
                legPos[i].DOLocalRotate(new Vector3(0, legPos[i].localRotation.y, 10 * -num), motionTime);
                num *= -1;
            }
            yield return new WaitForSeconds(motionTime);
        }
    }

    private IEnumerator HandMotion()
    {
        while (true)
        {
            int ran = Random.Range(10, 20);
            for(int i = 0; i < ran; i++)
            {
                hand.DOLocalRotate(new Vector3(0, hand.localRotation.y, 20), motionTime);
                yield return new WaitForSeconds(motionTime);
                hand.DOLocalRotate(new Vector3(0, hand.localRotation.y, -20), motionTime);
                yield return new WaitForSeconds(motionTime);
            }
            hand.DOLocalRotate(new Vector3(0, 0, 0f), motionTime * 2, RotateMode.FastBeyond360);
            yield return new WaitForSeconds(motionTime * 2);
        }
    }
}
