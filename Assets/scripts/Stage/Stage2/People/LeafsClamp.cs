﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeafsClamp : MonoBehaviour
{
    public Vector2[] clampPos = new Vector2[2];

    // Update is called once per frame
    void Update()
    {
        transform.localPosition = new Vector3(Mathf.Clamp(transform.localPosition.x, clampPos[0].x, clampPos[1].x),
            Mathf.Clamp(transform.localPosition.y, clampPos[0].y, clampPos[1].y), transform.localPosition.z);
    }
}
