﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoersonCreater : MonoBehaviour
{
    public GameObject crossPos;
    public GameObject roadPos;

    public GameObject[] person;

    public Transform people;

    void Start()
    {
        StartCoroutine(PersonCreate());
    }

    private IEnumerator PersonCreate()
    {
        int randomBool;
        while(true)
        {
            GameObject createPerson;
            PersonMove1 personMove;
            randomBool = Random.Range(0, 2);
            int num = Random.Range(0, 10);
            if (num < 8)
            {
                createPerson = PoolingManager.Instance.GetPool("Person");
                if(createPerson == null)
                    createPerson = Instantiate(person[0]);
                personMove = createPerson.GetComponent<PersonMove1>();
                personMove.PoolingName = "Person";
            }
            else
            {
                createPerson = PoolingManager.Instance.GetPool("HouseWife");
                if (createPerson == null)
                    createPerson = Instantiate(person[1]);
                personMove = createPerson.GetComponent<PersonMove1>();
                personMove.PoolingName = "HouseWife";
            }
            createPerson.transform.SetParent(people);
            createPerson.transform.localPosition = new Vector3(1000, 1000, 0);
            num = Random.Range(0, 10);

            if (num < 8)
                personMove.state = PersonMove1.State.road;
            else
                personMove.state = PersonMove1.State.cross;

            if (randomBool == 0)
                personMove.right = false;
            else if (randomBool == 1)
                personMove.right = true;

            switch (personMove.state)
            {
                case PersonMove1.State.cross:
                    for (int i = 0; i < crossPos.transform.childCount; i++)
                        personMove.crossPos[i] = crossPos.transform.GetChild(i).transform;
                    break;

                case PersonMove1.State.road:
                    for (int i = 0; i < roadPos.transform.childCount; i++)
                        personMove.roadPos[i] = roadPos.transform.GetChild(i).transform;
                    break;
            }
            //yield return new WaitForSeconds(0.1f);
            yield return new WaitForSeconds(Random.Range(0.5f, 10.0f));
        }
    }
}
