﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BabyMotion : MonoBehaviour
{
    public Transform arm_L;
    public Transform arm_R;
    public Transform head;
    public float angle;
    public float moveTime;

    void Start()
    {
        StartCoroutine("Motion");
        StartCoroutine("HeadMove");
    }

    private IEnumerator Motion()
    {
        while (true)
        {
            arm_L.DORotate(new Vector3(0, 0, angle), moveTime);
            arm_R.DORotate(new Vector3(0, 0, -angle), moveTime);
            yield return new WaitForSeconds(moveTime);
            arm_L.DORotate(new Vector3(0, 0, 0), moveTime);
            arm_R.DORotate(new Vector3(0, 0, 0), moveTime);
            yield return new WaitForSeconds(moveTime);
        }
    }

    private IEnumerator HeadMove()
    {
        while (true)
        {
            head.DORotate(new Vector3(0, 0, 15), moveTime / 3);
            yield return new WaitForSeconds(moveTime/3);
            head.DORotate(new Vector3(0, 0, -15), moveTime / 3);
            yield return new WaitForSeconds(moveTime/3);
        }
    }
}
