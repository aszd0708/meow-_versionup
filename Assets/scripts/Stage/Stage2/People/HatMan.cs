﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HatMan : MonoBehaviour, I_NonColObject
{
    public GameObject hat;
    public Transform dest;

    public void OnClick()
    {
        hat.GetComponent<CircleCollider2D>().enabled = false;
        StartCoroutine("HatMotion");
    }

    private IEnumerator HatMotion()
    {
        hat.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 180));
        hat.transform.DOMove(dest.position, 1.0f);
        hat.transform.DOLocalRotate(new Vector3(0, 0, 0), 1.0f, RotateMode.FastBeyond360);
        yield return new WaitForSeconds(1.0f);
        hat.transform.DOScale(new Vector3(1, 1, 1), 0.5f);
        hat.GetComponent<CircleCollider2D>().enabled = true;
        hat.GetComponent<HatCatMotion>().MotionStarter();
    }
}
