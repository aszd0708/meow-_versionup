﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonMove1 : MonoBehaviour
{
    public enum State
    {
        cross, road
    }

    public Transform[] crossPos = new Transform[6];
    public Transform[] roadPos = new Transform[4];

    public SpriteRenderer bodySprite;
    public SpriteRenderer headSprite;
    public SpriteRenderer[] legSprite = new SpriteRenderer[2];
    public SpriteRenderer[] armSprite = new SpriteRenderer[2];

    public State state;
    public bool right;

    private string poolingName;

    public string PoolingName { get => poolingName; set => poolingName = value; }

    void OnEnable()
    {
        Invoke("MoveStart", 0.5f);
    }

    private void MoveStart()
    {
        if (state == State.cross)
            StartCoroutine(CrossMove());
        else
            StartCoroutine(RoadMove());
    }

    private void FlipX()
    {
        if (!right)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }

        else
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
    }
    /*
     * 
            bodySprite.flipX = true;
            headSprite.flipX = true;
            for (int i = 0; i < 2; i++)
            {
                legSprite[i].flipX = true;
                armSprite[i].flipX = true;
            }
     */

    private IEnumerator CrossMove()
    {
        FlipX();
        MaskVisibleSetting(SpriteMaskInteraction.VisibleOutsideMask);
        if (right)
        {
            transform.position = new Vector3(crossPos[0].position.x, crossPos[0].position.y, -1);
            while (true)
            {
                float Time = Random.Range(20.0f, 50f);
                transform.DOMove(crossPos[1].position, Time).SetEase(Ease.Linear);
                yield return new WaitForSeconds(Time);
                transform.DOMove(crossPos[2].position, 5.0f).SetEase(Ease.Linear);
                yield return new WaitForSeconds(5.0f);
                transform.DOMove(crossPos[3].position, 1.0f).SetEase(Ease.Linear);
                yield return new WaitForSeconds(1.0f);
                transform.DOMove(crossPos[4].position, 5.0f).SetEase(Ease.Linear);
                yield return new WaitForSeconds(5.0f);
                Time = Random.Range(40f, 70f);
                transform.DOMove(crossPos[5].position, Time).SetEase(Ease.Linear);
                yield return new WaitForSeconds(Time);
                PoolingManager.Instance.SetPool(gameObject, PoolingName);
            }
        }

        else if (!right)
        {
            transform.position = new Vector3(crossPos[5].position.x, crossPos[5].position.y, -1);
            while (true)
            {
                float Time = Random.Range(40f, 70f);
                transform.DOMove(crossPos[4].position, Time).SetEase(Ease.Linear);
                yield return new WaitForSeconds(Time);
                transform.DOMove(crossPos[3].position, 5f).SetEase(Ease.Linear);
                yield return new WaitForSeconds(5f);
                transform.DOMove(crossPos[2].position, 1f).SetEase(Ease.Linear);
                yield return new WaitForSeconds(1f);
                transform.DOMove(crossPos[1].position, 5f).SetEase(Ease.Linear);
                yield return new WaitForSeconds(5f);
                Time = Random.Range(20.0f, 50f);
                transform.DOMove(crossPos[0].position, Time).SetEase(Ease.Linear);
                yield return new WaitForSeconds(Time);
                PoolingManager.Instance.SetPool(gameObject, PoolingName);
            }
        }
        yield break;
    }

    private IEnumerator RoadMove()
    {
        MaskVisibleSetting(SpriteMaskInteraction.None);
        FlipX();

        roadPos[0].position = new Vector3(roadPos[0].position.x, -SettingRandomPostion(), roadPos[0].position.z);
        roadPos[3].position = new Vector3(roadPos[3].position.x, roadPos[0].position.y, roadPos[3].position.z);

        if (right)
        {
            transform.position = new Vector3(roadPos[0].position.x, roadPos[0].position.y, -1);
            while (true)
            {
                float Time = Random.Range(50f, 120f);
                transform.DOMove(roadPos[3].position, Time);
                yield return new WaitForSeconds(Time);
                PoolingManager.Instance.SetPool(gameObject, PoolingName);
            }
        }
        else if (!right)
        {
            transform.position = new Vector3(roadPos[3].position.x, roadPos[3].position.y, -1);
            while (true)
            {
                float Time = Random.Range(50f, 120f);
                transform.DOMove(roadPos[0].position, Time);
                yield return new WaitForSeconds(Time);
                PoolingManager.Instance.SetPool(gameObject, PoolingName);
            }
        }

        yield break;
    }

    private int SettingRandomPostion()
    {
        int position = Random.Range(35, 44 + 1);
        while (position <= 38 && position >= 37)
            position = Random.Range(35, 44 + 1);

        PositionLayer(position);
        return position;
    }

    private void PositionLayer(int position)
    {
        int layerNum;
        switch (position)
        {
            case 35: layerNum = 5; break;
            case 34: layerNum = 6; break;
            default: layerNum = position - 31; break;
        }
        string layer = IntToLayer(layerNum);
        SpriteLayerSetting(layer);
    }

    public void SpriteLayerSetting(string layer)
    {
        bodySprite.sortingLayerName = layer;
        headSprite.sortingLayerName = layer;

        for (int i = 0; i < legSprite.Length; i++)
            legSprite[i].sortingLayerName = layer;
        for (int i = 0; i < armSprite.Length; i++)
            armSprite[i].sortingLayerName = layer;
    }

    private string IntToLayer(int layer)
    {
        return layer + "";
    }

    private void MaskVisibleSetting(SpriteMaskInteraction visible)
    {
        for (int i = 0; i < legSprite.Length; i++)
            legSprite[i].maskInteraction = visible;
        bodySprite.maskInteraction = visible;
    }

    private void RandomState()
    {
        int num = Random.Range(0, 2);
        state = (State)Random.Range((int)State.cross, (int)State.road + 1);
        switch (num)
        {
            case 0:
                right = false;
                break;
            case 1:
                right = true;
                break;
        }
    }
}
