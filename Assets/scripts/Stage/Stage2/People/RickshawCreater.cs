﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RickshawCreater : MonoBehaviour
{
    public GameObject ricksawMan;
    public Transform[] createPos = new Transform[2];

    public string layerName = "1";
    // Start is called before the first frame update
    
    public enum State
    {
        LEFT, RIGHT
    }
    [SerializeField]
    private State createDir;

    private void Start()
    {
        Debug.Log("인력거 생성0");
        StartCoroutine(Creater());
    }

    private IEnumerator Creater()
    {
        float timer;
        yield return new WaitForSeconds(3.0f);
        while (true)
        {
            Debug.Log("인력거 생성");
            GameObject create = PoolingManager.Instance.GetPool("Ricksaw");
            Debug.Log("인력거 생성2");
            if (create == null)
            {
                create = Instantiate(ricksawMan);
            }
            Debug.Log("인력거 생성3");
            SetSpriteLayer(create.transform, layerName);
            createDir = (State)Random.Range((int)State.LEFT, (int)State.RIGHT + 1);
            timer = Random.Range(120, 180);
            switch(createDir)
            {
                case State.LEFT:
                    //create = Instantiate(ricksawMan, createPos[0].position, Quaternion.Euler(0, 0, 0));
                    create.transform.position = createPos[0].position;
                    create.transform.rotation = Quaternion.identity;
                    create.transform.DOMoveX(createPos[1].position.x, timer);
                    break;

                case State.RIGHT:
                    //create = Instantiate(ricksawMan, createPos[1].position, Quaternion.Euler(0, 180, 0));
                    create.transform.position = createPos[1].position;
                    create.transform.rotation = Quaternion.Euler(0, 180, 0);
                    create.transform.DOMoveX(createPos[0].position.x, timer);
                    break;
            }
            yield return new WaitForSeconds(timer);
            PoolingManager.Instance.SetPool(create, "Ricksaw");
            timer = Random.Range(0, 30);
            yield return new WaitForSeconds(timer);
        }
    }

    public void SetSpriteLayer(Transform parentTransform, string layer)
    {
        SpriteRenderer sprite = parentTransform.GetComponent<SpriteRenderer>();
        if (sprite != null)
        {
            sprite.sortingLayerName = layer;
        }
        if (parentTransform.childCount != 0)
        {
            for (int i = 0; i < parentTransform.childCount; i++)
            {
                SetSpriteLayer(parentTransform.GetChild(i), layer);
            }
        }
    }
}
