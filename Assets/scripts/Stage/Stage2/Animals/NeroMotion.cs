﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NeroMotion : UsingItemCat, CatHouseCat
{
    public SpriteRenderer body;
    public Sprite[] bodySprite = new Sprite[2];
    public SpriteRenderer tail;
    public Sprite[] tailSprite = new Sprite[2];
    public SpriteRenderer eye;
    public Sprite[] eyeSprite = new Sprite[2];

    public GameObject door;
    public Transform dest;

    private bool touch;
    private bool showItem = true;

    public bool Touch { get => touch; set => touch = value; }
    public bool ShowItem { get => showItem; set => showItem = value; }

    void Start()
    {
        if (SceneManager.GetActiveScene().name == "Cat'sRoom")
            StartCoroutine("Motion");
        else
        {
            body.enabled = false;
            tail.enabled = false;
            eye.enabled = false;
            GetComponent<BoxCollider2D>().enabled = false;
        }
    }

    private IEnumerator Motion()
    {
        while(true)
        {
            for(int i = 0; i < 2; i++)
            {
                body.sprite = bodySprite[i];
                tail.sprite = tailSprite[i];
                yield return new WaitForSeconds(0.5f);
            }
        }
    }

    private IEnumerator EyeMotion()
    {
        while (true)
        {
            for (int i = 0; i < 2; i++)
            {
                eye.sprite = eyeSprite[i];
                yield return new WaitForSeconds(1.0f);
            }
        }
    }

    private IEnumerator Eye()
    {
        while(true)
        {
            eye.enabled = true;
            yield return new WaitForSeconds(1.0f);
            eye.enabled = false;
            yield return new WaitForSeconds(1.5f);
        }
    }

    public void OnClick()
    {
        AudioManager.Instance.PlaySound("cat_4", transform.position);
    }

    private IEnumerator MoveMotion()
    {
        door.GetComponent<Stage2ObjectSuperDoor>().OpenDoor();
        if (!door.GetComponent<Stage2ObjectSuperDoor>().isOpen)
            yield return new WaitForSeconds(door.GetComponent<Stage2ObjectSuperDoor>().motionTime);
        body.enabled =true;
        tail.enabled =true;
        eye.enabled = true;
        yield return new WaitForSeconds(1.0f);
        transform.DOMove(dest.position, 1.5f);
        yield return new WaitForSeconds(1.5f);
        GetComponent<BoxCollider2D>().enabled = true;
    }

    public override void UseItem(GameObject item)
    {
        StopCoroutine(Eye());
        StartCoroutine(Motion());
        StartCoroutine(EyeMotion());
        StartCoroutine(MoveMotion());
    }
}
