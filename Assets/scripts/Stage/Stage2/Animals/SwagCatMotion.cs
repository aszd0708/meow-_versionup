﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwagCatMotion : UsingItemCat
{
    public SpriteRenderer catMouse;
    public Sprite mouse;
    public Transform arm;
    public Transform head;
    public bool collect;
    public SpriteRenderer itemSprite;

    public Transform movePos;

    private Sprite originMouse;

    void Start()
    {
        collect = false;
        catMouse.sprite = originMouse;
        StartCoroutine(Motion());
    }

    public override void UseItem(GameObject item)
    {
        itemSprite.enabled = true;
        transform.DOMove(new Vector3(-39.81f, 41.71f, -1), 0.5f);
        collect = true;
        CanCollect = true;
    }

    private IEnumerator Motion()
    {
        while(true)
        {
            head.transform.DORotate(new Vector3(0, 0, -5), 0.2f);
            yield return new WaitForSeconds(0.2f);
            catMouse.sprite = mouse;
            for(int i =0; i < 3; i++)
            {
                head.transform.DORotate(new Vector3(0, 0, -3), 0.2f);
                arm.transform.DORotate(new Vector3(0, 0, 10), 0.2f);
                yield return new WaitForSeconds(0.2f);
                head.transform.DORotate(new Vector3(0, 0, -5), 0.2f);
                arm.transform.DORotate(new Vector3(0, 0, 0), 0.2f);
                yield return new WaitForSeconds(0.2f);
            }
            head.transform.DORotate(new Vector3(0, 0, 0), 0.2f);
            catMouse.sprite = originMouse;
            catMouse.sprite = mouse;
            catMouse.sprite = originMouse;
            yield return new WaitForSeconds(1.0f);
        }
    }

    public void MoveMotion()
    {
        itemSprite.enabled = true;
        transform.DOMove(new Vector3(-39.81f, 41.71f, -1), 0.5f);
        collect = true;
    }
}
