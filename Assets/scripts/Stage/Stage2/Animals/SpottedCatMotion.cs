﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpottedCatMotion : UsingItemCat
{
    public SpriteRenderer tail;
    public Sprite[] motion = new Sprite[2];
    public GameObject laser;
    public bool col;
    public Transform hitbox;
    public bool end;
    private float tailTime;
    void Start()
    {
        tailTime = 1.0f;
        col = false;
        end = false;
        StartCoroutine("TailMotion");
    }

    public override void UseItem(GameObject item)
    {

        if (!end)
            StartCoroutine("Motion", item);
        else
            Destroy(item);
    }

    public void MotionStarter(GameObject item)
    {
        if (!end)
            StartCoroutine("Motion", item);
        else
            Destroy(item);
    }

    private IEnumerator TailMotion()
    {
        while (true)
        {
            tail.flipX = true;
            yield return new WaitForSeconds(tailTime);
            tail.flipX = false;
            yield return new WaitForSeconds(tailTime);
        }
    }

    private IEnumerator Motion(GameObject item)
    {
        tailTime = 0.5f;
        end = true;
        bool flip = false;
        item.GetComponent<SpriteRenderer>().enabled = false;
        Debug.Log("실행");
        SpriteRenderer cat = GetComponent<SpriteRenderer>();
        laser.GetComponent<SpriteRenderer>().enabled = true;
        yield return new WaitForSeconds(1.0f);

        transform.DOJump(new Vector3(hitbox.position.x, transform.position.y, transform.position.z), 0.5f, 1, 0.5f);

        col = true;
        CanCollect = true;
        while (true)
        {
            for (int j = 0; j < Random.Range(1, 4); j++)
            {
                for (int i = 0; i < 2; i++)
                {
                    //transform.position
                    cat.sprite = motion[0];
                    yield return new WaitForSeconds(0.5f);

                    cat.sprite = motion[1];
                    yield return new WaitForSeconds(0.5f);
                }
            }

            if (!flip)
                transform.DOLocalRotate(new Vector3(0, 180, 0), 0.5f);
            else
                transform.DOLocalRotate(new Vector3(0, 0, 0), 0.5f);
            yield return new WaitForSeconds(0.5f);
            flip = !flip;
        }
    }

}
