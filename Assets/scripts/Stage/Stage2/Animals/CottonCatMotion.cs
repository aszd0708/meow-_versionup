﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CottonCatMotion : UsingItemCat, CatHouseCat
{
    public SpriteRenderer body;
    public SpriteRenderer eye;
    public Sprite sitBody;
    public Sprite[] walkbody = new Sprite[2];
    public Sprite[] eyeSprite = new Sprite[2];
    public Transform tail;
    public float walkTime;
    public SpriteRenderer item;

    public bool col;

    private bool touch;
    private bool showItem = true;

    public bool Touch { get => touch; set => touch = value; }
    public bool ShowItem { get => showItem; set => showItem = value; }

    void Start()
    {
        col = false;
        StartCoroutine("EyeMotion");
        StartCoroutine("TailMotion");
    }

    public void OnClick()
    {
        AudioManager.Instance.PlaySound("baby_cat", transform.position);
        //StartCoroutine("WalkMotion");
    }

    public override void UseItem(GameObject item)
    {
        StartCoroutine(WalkMotion());
    }

    private IEnumerator EyeMotion()
    {
        while (true)
        {
            eye.sprite = eyeSprite[0];
            yield return new WaitForSeconds(0.3f);
            eye.sprite = eyeSprite[1];
            yield return new WaitForSeconds(0.3f);
        }
    }

    private IEnumerator WalkMotion()
    {
        item.enabled = true;
        transform.DOMove(item.transform.position, 1.5f);
        for (int i = 0; i < 5; i++)
        {
            body.sprite = walkbody[i % 2];
            yield return new WaitForSeconds(0.3f);
        }
        Destroy(item);
        body.sprite = sitBody;
        col = true;
        CanCollect = true;
    }

    private IEnumerator TailMotion()
    {
        while (true)
        {
            tail.DORotate(new Vector3(0, 0, 20), 0.5f);
            yield return new WaitForSeconds(0.5f);
            tail.DORotate(new Vector3(0, 0, -20), 0.5f);
            yield return new WaitForSeconds(0.5f);
        }
    }
}
