﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BrownCatMotion : UsingItemCat
{
    public Transform head;
    public Transform tail;
    public GameObject walkBody;
    public GameObject sitBody;
    public Transform leftArm;
    public Transform rightArm;
    public Transform spider;

    public bool col;

    public GameObject door;
    public Transform hitBox;
    public Sprite[] walkSprite = new Sprite[2];
    private Vector3 spiderLeftPos;
    private Vector3 spiderRightPos;
    
    void Start()
    {
        SetPosition();
        sitBody.GetComponent<SpriteRenderer>().enabled = false;
        leftArm.GetComponent<SpriteRenderer>().enabled = false;
        rightArm.GetComponent<SpriteRenderer>().enabled = false;
        col = false;
        spiderLeftPos = new Vector3(-0.198f, -0.681f, 0);
        spiderRightPos = new Vector3(0.199f, -0.704f, 0);
    }
    
    void SetPosition()
    {
        head.localPosition = new Vector3(-0.016f, 0.629f, 0);
        tail.localPosition = new Vector3(1.41f, 0.53f, 0);
    }
    public override void UseItem(GameObject item)
    {
        StartCoroutine(Moving(item));
    }
    public void MotionStarter(GameObject item)
    {
        StartCoroutine("Moving", item);
    }

    private IEnumerator Moving(GameObject item)
    {
       // door.GetComponent<DoorMotion>().OnClick();
        head.GetComponent<SpriteRenderer>().enabled = true;
        walkBody.GetComponent<SpriteRenderer>().enabled = true;
        tail.GetComponent<SpriteRenderer>().enabled = true;
        transform.DOMove(hitBox.position, 1.5f);
        StartCoroutine("WalkMotion");
        yield return new WaitForSeconds(1.5f);
        StopCoroutine("WalkMotion");
        StartCoroutine("PlayingMotion2",item);
    }

    private IEnumerator WalkMotion()
    {
        while(true)
        {
            for(int i = 0; i < 2; i++)
            {
                walkBody.GetComponent<SpriteRenderer>().sprite = walkSprite[0];
                yield return new WaitForSeconds(0.5f);
                walkBody.GetComponent<SpriteRenderer>().sprite = walkSprite[1];
                yield return new WaitForSeconds(0.5f);
            }
        }
    }

    private IEnumerator PlayingMotion()
    {
        spider.SetParent(leftArm);
        spider.localPosition = spiderLeftPos;
        // 거미 던지기
        while (true)
        {
            leftArm.DORotate(new Vector3(0, 0, -20), 0.3f);
            yield return new WaitForSeconds(0.3f);
            leftArm.DORotate(new Vector3(0, 0, 10), 0.3f);
            yield return new WaitForSeconds(0.2f);
            leftArm.DORotate(new Vector3(0, 0, 0), 0.1f);
            spider.SetParent(rightArm);
            spider.DOLocalMoveX(spiderRightPos.x, 0.5f);
            spider.DOLocalMoveY(spiderRightPos.y + 0.5f, 0.25f).SetEase(Ease.OutCubic);
            yield return new WaitForSeconds(0.25f);
            spider.DOLocalMoveY(spiderRightPos.y, 0.25f).SetEase(Ease.OutCubic);
            yield return new WaitForSeconds(0.25f);

            yield return new WaitForSeconds(1.0f);

            rightArm.DORotate(new Vector3(0, 0, 20), 0.3f);
            yield return new WaitForSeconds(0.3f);
            rightArm.DORotate(new Vector3(0, 0, -10), 0.3f);
            yield return new WaitForSeconds(0.2f);
            rightArm.DORotate(new Vector3(0, 0, 0), 0.1f);
            spider.SetParent(leftArm);
            spider.DOLocalMoveX(spiderLeftPos.x, 0.5f);
            spider.DOLocalMoveY(spiderLeftPos.y + 0.5f, 0.25f).SetEase(Ease.OutCubic);
            yield return new WaitForSeconds(0.25f);
            spider.DOLocalMoveY(spiderLeftPos.y, 0.25f).SetEase(Ease.OutCubic);
            yield return new WaitForSeconds(0.25f);
        }
    }

    private IEnumerator PlayingMotion2(GameObject item)
    {
        head.localPosition = new Vector3(-0.078f, 0.761f, 0);
        tail.localPosition = new Vector3(0.515f, 0.422f, 0);
        GetComponent<BoxCollider2D>().offset = new Vector2(0, GetComponent<BoxCollider2D>().offset.y);
        GetComponent<BoxCollider2D>().size = new Vector2(GetComponent<BoxCollider2D>().size.x, 2);
        col = true;
        CanCollect = true;
        spider.SetParent(leftArm);
        spider.localPosition = spiderLeftPos;
        Destroy(item);
        walkBody.GetComponent<SpriteRenderer>().enabled = false;
        sitBody.GetComponent<SpriteRenderer>().enabled = true;
        leftArm.GetComponent<SpriteRenderer>().enabled = true;
        rightArm.GetComponent<SpriteRenderer>().enabled = true;
        spider.GetComponent<SpriteRenderer>().enabled = true;
        transform.rotation = Quaternion.Euler(0, 0, 0);
        while (true)
        {
            leftArm.DORotate(new Vector3(0, 0, -20), 0.3f);
            yield return new WaitForSeconds(0.3f);
            leftArm.DORotate(new Vector3(0, 0, 10), 0.3f);
            yield return new WaitForSeconds(0.2f);
            leftArm.DORotate(new Vector3(0, 0, 0), 0.1f);
            spider.SetParent(rightArm);
            spider.DOLocalMove(spiderRightPos, 0.5f);
            spider.DOLocalRotate(new Vector3(0, 0, 360f), 0.5f, RotateMode.FastBeyond360).SetEase(Ease.OutSine);
            yield return new WaitForSeconds(0.5f);

            //yield return new WaitForSeconds(1.0f);

            rightArm.DORotate(new Vector3(0, 0, 20), 0.3f);
            yield return new WaitForSeconds(0.3f);
            rightArm.DORotate(new Vector3(0, 0, -10), 0.3f);
            yield return new WaitForSeconds(0.2f);
            rightArm.DORotate(new Vector3(0, 0, 0), 0.1f);
            spider.SetParent(leftArm);
            spider.DOLocalMove(spiderLeftPos, 0.5f);
            spider.DOLocalRotate(new Vector3(0, 0, 0f), 0.5f, RotateMode.FastBeyond360).SetEase(Ease.OutSine);
            yield return new WaitForSeconds(0.5f);
        }
    }

    
}