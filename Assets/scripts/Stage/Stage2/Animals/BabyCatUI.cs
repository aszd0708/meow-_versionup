﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BabyCatUI : MonoBehaviour
{
    public RectTransform catUIPos;
    public Image mouse;
    public Sprite[] mouseSprite = new Sprite[2];
    private Vector2 originPos;
    private bool touch;

    void Start()
    {
        touch = true;
        originPos = catUIPos.anchoredPosition;
        catUIPos.localScale = new Vector3(0, 0, 1);
        StartCoroutine("UIMotion");
    }

    public void OnClick()
    {
        if(touch)
            StartCoroutine("UIMove");
    }

    private IEnumerator UIMotion()
    {
        while(true)
        {
            mouse.sprite = mouseSprite[0];
            yield return new WaitForSeconds(0.5f);
            mouse.sprite = mouseSprite[1];
            yield return new WaitForSeconds(0.5f);
        }
    }

    private IEnumerator UIMove()
    {
        catUIPos.localScale = new Vector3(1, 1, 1);
        touch = false;
        catUIPos.DOAnchorPosY(0f, 0.15f);
        yield return new WaitForSeconds(2.0f);
        catUIPos.DOAnchorPosY(originPos.y, 0.5f);
        yield return new WaitForSeconds(0.5f);
        catUIPos.localScale = new Vector3(0, 0, 1);
        touch = true;
    }
}
