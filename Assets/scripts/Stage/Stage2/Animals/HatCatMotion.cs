﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HatCatMotion : FieldCat
{
    public SpriteRenderer hand;
    public SpriteRenderer head;
    public SpriteRenderer eye;
    public SpriteRenderer[] lightEffect = new SpriteRenderer[2];
    public Sprite[] headSprite = new Sprite[2];
    public Sprite[] effect = new Sprite[4];

    public enum State
    {
        stage, house
    }

    public State state;

    void Start()
    {
        hand.enabled = false;
        head.enabled = false;
        eye.enabled = false;
        lightEffect[0].enabled = false;
        lightEffect[1].enabled = false;
        CanCollect = true;
        if(state == State.house)
        {
            StartCoroutine("EffectMotion");
            StartCoroutine("HeadMotion");
            StartCoroutine("Motion");
        }
    }

    public override void PlayTouchMotion()
    {
        base.PlayTouchMotion();
        MotionStarter();
    }

    public void MotionStarter()
    {
        StartCoroutine("EffectMotion");
        StartCoroutine("HeadMotion");
        StartCoroutine("Motion");
    }

    private IEnumerator EffectMotion()
    {
        int random;
        while (true)
        {
            random = Random.Range(0, 4);
            lightEffect[0].sprite = effect[random];
            lightEffect[1].sprite = effect[random];
            yield return new WaitForSeconds(0.5f);
        }
    }

    private IEnumerator HeadMotion()
    {
        while (true)
        {
            head.sprite = headSprite[0];
            yield return new WaitForSeconds(Random.Range(0.5f, 1.5f));
            head.sprite = headSprite[1];
            yield return new WaitForSeconds(Random.Range(0.5f, 1.5f));
        }
    }

    private IEnumerator Motion()
    {
        yield return new WaitForSeconds(0.3f);
        hand.enabled = true;
        yield return new WaitForSeconds(0.3f);
        head.enabled = true;
        eye.enabled = true;
        lightEffect[0].enabled = true;
        lightEffect[1].enabled = true;
        yield return new WaitForSeconds(5.0f);
        yield return new WaitForSeconds(5.0f);
    }
}
