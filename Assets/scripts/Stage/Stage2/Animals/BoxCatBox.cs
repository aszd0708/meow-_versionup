﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxCatBox : FieldCat
{
    public SpriteRenderer close;
    public SpriteRenderer[] lid = new SpriteRenderer[2];

    public GameObject cats;

    public bool open;

    public enum State
    {
        open, close
    }

    public State state;
    // Start is called before the first frame update
    void Start()
    {
        close.enabled = true;
        lid[0].enabled = false;
        lid[1].enabled = false;
        cats.SetActive(false);
        CanCollect = false;
        if (state == State.open)
            OnClick();
    }

    public override void PlayTouchMotion()
    {
        base.PlayTouchMotion();
        OnClick();
    }

    private void OnClick()
    {
        if (!open)
        {
            close.enabled = false;
            lid[0].enabled = true;
            lid[1].enabled = true;
            cats.SetActive(true);
            open = true;
            CanCollect = true;
            for (int i = 0; i < 3; i++)
                cats.GetComponent<BoxCatMotion>().StartMotion();
        }
        else
            return;
    }
}
