﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JellyDogMotion : UsingItemCat
{
    public SpriteRenderer sock;
    public Sprite standSprite;
    public Sprite biteSprite;
    public Sprite[] walkSprite;
    private SpriteRenderer jelly;
    // Start is called before the first frame update
    void Start()
    {
        jelly = GetComponent<SpriteRenderer>();
        sock.enabled = false;
        jelly.sprite = standSprite;
        GetComponent<CircleCollider2D>().enabled = false;
    }

    public override void UseItem(GameObject item)
    {
        StartCoroutine(Motion(item));
    }

    public void MotionStarter(GameObject item)
    {
        StartCoroutine("Motion",item);
    }

    private IEnumerator Motion(GameObject item)
    {
        GetComponent<CircleCollider2D>().enabled = true;
        GetComponent<SpriteRenderer>().sortingOrder = 1;
        sock.sortingOrder = 2;
        jelly.sprite = biteSprite;
        yield return new WaitForSeconds(0.5f);
        item.GetComponent<SpriteRenderer>().enabled = false;
        jelly.sprite = standSprite;
        sock.enabled = true;
        yield return new WaitForSeconds(1.0f);
        sock.enabled = false;
        // 가는곳 설정해서 가기
        for (int i = 0; i < 4; i++)
        {
            jelly.sprite = walkSprite[0];
            yield return new WaitForSeconds(0.5f);
            jelly.sprite = walkSprite[1];
            yield return new WaitForSeconds(0.5f);
        }
        jelly.sprite = standSprite;
        sock.enabled = true;
    }
}
