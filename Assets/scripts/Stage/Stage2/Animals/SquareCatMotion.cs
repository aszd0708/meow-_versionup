﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquareCatMotion : UsingItemCat
{
    public Transform[] leg = new Transform[4];
    public Transform tail;
    public Transform head;
    public GameObject hitBox;
    public CatFOMotion CFO;
    public bool collect;

    public enum State
    {
        stage, house
    }

    public State state;

    void Start()
    {
        collect = false;
        if(state == State.house)
            StartCoroutine("WalkMotion");
    }

    public void OnClick()
    {
        //Debug.Log(GetComponent<CatCheck>().loadCollect);
        //if(GetComponent<CatCheck>().loadCollect)
        //    CFO.OnClick();
    }

    public void StartMotion()
    {
        StartCoroutine("WalkMotion");
    }

    public override void UseItem(GameObject item)
    {
        Transform itemTransform = item.transform;
        item.GetComponent<SpriteRenderer>().sortingLayerName = "2";
        CFO.Trigger();
        CFO.item = itemTransform;
    }

    public void Trigger(Transform item)
    {
        CFO.Trigger();
        CFO.item = item;
    }

    public void StopCor()
    {
        StopCoroutine("WalkMotion");
    }

    private IEnumerator WalkMotion()
    {
        int a = 1;
        while(true)
        {
            head.DORotate(new Vector3(0, 0, 10 * a), 0.5f);
            tail.DORotate(new Vector3(0, 0, 10 * a), 0.5f);
            for (int i = 0; i< 4; i++)
            {
                leg[i].DORotate(new Vector3(0, 0, 20 * a), 0.5f);
                a *= -1;
            }
            a *= -1;
            yield return new WaitForSeconds(0.5f);
        }
    }
    
    public void GoToPos()
    {
        transform.position = new Vector3(hitBox.transform.position.x, hitBox.transform.position.y + 1, gameObject.transform.position.z);
        GetComponent<BoxCollider2D>().enabled = true;
    }

    private IEnumerator MoveMotion()
    {
        gameObject.GetComponent<BoxCollider2D>().enabled = false;
        gameObject.transform.DOMove(new Vector3(hitBox.transform.position.x, hitBox.transform.position.y + 3, gameObject.transform.position.z), 1.8f);
        yield return new WaitForSeconds(1.8f);
        gameObject.GetComponent<BoxCollider2D>().enabled = true;
    }
}
