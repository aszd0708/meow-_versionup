﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxCatMotion : MonoBehaviour
{
    public Sprite other1Head;
    public Sprite other2Head;
    public Sprite other3Head;

    public SpriteRenderer[] cat1Part;
    public SpriteRenderer[] cat2Part;
    public SpriteRenderer[] cat3Part;
    public float vibePowX;
    public float vibePowZ;
    public Sprite origin1Head;
    public Sprite origin2Head;
    public Sprite origin3Head;

    [Header("떠는데 필요한 변수들")]
    public float vibeTime;
    public float vibePower;
    public int vibrator;

    public void StartMotion()
    {
        StartCoroutine(_HeadMotion(cat1Part[0], other1Head, origin1Head, cat1Part));
        StartCoroutine(_HeadMotion(cat2Part[0], other2Head, origin2Head, cat2Part));
        StartCoroutine(_HeadMotion(cat3Part[0], other3Head, origin3Head, cat3Part));
    }

    private IEnumerator _HeadMotion(SpriteRenderer head, Sprite otherHead, Sprite originHead, SpriteRenderer[] cat)
    {
        while(true)
        {
            for(int i = 0; i < Random.Range(2,5); i++)
            {
                head.sprite = otherHead;
                yield return new WaitForSeconds(Random.Range(1.0f, 2f));
                head.sprite = originHead;
                yield return new WaitForSeconds(Random.Range(1.0f, 2f));
            }
            float random = Random.Range(0.2f, 0.5f);
            Vector3[] pos = new Vector3[cat.Length];
            Quaternion[] rot = new Quaternion[cat.Length];
            for (int i = 0; i < cat.Length; i++)
            {
                pos[i] = cat[i].transform.position;
                rot[i] = cat[i].transform.rotation;
                Transform catTransform = cat[i].transform;
                catTransform.DOShakePosition(vibeTime, vibePower, vibrator);
            }
            yield return new WaitForSeconds(random);
            for(int i = 0; i < cat.Length; i++)
            {
                cat[i].transform.position = pos[i];
                cat[i].transform.rotation = rot[i];
            }
        }
    }
}
