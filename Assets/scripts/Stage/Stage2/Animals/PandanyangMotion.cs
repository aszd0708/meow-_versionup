﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PandanyangMotion : MonoBehaviour
{
    public GameObject leftArm;
    public SpriteRenderer leaf;
    public SpriteRenderer mouse;

    private Vector3 originPos;
    void Start()
    {
        originPos = leftArm.transform.localPosition;
        StartCoroutine(Motion());
    }

    private IEnumerator Motion()
    {
        WaitForSeconds zeroPfive = new WaitForSeconds(0.5f);
        WaitForSeconds zeroPtwo = new WaitForSeconds(0.2f);
        WaitForSeconds one = new WaitForSeconds(1.0f);

        Vector3 movePos = new Vector3(-0.578f, 0.051f, leftArm.transform.position.z);

        while(true)
        {
            leaf.enabled = true;
            yield return zeroPfive;
            mouse.enabled = true;
            leftArm.transform.DOLocalMove(movePos, 0.5f);
            yield return zeroPfive;
            leaf.enabled = false;
            for (int i = 0; i < 3; i++)
            {
                mouse.enabled = false;
                yield return zeroPtwo;
                mouse.enabled = true;
                yield return zeroPtwo;
            }
            leftArm.transform.DOLocalMove(originPos, 0.5f);
            mouse.enabled = false;
            yield return one;
        }
    }
}
