﻿using BitBenderGames;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

/*
 * 2020-03-02
 * 코인을 막 뿌려주는 프리팹에 넣을 스크립트
 */
public class TouchCreateCoin : MonoBehaviour
{
    [Header("터치를 계속하다 일정 확률로 동전을 주는 스크립트")]
    private int touchCount;

    public int TouchCount { get => touchCount; set => touchCount = value; }
    [Header("터치 카운트 벨류 사운데 값 랜덤으로 값 정해짐")]
    public int[] countValues = new int[2];
    [Header("코인 퍼지는 프리팹")]
    public GameObject coinPrefab;
    [Header("이 코인 퍼지는 프리팹 Polling 이름")]
    public string poolingName;
    [Header("클릭 했을 시 동전++ 해주는 컴포넌트")]
    public MadCatControllor MCC;

    private MobileTouchCamera mainCam;
    //private TouchInterationCam touchController;
    private TouchManager touchManager;

    private float originZoom;

    private void Awake()
    {
        mainCam = FindObjectOfType<MobileTouchCamera>();
        //touchController = FindObjectOfType<TouchInterationCam>();
        touchManager = FindObjectOfType<TouchManager>();
        originZoom = mainCam.CamZoomMax;
    }

    private void Start()
    {
        SetCoin();
    }

    public void StartCreateCoin(Vector3 touchPos)
    {
        if (!CheckCountZero()) return;

        // 카운트++ 해주고 팝업창 나오게 함
        //MCC.MadCatTouch();
        // 광고 없는 팝업창 나오게 함

        //touchController.cameraTouch = false;
        touchManager.CameraTouch = false;
        StartCoroutine(_StartCreateCoin(touchPos));
    }

    private IEnumerator _StartCreateCoin(Vector3 touchPos)
    {
        CreateCoin(touchPos);
        mainCam.transform.DOMove(new Vector3(touchPos.x, touchPos.y, mainCam.transform.position.z), 0.5f);
        yield return StartCoroutine(Zoom(mainCam.CamZoomMax, mainCam.CamZoomMin, 0.5f));
        yield return new WaitForSeconds(0.5f);
        MCC.NonAdGiveCoin();
        SetCoin();
        mainCam.CamZoomMax = originZoom;
        yield break;
    }

    private IEnumerator Zoom(float max, float min, float animeTime)
    {
        MobileTouchCamera touchCam = mainCam;
        float zoomMax = max, zoomMin = min;
        float time = 0.0f;

        while (zoomMax > zoomMin)
        {
            time += Time.deltaTime / animeTime;

            zoomMax = Mathf.Lerp(max, min, time);

            touchCam.CamZoomMax = zoomMax;
            touchCam.ResetCameraBoundaries();
            yield return null;
        }
    }

    private void SetCoin()
    {
        TouchCount = Random.Range(countValues[0], countValues[1] + 1);
    }

    private bool CheckCountZero()
    {
        if (--TouchCount <= 0) return true;
        else return false;
    }

    public void CreateCoin(Vector3 touchPos)
    {
        GameObject coin = PoolingManager.Instance.GetPool(poolingName, transform);
        if (coin == null)
            coin = Instantiate(coinPrefab);
        coin.transform.position = touchPos;
    }
}
