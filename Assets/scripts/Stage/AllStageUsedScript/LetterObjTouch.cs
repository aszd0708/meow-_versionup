﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-04-13
 * 편지 터치했을때 실행하는 스크립트
 * 따로 추가할거 있으면 추가하면 될거 같음
 */

public class LetterObjTouch : MonoBehaviour, I_NonColObject
{
    [Header("LetterUI 스크립트")]
    public LetterUIMotion letterUI;
    
    public void OnClick()
    {
        letterUI.OpenLetter();
    }
}
