﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-04-05
 * 편지에 필요한 제이슨 텍스트 불러옴
 */

public struct LetterText
{
    public string StageNo;
    public string Kind;
    public string Text;
}

public class LetterTextLoader : JsonDataLoadManager<LetterText>
{
    [Header("편지 제이슨 파일")]
    public TextAsset jsonFile;
    [Header("스테이지별로 갇고옴 (숫자만 입력!)")]
    public string[] stageNum;

    public List<string> Hint { get => hint; set => hint = value; }
    public List<string> Letter { get => letter; set => letter = value; }

    private List<string> hint = new List<string>();
    private List<string> letter = new List<string>();

    private void Awake()
    {
        GetLetterText();
    }

    private void GetLetterText()
    {
        List<LetterText> loadList = LoadJson(jsonFile);

        for (int i = 0; i < loadList.Count; i++)
            if (CompareStageNo(loadList[i].StageNo))
            {
                if (loadList[i].Kind == "H") Hint.Add(loadList[i].Text);
                else if (loadList[i].Kind == "L") Letter.Add(loadList[i].Text);
                else if (loadList[i].Kind[0] == 'C')
                {
                    // 대충 관계에 관한 편지
                }
                else
                {
                    Debug.LogError("이상한 종류를 갖고 있습니다. 제이슨 수정해주세요.");
                    return;
                }
            }
    }

    private bool CompareStageNo(string stageNo)
    {
        for (int i = 0; i < stageNum.Length; i++)
            if (stageNo == stageNum[i])
                return true;

        return false;
    }

    public string GetRandomLetter(bool hint)
    {
        if (hint) return Hint[Random.Range(0, Hint.Count)];

        else return Letter[Random.Range(0, Letter.Count)];
    }
}
