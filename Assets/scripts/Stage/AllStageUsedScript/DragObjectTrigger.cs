﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragObjectTrigger : MonoBehaviour
{
    public string catName;
    public float V = 0.5f;

    public Vector3 itemPosition;

    public string sortingLayer;

    private ChanceNumImage chance;
    private GameObject hitBox;
    private GameObject trigger;
    private GameObject UIItem;

    private UsingItemCatsBox catBox;

    private void OnEnable()
    {
        catBox = FindObjectOfType<UsingItemCatsBox>();
    }

    void Start()
    {
        ItemTriggerObject trigger = FindObjectOfType<ItemTriggerObject>();

        chance = trigger.chance;
        for (int i = 0; i < trigger.trigger.Count; i++)
        {
            if (trigger.trigger[i].catName == catName)
            {
                hitBox = trigger.trigger[i].box;
                break;
            }
        }
    }

    // Update is called once per frame
    void Update() 
    {
        transform.position = new Vector3(Camera.main.ScreenToWorldPoint(UIItem.transform.position).x, Camera.main.ScreenToWorldPoint(UIItem.transform.position).y, -5);

        TouchOnOffManager.Instance.TouchOnOff(false);

        if (Input.GetMouseButtonUp(0))
        {
            if (Vector2.Distance(hitBox.transform.position, transform.position) <= V)
            {
                GameObject createItem = Instantiate(gameObject);
                SpriteRenderer itemSpriteRenderer = createItem.GetComponent<SpriteRenderer>();
                itemSpriteRenderer.color = new Color(itemSpriteRenderer.color.r, itemSpriteRenderer.color.g, itemSpriteRenderer.color.b, 255);
                createItem.transform.position = hitBox.transform.GetChild(hitBox.transform.childCount - 1).transform.position;
                itemSpriteRenderer.sortingLayerName = sortingLayer;
                createItem.GetComponent<DragItem>().enabled = false;
                createItem.GetComponent<ObjectTrigger>().enabled = false;

                UsingItemCat cat = catBox.GetUsingItemCats(catName);

                if (cat != null)
                    cat.UseItem(createItem);
                else return;

                VibratorControllor.ObjTouchVib();
            }
            else
            {
                chance.MinusChance();
                VibratorControllor.WrongObjTouch();
            }
            TouchOnOffManager.Instance.TouchOnOff(true);
            Destroy(gameObject);
            PoolingManager.Instance.GetPool(string.Format("{0}DragItem", catName));
        }
    }

    public void SetObject(GameObject UITem)
    {
        UIItem = UITem;
    }
}
