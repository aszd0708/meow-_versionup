﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemThirdTouch : MonoBehaviour, I_NonColObject
{
    public Sprite first;
    public Sprite second;
    public Sprite third;

    public void OnClick()
    {
        Debug.Log("클릭은 됨");
        if (gameObject.GetComponent<SpriteRenderer>().sprite == first)
            gameObject.GetComponent<SpriteRenderer>().sprite = second;
        else if (gameObject.GetComponent<SpriteRenderer>().sprite == second)
            gameObject.GetComponent<SpriteRenderer>().sprite = third;
        else
            return;
    }
}
