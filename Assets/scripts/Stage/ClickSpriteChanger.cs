﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickSpriteChanger : MonoBehaviour, I_NonColObject
{
    public List<Sprite> changeSprites = new List<Sprite>();

    private SpriteRenderer objSprite;
    private int index;

    private void Awake()
    {
        objSprite = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        index = Random.Range(0, changeSprites.Count);
        OnClick();
    }

    public void OnClick()
    {
        objSprite.sprite = changeSprites[index++];
        index %= changeSprites.Count;
    }
}
