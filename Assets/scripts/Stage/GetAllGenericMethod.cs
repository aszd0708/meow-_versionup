﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 원하는 타입(메소드)을 재귀함수로 전부 자식에 있는걸 갖고오는 함수
 */
public class GetAllGenericMethod<T> : MonoBehaviour
{
    public List<T> genericMethod = new List<T>();

    public void GetAllGeneric(Transform parent)
    {
        for (int i = 0; i < parent.childCount; i++)
        {
            if (parent.GetChild(i).childCount > 0)
                GetAllGeneric(parent.GetChild(i));

            if (parent.GetChild(i).GetComponent<SpriteRenderer>() == null)
                continue;
            else
                genericMethod.Add(parent.GetChild(i).GetComponent<T>());
        }
    }
}
