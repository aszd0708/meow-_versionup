﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AudioClip[] UIButton = new AudioClip[3];

    public void UITouch()
    {
        AudioManager.Instance.PlaySound(UIButton, Camera.main.transform.position, null, Random.Range(0.2f, 1.0f));
    }
}
