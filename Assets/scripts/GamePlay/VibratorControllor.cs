﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VibratorControllor : MonoBehaviour
{
    public static bool vib;

    public static void VibSetting()
    {
        if (!PlayerPrefs.HasKey("Vibrator"))
            PlayerPrefs.SetInt("Vibrator", 1);

        if (PlayerPrefs.GetInt("Vibrator") == 0)
            vib = false;
        else if (PlayerPrefs.GetInt("Vibrator") == 1)
            vib = true;
        else
            Debug.Log("진동 에러에러 빼애애애애액");
    }

    public static void TouchVib()
    {
        Debug.Log("배경 터치 진동 읭읭");
        // 배경 터치했을때
        if (vib)
            VibratorMK2.CreateOneShot(100);
        else
            return;
    }

    public static void ObjTouchVib()
    {
        Debug.Log("오브젝트 터치 진동 읭읭");
        // 
        if (vib)
            VibratorMK2.CreateOneShot(40);
        else return;
    }

    public static void WrongObjTouch()
    {
        Debug.Log("아이템 잘못 두는 진동 읭읭");
        // 아이템을 뒀을때 틀렸을때
        if (vib)
            VibratorMK2.CreateOneShot(200);
        else
            return;
    }
    public static void CatRoomVib()
    {
        // 고양이집 진동인데 On/Off 기능 넣을까 뺄까??
        Debug.Log("고양이집 진동 읭읭");

        if (vib)
            VibratorMK2.CreateOneShot(50);
        else
            return;

            //Vibrator.Vibrate(50);
    }
}
