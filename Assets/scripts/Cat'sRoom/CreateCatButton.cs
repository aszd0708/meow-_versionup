﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class CreateCatButton : MonoBehaviour
{
    public CatSaveLoad cat;
    public JsonCatExplanation explanation;
    public RaderChartInfo chartInfo;
    public GameObject button;
    public ScrollRectSnap scrollSnap;
    public GameObject uiRoot;
    public float[] x, y;
    public GameObject[] wall;
    public Transform pointPos;
    public Button back;
    public Reset reset;
    public GameObject item;
    public Text[] explanationText = new Text[3]; // 0 -point 1 - hobby 2 - story
    public Text nameExplanation;
    public GameObject instargramURL;
    public GameObject explantionBox;
    public GameObject tipTextBox;
    public float buttonDistance;
    public RadarPolygon radarPolygon;
    public Text[] raderText = new Text[5];
    public SpriteRenderer background;
    public RectTransform nameBox;
    public Image hiddenMark;
    public ParticleSystemRenderer kirakira;
    public Image rank;
    private Sprite[] itemImg;
    private Sprite[] catImg;
    private int num = 0;
    private float i_width;

    public List<string> exceptItemName = new List<string>();

    void Start()
    {
        PlayerPrefs.SetInt("HaveNewAnimal", 0);
        SetAniamlButton();
    }

    private void SetAniamlButton()
    {
        i_width = Screen.width;
        x[0] = (300f * i_width) / 2220f;
        x[1] = 2 * x[0];
        y = x;

        int countNum = 0;
        List<CatSaveLoad.JsonCat> jsonCat = cat.LoadCats();
        List<JsonCatExplanation.Explanation> jsonExplanation = explanation.LoadCats();
        List<RaderChartInfo.RaderChart> jsonChart = chartInfo.LoadCats();
        catImg = Resources.LoadAll<Sprite>("Cats'House/Animals/Cats");
        itemImg = Resources.LoadAll<Sprite>("Cats'House/Animals/Item");
        jsonCat = Sorting(jsonCat);
        //jsonCat.Sort(delegate (CatSaveLoad.JsonCat A, CatSaveLoad.JsonCat B) { return B.Collect.CompareTo(A.Collect); });
        Debug.Log(jsonCat.Count);
        for (int i = 0; i < jsonCat.Count; i++)
        {
            Button child = Instantiate(button).GetComponent<Button>();
            Debug.Log(i + " 번째 : ");
            jsonCat[i].Print();
            child.transform.GetChild(0).GetComponent<Image>().sprite = catImg[jsonCat[i].No - 1];
            child.GetComponent<CatButton>().name = jsonCat[i].Name;
            child.GetComponent<CatButton>().catNumber = jsonCat[i].No;
            if (!jsonCat[i].Collect)
            {
                child.transform.GetChild(0).GetComponent<Image>().color = new Color(0, 0, 0);
                child.GetComponent<ButtonAction>().catName = "???";
                child.enabled = false;
            }

            else
            {
                child.GetComponent<ButtonAction>().catName = jsonCat[i].Name;
            }

            child.transform.parent = uiRoot.transform;
            ButtonAction buttonAction = child.GetComponent<ButtonAction>();
            child.GetComponent<CatButton>().i = i;
            child.GetComponent<RectTransform>().anchoredPosition = new Vector3(i * buttonDistance, 0, 0);
            buttonAction.hiddenMark = hiddenMark;
            buttonAction.wall = wall;
            buttonAction.pointPos = pointPos.position;
            buttonAction.catinfo = jsonCat[i];
            buttonAction.back = back;
            buttonAction.explanation = explanationText;
            buttonAction.instarURL = instargramURL;
            buttonAction.nameExplanation = nameExplanation;
            buttonAction.explanationBox = explantionBox;
            buttonAction.tipBox = tipTextBox;
            buttonAction.reset = reset;
            buttonAction.background = background;
            buttonAction.nameBox = nameBox;

            buttonAction.Collect = jsonCat[i].Collect;

            for (int j = 0; j < jsonCat.Count; j++)
            {
                if (jsonCat[i].No == jsonExplanation[j].No)
                {
                    buttonAction.catExplanation = jsonExplanation[j];
                    break;
                }
            }

            if (jsonCat[i].ItemName != "none" && CreateItemObj(jsonCat[i].ItemName))
            {
                for (int j = 0; j < itemImg.Length; j++)
                {
                    string[] sp = itemImg[j].name.Split('_');
                    int a = Convert.ToInt32(sp[0]);
                    if (jsonCat[i].No == a)
                    {
                        buttonAction.itemImg = itemImg[j];
                        break;
                    }
                }
            }
            else
                countNum++;

            AnimalStageRadarChartMotion childRaderChart = child.GetComponent<AnimalStageRadarChartMotion>();
            childRaderChart.rank = rank;
            childRaderChart.kirakira = kirakira;
            childRaderChart.chartData = radarPolygon;
            childRaderChart.raderText = raderText;
            for (int j = 0; j < jsonCat.Count; j++)
            {
                if (jsonCat[i].No == jsonChart[j].No)
                {
                    //childRaderChart.chartInfo = jsonChart[j];
                    break;
                }
            }

            buttonAction.item = item;
            child.gameObject.GetComponent<CenterSize>().x = x;
            child.gameObject.GetComponent<CenterSize>().y = y;
            scrollSnap.GetCatScroll(child, i);
        }
    }

    private List<CatSaveLoad.JsonCat> Sorting(List<CatSaveLoad.JsonCat> catList)
    {
        List<CatSaveLoad.JsonCat> colCats = new List<CatSaveLoad.JsonCat>();
        List<CatSaveLoad.JsonCat> nonColCats = new List<CatSaveLoad.JsonCat>();

        for (int i = 0; i < catList.Count; i++)
        {
            if (catList[i].Collect) colCats.Add(catList[i]);
            else nonColCats.Add(catList[i]);
        }

        for (int i = 0; i < nonColCats.Count; i++)
            colCats.Add(nonColCats[i]);
        return colCats;        
    }

    private bool CreateItemObj(string itemName)
    {
        bool create = true;
        for(int i = 0; i < exceptItemName.Count; i++)
        {
            if(exceptItemName[i] == itemName)
            {
                create = false;
                break;
            }
        }

        return create;
    }
}
