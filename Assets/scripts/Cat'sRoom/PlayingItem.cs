﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayingItem : MonoBehaviour
{
    public bool touch;
    public CatHouseTouch catHouseTouch;
    private Vector3 touchPos;

    void Update()
    {
        Playing();
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -13, 13), Mathf.Clamp(transform.position.y, -6, 6), transform.position.z);
    }

    private void Playing()
    {
        if (touch)
        {
            touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (Input.GetMouseButton(0))
            {
                transform.position = new Vector3(touchPos.x, touchPos.y, transform.position.z);
            }

            else if (Input.GetMouseButtonUp(0))
            {
                touch = false;
                if(catHouseTouch)
                {
                    catHouseTouch.GrapTheItem = false;
                }
            }
        }
    }
}
