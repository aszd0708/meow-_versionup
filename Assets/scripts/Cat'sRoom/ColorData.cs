﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorData : MonoBehaviour
{
    public class WallRGB
    {
        public float R, G, B;

        public WallRGB(float r, float g, float b)
        {
            R = r; G = g; B = b;
        }
    }

    public enum ColorName
    {
        Yellow, YellowOrange, Orange, RedOrange, Red, RedViolet, Violet, BlueViolet, Blue, BlueGreen, Green, YellowGreen
    }

    public enum PastelColor
    {
        Blue, Purple, Red, Green, Yellow, Gray
    }

    public WallRGB[] RGB = new WallRGB[12];
    public WallRGB[] PastelRGB = new WallRGB[6];
    public WallRGB[] ExcepColor = new WallRGB[2];
    public int random;

    void Awake()
    {
        RGB[(int)ColorName.Yellow] = new WallRGB(ChangeRGB("FD"), ChangeRGB("F5"), ChangeRGB("90"));
        RGB[(int)ColorName.YellowOrange] = new WallRGB(ChangeRGB("FD"), ChangeRGB("E5"), ChangeRGB("85"));
        RGB[(int)ColorName.Orange] = new WallRGB(ChangeRGB("F7"), ChangeRGB("8E"), ChangeRGB("1E"));
        RGB[(int)ColorName.RedOrange] = new WallRGB(ChangeRGB("F7"), ChangeRGB("A9"), ChangeRGB("92"));
        RGB[(int)ColorName.Red] = new WallRGB(ChangeRGB("F5"), ChangeRGB("8E"), ChangeRGB("91"));
        RGB[(int)ColorName.RedViolet] = new WallRGB(ChangeRGB("CE"), ChangeRGB("9F"), ChangeRGB("CB"));
        RGB[(int)ColorName.Violet] = new WallRGB(ChangeRGB("77"), ChangeRGB("31"), ChangeRGB("91"));
        RGB[(int)ColorName.BlueViolet] = new WallRGB(ChangeRGB("B2"), ChangeRGB("AE"), ChangeRGB("D3"));
        RGB[(int)ColorName.Blue] = new WallRGB(ChangeRGB("A0"), ChangeRGB("B2"), ChangeRGB("D8"));
        RGB[(int)ColorName.BlueGreen] = new WallRGB(ChangeRGB("8D"), ChangeRGB("C6"), ChangeRGB("BF"));
        RGB[(int)ColorName.Green] = new WallRGB(ChangeRGB("2C"), ChangeRGB("B2"), ChangeRGB("4F"));
        RGB[(int)ColorName.YellowGreen] = new WallRGB(ChangeRGB("C5"), ChangeRGB("E2"), ChangeRGB("9F"));
        //RGB[(int)ColorName.Normal] = new WallRGB(255, 255, 255);

        PastelRGB[(int)PastelColor.Blue] = new WallRGB(ChangeRGB("E1"), ChangeRGB("EB"), ChangeRGB("F6"));
        PastelRGB[(int)PastelColor.Purple] = new WallRGB(ChangeRGB("EF"), ChangeRGB("E5"), ChangeRGB("EB"));
        PastelRGB[(int)PastelColor.Red] = new WallRGB(ChangeRGB("FC"), ChangeRGB("DF"), ChangeRGB("D7"));
        PastelRGB[(int)PastelColor.Green] = new WallRGB(ChangeRGB("D4"), ChangeRGB("EC"), ChangeRGB("DC"));
        PastelRGB[(int)PastelColor.Yellow] = new WallRGB(ChangeRGB("FC"), ChangeRGB("F9"), ChangeRGB("DA"));
        PastelRGB[(int)PastelColor.Gray] = new WallRGB(ChangeRGB("EE"), ChangeRGB("F2"), ChangeRGB("F5"));

        ShuffleArray(PastelRGB);
    }

    private int HexToDec(string hex)
    {
        int dec = System.Convert.ToInt32(hex, 16);
        return dec;
    }

    private float ChangeRGB(string hex)
    {
        return HexToDec(hex) / 255f;
    }

    public void ShuffleArray<T>(T[] array)
    {
        int random1;
        int random2;

        T tmp;

        for (int index = 0; index < array.Length; ++index)
        {
            random1 = UnityEngine.Random.Range(0, array.Length);
            random2 = UnityEngine.Random.Range(0, array.Length);

            tmp = array[random1];
            array[random1] = array[random2];
            array[random2] = tmp;
        }
    }

    public void RandomColor()
    {
        random = Random.Range(0, PastelRGB.Length);
    }
}
