﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using UnityEngine.UI;

public class CatSaveLoad : MonoBehaviour
{
    private string path;
    public TextAsset json;
    public string stage;
    [HideInInspector] public string key;


    public class JsonCat
    {
        public int No;
        public string Name;
        public string ItemName;
        public string CatPath;
        public bool Collect;

        public JsonCat(int no, string name, string itemName, string catPath, bool collect)
        {
            No = no;
            Name = name;
            ItemName = itemName;
            CatPath = catPath;
            Collect = collect;
        }

        public void Print()
        {
            Debug.Log("번호 : " + No + " 이름: " + Name + " 수집 유무 : " + Collect + " 아이템 이름 : " + ItemName);
        }

        public bool SendCollect()
        {
            return Collect;
        }
    }
    void Awake()
    {
        key = "rmeodhkcnacnsmsqka";
        path = Application.persistentDataPath;
    }

    public List<JsonCat> LoadCats()
    {
        Debug.Log("제이슨 로드");
        if (!System.IO.File.Exists(path + "/JsonCats.json"))
        {
            string jsonData = json.ToString();
            List<JsonCat> jsonCat = JsonConvert.DeserializeObject<List<JsonCat>>(jsonData);
            return jsonCat;
        }

        else
        {
            var jtc2 = JsonDataManager_Dummy.LoadJsonFile<List<JsonCat>>(path, "JsonCats", key);
            return jtc2;
        }
    }

    public void SaveCats(List<JsonCat> jsonCat, string key)
    {
        Debug.Log("제이슨 세이브");
        string jsonData = JsonDataManager_Dummy.ObjectToJson(jsonCat);
        jsonData = JsonAES.Encrypt(jsonData, key);
        JsonDataManager_Dummy.CreateJsonFile(path, "JsonCats", jsonData);
    }

    public void DelFile()
    {
        List<JsonCat> jsonCat = LoadCats();

        if(stage == "Stage1")
            for (int i = 1; i < 6; i++)
                jsonCat[i].Collect = false;

        else if(stage == "Stage2")
            for(int i = 6; i < 16; i++)
                jsonCat[i].Collect = false;

        SaveCats(jsonCat, key);
    }

    public void DelFile(int stageNum)
    {
        List<JsonCat> jsonCat = LoadCats();

        switch(stageNum)
        {
            case 0:
                jsonCat[0].Collect = false;
                break;

            case 1:
                for (int i = 1; i < 6; i++)
                    jsonCat[i].Collect = false;
                break;

            case 2:
                for (int i = 6; i < 16; i++)
                    jsonCat[i].Collect = false;
                break;
        }

        SaveCats(jsonCat, key);
    }
}
