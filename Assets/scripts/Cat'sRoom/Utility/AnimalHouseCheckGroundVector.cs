﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalHouseCheckGroundVector : Singleton<AnimalHouseCheckGroundVector>
{
    [SerializeField]
    private float[] rangeX;
    [SerializeField]
    private float[] rangeY;

    [SerializeField]
    private Transform[] triangleVector= new Transform[3];

    public bool IsInsideOnTriangle(Vector2 p, Vector2 t1, Vector2 t2, Vector2 t3)
    {
        float l1 = (p.x - t1.x) * (t3.y - t1.y) - (t3.x - t1.x) * (p.y - t1.y);                
        float l2 = (p.x - t2.x) * (t1.y - t2.y) - (t1.x - t2.x) * (p.y - t2.y);
        float l3 = (p.x - t3.x) * (t2.y - t3.y) - (t2.x - t3.x) * (p.y - t3.y);
        return (l1 > 0 && l2 > 0 && l3 > 0) || (l1 < 0 && l2 < 0 && l3 < 0);
    }

    public bool IsInsideOnTriangle(Vector2 pose)
    {
        return IsInsideOnTriangle(pose, triangleVector[0].position, triangleVector[1].position, triangleVector[2].position);
    }

    public bool IsInsideOnRangeX(Vector2 pose)
    {
        float y = Mathf.Abs(pose.y);
        float x = (y - 1.7f) / (-7.7f);

        float range = 28 * x;

        Debug.Log("Pose X : " + pose.x);
        Debug.Log("RangeX : " + range);
        return pose.x > range;
    }

    public bool IsInsideOnRangeY(Vector2 pose)
    {
        float x = Mathf.Abs(pose.x);
        float y = 1 - x / 28;

        float range = -6.0f - (-7.7f * y + 3.7f);
        Debug.Log("Pose Y : " + pose.y);
        Debug.Log("RangeY : " + range);
        return pose.y < range;
    }
}
