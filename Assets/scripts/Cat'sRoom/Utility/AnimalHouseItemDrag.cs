﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalHouseItemDrag : MonoBehaviour, CatHouseDragObject
{
    private bool touch;

    public bool Touch { get => touch; set => touch = value; }

    private void Update()
    {
        if (Touch)
        {
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            transform.position = (Vector2)touchPos;

            if(Input.GetMouseButtonUp(0))
            {
                Touch = false;
            }
        }
    }
}
