﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatHouseTouch : MonoBehaviour
{
    [Header("터치 속도")]
    public float checkTime = -0.5f;
    private float timeSpan = 0.5f;

    [Header("갖고 놀 수 있는 아이템")]
    public GameObject item;

    private bool grapTheItem;

    public bool GrapTheItem { get => grapTheItem; set => grapTheItem = value; }
    
    [Header("터치 감지할 레이어")]
    [SerializeField]
    private LayerMask dragItemLayer;

    [Header("아이템 드래그 레이어")]
    [SerializeField]
    private LayerMask touchObjectLayer;

    void Update()
    {
        if (!GrapTheItem)
            Touch();
    }

    private void Touch()
    {
        if (Input.GetMouseButtonDown(0))
        {
            timeSpan = Time.time;
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(touchPos, Vector2.zero, 0.0f, dragItemLayer);

            if (hit.collider != null)
            {
                GameObject hitObj = hit.collider.gameObject;
                if (hitObj == item)
                {
                    if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
                        return;
                    item.GetComponent<PlayingItem>().touch = true;
                    GrapTheItem = true;
                }
                //else if (hitObj.CompareTag("CatHouseDragObject"))
                //{
                //    hit.collider.GetComponent<CatHouseDragObject>().Touch = true;
                //}

                else
                {
                    CatHouseDragObject temp = hit.collider.GetComponent<CatHouseDragObject>();
                    if(temp != null)
                    {
                        temp.Touch = true;
                    }
                }
            }
        }

        else if (Input.GetMouseButtonUp(0))
        {
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(touchPos, Vector2.zero, 0.0f);

            timeSpan -= Time.time;
            if (timeSpan >= checkTime)
            {
                VibratorControllor.TouchVib();
                if (hit.collider != null)
                {
                    GameObject hitObj = hit.collider.gameObject;
                    DoTouchEvent(hitObj);                    
                }
            }
        }
    }

    private void DoTouchEvent(GameObject hitObject)
    {
        Debug.Log("눌려진 오브젝트 이름 : " + hitObject.name);
        if (hitObject == item)
        {
            if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
                return;
            item.GetComponent<PlayingItem>().touch = true;
            GrapTheItem = true;
        }

        else if (hitObject.CompareTag("CathouseCat"))
        {
            hitObject.GetComponent<CatHouseCat>().OnClick();
        }

        else if (hitObject.CompareTag("bird"))
        {
            hitObject.GetComponent<ai_spotted_cat_laser>().Touch = true;
        }
    }
}
