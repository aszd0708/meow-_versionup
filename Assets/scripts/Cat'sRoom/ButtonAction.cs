﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonAction : MonoBehaviour
{
    public CatSaveLoad.JsonCat catinfo;
    public JsonCatExplanation.Explanation catExplanation;
    public GameObject cat;
    public GameObject catPos;
    public GameObject item;
    public string catName;
    public Sprite itemImg;

    public GameObject[] wall;
    public Vector3 pointPos;

    public Button back;
    public Reset reset;
    public Text nameExplanation;
    public Text[] explanation = new Text[3]; // 0 - point 1 - hobby 2 - story
    public RectTransform nameBox;
    public GameObject instarURL;
    public GameObject explanationBox;
    public GameObject tipBox;
    public SpriteRenderer background;
    public Image hiddenMark;
    private bool hidden;
    private GameObject chart;
    private Vector3[] wallPos = new Vector3[3];
    private GameObject createCat;
    private float upSpeed;
    private float downSpeed;
    private bool instar;

    private bool collect;

    public bool Collect
    {
        set { collect = value; }
        get { return collect; }
    }

    void Start()
    {
        upSpeed = 1.5f;
        downSpeed = 0.2f;
    }

    public void OnClick()
    {
        cat = Resources.Load("Cats'House/Animals/Motion/" + catinfo.No.ToString() + "_" + catinfo.CatPath) as GameObject;
        Debug.Log("Cats'House/Animals/Motion/" + catinfo.No.ToString() + "_" + catinfo.CatPath + ".prefap");
        catExplanation.Print();
        catinfo.Print();
        if (catExplanation.Hint == "hidden")
            hiddenMark.enabled = true;
        else
            hiddenMark.enabled = false;
        hiddenMark.GetComponent<RectTransform>().localScale = new Vector3(0, 0, 0);
        nameExplanation.text = catExplanation.NameExplan();
        explanation[0].text = catExplanation.SendCharactor();
        explanation[1].text = catExplanation.SendFavorite();
        explanation[2].text = catExplanation.SendStory();
        tipBox.transform.GetChild(0).GetComponent<Text>().text = catExplanation.SendTip();
        chart = GameObject.Find("RadarChart1").gameObject;
        background.enabled = true;
        nameBox.anchoredPosition = new Vector2(0, 400);
        explanationBox.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 800);

        if (catExplanation.InstarURL() == "none")
            instar = false;
        else
        {
            instar = true;
            instarURL.GetComponent<InstarButton>().instarURL = "https://www.instagram.com/" + catExplanation.InstarURL();
        }
        
        for (int i = 0; i < wall.Length; i++)
        {
            wallPos[i] = wall[i].transform.position;
        }
        StartCoroutine(RoomChangeMotion());
    }

    public void ClickClear()
    {
        back.onClick.RemoveAllListeners();
        if (catName == "고먐미")
            StartCoroutine(WaitChange(FindObjectOfType<ap_square_cat_motion>().StartBack()));
        else
            StartCoroutine(BackRoomMotion());
    }

    public IEnumerator WaitChange(float time)
    {
        back.enabled = false;
        yield return new WaitForSeconds(time);
        back.enabled = true;
        StartCoroutine(BackRoomMotion());
        yield break;
    }

    private IEnumerator Rotate(RectTransform obj, float end, float start, float animeTime)
    {
        float max = end, min = start;
        float time = 0.0f;

        while (max > min)
        {
            time += Time.deltaTime / animeTime;
            min = Mathf.Lerp(end, start, time);
            obj.localRotation = Quaternion.Euler(min / 90, 0, 0);
            yield return null;
        }
    }

    private IEnumerator RoomChangeMotion()
    {
        item.GetComponent<SpriteRenderer>().sprite = itemImg;
        GameObject.Find("Canvas").GetComponent<Canvas>().enabled = false;
        for (int i = 0; i < wall.Length; i++)
        {
            wall[i].transform.DOMove(pointPos, 0.2f);
            wall[i].GetComponent<AnimalHouseWallColor>().ChangeColor();
            yield return new WaitForSeconds(0.2f);
        }
        createCat = Instantiate(cat);
        createCat.transform.position = new Vector3(pointPos.x, pointPos.y-1, -2);
        createCat.GetComponent<Transform>().localScale = new Vector2(0, 0);
        createCat.GetComponent<Transform>().DOScale(2, upSpeed/2).SetEase(Ease.OutElastic);
        if (instar)
            instarURL.GetComponent<Image>().color = new Color(1, 1, 1);
        else
            instarURL.GetComponent<Image>().color = new Color(0.5f, 0.5f, 0.5f);
        instarURL.GetComponent<Button>().enabled = instar;
        instarURL.GetComponent<RectTransform>().DOScale(1, upSpeed).SetEase(Ease.OutElastic);
        chart.GetComponent<RectTransform>().DOScale(1, upSpeed).SetEase(Ease.OutElastic);
        nameBox.DOAnchorPosY(0, upSpeed/2).SetEase(Ease.OutBounce);
        explanationBox.GetComponent<RectTransform>().DOAnchorPosY(-40, upSpeed / 2).SetEase(Ease.OutBounce);
        Debug.Log(explanationBox.GetComponent<RectTransform>().anchoredPosition);
        item.GetComponent<Transform>().DOScale(2, upSpeed / 2).SetEase(Ease.OutElastic);
        item.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        tipBox.GetComponent<RectTransform>().DOScale(1, upSpeed/2).SetEase(Ease.OutElastic);
        yield return new WaitForSeconds(upSpeed / 2);
        hiddenMark.GetComponent<RectTransform>().DOScale(new Vector3(1,1,0), upSpeed/2).SetEase(Ease.OutElastic);
        back.onClick.AddListener(ClickClear);
        yield return new WaitForSeconds(upSpeed / 2);

        
        //reset.catButton = gameObject.GetComponent<ButtonAction>();
        reset.room = true;
        yield break;
    }

    public IEnumerator BackRoomMotion()
    {
        wall[0].GetComponent<AnimalHouseWallColor>().color.RandomColor();
        createCat.GetComponent<Transform>().DOScale(0, downSpeed);
        item.GetComponent<Transform>().DOScale(0, downSpeed);
        item.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        nameBox.DOAnchorPosY(400, upSpeed / 2).SetEase(Ease.InQuint);
        explanationBox.GetComponent<RectTransform>().DOAnchorPosY(880, upSpeed / 2).SetEase(Ease.InQuint);
        yield return new WaitForSeconds(upSpeed / 2);
        for (int i = 0; i < wall.Length; i++)
            wall[i].transform.DOMove(wallPos[i], downSpeed);
        instarURL.GetComponent<RectTransform>().DOScale(0, downSpeed);
        chart.GetComponent<RectTransform>().DOScale(0, downSpeed);
        tipBox.GetComponent<RectTransform>().DOScale(0, downSpeed);
        yield return new WaitForSeconds(downSpeed + 0.1f);
        Destroy(createCat);
        background.enabled = false;
        GameObject.Find("Canvas").GetComponent<Canvas>().enabled = true;
        reset.room = false;
        yield break;
    }
}