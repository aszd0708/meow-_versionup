﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RomaticCat : MonoBehaviour
{
    public Transform head;
    public Transform tail;
    public GameObject walkBody;
    public GameObject sitBody;
    public Transform leftArm;
    public Transform rightArm;
    public Transform spider;
    private Vector3 spiderLeftPos;
    private Vector3 spiderRightPos;

    void Start()
    {
        SetPosition();
        head.GetComponent<SpriteRenderer>().enabled = true;
        walkBody.GetComponent<SpriteRenderer>().enabled = false;
        tail.GetComponent<SpriteRenderer>().enabled = true;
        spiderLeftPos = new Vector3(-0.198f, -0.681f, 0);
        spiderRightPos = new Vector3(0.199f, -0.704f, 0);
        MotionStarter();
    }

    void SetPosition()
    {
        head.localPosition = new Vector3(-0.016f, 0.629f, 0);
        tail.localPosition = new Vector3(1.41f, 0.53f, 0);
    }

    public void MotionStarter()
    {
        StartCoroutine("PlayingMotion2");
    }

    private IEnumerator PlayingMotion2()
    {
        head.localPosition = new Vector3(-0.017f, 0.72f, 0);
        tail.localPosition = new Vector3(0.515f, 0.422f, 0);
        spider.SetParent(leftArm);
        spider.localPosition = spiderLeftPos;
        walkBody.GetComponent<SpriteRenderer>().enabled = false;
        sitBody.GetComponent<SpriteRenderer>().enabled = true;
        leftArm.GetComponent<SpriteRenderer>().enabled = true;
        rightArm.GetComponent<SpriteRenderer>().enabled = true;
        spider.GetComponent<SpriteRenderer>().enabled = true;
        transform.rotation = Quaternion.Euler(0, 0, 0);
        while (true)
        {
            leftArm.DORotate(new Vector3(0, 0, -20), 0.3f);
            yield return new WaitForSeconds(0.3f);
            leftArm.DORotate(new Vector3(0, 0, 10), 0.3f);
            yield return new WaitForSeconds(0.2f);
            leftArm.DORotate(new Vector3(0, 0, 0), 0.1f);
            spider.SetParent(rightArm);
            spider.DOLocalMove(spiderRightPos, 0.5f);
            spider.DOLocalRotate(new Vector3(0, 0, 360f), 0.5f, RotateMode.FastBeyond360).SetEase(Ease.OutSine);
            yield return new WaitForSeconds(0.5f);

            //yield return new WaitForSeconds(1.0f);

            rightArm.DORotate(new Vector3(0, 0, 20), 0.3f);
            yield return new WaitForSeconds(0.3f);
            rightArm.DORotate(new Vector3(0, 0, -10), 0.3f);
            yield return new WaitForSeconds(0.2f);
            rightArm.DORotate(new Vector3(0, 0, 0), 0.1f);
            spider.SetParent(leftArm);
            spider.DOLocalMove(spiderLeftPos, 0.5f);
            spider.DOLocalRotate(new Vector3(0, 0, 0f), 0.5f, RotateMode.FastBeyond360).SetEase(Ease.OutSine);
            yield return new WaitForSeconds(0.5f);
        }
    }
}
