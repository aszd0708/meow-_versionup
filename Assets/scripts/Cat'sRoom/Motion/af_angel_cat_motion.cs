﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class af_angel_cat_motion : MonoBehaviour, CatHouseCat
{
    [Header("날개짓 하려고 만들어놓음 움직일땐 빠르게 아닐땐 느리게")]
    public Transform Lwing;
    public Transform Rwing;

    [Header("움직일때 사용할 변수들")]
    [Header("방향 전환하는 시간")]
    public float angeTime;
    [Header("움직이는 시간(거리에 따라 나눌것")]
    public float moveTime;
    [Header("날개 움직이는 속도")]
    public float wingFastTime;
    public float wingSlowTime;
    private float wingMotionTime;

    private Vector3 movePos;

    private Coroutine moveCor;
    private Coroutine idleCor;
    private Coroutine wingCor;

    [SerializeField]
    private bool showItem = true;
    public bool ShowItem { get => showItem; set => showItem = value; }
    public enum State
    {
        REST, FLY, LOOK, NOTHING
    }

    private State STATE;

    private bool touch;
    public bool Touch { get => touch; set => touch = value; }

    public void OnClick()
    {
        AudioManager.Instance.PlaySound("cat_6", transform.position);
        return;
    }

    private void Start()
    {
        idleCor = StartCoroutine(Idle());
        wingCor = StartCoroutine(WingMotion());
    }

    private void Update()
    {
        if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            return;

        SetTouchPosition();
    }
    private IEnumerator Move()
    {
        float distance = Vector2.Distance(transform.position, movePos);
        transform.DORotate(new Vector3(0,0, GetAngle(movePos) - 90), 0.1f);
        transform.DOMove(movePos, distance / moveTime);
        wingMotionTime = wingFastTime;
        yield return new WaitForSeconds(distance / moveTime);
        transform.DORotate(new Vector3(0, 0, 0), angeTime);
        yield return new WaitForSeconds(angeTime - 0.25f);
        STATE = State.REST;
        idleCor = StartCoroutine(Idle());
        wingCor = StartCoroutine(WingMotion());
        yield break;
    }

    private IEnumerator Idle()
    {
        wingMotionTime = wingSlowTime;
        Vector3 originPos = transform.localPosition;
        while (true)
        {
            transform.DOLocalMoveY(transform.localPosition.y + 1, 1.5f).SetEase(Ease.OutQuad);
            yield return new WaitForSeconds(1.5f);
            transform.DOLocalMoveY(originPos.y, 1.5f).SetEase(Ease.OutQuad);
            yield return new WaitForSeconds(1.5f);
        }
    }

    private void SetTouchPosition()
    {
        if(Input.GetMouseButton(0))
        {
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            movePos = touchPos;
            StateSetting(State.FLY);
        }
    }

    private void StateSetting(State changeState)
    {
        switch(STATE)
        {
            case State.FLY:
                switch(changeState)
                {
                    case State.FLY:
                        StopCoroutine(moveCor);
                        transform.DOKill();
                        moveCor = StartCoroutine(Move());
                        break;
                    case State.REST:
                        StopCoroutine(moveCor);
                        transform.DOKill();
                        wingCor = StartCoroutine(WingMotion());
                        break;
                }
                break;
            case State.REST:
                switch (changeState)
                {
                    case State.FLY:
                        StopCoroutine(idleCor);
                        transform.DOKill();
                        moveCor = StartCoroutine(Move());
                        StopCoroutine(wingCor);
                        Lwing.DOPause();
                        Rwing.DOPause();
                        break;
                    case State.REST:
                        break;
                }
                break;
        }
        STATE = changeState;
    }

    private float GetAngle(Vector2 pos)
    {
        float angle;

        angle = Mathf.Atan2(pos.y - transform.position.y,
                            pos.x - transform.position.x) * 180 / Mathf.PI;
        return angle;
    }

    private IEnumerator WingMotion()
    {
        while (true)
        {
            Lwing.transform.DORotate(new Vector3(Lwing.rotation.x, Lwing.rotation.y, Lwing.rotation.z + 10), wingMotionTime);
            Rwing.transform.DORotate(new Vector3(0, 0, Rwing.rotation.z - 10), wingMotionTime);
            yield return new WaitForSeconds(wingMotionTime);

            Lwing.transform.DORotate(new Vector3(Lwing.rotation.x, Lwing.rotation.y, Lwing.rotation.z - 3), wingMotionTime);
            Rwing.transform.DORotate(new Vector3(0, 0, Rwing.rotation.z+3), wingMotionTime);
            yield return new WaitForSeconds(wingMotionTime);
        }
    }
}
