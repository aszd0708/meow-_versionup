﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zc_dog_dog : MonoBehaviour, CatHouseCat
{
    [SerializeField]
    private SpriteRenderer bodyRender;
    [SerializeField]
    private SpriteRenderer eyeRender;

    [SerializeField]
    private Sprite[] bodySprites;
    [SerializeField]
    private float bodyChangeTime;
    [SerializeField]
    private Sprite[] eyeSprites;
    [SerializeField]
    private float eyeChangeTime;
    [SerializeField]
    private Sprite suprieseEyeSprite;

    private Coroutine eyemotionCor;
    private Coroutine bodyMotionCor;

    private bool bCanCollect = false;

    private bool touch;
    private bool showItem;

    public bool ShowItem { get => showItem; set => showItem = value; }
    public bool Touch { get => touch; set => touch = value; }

    private bool canTouch = true;

    private void OnEnable()
    {
        eyemotionCor = StartCoroutine(_RenderMotion(eyeRender, eyeSprites, eyeChangeTime));
        bodyMotionCor = StartCoroutine(_RenderMotion(bodyRender, bodySprites, bodyChangeTime));
    }

    public void OnClick()
    {
        if(canTouch)
        {
            StopCoroutine(eyemotionCor);
            eyeRender.sprite = suprieseEyeSprite;

            canTouch = false;
        }
        AudioManager.Instance.PlaySound("dog_3", transform.position);
    }

    private IEnumerator _RenderMotion(SpriteRenderer renderer, Sprite[] sprites, float time)
    {
        WaitForSeconds wait = new WaitForSeconds(time);
        int index = 0;
        while (true)
        {
            renderer.sprite = sprites[index++];
            index %= sprites.Length;
            yield return wait;
        }
    }
}
