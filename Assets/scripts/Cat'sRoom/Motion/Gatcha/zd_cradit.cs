﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zd_cradit : MonoBehaviour, CatHouseDragObject
{
    private bool touch;

    public bool Touch { get => touch; set => touch = value; }

    private bool temp = false;

    // Update is called once per frame
    void Update()
    {
        if(Touch)
        {
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            transform.position = touchPos;
            if(temp == false)
            {

                AudioManager.Instance.PlaySound("cheer_1", transform.position);
                temp = true;
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            Touch = false;
            temp = false;
        }
    }
}
