﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aj_panda_cat_motion : MonoBehaviour, CatHouseCat
{
    private bool haveBamboo;
    public bool HaveBamboo { get => haveBamboo; set => haveBamboo = value; }

    public GameObject leftArm;
    public SpriteRenderer leaf;
    public SpriteRenderer mouse;

    private Vector3 originPos;

    private int eatingCount;
    public int EatingCount { get => eatingCount; set => eatingCount = value; }
    public bool ShowItem { get => showItem; set => showItem = value; }
    public bool Touch { get => touch; set => touch = value; }

    public SpriteRenderer bamboo;

    public GameObject bambooDas;
    public Vector3 createBamboosPos;

    private bool touch;
    private bool showItem;

    private void Start()
    {
        GameObject bamboodas = Instantiate(bambooDas);
        bamboodas.transform.position = createBamboosPos;
        bamboodas.GetComponent<aj_bamboo_touch>().SetPanda(transform);
        originPos = leftArm.transform.localPosition;
    }

    private IEnumerator _EatingBamboo()
    {
        WaitForSeconds zeroPfive = new WaitForSeconds(0.5f);
        WaitForSeconds zeroPtwo = new WaitForSeconds(0.2f);
        WaitForSeconds one = new WaitForSeconds(1.0f);

        Vector3 movePos = new Vector3(-0.578f, 0.051f, leftArm.transform.position.z);

        while (HaveBamboo)
        {
            leaf.enabled = true;
            EatingCount--;
            if (EatingCount < 0)
                EatingEndBamboo();
            yield return zeroPfive;
            mouse.enabled = true;
            leftArm.transform.DOLocalMove(movePos, 0.5f);
            yield return zeroPfive;
            leaf.enabled = false;
            for (int i = 0; i < 3; i++)
            {
                mouse.enabled = false;
                yield return zeroPtwo;
                mouse.enabled = true;
                yield return zeroPtwo;
            }
            leftArm.transform.DOLocalMove(originPos, 0.5f);
            mouse.enabled = false;
            yield return one;
        }
        yield break;
    }

    public void GiveBamboo()
    {
        if (HaveBamboo)
            return;

        EatingCount = Random.Range(2, 4);
        bamboo.enabled = true;
        HaveBamboo = true;

        StartCoroutine(_EatingBamboo());
    }

    private void EatingEndBamboo()
    {
        HaveBamboo = false;
        bamboo.enabled = false;
    }

    public void OnClick()
    {
        AudioManager.Instance.PlaySound("slide_3", transform.position);
    }
}
