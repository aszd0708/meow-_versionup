﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ae_fat_cat_motion : MonoBehaviour, CatHouseCat
{
    [Header("바디 스프라이트")]
    public Sprite[] bodySprites = new Sprite[2];
    public Sprite[] eyeSprites = new Sprite[2];

    public SpriteRenderer body;
    public SpriteRenderer eye;
    public SpriteRenderer tail;

    public Sprite movingSprite;

    [Header("돌아가는 값들(0.5번 돌아가는 시간")]
    public float[] rotationAngle = new float[2];
    public float rotationTime;
    public int rotationCount;

    public Ease ease;

    private bool touch;
    public bool Touch { get => touch; set => touch = value; }
    private Coroutine motionCor;
    public Coroutine MotionCor { get => motionCor; set => motionCor = value; }

    [SerializeField]
    private bool showItem = true;
    public bool ShowItem { get => showItem; set => showItem = value; }
    private void Start()
    {
        MotionCor = StartCoroutine(RestMotion());
    }

    public void OnClick()
    {
        if (!Touch)
            StartCoroutine(Motion());

        AudioManager.Instance.PlaySound("fat_cat", transform.position);
    }

    private IEnumerator RestMotion()
    {
        int index = 0;
        while(true)
        {
            body.sprite = bodySprites[index];
            eye.sprite = eyeSprites[index];
            index++;
            index %= eyeSprites.Length;
            yield return new WaitForSeconds(Random.Range(1.5f, 3.0f));
        }
    }

    private IEnumerator Motion()
    {
        int index = 0;
        Touch = true;
        StopCoroutine(MotionCor);
        body.sprite = movingSprite;
        eye.enabled = false;
        tail.enabled = false;
        transform.DORotate(new Vector3(0, 0, rotationAngle[index++]), rotationTime).SetEase(ease);
        yield return new WaitForSeconds(rotationTime);
        for (int i = 0; i < rotationCount; i++)
        {
            transform.DORotate(new Vector3(0, 0, rotationAngle[index++]), rotationTime*2).SetEase(ease);
            index %= rotationAngle.Length;
            yield return new WaitForSeconds(rotationTime * 2);
        }
        transform.DORotate(new Vector3(0, 0, 0), rotationTime).SetEase(Ease.Linear);
        yield return new WaitForSeconds(rotationTime);
        eye.enabled = true;
        tail.enabled = true;
        MotionCor = StartCoroutine(RestMotion());
        Touch = false;
        yield break;
    }
}
