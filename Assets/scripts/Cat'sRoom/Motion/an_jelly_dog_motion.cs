﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class an_jelly_dog_motion : MonoBehaviour, CatHouseCat
{
    private bool touch;
    public bool Touch { get => touch; set => touch = value; }

    private bool haveSock;
    public bool HaveSock { get => haveSock; set => haveSock = value; }

    private float timeSpan;
    public float TimeSpan { get => timeSpan; set => timeSpan = value; }

    private bool motion;
    public bool Motion { get => motion; set => motion = value; }

    [Header("가만히 있는 시간")]
    public float maxStayTime;

    [Header("최대 쓰다듬을 거리?")]
    public float maxStrokeValue;

    [Header("터치 한 후 움직인 거리")]
    private float touchMoveDistance;

    [Header("쓰다듬을 거리")]
    public float distance;

    [Header("모션에 필요한 것들")]
    public SpriteRenderer sock;
    public Sprite standSprite;
    public Sprite biteSprite;
    public Sprite[] walkSprite;

    private SpriteRenderer jelly;

    private Vector3 lastTouchPos;
    private Vector3 currentTouchPos;
    private Vector3 deltaTouchPos;

    private bool biteState;
    public bool BiteState { get => biteState; set => biteState = value; }
    [SerializeField]
    private bool showItem = true;
    public bool ShowItem { get => showItem; set => showItem = value; }

    void Awake()
    {
        jelly = GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        jelly.sprite = standSprite;
    }

    private void MunzilMunzill()
    {
        if (Motion)
            return;

        if (Touch)
            Stroke();

        if (!HaveSock)
        {
            TimeSpan += Time.deltaTime;
            if (TimeSpan >= maxStayTime)
            {
                BiteSock();
                TimeSpan = 0;
                HaveSock = true;
                Touch = true;
            }
        }
    }

    public void OnClick()
    {
        if (!Motion)
        {
            if (BiteState) PutSock();
            else BiteSock();
            BiteState = !BiteState;
            AudioManager.Instance.PlaySound("dog_1", transform.position);
        }
    }

    private void Stroke()
    {
        if (HaveSock)
        {
            if (Input.GetMouseButton(0))
            {
                Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Debug.Log("거리 : " + Vector3.Distance(transform.position, touchPos));
                if (Vector3.Distance(transform.position, touchPos) <= distance)
                {
                    SetCurrentLastTouchPos();
                    touchMoveDistance += Mathf.Abs(deltaTouchPos.x);
                    touchMoveDistance += Mathf.Abs(deltaTouchPos.y);
                }

                if (touchMoveDistance >= maxStrokeValue)
                {
                    touchMoveDistance = 0;
                    HaveSock = false;
                    Touch = false;
                    PutSock();
                }
            }

            else if (Input.GetMouseButtonUp(0))
            {
                touchMoveDistance = 0;
            }
        }
    }

    private void SetCurrentLastTouchPos()
    {
        currentTouchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        deltaTouchPos = currentTouchPos - lastTouchPos;
        lastTouchPos = currentTouchPos;
    }

    private void BiteSock()
    {
        Debug.Log("문다");
        StartCoroutine(_BiteSock());
    }

    private IEnumerator _BiteSock()
    {
        WaitForSeconds wait5 = new WaitForSeconds(0.5f);
        WaitForSeconds wait10 = new WaitForSeconds(1.0f);
        Motion = true;

        sock.transform.localPosition = new Vector3(0, -0.8f, 0);
        jelly.sortingOrder = 1;
        sock.sortingOrder = 2;
        jelly.sprite = biteSprite;
        yield return wait5;

        sock.transform.localPosition = new Vector3(0, -0.3f, 0);
        jelly.sprite = standSprite;
        sock.enabled = true;
        yield return wait10;

        sock.enabled = false;
        // 가는곳 설정해서 가기
        for (int i = 0; i < 4; i++)
        {
            jelly.sprite = walkSprite[0];
            yield return wait5;
            jelly.sprite = walkSprite[1];
            yield return wait5;
        }
        jelly.sprite = standSprite;
        sock.enabled = true;

        Motion = false;
        yield break;
    }

    private void PutSock()
    {
        Debug.Log("뱉는다");
        StartCoroutine(_PutSock());
    }

    private IEnumerator _PutSock()
    {
        WaitForSeconds wait5 = new WaitForSeconds(0.5f);
        Motion = true;

        sock.transform.localPosition = new Vector3(0, -0.8f, 0);
        jelly.sortingOrder = 1;
        sock.sortingOrder = 2;
        jelly.sprite = biteSprite;
        yield return wait5;

        sock.enabled = true;
        jelly.sprite = standSprite;
        sock.enabled = true;

        Motion = false;
        yield break;
    }
}
