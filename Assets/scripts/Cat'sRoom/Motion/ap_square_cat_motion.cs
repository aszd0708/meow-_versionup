﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ap_square_cat_motion : MonoBehaviour, CatHouseCat, AnimalHouseSpecialAnimAnimal
{
    public Transform[] leg = new Transform[4];
    public Transform tail;
    public Transform head;

    public GameObject smallCat;
    private GameObject createSmallCatObj;

    public GameObject UFO;
    private GameObject createUFO;

    private bool touch;
    public bool Touch { get => touch; set => touch = value; }

    private Coroutine moveCor;

    private List<GameObject> createCatList = new List<GameObject>();
    public Transform smallCats;
    private int listIndex;
    public int ListIndex { get => listIndex; set => listIndex = value; }

    public List<SpriteRenderer> sprites = new List<SpriteRenderer>();

    [SerializeField]
    private bool showItem = true;
    public bool ShowItem { get => showItem; set => showItem = value; }
    void Awake()
    {
        SpriteRendererSetting(false);
    }

    void Start()
    {
        ListIndex = -1;
        createUFO = Instantiate(UFO);
        createUFO.GetComponent<ap_square_cat_UFO>().boxCat = transform;
        createUFO.GetComponent<ap_square_cat_UFO>().StartCor();
    }

    private void SpriteRendererSetting(bool turn)
    {
        for (int i = 0; i < sprites.Count; i++)
            sprites[i].enabled = turn;
    }

    public void OnClick()
    {
        CreateCat();
        AudioManager.Instance.PlaySound("ufo_2", transform.position);
    }

    public void StartMotion()
    {
        SpriteRendererSetting(true);
        moveCor = StartCoroutine(_WalkMotion());
    }

    public void StopCor()
    {
        StopCoroutine(moveCor);
    }

    private IEnumerator _WalkMotion()
    {
        int a = 1;
        int index = 0;
        WaitForSeconds waitSeconds = new WaitForSeconds(0.5f);
        Vector3[] headTailRot = new Vector3[2];
        headTailRot[0] = new Vector3(0, 0, 10);
        headTailRot[1] = new Vector3(0, 0, -10);

        Vector3[] legRot = new Vector3[2];
        legRot[0] = new Vector3(0, 0, 20);
        legRot[1] = new Vector3(0, 0, -20);

        while (true)
        {
            head.DORotate(headTailRot[index], 0.5f);
            tail.DORotate(headTailRot[index], 0.5f);
            for (int i = 0; i < 4; i++)
            {
                leg[i].DORotate(legRot[index++], 0.5f);
                index %= legRot.Length;
            }
            index++;
            index %= headTailRot.Length;
            yield return waitSeconds;
        }
    }

    private void CreateCat()
    {
        if(ListIndex >= 20)
        {
            return;
        }
        ListIndex++;
        createCatList.Add(Instantiate(smallCat));
        createCatList[ListIndex].transform.SetParent(smallCats);
        createCatList[ListIndex].transform.localPosition = Vector3.zero;
        createCatList[ListIndex].transform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));
        createCatList[ListIndex].transform.DOLocalJump(new Vector3(transform.position.x + Random.Range(-4f, 4f), Random.Range(-2f,1.5f)), 1, Random.Range(1, 4), 1.0f);
    }

    public void SetNull()
    {
        smallCats.gameObject.SetActive(false);
    }

    public float StartBack()
    {
        createUFO.GetComponent<ap_square_cat_UFO>().StartHome();
        return 7 + createUFO.GetComponent<ap_square_cat_UFO>().moveTime*2;
    }

    public float GetGoBackAnimTime()
    {
        createUFO.GetComponent<ap_square_cat_UFO>().StartHome();
        return 7 + createUFO.GetComponent<ap_square_cat_UFO>().moveTime * 2;
    }
}