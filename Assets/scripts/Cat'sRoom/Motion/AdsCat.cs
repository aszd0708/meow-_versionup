﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AdsCat : MonoBehaviour, CatHouseCat
{
    public List<Sprite> catImg = new List<Sprite>();

    private Transform lookAtObj;
    public float changeTime;
    public float flipTime;

    private SpriteRenderer catSprite;

    private bool touch;
    private bool showItem = true;

    public bool Touch { get => touch; set => touch = value; }
    public bool ShowItem { get => showItem; set => showItem = value; }

    void Awake()
    {
        lookAtObj = GameObject.Find("Item").transform;
        if (SceneManager.GetActiveScene().name != "Cat'sRoom")
            catSprite = transform.GetChild(0).GetComponent<SpriteRenderer>();
        else
            catSprite = GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpriteMotion());
        StartCoroutine(FlipMotion());
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.Euler(0, 0, GetAngle(lookAtObj.position) - 90);
    }

    private IEnumerator SpriteMotion()
    {
        int index = 0;
        WaitForSeconds wait = new WaitForSeconds(changeTime);
        while (true)
        {
            catSprite.sprite = catImg[index++];
            index %= catImg.Count;
            yield return wait;
        }
    }

    private IEnumerator FlipMotion()
    {
        bool flip = true;
        WaitForSeconds flipWait = new WaitForSeconds(flipTime);
        while (true)
        {
            catSprite.flipX = flip;
            flip = !flip;
            yield return flipWait;
        }
    }

    private float GetAngle(Vector2 randomPos)
    {
        float angle;

        angle = Mathf.Atan2(randomPos.y - transform.position.y,
                            randomPos.x - transform.position.x) * 180 / Mathf.PI;
        return angle;
    }

    public void OnClick()
    {
        AudioManager.Instance.PlaySound("cat_5", transform.position);
    }
}
