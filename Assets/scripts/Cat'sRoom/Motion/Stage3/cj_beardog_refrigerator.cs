﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class cj_beardog_refrigerator : MonoBehaviour, CatHouseCat
{
    private bool touch;

    public bool Touch { get => touch; set => touch = value; }

    [SerializeField]
    private SpriteRenderer[] doorRanderers;

    [SerializeField]
    private Sprite openSprite;
    [SerializeField]
    private Sprite closeSprite;

    [SerializeField]
    private GameObject meatObj;
    
    [SerializeField]
    private Transform meatParent;

    [SerializeField]
    private float randomValue = 3.0f;

    [SerializeField]
    private bool showItem = true;
    public bool ShowItem { get => showItem; set => showItem = value; }

    private bool bOpen;
    
    public void OnClick()
    {
        if(Touch)
        {
            return;
        }
            StartCoroutine(_Open());
    }

    private IEnumerator _a()
    {
        while(true)
        {
            StartCoroutine(_Open());
            yield return new WaitForSeconds(0.1f);
        }
    }

    private IEnumerator _Open()
    {
        Touch = true;
        int randomIndeax = Random.Range(0, doorRanderers.Length);
        doorRanderers[randomIndeax].sprite = openSprite;
        StartCoroutine(_CreateMeat(doorRanderers[randomIndeax].transform.position));
        yield return new WaitForSeconds(0.1f);
        doorRanderers[randomIndeax].sprite = closeSprite;
        Touch = false;
        yield break;
    }



    private IEnumerator _CreateMeat(Vector2 createPose)
    {
        GameObject createObj = PoolingManager.Instance.GetPool("Meat");
        if (!createObj)
        {
            createObj = Instantiate(meatObj);
        }
        createObj.SetActive(true);
        createObj.transform.position = createPose;
        createObj.transform.DOMove(GetDropPose(createPose) * randomValue, 0.5f);
        yield return new WaitForSeconds(0.5f);
        createObj.transform.SetParent(meatParent);
        yield break;
    }

    private Vector2 GetDropPose(Vector2 startPose)
    {
        Vector2 dropPose = startPose + (Vector2)Random.insideUnitSphere;
        return dropPose;
    }

    private void OnDisable()
    {
        for(int i = 0; i < meatParent.childCount; i++)
        {
            PoolingManager.Instance.SetPool(meatParent.GetChild(i).gameObject, "Meat");
        }
    }
}
