﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cd_banggal : MonoBehaviour, CatHouseCat
{
    [Header("각 부분 SpriteRenderer")]
    public SpriteRenderer bodySprite, tailSprite, eyeSprite, mouthSprite, faceSprite;

    [Header("각 부분 Sprites")]
    public Sprite[] bodySprites, faceSprites, eyeSprites;

    [Header("눈 깜빡이는 속도")]
    public float blinkTime;

    [Header("바디 스프라이트 변하는 속도")]
    public float bodyChageTime;

    [Header("화내고 있는 시간")]
    public float angryTime;

    [Header("흔드든데 필요한 변수들")]
    public float vibePower;
    public int vibrator;

    public SpriteRenderer item;

    private Coroutine eyeCor;

    private bool canTouch;
    public bool CanTouch { get => canTouch; set => canTouch = value; }
    public bool Touch { get => touch; set => touch = value; }

    private bool touch;

    #region Shake and Movement Corountine

    [SerializeField]
    private float movementStartDistance = 1.0f;

    [SerializeField]
    private bool doShake = false;

    private Vector3 originPose;

    [SerializeField]
    private float shakePower;

    [SerializeField]
    private Transform dollTranform;
    public Transform DollTranform { get => dollTranform; set => dollTranform = value; }

    private bool haveDoll = false;

    private bool playMotionCoroutine;
    private Coroutine motionCor;
    #endregion

    [SerializeField]
    private bool showItem = true;
    public bool ShowItem { get => showItem; set => showItem = value; }
    private void OnEnable()
    {
        eyeCor = StartCoroutine(_EyeMotion());
        //SetSprites(false);
        Invoke("SetOriginPose", 0.5f);
    }

    private void SetOriginPose()
    {
        originPose = transform.position;
    }

    private void Update()
    {
        ShakePosition();
        CheckDollDistance();
    }

    public void OnClick()
    {
        AudioManager.Instance.PlaySound("cat_8", transform.position);
        if (!CanTouch)
            return;

        StartCoroutine(_AngryMotion());
    }

    private IEnumerator _EyeMotion()
    {
        int randomCount;
        int eyeIndex = 0;
        WaitForSeconds blinkWait = new WaitForSeconds(blinkTime);
        WaitForSeconds waitTime = new WaitForSeconds(5.0f);
        while (true)
        {
            randomCount = Random.Range(0, 4);

            for (int i = 0; i < randomCount * 2; i++)
            {
                eyeSprite.sprite = eyeSprites[eyeIndex++];
                eyeIndex %= eyeSprites.Length;
                yield return blinkWait;
            }
            yield return waitTime;
        }
    }

    private IEnumerator _AngryMotion()
    {
        CanTouch = false;
        WaitForSeconds bodyChangeWait = new WaitForSeconds(bodyChageTime);
        WaitForSeconds angryWait = new WaitForSeconds(angryTime);
        SetAngry();
        for (int i = 1; i < bodySprites.Length; i++)
        {
            bodySprite.sprite = bodySprites[i];
            faceSprite.sprite = faceSprites[i - 1];
            if (i == bodySprites.Length - 1) tailSprite.enabled = true;
            yield return bodyChangeWait;
        }
        doShake = true;
        //transform.DOShakePosition(angryTime, vibePower, vibrator);
        yield return new WaitWhile(() => haveDoll);
        StartCoroutine(_ReturnMotion());
        yield break;
    }

    private IEnumerator _ReturnMotion()
    {
        WaitForSeconds bodyChangeWait = new WaitForSeconds(bodyChageTime);
        doShake = false;
        tailSprite.enabled = false;
        for (int i = bodySprites.Length - 2; i >= 1; i--)
        {
            bodySprite.sprite = bodySprites[i];
            faceSprite.sprite = faceSprites[i];
            yield return bodyChangeWait;
        }
        SetIdle();
        CanTouch = true;

        motionCor = null;

        yield break;
    }

    private void SetIdle()
    {
        faceSprite.enabled = false;
        bodySprite.sprite = bodySprites[0];
        eyeSprite.enabled = true;
        mouthSprite.enabled = true;

        eyeCor = StartCoroutine(_EyeMotion());
    }

    private void SetAngry()
    {
        faceSprite.enabled = true;
        bodySprite.sprite = bodySprites[0];
        eyeSprite.enabled = false;
        mouthSprite.enabled = false;

        StopCoroutine(eyeCor);
    }

    public void SetSprites(bool isOn)
    {
        bodySprite.enabled = isOn;
        tailSprite.enabled = isOn;
        eyeSprite.enabled = isOn;
        mouthSprite.enabled = isOn;
        faceSprite.enabled = isOn;
    }

    private void ShakePosition()
    {
        if(doShake)
        {
            transform.position = originPose + Random.insideUnitSphere * shakePower;
        }
    }

    private void CheckDollDistance()
    {
        if(DollTranform)
        {
            if(Vector2.Distance(transform.position, DollTranform.position) <= movementStartDistance)
            {
                if(!playMotionCoroutine)
                {
                    if(motionCor == null)
                    {
                        motionCor = StartCoroutine(_AngryMotion());
                        playMotionCoroutine = true;
                    }
                }
                haveDoll = true;
            }

            else
            {
                if(playMotionCoroutine)
                {
                    playMotionCoroutine = false;
                }
                else
                {
                    haveDoll = false;
                }
            }
        }
    }
}
