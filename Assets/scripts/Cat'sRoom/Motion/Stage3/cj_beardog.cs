﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cj_beardog : MonoBehaviour, CatHouseCat
{
    [Header("각 부분들 스프라이트 랜더러")]
    public SpriteRenderer bodySprite, frontFaceSprite, sideFaceSprite, movementEyeSprite;

    [Header("각 부분들 스프라이트들")]
    public Sprite[] bodySprites, frontFaceSprites, sideFaceSprites;

    [Header("각 부분들 Trasnform")]
    public Transform bodyTransform, faceTransform, tailTrasnform;

    [Header("(아이템 주기 전)꼬리 흔드는 시간")]
    public float nonItemTailShakeTime;
    [Header("(아이템 주기 전)꼬리 흔드는 시간")]
    public float itemTailShakeTime;

    [SerializeField]
    private Transform destinationTransform;

    [SerializeField]
    private Sprite[] movementBodySprites;

    [SerializeField]
    private Sprite idleSprite;

    [SerializeField]
    private Sprite happySprite;

    private Coroutine tailCor;

    #region AnimalHouse
   
    [SerializeField]
    private float moveSpeed;

    private bool isMove;
    
    [Header("아이템 부모")]
    [SerializeField]
    private Transform itemParent;

    #endregion

    [SerializeField]
    private bool showItem = true;
    public bool ShowItem { get => showItem; set => showItem = value; }



    private bool touch;
    public bool Touch { get => touch; set => touch = value; }


    private void OnEnable()
    {
        tailCor = StartCoroutine(_TailMotion(itemTailShakeTime));
        StartCoroutine(_FindTarget());
    }

    private IEnumerator _TailMotion(float shakeTime)
    {
        WaitForSeconds shakeWait = new WaitForSeconds(shakeTime);
        Vector3[] shakeRot = { new Vector3(0, 0, 20), new Vector3(0, 0, -20) };
        int index = 0;
        while (true)
        {
            if(!isMove)
            {
                tailTrasnform.DOLocalRotate(shakeRot[index++], shakeTime);
                index %= shakeRot.Length;
            }
            yield return shakeWait;
        }
    }

    public void StartItemEvent(GameObject itemObj)
    {
        destinationTransform.GetComponent<SpriteRenderer>().enabled = true;
    }

    private void SetArriveSprite()
    {
        bodySprite.sprite = happySprite;
        frontFaceSprite.enabled = true;
        sideFaceSprite.enabled = false;
    }

    private void SetMoveToTargetSprite()
    {
        frontFaceSprite.enabled = false;
        sideFaceSprite.enabled = true;
    }

    private IEnumerator _BodyMotion()
    {
        int index = 0;
        frontFaceSprite.enabled = false;
        sideFaceSprite.enabled = true;
        while(true)
        {
            bodySprite.sprite = movementBodySprites[index++];
            index %= movementBodySprites.Length;
            yield return new WaitForSeconds(2.0f / 8f);
        }
    }
    private IEnumerator _GoDestination(GameObject movePose)
    {
        SetMoveToTargetSprite();
        isMove = true;
        Coroutine walkingMotionCor = StartCoroutine(_BodyMotion());
        while (Vector2.Distance(movePose.transform.position, transform.position) >= 1.5f)
        {
            Vector2 direction = movePose.transform.position - transform.position;

            if (movePose.transform.position.x > transform.position.x)
            {
                transform.rotation = Quaternion.Euler(Vector3.up * 180);
                Vector3 flipDestVector = new Vector3(-1, 1);
                direction *= flipDestVector;
            }
            else
            {
                transform.rotation = Quaternion.identity;
            }

            transform.Translate(direction.normalized * moveSpeed * Time.deltaTime);

            yield return new WaitForEndOfFrame();
        }
        PoolingManager.Instance.SetPool(movePose, "Meat");
        isMove = false;
        StopCoroutine(walkingMotionCor);
        SetArriveSprite();
        StartCoroutine(_FindTarget());
        yield break;
    }

    private IEnumerator _FindTarget()
    {
        yield return new WaitForSeconds(0.1f);
        while (true)
        {
            if (itemParent.childCount == 0)
            {
                yield return new WaitForSeconds(1.0f);
                continue;
            }

            else
            {
                GameObject item = itemParent.GetChild(Random.Range(0, itemParent.childCount)).gameObject;
                StartCoroutine(_GoDestination(item));
                yield break;
            }
        }
    }

    public void OnClick()
    {
        AudioManager.Instance.PlaySound("dog_1", transform.position);
    }
}
