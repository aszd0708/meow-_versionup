﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cf_flex : MonoBehaviour, CatHouseCat
{
    [SerializeField]
    private Transform windowTransform;

    [SerializeField]
    private Transform[] wheels = new Transform[2];

    [SerializeField]
    private SpriteRenderer headRenderer;

    [SerializeField]
    private Sprite[] headSprites = new Sprite[2];

    [Header("바퀴 가속도")]
    [SerializeField]
    private float speed;

    [Header("최대 속도")]
    [SerializeField]
    private float maxSpeed;

    private float nowSpeed = 0, rot = 0;

    private bool isOpen = false;
    private bool isMotion = false;

    private Vector2 movePose;

    private bool isRight;

    private bool touch;

    public bool Touch { get => touch; set => touch = value; }

    [SerializeField]
    private bool showItem = true;
    public bool ShowItem { get => showItem; set => showItem = value; }
    private void OnEnable()
    {
        StartCoroutine(_PlayHeadMotion(0.5f));
    }

    private void Update()
    {
        if(Input.GetMouseButton(0))
        {
            movePose = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //if(AnimalHouseCheckGroundVector.Instance)
            //{
            //    if(!AnimalHouseCheckGroundVector.Instance.IsInsideOnTriangle(movePose))
            //    {
            //        movePose = transform.position;
            //    }
            //}
            Accelate();
        }

        else
        {
            Stop();
            //rot -= Time.deltaTime * 10;
            //if (rot <= 0)
            //{
            //    rot = 0;
            //}
        }
        RotateWheels();
        DriveCar();
    }
    private IEnumerator _PlayHeadMotion(float time)
    {
        WaitForSeconds wait = new WaitForSeconds(time);

        int index = 0;
        while(true)
        {
            headRenderer.sprite = headSprites[index++];
            index %= headSprites.Length;
            yield return wait;
        }
    }

    public void OnClick()
    {
        if(isMotion)
        {
            return;
        }

        AudioManager.Instance.PlaySound("car", transform.position);
        StartCoroutine(_WindowControl());
    }

    private IEnumerator _WindowControl()
    {
        isMotion = true;
        if (isOpen)
        {
            windowTransform.DOLocalMoveY(0.42f, 0.5f);
        }
        else
        {
            windowTransform.DOLocalMoveY(-0.71f, 0.5f);
        }
        yield return new WaitForSeconds(0.5f);
        isOpen = !isOpen;
        isMotion = false;
        yield break;
    }

    private void DriveCar()
    {
        rot += Time.deltaTime * nowSpeed;
        rot %= 360;
        nowSpeed = Mathf.Clamp(nowSpeed, 0, maxSpeed);

        MoveCar();
    }

    private void RotateWheels()
    {
        if (Vector3.Distance(transform.position, movePose) < 0.5f)
        {
            return;
        }
        float min = -1;
        if(!isRight)
        {
            min = 1;
        }
        for (int i = 0; i < wheels.Length; i++)
        {
            wheels[i].rotation = Quaternion.Euler(0, 0, rot * 50* min);
        }
    }

    private void Accelate()
    {
        nowSpeed += speed;
    }

    private void Stop()
    {
        if (nowSpeed <= 0)
            return;
        nowSpeed -= speed;
        if (rot <= 0)
            rot = 0;
    }

    private void MoveCar()
    {
        Vector2 direction = movePose - (Vector2)transform.position;

        if (Vector3.Distance(transform.position, movePose) < 2.0f)
        {
            return;
        }

        if (movePose.x < transform.position.x)
        {
            transform.rotation = Quaternion.Euler(Vector3.up * 180);
            Vector3 flipDestVector = new Vector3(-1, 1);
            direction *= flipDestVector;
            isRight = false;
        }
        else
        {
            transform.rotation = Quaternion.identity;
            isRight = true;
        }

        Vector2 transformPose = (Vector2)transform.position + direction.normalized * nowSpeed * Time.deltaTime;
        Debug.Log("XY : " + transformPose);
        Vector2 transformPoseX = transformPose * Vector2.right;
        Vector2 transformPoseY = transformPose * Vector2.up;
        Vector2 moveTransform = Vector2.zero;

        if (AnimalHouseCheckGroundVector.Instance.IsInsideOnRangeX(transformPose))
        {
            Debug.Log("Y : " + transformPoseY);
            moveTransform += direction.normalized * Vector2.right;
        }
        else
        {
            return;
        }

        Debug.Log("X : " + transformPoseX);
        if (AnimalHouseCheckGroundVector.Instance.IsInsideOnRangeY(transformPose))
        {
            moveTransform += direction.normalized * Vector2.up;
        }
        else
        {
            return;
        }
        //transform.Translate(moveTransform - (Vector2)transform.position);

        transform.Translate(moveTransform * nowSpeed * Time.deltaTime);
    }
}
