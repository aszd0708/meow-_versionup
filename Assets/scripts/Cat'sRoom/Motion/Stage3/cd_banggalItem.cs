﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cd_banggalItem : MonoBehaviour
{
    private void Start()
    {
        StartCoroutine(_FindAnimal());
    }

    private IEnumerator _FindAnimal()
    {
        bool isFind = false;
        cd_banggal animal;
        while (!isFind)
        {
            animal = FindObjectOfType<cd_banggal>();

            if(animal)
            {
                animal.DollTranform = transform;
                isFind = true;
                yield break;
            }
            yield return new WaitForSeconds(0.1f);
        }
    }
}
