﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ci_lioncat : MonoBehaviour, CatHouseCat
{
    [Header("각 부분들 SpriteRenderer")]
    public SpriteRenderer bodySprite, eyeSprite, mouthSprite, tailSprite;

    [Header("각 부분들 Sprite")]
    public Sprite[] bodySprites, tailSprites, eyeSprites;

    [Header("눈 감빡이는 시간")]
    public float blinkTime;

    [Header("짖는 시간")]
    public float barkTime;
    [Header("일어서고 앉는 시간")]
    public float standTime;

    [Header("꼬리 움직일때 필요한 컴포넌트")]
    public Transform tailTransform;
    [Header("앉아 있을 때 꼬리 움직이는 각도(최소 최대)")]
    public float[] sitTailAngle = new float[2];
    [Header("서 있을 때 꼬리 움직이는 각도(최소 최대)")]
    public float[] standTailAngle = new float[2];
    [Header("꼬리 움직이는 시간")]
    public float tailMoveTime;

    [Header("카메라 흔들릴때 씀 솔직히 빼도 괜찮음 ㅋㅋ")]
    public Transform mainCam;
    [Header("흔드든데 필요한 변수들")]
    public float vibePower;
    public int vibrator;

    private Coroutine eyeCor, tailCor;

    private bool touch = false;
    public bool Touch { get => touch; set => touch = value; }

    [SerializeField]
    private bool showItem = true;
    public bool ShowItem { get => showItem; set => showItem = value; }
    public void OnClick()
    {
        if (Touch)
            return;
        StartCoroutine(_StandMotion());
        AudioManager.Instance.PlaySound("cat_9", transform.position);
    }

    private void OnEnable()
    {
        eyeCor = StartCoroutine(_EyeMotion());
        tailCor = StartCoroutine(_SitTailMotion());

        mainCam = Camera.main.transform;
    }

    private IEnumerator _EyeMotion()
    {
        int randomCount;
        int eyeIndex = 0;
        WaitForSeconds blinkWait = new WaitForSeconds(blinkTime);
        WaitForSeconds waitTime = new WaitForSeconds(5.0f);
        while (true)
        {
            randomCount = Random.Range(0, 4);

            for (int i = 0; i < randomCount * 2; i++)
            {
                eyeSprite.sprite = eyeSprites[eyeIndex++];
                eyeIndex %= eyeSprites.Length;
                yield return blinkWait;
            }
            yield return waitTime;
        }
    }

    private IEnumerator _SitTailMotion()
    {
        Vector3[] moveAngle = new Vector3[2];
        moveAngle[0] = new Vector3(0, 0, sitTailAngle[0]);
        moveAngle[1] = new Vector3(0, 0, sitTailAngle[1]);
        int moveAngleIndex = 0;
        WaitForSeconds tailWait = new WaitForSeconds(tailMoveTime);
        while (true)
        {
            tailTransform.DORotate(moveAngle[moveAngleIndex++], tailMoveTime).SetEase(Ease.OutCubic);
            moveAngleIndex %= moveAngle.Length;
            yield return tailWait;
        }
    }

    private IEnumerator _StandMotion()
    {
        SetStanding();
        Touch = true;
        int random = Random.Range(1, 4);
        WaitForSeconds barkWait = new WaitForSeconds(barkTime);
        WaitForSeconds standWait = new WaitForSeconds(standTime);
        int index = 0;
        yield return standWait;

        for (int i = 0; i < random * 2; i++)
        {
            bodySprite.sprite = bodySprites[index + 2];
            index++;
            index %= bodySprites.Length - 2;
            if(mainCam)
            {
                mainCam.DOShakePosition(barkTime, vibePower, vibrator);
            }
            yield return barkWait;
        }
        bodySprite.sprite = bodySprites[1];
        yield return standWait;
        Touch = false;
        SetSitting();
        yield break;
    }

    private void SetSitting()
    {
        bodySprite.sprite = bodySprites[0];
        eyeSprite.enabled = true;
        mouthSprite.enabled = true;
        tailSprite.sprite = tailSprites[0];

        eyeCor = StartCoroutine(_EyeMotion());
        tailCor = StartCoroutine(_SitTailMotion());
    }

    private void SetStanding()
    {
        StopCoroutine(eyeCor);
        StopCoroutine(tailCor);
        tailTransform.DOPause();

        bodySprite.sprite = bodySprites[1];
        eyeSprite.enabled = false;
        mouthSprite.enabled = false;
        tailSprite.sprite = tailSprites[1];
    }
}
