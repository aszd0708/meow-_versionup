﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ch_jumppy : MonoBehaviour, CatHouseCat
{

    [Header("부분별 스프라이트 랜더러")]
    public SpriteRenderer bodySprite, tailSprite;

    [Header("각 부분 스프라이트")]
    public Sprite[] bodySprites, tailSprites;
    public Sprite standingSprite, catchSprite;

    [SerializeField]
    private Sprite jumpingSprite;
    [SerializeField]
    private Sprite landingSprite;

    [Header("몸 부분 스프라이트 변경 시간")]
    public float bodyChangeTime;

    [Header("꼬리 트랜스폼")]
    public Transform tailTransform;

    [Header("먹이에게 가기 전에 흔드는 횟수")]
    public int shakingCount;
    public float shakingTime;

    [Header("꼬리가 움직이는 앵글")]
    public float tailAngle;

    #region AnimalHouse
    private bool touch = true;
    public bool Touch { get => touch; set => touch = value; }

    [SerializeField]
    private Transform target;

    [SerializeField]
    private float targetDistance = 5;

    [SerializeField]
    private float jumpTime;

    private bool playMotion = false;

    private Coroutine bodyCor;

    private bool lockonTarget;

    #endregion

    [SerializeField]
    private bool showItem = true;
    public bool ShowItem { get => showItem; set => showItem = value; }
    private void Start()
    {
        bodySprite.sprite = standingSprite;
    }

    private void Update()
    {
        if(CheckTarget())
        {
            if (!playMotion)
            {
                if (bodyCor == null)
                {
                    bodyCor = StartCoroutine(_JumpToTarget());
                    playMotion = true;
                }
            }
            lockonTarget = true;
        }
        else
        {
            if (playMotion)
            {
                playMotion = false;
            }
            else
            {
                lockonTarget = false;
            }
        }
    }

    public void OnClick()
    {
        AudioManager.Instance.PlaySound("cat_5", transform.position);
    }

    private IEnumerator _JumpToTarget()
    {
        int index = 0;
        bool isFlip = false;
        WaitForSeconds shakingWait = new WaitForSeconds(shakingTime);

        isFlip = target.position.x - transform.position.x < 0 ? false : true;

        if(isFlip)
        {
            transform.rotation = Quaternion.Euler(Vector3.up * 180);
        }
        else
        {
            transform.rotation = Quaternion.identity;
        }

        StartCoroutine(_TailMotion(shakingCount * 3 * shakingTime));
        for (int i = 0; i < shakingCount * 3; i++)
        {
            bodySprite.sprite = bodySprites[index++];
            index %= bodySprites.Length;
            yield return shakingWait;
        }
 
        if (lockonTarget)
        {
            Vector2 arrivePose;
            if(isFlip)
            {
                arrivePose = new Vector2((target.position.x - transform.position.x), transform.position.y);
            }
            else
            {
                arrivePose = new Vector2((target.position.x - transform.position.x), transform.position.y);
            }
            float jumpHeight = target.position.y - transform.position.y;
            transform.DOJump(arrivePose, jumpHeight, 1, jumpTime);
            bodySprite.sprite = jumpingSprite;
            yield return new WaitForSeconds(jumpTime);
        }
        bodySprite.sprite = landingSprite;
        yield return new WaitForSeconds(1.0f);
        bodySprite.sprite = standingSprite;
        bodyCor = null;
        playMotion = false;
        yield break;
    }

    private bool CheckTarget()
    {
        if (target)
        {
            if (target.position.y > transform.position.y && Vector2.Distance(transform.position, target.position) <= targetDistance)
            {
                return true;
            }
        }
        return false;
    }

    private IEnumerator _TailMotion(float time)
    {
        WaitForSeconds waitTime = new WaitForSeconds(time / 5);
        Vector3[] angle = { new Vector3(0, 0, tailAngle), new Vector3(0, 0, -tailAngle) };
        int index = 0;
        for (int i = 0; i < 5; i++)
        {
            tailTransform.DORotate(angle[index++], time / 5);
            index %= angle.Length;
            yield return waitTime;
        }
        yield break;
    }
}
