﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cc_bbaru_touch : MonoBehaviour
{
    [SerializeField]
    [Header("원하는 터치 시간")]
    private float touchTime;
    private float nowTouchTime;

    private Vector3 downTouchPose;

    [SerializeField]
    private GameObject createObj;

    [SerializeField]
    private Transform objParent;
    [SerializeField]
    private bool showItem = true;
    public bool ShowItem { get => showItem; set => showItem = value; }
    void Update()
    {
        // UI터치 막음
        if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            return;

        if (Input.GetMouseButtonDown(0))
        {
            downTouchPose = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }

        if (Input.GetMouseButton(0))
        {
            nowTouchTime += Time.deltaTime;
        }

        else if (Input.GetMouseButtonUp(0))
        {
            Vector3 touchPose = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(touchPose, Vector2.zero, 0.0f);

            if (nowTouchTime <= touchTime)
            {
                if (!hit.transform)
                {
                    if(AnimalHouseCheckGroundVector.Instance.IsInsideOnTriangle(touchPose))
                    {
                        nowTouchTime = 0;
                        CreateCach(touchPose);
                    }
                }
            }
            nowTouchTime = 0;
        }
    }

    private void CreateCach(Vector2 createPose)
    {
        GameObject obj = PoolingManager.Instance.GetPool("Cach");
        if(!obj)
        {
            obj = Instantiate(createObj);
        }
        obj.SetActive(true);
        obj.transform.position = createPose;
        obj.transform.SetParent(objParent);
    }

    public void Clear()
    {
        for(int i = 0; i < objParent.childCount; i++)
        {
            PoolingManager.Instance.SetPool(objParent.GetChild(i).gameObject, "Cach");
        }
    }
}
