﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cc_bbaru : MonoBehaviour
{
    [Header("각 부분들 SpriteRenderer")]
    public SpriteRenderer bodySprite, eyeSprite, tailSprite;
    [Header("각 부분들 Sprite")]
    public Sprite sitBodySprite;
    public Sprite[] walkingBodySprites, eyeSprites;

    [Header("각 부분들 Transform")]
    public Transform eyeTransform, mouthTransform, tailTransform;

    [Header("꼬리 흔드는 시간")]
    public float tailTime;

    [Header("눈 깜빡이는 시간")]
    public float blinkTime;
    [Header("걸을 때 스프라이트가 바뀌는 시간")]
    public float walkingChangeTime;
    [Header("총 걷는 시간")]
    public float walkingTime;

    public float moveSpeed;

    private Coroutine tailCor;
    private Coroutine eyeCor;

    private bool canTouch = false;
    public bool CanTouch { get => canTouch; set => canTouch = value; }

    [SerializeField]
    private Transform itemParents;

    [SerializeField]
    private bool showItem = true;
    public bool ShowItem { get => showItem; set => showItem = value; }
    private void Start()
    {
        tailCor = StartCoroutine(_TailMotion());
        eyeCor = StartCoroutine(_EyeMotion());

        StartCoroutine(_FindTarget());
    }

    public void OnClick()
    {
        AudioManager.Instance.PlaySound("fat_cat", transform.position);
    }

    private IEnumerator _EyeMotion()
    {
        int randomCount;
        int eyeIndex = 0;
        WaitForSeconds blinkWait = new WaitForSeconds(blinkTime);
        WaitForSeconds waitTime = new WaitForSeconds(5.0f);
        while (true)
        {
            randomCount = Random.Range(0, 4);

            for (int i = 0; i < randomCount * 2; i++)
            {
                eyeSprite.sprite = eyeSprites[eyeIndex++];
                eyeIndex %= eyeSprites.Length;
                yield return blinkWait;
            }
            yield return waitTime;
        }
    }

    private IEnumerator _WalkingMotion()
    {
        int index = 0;
        WaitForSeconds spriteChangeWait = new WaitForSeconds(walkingChangeTime);

        while (true)
        {
            bodySprite.sprite = walkingBodySprites[index++];
            index %= walkingBodySprites.Length;
            yield return spriteChangeWait;
        }
    }

    private IEnumerator _TailMotion()
    {
        int index = 0;
        Vector3[] rotationAngle = { new Vector3(0, 0, 20), new Vector3(0, 0, -20) };
        WaitForSeconds tailWait = new WaitForSeconds(tailTime);

        while (true)
        {
            tailTransform.DORotate(rotationAngle[index++], tailTime).SetEase(Ease.OutCubic);
            index %= rotationAngle.Length;
            yield return tailWait;
        }
    }

    private IEnumerator _GoDestination(GameObject movePose)
    {
        SetWalking();

        Coroutine walkingMotionCor = StartCoroutine(_WalkingMotion());
        while (Vector2.Distance(movePose.transform.position, transform.position) >= 0.5f)
        {
            Vector2 direction = movePose.transform.position - transform.position;

            if(movePose.transform.position.x > transform.position.x)
            {
                transform.rotation = Quaternion.Euler(Vector3.up * 180);
                Vector3 flipDestVector = new Vector3(-1, 1);
                direction *= flipDestVector;
            }
            else
            {
                transform.rotation = Quaternion.identity;
            }

            transform.Translate(direction.normalized * moveSpeed * Time.deltaTime);

            yield return null;
        }
        PoolingManager.Instance.SetPool(movePose, "Cach");
        StopCoroutine(walkingMotionCor);
        SetSitting();
        StartCoroutine(_FindTarget());
        yield break;
    }

    private void SetSitting()
    {
        bodySprite.sprite = sitBodySprite;
        tailCor = StartCoroutine(_TailMotion());
    }

    private void SetWalking()
    {
        StopCoroutine(tailCor);
        tailTransform.Rotate(Vector3.zero);
    }

    private IEnumerator _FindTarget()
    {
        yield return new WaitForSeconds(0.1f);
        while (true)
        {
            if (itemParents.childCount == 0)
            {
                yield return new WaitForSeconds(1.0f);
                continue;
            }

            else
            {
                GameObject item = itemParents.GetChild(Random.Range(0, itemParents.childCount)).gameObject;
                StartCoroutine(_GoDestination(item));
                yield break;
            }
        }
    }
}
