﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ce_trashcat_item : MonoBehaviour, CatHouseDragObject
{
    [Header("확인하기 위한 Key값")]
    [SerializeField]
    private int key;

    private ce_trashcat animal;

    [SerializeField]
    private bool touch;

    public bool Touch { get => touch; set => touch = value; }

    private float objectDistance = 1.0f;

    private Vector2 originPose;

    [SerializeField]
    private bool showItem = true;
    public bool ShowItem { get => showItem; set => showItem = value; }
    private void Start()
    {
        if(!animal)
        {
            animal = FindObjectOfType<ce_trashcat>();
        }

        Invoke("SetPosition", 0.5f);
    }

    void SetPosition()
    {
        originPose = transform.position;
    }

    private void Update()
    {
        if(Touch)
        {
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            transform.position = (Vector2)touchPos;

            if (Input.GetMouseButtonUp(0))
            {
                Touch = false;


                if (Vector2.Distance(transform.position, animal.transform.position) < objectDistance)
                {
                    CheckItem();
                }

                else
                {
                    transform.position = originPose;
                }
            }
        }
    }

    private void CheckItem()
    {
        if(animal)
        {
            animal.ItemEvent(key, gameObject);
        }
    }
}
