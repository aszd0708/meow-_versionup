﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class db_tiger_dduck : MonoBehaviour, CatHouseDragObject
{
    private bool touch;

    public bool Touch { get => touch; set => touch = value; }

    [SerializeField]
    private db_tiger tiger;

    private void Update()
    {
        if(Touch == true)
        {
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            transform.position = touchPos;

            tiger.FollowDduck = true;

            if (Input.GetMouseButtonUp(0))
            {
                Touch = false;
            }
        }

        else
        {
            float distance = Vector2.Distance(transform.position, tiger.transform.position);

            if (distance < 0.5f)
            {
                SetOffPosition();
                tiger.FollowDduck = false;
            }
        }
    }


    private void SetOffPosition()
    {
        tiger.SetDduckPose(transform);
    }
}
