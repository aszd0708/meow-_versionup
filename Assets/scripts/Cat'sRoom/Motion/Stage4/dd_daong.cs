﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class dd_daong : MonoBehaviour, CatHouseCat
{
    [SerializeField]
    private SpriteRenderer render;

    [SerializeField]
    private Sprite jumpSprite;
    [SerializeField]
    private Sprite idleSprite;

    [SerializeField]
    private Transform jumpTransform;
    [SerializeField]
    private Transform[] landingTransforms;

    [SerializeField]
    private float jumpPower;

    [SerializeField]
    private float jumpAngle;
    [SerializeField]
    private float laningAngle;

    [SerializeField]
    private float motionDuring;

    [SerializeField]
    private Ease landingEase = Ease.InExpo;

    [SerializeField]
    private Ease jumpingEase = Ease.OutExpo;

    private bool bIsUp;

    private bool touch;
    [SerializeField]
    private bool showItem;

    public bool Touch { get => touch; set => touch = value; }
    public bool ShowItem { get => showItem; set => showItem = value; }

    private bool bCanTouch = true;

    public void OnClick()
    {
        if(bCanTouch == false)
        {
            return;
        }
        JumpToPose();
        AudioManager.Instance.PlaySound("cat_4", transform.position);
    }

    private void JumpToPose()
    {
        if(bIsUp)
        {
            StartCoroutine(_Landing());
        }

        else
        {
            StartCoroutine(_Jumping());
        }
        bIsUp = !bIsUp;
    }

    private IEnumerator _Landing()
    {
        bCanTouch = false;
        Vector2 randomTransform = landingTransforms[Random.Range(0, landingTransforms.Length)].position;
        SetFlip(randomTransform);
        render.sprite = jumpSprite;
        transform.DOJump(randomTransform, jumpPower, 1, motionDuring).SetEase(landingEase);
        Vector3 rotation = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, laningAngle);
        transform.DORotate(rotation, motionDuring).SetEase(landingEase);
        yield return new WaitForSeconds(motionDuring);

        render.sprite = idleSprite;
        bCanTouch = true;
        transform.rotation = Quaternion.Euler(0, 0, 0);
        yield break;
    }

    private IEnumerator _Jumping()
    {
        bCanTouch = false;
        Vector2 dest = jumpTransform.position;
        SetFlip(dest);
        render.sprite = jumpSprite;
        transform.DOJump(dest, jumpPower, 1, motionDuring).SetEase(jumpingEase);
        Vector3 rotation = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, jumpAngle);
        transform.DORotate(rotation, motionDuring).SetEase(jumpingEase);
        yield return new WaitForSeconds(motionDuring);
        render.sprite = idleSprite;
        bCanTouch = true;
        transform.rotation = Quaternion.Euler(0, 0, 0);
        yield break;
    }

    private void SetFlip(Vector3 destination)
    {
        float x = transform.position.x - destination.x;
        if(x >= 0)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
        else
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
    }
}
