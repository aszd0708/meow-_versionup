﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class di_gomsoon_hanes : MonoBehaviour, CatHouseDragObject
{
    private bool touch;

    public bool Touch { get => touch; set => touch = value; }
    public bool BCantouch { get => bCantouch; set => bCantouch = value; }

    [SerializeField]
    private di_gomsoon gomsoon;

    [SerializeField]
    private float eventDistance = 0.5f;

    private bool bCantouch = true;

    private void Update()
    {
        if(bCantouch == false) { return; }

        if(Touch)
        {
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            transform.position = touchPos;
        }

        else
        {
            float distance = Vector2.Distance(transform.position, gomsoon.transform.position);

            if(distance <= eventDistance)
            {
                gomsoon.ItemEvent();
            }
        }

        if(Input.GetMouseButtonUp(0))
        {
            Touch = false;
        }
    }
}
