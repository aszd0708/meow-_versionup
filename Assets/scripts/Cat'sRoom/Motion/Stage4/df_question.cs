﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class df_question : MonoBehaviour, CatHouseCat
{
    [SerializeField]
    private SpriteRenderer bodyRenderer;

    [SerializeField]
    private Sprite[] bodySprites;

    [SerializeField]
    private SpriteRenderer tailRenderer;

    [SerializeField]
    private Sprite questionTailSprites;
    [SerializeField]
    private Sprite exclamationTailSprites;

    [SerializeField]
    private Transform tailTransform;

    [SerializeField]
    private float tailMotionTime;

    public enum State
    {
        QUESTION, EXCLAMATION
    }

    [SerializeField]
    private State nowState;

    private bool canTouch = false;

    Coroutine tailCor;

    private bool touch;
    private bool showItem;

    public bool ShowItem { get => showItem; set => showItem = value; }
    public bool Touch { get => touch; set => touch = value; }

    private void OnEnable()
    {
        bodyRenderer.sprite = bodySprites[0];
        tailCor = StartCoroutine(_QuestionTailMotion());
    }

    private void TailAnim()
    {
        StopCoroutine(tailCor);
        bodyRenderer.sprite = bodySprites[0];
        tailTransform.DOPause();
        StartCoroutine(_ExclamationTailMotion());
    }

    private IEnumerator _QuestionTailMotion()
    {
        WaitForSeconds wait = new WaitForSeconds(1.0f);
        while (true)
        {
            tailTransform.DORotate(Vector3.forward * 15, 1.0f);
            yield return wait;
            tailTransform.DORotate(Vector3.forward * -15, 1.0f);
            yield return wait;
        }
    }

    private IEnumerator _ExclamationTailMotion()
    {
        canTouch = false;
        tailRenderer.sprite = exclamationTailSprites;
        tailTransform.rotation = Quaternion.Euler(Vector3.forward * 20);
        tailTransform.DORotate(Vector3.zero, 0.5f).SetEase(Ease.OutElastic);
        yield return new WaitForSeconds(0.5f);
        nowState = State.QUESTION;
        canTouch = true;
        tailCor = StartCoroutine(_QuestionTailMotion());
        yield break;
    }

    public void OnClick()
    {
        if(canTouch == false)
        {
            return;
        }
        TailAnim();
        AudioManager.Instance.PlaySound("cat_11", transform.position);
    }
}
