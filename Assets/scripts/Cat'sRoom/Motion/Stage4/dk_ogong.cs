﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dk_ogong : MonoBehaviour, CatHouseCat
{
    [Header("움직일때 사용할 변수들")]
    [Header("방향 전환하는 시간")]
    public float angeTime;
    [Header("움직이는 시간(거리에 따라 나눌것")]
    public float moveTime;
    [Header("날개 움직이는 속도")]
    [SerializeField]
    private float maxRotationSpeed = 100.0f;
    [SerializeField]
    private float accelateRotation = 10.0f;

    private float currentRotationSpeed = 0;

    [SerializeField]
    private Transform bongTransform;

    private Vector3 movePos;

    private Coroutine moveCor;
    private Coroutine idleCor;
    private Coroutine wingCor;

    [SerializeField]
    private bool showItem = true;
    public bool ShowItem { get => showItem; set => showItem = value; }
    public enum State
    {
        REST, FLY, LOOK, NOTHING
    }

    private State STATE;

    private bool touch;
    public bool Touch { get => touch; set => touch = value; }

    private bool bIsMove = false;

    public void OnClick()
    {
        AudioManager.Instance.PlaySound("king", transform.position);
    }

    private void Start()
    {
        idleCor = StartCoroutine(Idle());
        //wingCor = StartCoroutine(WingMotion());
    }

    private void Update()
    {
        if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            return;

        SetTouchPosition();
    }
    private IEnumerator Move()
    {
        float distance = Vector2.Distance(transform.position, movePos);
        transform.DOMove(movePos, distance / moveTime);
        yield return new WaitForSeconds(distance / moveTime);
        STATE = State.REST;
        idleCor = StartCoroutine(Idle());
        yield break;
    }

    private IEnumerator Idle()
    {
        Vector3 originPos = transform.localPosition;
        while (true)
        {
            transform.DOLocalMoveY(transform.localPosition.y + 1, 1.5f).SetEase(Ease.OutQuad);
            yield return new WaitForSeconds(1.5f);
            transform.DOLocalMoveY(originPos.y, 1.5f).SetEase(Ease.OutQuad);
            yield return new WaitForSeconds(1.5f);
        }
    }

    private void SetTouchPosition()
    {
        if (Input.GetMouseButton(0))
        {
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            movePos = touchPos;
            StateSetting(State.FLY);
        }
    }

    private void StateSetting(State changeState)
    {
        switch (STATE)
        {
            case State.FLY:
                switch (changeState)
                {
                    case State.FLY:
                        if(bIsMove == true)
                        {
                            return;
                        }
                        StopCoroutine(moveCor);
                        transform.DOKill();
                        moveCor = StartCoroutine(Move());
                        break;
                    case State.REST:
                        StopCoroutine(moveCor);
                        transform.DOKill();
                        //wingCor = StartCoroutine(WingMotion());
                        break;
                }
                break;
            case State.REST:
                switch (changeState)
                {
                    case State.FLY:
                        if (bIsMove == true)
                        {
                            return;
                        }
                        StopCoroutine(idleCor);
                        transform.DOKill();
                        moveCor = StartCoroutine(Move());
                        //StopCoroutine(wingCor);
                        break;
                    case State.REST:
                        break;
                }
                break;
        }
        STATE = changeState;
    }

    private float GetAngle(Vector2 pos)
    {
        float angle;

        angle = Mathf.Atan2(pos.y - transform.position.y,
                            pos.x - transform.position.x) * 180 / Mathf.PI;
        return angle;
    }

    private IEnumerator WingMotion()
    {
        WaitForEndOfFrame wait = new WaitForEndOfFrame();
        while (true)
        {
            currentRotationSpeed += accelateRotation * Time.deltaTime;
            if(currentRotationSpeed >= maxRotationSpeed)
            {
                currentRotationSpeed = maxRotationSpeed;
            }

            bongTransform.rotation = Quaternion.Euler
                (
                bongTransform.rotation.eulerAngles.x, 
                bongTransform.rotation.eulerAngles.y,
                currentRotationSpeed
                );

            yield return wait;
        }
    }
}
