﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class da_tom : MonoBehaviour, CatHouseCat
{
    private bool touch;
    public bool Touch { get => touch; set => touch = value; }


    private bool showItem;

    public bool ShowItem { get => showItem; set => showItem = value; }

    public void OnClick()
    {
        AudioManager.Instance.PlaySound("cat_5", transform.position);
    }
}
