﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dc_cerberus : MonoBehaviour, CatHouseCat
{
    [SerializeField]
    private SpriteRenderer leftBodyRender;
    [SerializeField]
    private Sprite[] leftBodySprites;
    [SerializeField]
    private Sprite leftIdleSprite;
    [SerializeField]
    private SpriteRenderer rightBodyRender;
    [SerializeField]
    private Sprite[] rightBodySprites;
    [SerializeField]
    private Sprite rightIdleSprite;
    [SerializeField]
    private SpriteRenderer centerBodyRender;
    [SerializeField]
    private Sprite[] centerBodySprites;
    [SerializeField]
    private Sprite centerIdleSprite;

    [SerializeField]
    private float restTime;

    [SerializeField]
    private SpriteRenderer leftFaceRender;
    [SerializeField]
    private Sprite[] leftFaceSprites;
    [SerializeField]
    private SpriteRenderer rightFaceRender;
    [SerializeField]
    private Sprite[] rightFaceSprites;
    [SerializeField]
    private SpriteRenderer centerFaceRender;
    [SerializeField]
    private Sprite[] centerFaceSprites;

    private Coroutine moveCor;
    private Coroutine bodySpriteCor;
    private Coroutine faceSpriteCor;

    private Transform[] triTransform = new Transform[3];

    [SerializeField]
    private float maxDistance = 0.5f;

    [SerializeField]
    private float speed = 1.0f;


    private bool touch;
    private bool showItem = true;

    public bool Touch { get => touch; set => touch = value; }
    public bool ShowItem { get => showItem; set => showItem = value; }

    private void Update()
    {
        if(Input.GetMouseButton(0))
        {
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if (AnimalHouseCheckGroundVector.Instance.IsInsideOnTriangle(touchPos))
            {
                Debug.Log("범위 안");
                Vector2 movePose = touchPos - (Vector2)transform.position;
                movePose *= speed * Time.deltaTime;

                float distance = Vector2.Distance(touchPos, transform.position);
                if (distance > maxDistance)
                {
                    transform.position = new Vector2
                        (
                        transform.position.x + movePose.x,
                        transform.position.y + movePose.y
                        );

                    if (bodySpriteCor == null)
                    {
                        bodySpriteCor = StartCoroutine(_MoveSprite());
                    }
                    if (faceSpriteCor == null)
                    {
                        faceSpriteCor = StartCoroutine(_FaceSprite());
                    }
                }

                else
                {
                    SetIdleMotion();
                }
            }

            else
            {
                SetIdleMotion();
            }
        }
        else
        {
            SetIdleMotion();
        }
    }

    private void SetIdleMotion()
    {
        if(bodySpriteCor != null)
        {
            StopCoroutine(bodySpriteCor);
            bodySpriteCor = null;
        }
        if(faceSpriteCor != null)
        {
            StopCoroutine(faceSpriteCor);
            faceSpriteCor = null;
        }

        leftBodyRender.sprite = leftIdleSprite;
        rightBodyRender.sprite = rightIdleSprite;
        centerBodyRender.sprite = centerIdleSprite;
    }

    private IEnumerator _MoveSprite()
    {
        int index = 0;
        WaitForSeconds wait = new WaitForSeconds(0.5f);
        while (true)
        {
            leftBodyRender.sprite = leftBodySprites[index];
            rightBodyRender.sprite = rightBodySprites[index];
            centerBodyRender.sprite = centerBodySprites[index];
            index++;
            index %= centerBodySprites.Length;
            yield return wait;
        }
    }

    private IEnumerator _FaceSprite()
    {
        int index = 0;
        WaitForSeconds wait = new WaitForSeconds(0.5f);
        while (true)
        {
            leftFaceRender.sprite = leftFaceSprites[index];
            rightFaceRender.sprite = rightFaceSprites[index];
            centerFaceRender.sprite = centerFaceSprites[index];
            index++;
            index %= centerFaceSprites.Length;
            yield return wait;
        }
    }

    public void OnClick()
    {
        AudioManager.Instance.PlaySound("dog_6", transform.position);
    }
}
