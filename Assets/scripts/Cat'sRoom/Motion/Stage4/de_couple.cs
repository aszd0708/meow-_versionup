﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class de_couple : MonoBehaviour, CatHouseCat
{
    private readonly int MAX_LINE_COUNT = 50;

    [SerializeField]
    private LineRenderer[] lines;

    private Vector3[] positions;

    private int positionsPointer = 0;

    private bool touch;
    private bool showItem;

    public bool ShowItem { get => showItem; set => showItem = value; }
    public bool Touch { get => touch; set => touch = value; }

    [Header("터치 속도")]
    public float checkTime = -0.5f;
    private float timeSpan = 0.5f;

    [SerializeField]
    private Stage4AnimalBichornPart[] animal;

    private void Awake()
    {
        SetPositions();
        ResetLine();
    }

    private void OnEnable()
    {
        Invoke("StartEvent", 0.5f);
    }

    private void StartEvent()
    {
        for (int i = 0; i < animal.Length; i++)
        {
            animal[i].StartEvent();
        }
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            timeSpan = Time.time;
        }

        else if (Input.GetMouseButtonUp(0))
        {
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(touchPos, Vector2.zero, 0.0f);

            timeSpan -= Time.time;
            if (timeSpan >= checkTime)
            {
                SetLinePosition(touchPos);
            }
        }
    }

    private void SetLinePosition(Vector2 position)
    {
        if(positionsPointer >= MAX_LINE_COUNT) { return; }
        for(int i = 0; i < lines.Length; i++)
        {
            lines[i].SetPosition(positionsPointer, position);
        }
        positionsPointer++;
    }

    private void SetPositions()
    {
        positions = new Vector3[MAX_LINE_COUNT];

        for (int i = 0; i < MAX_LINE_COUNT; i++)
        {
            positions[i] = lines[0].transform.position;
        }
    }

    public void ResetLine()
    {
        for(int i = 0; i < lines.Length; i++)
        {
            lines[i].SetPositions(positions);
        }
        positionsPointer = 0;
    }

    public void OnClick()
    {
        ResetLine();
    }
}
