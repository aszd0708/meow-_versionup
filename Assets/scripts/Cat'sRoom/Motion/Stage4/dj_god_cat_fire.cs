﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class dj_god_cat_fire : MonoBehaviour, CatHouseCat
{
    [SerializeField]
    private SpriteRenderer fireRender;

    [SerializeField]
    private Sprite[] fireSprites;

    [SerializeField]
    private float changeTime;

    private bool bIfFire = true;

    public bool BIfFire { get => bIfFire; set => bIfFire = value; }

    private bool touch;
    private bool showItem;
    public bool Touch { get => touch; set => touch = value; }
    public bool ShowItem { get => showItem; set => showItem = value; }

    private Coroutine putoffCor;

    private void OnEnable()
    {
        transform.DOScale(1, 2.0f);
        StartCoroutine(_FireEffect());
    }

    public void PutOffFire()
    {
        if(putoffCor == null)
        {
            putoffCor = StartCoroutine(_PutOffFire());
        }
    }

    private IEnumerator _FireEffect()
    {
        WaitForSeconds wait = new WaitForSeconds(changeTime);

        int index = 0;
        while (BIfFire)
        {
            fireRender.sprite = fireSprites[index++];
            index %= fireSprites.Length;
            yield return wait;
        }
        yield break;
    }

    private IEnumerator _PutOffFire()
    {
        transform.DOScale(0, 3.0f);
        yield return new WaitForSeconds(3.0f);
        gameObject.SetActive(false);
        yield break;
    }

    public void OnClick()
    {
        transform.DOKill();
        PutOffFire();
    }
}
