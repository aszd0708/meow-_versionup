﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class dg_fury : MonoBehaviour, CatHouseCat
{
    [SerializeField]
    private SpriteRenderer render;

    [SerializeField]
    private SpriteRenderer eyeRender;

    [SerializeField]
    private Sprite[] motionSprites;

    [SerializeField]
    private Sprite idleSprites;

    [SerializeField]
    private Sprite jumpSprites;

    [SerializeField]
    private float backTime = 10.0f;

    private float currentTime;

    private bool bPlayMotion;

    private bool bHasLeaf;

    private bool touch;
    private bool showItem = false;

    public bool ShowItem { get => showItem; set => showItem = value; }
    public bool Touch { get => touch; set => touch = value; }

    private Coroutine motionCor;


    private void OnEnable()
    {
        bPlayMotion = false;
        currentTime = 0;

        render.sprite = idleSprites;
        eyeRender.enabled = true;
    }

    private void Update()
    {
        if(bHasLeaf == true) { return; }
        currentTime += Time.deltaTime;

        if(currentTime >= backTime)
        {
            if(bPlayMotion == false)
            {
                // 대충 뒤로 버튼 누르는 모션
                StartCoroutine(_Motion());
                bPlayMotion = true;
            }
        }
    }

    private void OnDisable()
    {
        if(motionCor != null)
        {
            StopCoroutine(motionCor);
        }
        currentTime = 0;
    }

    private IEnumerator _Motion()
    {
        transform.rotation = Quaternion.Euler(Vector3.up * 180);
        transform.DOMove(PushBackButton(), 1.0f);
        render.sprite = jumpSprites;
        yield return new WaitForSeconds(1.0f);
        ButtonAction();
        yield break;
    }

    private void ButtonAction()
    {
        Button button = FindObjectOfType<Reset>().GetComponent<Button>();
        button.onClick.Invoke();
    }

    private Vector2 PushBackButton()
    {
        Vector3 pose = FindObjectOfType<FindEXITButton>().GetComponent<RectTransform>().position;
        Vector2 position = Camera.main.ScreenToWorldPoint(pose);

        return position;
    }

    public void OnClick()
    {
        currentTime = backTime;
        AudioManager.Instance.PlaySound("cat_7", transform.position);
    }

    public void GetLeaf()
    {
        if(bHasLeaf == true) { return; }
        eyeRender.enabled = false;
        bHasLeaf = true;
        currentTime = 0;

        motionCor = StartCoroutine(_LeafMotion());
    }

    private IEnumerator _LeafMotion()
    {
        WaitForSeconds wait = new WaitForSeconds(2.0f);
        int index = 0;
        while(true)
        {
            render.sprite = motionSprites[index];
            index++;
            index %= motionSprites.Length;
            yield return wait;
        }
    }
}
