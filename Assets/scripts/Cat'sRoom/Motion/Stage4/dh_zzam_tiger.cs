﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dh_zzam_tiger : MonoBehaviour, CatHouseCat
{
    [SerializeField]
    private GameObject idleObj;
    [SerializeField]
    private GameObject moveObj;

    [SerializeField]
    private SpriteRenderer moveRender;
    [SerializeField]
    private Sprite[] moveSprites;

    [SerializeField]
    private Transform sausageTransform;

    [SerializeField]
    private float targetDistance;

    [SerializeField]
    private float speed = 1.0f;

    private Coroutine moveMotionCor;

    private bool touch;
    private bool showItem = true;

    public bool Touch { get => touch; set => touch = value; }
    public bool ShowItem { get => showItem; set => showItem = value; }

    private void Update()
    {
        float distance = Vector2.Distance(transform.position, sausageTransform.position);

        if(distance <= targetDistance)
        {
            idleObj.SetActive(true);
            moveObj.SetActive(false);

            if(moveMotionCor != null)
            {
                StopCoroutine(moveMotionCor);
                moveMotionCor = null;
            }
        }
        else
        {
            idleObj.SetActive(false);
            moveObj.SetActive(true);

            if(moveMotionCor == null)
            {
                moveMotionCor = StartCoroutine(_MoveMotion());
            }
            Flip(sausageTransform.position);
            Vector2 moveDirection = sausageTransform.position - transform.position;
            moveDirection *= speed * Time.deltaTime;
            transform.position = new Vector2(transform.position.x + moveDirection.x, transform.position.y + moveDirection.y);
        }
    }

    private IEnumerator _MoveMotion()
    {
        int index = 0;
        while(true)
        {
            moveRender.sprite = moveSprites[index];
            index++;
            index %= moveSprites.Length;
            yield return new WaitForSeconds(0.25f);
        }            
    }
    private void Flip(Vector2 pose)
    {
        float x = transform.position.x - pose.x;
        if (x <= 0)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
        else
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
    }

    public void OnClick()
    {
        AudioManager.Instance.PlaySound("fat_cat", transform.position);
    }
}
