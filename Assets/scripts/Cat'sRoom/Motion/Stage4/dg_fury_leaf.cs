﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dg_fury_leaf : MonoBehaviour, CatHouseDragObject
{
    private bool touch;

    public bool Touch { get => touch; set => touch = value; }

    [SerializeField]
    private dg_fury fury;

    [SerializeField]
    private float distance = 0.5f;

    [SerializeField]
    private SpriteRenderer render;
    [SerializeField]
    private Collider2D colliderComponent;

    private void OnEnable()
    {
        render.enabled = true;
        colliderComponent.enabled = true;
    }

    private void Update()
    {
        if(Touch)
        {
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            transform.position = touchPos;
        }

        else
        {
            float currentDistance = Vector2.Distance(transform.position, fury.transform.position);

            if(currentDistance <= distance)
            {
                fury.GetLeaf();
                colliderComponent.enabled = false;
                render.enabled = false;
            }
        }

        if(Input.GetMouseButtonUp(0))
        {
            Touch = false;
        }
    }
}
