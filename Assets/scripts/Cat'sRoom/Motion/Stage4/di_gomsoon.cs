﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class di_gomsoon : MonoBehaviour, CatHouseCat
{
    [SerializeField]
    private Transform[] movePoses;

    [SerializeField]
    private Transform bodyTransform;

    [SerializeField]
    private Sprite[] moveSprites;
    [SerializeField]
    private Sprite idleSprite;
    [SerializeField]
    private SpriteRenderer bodyRender;
    [SerializeField]
    private SpriteRenderer hanesRender;

    [SerializeField]
    private Sprite[] hanesSprites;
    [SerializeField]
    private Sprite idleHanesSprite;

    [SerializeField]
    private float moveSpeed;

    private bool bIsMove;

    private bool touch;
    [SerializeField]
    private bool showItem;

    public bool Touch { get => touch; set => touch = value; }
    public bool ShowItem { get => showItem; set => showItem = value; }

    [SerializeField]
    private di_gomsoon_hanes item;
    [SerializeField]
    private SpriteRenderer itemRender;
    [SerializeField]
    private Transform itemTransform;

    private Coroutine moveCor;
    private Coroutine motionCor;

    private void OnEnable()
    {
        ItemEvent();
    }

    public void OnClick()
    {
        if(bIsMove)
        {
            StopRunning();
            JumpItem();
        }
        AudioManager.Instance.PlaySound("dog_5", transform.position);
    }

    public void ItemEvent()
    {
        StartRunning();
        itemRender.enabled = true;
        itemTransform.position = Vector3.one * 1000;
        bIsMove = true;
    }

    private void StopRunning()
    {
        if (moveCor != null)
        {
            StopCoroutine(moveCor);
            moveCor = null;
        }

        if (motionCor != null)
        {
            StopCoroutine(motionCor);
            motionCor = null;
        }
        bIsMove = false;

        bodyTransform.DOKill();
        bodyRender.sprite = idleSprite;
        hanesRender.sprite = idleHanesSprite;
        hanesRender.enabled = false;
    }

    private void StartRunning()
    {
        hanesRender.enabled = true;
        moveCor = StartCoroutine(_Move());
        bIsMove = true;
    }

    private void JumpItem()
    {
        StartCoroutine(_JumpItem());
    }

    private IEnumerator _JumpItem()
    {
        item.BCantouch = false;
        itemTransform.position = bodyTransform.position;
        itemRender.enabled = true;

        itemTransform.DOJump(new Vector2(bodyTransform.position.x + 1.0f, bodyTransform.position.y), 1, 1,1);
        yield return new WaitForSeconds(1.0f);
        item.BCantouch = true;
        yield break;
    }

    private IEnumerator _Move()
    {
        while(true)
        {
            Vector2 movePose = GetMovePose();
            float distance = Vector2.Distance(movePose, bodyTransform.position);
            float time = distance / moveSpeed;

            Flip(movePose);
            bodyTransform.DOMove(movePose, time).SetEase(Ease.Linear);
            motionCor = StartCoroutine(_Motion());
            Debug.Log(time);
            yield return new WaitForSeconds(time);
            if(motionCor != null)
            {
                StopCoroutine(motionCor);
                motionCor = null;
            }

            bodyRender.sprite = idleSprite;
            hanesRender.sprite = idleHanesSprite;

            yield return new WaitForSeconds(1.0f);
        }
        yield break;
    }

    private IEnumerator _Motion()
    {
        int index = 0;
        WaitForSeconds wait = new WaitForSeconds(0.5f);
        while(true)
        {
            bodyRender.sprite = moveSprites[index];
            hanesRender.sprite = hanesSprites[index];
            index++;
            index %= moveSprites.Length;
            yield return wait;
        }
    }

    private void Flip(Vector2 pose)
    {
        float x = bodyTransform.position.x - pose.x;
        if(x <= 0)
        {
            bodyTransform.rotation = Quaternion.Euler(0, 180, 0); 
        }
        else
        {
            bodyTransform.rotation = Quaternion.Euler(0, 0, 0);
        }
    }

    private Vector2 GetMovePose()
    {
        float randomX = Random.Range(movePoses[0].position.x, movePoses[1].position.x);
        float randomY = Random.Range(movePoses[0].position.y, movePoses[1].position.y);
        return new Vector2(randomX, randomY);
    }
}
