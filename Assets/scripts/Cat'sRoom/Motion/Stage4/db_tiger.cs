﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class db_tiger : MonoBehaviour, CatHouseCat
{
    [SerializeField]
    private GameObject[] walkingMotion;

    [SerializeField]
    private GameObject idleMotion;

    [SerializeField]
    private Transform dduckTransform;

    [SerializeField]
    private Transform handTransform;

    private bool followDduck = true;

    public bool FollowDduck { get => followDduck; set => followDduck = value; }

    private Coroutine moveCor;

    private bool touch;
    private bool showItem = false;

    public bool Touch { get => touch; set => touch = value; }
    public bool ShowItem { get => showItem; set => showItem = value; }

    void Update()
    {
        if(FollowDduck == true)
        {
            if(moveCor == null)
            {
                moveCor = StartCoroutine(_MoveMotion());
            }

            MoveToDduck();
        }

        else
        {
            if(moveCor != null)
            {
                StopCoroutine(moveCor);
                moveCor = null;

                for(int i = 0; i < walkingMotion.Length; i++)
                {
                    walkingMotion[i].SetActive(false);
                }

                idleMotion.SetActive(true);
            }
        }
    }

    public void SetDduckPose(Transform t)
    {
        t.position = handTransform.position;
    }

    private void MoveToDduck()
    {
        Vector2 moveTo = (dduckTransform.position - transform.position);
        transform.Translate(moveTo.normalized * 0.1f);
    }

    private IEnumerator _MoveMotion()
    {
        int index = 0;
        idleMotion.SetActive(false);
        WaitForSeconds wait =  new WaitForSeconds(0.25f);
        while(FollowDduck)
        {
            walkingMotion[index++].SetActive(false);
            index %= walkingMotion.Length;
            walkingMotion[index].SetActive(true);
            yield return wait;
        }
        yield break;
    }

    public void OnClick()
    {
        AudioManager.Instance.PlaySound("cat_9", transform.position);
    }
}
