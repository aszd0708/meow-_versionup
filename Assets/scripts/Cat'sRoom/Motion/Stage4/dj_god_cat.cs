﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dj_god_cat : MonoBehaviour, CatHouseCat
{
    [SerializeField]
    private dj_god_cat_motion catMotion;

    private bool touch;
    private bool showItem = false;

    public bool Touch { get => touch; set => touch = value; }
    public bool ShowItem { get => showItem; set => showItem = value; }

    public void OnClick()
    {
        catMotion.Motion();
    }
}
