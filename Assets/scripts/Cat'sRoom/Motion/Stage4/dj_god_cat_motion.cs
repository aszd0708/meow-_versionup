﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;

public class dj_god_cat_motion : MonoBehaviour
{
    [SerializeField]
    private Transform armTransform;

    [SerializeField]
    private float armAngle;

    [SerializeField]
    private float moveY;

    [SerializeField]
    private UnityEvent armEvent;

    private bool bEnd = true;

    public void Motion()
    {
        if(bEnd == true)
        {
            StartCoroutine(_Motion());
        }
    }

    private IEnumerator _Motion()
    {
        bEnd = false;
        float yPose = transform.localPosition.y;
        transform.DOLocalMoveY(moveY, 1.0f);
        yield return new WaitForSeconds(1.5f);
        armTransform.DORotate(Vector3.forward * armAngle, 0.5f);
        AudioManager.Instance.PlaySound("god", transform.position);
        yield return new WaitForSeconds(0.5f);
        armEvent.Invoke();
        armTransform.DORotate(Vector3.zero, 0.5f);
        yield return new WaitForSeconds(1.5f);
        transform.DOLocalMoveY(yPose, 1.0f);
        bEnd = true;
        yield break;
    }
}
