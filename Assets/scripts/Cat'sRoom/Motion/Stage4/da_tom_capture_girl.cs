﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class da_tom_capture_girl : MonoBehaviour, CatHouseCat
{
    [SerializeField]
    private float changeCoolTime = 1.0f;
    private float currentCoolTime = 0.0f;

    [SerializeField]
    private Stage4ObjectCaptureGirlMotion[] motions;

    int currentMotion = 0;

    private bool touch;
    private bool showItem;

    public bool Touch { get => touch; set => touch = value; }
    public bool ShowItem { get => showItem; set => showItem = value; }

    private void OnEnable()
    {
        currentCoolTime = changeCoolTime;
        for (int i = 0; i < motions.Length; i++)
        {
            motions[i].gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        CheckCoolTime();
    }

    private void PlayMotion()
    {
        motions[currentMotion].gameObject.SetActive(false);
        changeCoolTime = motions[currentMotion].TotalTime;
        currentMotion = Random.Range(0, motions.Length);
        motions[currentMotion].gameObject.SetActive(true);
        currentCoolTime = 0;
    }

    private void CheckCoolTime()
    {
        currentCoolTime += Time.deltaTime;
        if (currentCoolTime >= changeCoolTime)
        {
            PlayMotion();
        }
    }

    public void OnClick()
    {
        PlayMotion();
    }
}
