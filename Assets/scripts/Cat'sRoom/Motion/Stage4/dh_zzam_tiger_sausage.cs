﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dh_zzam_tiger_sausage : MonoBehaviour, CatHouseDragObject
{
    private bool touch;

    public bool Touch { get => touch; set => touch = value; }

    void Update()
    {
        if(Touch)
        {
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = touchPos;
        }

        if(Input.GetMouseButtonUp(0))
        {
            Touch = false;
        }
    }
}
