﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ai_spotted_cat_motion : MonoBehaviour, CatHouseCat
{
    private bool touch;
    public bool Touch { get => touch; set => touch = value; }

    public GameObject laser;
    private Transform laserTransform;
    private Transform laserDot;

    public SpriteRenderer tail;
    public Sprite[] motion = new Sprite[2];

    private SpriteRenderer cat;
    private bool flip;

    [SerializeField]
    private bool showItem = true;
    public bool ShowItem { get => showItem; set => showItem = value; }
    private void Awake()
    {
        cat = GetComponent<SpriteRenderer>();
    }

    public void OnClick()
    {

        AudioManager.Instance.PlaySound("cat_4", transform.position);
        if (Touch) return;

        Touch = true;

        laserTransform = Instantiate(laser).transform;
        laserTransform.position = transform.position;
        laserTransform.DOLocalJump(new Vector3(Random.Range(-2, 2), 0), 1.0f, 1, 1.0f);

        laserDot = laserTransform.GetChild(0);

        laserTransform.GetComponent<ai_spotted_cat_laser>().Cat = this;
    }

    public void SetSetting()
    {
        StartCoroutine(_FollowLaser());
        StartCoroutine(_PlayMotion());
        StartCoroutine(_TailMotion());
    }

    private IEnumerator _FollowLaser()
    {
        WaitForSeconds fixedUpdate = new WaitForSeconds(0.1f);
        Vector3 movePos = Vector3.zero;
        while (true)
        {
            movePos = new Vector3(laserDot.position.x, laserDot.position.y - 2);
            transform.DOMove(movePos, 0.1f);
            yield return fixedUpdate;
        }
    }

    private IEnumerator _PlayMotion()
    {
        Vector3 flipVector = new Vector3(0, 180, 0);
        Vector3 nonFlipVector = new Vector3(0, 0, 0);
        WaitForSeconds waitTime = new WaitForSeconds(0.5f);
        int index = 0;

        while (true)
        {
            for (int j = 0; j < Random.Range(1, 4); j++)
            {
                for (int i = 0; i < 2; i++)
                {
                    cat.sprite = motion[index++];
                    index %= motion.Length;
                    yield return waitTime;
                }
            }

            if (!flip)
                transform.DOLocalRotate(flipVector, 0.5f);
            else
                transform.DOLocalRotate(nonFlipVector, 0.5f);
            yield return waitTime;
            flip = !flip;
        }
    }

    private IEnumerator _TailMotion()
    {
        WaitForSeconds waitTime = new WaitForSeconds(0.5f);
        bool flip = true; 
        while (true)
        {
            tail.flipX = flip;
            yield return waitTime;
            flip = !flip;
        }
    }
}
