﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ak_hat_cat_motion : MonoBehaviour, CatHouseCat
{
    public SpriteRenderer hand;
    public SpriteRenderer head;
    public SpriteRenderer eye;
    public SpriteRenderer[] lightEffect = new SpriteRenderer[2];
    public Sprite[] headSprite = new Sprite[2];
    public Sprite[] effect = new Sprite[4];

    private bool touch;
    public bool Touch { get => touch; set => touch = value; }

    public enum State
    {
        IN, OUT
    }

    public State STATE;

    private Coroutine eyeEffectCor;
    private Coroutine headMotionCor;

    [SerializeField]
    private bool showItem = true;
    public bool ShowItem { get => showItem; set => showItem = value; }

    void Start()
    {
        hand.enabled = false;
        head.enabled = false;
        eye.enabled = false;
        lightEffect[0].enabled = false;
        lightEffect[1].enabled = false;
    }

    public void OnClick()
    {
        if (Touch)
            return;
        switch(STATE)
        {
            case State.IN:
                GoOutside();
                STATE = State.OUT;
                break;
            case State.OUT:
                GoInside();
                STATE = State.IN;
                break;
        }
        AudioManager.Instance.PlaySound("cat_5", transform.position);
    }

    private void GoInside()
    {
        StartCoroutine(_GoInside());
    }

    private IEnumerator _GoInside()
    {
        Touch = true;
        WaitForSeconds onePointThree = new WaitForSeconds(0.3f);
        yield return onePointThree;
        head.enabled = false;
        eye.enabled = false;
        lightEffect[0].enabled = false;
        lightEffect[1].enabled = false;
        yield return onePointThree;
        hand.enabled = false;

        StopCoroutine(eyeEffectCor);
        StopCoroutine(headMotionCor);
        Touch = false;
        yield break;
    }

    public void GoOutside()
    {
        eyeEffectCor = StartCoroutine(EffectMotion());
        headMotionCor = StartCoroutine(HeadMotion());
        StartCoroutine(Motion());
    }

    private IEnumerator EffectMotion()
    {
        int random;
        WaitForSeconds waitSecond = new WaitForSeconds(0.5f);
        while (true)
        {
            random = Random.Range(0, 4);
            lightEffect[0].sprite = effect[random];
            lightEffect[1].sprite = effect[random];
            yield return waitSecond;
        }
    }

    private IEnumerator HeadMotion()
    {
        WaitForSeconds randomTime = new WaitForSeconds(1.0f);
        while (true)
        {
            head.sprite = headSprite[0];
            yield return randomTime;
            head.sprite = headSprite[1];
            yield return randomTime;
        }
    }

    private IEnumerator Motion()
    {
        Touch = true;
        WaitForSeconds onePointThree = new WaitForSeconds(0.3f);
        yield return onePointThree;
        hand.enabled = true;
        yield return onePointThree;
        head.enabled = true;
        eye.enabled = true;
        lightEffect[0].enabled = true;
        lightEffect[1].enabled = true;
        Touch = false;
        yield break;
    }
}
