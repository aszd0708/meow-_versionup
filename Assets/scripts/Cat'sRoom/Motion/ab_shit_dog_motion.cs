﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ab_shit_dog_motion : MonoBehaviour, CatHouseCat
{
    public GameObject shitObj;

    public SpriteRenderer leftEyeRenderer;
    public SpriteRenderer rightEyeRenderer;

    public Sprite[] leftEye = new Sprite[2];
    public Sprite[] rightEye = new Sprite[2];

    public Transform shitBox;

    public float shakeAmount_X = 1;
    public float shakeAmount_Z = 20;
    public float shakeDuration = 1;

    private Coroutine motionCor;

    private bool touch;

    private int shitCount = 0;

    public bool Touch { get => touch; set => touch = value; }
    [SerializeField]
    private bool showItem = true;
    public bool ShowItem { get => showItem; set => showItem = value; }

    public void OnClick()
    {
        if(!Touch)
            StartCoroutine(Click());
    }

    private IEnumerator Click()
    {
        Touch = true;
        AudioManager.Instance.PlaySound("dog_2", transform.position);
        yield return StartCoroutine(Motion());
        Touch = false;
    }

    private IEnumerator Motion()
    {
        iTween.ShakePosition(gameObject, iTween.Hash(
                        "x", shakeAmount_X,
                        "y", 0f,
                        "delay", 0f,
                        "time", shakeDuration));
        iTween.ShakeRotation(gameObject, iTween.Hash(
            "x", 0,
            "z", shakeAmount_Z,
            "delay", 0f,
            "time", shakeDuration));
        leftEyeRenderer.sprite = leftEye[1];
        rightEyeRenderer.sprite = rightEye[1];
        yield return new WaitForSeconds(shakeDuration);
        StartCoroutine(Shit());
        touch = false;
    }

    private IEnumerator Shit()
    {
        GameObject shit;
        Vector3 createPos = new Vector3(gameObject.transform.position.x + 1f, gameObject.transform.position.y, 0.0f);

        shit = PoolingManager.Instance.GetPool("Shit");
        if (shit == null)
            shit = Instantiate(shitObj);
        shit.transform.position = createPos;
        shit.transform.rotation = Quaternion.identity;

        if (shitBox != null)
            shit.transform.parent = shitBox.transform;
        else
            shit = gameObject.transform.transform.GetChild(shitCount).gameObject;
        shitCount++;

        if (shitCount >= 5)
        {
            if (shitBox != null)
                PoolingManager.Instance.SetPool(shitBox.GetChild(2).gameObject, "Shit");
            shitCount--;
        }
        leftEyeRenderer.sprite = leftEye[0];
        rightEyeRenderer.sprite = rightEye[0];
        touch = true;
        yield break;
    }
}
