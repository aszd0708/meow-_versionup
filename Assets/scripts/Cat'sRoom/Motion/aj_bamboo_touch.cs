﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class aj_bamboo_touch : MonoBehaviour, CatHouseCat
{
    private bool touch;
    public bool Touch { get => touch; set => touch = value; }

    public GameObject bamboo;
    private GameObject createBamboo;
    public GameObject CreateBamboo { get => createBamboo; set => createBamboo = value; }

    private Transform panda;
    public Transform Panda { get => panda; set => panda = value; }

    private Button resetBtn;

    private Vector3 movePos;

    [SerializeField]
    private bool showItem = true;
    public bool ShowItem { get => showItem; set => showItem = value; }

    private void Start()
    {
        resetBtn = FindObjectOfType<Reset>().GetComponent<Button>();
        SetBtn();
        movePos = new Vector3(transform.position.x + 3, transform.position.y, 0);
    }
    public void SetBtn()
    {
        resetBtn.onClick.AddListener(Del);
    }
    private void Del()
    {
        StartCoroutine(_Del());
    }

    private IEnumerator _Del()
    {
        transform.DOScale(new Vector3(0, 0, 0), 0.5f);
        yield return new WaitForSeconds(0.5f);
        resetBtn.onClick.RemoveListener(Del);
        gameObject.SetActive(false);
    }

    public void OnClick()
    {
        if (Touch)
            return;
        CreateBambooObj();
    }

    private void CreateBambooObj()
    {
        CreateBamboo = PoolingManager.Instance.GetPool("Bamboo");
        if (CreateBamboo == null)
            CreateBamboo = Instantiate(bamboo);

        CreateBamboo.transform.position = transform.position;
        CreateBamboo.transform.DOJump(movePos, 0.5f, 2, 1.0f);
        CreateBamboo.GetComponent<aj_bamboo_drag>().SetComponenet(Panda, this);
        Touch = true;
    }

    public void SetPanda(Transform _panda)
    {
        Panda = _panda;
    }
}
