﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpottedCat : MonoBehaviour
{
    public SpriteRenderer tail;
    public Sprite[] motion = new Sprite[2];

    void Start()
    {
        MotionStarter();
    }

    public void MotionStarter()
    {
        StartCoroutine("Motion");
    }

    private IEnumerator TailMotion()
    {
        while (true)
        {
            tail.flipX = true;
            yield return new WaitForSeconds(0.5f);
            tail.flipX = false;
            yield return new WaitForSeconds(0.5f);
        }
    }

    private IEnumerator Motion()
    {
        bool flip = false;
        SpriteRenderer cat = GetComponent<SpriteRenderer>();
        while (true)
        {
            for (int j = 0; j < Random.Range(1, 4); j++)
            {
                for (int i = 0; i < 2; i++)
                {
                    //transform.position
                    cat.sprite = motion[0];
                    yield return new WaitForSeconds(0.5f);

                    cat.sprite = motion[1];
                    yield return new WaitForSeconds(0.5f);
                }
            }

            if (!flip)
                transform.DOLocalRotate(new Vector3(0, 180, 0), 0.5f);
            else
                transform.DOLocalRotate(new Vector3(0, 0, 0), 0.5f);
            yield return new WaitForSeconds(0.5f);
            flip = !flip;
        }
    }
}
