﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ag_Box_cat_motion : MonoBehaviour, CatHouseCat
{
    private bool touch;

    public bool Touch { get => touch; set => touch = value; }

    public SpriteRenderer close;
    public SpriteRenderer[] lid = new SpriteRenderer[2];

    [Header("전체 ")]
    public GameObject cats;

    [Header("고양이들 스프라이트")]
    public Sprite[] leftCatSprites = new Sprite[2];
    public Sprite[] rightCatSprites = new Sprite[2];
    public Sprite[] centerCatSprites = new Sprite[2];
     
    [Header("고양이들 스프라이트 랜더러")]
    public SpriteRenderer leftCatBody, rightCatBody, centerCatBody;

    [Header("고양이")]
    public Transform leftCat, rightCat, centerCat;

    [Header("떠는데 필요한 변수들")]
    public float vibeTime;
    public float vibePower;
    public int vibrator;

    private WaitForSeconds vibeWait;

    [SerializeField]
    private bool showItem = true;
    public bool ShowItem { get => showItem; set => showItem = value; }
    public enum State
    {
        OPEN, CLOSED
    }

    public State STATE = State.CLOSED;
    // Start is called before the first frame update
    void Start()
    {
        vibeWait = new WaitForSeconds(vibeTime);
        close.enabled = true;
        lid[0].enabled = false;
        lid[1].enabled = false;
        cats.SetActive(false);
        if (STATE == State.OPEN)
            OnClick();

        StartCoroutine(BodyCange(rightCatBody, rightCatSprites));
        StartCoroutine(BodyCange(leftCatBody, leftCatSprites));
        StartCoroutine(BodyCange(centerCatBody, centerCatSprites));
    }

    public void OnClick()
    {
        switch (STATE)
        {
            case State.CLOSED:
                close.enabled = false;
                lid[0].enabled = true;
                lid[1].enabled = true;
                cats.SetActive(true);
                STATE = State.OPEN;
                break;

            case State.OPEN:
                if (Touch) return;
                StartCoroutine(VibeCat(leftCat));
                StartCoroutine(VibeCat(rightCat));
                StartCoroutine(VibeCat(centerCat));
                break;
        }
        AudioManager.Instance.PlaySound("baby_cat", transform.position);
    }

    private IEnumerator VibeCat(Transform cat)
    {
        Touch = true;
        cat.DOShakePosition(vibeTime, vibePower, vibrator);
        yield return vibeWait;
        Touch = false;
        yield break;
    }

    private IEnumerator BodyCange(SpriteRenderer renderer, Sprite[] sprite)
    {
        WaitForSeconds waitTime = new WaitForSeconds(Random.Range(3f, 5f));
        int index = 0;
        while (true)
        {
            renderer.sprite = sprite[index++];
            index %= sprite.Length;
            yield return waitTime;
        }
    }
}
