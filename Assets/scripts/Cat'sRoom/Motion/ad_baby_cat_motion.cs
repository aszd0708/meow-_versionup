﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ad_baby_cat_motion : MonoBehaviour, CatHouseCat
{
    public float shakeAmount_X;
    public float shakeAmount_Z;

    public float stopTime;
    public float vibeTime;

    [Header("차래대로 몸 눈 스프라이트 랜더러")]
    public SpriteRenderer bodySpriteRenderer;
    public SpriteRenderer eyeSpriteRenderer;

    [Header("몸 눈 스프라이트")]
    public Sprite[] bodySprite = new Sprite[2];
    public Sprite[] eyeSprite = new Sprite[2];
    [Header("떨리는 이펙트 스프라이트랜더러")]
    public SpriteRenderer vibeEffect;

    private bool touch;

    public bool Touch { get => touch; set => touch = value; }

    private Coroutine motionCor;

    // Start is called before the first frame update
    [SerializeField]
    private bool showItem = true;
    public bool ShowItem { get => showItem; set => showItem = value; }

    private void Start()
    {
        motionCor = StartCoroutine(Moving());
    }

    private void VibeBody()
    {
        iTween.ShakePosition(gameObject, iTween.Hash(
                    "x", shakeAmount_X,
                    "time", vibeTime));

        iTween.ShakeRotation(gameObject, iTween.Hash(
                "z", shakeAmount_Z,
                "time", vibeTime));
    }

    public void OnClick()
    {
        if(!Touch)
            StartCoroutine(Motion());

    }

    private IEnumerator Moving()
    {
        vibeEffect.enabled = false;
        int index = 0;
        while (true)
        {
            eyeSpriteRenderer.sprite = eyeSprite[index];
            bodySpriteRenderer.sprite = bodySprite[index];
            index++;
            index %= eyeSprite.Length;
            yield return new WaitForSeconds(Random.Range(1.0f, 1.5f));
        }
    }

    private IEnumerator Motion()
    {
        Touch = true;
        vibeEffect.enabled = true;
        StopCoroutine(motionCor);
        eyeSpriteRenderer.sprite = eyeSprite[1];
        bodySpriteRenderer.sprite = bodySprite[1];
        VibeBody();
        AudioManager.Instance.PlaySound("baby_cat", transform.position);
        yield return new WaitForSeconds(vibeTime);
        motionCor = StartCoroutine(Moving());
        Touch = false;
    }
}
