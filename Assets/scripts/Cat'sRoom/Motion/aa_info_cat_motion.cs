﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aa_info_cat_motion : MonoBehaviour, CatHouseCat
{
    [Header("감은 눈")]
    public Sprite closedEye;
    public Transform tail;
    [Header("눈 스프라이트 랜더러")]
    public SpriteRenderer eyeSpriteRenderer;

    [Header("고양이 꼬리 움직이는 시간")]
    public float catTailMoveTime;

    private Sprite opendEye;
    
    private Coroutine tailMotion;

    private bool touch;
    public bool Touch { get => touch; set => touch = value; }

    [SerializeField]
    private bool showItem = true;
    public bool ShowItem { get => showItem; set => showItem = value; }

    private void Awake()
    {
        opendEye = eyeSpriteRenderer.sprite;
    }


    private void Start()
    {
        Touch = false;
        tailMotion = StartCoroutine(TailMotion());
    }

    public void OnClick()
    {
        if (!Touch)
            StartCoroutine(Click());


        AudioManager.Instance.PlaySound("cat_5", transform.position);
    }

    private IEnumerator Click()
    {
        Touch = true;
        StopTailMotion();
        yield return StartCoroutine(ShakeMove(transform));
        tailMotion = StartCoroutine(TailMotion());
        Touch = false;
    }

    private IEnumerator ShakeMove(Transform obj)
    {
        obj.DOShakeRotation(0.7f, 5, 5, 45);
        obj.DOShakePosition(0.7f, 0.1f, 10, 30);
        yield return new WaitForSeconds(0.7f);
        yield return new WaitForSeconds(0.25f);
    }

    private IEnumerator TailMotion()
    {
        float rotation = 20;
        while (true)
        {
            tail.DORotate(new Vector3(0, 0, rotation), catTailMoveTime).SetEase(Ease.Linear);
            yield return new WaitForSeconds(catTailMoveTime);
            if (rotation == 20) rotation = -10;
            else rotation = 20;
        }
    }

    private void StopTailMotion()
    {
        StopCoroutine(tailMotion);
        tail.DOPause();
    }
}
