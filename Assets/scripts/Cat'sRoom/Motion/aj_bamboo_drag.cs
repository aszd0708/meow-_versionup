﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class aj_bamboo_drag : MonoBehaviour, CatHouseDragObject
{
    private bool touch = false;
    public bool Touch { get => touch; set => touch = value; }

    private Vector3 touchPos;

    private Transform panda;
    public Transform Panda { get => panda; set => panda = value; }

    private aj_panda_cat_motion pandaScript;
    public aj_panda_cat_motion PandaScript { get => pandaScript; set => pandaScript = value; }

    private aj_bamboo_touch bambooDas;
    public aj_bamboo_touch BambooDas { get => bambooDas; set => bambooDas = value; }

    public float dis;
    private Button resetBtn;

    [SerializeField]
    private bool showItem = true;
    public bool ShowItem { get => showItem; set => showItem = value; }
    private void Start()
    {
        resetBtn = FindObjectOfType<Reset>().GetComponent<Button>();
        SetBtn();
    }
    public void SetBtn()
    {
        resetBtn.onClick.AddListener(Del);
    }
    private void Del()
    {
        StartCoroutine(_Del());
    }

    private IEnumerator _Del()
    {
        transform.DOScale(new Vector3(0, 0, 0), 0.5f);
        yield return new WaitForSeconds(0.5f);
        resetBtn.onClick.RemoveListener(Del);
        gameObject.SetActive(false);
    }

    private void Update()
    {
        DoDrag();
    }

    private void DoDrag()
    {
        if (Touch)
        {
            touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (Input.GetMouseButton(0))
            {
                transform.position = new Vector3(touchPos.x, touchPos.y, transform.position.z);
            }

            else if (Input.GetMouseButtonUp(0))
            {
                Touch = false;
                float distance = Vector3.Distance(transform.position, Panda.position);
                if (distance <= dis)
                    // 대충 팬더 상호작용 한다는 뜻
                    PandaScript.GiveBamboo();
                BambooDas.Touch = false;
                resetBtn.onClick.RemoveListener(Del);
                PoolingManager.Instance.SetPool(gameObject, "Bamboo");
            }
        }
    }

    public void SetComponenet(Transform _panda, aj_bamboo_touch _bambooDas)
    {
        Panda = _panda;
        PandaScript = _panda.GetComponent<aj_panda_cat_motion>();
        BambooDas = _bambooDas;
    }
}
