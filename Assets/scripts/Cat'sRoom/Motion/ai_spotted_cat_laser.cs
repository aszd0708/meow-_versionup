﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ai_spotted_cat_laser : MonoBehaviour, CatHouseDragObject
{
    private bool touch;
    public bool Touch { get => touch; set => touch = value; }
    public CatHouseTouch catHouseTouch;

    private ai_spotted_cat_motion cat;

    private Vector3 touchPos;

    public ai_spotted_cat_motion Cat { get => cat; set => cat = value; }

    private bool firstTouch;
    public bool FirstTouch { get => firstTouch; set => firstTouch = value; }

    private SpriteRenderer dot;

    private Button resetBtn;

    [SerializeField]
    private bool showItem = true;
    public bool ShowItem { get => showItem; set => showItem = value; }
    private void Awake()
    {
        catHouseTouch = FindObjectOfType<CatHouseTouch>();
        FirstTouch = true;
        dot = transform.GetChild(0).GetComponent<SpriteRenderer>();

        resetBtn = FindObjectOfType<Reset>().GetComponent<Button>();
        SetBtn();
    }

    public void SetBtn()
    {
        resetBtn.onClick.AddListener(Del);
    }

    void Update()
    {
        Playing();
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -13, 13), Mathf.Clamp(transform.position.y, -6, 6), transform.position.z);
    }

    private void Playing()
    {
        if (Touch)
        {
            if(FirstTouch)
            {
                dot.enabled = true;
                Cat.SetSetting();

                FirstTouch = false;
            }

            else
            {
                touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                if (Input.GetMouseButton(0))
                {
                    transform.position = new Vector3(touchPos.x, touchPos.y, transform.position.z);
                }

                else if (Input.GetMouseButtonUp(0))
                {
                    Touch = false;
                    catHouseTouch.GrapTheItem = false;
                }
            }
        }
    }

    private void Del()
    {
        StartCoroutine(DelLaser());
    }

    private IEnumerator DelLaser()
    {
        transform.DOScale(new Vector3(0, 0, 0), 0.5f);
        yield return new WaitForSeconds(0.5f);
        resetBtn.onClick.RemoveListener(Del);
        gameObject.SetActive(false);
    }
}
