﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ac_cheeze_cat_motion : MonoBehaviour, CatHouseCat
{
    public Sprite[] move = new Sprite[2];
    public Sprite catchMouse;
    public Sprite[] eye = new Sprite[2];
    public GameObject collectEffect;

    public bool huntingMouse;

    public float speed;
    public Vector2 eyePos;
    public SoundManager soundManager;
    private Vector3 originPos;
    private bool touch;

    private CatCheck check;
    public bool Touch { get => touch; set => touch = value; }
    [SerializeField]
    private bool showItem = true;
    public bool ShowItem { get => showItem; set => showItem = value; }

    void Awake()
    {
        check = GetComponent<CatCheck>();
    }

    public void OnClick()
    {
        if(!Touch)
            StartCoroutine(CatchMotion());

    }
    private IEnumerator CatchMotion()
    {
        Touch = true;
        transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = catchMouse;
        transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = eye[1];
        originPos = transform.GetChild(1).transform.position;
        transform.GetChild(1).transform.localPosition = new Vector3(eyePos.x, eyePos.y, 0);
        AudioManager.Instance.PlaySound("cat_11", transform.position);
        yield return new WaitForSeconds(1.0f);
        GetComponent<Animator>().enabled = true;
        transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = move[0];
        transform.GetChild(1).transform.position = new Vector3(originPos.x, originPos.y, originPos.z);
        huntingMouse = true;
        Touch = false;
    }
}
