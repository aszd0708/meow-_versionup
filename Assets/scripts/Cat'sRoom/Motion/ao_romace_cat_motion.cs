﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ao_romace_cat_motion : MonoBehaviour, CatHouseCat
{
    private bool touch;
    public bool Touch { get => touch; set => touch = value; }

    private bool haveSpider;
    private GameObject spiderObj;
    public GameObject SpiderObj { get => spiderObj; set => spiderObj = value; }

    public GameObject spiderWep;
    public Vector3 createPos;
    

    public Transform head;
    public Transform tail;
    public SpriteRenderer walkBody;
    public SpriteRenderer sitBody;
    public Transform leftArm;
    public Transform rightArm;
    public Transform spider;
    private Vector3 spiderLeftPos;
    private Vector3 spiderRightPos;

    private SpriteRenderer spiderSprite;

    private Coroutine motionCor;
    [SerializeField]
    private bool showItem = true;
    public bool ShowItem { get => showItem; set => showItem = value; }

    void Awake()
    {
        spiderSprite = spider.GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        GameObject wep = Instantiate(spiderWep);
        wep.transform.position = createPos;

        SetPosition();
        spiderSprite.enabled = false;

        head.GetComponent<SpriteRenderer>().enabled = true;
        walkBody.enabled = false;
        sitBody.enabled = true;
        tail.GetComponent<SpriteRenderer>().enabled = true;
        tail.localPosition = new Vector3(0.515f, 0.422f, 0);

        spiderLeftPos = new Vector3(-0.198f, -0.681f, 0);
        spiderRightPos = new Vector3(0.199f, -0.704f, 0);

    }

    void SetPosition()
    {
        head.localPosition = new Vector3(-0.016f, 0.629f, 0);
        tail.localPosition = new Vector3(1.41f, 0.53f, 0);
    }

    public void MotionStarter()
    {
        motionCor = StartCoroutine(PlayingMotion2());
    }

    private IEnumerator PlayingMotion2()
    {
        WaitForSeconds two = new WaitForSeconds(0.2f);
        WaitForSeconds three = new WaitForSeconds(0.3f);
        WaitForSeconds five = new WaitForSeconds(0.5f);
        haveSpider = true;
        head.localPosition = new Vector3(-0.017f, 0.72f, 0);
        tail.localPosition = new Vector3(0.515f, 0.422f, 0);

        spider.SetParent(leftArm);
        spider.localPosition = spiderLeftPos;

        walkBody.enabled = false;
        sitBody.enabled = true;
        leftArm.GetComponent<SpriteRenderer>().enabled = true;
        rightArm.GetComponent<SpriteRenderer>().enabled = true;
        spiderSprite.enabled = true;
        transform.rotation = Quaternion.Euler(0, 0, 0);

        while (true)
        {
            leftArm.DORotate(new Vector3(0, 0, -20), 0.3f);
            yield return three;
            leftArm.DORotate(new Vector3(0, 0, 10), 0.3f);
            yield return two;

            leftArm.DORotate(new Vector3(0, 0, 0), 0.1f);
            spider.SetParent(rightArm);
            spider.DOLocalMove(spiderRightPos, 0.5f);
            spider.DOLocalRotate(new Vector3(0, 0, 360f), 0.5f, RotateMode.FastBeyond360).SetEase(Ease.OutSine);
            yield return five;

            //yield return new WaitForSeconds(1.0f);

            rightArm.DORotate(new Vector3(0, 0, 20), 0.3f);
            yield return three;
            rightArm.DORotate(new Vector3(0, 0, -10), 0.3f);
            yield return two;

            rightArm.DORotate(new Vector3(0, 0, 0), 0.1f);
            spider.SetParent(leftArm);
            spider.DOLocalMove(spiderLeftPos, 0.5f);
            spider.DOLocalRotate(new Vector3(0, 0, 0f), 0.5f, RotateMode.FastBeyond360).SetEase(Ease.OutSine);
            yield return five;
        }
    }

    private IEnumerator StopMotion()
    {
        haveSpider = false;
        StopCoroutine(motionCor);
        leftArm.DOPause();
        rightArm.DOPause();
        spider.DOPause();
        spiderSprite.enabled = false;
        Touch = false;
        SpiderObj.SetActive(true);
        SpiderObj.GetComponent<SpriteRenderer>().enabled = true;
        SpiderObj.GetComponent<ao_roamce_cat_spider>().enabled = true;
        SpiderObj.GetComponent<ao_roamce_cat_spider>().GoHome();
        yield break;
    }

    public void GetSpider(GameObject _spiderObj)
    {
        Touch = true;
        haveSpider = true;
        SpiderObj = _spiderObj;
        SpiderObj.GetComponent<SpriteRenderer>().enabled = false;
        SpiderObj.GetComponent<ao_roamce_cat_spider>().enabled = false;
        MotionStarter();
    }

    public void OnClick()
    {
        if(haveSpider)
            StartCoroutine(StopMotion());

        AudioManager.Instance.PlaySound("cat_4", transform.position);
    }
}
