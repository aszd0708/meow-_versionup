﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ap_square_cat_UFO : MonoBehaviour
{
    public Transform beemMask;
    public Transform[] catilien = new Transform[2];
    public Transform boxCat;
    public Transform catFOBody;
    public Transform destination;
    public SpriteRenderer beem;
    public Vector3[] movePos = new Vector3[2];
    public float moveTime;
    public Transform item;
    private bool end;

    private Coroutine motionCor;
    // 고양이 Y + 8

    public Vector3 firstStopPos;

    private void Start()
    {
        end = false;
        beem.enabled = false;
    }

    public void OnClick()
    {
        if (end)
            StartCoroutine(GoHome());
    }

    public void StartCor()
    {
        StartCoroutine(UFOCome());
        StartCoroutine(SetBtn());
    }

    private IEnumerator SetBtn()
    {
        Button a = FindObjectOfType<Reset>().GetComponent<Button>();
        a.enabled = false;
        yield return new WaitForSeconds(moveTime * 2 + 8);
        a.enabled = true;
        yield break;
    }

    private IEnumerator OutDoor()
    {
        //motionCor = StartCoroutine(Motion());
        motionCor = StartCoroutine(Motion());
        yield return new WaitForSeconds(0.75f);
        boxCat.GetComponent<ap_square_cat_motion>().StartMotion();
        beem.enabled = true;
        beemMask.GetComponent<SpriteMask>().enabled = true;
        boxCat.position = destination.position;
        boxCat.localScale = new Vector3(0, 0, 0);
        beemMask.DOLocalMoveY(-1.45f, 1.5f);
        yield return new WaitForSeconds(1.5f);
        boxCat.DOLocalRotate(new Vector3(0, 0, 1800f), 3.0f, RotateMode.FastBeyond360).SetEase(Ease.Linear);
        boxCat.DOScale(new Vector3(2.0f, 2.0f, 0), 3.0f).SetEase(Ease.Linear);
        boxCat.DOMove(new Vector3(0, -2.8f, 0), 3.0f).SetEase(Ease.Linear);
        yield return new WaitForSeconds(3.0f);
        boxCat.GetComponent<ap_square_cat_motion>().SendMessage("StartMotion", SendMessageOptions.DontRequireReceiver);
        end = true;
        StartCoroutine(GoHome());
        yield break;
    }

    private IEnumerator GoHome()
    {
        end = false;
        StopCoroutine(motionCor);
        catilien[0].DOLocalMove(new Vector3(0, 0, 0), 0.75f);
        catilien[1].DOLocalMove(new Vector3(0, 0, 0), 0.75f);
        yield return new WaitForSeconds(0.75f);
        beemMask.DOLocalMoveY(1.7f, 1.5f);
        yield return new WaitForSeconds(1.5f);
        AudioManager.Instance.PlaySound("ufo_1", transform.position);
        beem.enabled = false;
        beemMask.GetComponent<SpriteMask>().enabled = false;
        transform.DOScale(new Vector3(10, 10, 0), moveTime);
        transform.DOMove(movePos[1], moveTime / 2).SetEase(Ease.OutCubic);
        yield return new WaitForSeconds(moveTime / 2);
        transform.DOMove(movePos[0], moveTime / 2).SetEase(Ease.InCubic);
        yield return new WaitForSeconds(moveTime / 2);
        yield break;
    }

    private IEnumerator Motion()
    {
        catilien[0].DOLocalMoveX(-1.2f, 0.75f);
        catilien[0].DOLocalMoveY(0.1f, 0.75f).SetEase(Ease.InOutBounce);
        catilien[1].DOLocalMoveX(1.2f, 0.75f);
        catilien[1].DOLocalMoveY(-0.1f, 0.75f).SetEase(Ease.InOutBounce);
        yield return new WaitForSeconds(0.75f);
        // 둥실둥실
        while (true)
        {
            float num = 0.15f;
            for (int i = 0; i < 2; i++)
            {
                catilien[i].DOLocalMoveY(catilien[i].localPosition.y - num, 1.5f);
                num *= -1;
            }
            yield return new WaitForSeconds(1.5f);
            for (int i = 0; i < 2; i++)
            {
                catilien[i].DOLocalMoveY(catilien[i].localPosition.y + num, 1.5f);
                num *= -1;
            }
            yield return new WaitForSeconds(1.5f);
        }
    }

    private IEnumerator UFOCome()
    {
        yield return new WaitForSeconds(0.5f);
        AudioManager.Instance.PlaySound("ufo_1", transform.position);
        transform.localScale = new Vector3(10, 10, 0);
        transform.position = movePos[0];
        transform.DOScale(new Vector3(3, 3, 0), moveTime);
        transform.DOMove(movePos[1], moveTime / 2).SetEase(Ease.OutCubic);
        yield return new WaitForSeconds(moveTime / 2);
        transform.DOMove(firstStopPos, moveTime / 2).SetEase(Ease.InCubic);
        yield return new WaitForSeconds(moveTime / 2);
        StartCoroutine(OutDoor());
        yield break;
    }

    // 여기서부터 집으로 돌아가는 함수

    public void StartHome()
    {
        StartCoroutine(Reset());
    }
    private IEnumerator Reset()
    {
        yield return StartCoroutine(GoCatPos());
        yield return StartCoroutine(GoHome());
        gameObject.SetActive(false);
        yield break;
    }

    private IEnumerator GoCatPos()
    {
        transform.localScale = new Vector3(10, 10, 0);
        transform.position = movePos[0];
        transform.DOScale(new Vector3(3, 3, 0), moveTime);
        transform.DOMove(movePos[1], moveTime / 2).SetEase(Ease.OutCubic);
        yield return new WaitForSeconds(moveTime / 2);
        transform.DOMove(firstStopPos, moveTime / 2).SetEase(Ease.InCubic);
        yield return new WaitForSeconds(moveTime / 2);

        ap_square_cat_motion cat = boxCat.GetComponent<ap_square_cat_motion>();
        motionCor = StartCoroutine(Motion());
        yield return new WaitForSeconds(0.75f);
        cat.StartMotion();
        beem.enabled = true;
        beemMask.GetComponent<SpriteMask>().enabled = true;
        beemMask.DOLocalMoveY(-1.45f, 1.5f);
        yield return new WaitForSeconds(1.5f);
        for(int i = 0; i < cat.smallCats.childCount; i++)
        {
            cat.smallCats.GetChild(i).transform.DOLocalRotate(new Vector3(0, 0, 1800f), 3.0f, RotateMode.FastBeyond360).SetEase(Ease.Linear);
            cat.smallCats.GetChild(i).transform.DOMove(destination.position, 1.0f);
        }
        yield return new WaitForSeconds(1.0f);
        cat.SetNull();
        boxCat.transform.DOLocalRotate(new Vector3(0, 0, 1800f), 3.0f, RotateMode.FastBeyond360).SetEase(Ease.Linear);
        boxCat.transform.DOScale(Vector3.zero, 1.5f);
        boxCat.transform.DOMove(destination.position, 1.5f);
        yield return new WaitForSeconds(1.5f);
        yield break;
    }
}
