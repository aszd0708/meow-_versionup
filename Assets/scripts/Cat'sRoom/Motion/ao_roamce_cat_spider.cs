﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ao_roamce_cat_spider : MonoBehaviour, CatHouseDragObject
{
    private bool touch;
    public bool Touch { get => touch; set => touch = value; }
    private Transform cat;
    public Transform Cat { get => cat; set => cat = value; }

    [Header("돌아가는 시간")]
    public float homeTime;
    [Header("원래 자리")]
    private Vector3 originPos;
    public float distance;

    private ao_romace_cat_motion catMotion;

    [Header("지금 터치하고 있는 곳")]
    private Vector2 touchPos;

    private Button resetBtn;
    [SerializeField]
    private bool showItem = true;
    public bool ShowItem { get => showItem; set => showItem = value; }
    private void Awake()
    {
        catMotion = FindObjectOfType<ao_romace_cat_motion>();
        cat = catMotion.transform;
    }

    private void Start()
    {
        resetBtn = FindObjectOfType<Reset>().GetComponent<Button>();
        originPos = transform.position;
        SetBtn();
    }

    private void Update()
    {
        if (Touch)
            GrapObj();
    }

    private void SetRandomRotation()
    {
        transform.rotation = Quaternion.Euler(0, 0, Random.Range(0f, 360f));
    }

    private void GrapObj()
    {
        if(Input.GetMouseButton(0))
        {
            touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = touchPos;
        }

        else if(Input.GetMouseButtonUp(0))
        {
            if(Vector3.Distance(transform.position, cat.position) < distance)
            {
                // 대충 고양이 상호작용 하라는 뜻
                catMotion.GetSpider(gameObject);
            }

            else
            {
                // 대충 원래 자리로 돌아가는 명령어
                transform.DOMove(originPos, Vector3.Distance(transform.position, originPos) / homeTime);
                SetRandomRotation();
            }

            Touch = false;
        }
    }

    public void GoHome()
    {
        transform.DOMove(originPos, Vector3.Distance(transform.position, originPos) / homeTime);
        SetRandomRotation();
    }


    public void SetBtn()
    {
        resetBtn.onClick.AddListener(Del);
    }
    private void Del()
    {
        StartCoroutine(_Del());
    }

    private IEnumerator _Del()
    {
        transform.parent.DOScale(new Vector3(0, 0, 0), 0.5f);
        yield return new WaitForSeconds(0.5f);
        resetBtn.onClick.RemoveListener(Del);
        transform.parent.gameObject.SetActive(false);
    }
}
