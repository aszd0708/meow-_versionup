﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ah_swag_cat_motion : MonoBehaviour, CatHouseCat
{
    private bool touch;

    public bool Touch { get => touch; set => touch = value; }

    public Transform item;

    [Header("움직이는 시간(크면 빠름 => 거리로 나눔")]
    public float movetime;

    public SpriteRenderer catMouse;
    public Sprite mouse;
    public Transform arm;
    public Transform head;
    public bool collect;

    private Sprite originMouse;

    [SerializeField]
    private bool showItem = true;
    public bool ShowItem { get => showItem; set => showItem = value; }
    void Start()
    {
        collect = false;
        catMouse.sprite = originMouse;
        StartCoroutine(Motion());
        item = FindObjectOfType<PlayingItem>().transform;
    }

    private IEnumerator Motion()
    {
        while (true)
        {
            head.transform.DORotate(new Vector3(0, 0, -5), 0.2f);
            yield return new WaitForSeconds(0.2f);
            catMouse.sprite = mouse;
            for (int i = 0; i < 3; i++)
            {
                head.transform.DORotate(new Vector3(0, 0, -3), 0.2f);
                arm.transform.DORotate(new Vector3(0, 0, 10), 0.2f);
                yield return new WaitForSeconds(0.2f);
                head.transform.DORotate(new Vector3(0, 0, -5), 0.2f);
                arm.transform.DORotate(new Vector3(0, 0, 0), 0.2f);
                yield return new WaitForSeconds(0.2f);
            }
            head.transform.DORotate(new Vector3(0, 0, 0), 0.2f);
            catMouse.sprite = originMouse;
            catMouse.sprite = mouse;
            catMouse.sprite = originMouse;
            yield return new WaitForSeconds(1.0f);
        }
    }

    public void OnClick()
    {
        if (Touch) return;
        StartCoroutine(_FollowItem());
        AudioManager.Instance.PlaySound("cat_10", transform.position);
    }

    private IEnumerator _FollowItem()
    {
        Touch = true;
        float distance = Vector3.Distance(transform.position, item.position);
        transform.DOMove(item.position, distance / movetime);
        yield return new WaitForSeconds(distance / movetime);
        Touch = false;
        yield break;
    }
}
