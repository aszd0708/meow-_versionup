﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaderChartInfo : MonoBehaviour
{
    [HideInInspector] public string key;
    private string path;
    public TextAsset json;

    public class RaderChart
    {
        public int No;
        public string Info1;
        public float Value1;
        public string Info2;
        public float Value2;
        public string Info3;
        public float Value3;
        public string Info4;
        public float Value4;
        public string Info5;
        public float Value5;

        public RaderChart(int no, string info1, float value1, string info2, float value2, string info3, float value3, string info4, float value4, string info5, float value5)
        {
            No = no;
            Info1 = info1; Value1 = value1;
            Info2 = info2; Value2 = value2;
            Info3 = info3; Value3 = value3;
            Info4 = info4; Value4 = value4;
            Info5 = info5; Value5 = value5;
        }

        public void Print()
        {
            Debug.Log("No : " + No + 
                "\n정보1 : " + Info1 + " 값 : " + Value1 +
                "\n정보2 : " + Info2 + " 값 : " + Value2 +
                "\n정보3" + Info3 + " 값 : " + Value3 +
                "\n정보4" + Info4 + " 값 : " + Value4 +
                "\n정보5" + Info5 + " 값 : " + Value5);
        }

        public string SentInfo(int a)
        {
            switch (a)
            {
                case 1: return Info1;
                case 2: return Info2;
                case 3: return Info3;
                case 4: return Info4;
                case 5: return Info5;
                default: Debug.Log("에러에러");
                    break;
            }
            return null;
        }

        public float SendValue(int a)
        {
            switch (a)
            {
                case 1: return Value1;
                case 2: return Value2;
                case 3: return Value3;
                case 4: return Value4;
                case 5: return Value5;
                default:
                    Debug.Log("에러에러");
                    break;
            }
            return 0;
        }
    }

    void Start()
    {
        key = "Paint You Black";
        path = Application.persistentDataPath;
    }

    public List<RaderChart> LoadCats()
    {
        if (!System.IO.File.Exists(path + "/JsonChart.json"))
        {
            string jsonData = json.ToString();
            List<RaderChart> jsonCat = JsonConvert.DeserializeObject<List<RaderChart>>(jsonData);
            return jsonCat;
        }
        else
        {
            List<RaderChart> cat = new List<RaderChart>();
            var jtc2 = JsonDataManager_Dummy.LoadJsonFile<List<RaderChart>>(path, "JsonChart", key);
            return jtc2;
        }
    }

    public void SaveCats(List<RaderChart> jsonCat, string key)
    {
        string jsonData = JsonDataManager_Dummy.ObjectToJson(jsonCat);
        jsonData = JsonAES.Encrypt(jsonData, key);
        Debug.Log(jsonData);
        JsonDataManager_Dummy.CreateJsonFile(path, "JsonChart", jsonData);
    }
}
