﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class CatExplan : MonoBehaviour
{
    private string path;
    private string key;

    public class JsonExplanation
    {
        public int No;
        public string Name;
        public string Charactor;
        public string Favorite;

        public JsonExplanation(int no, string name, string charactor, string favorite)
        {
            No = no;
            Name = name;
            Charactor = charactor;
            Favorite = favorite;
        }

        public void Print()
        {
            Debug.Log(" 이름: " + Name + " 특징 : " + Charactor + " 좋아하는 것 : " + Favorite);
        }
    }

    private List<JsonExplanation> jsonExplanation = new List<JsonExplanation>();

    void Start()
    {
        key = "rkdguswl";
        path = Application.dataPath;

        jsonExplanation.Add(new JsonExplanation(3,"뚱냥", "뚱함", "츄르"));
        jsonExplanation[0].Print();
        SaveCats(jsonExplanation);
    }

    string ObjectToJson(object obj)
    {
        return JsonConvert.SerializeObject(obj);
    }

    T JsonToOject<T>(string jsonData)
    {
        return JsonConvert.DeserializeObject<T>(jsonData);
    }

    void CreateJsonFile(string createPath, string fileName, string jsonData)
    {
        FileStream fileStream = new FileStream(string.Format("{0}/{1}.json", createPath, fileName), FileMode.Create);
        byte[] data = Encoding.UTF8.GetBytes(jsonData);
        fileStream.Write(data, 0, data.Length);
        fileStream.Close();
    }

    T LoadJsonFile<T>(string loadPath, string fileName)
    {
        FileStream fileStream = new FileStream(string.Format("{0}/{1}.json", loadPath, fileName), FileMode.Open);
        byte[] data = new byte[fileStream.Length];
        fileStream.Read(data, 0, data.Length);
        fileStream.Close();
        string jsonData = Encoding.UTF8.GetString(data);
        //jsonData = JsonAES.Decrypt(jsonData, key);
        return JsonConvert.DeserializeObject<T>(jsonData);
    }

    public List<JsonExplanation> LoadCats()
    {
        List<JsonExplanation> cat = new List<JsonExplanation>();
        var jtc2 = LoadJsonFile<List<JsonExplanation>>(path, "JsonExplanation");
        jtc2[0].Print();
        return jtc2;
    }

    public void SaveCats(List<JsonExplanation> jsonCat)
    {
        string jsonData = ObjectToJson(jsonCat);
        //jsonData = JsonAES.Encrypt(jsonData, key);
        Debug.Log(jsonData);
        CreateJsonFile(path, "JsonExplanation", jsonData);
    }
}
