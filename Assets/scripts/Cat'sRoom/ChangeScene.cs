﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    public FadeInOut fade;

    public void ChangePrevScene()
    {
        fade.FadeOutF();
        Invoke("Change", 1.5f);
    }

    public void Change()
    {
        SceneManager.LoadScene(PlayerPrefs.GetString("prevScene"));
    }

    public void MainScene()
    {
        fade.FadeOutF();
        Invoke("ChangeMainScene", 1.5f);
    }

    public void ChangeMainScene()
    {

        SceneManager.LoadScene("MainMenu");
    }
}
