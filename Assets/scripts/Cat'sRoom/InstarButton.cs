﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstarButton : MonoBehaviour
{
    public string instarURL;

    public void OnClick()
    {
        Application.OpenURL(instarURL);
    }
}
