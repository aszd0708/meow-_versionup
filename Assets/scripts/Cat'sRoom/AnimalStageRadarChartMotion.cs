﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class AnimalStageRadarChartMotion : MonoBehaviour
{
    public RadarPolygon chartData;
    public AnimalChart chartInfo;
    public Text[] raderText = new Text[5];
    public float[] dataValue = new float[5];
    public Sprite[] rankSprite = new Sprite[5];
    public Material[] starMaterial = new Material[5];
    public Image rank;
    public ParticleSystemRenderer kirakira;

    public float chartSpeed= 1.0f;

    public void OnClick()
    {
        raderText[0].text = chartInfo.Info1;
        dataValue[0] = chartInfo.Value2 / 5;
        raderText[1].text = chartInfo.Info2;
        dataValue[1] = chartInfo.Value1 / 5;
        raderText[2].text = chartInfo.Info3;
        dataValue[2] = chartInfo.Value5 / 5;
        raderText[3].text = chartInfo.Info4;
        dataValue[3] = chartInfo.Value4 / 5;
        raderText[4].text = chartInfo.Info5;
        dataValue[4] = chartInfo.Value3 / 5;

        int num = (int)chartInfo.Value1;
        rank.sprite = rankSprite[num-1];
        kirakira.material = starMaterial[num - 1];

        for (int i = 0; i < chartData.value.Length; i++)
        {
            chartData.value[i] = 0.0f;
            chartData.SetAllDirty();
            StartCoroutine(ShowChartMotion(i, dataValue[i], chartSpeed));
        }
    }

    private IEnumerator ShowChartMotion(int dataNum, float dataValue, float speed)
    {
        yield return new WaitForSeconds(0.6f);

        float nowValue = 0;
        while(nowValue <= dataValue)
        {
            nowValue += speed * Time.deltaTime;
            chartData.value[dataNum] = nowValue;
            chartData.SetAllDirty();
            yield return null;
        }
        yield break;
    }

    private void ShowChart(int dataNum, float dataValue)
    {
        while (true)
        {
            if (chartData.value[dataNum] >= dataValue)
                return;
            chartData.value[dataNum] += 0.01f;
        }
    }
}
