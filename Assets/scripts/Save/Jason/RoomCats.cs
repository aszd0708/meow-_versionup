﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using UnityEngine.UI;

public class RoomCats : MonoBehaviour
{
    public TextAsset Json;
    public Sprite[] catImg;
    public ScrollRectSnap scollSnap;
    public GameObject button;
    public GameObject uiRoot;
    public float[] x, y;

    public class JsonCat
    {
        public int No;
        public string Name;
        public string ItemName;
        public string CatPath;
        public bool Collect;

        public JsonCat(int no, string name, string itemName, string catPath, bool collect)
        {
            No = no;
            Name = name;
            ItemName = itemName;
            CatPath = catPath;
            Collect = collect;
        }

        public void Print()
        {
            Debug.Log("번호 : " + No + " 이름: " + Name + " 수집 유무 : " + Collect + " 아이템 이름 : " + ItemName);
        }
    }

    string ObjectToJson(object obj)
    {
        return JsonConvert.SerializeObject(obj);
    }

    T JsonToOject<T>(string jsonData)
    {
        return JsonConvert.DeserializeObject<T>(jsonData);
    }

    void CreateJsonFile(string createPath, string fileName, string jsonData)
    {
        FileStream fileStream = new FileStream(string.Format("{0}/{1}.json", createPath, fileName), FileMode.Create);
        byte[] data = Encoding.UTF8.GetBytes(jsonData);
        fileStream.Write(data, 0, data.Length);
        fileStream.Close();
    }

    T LoadJsonFile<T>(string loadPath, string fileName)
    {
        FileStream fileStream = new FileStream(string.Format("{0}/{1}.json", loadPath, fileName), FileMode.Open);
        byte[] data = new byte[fileStream.Length];
        fileStream.Read(data, 0, data.Length);
        fileStream.Close();
        string jsonData = Encoding.UTF8.GetString(data);
        return JsonConvert.DeserializeObject<T>(jsonData);
    }

    void Start()
    {
        catImg = Resources.LoadAll<Sprite>("Cats'House/Animals/Cats");
        List<JsonCat> cat = new List<JsonCat>();
        var jtc2 = LoadJsonFile<List<JsonCat>>(Application.dataPath, "JsonCats");
    }

    public void CreateButton(List<JsonCat> jtc2)
    {
        jtc2.Sort(delegate (JsonCat A, JsonCat B) { return B.Collect.CompareTo(A.Collect); });
        for (int i = 0; i < jtc2.Count; i++)
            jtc2[i].Print();
        for (int i = 0; i < jtc2.Count; i++)
        {
            Button child = Instantiate(button).GetComponent<Button>();
            child.transform.GetChild(0).GetComponent<Image>().sprite = catImg[jtc2[i].No - 1];
            child.GetComponent<CatButton>().name = jtc2[i].Name;
            child.GetComponent<CatButton>().catNumber = jtc2[i].No;
            if (!jtc2[i].Collect)
            {
                child.transform.GetChild(0).GetComponent<Image>().color = new Color(0, 0, 0);
                child.enabled = false;
            }
            child.transform.parent = uiRoot.transform;
            child.GetComponent<CatButton>().i = i;
            child.GetComponent<RectTransform>().anchoredPosition = new Vector3(i * 700, 0, 0);
            child.gameObject.GetComponent<CenterSize>().x = x;
            child.gameObject.GetComponent<CenterSize>().y = y;
            scollSnap.GetCatScroll(child, i);
        }
    }
}