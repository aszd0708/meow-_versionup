﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class JsonDataManager_Dummy : MonoBehaviour
{
    [Header("제이슨 데이터")]
    public TextAsset jsonData;

    static public string ObjectToJson(object obj)
    {
        return JsonConvert.SerializeObject(obj);
    }

    static public T JsonToOject<T>(string jsonData)
    {
        return JsonConvert.DeserializeObject<T>(jsonData);
    }   

    static public void CreateJsonFile(string createPath, string fileName, string jsonData)
    {
        FileStream fileStream = new FileStream(string.Format("{0}/{1}.json", createPath, fileName), FileMode.Create);
        byte[] data = Encoding.UTF8.GetBytes(jsonData);
        fileStream.Write(data, 0, data.Length);
        fileStream.Close();
    }

    static public T LoadJsonFile<T>(string loadPath, string fileName, string key)
    {
        FileStream fileStream = new FileStream(string.Format("{0}/{1}.json", loadPath, fileName), FileMode.Open);
        byte[] data = new byte[fileStream.Length];
        fileStream.Read(data, 0, data.Length);
        fileStream.Close();
        string jsonData = Encoding.UTF8.GetString(data);
        jsonData = JsonAES.Decrypt(jsonData, key);
        return JsonConvert.DeserializeObject<T>(jsonData);
    }
}
