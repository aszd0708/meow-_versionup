﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JsonCatExplanation : MonoBehaviour
{
    [HideInInspector] public string key;
    private string path;
    public TextAsset json;

    public class Explanation
    {
        public int No;
        public string Name;
        public string Charactor;
        public string Favorite;
        public string Hint;
        public string Story;
        public string Tip;
        public string Instargram;
        public string HashTag;

        public Explanation(int no, string name, string charactor, string favorite, string hint, string story, string tip, string instargram, string hashTag)
        {
            No = no;
            Name = name;
            Charactor = charactor;
            Favorite = favorite;
            Hint = hint;
            Story = story;
            Tip = tip;
            Instargram = instargram;
            HashTag = hashTag;
        }

        public void Print()
        {
            Debug.Log("No : " + No + "\nName : " + Name + "\n특징 : " + Charactor + "\n취미 : " + Favorite + "\n힌트 : " + Hint + "\n스토리 : " + Story + "\n팁 : " + Tip +"\n인스타 : " + Instargram); 
        }

        public string Explan()
        {
            return "특징 :\t" + Charactor + "\n취미 :\t" + Favorite  + "\n스토리 :\t" + Story;
        }

        public string SendCharactor()
        {
            return Charactor;
        }

        public string SendFavorite()
        {
            return Favorite;
        }

        public string SendStory()
        {
            return Story;
        }

        public string SendTip()
        {
            return Tip;
        }

        public string NameExplan()
        {
            return Name;
        }

        public string InstarURL()
        {
            return Instargram;
        }

        public string HashTagText()
        {
            return HashTag;
        }
    }

    void Start()
    {
        key = "Tell Me If You Can";
        path = Application.persistentDataPath;
    }
    
    public List<Explanation> LoadCats()
    {
        if (!System.IO.File.Exists(path + "/JsonExplanation.json"))
        {
            string jsonData = json.ToString();
            List<Explanation> jsonCat = JsonConvert.DeserializeObject<List<Explanation>>(jsonData);
            return jsonCat;
        }
        else
        {
            List<Explanation> cat = new List<Explanation>();
            var jtc2 = JsonDataManager_Dummy.LoadJsonFile<List<Explanation>>(path, "JsonExplanation", key);
            return jtc2;
        }
    }

    public void SaveCats(List<Explanation> jsonCat, string key)
    {
        string jsonData = JsonDataManager_Dummy.ObjectToJson(jsonCat);
        jsonData = JsonAES.Encrypt(jsonData, key);
        Debug.Log(jsonData);
        JsonDataManager_Dummy.CreateJsonFile(path, "JsonExplanation", jsonData);
    }
}
