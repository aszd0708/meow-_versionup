﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-02-11
 * 스테이지별 언락을 해주는 클래스
 */

public class StageUnlockData : SerializableManager<bool>
{
    private List<bool> stageComplete = new List<bool>();

    [Header("현재 스테이지 인덱스 Tutorial = 0")]
    public int nowStage;

    [Header("스테이지의 총 갯수를 스크립트에서 수정해주기")]
    private int stageCount = 3;

    private void Awake()
    {
        SaveDataName = "StageComplete";

        stageComplete = LoadDataFile();

        if (stageComplete == null)
            stageComplete = DefaultDataSetting();

        if (stageComplete.Count == stageCount)
            stageComplete = DifferentDataSetting();
    }

    private List<bool> DefaultDataSetting()
    {
        List<bool> defaultData = new List<bool>();

        for (int i = 0; i < stageCount + 1; i++)
            defaultData.Add(false);

        return defaultData;
    }

    private List<bool> DifferentDataSetting()
    {
        List<bool> differentData = stageComplete;

        for (int i = stageComplete.Count + 1; i < stageCount + 1; i++)
            differentData.Add(false);

        return differentData;
    }

    public bool GetStageComplete() { return stageComplete[nowStage]; }

    public bool GetStageComplete(int stageNum) { return stageComplete[stageNum]; }

    public void SetStageComplete(bool isComplete)
    {
        stageComplete[nowStage] = isComplete;
        SaveDataFile(stageComplete);
    }

    public void SetStageComplete(int stageNum, bool isComplete)
    {
        stageComplete[stageNum] = isComplete;
        SaveDataFile(stageComplete);
    }
}
