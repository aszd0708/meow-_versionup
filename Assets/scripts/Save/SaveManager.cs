﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
    private Stage1SaveLoad saveData;
    private CatSaveLoad saveJson;
    private StageUnlockData unlockData;

    [Header("여기 스테이지 번호는 지금까지의 스테이지 갯수 tutorial = 0 스크립트에서 수정해주기")]
    private int stageCount = 3;

    private void Awake()
    {
        saveData = GetComponent<Stage1SaveLoad>();
        saveJson = GetComponent<CatSaveLoad>();
        unlockData = GetComponent<StageUnlockData>();
    }

    public void DeleteStageData()
    {
        saveJson.DelFile();
        saveData.ResetSave();
    }

    public void DeleteStageData(int stageNumber)
    {
        saveJson.DelFile(stageNumber);
        saveData.DeleteData(stageNumber);
    }

    public void DeleteStageUnlock()
    {
        unlockData.DeleteDataFile();
    }

    public void DeleteAllSaveData()
    {
        for(int i = 0; i < stageCount; i++)
        {
            DeleteStageData(i);
        }
        unlockData.DeleteDataFile();
    }
}
