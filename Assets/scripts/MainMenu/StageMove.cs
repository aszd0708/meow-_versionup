﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageMove : MonoBehaviour
{
    public float speed;
    public int stageNum;
    public GameObject stage;

    private GameObject center;
    private bool move;

    private bool touch = true;

    private Vector2 downPosition = new Vector2(0, 0);
    private Vector2 upPosition = new Vector2(0, 0);
    void Start()
    {
        move = true;
        Debug.Log(stage.transform.GetChild(0).gameObject);
        center = stage.transform.GetChild(stageNum).gameObject;
        for(int i = 0; i<stage.transform.childCount; i++)
        {
            stage.transform.GetChild(i).GetComponent<BoxCollider2D>().enabled = false;
        }
        center.GetComponent<BoxCollider2D>().enabled = true;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if(touch)
            {
                downPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                Debug.Log(downPosition);
                touch = false;
            }
        }

        else if (Input.GetMouseButtonUp(0))
        {
            upPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            touch = true;
            if(downPosition.x < upPosition.x)
            {
                LeftMove();
            }

            else if(downPosition.x > upPosition.x)
            {
                RightMove();
            }
            
            Debug.Log(downPosition);
            Debug.Log(upPosition);
        }
    }

    public void RightMove()
    {
        if(move)
        {
            if (stageNum + 1 >= stage.transform.childCount)
                return;
            center.GetComponent<BoxCollider2D>().enabled = false;
            move = false;
            iTween.MoveBy(stage, iTween.Hash("x", -7.0f, "Time", speed, "easeType", iTween.EaseType.linear));
            center = stage.transform.GetChild(++stageNum).gameObject;
            Invoke("ChangeMove", speed);
        }
        
    }

    public void LeftMove()
    {
        if (move)
        {
            if (stageNum - 1 < 0)
                return;
            center.GetComponent<BoxCollider2D>().enabled = false;
            move = false;
            iTween.MoveBy(stage, iTween.Hash("x", 7.0f, "Time", speed, "easeType", iTween.EaseType.linear));
            center = stage.transform.GetChild(--stageNum).gameObject;
            Invoke("ChangeMove", speed);
        }
            
    }

    private void ChangeMove()
    {
        move = true;
        center.GetComponent<BoxCollider2D>().enabled = true;
    }
}
