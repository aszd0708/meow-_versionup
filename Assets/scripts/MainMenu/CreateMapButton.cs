﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CreateMapButton : MonoBehaviour
{

    [System.Serializable]
    public class MapButtonInfo
    {
        public enum MapKind
        {
            SPRITE, PREFEB
        }

        public Sprite mapSprite;
        public GameObject mapPreb;
        public MapKind MAPKIND;

        public Sprite nonColSprite;
        public Vector2 catPosition;
        public string sceneName;
        public string mapName;
        public bool mapLock;
        public int start, end;
        public StartStage.Kind KIND;
        public int gachaNumber;
    }
    private int sceneNum = 0;

    public GameObject stage;
    public GameObject cat;
    public GameObject mapButton;
    public Vector2 mapPosition;
    public Vector2 gatchaPos;
    public List<MapButtonInfo> MapList;

    public GameObject collectPosition;
    public GameObject collect;
    public GameObject nonCollect;

    public float colDis;

    private CatSaveLoad data;
    private StageUnlockData stageUnlock;
    private List<CatSaveLoad.JsonCat> catCollect;
    private Vector2 createPosition;
    private MapButtonInfo prevMap;
    // Start is called before the first frame update

    void Awake()
    {
        data = GetComponent<CatSaveLoad>();
        stageUnlock = GetComponent<StageUnlockData>();
    }

    void Start()
    {
        catCollect = data.LoadCats();   
        createPosition = new Vector2(8, mapPosition.y);
        //stageUnlock.SetStageComplete(0, true);
        PopulateList();
    }

    public void PopulateList()
    {
        for(int count = 0; count < MapList.Count; count++)
        {
            bool colMap = true;
            GameObject createMap ;
            MapButton mapScript ;
            StartStage startStage;
            
            // 여기 수정
            if (MapList[count].mapSprite != MapList[count].nonColSprite)
            {
                colMap = stageUnlock.GetStageComplete(count / 2);
                Debug.Log(count + "번째 맵이 열렸나!?!?" + colMap);

                if(count % 2 == 1)
                {
                    Debug.Log(count + "번째 가차가 열렸나!?!?" + colMap);
                    for (int i = MapList[count-1].start; i <= MapList[count - 1].end; i++)
                    {
                        Debug.Log(catCollect[i].Name + " : " + catCollect[i].Collect);
                        if (catCollect[i].Collect)
                        {
                            colMap = true;
                            break;
                        }
                        else colMap = false;
                    }
                }
            }

            if (colMap)
            {
                switch (MapList[count].MAPKIND)
                {
                    case MapButtonInfo.MapKind.SPRITE:
                        createMap = Instantiate(mapButton) as GameObject;
                        break;
                    case MapButtonInfo.MapKind.PREFEB:
                        createMap = Instantiate(MapList[count].mapPreb) as GameObject;
                        break;
                    default:
                        createMap = null;
                        break;
                }
                mapScript = createMap.GetComponent<MapButton>();
                startStage = createMap.GetComponent<StartStage>();
                mapScript.mapSprite = MapList[count].mapSprite;
            }

            else
            {
                createMap = Instantiate(mapButton) as GameObject;
                mapScript = createMap.GetComponent<MapButton>();
                startStage = createMap.GetComponent<StartStage>();
                mapScript.mapSprite = MapList[count].nonColSprite;
                createMap.GetComponent<BoxCollider2D>().enabled = false;
            }

            createMap.transform.position = createPosition;
            createMap.name = MapList[count].sceneName;

            mapScript.catPosition = MapList[count].catPosition;
            mapScript.sceneName = MapList[count].sceneName;
            mapScript.mapName = MapList[count].mapName;
            mapScript.mapLock = MapList[count].mapLock;
            mapScript.cat = cat;
            mapScript.num = sceneNum + 1;
            CheckCollect(createMap, MapList[count].start, MapList[count].end);
            sceneNum++;
            
            createMap.transform.SetParent(stage.transform);

            startStage.KIND = MapList[count].KIND;
            startStage.GatchaNum = MapList[count].gachaNumber;

            mapScript.Create();
            if (MapList[count].KIND != StartStage.Kind.GATCHA)
            {
                createPosition = new Vector2(createPosition.x + Mathf.Abs(gatchaPos.x), mapPosition.y);
                prevMap = MapList[count];
            }
            else
            {
                createPosition = new Vector2(createPosition.x + Mathf.Abs(mapPosition.x), mapPosition.y);
                createMap.transform.GetChild(1).transform.localScale = new Vector3(0, 0, 0);
            }
        }
    }

    public void CheckCollect(GameObject createMap, int start, int end)
    {
        if (start == -1 && end == -1)
            return;
        float distance = 0;
        int count = end - start + 1;
        int fiveCount = 0;
        GameObject collectPos = Instantiate(collectPosition);
        GameObject[] calPos;
        if (count < 5)
            calPos = new GameObject[count % 5 + 1];
        else
            calPos = new GameObject[5];
        collectPos.transform.SetParent(createMap.transform);
        collectPos.transform.localPosition = new Vector3(0, 0, 0);
        distance = 5 / 2 * -1f;

        if (count % 2 == 0)
            distance = distance - colDis * 3;
        else if (count % 2 == 1)
            distance = distance - colDis * 2;

        for (int i = start, num = 0; i <= end; i++, num++, fiveCount++)
        {
            GameObject collection = null;
            if (catCollect[i].Collect == false)
                collection = Instantiate(nonCollect);

            else if(catCollect[i].Collect == true)
                collection = Instantiate(collect);

            collection.transform.SetParent(collectPos.transform);

            distance = num % 5 * colDis * 2;
            
            collection.transform.localPosition = new Vector3(distance, -(num / 5) * colDis * 2 - 1, -5);
            calPos[fiveCount] = collection;
            Debug.Log(fiveCount);
            if (fiveCount == 4)
            {   
                float midDis = colDis * 2 * (fiveCount) / 2;
                Debug.Log(num + " " + midDis);
                for (int a = 0; a < calPos.Length; a++)
                {
                    calPos[a].transform.localPosition = new Vector3(calPos[a].transform.localPosition.x - midDis, calPos[a].transform.localPosition.y, calPos[a].transform.localPosition.z);
                }
                fiveCount = -1;
            }
        }

        if(count % 5 != 0)
        {
            int smallCount;
            float midDis;
            smallCount = count % 10 - count / 5 * 5;
            midDis = colDis * 2 * (fiveCount - 1) / 2;
            for (int i = 0; i < fiveCount; i++)
            {
                Debug.Log(midDis);
                calPos[i].transform.localPosition = new Vector3(calPos[i].transform.localPosition.x - midDis, calPos[i].transform.localPosition.y, calPos[i].transform.localPosition.z);
            }
        }
    }
}
