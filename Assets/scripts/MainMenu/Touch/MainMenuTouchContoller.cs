﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// 스테이지 터치했을때 이벤트 컨트롤러
/// </summary>
public class MainMenuTouchContoller : Singleton<MainMenuTouchContoller>, ITouchManger
{
    /* 
     * 터치했을시 시간 넘어가면 드래그로 인식
     * 안넘어가면 터치로 인식
     */
    [Header("터치로 인식되는 시간")]
    public float checkTouchTime;
    /* 한번 터치했을때 터치한 시간 */
    private float totalTouchTime = 0;

    [Header("카메라 움직임")]
    public MainMenuCamereController cameraContoller;
    [Header("스테이지 터치")]
    public MainMenuStageTouchController stageTouchController;

    [Header("여기서 모든 관리 함")]
    public MainMenuCatMove catMove;

    private bool canTouch = true;

    /// <summary>
    /// 터치 막아주는 컴포넌트
    /// </summary>
    public bool CanTouch { get => canTouch; set => canTouch = value; }

    private bool bMoveCam = true;
    public bool BMoveCam { get => bMoveCam; set => bMoveCam = value; }

    private void Update()
    {
        if (EventSystem.current.IsPointerOverGameObject()) { return; }

        if (!CanTouch) { return; }

        Touch();
    }

    private void LateUpdate()
    {
        cameraContoller.CheckNaN();
        cameraContoller.SetCameraPositionClamp();
        if (!BMoveCam) { return; }

        cameraContoller.DragEvent();        
    }

    private void Touch()
    {
        // 터치 막아줌

        if (Input.GetMouseButtonDown(0))
        {
            totalTouchTime = 0.0f;
        }

        else if (Input.GetMouseButton(0))
        {
            totalTouchTime += Time.deltaTime;

        }

        else if (Input.GetMouseButtonUp(0))
        {
            if (totalTouchTime <= checkTouchTime && !cameraContoller.StartDraging)
            {
                stageTouchController.TouchEvent(Camera.main.ScreenToWorldPoint(Input.mousePosition));
            }
        }
    }

    /// <summary>
    /// 고양이가 움직일때 실행하는 함수들
    /// 카메라 타겟 잡아주고 고양이 목적지 설정
    /// 터치 막음
    /// </summary>
    /// <param name="destination"></param>
    /// <param name="stageInfo"></param>
    public void StartCatMove(Vector3 destination, MainMenuStageNowInfo stageInfo)
    {
        catMove.MoveToDetination(destination, stageInfo);
        CanTouch = false;

        cameraContoller.SetTarget(catMove.transform);
    }

    /// <summary>
    /// 고양이 움직임이 끝나면 실행하는 함수들
    /// 터치 막은거 풀어주고 타겟 없는걸로 재설정
    /// </summary>
    public void EndCatMove()
    {
        CanTouch = true;
        cameraContoller.SetTarget();
    }

    /// <summary>
    /// 튜토리얼 끝나고 나오는 모션 때문에 이케 만듬
    /// </summary>
    public void SetTutorialMotionCamera()
    {
        cameraContoller.SetTarget(catMove.transform);
        CanTouch = false;
    }

    public void ForbitTouch(bool canTouch)
    {
        if(cameraContoller.IsTargetting)
        {
            CanTouch = false;
        }
        else
        {
            CanTouch = canTouch;
        }
        BMoveCam = canTouch;
    }
}
