﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 카메라 움직이는 스크립트
/// 터치로 움직이게 할거임
/// </summary>
public class MainMenuCamereController : MonoBehaviour
{
    [Header("움직일 메인 카메라")]
    public Transform mainCamTransform;

    [Header("카메라 움직일 속도")]
    [Range(0.0f, 20.0f)]
    public float speed;

    [Header("드래그 해서 움직일때 얼마나 부드럽게 움직이는지")]
    [Range(0.0f, 20.0f)]
    public float dragSmooth;

    [Header("움직일수 있는 최소 X축")]
    public float minX;

    [Header("움직일수 있는 최대 X축")]
    private float maxX;

    [Header("부드럽게 움직이는 가속도")]
    public float smoothAccelation = 5;

    [Header("스테이지 부모")]
    public Transform stagesParent;

    /* 드래그 할때 켜지는 부울값 */
    private bool isDraging = false;

    /* 드래그 시작시 처음 누르는 X 좌표 */
    private float startDragingX;
    /* 드래그 중일때 X좌표 */
    private float nowDragingX;

    /* 드래그 종료 후 매끄럽게 움직이기 위한 부울값 */
    private bool isSmooth;
    /* 드래그한 총 시간 */
    private float dragTime;
    /* 총 드래그한 거리 */
    private float moveDistance;
    /* 매끄럽게 움직일때 속도 */
    private float nowMoveSpeed;

    /* 타게팅 중임? */
    private bool isTargetting = false;

    [Header("타게팅 해서 카메라로 계속 보여줌")]
    private Transform target;

    public Transform Target { get => target; set => target = value; }


    private bool startDraging = false;
    /// <summary>
    /// 드래그를 조금이라도 했는지
    /// </summary>
    public bool StartDraging { get => startDraging; set => startDraging = value; }
    public bool IsTargetting { get => isTargetting; set => isTargetting = value; }

    private void Start()
    {
        maxX = stagesParent.GetChild(stagesParent.childCount - 1).transform.position.x;
    }

    public void SetCameraPositionClamp()
    {
        float clampX = Mathf.Clamp(mainCamTransform.position.x, minX, maxX);
        mainCamTransform.position = new Vector3(clampX, 0, mainCamTransform.position.z);
    }

    /// <summary>
    /// 터치 했을때의 이벤트들
    /// </summary>
    public void DragEvent()
    {
        if (IsTargetting)
        {
            MoveTargetPose();
            return;
        } 

        if (Input.GetMouseButtonDown(0))
        {
            dragTime = 0;
            startDragingX = Camera.main.ScreenToWorldPoint(Input.mousePosition).x;
            nowDragingX = Camera.main.ScreenToWorldPoint(Input.mousePosition).x;

            isDraging = true;
            isSmooth = false;
        }
        else if(Input.GetMouseButtonUp(0))
        {
            isDraging = false;
            isSmooth = true;

            StartDraging = false;

            moveDistance = nowDragingX - startDragingX;
            nowMoveSpeed = moveDistance / dragTime;
        }
        else if(Input.GetMouseButton(0) && isDraging)
        {
            DoDrag();
        }

        else if(!Input.GetMouseButton(0) && isSmooth)
        {
            DoSmoothMotion();
        }

        //mainCamTransform.position = new Vector3(clampX, mainCamTransform.position.y, mainCamTransform.position.z);
    }

    /// <summary>
    /// 드래그 했을때 실행되는 함수
    /// 드래그 한 방향의 반대로 카메라가 움직임
    /// </summary>
    private void DoDrag()
    {
        Vector3 nowPosition = mainCamTransform.position;
        float distance = nowDragingX - startDragingX;
        float nowSpeed = speed;
        nowDragingX = Camera.main.ScreenToWorldPoint(Input.mousePosition).x;

        if (Mathf.Abs(distance) >= 0.1f)
            StartDraging = true;

        if (mainCamTransform.position.x <= minX && distance >= 0)
            nowSpeed = 0;
        else if (mainCamTransform.position.x >= maxX && distance <= 0)
            nowSpeed = 0;

        mainCamTransform.position = Vector3.Lerp(mainCamTransform.position, 
            new Vector3((nowPosition.x - distance * nowSpeed), nowPosition.y, nowPosition.z), Time.deltaTime * dragSmooth);

        dragTime += Time.deltaTime;
    }

    /// <summary>
    /// 터치 끝나면 가속도로 부드럽게 움직이는 함수
    /// </summary>
    private void DoSmoothMotion()
    {
        Vector3 nowPosition = mainCamTransform.position;
        if (mainCamTransform.position.x <= minX || mainCamTransform.position.x >= maxX)
        {
            nowMoveSpeed = 0;
        }

        Vector3 lerpVector3 = Vector3.Lerp(mainCamTransform.position,
            new Vector3((nowPosition.x - nowMoveSpeed), nowPosition.y, nowPosition.z), Time.deltaTime * dragSmooth);

        if(float.IsNaN(lerpVector3.x) == true)
        {
            lerpVector3 = new Vector3(mainCamTransform.position.x, lerpVector3.y, lerpVector3.z);
        }

        mainCamTransform.position = lerpVector3;

        nowMoveSpeed = Mathf.Lerp(nowMoveSpeed, 0, Time.deltaTime * smoothAccelation);
    }

    /// <summary>
    /// 타겟으로 이동
    /// </summary>
    private void MoveTargetPose()
    {
        Vector3 targetPosition = Target.position;
        //float distance = Mathf.Abs(targetPosition.x - mainCamTransform.position.x);

        mainCamTransform.position = Vector3.Lerp(mainCamTransform.position,
            new Vector3((targetPosition.x), mainCamTransform.position.y, mainCamTransform.position.z), Time.deltaTime * dragSmooth);
    }

    private void SetLerpPositiona()
    {
        if (mainCamTransform.position.x <= minX)
            Vector3.Lerp(mainCamTransform.position, new Vector3(minX, 0, mainCamTransform.position.z), Time.deltaTime * 200);
        else if (mainCamTransform.position.x >= maxX)
            Vector3.Lerp(mainCamTransform.position, new Vector3(maxX, 0, mainCamTransform.position.z), Time.deltaTime * 200);
    }

    /// <summary>
    /// 기본값 null
    /// 만약 기본값만 입력했다면 타게팅이 풀리게 됨
    /// </summary>
    /// <param name="target"></param>
    public void SetTarget(Transform target = null)
    {
        nowMoveSpeed = 0;
        if (target == null)
        {
            IsTargetting = false;
        }
        else
        {
            IsTargetting = true;
        }
        Target = target;
    }

    public void CheckNaN()
    {       
        if (float.IsNaN(mainCamTransform.position.x) == true)
        {
            mainCamTransform.position = new Vector3(Target.position.x, mainCamTransform.position.y, mainCamTransform.position.z);
            Debug.Log("IsNaN");
        }
    }
}
