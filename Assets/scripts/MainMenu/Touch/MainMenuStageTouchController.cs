﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// 드래그와 오브젝트 터치 둘다 관리해주는 클래스
/// </summary>
public class MainMenuStageTouchController : MonoBehaviour
{
    [Header("스테이지 버튼 레이어")]
    public LayerMask touchLayer;

    [Header("고양이 움직이게 함")]
    public MainMenuCatMove catMove;

    public GameObject testObj;

    /// <summary>
    /// 오브젝트 터치 이벤트
    /// 터치 좌표 받아서 오브젝트 구한 뒤 함
    /// </summary>
    /// <param name="touchPose"></param>
    public void TouchEvent(Vector3 touchPose)
    {
        RaycastHit2D hit = Physics2D.Raycast(touchPose, Vector2.zero, 0.0f, touchLayer);
        if (hit.transform == null)
            return;
        GameObject hitObj = testObj = hit.collider.gameObject;

        if (hitObj == null)
            return;

        MainMenuStageNowInfo stageInfo = hitObj.GetComponent<MainMenuStageNowInfo>();
        SettingStageButton stageSetting = hitObj.GetComponent<SettingStageButton>();

        if (stageInfo.HaveCat)
        {
            // 여기 터치 막고 스테이지 넘어가는거  
            if (stageSetting.StageKind == StageKind.GATCHA)
            {
                PlayerPrefs.SetInt("GatchaStageIndex", stageSetting.GatchaIndex);
            }
            PlayerPrefs.SetInt("NowSelectStage", stageSetting.StageIndex);
            SceneChangeManager.Instance.ChangeScene(stageSetting.StageSceneName);


            AudioManager.Instance.PlaySound("cat_5", transform.position);
        }

        else
        {
            // 여기 고양이 움직이게 하는거
            // 터치 막음
            Vector2 position = hitObj.transform.position;
            MoveToStage(position, stageSetting, stageInfo);
        }
    }

    private void MoveToStage(Vector2 destination, SettingStageButton stageSetting, MainMenuStageNowInfo info)
    {
        PlayerPrefs.SetInt("NowSelectStage", stageSetting.StageIndex);
        MainMenuTouchContoller.Instance.StartCatMove(destination + stageSetting.CatStandPose, info);
        MainMenuStageNowInfosController.Instance.SetNonCat();
        info.HaveCat = true;
    }

    /// <summary>
    /// 튜토리얼 끝나고 고양이 이동시키는 함수
    /// </summary>
    public void TutorialEndStageMove(Vector2 destination, SettingStageButton stageSetting, MainMenuStageNowInfo info)
    {
        MoveToStage(destination, stageSetting, info);
    }
}
