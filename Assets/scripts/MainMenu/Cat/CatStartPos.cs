﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatStartPos : MonoBehaviour
{
    public Transform stages;
    // Start is called before the first frame update
    void OnEnable()
    {
        Invoke("SetPos", 0.1f);
    }

    private void SetPos()
    {
        if (!PlayerPrefs.HasKey("prevScene"))
            PlayerPrefs.SetString("prevScene", stages.GetChild(1).GetComponent<MapButton>().sceneName);

        string sceneName = PlayerPrefs.GetString("prevScene");

        if (sceneName != "Stage0 Tutorial")
            stages.GetChild(0).GetComponent<StageClick>().state = StageClick.State.nonCat;

        Debug.Log("씬 이름 : " + sceneName);
        for (int i = 0; i < stages.childCount; i++)
        {
            if (sceneName == stages.GetChild(i).GetComponent<MapButton>().sceneName)
            {
                transform.SetParent(stages.GetChild(i).transform);
                transform.localPosition = new Vector3(stages.GetChild(i).transform.GetChild(0).transform.localPosition.x, transform.localPosition.y, 0);
                stages.GetChild(i).GetComponent<CatMove>().stage = stages.GetChild(i).gameObject;
                stages.GetChild(i).GetComponent<CatMove>().StateChange();
                stages.position = new Vector3(-stages.GetChild(i).transform.position.x, stages.position.y, stages.position.z);
                if (i != 1)
                    stages.GetChild(1).GetComponent<StageClick>().state = StageClick.State.nonCat;
                break;
            }
        }
    }
}
