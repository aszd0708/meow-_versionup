﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

/// <summary>
/// 메인메뉴 고양이 움직이는 스크립트
/// </summary>
public class MainMenuCatMove : MonoBehaviour
{
    [Header("모션 컴포넌트")]
    public MainMenuCatMotion motion;

    [Header("움직이는 속도")]
    public float speed;

    private bool isArrival;

    public bool IsArrival { get => isArrival; set => isArrival = value; }

    public enum State
    {
        MOVE, IDLE
    }

    public GameObject fingerObj;

    private State nowState;
    /// <summary>
    /// 지금 고양이 상태
    /// </summary>
    public State NowState 
    { 
        get => nowState;
        set
        { 
            nowState = value; 
            switch(nowState)
            {
                case State.IDLE:
                    fingerObj.SetActive(true);
                    break;

                case State.MOVE:
                    fingerObj.SetActive(false);
                    break;
            }
        }
    }

    public void Start()
    {
        NowState = State.IDLE;
    }

    /// <summary>
    /// 목적지까지 가는 함수
    /// </summary>
    /// <param name="destination"></param>
    public void MoveToDetination(Vector3 destination, MainMenuStageNowInfo stageState)
    {
        StartCoroutine(_MoveToDestination(destination, stageState));
    }

    private IEnumerator _MoveToDestination(Vector3 destination, MainMenuStageNowInfo stageState)
    {
        Vector2 vec = Vector2.zero;
        if (destination.x > transform.position.x)
        {
            vec = Vector2.right;
        }

        else
        {
            vec = Vector2.left;
        }

        NowState = State.MOVE;
        motion.StartMotion(destination);

        while(Mathf.Abs(transform.position.x - destination.x) > 0.1f)
        {
            transform.Translate(vec * speed * Time.deltaTime);
            //transform.DOMove(destination, speed);
            yield return null;
        }

        transform.position = destination;
        motion.Arrival = true;
        NowState = State.IDLE;
        StartCoroutine(_CheckArraival(stageState));
        yield break;
    }

    /// <summary>
    /// 도착했으면 스테이지 고양이 있다고 체크해주는 함수
    /// 코루틴 사용
    /// </summary>
    /// <param name="stageState"></param>
    /// <returns></returns>
    private IEnumerator _CheckArraival(MainMenuStageNowInfo stageState)
    {
        stageState.HaveCat = true;

        MainMenuTouchContoller.Instance.EndCatMove();

        yield break;
    }
}
