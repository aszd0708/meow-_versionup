﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatMove : MonoBehaviour
{
    public GameObject cat;
    public GameObject stage;
    public float speed = 2.0f;

    private float narrowGapMoveTime;
    private float wideGapMoveTime;

    private int nowStageNum;

    private Transform parentTransform;
    private MainMenuFingerMotion finger;
    public float NarrowGapMoveTime { get => narrowGapMoveTime; set => narrowGapMoveTime = value; }
    public float WideGapMoveTime { get => wideGapMoveTime; set => wideGapMoveTime = value; }

    private void Awake()
    {
        finger = FindObjectOfType<MainMenuFingerMotion>();
    }

    private void Start()
    {
        parentTransform = transform.parent;
        NarrowGapMoveTime = 0.8f;
        WideGapMoveTime = 1.0f;
    }

    public void OnClick()
    {
        cat.transform.SetParent(gameObject.transform);
        Debug.Log(gameObject.name);
        cat.transform.DOLocalMoveX(gameObject.transform.GetChild(0).transform.localPosition.x, 2).SetEase(Ease.Linear);
    }

    public void Move(GameObject target)
    {
        finger.FingerMotionController(true);
        int mapCount = CountMapNumber(target);
        float time = GetMapMoveTime(target);
        cat.transform.parent.GetComponent<StageClick>().state = StageClick.State.nonCat;
        stage = target;
        cat.transform.SetParent(stage.transform);
        cat.GetComponent<MainMenuCatMotion>().Arrival = false;
        cat.GetComponent<MainMenuCatMotion>().ChangeMotion(target);
        cat.transform.DOLocalMoveX(gameObject.transform.GetChild(0).transform.localPosition.x, time).SetEase(Ease.Linear).OnComplete(StateChange);
        Invoke("SetFingerStop", time);
    }

    private void SetFingerStop()
    {
        finger.FingerMotionController(false);
    }

    public void StateChange()
    {
        Debug.Log("상태 변환");
        cat.GetComponent<MainMenuCatMotion>().Arrival = true;
        stage.GetComponent<StageClick>().state = StageClick.State.cat;
    }

    public int CountMapNumber(GameObject target)
    {
        int count;

        int startNumber = cat.transform.parent.GetComponent<MapButton>().num;
        int endNumber = target.GetComponent<MapButton>().num;

        Debug.Log("시작 맵 : " + startNumber);
        Debug.Log("끝 맵 : " + endNumber);

        count = Mathf.Abs(startNumber - endNumber);

        Debug.Log("맵차이" + (startNumber - endNumber));

        return count;
    }

    private float GetMapMoveTime(GameObject target)
    {
        float time = 0;

        int nowStageCount = cat.transform.parent.GetComponent<MapButton>().num;
        int destStageCount = target.GetComponent<MapButton>().num;

        print("시작 맵 카운트 : " + nowStageCount);
        print("목적지 맵 카운트 : " + destStageCount);

        if (Mathf.Abs(nowStageCount - destStageCount) >= 3)
            return 1.5f;

        if(nowStageCount > destStageCount)
        {
            int temp = nowStageCount;
            nowStageCount = destStageCount;
            destStageCount = temp;
        }

        if (nowStageCount % 2 == 0)
            time -= NarrowGapMoveTime;
        else
            time -= WideGapMoveTime;

        for (int i = nowStageCount; i <= destStageCount; i++)
        {
            if (i % 2 == 0)
                time += NarrowGapMoveTime;
            else
                time += WideGapMoveTime;
        }

        print("걸리는 시간 : " + time);
        return time;
    }

    private int GetChildCount(GameObject map)
    {
        int num = 0;
        for (int i = 0; i < parentTransform.childCount; i++)
            if (parentTransform.GetChild(i) == map.transform)
                num = i;
        return num;
    }

    public void SetNarrowGapMoveTime(float setting)
    {
        NarrowGapMoveTime += setting;
    }

    public void SetWideGapMoveTime(float setting)
    {
        WideGapMoveTime += setting;
    }
}
