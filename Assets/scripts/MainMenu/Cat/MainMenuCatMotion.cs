﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuCatMotion : MonoBehaviour
{
    public Sprite walkMotion1, walkMotion2;
    public ClickStage click;
    private Sprite origin;
    public float time;


    private SpriteRenderer[] childSprite = new SpriteRenderer[3];
    private Sprite[] walkSprite = new Sprite[2];

    private bool arrival;
    /// <summary>
    /// 도착할때 사용하는 프로퍼티
    /// false일시 모션을 계속 해주며
    /// true 일시 모션이 끝남
    /// </summary>
    public bool Arrival { get => arrival; set => arrival = value; }

    void Start()
    {
        origin = transform.GetChild(0).GetComponent<SpriteRenderer>().sprite;
        for (int i = 0; i < childSprite.Length; i++)
            childSprite[i] = transform.GetChild(i).GetComponent<SpriteRenderer>();
        walkSprite[0] = walkMotion1;
        walkSprite[1] = walkMotion2;
    }

    public void ChangeMotion(GameObject target)
    {
        if (transform.position.x > target.transform.position.x)
            childSprite[0].flipX = true;

        else
            childSprite[0].flipX = false;

        StartCoroutine(_Motion());
    }

    public void StartMotion(Vector2 destination)
    {
        Arrival = false;

        if (transform.position.x > destination.x)
            childSprite[0].flipX = true;

        else
            childSprite[0].flipX = false;

        StartCoroutine(_Motion());
    }

    private IEnumerator _Motion()
    {
        int i = 0;
        childSprite[1].enabled = false;
        childSprite[2].enabled = false;

        while (!Arrival)
        {
            childSprite[0].sprite = walkSprite[i];
            i++;
            i %= walkSprite.Length;
            yield return new WaitForSeconds(0.25f);
        }
        childSprite[0].sprite = origin;
        childSprite[0].flipX = true;
        childSprite[1].enabled = true;
        childSprite[2].enabled = true;
        yield return null;
    }
}
