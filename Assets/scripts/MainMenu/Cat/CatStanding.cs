﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatStanding : MonoBehaviour
{
    public GameObject cat;
    public GameObject stage;
    public int stageNum;

    private GameObject nowStage;
    // Start is called before the first frame update
    void Start()
    {
        nowStage = stage.transform.GetChild(stageNum).gameObject;
        cat.transform.parent = nowStage.transform;
        cat.transform.localPosition = nowStage.transform.GetChild(0).transform.localPosition;
    }
}
