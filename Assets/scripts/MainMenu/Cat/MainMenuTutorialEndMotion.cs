﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 튜토리얼 끝나고 난 뒤에 실행하는 모션
/// 전에 만들었던 것을 토대로 만들거
/// </summary>
public class MainMenuTutorialEndMotion : MonoBehaviour
{
    /*
    #region 원래 있던 변수들
    public Transform stagePanel;
    public Transform mainCamera;

    // 슬라이드로 스테이지 이동하는 클래스
    public MapDragMove dragMove;
    public ClickStage clickStage;

    // 고양이와 그 안에 있는 클래스들
    private MainMenuCatMotion catMotion;

    // 이건 할 수 없이 Awake에 사용해야 작동할듯
    // 여기 있는 정보로 이동 후 정보 저장해주기
    private CatStartPos catStartPos;

   

    // 화살표 UI
    public Image arrow;
    public ShowCat arrowS;
    public Transform catPos;
    public StageClick SC;
    private CameraMove camMove;
    private bool isCat = false;
    private RectTransform arrowRect;

    public MainMenuFingerMotion finger;
    #endregion
    */

    [Header("드래그 및 터치 컨트롤러")]
    public MainMenuTouchContoller touchController;

    [Header("풀때기 부모")]
    public Transform grace;
    // 풀숲 애니메이션에 구양이 귀만 보이게 하는 마스크
    [Header("풀숲 얼굴 마스크")]
    public GameObject mask;
    [Header("고양이 트랜스폼")]
    public Transform cat;
    // 애니메이션에 꼬리 있으면 엄청 이상해서 중간에 다시 넣어줌
    [Header("꼬리 SpriteRenderer")]
    public SpriteRenderer tail;
    private SpriteRenderer[] catPart = new SpriteRenderer[3];
    [Header("튜토리얼 모션 끝나고 가는 포지션")]
    public Vector2 catTutorialEndPos;
    [Header("풀들 날아가는 모션")]
    public CreateLeaf createLeaf;
    [Header("튜토리얼 인포들")]
    public MainMenuStageNowInfosController stageInfos;
    [Header("스테이지 터치 컨트롤러(고양이 움직일때 사용)")]
    public MainMenuStageTouchController stageController;

    public MainMenuFingerMotion finger;

    void Awake()
    {
        // 여기서 빼주는 이유는 손가락 스프라이트도 있어서 그럼
        for (int i = 0; i < cat.transform.childCount - 1; i++)
            catPart[i] = cat.transform.GetChild(i).GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        int viewOpening = PlayerPrefs.GetInt("OpeningStart", 1);

        if(viewOpening == 1)
        {
            StartCoroutine(_MoveToStage());
            PlayerPrefs.SetInt("OpeningStart", 0);
            stageInfos.SetNonCat();
        }

        else
        {
            stageInfos.SetStartPosition();
        }
    }

    public void SetMotionSetting()
    {
        touchController.SetTutorialMotionCamera();
    }

    private void SetCatPart(bool enabled)
    {
        for (int i = 0; i < catPart.Length; i++)
            catPart[i].enabled = enabled;
    }

    // 연출 하는 코루틴
    /*
     * 풀때기 움찔움찔(지금보다 더)
     * -> 고양이 귀 얼굴만 살짝 보임
     * -> 다시 움찔움찔(지금보다 더)
     * -> 고양이 앞으로 오게 하기 (꼬리 보이게 만들기)
     * 그럼 밑에 Action()코루틴은 여기까지 구현하면 댐 때앰
     * -> 이동
     */
    private int random;
    private IEnumerator _PlayAction()
    {
        random = Random.Range(0, 100);
        Transform left = grace.GetChild(0);
        Transform right = grace.GetChild(1);

        SetCatPart(false);
        yield return new WaitForSeconds(2.0f);
        yield return StartCoroutine(_ShakeGrace(new Transform[2] { left, right }, null));

        for (int i = 0; i < 2; i++)
        {
            catPart[i].enabled = true;
            catPart[i].maskInteraction = SpriteMaskInteraction.VisibleOutsideMask;
        }
        cat.transform.position = catTutorialEndPos;
        yield return new WaitForSeconds(1.5f);
        yield return StartCoroutine(_ShakeGrace(new Transform[2] { left, right }, cat.transform));
        catPart[0].maskInteraction = SpriteMaskInteraction.None;
        catPart[1].maskInteraction = SpriteMaskInteraction.None;
        SetCatPart(true);
        yield return new WaitForSeconds(1.0f);
        mask.SetActive(false);
        // 대충 2번 풀이 흔드는 명령어 (흔들고 쉬고 흔들고)
        yield return new WaitForSeconds(0.75f);
        yield break;

    }

    private IEnumerator _ShakeTransform(Transform obj)
    {
        for (int i = 0; i < 2; i++)
        {
            obj.DOShakeRotation(0.7f, 5, 5, 45);
            obj.DOShakePosition(0.7f, 0.1f, 10, 30);
            yield return new WaitForSeconds(0.7f);
            yield return new WaitForSeconds(0.25f);
        }
        yield break;
    }

    // 풀때기 흔들거리는 함수
    private IEnumerator _ShakeGrace(Transform[] obj, Transform single)
    {
        if (single != null)
            StartCoroutine(_ShakeTransform(single));
        for (int i = 0; i < 2; i++)
        {
            for (int a = 0; a < obj.Length; a++)
            {
                int random = Random.Range(2, 5);
                obj[a].DOShakeRotation(0.7f, 15, 10, 45);
                obj[a].DOShakePosition(0.7f, 0.25f, 30, 30);

                for (int b = 0; b < random; b++)
                    createLeaf.SendMessage("Create", random, SendMessageOptions.DontRequireReceiver);
            }
            yield return new WaitForSeconds(0.7f);
            yield return new WaitForSeconds(0.25f);
        }
        yield break;
    }

    private IEnumerator _MoveToStage()
    {
        SetMotionSetting();
        finger.gameObject.SetActive(false);
        finger.GetComponent<SpriteRenderer>().enabled = false;
        yield return StartCoroutine(_PlayAction());
        //mainCamera.position = transform.position;
        //cat.transform.position = catPos.position;
        //isCat = true;        
        yield return new WaitForSeconds(0.5f);

        MainMenuStageNowInfo stage1Info = stageInfos.infos[1];
        SettingStageButton settingBtn = stage1Info.GetComponent<SettingStageButton>();

        stageController.TutorialEndStageMove(stage1Info.transform.position, settingBtn, stage1Info);

        PlayerPrefs.SetInt("TutorialEnd", 0);
        finger.GetComponent<SpriteRenderer>().enabled = true;
        yield break;
    }
}
