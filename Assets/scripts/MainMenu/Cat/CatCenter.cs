﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatCenter : MonoBehaviour
{
    public GameObject mainCamera;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.transform.position = new Vector2(mainCamera.transform.position.x - 2.0f, -2.0f);
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position = new Vector3(mainCamera.transform.position.x-2.0f, gameObject.transform.position.y, -2);
    }
}
