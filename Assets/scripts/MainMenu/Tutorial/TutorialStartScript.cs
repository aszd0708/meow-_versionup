﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialStartScript : MonoBehaviour
{
    public RectTransform tutorialPanel;
    public ClickStage stageMove;

    public MapDragMove drag;

    private void Start()
    {
        tutorialPanel.localScale = new Vector2(0, 0);
    }

    public void CreateTutorialPanel()
    {
        drag.enabled = false;
        tutorialPanel.DOScale(new Vector3(1, 1, 0), 0.5f).SetEase(Ease.OutElastic);
    }

    public void DestPanel()
    {
        drag.enabled = true;
        stageMove.catArrival = true;
        tutorialPanel.DOScale(new Vector2(0, 0), 0.5f).SetEase(Ease.Linear);
    }
}
