﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

/*
  2020-01-14
  튜토리얼을 나간 뒤 나오는 애니메이션에 관한 클래스
  수풀에 고양이가 나와 첫번째 스테이지 버튼으로 가게 해주는 클래스
 */

public class TutorialEnd : MonoBehaviour
{
    public Transform stagePanel;
    public Transform mainCamera;

    // 슬라이드로 스테이지 이동하는 클래스
    public MapDragMove dragMove;
    public ClickStage clickStage;

    // 고양이와 그 안에 있는 클래스들
    public GameObject cat;
    private MainMenuCatMotion catMotion;

    // 이건 할 수 없이 Awake에 사용해야 작동할듯
    // 여기 있는 정보로 이동 후 정보 저장해주기
    private CatStartPos catStartPos;

    // 풀숲 애니메이션 넣으려 해서 넣은 변수
    public Transform grace;
    // 풀숲 애니메이션에 구양이 귀만 보이게 하는 마스크
    public SpriteMask mask;
    // 애니메이션에 꼬리 있으면 엄청 이상해서 중간에 다시 넣어줌
    public SpriteRenderer tail;
    private SpriteRenderer[] catPart = new SpriteRenderer[3];
    public Vector2 catTutorialEndPos;
    private CreateLeaf createLeaf;

    // 화살표 UI
    public Image arrow;
    public ShowCat arrowS;
    public Transform catPos;
    public StageClick SC;
    private CameraMove camMove;
    private bool isCat = false;
    private RectTransform arrowRect;

    public MainMenuFingerMotion finger;

    void Awake()
    {
        createLeaf = GetComponent<CreateLeaf>();
        camMove = mainCamera.GetComponent<CameraMove>();
        catMotion = cat.GetComponent<MainMenuCatMotion>();
        catStartPos = cat.GetComponent<CatStartPos>();
        catStartPos.enabled = false;
        arrowRect = arrow.GetComponent<RectTransform>();
        // 여기서 빼주는 이유는 손가락 스프라이트도 있어서 그럼
        for (int i = 0; i < cat.transform.childCount - 1; i++)
            catPart[i] = cat.transform.GetChild(i).GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        PlayerPrefs.SetString("CatPos", "Stage1 Park");
        int end = PlayerPrefs.GetInt("TutorialEnd", 1);

        if (end == 1)
        {
            finger.FingerMotionController(true);
            Debug.Log("튜토리얼끝 실행" + end);
            SettingState(false);
            StartCoroutine(MoveToStage());
        }

        else
        {
            finger.FingerMotionController(false);
            //grace.gameObject.SetActive(false);
            Debug.Log("튜토리얼끝 안실행" + end);
            enabled = false;
            catStartPos.enabled = true;
        }
    }

    private void SettingState(bool stage)
    {
        arrow.enabled = stage;
        //catStartPos.enabled = stage;
        dragMove.enabled = stage;
        clickStage.enabled = stage;
        //cat.SetActive(stage);
    }

    private void SetCatPart(bool enabled)
    {
        for (int i = 0; i < catPart.Length; i++)
            catPart[i].enabled = enabled;
    }

    // 연출 하는 코루틴
    /*
     * 풀때기 움찔움찔(지금보다 더)
     * -> 고양이 귀 얼굴만 살짝 보임
     * -> 다시 움찔움찔(지금보다 더)
     * -> 고양이 앞으로 오게 하기 (꼬리 보이게 만들기)
     * 그럼 밑에 Action()코루틴은 여기까지 구현하면 댐 때앰
     * -> 이동
     */
    private int random;
    private IEnumerator Action()
    {
        random = Random.Range(0, 100);
        arrowS.enabled = false;
        arrowRect.localScale = new Vector2(0, 0);
        Transform left = grace.GetChild(0);
        Transform right = grace.GetChild(1);

        stagePanel.position = new Vector3(stagePanel.position.x, stagePanel.position.y);
        SetCatPart(false);
        yield return new WaitForSeconds(2.0f);
        yield return StartCoroutine(ShakeMove(new Transform[2] { left, right }, null));

        for (int i = 0; i < 2; i++)
        {
            catPart[i].enabled = true;
            catPart[i].maskInteraction = SpriteMaskInteraction.VisibleOutsideMask;
        }
        cat.transform.position = catTutorialEndPos;
        yield return new WaitForSeconds(1.5f);
        yield return StartCoroutine(ShakeMove(new Transform[2] { left, right }, cat.transform));
        catPart[0].maskInteraction = SpriteMaskInteraction.None;
        catPart[1].maskInteraction = SpriteMaskInteraction.None;
        SetCatPart(true);
        yield return new WaitForSeconds(1.0f);
        mask.gameObject.SetActive(false);
        // 대충 2번 풀이 흔드는 명령어 (흔들고 쉬고 흔들고)
        arrowS.enabled = true;
        yield return new WaitForSeconds(0.75f);

        arrowRect.localScale = new Vector2(1, 1);
        yield break;

    }

    private IEnumerator ShakeMove(Transform obj)
    {
        for (int i = 0; i < 2; i++)
        {
            obj.DOShakeRotation(0.7f, 5, 5, 45);
            obj.DOShakePosition(0.7f, 0.1f, 10, 30);
            yield return new WaitForSeconds(0.7f);
            yield return new WaitForSeconds(0.25f);
        }
        yield break;
    }

    // 풀때기 흔들거리는 함수
    private IEnumerator ShakeMove(Transform[] obj, Transform single)
    {
        if (single != null)
            StartCoroutine(ShakeMove(single));
        for (int i = 0; i < 2; i++)
        {
            for (int a = 0; a < obj.Length; a++)
            {
                int random = Random.Range(2, 5);
                obj[a].DOShakeRotation(0.7f, 15, 10, 45);
                obj[a].DOShakePosition(0.7f, 0.25f, 30, 30);

                for (int b = 0; b < random; b++)
                    createLeaf.SendMessage("Create", random, SendMessageOptions.DontRequireReceiver);
            }
            yield return new WaitForSeconds(0.7f);
            yield return new WaitForSeconds(0.25f);
        }
        yield break;
    }

    private IEnumerator MoveToStage()
    {
        yield return StartCoroutine(Action());
        //mainCamera.position = transform.position;
        cat.SetActive(true);
        cat.transform.SetParent(null);
        //cat.transform.position = catPos.position;
        //isCat = true;        
        yield return new WaitForSeconds(0.5f);
        Transform stage1Pos = stagePanel.GetChild(1);

        cat.transform.SetParent(stage1Pos);
        cat.transform.DOLocalMove(new Vector2(-0.7f, 0.4f), catMotion.time);

        catMotion.Arrival = false;
        catMotion.ChangeMotion(stage1Pos.gameObject);
        stagePanel.DOMoveX(-stage1Pos.position.x, 1.5f);
        //stagePanel.DOMoveX(-stage1Pos.position.x, catMotion.time);
        //camMove.touch = false;
        SC.state = StageClick.State.nonCat;
        yield return new WaitForSeconds(catMotion.time);
        finger.FingerMotionController(false);
        stage1Pos.GetComponent<StageClick>().state = StageClick.State.cat;
        catMotion.Arrival = true;
        isCat = true;

        // 끝날 때 튜토리얼에서 나왔다는 프리퍼런스 수정 
        // 모든 명령어 원 상태로 돌려놓기
        SettingState(true);

        PlayerPrefs.SetInt("TutorialEnd", 0);
        yield break;
        //stagePanel.position = new Vector2(-stage1Pos.position.x, stagePanel.position.y);
    }
}
