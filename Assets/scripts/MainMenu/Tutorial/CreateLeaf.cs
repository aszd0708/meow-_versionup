﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-01-27
 * 나뭇잎이 생성 된 후 연출을 재생시키는 스크립트
 */

public class CreateLeaf : MonoBehaviour
{
    public GameObject leafObj;
    public List<Sprite> sprites = new List<Sprite>();

    public List<Sprite> easterSprites = new List<Sprite>();

    private void Create(int random)
    {
        GameObject leaf = Instantiate(leafObj);
        SpriteRenderer leafSprite = leaf.GetComponent<SpriteRenderer>();

        // 이스터에그
        // 5퍼센트 확률로 풀 말고 이상한게 마아아악 나옴
        //int random = Random.Range(0, 100);
        if (random >= 95)
            leafSprite.sprite = easterSprites[Random.Range(0, easterSprites.Count)];
        else
            leafSprite.sprite = sprites[Random.Range(0, sprites.Count)];

        leafSprite.flipX = SetFlip();
        leafSprite.flipY = SetFlip();

        leaf.transform.SetParent(transform);
        leaf.transform.localPosition = Vector3.zero;

        leaf.transform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));

        leaf.SendMessage("CreateMotionStarter", SetValue(Random.Range(2f, 3f), 1.5f), SendMessageOptions.DontRequireReceiver);
        return;
    }

    private float[] SetValue(float distance, float time)
    {    
        return new float[2] { distance, time };
    }

    private bool SetFlip()
    {
        int random = Random.Range(0, 2);
        switch(random)
        {
            case 0: return false;
            case 1: return true; 
            default: return true;
        }
    }
}
