﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-01-26 
 * 나뭇잎 움직이는 연출 하는 스크립트.
 */

public class LeafMoving : MonoBehaviour
{
    public float time;
    public int count;
    private float[] xPos = new float[2];
    private List<float> yPos = new List<float>();
    private SpriteRenderer sprite;

    void Awake()
    {
        sprite = GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        //SetMovePos(new float[2] { 0, -1.7f }, -5.5f, 4);
        //StartCoroutine(Motion());
    }

    // 여기서부터
    void SetMovePos(float[] moveX, float moveY, int _count)
    {
        count = _count;
        yPos.Add(moveY / count);
        for (int i = 1; i < count; i++)
        {
            yPos.Add(yPos[i - 1] + yPos[0]);
        }

        xPos[0] = moveX[0]; xPos[1] = moveX[1];
    }

    private IEnumerator Motion()
    {
        int index = Random.Range(0, 1);
        float moveY = yPos[0] / 2;
        transform.position = new Vector3((xPos[0] + xPos[1]) / 2, -2.5f);
        transform.DOMoveX(xPos[index], time/2).SetEase(Ease.OutExpo);
        transform.DOMoveY(moveY - 2.5f, time / 2).SetEase(Ease.OutBack);
        yield return new WaitForSeconds(time / 2);
        for(int i = 0; i < count; i++)
        {
            index++;
            index %= xPos.Length;
            transform.DOMoveX(xPos[index], time).SetEase(Ease.OutExpo);
            transform.DOMoveY(yPos[i] + moveY - 2.5f, time).SetEase(Ease.OutBack);
            yield return new WaitForSeconds(time);
        }
        yield break;
    }
    // 여기까지 나뭇잎 살랑살랑

    private void CreateMotionStarter(float[] temp)
    {
        float distance = temp[0];
        float time = temp[1];
        StartCoroutine(CreateMotion(distance, time));
    }

    private IEnumerator CreateMotion(float distance, float time)
    {
        Vector2 toDirPos = AnlgeToDir(Random.Range(0.0f, 180f)) * distance;
        transform.DOLocalMove(toDirPos, time).SetEase(Ease.OutQuart);
        yield return new WaitForSeconds(time - 1f);
        float destination = Random.Range(0, -2.0f);
        time = Random.Range(1.0f, 1.5f);
        transform.DOLocalMoveY(destination, time).SetEase(Ease.Linear);
        yield return new WaitForSeconds(time - 0.5f);
        sprite.DOFade(0, 0.5f);
        yield return new WaitForSeconds(0.6f);
        gameObject.SetActive(false);
    }

    private Vector2 AnlgeToDir(float angle)
    {
        Vector2 dir;
        float x = Mathf.Sin(angle);
        float y = Mathf.Cos(angle);
        y = Mathf.Abs(y);
        dir = new Vector2(x, y);
        return dir;
    }
}
