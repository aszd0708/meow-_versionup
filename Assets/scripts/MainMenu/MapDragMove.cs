﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapDragMove : MonoBehaviour
{
    public float smooth;
    public float checkTime = -0.5f;
    private float timeSpan;
    private bool touch;
    private Vector2 downTouch;
    private Vector2 nowTouch;
    private Vector2 childPos;
    private float moveDis = 0;
    private float time = 0;
    private float distance = 0;
    private float speed = 0;

    private enum State
    {
        left, right, idle
    }

    private State state = State.idle;

    void Start()
    {
        childPos = transform.GetChild(transform.childCount - 1).transform.position;
    }

    void Update()
    {
        transform.position = new Vector2(Mathf.Clamp(transform.position.x, -childPos.x, 0),0);
        
        if (Input.GetMouseButtonDown(0))
        {
            time = 0;
            timeSpan = Time.time;
            downTouch = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            distance = downTouch.x;
            touch = false;
            moveDis = 0;
            state = State.idle;
        }

        else if (Input.GetMouseButton(0))
        {
            time += Time.deltaTime;
            nowTouch = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            gameObject.transform.position = new Vector2((gameObject.transform.position.x) - (downTouch.x - nowTouch.x), gameObject.transform.position.y);
            moveDis += downTouch.x - nowTouch.x;
            downTouch = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }

        else if(Input.GetMouseButtonUp(0))
        {
            timeSpan -= Time.time;
            if (timeSpan >= checkTime)
            {
                touch = true;
                distance = nowTouch.x - distance;
                speed = distance / time;
                if (speed > 0)
                    state = State.right;
                else
                    state = State.left;
                VibratorControllor.ObjTouchVib();
            }
        }

        if(touch)
        {
            transform.position = new Vector2(transform.position.x + Mathf.Lerp(0, speed, Time.deltaTime), transform.position.y);
            
            if(state == State.right)
            {
                speed -= Time.deltaTime * smooth;
                if(speed <= 0)
                {
                    speed = 0;
                    state = State.idle;
                }
            }

            else if(state == State.left)
            {
                speed += Time.deltaTime * smooth;
                if (speed >= 0)
                {
                    speed = 0;
                    state = State.idle;
                }
            }

            if (transform.position.x > 0)
                transform.position = new Vector2(0, 0);
            if (transform.position.x < -childPos.x)
                transform.position = new Vector2(-childPos.x, 0);
        }
    }
}
