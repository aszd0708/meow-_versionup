﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapMoveTimeController : MonoBehaviour
{
    public Text narrowTimeText;
    public Text wideTimeText;

    private List<CatMove> moveTimeScript = new List<CatMove>();
    // Start is called before the first frame update
    void Start()
    {
        Invoke("Setting", 0.5f);
    }

    private void Setting()
    {
        for (int i = 0; i < transform.childCount; i++)
            moveTimeScript.Add(transform.GetChild(i).GetComponent<CatMove>());
        NarrowMoveTimeSetting(0);
        WideMoveTimeSetting(0);
    }

    public void NarrowMoveTimeSetting(float time)
    {
        for (int i = 0; i < moveTimeScript.Count; i++)
            moveTimeScript[i].NarrowGapMoveTime += time;
        narrowTimeText.text = moveTimeScript[0].NarrowGapMoveTime + "";
    }
    public void WideMoveTimeSetting(float time)
    {
        for (int i = 0; i < moveTimeScript.Count; i++)
            moveTimeScript[i].WideGapMoveTime += time;
        wideTimeText.text = moveTimeScript[0].WideGapMoveTime + "";
    }
}
