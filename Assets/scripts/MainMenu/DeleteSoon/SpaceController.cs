﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpaceController : MonoBehaviour
{
    public VerticalLayoutGroup space;

    public Text spaceValueText;

    private void Start()
    {
        spaceValueText.text = space.spacing + "";
    }

    public void SettingSpace(float value)
    {
        space.spacing += value;
        if (space.spacing <= 0)
            space.spacing = 0;
        spaceValueText.text = space.spacing + "";
    }
}
