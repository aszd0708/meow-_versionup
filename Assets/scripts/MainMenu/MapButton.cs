﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapButton : MonoBehaviour
{
    public Sprite mapSprite;
    public Vector2 catPosition;
    public string sceneName;
    public string mapName;
    public bool mapLock;
    public int num;
    public GameObject cat;
    public TextMesh text;

    public void Create()
    {
        gameObject.GetComponent<SpriteRenderer>().sprite = mapSprite;
        gameObject.transform.GetChild(0).transform.localPosition = catPosition;
        gameObject.GetComponent<CatMove>().cat = cat;
        if (mapLock)
        {
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
            gameObject.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0);
        }
        text.text = mapName;
    }
}
