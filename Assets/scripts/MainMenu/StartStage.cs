﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartStage : MonoBehaviour
{
    public enum Kind
    {
        STAGE, GATCHA, TUTORIAL
    }
    public Kind KIND;
    private int gatchaNum;

    public int GatchaNum { get => gatchaNum; set => gatchaNum = value; }

    public void ClickStage()
    {
        switch (KIND)
        {
            case Kind.GATCHA:
                // 여기서 저장 해 준 뒤 
                PlayerPrefs.SetInt("GatchaStageIndex", GatchaNum);
                //SceneManager.LoadScene(gameObject.GetComponent<MapButton>().sceneName);
                StartCoroutine(StageStart());
                break;
            case Kind.STAGE:
                //SceneManager.LoadScene(gameObject.GetComponent<MapButton>().sceneName);
                StartCoroutine(StageStart());
                break;
            case Kind.TUTORIAL:
                // 대충 두투리얼 들어가겠냐는 표시
                GetComponent<TutorialStartScript>().CreateTutorialPanel();
                break;
        }
    }

    private IEnumerator StageStart()
    {
        PlayerPrefs.SetString("prevScene", GetComponent<MapButton>().sceneName);
        Debug.Log("씬 이름: " + PlayerPrefs.GetString("prevScene"));
        //yield return new WaitForSeconds(2.0f);
        if (KIND == Kind.GATCHA)
        {
            SceneChangeManager.Instance.ChangeScene("Gatcha");
            int stageIndex = PlayerPrefs.GetInt("GatchaStageIndex", 1) - 1;
            Debug.Log("가차 인덱스 : " + stageIndex);
            ///SceneManager.LoadScene("GatCha");
        }
        else
        {
            SceneChangeManager.Instance.ChangeScene(GetComponent<MapButton>().sceneName);
            //SceneManager.LoadScene();
        }
        yield break;
    }

    public void GoTutorial()
    {
        StartCoroutine(StageStart());
    }
}
