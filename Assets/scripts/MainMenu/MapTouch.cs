﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MapTouch : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(touchPos, Vector2.zero, 0.0f);

            if (hit.collider != null)
            {
                Debug.Log(hit.collider.GetComponent<MapButton>().mapName);
                GameObject interMap = hit.collider.gameObject;

                if (interMap.tag == "mapButton")
                {
                    Debug.Log(interMap.GetComponent<MapButton>().mapName);
                    //Debug.Log(interMap.GetComponent<MapButton>().mapName);
                    //iTween.MoveTo(gameObject, iTween.Hash("x", transform.TransformDirection(interMap.transform.GetChild(0).transform.position).x, "time", 3, "easeType", iTween.EaseType.linear));
                    interMap.GetComponent<CatMove>().OnClick();
                }
            }
        }
    }
}
