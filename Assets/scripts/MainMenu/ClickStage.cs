﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ClickStage : MonoBehaviour
{
    public float checkTime = -0.5f;
    private float timeSpan = 0.5f;
    public bool catArrival;
    public Transform stage;
    private Vector3 movePos;

    public CameraMove camMove;

    // Update is called once per frame
    void Start()
    {
        catArrival = true;
        timeSpan = Time.time;
    }

    void Update()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;

        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                if (EventSystem.current.IsPointerOverGameObject(i))
                    return;
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            timeSpan = Time.time;
            movePos = stage.position;
        }

        else if (Input.GetMouseButtonUp(0))
        {
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(touchPos, Vector2.zero, 0.0f);

            timeSpan -= Time.time;

            if (Mathf.Abs(movePos.x - stage.position.x) > 0.5f)
                return;
            
            if (timeSpan >= checkTime)
            {
                if (hit.collider != null)
                {
                    if (catArrival)
                    {
                        //hit.collider.GetComponent<StageClick>().OnClick();
                        catArrival = false;
                        StartCoroutine("Jelly", hit.collider.gameObject);
                    }
                    else
                        return;
                }
            }
        }

        if (!catArrival)
            camMove.selelct = true;
        else if (catArrival)
            camMove.selelct = false;
    }

    private IEnumerator Jelly(GameObject hitObject)
    {
        Vector3 origin = hitObject.transform.localScale;
        hitObject.transform.DOScale(new Vector3(origin.x - 0.1f, origin.y - 0.1f, origin.z), 0.1f);
        yield return new WaitForSeconds(0.1f);
        hitObject.transform.DOScale(new Vector3(origin.x, origin.y, origin.z), 0.8f).SetEase(Ease.OutElastic);
        yield return new WaitForSeconds(0.3f);
        hitObject.GetComponent<StageClick>().OnClick();
    }
}
