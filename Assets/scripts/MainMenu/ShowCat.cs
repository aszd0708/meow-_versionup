﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowCat : MonoBehaviour
{
    public Transform cameraPos;
    public GameObject arrow;
    private Vector3 arrowPos;
    private bool visible;
    private Image arrowImg;
    private RectTransform arrowTranse;

    void Start()
    {
        arrowPos = arrow.transform.position;
        arrowImg = arrow.GetComponent<Image>();
        arrowTranse = arrow.GetComponent<RectTransform>();
    }

    void Update()
    {
        if (cameraPos.position.x - transform.position.x > 11)
        {
            // 왼쪽
            arrowTranse.anchorMax = new Vector2(0.1f, 0.5f);
            arrowTranse.anchorMin = new Vector2(0, 0.5f);
            float dis = CameraDis();
            arrowImg.enabled = true;
            
            arrowTranse.localRotation = Quaternion.Euler(0, 0, 0);
            //arrow.GetComponent<SpriteRenderer>().enabled = true;
            //
            //arrow.GetComponent<SpriteRenderer>().flipX = false;
            //arrow.transform.position = arrowPos;
        }

        else if (cameraPos.position.x - transform.position.x < -10)
        {
            // 오른쪽
            arrowTranse.anchorMax = new Vector2(0f, 0.5f);
            arrowTranse.anchorMin = new Vector2(0.9f, 0.5f);
            float dis = CameraDis();
            arrowImg.enabled = true;
            
            arrowTranse.localRotation = Quaternion.Euler(0, 180, 0);
            //arrow.GetComponent<SpriteRenderer>().enabled = true;
            //
            //arrow.GetComponent<SpriteRenderer>().flipX = true;
            //arrow.transform.position = new Vector3(-arrowPos.x, arrowPos.y, arrowPos.z);
        }

        else
            arrow.GetComponent<Image>().enabled = false;
            //arrow.GetComponent<SpriteRenderer>().enabled = false;
    }

    void OnBecameInvisible()
    {
        visible = false;
    }

    void OnBecameVisible()
    {
        visible = true;
    }

    private float CameraDis()
    {
        float dis = cameraPos.position.x - transform.position.x;
        return dis;
    }
}
