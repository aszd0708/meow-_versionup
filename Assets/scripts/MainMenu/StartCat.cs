﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartCat : MonoBehaviour
{
    public GameObject cat;
    public GameObject stage;
    public int startPos = 0;
    // Start is called before the first frame update
    void Start()
    {
        cat.transform.SetParent(stage.transform.GetChild(startPos).transform);
        stage.transform.GetChild(startPos).GetComponent<StageClick>().state = StageClick.State.cat;
        cat.transform.localPosition = stage.transform.GetChild(startPos).transform.GetChild(0).transform.localPosition;
    }
}
