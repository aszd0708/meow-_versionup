﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageClick : MonoBehaviour
{
    // Start is called before the first frame update
    private StartStage stage;
    private CatMove catMove;
    public enum State
    {
        nonCat, cat, moving
    }

    public State state = State.nonCat;

    private void Awake()
    {
        stage = GetComponent<StartStage>();
        catMove = GetComponent<CatMove>();
    }

    public void OnClick()
    {
        if(state == State.nonCat)
        {
            catMove.Move(gameObject);
        }

        else if(state == State.cat)
        {
            stage.ClickStage();
        }

        else if(state == State.moving)
        {
            return;
        }
    }

    private IEnumerator ChangeScene()
    {
        yield return new WaitForSeconds(2.0f);
        stage.ClickStage();
    }
}
