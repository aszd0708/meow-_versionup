﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject cat;
    public float speed;
    
    void Start()
    {
        transform.position = new Vector3(cat.transform.position.x+0.7f, gameObject.transform.position.y, gameObject.transform.position.z);
    }

    void Update()
    {
        Vector3 position = new Vector3(cat.transform.position.x + 0.7f, gameObject.transform.position.y, gameObject.transform.position.z);
        transform.position = Vector3.Lerp(transform.position, position, Time.deltaTime * speed);
    }
}
