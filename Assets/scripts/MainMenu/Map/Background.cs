﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{
    public float speed;
    public float minX, maxX;
    public float minY, maxY;

    void Awake()
    {
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, 3.0f);
    }

    void Update()
    {
        // if (transform.localPosition.x < minX)
        //     transform.localPosition = new Vector3(maxX, transform.localPosition.y, transform.localPosition.z);
        // else if (transform.localPosition.x > maxX)
        //     transform.localPosition = new Vector3(minX, transform.localPosition.y, transform.localPosition.z);

        if (transform.position.y < minY)
            transform.position = new Vector3(transform.position.x, maxY, transform.position.z);
        else if (transform.position.y > maxY)
            transform.position = new Vector3(transform.position.x, minY, transform.position.z);

        transform.position = new Vector3(transform.position.x, transform.position.y + Time.deltaTime * speed, transform.position.z);
    }
}
