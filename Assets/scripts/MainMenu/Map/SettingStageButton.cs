﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StageKind
{
    TUTORIAL, STAGE, GATCHA
}

[System.Serializable]
public struct StageButtonInfo
{
    public enum MapKind
    {
        SPRITE, PREFEB
    }

    [Header("맵 이미지")]
    public Sprite mapSprite;
    [Header("만약 오픈이 안된 스테이지 라면")]
    public Sprite nonOpenStageSprite;
    [Header("맵 프리팹")]
    public GameObject mapPreb;

    [Header("프리팹인지 스프라이트인지")]
    public MapKind MAPKIND;

    [Header("스테이지 종류")]
    public StageKind STAGE;
    [Header("고양이 있는 포지션")]
    public Vector2 catPosition;
    [Header("변경할 씬 이름")]
    public string sceneName;
    [Header("지금 이 맵의 이름(한글로 작성)")]
    public string mapName;
    [Header("이 맵이 오픈 되었나...?")]
    public bool isOpen;
    [Header("고양이 몇마리 수집했는지를 알려주는 처음과 끝")]
    public int start;
    public int end;

    public int[] animalIndex;

    [Header("만약 가챠일경우 몇번째 가챠인지")]
    public int gachaNumber;

    [Header("스테이지 인덱스")]
    public int stageIndex;
}

public class SettingStageButton : MonoBehaviour
{
    #region 스테이지 가챠 둘다 필요한 변수들 (public)
    [Header("스테이지 버튼 스프라이트 랜더러")]
    public SpriteRenderer stageButtonSpriteRenderer;

    [Header("스테이지 이름 넣을 텍스트")]
    public TextMesh stageNameText;

    [Header("터치 막을 콜라이더")]
    public Collider2D boxCol;

    #endregion

    #region 스테이지 가챠 둘다 필요한 변수들 (private, property)
    [Header("고양이 서있는 포지션")]
    private Vector2 catStandPose;

    [SerializeField]
    [Header("씬 이름 위에 있는 구조체 받아서 사용할거")]
    private string stageSceneName;


    [Header("튜토리얼 스테이지 가챠 세개중에 한개 설정")]
    private StageKind stageKind;
    public string StageSceneName { get => stageSceneName; set => stageSceneName = value; }
    public Vector2 CatStandPose { get => catStandPose; set => catStandPose = value; }
    public StageKind StageKind { get => stageKind; set => stageKind = value; }
    #endregion

    [Header("가챠는 빼기 위해 넣음")]
    public GameObject textObj;

    #region 수집 카운트
    [Header("콜렉트 한 게임 오브젝트")]
    public GameObject collcetObj;
    [Header("콜렉트 안한 게임 오브젝트")]
    public GameObject nonCollectObj;
    [Header("동물 수집 카운트 위치")]
    public Transform animalCollectCountTrasnform;
    [Header("각 오브젝트 간격")]
    public float colDis;
    #endregion

    /* 지금 고양이가 이 스테이지에 있는지 확인하기 위한 부울값 */
    private bool haveCat;
    public bool HaveCat { get => haveCat; set => haveCat = value; }

    private int stageIndex;
    /// <summary>
    /// 이 오브젝트의 스테이지 인덱스
    /// </summary>
    public int StageIndex { get => stageIndex; set => stageIndex = value; }

    [SerializeField]
    [Header("가챠 인덱스")]
    private int gatchaIndex;
    public int GatchaIndex { get => gatchaIndex; set => gatchaIndex = value; }



    /// <summary>
    /// 버튼 만들어주고 스프라이트 밑에 생성해주는 함수
    /// </summary>
    /// <param name="setinfo"></param>
    /// <param name="startIndex"></param>
    /// <param name="endIndex"></param>
    /// <param name="collectCount"></param>
    public void CreateStageButton(StageButtonInfo setinfo, int[] animalIndex, int collectCount)
    {
        SetStageButton(setinfo, animalIndex);
        if (setinfo.STAGE == StageKind.GATCHA)
            return;
        SetCollectCountSprite(animalIndex, collectCount);
    }

    /// <summary>
    /// 스테이지 버튼 세팅 함수
    /// </summary>
    /// <param name="setInfo"></param>
    private void SetStageButton(StageButtonInfo setInfo, int[] animalIndex)
    {
        // 오픈 됐는지 안됐는지 확인해서 스프라이트 넣음
        if (setInfo.isOpen)
        {
            stageButtonSpriteRenderer.sprite = setInfo.mapSprite;
            stageNameText.text = setInfo.mapName;
            boxCol.enabled = true;
        }
        else
        {
            stageButtonSpriteRenderer.sprite = setInfo.nonOpenStageSprite;
            stageNameText.text = string.Format("???");
            boxCol.enabled = false;
        }
        StageIndex = setInfo.stageIndex;
        CatStandPose = setInfo.catPosition;
        StageSceneName = setInfo.sceneName;
        StageKind = setInfo.STAGE;
        if (StageKind == StageKind.GATCHA)
        {
            textObj.SetActive(false);
            bool bIsOpen = false;
            for(int i = 0; i < animalIndex.Length; i++)
            {
                if(SaveDataManager.Instance.Data.Animals[animalIndex[i]].IsCollect)
                {
                    bIsOpen = true;
                    break;
                }
            }

            if(bIsOpen == false)
            {
                stageButtonSpriteRenderer.sprite = setInfo.nonOpenStageSprite;
                boxCol.enabled = false;
            }
            GatchaIndex = setInfo.gachaNumber;
        }
    }

    /// <summary>
    /// 스테이지 밑에 콜렉트 된 고양이 갯수 나타내는 함수
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <param name="collectCount"></param>
    private void SetCollectCountSprite(int[] animalIndex, int collectCount)
    {
        if (animalIndex.Length <= 0)
            return;
        float distance = 0;
        int count = animalIndex.Length;
        int fiveCount = 0;
        GameObject[] calPos;
        if (count < 5)
            calPos = new GameObject[count % 5 + 1];
        else
            calPos = new GameObject[5];

        for (int i = 0, num = 0; i < animalIndex.Length; i++, num++, fiveCount++)
        {
            GameObject collection = null;
            if(SaveDataManager.Instance.Data.Animals[animalIndex[i]].IsCollect)
                collection = Instantiate(collcetObj);
            else
                collection = Instantiate(nonCollectObj);

            collection.transform.SetParent(animalCollectCountTrasnform);

            distance = num % 5 * colDis * 2;

            collection.transform.localPosition = new Vector3(distance, -(num / 5) * colDis * 2 - 1, -5);
            calPos[fiveCount] = collection;
            if (fiveCount >= 4)
            {
                float midDis = colDis * 2 * (fiveCount) / 2;
                for (int a = 0; a < calPos.Length; a++)
                {
                    Transform pose = calPos[a].transform;
                    pose.localPosition = new Vector3(pose.localPosition.x - midDis, pose.localPosition.y, pose.localPosition.z);
                }
                fiveCount = -1;
            }
        }

        if (count % 5 != 0)
        {
            float midDis;
            midDis = colDis * 2 * (fiveCount - 1) / 2;
            for (int i = 0; i < fiveCount; i++)
            {
                Transform pose = calPos[i].transform;
                calPos[i].transform.localPosition = new Vector3(pose.localPosition.x - midDis, pose.localPosition.y);
            }
        }
    }

    public void ChangeScene()
    {

    }
}
