﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 버튼 생성하는 클래스
/// </summary>
public class StageButtonCreater : MonoBehaviour
{
    [Header("스테이지 오브젝트")]
    public GameObject stageButtonObject;

    [Header("스테이지 버튼 인포들")]
    public StageButtonInfo[] stageButtonsInfo;

    [Header("스테이지 포지션들")]
    public float createXPose;
    public float createYPose;
    public float gatchaXPose;
    public float stageXPose;

    [Header("스테이지 부모")]
    public Transform stagesParent;

    private List<AnimalData> animalData;
    private PlayerData playerData;

    private void Awake()
    {
        animalData = SaveDataManager.Instance.Data.Animals;
        playerData = SaveDataManager.Instance.Data.Player;
        CreateStageButton();
    }

    /// <summary>
    /// 생성
    /// </summary>
    public void CreateStageButton()
    {
        for (int i = 0; i < stageButtonsInfo.Length; i++)
        {
            GameObject stageButton = Instantiate(stageButtonObject);
            stageButton.transform.SetParent(stagesParent);

            /* 
             * 중요!! 튜토리얼이1 그다음 일반 스테이지2 가차3 .... 이케됨 그래서 2부터 시작
             * 아마 가챠랑 분리해야 하면 플레이어 데이터 수정한 뒤 이곳도 수정해야 댐
             */
            for (int j = 2; j <= playerData.OpenStage; j++)
                stageButtonsInfo[i].isOpen = true;

            stageButtonsInfo[i].stageIndex = i + 2;  
            StageButtonInfo info = stageButtonsInfo[i];
            SettingStageButton settingButton = stageButton.GetComponent<SettingStageButton>();
            switch (info.STAGE)
            {
                case StageKind.GATCHA:
                    createXPose += Mathf.Abs(gatchaXPose);
                    break;
                case StageKind.STAGE:
                    createXPose += Mathf.Abs(stageXPose);
                    break;
            }
            settingButton.CreateStageButton(info, info.animalIndex, GetCollectCount(info.animalIndex));
            stageButton.transform.position = new Vector2(createXPose, createYPose);
        }
    }

    /// <summary>
    /// 콜렉트 카운트 계산
    /// </summary>
    /// <param name="startIndex"></param>
    /// <param name="endIndex"></param>
    /// <returns></returns>
    private int GetCollectCount(int[] animalIndex)
    {
        int count = 0;
        for (int i = 0; i < animalIndex.Length; i++)
        {
            if (animalData[animalIndex[i]].IsCollect)
                count++;
        }
        return count;
    }
}
