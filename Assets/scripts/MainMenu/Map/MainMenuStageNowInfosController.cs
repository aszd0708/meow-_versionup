﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 고양이 갖고있는지 확인 겸 체크
/// </summary>
public class MainMenuStageNowInfosController : Singleton<MainMenuStageNowInfosController>
{
    [Header("스테이지 인포들")]
    public MainMenuStageNowInfo[] infos;

    [Header("고양이 처음 지점")]
    public Transform cat;
    [Header("카메라 처음 시점")]
    public Transform cam;

    private void Start()
    {

        //SetStartPosition();
    }

    public void SetNonCat()
    {
        infos = new MainMenuStageNowInfo[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
            infos[i] = transform.GetChild(i).GetComponent<MainMenuStageNowInfo>();

        for (int i = 0; i < infos.Length; i++)
        {
            infos[i].HaveCat = false;
        }
    }

    /// <summary>
    /// 처음 고양이와 카메라 포지션 잡아주는 함수
    /// </summary>
    public void SetStartPosition()
    {
        infos = new MainMenuStageNowInfo[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
            infos[i] = transform.GetChild(i).GetComponent<MainMenuStageNowInfo>();

        int index = PlayerPrefs.GetInt("NowSelectStage", 0);
        if(index != 0)
        {
            index--;
        }
        SettingStageButton setting = infos[index].GetComponent<SettingStageButton>();

        Vector2 pose = infos[index].transform.position;
        cat.position = pose + setting.CatStandPose;
        cam.position = new Vector3(pose.x + setting.CatStandPose.x , cam.position.y, cam.position.z);
        SetNonCat();
        infos[index].HaveCat = true;
    }
}
