﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundSetSpeed : MonoBehaviour
{
    public float speed;
    public float minX, maxX;
    public float minY, maxY;
    void Awake()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            BackgroundLineSetSpeed obj = transform.GetChild(i).GetComponent<BackgroundLineSetSpeed>();
            obj.speed = speed;
            if(i%2 == 0)
            {
                obj.minX = minX;
                obj.maxX = maxX;
            }

            else
            {
                obj.minX = minX + transform.GetChild(i).transform.localPosition.x;
                obj.maxX = maxX + transform.GetChild(i).transform.localPosition.x;
            }
            obj.minY = minY;
            obj.maxY = maxY;
        }
    }
}
