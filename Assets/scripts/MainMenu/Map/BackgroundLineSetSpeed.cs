﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundLineSetSpeed : MonoBehaviour
{
    public float speed;
    public float minX, maxX;
    public float minY, maxY;
    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i< transform.childCount; i++)
        {
            Background bg = transform.GetChild(i).GetComponent<Background>();
            bg.speed = speed;
            bg.minX = minX;
            bg.maxX = maxX;
            bg.minY = minY;
            bg.maxY = maxY;
        }
    }
}
