﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 스테이지 지금 고양이 있는지 확인
/// </summary>
public class MainMenuStageNowInfo : MonoBehaviour
{
    private bool haveCat = false;

    /// <summary>
    /// 지금 고양이가 이 스테이지에 있는지 확인
    /// </summary>
    public bool HaveCat { get => haveCat; set => haveCat = value; }

    public Vector3 GetStagePosition()
    { return transform.position; }
}
