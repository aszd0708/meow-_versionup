﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-03-17
 * 메인 창에서 고양이 옆에(대각선) 손꾸락 나와서 움직이는 스크립트
 * 대각선에 있기 때문에 튜토리얼씬에 있는것과 다르게 움직임
 */
public class MainMenuFingerMotion : MonoBehaviour
{
    public float distance;
    public float time;
    public float pickTime;
    public float size;

    private Vector3 firstPos;
    private SpriteRenderer spriteRenderer;
    private Coroutine fingerMotion;

    private Vector3 moveVec;
    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
    // Start is called before the first frame update
    void Start()
    {
        firstPos = transform.localPosition;
        moveVec = new Vector3(transform.localPosition.x + distance, transform.localPosition.y - distance, transform.localPosition.z);
    }

    public void FingerMotionController(bool move)
    { 
        spriteRenderer.enabled = !move;

        if (!move) fingerMotion = StartCoroutine(FingerMotion());
        else
        {
            transform.localScale = new Vector3(1, 1, 1);
            transform.localPosition = firstPos;
            transform.DOPause();
            if(fingerMotion != null)
                StopCoroutine(fingerMotion);
        }
    }

    private void OnEnable()
    {
        StartCoroutine(FingerMotion());
    }

    private void OnDisable()
    {
        transform.localScale = new Vector3(1, 1, 1);
        transform.localPosition = firstPos;
        transform.DOPause();
    }

    private IEnumerator FingerMotion()
    {
        WaitForSeconds waitTime = new WaitForSeconds(time);
        Vector2[] movePoses = new Vector2[4];
        movePoses[0] = new Vector2(transform.localScale.x + size, transform.localScale.y - size);
        movePoses[1] = new Vector2(transform.localScale.x - size * 2, transform.localScale.y + size * 2);
        movePoses[2] = new Vector2(transform.localScale.x + size * 2, transform.localScale.y - size * 2);
        movePoses[3] = new Vector2(transform.localScale.x - size, transform.localScale.y + size);
        while (true)
        {
            transform.DOScale(movePoses[0], time);
            yield return waitTime;
            transform.DOLocalMove(moveVec, time * 2);
            transform.DOScale(movePoses[1], time);
            yield return waitTime;
            transform.DOScale(movePoses[2], time);
            yield return waitTime;
            transform.DOLocalMove(firstPos, time * 2);
            transform.DOScale(movePoses[3], time);
            yield return waitTime;
            yield return waitTime;
        }
    }
}
