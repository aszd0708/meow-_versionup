﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectMotion : MonoBehaviour
{
    public void Motion()
    {
        StartCoroutine("MotionStart");
    }

    public void EndMotion()
    {
        StartCoroutine("EndMotion");
    }

    private IEnumerator MotionStart()
    {
        GetComponent<Transform>().localScale = new Vector3(0, 0, 0);
        GetComponent<Transform>().DOScale(1.2f, 0.5f).SetEase(Ease.OutElastic);
        yield return new WaitForSeconds(3.0f);
        StartCoroutine("EndMotionStart");
        yield return null;
    }

    private IEnumerator EndMotionStart()
    {
        GetComponent<Transform>().DOScale(0.0f, 0.5f);
        yield return new WaitForSeconds(0.5f);
        Destroy(gameObject);
    }
}
