﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemSpace : MonoBehaviour
{
    private HorizontalLayoutGroup hor;
    private FoldUI fold;

    private void Awake()
    {
        hor = GetComponent<HorizontalLayoutGroup>();
        fold = GetComponent<FoldUI>();
    }

    // Start is called before the first frame update
    void Start()
    {        
        Invoke("SetSpace", 0.1f);
    }

    private void SetSpace()
    {
        float space;
        if (gameObject.transform.childCount == 0)
            return;
        space = (1920 - Screen.width) / 16;
        hor.spacing = space;
        fold.ReCorrection();
        fold.FoldNow = true;

        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            gameObject.transform.GetChild(i).GetComponent<Image>().enabled = true;
        }
    }
}