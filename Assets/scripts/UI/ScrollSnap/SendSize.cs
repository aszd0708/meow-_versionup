﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SendSize : MonoBehaviour
{
    public RectTransform itemBtnRect;
    public FoldUI foldSize;
    // Start is called before the first frame update
    void OnEnable()
    {
        GetSize();
    }

    private void GetSize()
    {
        foldSize.ButtonSize = itemBtnRect.sizeDelta;
        foldSize.FoldNow = false;
    }
}
