﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class CatHint : MonoBehaviour
{
    public bool showHint = false;
    public VerticalScrollSnap scollView;

    [Header("좀더 편하게 컨텐츠 자식 갖고오는 부모")]
    public Transform contents;
    private Vector2 originPos0;
    private Vector2 originPos1;
    // Start is called before the first frame update

    private Coroutine timer;

    private Text text;

    private List<string> catBtn = new List<string>();

    private void Awake()
    {
        text = transform.GetChild(0).GetComponent<Text>();
    }

    void Start()
    {
        originPos0 = GetComponent<RectTransform>().offsetMax;
        originPos1 = GetComponent<RectTransform>().offsetMin;
        Invoke("ScrollCatBtnScriptSetting", 0.3f);
    }

    void Update()
    {
        if (showHint)
        {
            if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            {
                    text.text = catBtn[scollView.CurrentPage];
                   //GoOriginPos();
                   //showHint = false;
            }
            else
            {
                if (Input.GetMouseButtonDown(0))
                {
                    GoOriginPos();
                    StopCoroutine(timer);
                    showHint = false;
                }
            }
        }
    }

    public void GoOriginPos()
    {
        GetComponent<RectTransform>().offsetMin = originPos0;
        GetComponent<RectTransform>().offsetMax = originPos1;
        showHint = false;
    }

    public void StopCor()
    {
        StopCoroutine(timer);
    }

    public void Center()
    {
        timer = StartCoroutine(Back());
    }

    private IEnumerator Back()
    {
        GetComponent<RectTransform>().offsetMax = new Vector2(0, 0);
        GetComponent<RectTransform>().offsetMin = new Vector2(0, 0);
        yield return new WaitForSeconds(3.0f);
        GetComponent<RectTransform>().offsetMin = originPos0;
        GetComponent<RectTransform>().offsetMax = originPos1;
        yield return null;
    }

    private void ScrollCatBtnScriptSetting()
    {
        for(int i = contents.childCount - 1; i >= 0; i--)
        {
            catBtn.Add(contents.GetChild(i).GetComponent<SampleCatButton>().text);
        }
    }
}
