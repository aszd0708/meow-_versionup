﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class GetSizeY : MonoBehaviour
{
    public CreateScrollCatList catNum;
    public int hiddenCatNum;
    public float size;

    //public VerticalScrollSnap scrollSnap;

    private float firstBtnYSize;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("GetSize", 0.2f);
    }

    public void SetSize()
    {
        for (int i = 0; i < catNum.CatList.Count - hiddenCatNum; i++)
            transform.GetChild(i).GetComponent<Size>().size = size * i + firstBtnYSize;

        transform.GetChild(0).GetComponent<GoCenter>().StartCenter();
    }

    public void GetSize()
    {
        //size = -scrollSnap._childSize;
        //firstBtnYSize = -scrollSnap.GetPositonforPage(scrollSnap.ChildObjects.Length - 1);
        SetSize();
    }
}
