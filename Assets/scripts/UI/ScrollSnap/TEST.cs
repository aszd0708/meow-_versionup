﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI.Extensions;

public class TEST : MonoBehaviour
{
    public GameObject TESTer;
    public GameObject contents;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("Add", 0.1f);
    }

    // Update is called once per frame
    private void Add()
    {
        for (int i = 0; i < contents.transform.childCount; i++)
            TESTer.GetComponent<HorizontalScrollSnap>().AddChild(contents.transform.GetChild(i).gameObject);    
    }
}
