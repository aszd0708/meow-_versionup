﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemCreater : MonoBehaviour
{
    public GameObject itemPanel;
    private int num;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("StartGetSize", 0.7f);
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void CreateItem(GameObject item)
    {
        num = itemPanel.transform.childCount;
        GameObject createItem = Instantiate(item, gameObject.transform);
        if (gameObject.transform.childCount > 1)
            gameObject.transform.GetChild(0).GetComponent<SampleButtonScript>().Destroy();
        for(int i = 0; i < itemPanel.transform.childCount; i++)
        {
            num--;
            if (item == itemPanel.transform.GetChild(i).gameObject)
                break;
        }
        GetComponent<RectTransform>().anchoredPosition = new Vector2(-(item.GetComponent<RectTransform>().sizeDelta.x * num), GetComponent<RectTransform>().anchoredPosition.y);
        createItem.GetComponent<RectTransform>().anchoredPosition = new Vector2(item.GetComponent<RectTransform>().anchoredPosition.x, 0.0f);
        createItem.GetComponent<Image>().enabled = false;
        createItem.transform.GetChild(1).GetComponent<UIItemDragHandler>().enabled = true;
    }

    private void StartGetSize()
    {
        GetComponent<RectTransform>().sizeDelta = itemPanel.GetComponent<RectTransform>().sizeDelta;
        GetComponent<RectTransform>().anchoredPosition = itemPanel.GetComponent<RectTransform>().anchoredPosition;
    }
}
