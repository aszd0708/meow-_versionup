﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class GoCenter : MonoBehaviour
{
    private GameObject catText;
    private CatHint catHint;
    private SampleCatButton info;
    public SoundManager soundManager;
    public Size size;
    VerticalScrollSnap snap;
    Vector3 firstLerpPos;
    Vector2 firstTextPos0;
    Vector2 firstTextPos1;

    private void Awake()
    {
        soundManager = FindObjectOfType<SoundManager>();
        size = GetComponent<Size>();
        info = GetComponent<SampleCatButton>();
    }

    void Start()
    {
        catHint = FindObjectOfType<CatHint>();
        catText = catHint.gameObject;

        firstTextPos0 = catText.GetComponent<RectTransform>().offsetMax;
        firstTextPos1 = catText.GetComponent<RectTransform>().offsetMin;

        snap = gameObject.transform.parent.parent.GetComponent<VerticalScrollSnap>();
        Invoke("GetLerp", 0.1f);
    }

    private void GetLerp()
    {
        snap._lerp = true;
        snap.UpdateLayout();
        firstLerpPos = new Vector3(0, Mathf.Abs(snap.panelDimensions.y + 40), 0);
    }

    public void Center()
    {
        soundManager.UITouch();
        snap._lerp_target = new Vector3(0, Mathf.Abs((int)size.size), 0);
        snap._lerp = true;

        catHint.Center();
        catHint.StopCor();
        catHint.showHint = true;

        if (info.text == "none")
            return;

        catText.transform.GetChild(0).GetComponent<Text>().text = info.text;
    }

    public void StartCenter()
    {
        snap._lerp_target = new Vector3(0, Mathf.Abs((int)size.size), 0);
        snap._lerp = true;
    }

    public void SetLerpState(bool state)
    {
        snap.SetLerp(state);
    }
}
