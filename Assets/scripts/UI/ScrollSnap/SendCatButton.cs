﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class SendCatButton : MonoBehaviour
{
    private List<GameObject> cats = new List<GameObject>();
    private int target;
    private GameObject targetCat;
    public VerticalScrollSnap scrollSnap;
    public GameObject catHint;

    void Start()
    {
        //Invoke("Setting", 0.2f);
        Setting();
    }

    void Setting()
    {
        for (int i = 0; i < scrollSnap.ChildObjects.Length; i++)
            cats.Add(scrollSnap.ChildObjects[i]);
        scrollSnap._scrollStartPosition = (scrollSnap.ChildObjects.Length - 1) * (scrollSnap._childSize);
        //scrollSnap
    }

    /*
    void Update()
    {
        target = Mathf.Abs(scrollSnap.CurrentPage - (transform.GetChild(0).transform.childCount -1));
        targetCat = cats[target];

        if(catHint.GetComponent<CatHint>().showHint)
            catHint.transform.GetChild(0).GetComponent<Text>().text = targetCat.GetComponent<SampleCatButton>().text;
    }
    */
}
