﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

/*
 * 2020-04-12
 * 편지 UI 모션
 * 이거 생각보다 오래 걸리고 버그 많을거 같고 좀 힘들거 같다
 * 그래도 일단 만들어봄
 */

public class LetterUIMotion : Singleton<LetterUIMotion>
{
    [Header("편지 판넬")]
    public GameObject letterPanel;

    [Header("편지 내용에 관한 오브젝트들")]
    public Text letterText;

    [Header("각 부분 RectTransform")]
    public RectTransform capRect;
    public RectTransform letterRect, paperRect, maskRect, bottomRect;

    [Header("편지가 움직이는 위치")]
    public Vector3 letterMovePos;

    [Header("편지들이 처음에 있는 위치들")]
    private Vector3 paperOriginPos;
    private Vector3 maskOriginPos, bottomOriginPos, capOriginPos;

    [Header("각 움직임의 시간")]
    public float letterMoveTime;
    public float paperMoveTime, capRotTime;

    [Header("각 부분이 올라오는 포지션(계산해서 넣어짐)")]
    private Vector3 capMovePos;
    private Vector3 paperMovePos, maskMovePos, bottomMovePos;

    [Header("각 부분이 살짝 걸치는 포지션")]
    private Vector3 capMiddleMovePos;
    private Vector3 paperMiddleMovePos, maskMiddleMovePos, bottomMiddleMovePos;

    [Header("올라오는 Y축")]
    public float yPos, middleYPos;
    [Header("종이 올라오는 위치")]
    public Vector2 paperLastMovePos;


    [Header("편지 리스트 터치 막는 변수들")]
    public Image prohibitImg;
    public Button listExitButton;

    private void Start()
    {
        SetMovePoses();

        paperOriginPos = paperRect.anchoredPosition;
        maskOriginPos = maskRect.anchoredPosition;
        bottomOriginPos = bottomRect.anchoredPosition;
        capOriginPos = capRect.anchoredPosition;

        letterPanel.SetActive(false);
    }

    /// <summary>
    /// 편지 시작
    /// </summary>
    /// <param name="content"></param>
    public void StartLetter(string content)
    {
        letterText.text = content;
        PopupManager.Instance.PopupCount++;
        OpenLetter();
    }

    private void SetMovePoses()
    {
        Vector2 detinationPos = new Vector3(0, yPos);

        capMovePos = ObjMovePosSetting(capRect, detinationPos);
        paperMovePos = ObjMovePosSetting(paperRect, detinationPos);
        maskMovePos = ObjMovePosSetting(maskRect, detinationPos);
        bottomMovePos = ObjMovePosSetting(bottomRect, detinationPos);

        Vector2 middleDestinationPos = new Vector3(0, middleYPos);

        capMiddleMovePos = ObjMovePosSetting(capRect, middleDestinationPos);
        paperMiddleMovePos = ObjMovePosSetting(paperRect, middleDestinationPos);
        maskMiddleMovePos = ObjMovePosSetting(maskRect, middleDestinationPos);
        bottomMiddleMovePos = ObjMovePosSetting(bottomRect, middleDestinationPos);
    }

    private Vector2 ObjMovePosSetting(RectTransform rect, Vector2 movePos)
    {
        Vector2 setMovePos = new Vector3(movePos.x - rect.anchoredPosition.x, movePos.y - rect.anchoredPosition.y);
        return setMovePos;
    }

    public void OpenLetter()
    {
        letterPanel.SetActive(true);
        StartCoroutine(_OpenLetterMotion());
    }

    private IEnumerator _OpenLetterMotion()
    {
        listExitButton.enabled = false;
        prohibitImg.enabled = true;
        //MenuManager.Instance.SetState(true);
        // 편지가 올라옴
        capRect.DOAnchorPos(capMovePos, letterMoveTime);
        maskRect.DOAnchorPos(maskMovePos, letterMoveTime);
        bottomRect.DOAnchorPos(bottomMovePos, letterMoveTime);
        paperRect.DOAnchorPos(letterMovePos, letterMoveTime);
        yield return new WaitForSeconds(letterMoveTime);

        capRect.DOLocalRotate(new Vector3(180, 0, 0), capRotTime);
        AudioManager.Instance.PlaySound("letter", transform.position);
        yield return new WaitForSeconds(capRotTime);
        capRect.SetSiblingIndex(0);

        paperRect.DOAnchorPos(paperLastMovePos, paperMoveTime);

        capRect.DOAnchorPos(capMiddleMovePos, paperMoveTime);
        maskRect.DOAnchorPos(maskMiddleMovePos, paperMoveTime);
        bottomRect.DOAnchorPos(bottomMiddleMovePos, paperMoveTime);
        yield break;
    }

    /// <summary>
    /// 편지 끝
    /// </summary>
    public void CloseLetter()
    {
        StartCoroutine(_CloseLetterMotion());
    }

    private IEnumerator _CloseLetterMotion()
    {
        capRect.DOAnchorPos(capMovePos, paperMoveTime);
        maskRect.DOAnchorPos(maskMovePos, paperMoveTime);
        bottomRect.DOAnchorPos(bottomMovePos, paperMoveTime);
        paperRect.DOAnchorPos(letterMovePos, paperMoveTime);
        yield return new WaitForSeconds(paperMoveTime);

        capRect.DORotateQuaternion(Quaternion.identity, capRotTime);
        capRect.SetSiblingIndex(3);
        yield return new WaitForSeconds(capRotTime);

        capRect.DOAnchorPos(capOriginPos, paperMoveTime);
        maskRect.DOAnchorPos(maskOriginPos, paperMoveTime);
        bottomRect.DOAnchorPos(bottomOriginPos, paperMoveTime);
        paperRect.DOAnchorPos(paperOriginPos, paperMoveTime);
        yield return new WaitForSeconds(paperMoveTime);

        //MenuManager.Instance.SetState(false);

        PopupManager.Instance.PopupCount--;

        listExitButton.enabled = true;
        prohibitImg.enabled = false;

        letterPanel.SetActive(false);
        yield break;
    }
}
