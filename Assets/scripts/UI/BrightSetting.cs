﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BrightSetting : MonoBehaviour
{
    public Image bright;
    private float backBri = 1f;
    private Slider backBright;

    private void Awake()
    {
        backBright = GetComponent<Slider>();
        backBri = PlayerPrefs.GetFloat("backBright", 1.0f);
        backBright.value = backBri;
        bright.color = new Vector4(bright.color.r, bright.color.g, bright.color.b, (255f - backBright.value * 255f) / (255f * 2f));

    }
    private void Start()
    {
        
    }

    void Update()
    {
        SoundSlider();
    }

    public void SoundSlider()
    {
        backBri = PlayerPrefs.GetFloat("backBright", 1.0f);
        bright.color = new Vector4(bright.color.r, bright.color.g, bright.color.b, (255f  - backBright.value * 255f) / (255f * 2f));
        backBri = backBright.value;
        PlayerPrefs.SetFloat("backBright", backBri);
    }
}
