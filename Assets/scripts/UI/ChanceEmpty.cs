﻿using BitBenderGames;
using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChanceEmpty : MonoBehaviour
{
    public GameObject child;
    private bool show;
    private bool flag = false;
    public GameObject canvas;
    public Timer timer;
    public float effectTime;
    private Vector2 scale;
    public string StageNumChance;
    // Start is called before the first frame update

    private bool once = false;
    private Text text;

    private Coroutine popupTimer;

    //private TouchInterationCam touchScript;
    private TouchManager touchManager;
    void Awake()
    {
        text = GetComponent<Text>();
        //touchScript = FindObjectOfType<TouchInterationCam>();
        touchManager = FindObjectOfType<TouchManager>();
    }

    void Start()
    {
        show = false;
        scale = child.GetComponent<RectTransform>().localScale;
        child.GetComponent<RectTransform>().localScale = new Vector2(0, 0);

        if (EncryptedPlayerPrefs.HasKey(StageNumChance))
            text.text = EncryptedPlayerPrefs.GetInt(StageNumChance).ToString();

        else
            EncryptedPlayerPrefs.SetInt(StageNumChance, Convert.ToInt32(text.text));

        popupTimer = StartCoroutine(Popup());
    }

    private IEnumerator Popup()
    {
        while (true)
        {
            if (System.Convert.ToInt32(text.text) <= 0 && flag)
            {
                Debug.Log(timer.startTime);
                show = true;
                flag = false;
                once = false;

                timer.startTime = System.DateTime.Now;
                timer.TimePlus();
                timer.SaveTime();

                ShowPopup(show);
            }

            else if (System.Convert.ToInt32(text.text) > 0)
            {
                show = false;
                flag = true;
                once = false;
            }

            yield return new WaitForSeconds(effectTime);
        }
    }

    public void ConvertShow()
    {
        show = false;
    }

    public void ShowPopup(bool show)
    {
        if (show)
        {
            MenuManager.Instance.SetState(true);
            canvas.SetActive(true);
            child.transform.SetParent(canvas.transform);
            child.transform.DOScale(scale, effectTime).SetEase(Ease.InOutQuint);
            once = true;
            StopCoroutine(popupTimer);
        }

        else
        {
            MenuManager.Instance.SetState(false);
            child.transform.DOScale(0.0f, effectTime).SetEase(Ease.InOutQuint);
            once = true;
            //touchScript.NoChance = false;
            touchManager.NoChance = false;
        }
    }

    public void ResetNoChance()
    {
        ShowPopup(false);
        //touchScript.NoChance = true;
        touchManager.NoChance = true;
    }

    public void ADReset()
    {
        ShowPopup(false);
    }

    public void StartChecker()
    {
        popupTimer = StartCoroutine(Popup());
        //touchScript.NoChance = false;
    }
}
