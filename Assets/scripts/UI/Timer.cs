﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public System.DateTime startTime;
    public System.DateTime nowTime;
    private System.TimeSpan calTime;
    private System.TimeSpan leftTime;
    private System.DateTime resetTime;

    public double time = 20;
    public System.DateTime chanceTime;

    public int chance;

    public Text chanceNum;
    public ChanceNumImage numImg;
    public string stageName;

    private Text text;

    private void Awake()
    {
        text = GetComponent<Text>();
    }
    void Start()
    {
        resetTime = new DateTime(1995, 11, 10, 18, 00, 00);
        if (!File.Exists(Application.persistentDataPath + "/"+stageName+"Time.dat"))
        {
            startTime = resetTime;
            SaveTime();
        }
        else
            LoadTime();

        TimePlus();
        StartTimer(); 
    }

    public void TimePlus()
    {
        chanceTime = startTime.AddMinutes(+time);
    }

    public void SaveTime()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/" + stageName + "Time.dat");

        System.DateTime saveInfo;

        saveInfo = startTime;

        bf.Serialize(file, saveInfo);
        file.Close();
        Debug.Log(resetTime);
        Debug.Log("시간 세이브 성공");
    }

    public void LoadTime()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.OpenRead(Application.persistentDataPath + "/" + stageName + "Time.dat");

        if (file != null && file.Length > 0)
        {
            // 파일 역직렬화하여 B에 담기
            System.DateTime saveInfo = (System.DateTime)bf.Deserialize(file);
            // B --> A에 할당
            startTime = saveInfo;
            file.Close();
        }
    }

    public void StartTimer()
    {
        StartCoroutine("ChanceTimer");
    }

    private IEnumerator ChanceTimer()
    {
        while(true)
        {
            if (startTime == resetTime)
            {
                GetComponent<Text>().text = " ";
                yield return null;
            }

            else
            {
                nowTime = System.DateTime.Now;
                leftTime = chanceTime - nowTime;
                GetComponent<Text>().text = System.Convert.ToString(leftTime.Minutes) + "분" + System.Convert.ToString(leftTime.Seconds) + "초 후 횟수 +" + chance;
                
                if((leftTime.Minutes + leftTime.Seconds) <= 0)
                {
                    GiveChance();
                    // 광고 함수
                    // 그리고 횟수 증가 ㅇㅋ?
                }

                else
                {
                    chanceNum.text = "0";
                    numImg.NumImg();
                }
            }
            yield return null;
        }
    }

    public void GiveChance()
    {
        ResetTime();
        chanceNum.text = "20";
        EncryptedPlayerPrefs.SetInt(chanceNum.GetComponent<ChanceEmpty>().StageNumChance, Convert.ToInt32(chanceNum.text)); 
        numImg.NumImg();
        SaveTime();
        chanceNum.GetComponent<ChanceEmpty>().StartChecker();
    }

    public void ResetTime()
    {
        startTime = resetTime;
    }
}
