﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SampleCatButton : MonoBehaviour
{
    public GameObject fieldCat;
    public Button button;
    public Text nameLable;
    //public Image catImage;
    public string text;
    public bool collect;

    [Header("버튼 이미지")]
    public Image spriteImage;

    private void Awake()
    {
        if (gameObject.tag == "hiddenCat")
            return;
    }

    public void OnClick()
    {
        collect = true;
        if (gameObject.tag == "hiddenCat")
            return;
        spriteImage.color = new Color(255, 255, 255);
    }
}
