﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

[System.Serializable]
public class Cat
{
    public GameObject fieldCat;
    public Sprite catSprite;
    public string name;
    public bool isuse;
    public bool collect;
    public string text;
    public Button.ButtonClickedEvent thingToDo;
}

public class CreateScrollCatList : MonoBehaviour
{
    public GameObject samepleButton;
    public Transform contentPanel;
    public Transform hiddenConternt;
    public List<Cat> CatList;
    public VerticalScrollSnap verticalSnap;

    public GameObject[] hiddenCat;
    public GameObject none;

    private bool hidden;
    // Start is called before the first frame update
    void Awake()
    {
        PopulateList();
    }

    void Start()
    {
        GoFirst();
    }
    // Update is called once per frame
    void PopulateList()
    {
        int a = 1;
        verticalSnap.ChildObjects = new GameObject[CatList.Count - hiddenCat.Length];
        foreach (var cat in CatList)
        {
            for(int i = 0; i < hiddenCat.Length; i++)
            {
                if (cat.fieldCat == hiddenCat[i])
                    hidden = true;
                else
                    hidden = false;
            }

            if(!hidden)
            {
                GameObject newButton = Instantiate(samepleButton);
                SampleCatButton buttonScript = newButton.GetComponent<SampleCatButton>();
                buttonScript.nameLable.text = cat.name;
                //buttonScript.catImage.sprite = cat.catSprite;

                // 이 밑에 있는게 최근
                //buttonScript.GetComponent<Image>().sprite = cat.catSprite;
                // 이 밑에 있는게 고친거
                buttonScript.spriteImage.sprite = cat.catSprite;

                buttonScript.fieldCat = cat.fieldCat;
                buttonScript.text = cat.text;

                if (!buttonScript.collect)
                {
                    //buttonScript.catImage.color = new Color(0, 0, 0);

                    // 이 밑에 있는게 최근
                    //buttonScript.GetComponent<Image>().color = new Color(0, 0, 0);
                    // 이 밑에 있는게 고친거
                    buttonScript.spriteImage.color = new Color(0, 0, 0);

                }
                newButton.transform.SetParent(contentPanel);
                verticalSnap.ChildObjects[verticalSnap.ChildObjects.Length - a] = newButton;
                a++;
                //contentPanel.parent.GetComponent<VerticalScrollSnap>().
            }

            else
            {
                GameObject newNone = Instantiate(none);
                SampleCatButton noneScript = newNone.GetComponent<SampleCatButton>();
                
                //noneScript.nameLable.text = cat.name;
                noneScript.fieldCat = cat.fieldCat;
                noneScript.text = cat.text;
                newNone.transform.SetParent(hiddenConternt);
            }
        }
    }

    public void GoFirst()
    {
        verticalSnap.StartingScreen = CatList.Count;
    }
}
