﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VibratorMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        VibratorControllor.VibSetting();

        if (PlayerPrefs.GetInt("Vibrator") == 0)
            transform.GetChild(0).GetComponent<Text>().text = ("진동 : 꺼짐");

        else if (PlayerPrefs.GetInt("Vibrator") == 1)
            transform.GetChild(0).GetComponent<Text>().text = ("진동 : 켜짐");
    }

    public void OnClick()
    {
        if (PlayerPrefs.GetInt("Vibrator") == 0)
        {
            VibratorControllor.vib = true;
            PlayerPrefs.SetInt("Vibrator", 1);
            transform.GetChild(0).GetComponent<Text>().text = ("진동 : 켜짐");
        }

        else if (PlayerPrefs.GetInt("Vibrator") == 1)
        {
            VibratorControllor.vib = false;
            PlayerPrefs.SetInt("Vibrator", 0);
            transform.GetChild(0).GetComponent<Text>().text = ("진동 : 꺼짐");
        }
    }
}
