﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIItemDragHandler : MonoBehaviour, IDragHandler, IEndDragHandler, IPointerDownHandler, IPointerUpHandler
{
    private Vector3 position;
    private bool create;
    public DragItem item;
    private float size;
    void Start()
    {
        position = transform.localPosition;
        create = true;
    }

    public void OnDrag(PointerEventData eventData)
    {
        size = transform.parent.GetComponent<RectTransform>().sizeDelta.y;
        if (create)
            CreateItem();
        transform.position = new Vector3(Input.mousePosition.x, Input.mousePosition.y + size, Input.mousePosition.z);
        create = false;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        //GetComponent<RectTransform>().offsetMin = new Vector2(0, 0);
        //GetComponent<RectTransform>().offsetMax = new Vector2(0, 0);
        Invoke("SetPo", 0.01f);
    }

    void Update()
    {
        if (Input.GetMouseButtonUp(0))
            create = true;
    }

    void CreateItem()
    {
        Vector3 touchPos = Camera.main.ScreenToWorldPoint(gameObject.transform.position);
        GameObject item = Instantiate(transform.parent.GetComponent<SampleButtonScript>().itemDrag, touchPos, Quaternion.identity);
        item.GetComponent<ObjectTrigger>().SetObject(gameObject);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        size = transform.parent.GetComponent<RectTransform>().sizeDelta.y;
        transform.position = new Vector3(Input.mousePosition.x, Input.mousePosition.y + size, Input.mousePosition.z);
    }

    private void SetPo()
    {
        GetComponent<RectTransform>().offsetMin = new Vector2(0, 0);
        GetComponent<RectTransform>().offsetMax = new Vector2(0, 0);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Invoke("SetPo", 0.05f);
    }
}
