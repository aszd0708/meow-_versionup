﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeInOut : MonoBehaviour
{
    public float animTime = 1.0f;

    public AudioSource BGM;

    private Image fadeImage;

    private float outStart = 0.0f;
    private float outEnd = 1.0f;
    private float inStart = 1.0f;
    private float inEnd = 0.0f;
    private float time = 0.0f;

    // Start is called before the first frame update
    void Awake()
    {
        fadeImage = GetComponent<Image>();
    }

    void Start()
    {
        // 처음 색 투명하지 않게 만들어줌
        fadeImage.color = new Color(fadeImage.color.r, fadeImage.color.g, fadeImage.color.b, 255);
        StartCoroutine(FadeIn());
        if (BGM != null)
            StartCoroutine(BGMFadeOut(0, PlayerPrefs.GetFloat("backVol", 1.0f), 0));
    }

    public void FadeOutF()
    {
        StartCoroutine(FadeOut());
        if(BGM != null)
            StartCoroutine(BGMFadeIn(PlayerPrefs.GetFloat("backVol", 1.0f), 0, 0));
    }

    private IEnumerator FadeIn()
    {
        Color color = fadeImage.color;
        time = 0.0f;
        color.a = Mathf.Lerp(inStart, inEnd, time);

        while (color.a > 0.0f)
        {
            time += Time.deltaTime / animTime;

            color.a = Mathf.Lerp(inStart, inEnd, time);

            fadeImage.color = color;

            if(fadeImage.color.a <= 0)
                GetComponent<Image>().enabled = false;
            yield return null;
        }
    }

    private IEnumerator FadeOut()
    {
        Color color = fadeImage.color;
        GetComponent<Image>().enabled = true;
        time = 0.0f;
        color.a = Mathf.Lerp(outStart, outEnd, time);

        while (color.a < 1.0f)
        {
            time += Time.deltaTime / animTime;

            color.a = Mathf.Lerp(outStart, outEnd, time);

            fadeImage.color = color;

            yield return null;
        }
    }

    private IEnumerator BGMFadeIn(float inStart, float inEnd, float time)
    {
        float bgmVolume = BGM.volume;
        time = 0.0f;
        bgmVolume = Mathf.Lerp(inStart, inEnd, time);

        while (bgmVolume > 0.0f)
        {
            time += Time.deltaTime / animTime;

            bgmVolume = Mathf.Lerp(inStart, inEnd, time);

            BGM.volume = bgmVolume;
            yield return null;
        }

        BGM.enabled = false;
        yield return null;
    }

    private IEnumerator BGMFadeOut(float outStart, float outEnd, float time)
    {
        float prefVolume = PlayerPrefs.GetFloat("backVol", 1.0f);
        float bgmVolume = BGM.volume;
        time = 0.0f;
        bgmVolume = Mathf.Lerp(outStart, outEnd, time);
        

        while(bgmVolume < prefVolume)
        {
            time += Time.deltaTime / animTime;

            bgmVolume = Mathf.Lerp(outStart, outEnd, time);

            BGM.volume = bgmVolume;

            yield return null;
        }
        yield return null;
    }
}
