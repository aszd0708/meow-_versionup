﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioSetting : MonoBehaviour
{
    public Slider backVolume;
    public Image soundImage;
    public Sprite[] soundSprite = new Sprite[2];
    private float backVol = 1f;

    public new AudioSource audio;

    private void Awake()
    {
        backVol = PlayerPrefs.GetFloat("backVol", 1.0f);
        backVolume.value = backVol;
        audio.volume = backVolume.value;
        SoundSlider();
    }

    private void Start()
    {
        backVol = PlayerPrefs.GetFloat("backVol", 1.0f);
        backVolume.value = backVol;
        audio.volume = backVolume.value;
    }

    void Update()
    {
        SoundSlider();
    }

    public void SoundSlider()
    {
        if (backVolume.value <= 0)
            soundImage.sprite = soundSprite[1];
        else
            soundImage.sprite = soundSprite[0];
        audio.volume = backVolume.value * 0.5f;
        backVol = backVolume.value;
        PlayerPrefs.SetFloat("backVol", backVol);
    }
}
