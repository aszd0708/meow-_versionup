﻿using BitBenderGames;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowSettingPopup : MonoBehaviour
{
    public float effectTime;
    public GameObject child;
    public GameObject vibratorButton;
    //public TouchInterationCam cameraTouch;
    public TouchManager touchManager;
    public TouchInputController cameraMove;
    public AudioClip effect;
    public GameObject canvas;
    private BackgoundTouch backgound;
    private Vector2 scale;
    private bool show;

    private RectTransform rect;
    // Start is called before the first frame update
    void Start()
    {
        backgound = FindObjectOfType<BackgoundTouch>();
        rect = child.GetComponent<RectTransform>();
        scale = rect.localScale;
        rect.localScale = new Vector2(0, 0);
        show = false;
    }
    public void ShowPopup()
    {
        if (show)
            StartCoroutine(HideEffect());
        else
            StartCoroutine(ShowEffect());
    }
    
    public void Vibrator()
    {
        if(PlayerPrefs.GetInt("vibrator") == 1)
        {
            PlayerPrefs.SetInt("vibrator", 0);
            PlayerPrefs.Save();
            backgound.vibrator = false;
            vibratorButton.GetComponent<Text>().text = ("진동 : 꺼짐");
        }

        else if(PlayerPrefs.GetInt("vibrator") == 0)
        {
            PlayerPrefs.SetInt("vibrator", 1);
            PlayerPrefs.Save();
            backgound.vibrator = true;
            vibratorButton.GetComponent<Text>().text = ("진동 : 켜짐");
        }
    }

    private IEnumerator ShowEffect()
    {
        MenuManager.Instance.SetState(true);
        child.transform.SetParent(canvas.transform);
        rect.anchoredPosition = new Vector2(0, 0);
        child.transform.DOScale(scale, effectTime).SetEase(Ease.InOutQuint);
        show = true;
        yield return new WaitForSeconds(effectTime);
    }

    private IEnumerator HideEffect()
    {
        MenuManager.Instance.SetState(false);
        show = false;
        child.transform.DOScale(0.0f, effectTime).SetEase(Ease.InOutQuint);
        yield return new WaitForSeconds(effectTime);
        rect.anchoredPosition = new Vector2(-1000, -1000);
    }
}
