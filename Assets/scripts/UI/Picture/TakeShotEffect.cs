﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TakeShotEffect : MonoBehaviour
{
    public float animTime = 1.0f;
    public Color firstColor;
    // Start is called before the first frame update

    [Header("이 클래스의 이미지")]
    private Image img;

    private readonly float outStart = 0.0f;
    private readonly float outEnd = 1.0f;
    private readonly float inStart = 1.0f;
    private readonly float inEnd = 0.0f;
    private readonly float colorStart = 0.5f;
    private readonly float colorEnd = 1.0f;

    private float time = 0.0f;


    private List<SpriteRenderer> sprites = new List<SpriteRenderer>();

    public Image Img { get => img; set => img = value; }

    private void Awake()
    {
        Img = GetComponent<Image>();
    }

    void Start()
    {
        firstColor = Img.color;
    }

    public void Shot(GameObject cat)
    {
        //StartCoroutine("GrayFadeOutSprite", cat);
        StartCoroutine(GrayFadeOut(cat));
    }

    private void AddSpriteList(GameObject obj)
    {
        Transform objT = obj.GetComponent<Transform>();

        if (obj.name == "MadCat")
        {
            sprites.Add(obj.GetComponent<SpriteRenderer>());
            return;
        }

        if (obj.GetComponent<SpriteRenderer>() != null)
            sprites.Add(obj.GetComponent<SpriteRenderer>());
        for (int i = 0; i < objT.childCount - 1; i++)
        {
            if (objT.GetChild(i).transform.childCount > 0)
            {
                if (objT.GetChild(i).GetComponent<SpriteRenderer>() != null)
                    sprites.Add(objT.GetChild(i).GetComponent<SpriteRenderer>());
                for (int j = 0; j < objT.GetChild(i).transform.childCount; j++)
                {
                    if (objT.GetChild(i).transform.GetChild(j).childCount > 0)
                        for (int a = 0; a < objT.GetChild(i).transform.GetChild(j).transform.childCount; a++)
                            sprites.Add(objT.GetChild(i).transform.GetChild(j).transform.GetChild(a).GetComponent<SpriteRenderer>());

                    else
                        sprites.Add(objT.GetChild(i).transform.GetChild(j).GetComponent<SpriteRenderer>());
                }
            }

            else
                sprites.Add(objT.GetChild(i).GetComponent<SpriteRenderer>());
        }
    }

    private void AddSpriteList(Transform objT)
    {
        if (objT.GetComponent<SpriteRenderer>() != null)
        {
            sprites.Add(objT.GetComponent<SpriteRenderer>());
        }
        for (int i = 0; i < objT.childCount; i++)
            AddSpriteList(objT.GetChild(i));
    }


    public void LayerSetting(string layer)
    {
        for (int i = 0; i < sprites.Count; i++)
            sprites[i].sortingLayerName = layer;
    }

    public void ListReset()
    {
        for (int i = 0; i < sprites.Count; i++)
            sprites[i].color = new Color(255, 255, 255);
        sprites.Clear();
    }

    private IEnumerator GrayFadeOut(GameObject obj)
    {
        sprites.Clear();
        AddSpriteList(obj.transform);
        LayerSetting("PhotoCat");
        List<Color> colors = new List<Color>();
        Color color = GetComponent<Image>().color;
        time = 0.0f;
        color.a = Mathf.Lerp(inStart, inEnd, time);
        for (int i = 0; i < sprites.Count; i++)
        {
            colors.Add(sprites[i].color);
            colors[i] = new Color(Mathf.Lerp(colorStart, colorEnd, time),
                                  Mathf.Lerp(colorStart, colorEnd, time),
                                  Mathf.Lerp(colorStart, colorEnd, time));
            sprites[i].color = colors[i];
        }

        while (color.a > 0.0f)
        {
            time += Time.deltaTime / animTime * 3;

            color.a = Mathf.Lerp(inStart, inEnd, time);
            for (int i = 0; i < sprites.Count; i++)
            {
                colors[i] = new Color(Mathf.Lerp(colorStart, colorEnd, time),
                                      Mathf.Lerp(colorStart, colorEnd, time),
                                      Mathf.Lerp(colorStart, colorEnd, time));
                sprites[i].color = colors[i];
            }
            Img.color = color;
            yield return null;
        }
        for (int i = 0; i < sprites.Count; i++)
        {
            colors[i] = Color.white;
            sprites[i].color = colors[i];
        }
        yield return null;
    }

    private IEnumerator FadeIn()
    {
        Color color = GetComponent<Image>().color;
        time = 0.0f;

        color.a = Mathf.Lerp(inStart, inEnd, time);
        Debug.Log(color);

        while (color.a > 0.0f)
        {
            time += Time.deltaTime / (animTime);

            color.a = Mathf.Lerp(inStart, inEnd, time);

            GetComponent<Image>().color = color;
            yield return null;
        }
        yield return null;
    }

    private IEnumerator FadeOut()
    {
        Color color = GetComponent<Image>().color;
        time = 0.0f;
        color.a = Mathf.Lerp(outStart, outEnd, time);

        while (color.a < 1.0f)
        {
            time += Time.deltaTime / 0.2f;

            color.a = Mathf.Lerp(outStart, outEnd, time);

            GetComponent<Image>().color = color;
            yield return null;
        }
        StartCoroutine("FadeIn");
        yield return null;
    }
}
