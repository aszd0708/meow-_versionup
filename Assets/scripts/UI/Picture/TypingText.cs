﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TypingText : MonoBehaviour
{
    public Text originText;
    public PhotoShot speed;
    public float typingSpeed;
    private string text;
    private List<char> typing = new List<char>();

    public enum State
    {
        typing, end, idle
    }


    public State state;
    public void InputText()
    {
        text = originText.text;
        for(int i =0; i < text.Length; i++)
            typing.Add(text[i]);
        originText.text = null;
        StartCoroutine("Printing");
    }

    private IEnumerator Printing()
    {
        yield return new WaitForSeconds(speed.speed);
        state = State.typing;
        for(int i = 0; i < typing.Count; i++)
        {
            if(state == State.typing)
            {
                originText.text += typing[i];
                GetComponent<AudioSource>().Play();
                yield return new WaitForSeconds(typingSpeed);
            }

            else if(state == State.end)
            {
                EndTyping();
                break;
            }
        }
        typing.Clear();
        state = State.idle;
        yield return null;
    }

    private void EndTyping()
    {
        originText.text = text;
    }
}
