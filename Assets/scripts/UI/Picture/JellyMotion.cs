﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JellyMotion : MonoBehaviour
{
    private Vector2 originScale;
    // Start is called before the first frame update
    void Start()
    {
        originScale = gameObject.GetComponent<RectTransform>().localScale;
        GetComponent<RectTransform>().localScale = new Vector2(0.0f, 0.0f);
    }

    public void Motion(float time)
    {
        transform.DOScale(originScale, time).SetEase(Ease.OutElastic);
    }
}
