﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class FadeInLetterByLetter : MonoBehaviour
{
    public enum State
    {
        typing, end, idle
    }
    public State state;
    public PhotoShot end;
    [SerializeField] private Text textToUse;
    [SerializeField] private bool useThisText = false;
    [TextAreaAttribute(4, 15)]
    [SerializeField] private string textToShow;
    [SerializeField] private bool useTextText = false;
    [SerializeField] private float fadeSpeedMultiplier = 0.25f;
    [SerializeField] private bool fade;
    private float colorFloat = 0.1f;
    private int colorInt;
    private int letterCounter = 0;
    private string shownText;

    private void Start()
    {
        if (useThisText)
        {
            textToUse = GetComponent<Text>();
        }
    }
    private IEnumerator FadeInText()
    {
        int size = textToUse.fontSize;
        textToUse.fontSize = (int)SettingFontSize(GetFactor(), size);
        state = State.typing;
        while (letterCounter < textToShow.Length)
        {
            if (colorFloat < 1.0f)
            {
                colorFloat += Time.deltaTime * fadeSpeedMultiplier;
                colorInt = (int)(Mathf.Lerp(0.0f, 1.0f, colorFloat) * 255.0f);
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append(shownText);
                stringBuilder.Append("<color=\"#376189");
                stringBuilder.Append(string.Format("{0:X}", colorInt));
                stringBuilder.Append("\">");
                stringBuilder.Append(textToShow[letterCounter]);
                stringBuilder.Append("</color>");
                textToUse.text = stringBuilder.ToString();
                //textToUse.text = shownText + "<color=\"#376189" + string.Format("{0:X}", colorInt) + "\">" + textToShow[letterCounter] + "</color>";
            }
            else
            {
                colorFloat = 0.1f;
                shownText += textToShow[letterCounter];
                letterCounter++;
            }

            if(state == State.end)
            {
                textToUse.text = textToShow;
                state = State.idle;
                break;
            }
            yield return null;
        }
        end.state = PhotoShot.State.end;
        textToShow = "";
        shownText = "";
        letterCounter = 0;
        yield break;
    }

    public void SetText(string text)
    {
        textToShow = text;
    }

    public void Fade()
    {
        StartCoroutine(FadeInText());
    }

    public float SettingFontSize(float factor , float fontSize)
    {
        float width = Screen.width / factor;
        float height = Screen.height / factor;
        float value = ((width / height) / (16f / 9f)) * 20;

        return value;
    }

    private float GetFactor()
    {
        float width = Screen.width, height = Screen.height;
        float max = 0, min = 0;
        if (width > height)
        {
            max = height;
            min = width;
        }
        else
        {
            max = width;
            max = height;
        }
        while (max % min != 0)
        {
            float temp = max % min;
            max = min;
            min = temp;
        }

        return min;
    }
}
