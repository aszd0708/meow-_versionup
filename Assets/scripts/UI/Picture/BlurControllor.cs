﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlurControllor : MonoBehaviour
{
    public Image[] img = new Image[4];

    void Start()
    {
        for (int i = 0; i < img.Length; i++)
            img[i] = transform.GetChild(i).GetComponent<Image>();
        Off();
    }

    public void On(Vector3 pos)
    {
        for(int i = 0; i < img.Length; i++)
            img[i].enabled = true;
    }

    public void Off()
    {
        for (int i = 0; i < img.Length; i++)
            img[i].enabled = false;
    }
}