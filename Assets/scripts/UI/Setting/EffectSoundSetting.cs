﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class EffectSoundSetting : MonoBehaviour
{
    public Text text;
    
    // Start is called before the first frame update
    void Start()
    {
        int value;
        value = PlayerPrefs.GetInt("EffectSound", 1);
        Debug.Log("볼륨값 : " + value);
        Setting(value);
    }

    public void OnClick()
    {
        int value;
        value = PlayerPrefs.GetInt("EffectSound");
        if (value == 1)
        {
            PlayerPrefs.SetInt("EffectSound", 0);
        }

        else if (value == 0)
        {
            PlayerPrefs.SetInt("EffectSound", 1);
        }
        value = PlayerPrefs.GetInt("EffectSound");
        Setting(value);
    }

    private void Setting(int value)
    {
        Debug.Log("볼륨값 : " + value);
        if (value == 1)
        {
            text.text = "효과음 : 켜짐";
        }
        
        else if(value == 0)
        {
            text.text = "효과음 : 꺼짐";
        }
    }
}
