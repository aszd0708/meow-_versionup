﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CreateScrollList : MonoBehaviour
{
    [System.Serializable]
    public class Item
    {
        public string name;
        public bool isuse;
        public bool collect;
        public Sprite itemSpirte;
        public GameObject fieldObject;
        public GameObject dragItem;
        public Button.ButtonClickedEvent thingToDo;
    }

    public GameObject samepleButton;
    public Transform contentPanel;
    public List<Item> ItemList;

    public GameObject itemButtonUI;
    public FoldUI fold;

    public GameObject itemPanel;
    public Vector2 size;

    public GameObject createItemPanel;
    
    public void PopulateList()
    {
        foreach (var item in ItemList)
        {
            CreateItemButton(item);
        }
    }

    public void InsertItem(GameObject fieldObject, string nameLable, Sprite itemImage, GameObject itemDrag, bool collect)
    {
        Item createItem = new Item();
        createItem.fieldObject = fieldObject;
        createItem.name = nameLable;
        createItem.collect = collect;
        createItem.dragItem = itemDrag;
        createItem.itemSpirte = itemImage;
        CreateItemButtonCollect(createItem);
        if (fold.FoldNow == true)
        {
            itemButtonUI.transform.GetChild(0).GetComponent<Image>().enabled = true;    
        }
    }
                           
    public void CreateItemButton(Item item)
    {
        GameObject newButton = Instantiate(samepleButton) as GameObject;
        SampleButtonScript buttonScript = newButton.GetComponent<SampleButtonScript>();
        buttonScript.nameLable = item.name;
        newButton.transform.GetChild(0).GetComponent<Text>().text = buttonScript.nameLable;
        buttonScript.itemImage = item.itemSpirte;
        buttonScript.collect = item.collect;
        buttonScript.itemDrag.GetComponent<SpriteRenderer>().sprite = item.itemSpirte;
        buttonScript.fieldObject = item.fieldObject;
        buttonScript.itemDrag = item.dragItem;
        newButton.GetComponent<RectTransform>().sizeDelta = size;
        newButton.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        newButton.transform.GetChild(1).GetComponent<Image>().sprite = buttonScript.itemImage;
        newButton.transform.SetParent(contentPanel);
        
        ItemList.Add(item);
    }

    public void CreateItemButtonCollect(Item item)
    {
        GameObject newButton = Instantiate(samepleButton) as GameObject;
        SampleButtonScript buttonScript = newButton.GetComponent<SampleButtonScript>();
        buttonScript.nameLable = item.name;
        newButton.transform.GetChild(0).GetComponent<Text>().text = buttonScript.nameLable;
        buttonScript.itemImage = item.itemSpirte;
        buttonScript.collect = item.collect;
        buttonScript.itemDrag = item.dragItem;
        buttonScript.itemDrag.GetComponent<SpriteRenderer>().sprite = item.itemSpirte;
        buttonScript.fieldObject = item.fieldObject;
        newButton.GetComponent<RectTransform>().sizeDelta = size;
        newButton.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        newButton.transform.GetChild(1).GetComponent<Image>().sprite = buttonScript.itemImage;
        newButton.transform.SetParent(contentPanel);

        if (!fold.FoldNow)
            fold.CollectCorrection();

        newButton.GetComponent<Image>().enabled = true;
        newButton.transform.GetChild(1).GetComponent<Image>().enabled = true;

        ItemList.Add(item);
    }
}
