﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class ChanceNumImage : MonoBehaviour
{
    public Sprite[] num = new Sprite[10];
    public Text numText;
    public RectTransform minNumPos;
    public GameObject minNumImg;
    public ChanceEmpty empty;
    private int[] number = new int[2];
    // Start is called before the first frame update
    void Start()
    {
        NumImg();
    }
    
    private void Split()
    {
        int totalNum = Convert.ToInt32(numText.text);

        number[0] = totalNum / 10;
        number[1] = totalNum % 10;
    }

    public void NumImg()
    {
        if (Convert.ToInt32(numText.text) <= 0)
            numText.text = "0";
        Split();
        for(int i=0;i<10;i++)
        {
            if(number[0] == i)
            {
                transform.GetChild(0).gameObject.GetComponent<Image>().sprite = num[i];
            }

            if(number[1] == i)
            {
                transform.GetChild(1).gameObject.GetComponent<Image>().sprite = num[i];
            }
        }
    }

    public void CreatMinImg()
    {
        GameObject img = Instantiate(minNumImg);
        RectTransform imgRect = img.GetComponent<RectTransform>();

        imgRect.SetParent(minNumPos);
        imgRect.localPosition = new Vector3(0, 0, 0);
        imgRect.localScale = new Vector3(1, 1, 1);
        imgRect.sizeDelta = new Vector2(1, 1);
    }

    public void MinusChance()
    {
        string chance = numText.text;
        chance = Convert.ToString(Convert.ToInt32(chance) - 1);
        EncryptedPlayerPrefs.SetInt(empty.StageNumChance, Convert.ToInt32(chance));
        numText.text = chance;
        CreatMinImg();
        NumImg();
    }
}
