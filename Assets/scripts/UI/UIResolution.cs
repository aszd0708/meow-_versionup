﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIResolution : MonoBehaviour
{
    public int SetWidth, SetHeight;
    public bool fullscreen;

    void awake()
    {
        Screen.SetResolution(Screen.width, Screen.width *SetWidth /SetHeight, fullscreen);
    }
}
