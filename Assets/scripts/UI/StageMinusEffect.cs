﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 마이너스 이펙트
/// </summary>
public class StageMinusEffect : MonoBehaviour
{
    [Header("애니메이션 시간")]
    public float animTime = 1.0f;

    [Header("이미지들")]
    public Image[] fadeImage = new Image[2];

    private RectTransform rect;

    private float inStart = 1.0f;
    private float inEnd = 0.0f;
    private float time = 0.0f;
    // Start is called before the first frame update

    private void Awake()
    {
        rect = GetComponent<RectTransform>();
    }

    void Start()
    {
        inStart = animTime;
    }

    private void OnDisable()
    {
        for(int i = 0; i < fadeImage.Length; i++)
        {
            Color color = fadeImage[i].color;
            color.a = 1;
            fadeImage[i].color = color;
        }        
    }

    public void StartEffect(RectTransform pose)
    {
        StartCoroutine(_StartEffect(pose.localPosition));
    }

    private IEnumerator _StartEffect(Vector3 pose)
    {
        rect.localPosition = Vector3.zero;
        rect.sizeDelta = Vector2.one;
        Color[] color = new Color[2];
        color[0] = fadeImage[0].color;
        color[1] = fadeImage[1].color;

        for (int i = 0; i < 2; i++)
            fadeImage[i].enabled = true;
        time = 0.0f;
        for(int i = 0; i < 2; i ++)
            color[i].a = Mathf.Lerp(inStart, inEnd, time);

        rect.DOMoveY(transform.position.y - 40.0f, animTime);

        while (color[0].a > 0.0f)
        {
            time += Time.deltaTime / animTime;
            for (int i = 0; i < 2; i++)
                color[i].a = Mathf.Lerp(inStart, inEnd, time);

            for (int i = 0; i < 2; i++)
                fadeImage[i].color = color[i];

            yield return null;
        }

        PoolingManager.Instance.SetPool(gameObject, "MinusEffect");
        yield break;
    }
}
