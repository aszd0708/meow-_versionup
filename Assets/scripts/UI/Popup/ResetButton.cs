﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResetButton : MonoBehaviour
{
    [Header("리셋할 패널")]
    public RectTransform ResetPopup;

    [Header("막아야 하는 버튼")]
    public Button[] exit; 
    
    public void OnClick()
    {
        for (int i = 0; i < exit.Length; i++)
            exit[i].enabled = false;
        PopupManager.Instance.OpenPopup(ResetPopup);
    }
    
    public void HideClick()
    {
        for (int i = 0; i < exit.Length; i++)
            exit[i].enabled = true;
        PopupManager.Instance.ClosedPopup(ResetPopup);
    }
}