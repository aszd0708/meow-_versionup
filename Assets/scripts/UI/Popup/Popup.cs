﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup : MonoBehaviour
{
    static public void PopupCreate(GameObject popup, float speed)
    {
        RectTransform popupPos = popup.GetComponent<RectTransform>();
        popupPos.DOScale(new Vector3(1, 1, 1), speed).SetEase(Ease.OutElastic);
    }

    static public void PopupHide(GameObject popup, float speed)
    {
        RectTransform popupPos = popup.GetComponent<RectTransform>();
        popupPos.DOScale(new Vector3(0, 0, 0), speed);
    }

    static private IEnumerator PopupCreateMotion(RectTransform popupPos)
    {
        yield return null;
    }

    static private IEnumerator PopupHideMotion(RectTransform popupPos)
    {
        yield return new WaitForSeconds(0.5f);
    }
}
