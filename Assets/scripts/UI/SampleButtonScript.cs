﻿using BitBenderGames;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SampleButtonScript : MonoBehaviour
{
    public GameObject fieldObject;
    public Button button;
    public string nameLable;
    public Sprite itemImage;
    public GameObject itemDrag;
    public bool collect;

    public GameObject selectCircle;

    public GameObject bag;
    private Vector3 bagPos;
    private bool col;
    //public GameObject itme;

    private void Start()
    {
        col = false;
    }

    public void OnClick()
    {
        collect = true;

        if (!col)
        {
            transform.DOPause();
            col = true;
            if (gameObject.name == "Butterfly")
                gameObject.GetComponent<ButterflyMove>().state = ButterflyMove.State.idle;
            GameObject circle = Instantiate(selectCircle, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, -8), Quaternion.identity);
            circle.GetComponent<SpriteRenderer>().sortingLayerName = "8";
            circle.GetComponent<SpriteRenderer>().sortingOrder = 1;
            GetComponent<SpriteRenderer>().sortingLayerName = "8";
            GetComponent<SpriteRenderer>().sortingOrder = 2;
            if (!(GetComponent<CircleCollider2D>() == null))
                GetComponent<CircleCollider2D>().enabled = false;
            else if (!(GetComponent<BoxCollider2D>() == null))
                GetComponent<BoxCollider2D>().enabled = false;
            transform.position = new Vector3(transform.position.x, transform.position.y, -9.0f);
            StartCoroutine(GoBag());
        }

        else return;
    }

    public void LoadGo()
    {
        collect = true;
        FindObjectOfType<CreateScrollList>().InsertItem(fieldObject, nameLable, itemImage, itemDrag, collect);
        transform.position = new Vector3(transform.position.x, transform.position.y, -9.0f);
    }

    public void Destroy()
    {
        Destroy(gameObject);
    }

    private IEnumerator GoBag()
    {
        yield return new WaitForSeconds(1.0f);
        bagPos = Camera.main.ScreenToWorldPoint(bag.transform.position);
        //DOTween.Sequence().Append(transform.DOMoveX(bagPos.x, 1.0f).SetEase(Ease.Linear)).Join(transform.DOMoveY(bagPos.y, 1.0f).SetEase(Ease.InBack));
        //transform.DOMoveX(bagPos.x, 1.0f).SetEase(Ease.InExpo);
        //transform.DOMoveY(bagPos.y, 1.0f).SetEase(Ease.OutElastic);
        //GetComponent<Image>().enabled = true;
        //transform.GetChild(1).gameObject.GetComponent<Image>().enabled = true;
        if (!(GetComponent<CircleCollider2D>() == null))
            gameObject.GetComponent<CircleCollider2D>().enabled = false;
        else if (!(GetComponent<BoxCollider2D>() == null))
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
        transform.DOJump(new Vector3(bagPos.x, bagPos.y, -9.0f), 5.0f, 1, 1.0f);
        yield return new WaitForSeconds(1.0f);
        FindObjectOfType<CreateScrollList>().InsertItem(fieldObject, nameLable, itemImage, itemDrag, collect);
        GetComponent<ItemCheck>().ColletObject();
        Destroy(gameObject);
    }
}