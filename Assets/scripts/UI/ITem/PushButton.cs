﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PushButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
{
    private GameObject item;
    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("눌림");
        item = gameObject.transform.GetChild(1).gameObject;

        Instantiate(item);
        item.GetComponent<UIItemDragHandler>().enabled = true;
        item.transform.position = new Vector3(Input.mousePosition.x, Input.mousePosition.y + 169, Input.mousePosition.z);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        //Destroy(item);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        return;
    }
}
