﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisableN : MonoBehaviour
{
    private Vector2 originPos;
    void Start()
    {
        originPos = GetComponent<RectTransform>().anchoredPosition;
        Invoke("StartMove", 0.5f);
    }

    public void Image()
    {
        GetComponent<Image>().enabled = false;
    }

    private void StartMove()
    {
        GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
        Image();
    }
}
