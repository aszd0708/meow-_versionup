﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class FoldUI : Singleton<FoldUI>
{
    private bool fold = true;
    [SerializeField]
    private RectTransform rect;
    public float speed;
    public float foldSpeed;
    private bool end;
    public float size = 0;
    public float fSize;
    public GameObject itemButtonUI;
    public GameObject contents;
    public GameObject item;
    public Vector2 buttonSize;

    public float foldSize = 136;

    private RectTransform contentsRect;

    [SerializeField]
    [Header("New 마크 오브젝트")]
    private GameObject newMarkObj;

    public bool FoldNow { get => fold; set => fold = value; }
    public Vector2 ButtonSize { get => GetButtonSize(); set => buttonSize = value; }

    [SerializeField]
    [Header("일반 버튼 Rect")]
    private RectTransform defaultBtnRect;

    // Start is called before the first frame update

    protected override void Awake()
    {
        if(rect == null)
        {
            rect = GetComponent<RectTransform>();
        }
        contentsRect = rect.GetComponent<RectTransform>();
    }

    void Start()
    {
        FoldNow = true;
        newMarkObj.SetActive(false);
        Invoke("Fold", 0.1f);
    }

    public void CollectItem()
    {
        if (contents.transform.childCount <= 1)
            speed = foldSpeed;
        else
        {
            speed = contents.transform.childCount * foldSpeed;
            speed /= 2;
        }

        size = rect.sizeDelta.x/* - ButtonSize.x*/;

        rect.DOAnchorPosX(-size, speed).SetEase(Ease.InOutBack);
        fSize = size;
        size = 0;

        FoldNow = false;
        //if (contents.transform.childCount <= 1)
        //{
        //speed = foldSpeed;
        //fSize = 0;
        //}
        //else
        //{
        //speed = (contents.transform.childCount) * foldSpeed;
        //speed /= 2;

        //size = rect.sizeDelta.x - ButtonSize.x;

        //fSize = size;
        //}

        //rect.DOAnchorPosX(0, speed).SetEase(Ease.InOutBack);
        //size = 0;
        //FoldNow = false;
    }

    public void Fold()
    {
        if (contents.transform.childCount <= 1)
            speed = foldSpeed;
        else
        {
            //for (int i = 0; i < contents.transform.childCount; i++)
            speed = (contents.transform.childCount + 1) * foldSpeed;
            speed /= 2;
        }

        if (FoldNow) Correction();

        else FoldBag();

        FoldNow = !FoldNow;
    }

    /// <summary>
    /// 접혀있던 UI를 오픈하는 함수
    /// </summary>
    public void OpenUI()
    {
        if (contents.transform.childCount <= 1)
            speed = foldSpeed;
        else
        {
            for (int i = 0; i < contents.transform.childCount; i++)
                speed = i * foldSpeed;
            speed /= 2;
        }

        Correction();
    }

    /// <summary>
    /// 접어있는 UI를 펼치는 함수
    /// </summary>
    public void Correction()
    {
        size = contentsRect.sizeDelta.x - ButtonSize.x;

        rect.DOAnchorPosX(-size, speed).SetEase(Ease.InOutBack);
        fSize = size;
        size = 0;
    }

    public void ReCorrection()
    {
        rect.localPosition = new Vector3(contentsRect.sizeDelta.x - ButtonSize.x, rect.localPosition.y, 0);
    }

    public void CollectCorrection()
    {
        StartCoroutine(SetPlace());
    }

    private IEnumerator SetPlace()
    {
        yield return new WaitForSeconds(0.1f);
        float size = contents.transform.GetChild(contents.transform.childCount - 1).GetComponent<RectTransform>().anchoredPosition.x;
        rect.DOAnchorPosX(-size, speed).SetEase(Ease.OutElastic);
        yield break;
    }

    public void FoldBag()
    {
        //if (contents.transform.childCount <= 1)
        //    rect.DOAnchorPosX(contentsRect.sizeDelta.x * 4, speed).SetEase(Ease.InOutBack);
        //else
        //    rect.DOAnchorPosX(contentsRect.sizeDelta.x - buttonSize.x * contents.transform.childCount, speed).SetEase(Ease.InOutBack);
        rect.DOAnchorPosX(foldSize, speed).SetEase(Ease.InOutBack);
    }

    /// <summary>
    /// 새로 만든 버튼 rect 값들 수정 후 넣어주는 함수
    /// 알아서 자식으로 넣어줌
    /// </summary>
    /// <param name="button">원하는 버튼값</param>
    public void SetSize(GameObject button)
    {
        button.transform.SetParent(transform);

        RectTransform rect = button.GetComponent<RectTransform>();

        rect.sizeDelta = ButtonSize;
        rect.localScale = Vector3.one;
        rect.pivot = Vector2.zero;
    }

    public void SetNewMark()
    {
        if (FoldNow) newMarkObj.SetActive(true);
    }

    public Vector2 GetButtonSize()
    {
        return defaultBtnRect.sizeDelta;
    }
}
