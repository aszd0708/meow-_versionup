﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CatHouseNewMark : MonoBehaviour
{
    [Header("값을 갖고오는 프로퍼티")]
    [Header("이건 Get만 사용하는걸로")]
    private bool haveNewAnimal;
    public bool HaveNewAnimal
    {
        get
        {
            int value = PlayerPrefs.GetInt("HaveNewAnimal", 0);

            if (value == 1) haveNewAnimal = true;
            else haveNewAnimal = false;

            return haveNewAnimal;
        }
        set => haveNewAnimal = value;
    }

    private Image newMark;

    private void Awake()
    {
        newMark = GetComponent<Image>();
    }

    private void Start()
    {
        SetNewMark();
    }

    private void SetNewMark()
    {
        if (HaveNewAnimal) newMark.enabled = true;
        else newMark.enabled = false;
    }

    public void SetData(bool value)
    {
        if(value) PlayerPrefs.SetInt("HaveNewAnimal", 1);
        else PlayerPrefs.SetInt("HaveNewAnimal", 0);
        SetNewMark();
    }
}
