﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMove : MonoBehaviour
{
    public void GoMain()
    {
        FindObjectOfType<FadeInOut>().FadeOutF();
        StartCoroutine(ChangeScene("MainMenu"));
    }

    public void TutorialEnd()
    {
        PlayerPrefs.SetInt("TutorialEnd", 1);
        GoMain();
    }

    public void GoRoom()
    {
        FindObjectOfType<FadeInOut>().FadeOutF();

        PlayerPrefs.SetString("prevScene", SceneManager.GetActiveScene().name);
        PlayerPrefs.SetString("CatPos", SceneManager.GetActiveScene().name);        
        PlayerPrefs.Save();

        StartCoroutine(ChangeScene("Cat'sRoom"));
    }

    public void ChangeStage(string stageName)
    {
        StartCoroutine(ChangeScene(stageName));
    }

    private IEnumerator ChangeScene(string sceneName)
    {
        yield return new WaitForSeconds(2.0f);
        SceneManager.LoadScene(sceneName);
    }
}
