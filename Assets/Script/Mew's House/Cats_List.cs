﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

/*
 * 고양이 이름 배경 숫자 요소를 이용하여 리스트 제작,
 * 리스트 세이브 로드
 * 고양이 추가시 고양이에 부여된 숫자에 의해 순서대로 배치
 */

public class Cats_List : MonoBehaviour
{
    [Serializable]
    public class CatData
    {
        private string cat;
        private string background;
        private int num;

        public CatData(string cat, string background, int num)
        {
            this.cat = cat;
            this.background = background;
            this.num = num;
        }

        public string Cat
        {
            get{return cat;}
        }

        public string Background
        {
            get{return background;}
        }

        public int Number
        {
            get{return num;}
        }
    }
    
    List<CatData> Cats = new List<CatData>();
    List<CatData> resetCats = new List<CatData>();
    
    public void CatsListAdd(CatData cat)
    {
        int num;
        Debug.Log(Cats.Count);
        if (Cats.Count == 0)
        {
            Cats.Insert(0, cat);
        }

        else
        {
            for (int i = 0; i < Cats.Count; i++)
            {
                num = Cats[i].Number;
                if (num > cat.Number)
                {
                    Cats.Insert(i, cat);
                    break;
                }

                else
                {
                    Cats.Insert(0, cat);
                    break;
                }
            }
        }
        SaveCats();
        //LogState();
    }

    public void CombineCatInfo(string cat, string background, int num)
    {
        CatData catInfo = new CatData(cat, background, num);
        Debug.Log("감");
        CatsListAdd(catInfo);
    }

    // Save & Load !
    public void SaveCats()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/CatCollectInfo.dat");

        List<CatData> saveCats = new List<CatData>();

        saveCats = Cats;
        
        bf.Serialize(file, saveCats);
        file.Close();
        Debug.Log(Cats.Count + "세이브 성공");
    }
    // 고먐미 로드 함수
    public void LoadCats()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.OpenRead(Application.persistentDataPath + "/CatCollectInfo.dat");

        if (file != null && file.Length > 0)
        {
            // 파일 역직렬화하여 B에 담기
            List<CatData> saveCats = (List<CatData>)bf.Deserialize(file);
            // B --> A에 할당
            Cats = saveCats;
            Debug.Log("로드 성공");
            file.Close();
        }
        else return;
    }

    // 고양이 세이브 인수 받아서 로드 하는 함수
    //public void LoadCats(List<CatData> Cats)
    //{
    //    BinaryFormatter bf = new BinaryFormatter();
    //    FileStream file = File.OpenRead(Application.persistentDataPath + "/CatCollectInfo.dat");

    //    if (file != null && file.Length > 0)
    //    {
    //        // 파일 역직렬화하여 B에 담기
    //        List<CatData> saveCats = (List<CatData>)bf.Deserialize(file);

    //        // B --> A에 할당
    //        Cats = saveCats;

    //        Debug.Log(Cats.Count);
    //        Debug.Log("로드 성공");
    //    }
    //}
    // 고먐미 세미브 초기화 함수
    public void ResetSaveData()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/CatCollectInfo.dat");
        if (System.IO.File.Exists(Application.persistentDataPath + "/CatCollectInfo.dat"))
        {
            List<CatData> saveCats = new List<CatData>();

            saveCats = resetCats;

            bf.Serialize(file, saveCats);
            file.Close();
            LoadCats();
            Debug.Log("세이브 초기화 성공");
            file.Close();
        }

        else return;
        
    }
    // 고양이 이름 문자열 전송 함수
    public string SendCatName(int num)
    {
        int i = CheckCat(num) ;
        return Cats[i].Cat;
    }

    // 고양이 배경 문자열 전송 함수
    public string SendCatBackground(int num)
    {
        int i = CheckCat(num);
        return Cats[i].Background;
    }

    public int SendCatNumber(int num)
    {
        int i = CheckCat(num);
        return Cats[i].Number;
    }

    // 고먐미 검사 함수
    private int CheckCat(int num)
    {
        int i;
        for (i = 0; i < Cats.Count; i++)
        {
            if (Cats[i].Number == num)
                break;
        }
        return i;
    }

    void Start()
    {
        LoadCats();
    }
    
    public void LogState()
    {
        Debug.Log(Cats[0].Number);
    }
}
