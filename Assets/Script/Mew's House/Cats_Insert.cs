﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cats_Insert : MonoBehaviour
{
    Vector3 touchPos;
    GameObject touchCat;
    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(touchPos, Vector2.zero, 0.0f);

        if(Input.GetMouseButtonDown(0))
        {
            if (hit.collider != null)
            {
                if(hit.collider.name == "Item")
                {
                    hit.collider.GetComponent<PlayingItem>().touch = true;
                }

                else if(hit.collider.name == "CathouseCat")
                {
                    hit.collider.GetComponent<CatHouseCat>().OnClick();
                }

                // 이게 어떤거에 필요한질 모르겠음
                /*
                else
                {
                    Debug.Log(hit.collider.name);
                    touchCat = hit.collider.gameObject;

                    touchCat.GetComponent<Send_CatInfo>().SendCatInfo();
                    Debug.Log("보내짐");
                }
                */
            }
        }
    }
}
