﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.UI;

public class LoadCatList_Show : MonoBehaviour
{
    List<Cats_List.CatData> Cats;
    ScrollRectSnap catsScroll;
    //private Button button;
    public GameObject uiRoot;
    public int num = 0;
    public float[] x = new float[2];
    public float[] y = new float[2];

    // Use this for initialization
    void Awake()
    {
        catsScroll = GameObject.Find("GameController").GetComponent<ScrollRectSnap>();
        LoadCats();
        //Debug.Log(Cats.Count);
        //Debug.Log(Cats[1].Cat);

        // 리스트 고양이 이름에 따라 프리팹 불러와 생성 (반대로 생성되서 고쳐야함)
        for (int i = 0; i < Cats.Count; i++)
        {
            Button child = Instantiate((GameObject)Resources.Load("Cats'House/Prefabs/" + Cats[i].Cat, typeof(GameObject))).GetComponent<Button>();
            child.transform.parent = uiRoot.transform;
            child.GetComponent<CatButton>().i = i;
            child.GetComponent<RectTransform>().anchoredPosition = new Vector3(i * 700, 0, 0);
            child.gameObject.GetComponent<CenterSize>().x = x;
            child.gameObject.GetComponent<CenterSize>().y = y;
            catsScroll.GetCatScroll(child, i);
            num++;
        }
    }

    // 세이브 불러오는 함수
    public void LoadCats()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.OpenRead(Application.persistentDataPath + "/CatCollectInfo.dat");

        if (file != null && file.Length > 0)
        {
            // 파일 역직렬화하여 B에 담기
            List<Cats_List.CatData> saveCats = (List<Cats_List.CatData>)bf.Deserialize(file);

            // B --> A에 할당
            Cats = saveCats;
        }

        else return;
    }

    //여기서 부터 고양이 리스트 요소 한개씩 반환하는 함수

    public string SendCatName(int i)
    {
        return Cats[i].Cat;
    }

    public int SendCatNumber(int i)
    {
        return Cats[i].Number;
    }

    public string SendCatBackground(int i)
    {
        return Cats[i].Background;
    }

    public int SendNumer()
    {
        return num;
    }

    public int SendCatCount()
    {
        return Cats.Count;
    }
    
    public int SendNumber(int i)
    {
        return i;
    }
}
