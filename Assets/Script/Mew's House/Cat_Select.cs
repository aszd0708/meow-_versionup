﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class Cat_Select : MonoBehaviour
{
    public float speed = 0.1f;
    public float distance = 4.0f;
    public GameObject up;
    public GameObject down;

    Cats_List catList = GameObject.Find("Cat List").GetComponent<Cats_List>();

    List<Cats_List.CatData> CatsData;

    void Start()
    {
        LoadCats();
    }

    void Update()
    {

    }

    private void LoadCats()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.OpenRead(Application.persistentDataPath + "/CatCollectInfo.dat");

        if (file != null && file.Length > 0)
        {
            // 파일 역직렬화하여 B에 담기
            List<Cats_List.CatData> saveCats = (List<Cats_List.CatData>)bf.Deserialize(file);

            // B --> A에 할당
            CatsData = saveCats;

            Debug.Log(CatsData.Count);
            Debug.Log("로드 성공");
        }
    }

    private void UpSlide()
    {
        
    }
}
