﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reset_Save : MonoBehaviour
{
    Cats_List saveFile;
    // Use this for initialization
    void Start()
    {
        saveFile = GameObject.Find("Cat List").GetComponent<Cats_List>();
    }

    void PushButton()
    {
        saveFile.ResetSaveData();
    }
}
