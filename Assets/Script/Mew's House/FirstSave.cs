﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstSave : MonoBehaviour
{
    public Cats_List catList;
    void Awake()
    {
        if (!System.IO.File.Exists(Application.persistentDataPath + "/CatCollectInfo.dat"))
        {
            catList.SaveCats();
        }
    }
}
