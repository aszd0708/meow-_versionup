﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
/*
* 버튼 누르기 전으로 초기화 해주는 함수
*/
public class Reset : MonoBehaviour
{
    public bool room = false;
    public AnimalHouseButton catButton;

    //void Update()
    //{
    //    if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
    //        return;
    //    if (Input.touchCount > 0)
    //    {
    //        for (int i = 0; i < Input.touchCount; i++)
    //        {
    //            if (EventSystem.current.IsPointerOverGameObject(i))
    //                return;
    //        }
    //    }

    //    if (room)
    //    {
    //        if (Input.GetMouseButtonDown(0))
    //        {
    //            catButton.ClickClear();
    //        }
    //    }
    //}
    public void OnClick()
    {
        if(room)
            catButton.ClickClear();
    }
}
