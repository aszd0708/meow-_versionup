﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatButtomSize : MonoBehaviour
{
    /*
     * 가운데에서 멀어진 만큼 크기 줄이고 싶었으나 어캐하는지 감을 못잡겠음
     * 추후 추가함
     * 아아 기획서 보고 추가함.
     */
    public float size;
    private GameObject Center;

    // Use this for initialization
    void Start()
    {
        Center = GameObject.Find("CenterToCompare");
    }
    
    void Update()
    {
        float distance = gameObject.GetComponent<RectTransform>().anchoredPosition.x - Center.GetComponent<RectTransform>().anchoredPosition.x;

        float width = gameObject.GetComponent<RectTransform>().rect.width - distance * size;
        float height = gameObject.GetComponent<RectTransform>().rect.height - distance * size;
        width = Mathf.Clamp(width, 500, 300);
        height = Mathf.Clamp(height, 500, 300);

        gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(width, height);
    }
}
