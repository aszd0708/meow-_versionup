﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

public class ScrollRectSnap : MonoBehaviour
{
    // Public Variables
    public RectTransform panel; // 스크롤판넬 
    public List<Button> cat;
    public RectTransform center; // 고양이 중앙
    public int startCat = 1;
    public float size = 0.1f;
    public Text catName;
    public float buttonDis = 1200;

    // 이 버튼 누르면 그 버튼 가운데로 감
    public GameObject centerBtn;

    private bool move = false;
    // Private Variables
    private float[] distance; // 모든 고양이를 중앙과 거리
    private float[] distReposition;
    private bool dragging = false; // 판넬을 그래그 할때 참이 됨
    private int catDistance; // 고양이 사이 거리를 고정
    private int minCatNum; // 중앙과 가장 적은 거리와 함께 있는 고양이 숫자를 고정한다.
    [SerializeField]
    private int catLength;
    private bool messageSend = false;
    private float lerpSpeed = 5.0f;
    private bool targetNearestCat = true; // 참 되면 이것은 가까운 버튼이 된다.


    void Start()
    {
       
    }

    void Update()
    {
        if (catLength == 0)
            return;
        if (move)
            return;
        for (int i = 0; i < cat.Count; i++)
        {
            distReposition[i] = center.GetComponent<RectTransform>().position.x - cat[i].GetComponent<RectTransform>().position.x;
            distance[i] = Mathf.Abs(distReposition[i]);

            if (distReposition[i] > buttonDis)
            {
                float curX = cat[i].GetComponent<RectTransform>().anchoredPosition.x;
                float curY = cat[i].GetComponent<RectTransform>().anchoredPosition.y;

                Vector2 newAnchoredPos = new Vector2(curX + (catLength * catDistance), curY);
                cat[i].GetComponent<RectTransform>().anchoredPosition = newAnchoredPos;
            }

            if (distReposition[i] < -buttonDis)
            {
                float curX = cat[i].GetComponent<RectTransform>().anchoredPosition.x;
                float curY = cat[i].GetComponent<RectTransform>().anchoredPosition.y;

                Vector2 newAnchoredPos = new Vector2(curX - (catLength * catDistance), curY);
                cat[i].GetComponent<RectTransform>().anchoredPosition = newAnchoredPos;
            }
        }

        if (targetNearestCat)
        {
            float minDistance = Mathf.Min(distance); // 적은 거리를 갖고온다
            int num = minCatNum;
            for (int a = 0; a < cat.Count; a++)
            {
                if (minDistance == distance[a])
                {
                    minCatNum = a;
                }
            }
            if (minCatNum != num)
                VibratorControllor.CatRoomVib();
        }

        // 드래그를 안하고 있을 때 중앙으로 오게 함
        if (!dragging)
        {
            LerpToCat(-cat[minCatNum].GetComponent<RectTransform>().anchoredPosition.x);
        }

        catName.text = cat[minCatNum].GetComponent<AnimalHouseButton>().catName;
    }

    //중앙으로 가까운게 천천히 중앙으로 이동
    void LerpToCat(float position)
    {
        float newX = Mathf.Lerp(panel.anchoredPosition.x, position, Time.deltaTime * lerpSpeed);

        if (Mathf.Abs(position - newX) < 5.0f)
        {
            lerpSpeed = 25.0f;
        }

        if (Mathf.Abs(newX) >= Mathf.Abs(position) - 1.0f && Mathf.Abs(newX) <= Mathf.Abs(position) + 1 && !messageSend)
        {
            messageSend = true;
        }

        Vector2 newPosition = new Vector2(newX, panel.anchoredPosition.y);
        

        panel.anchoredPosition = newPosition;
    }

    public void MoveCenter(float position)
    {
        //Vector2 newPos = new Vector2(position, panel.anchoredPosition.y);
        //panel.DOAnchorPos(newPos, 1.0f);
        Debug.Log("눌렀다ㅏㅏㅏㅏ");
        StartCoroutine(MoveCenter(position, 0.5f));
    }

    public void MoveCenter(RectTransform rect)
    {
        //Vector2 newPos = new Vector2(position, panel.anchoredPosition.y);
        //panel.DOAnchorPos(newPos, 1.0f);
        Debug.Log("눌렀다ㅏㅏㅏㅏ");
        Vector2 newPos = new Vector2(-rect.anchoredPosition.x, panel.anchoredPosition.y);
        panel.DOAnchorPos(newPos, 0.5f);
    }

    private IEnumerator MoveCenter(float pos, float time)
    {
        move = true;
        Vector2 newPos = new Vector2(pos, panel.anchoredPosition.y);
        panel.DOAnchorPos(newPos, 1.0f);
        yield return new WaitForSeconds(time);
        move = false;
    }

    private IEnumerator MoveToCentor(float pos, float lerpTime)
    {
        float move = pos;
        float time = 0.0f;

        float toX = panel.anchoredPosition.x;

        Debug.Log("움직이기 전이다ㅏㅏㅏㅏ");
        while (Mathf.Abs(pos - toX) <= 0.05f)
        {
            Debug.Log("움직인다ㅏㅏㅏㅏ");
            time += Time.deltaTime / lerpTime;

            move = Mathf.Lerp(panel.anchoredPosition.x,pos, time);
            
            yield return null;
        }
    }

    public void StartDrag()
    {
        messageSend = false;
        lerpSpeed = 5.0f;
        dragging = true;

        targetNearestCat = true;
    }

    public void EndDrag()
    {
        dragging = false;
    }

    public void GoToButton(int buttonIndex)
    {
        targetNearestCat = false;
        minCatNum = buttonIndex - 1;
    }

    public void GetCatScroll(Button catButton, int a)
    {
        cat.Add(catButton);
    }

    public void SetSnap()
    {
        panel.localPosition = new Vector2(-0.1f, panel.localPosition.y);
        catLength = cat.Count;
        if (catLength == 0)
            return;

        distance = new float[catLength];
        distReposition = new float[catLength];

        // 고양이 사이에 거리를 구한다
        if (catLength > 1)
            catDistance = (int)Mathf.Abs(cat[1].GetComponent<RectTransform>().anchoredPosition.x - cat[0].GetComponent<RectTransform>().anchoredPosition.x);

        panel.anchoredPosition = new Vector2((startCat - 1) * -catDistance, panel.anchoredPosition.y);
    }
}