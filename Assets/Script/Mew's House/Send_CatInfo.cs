﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Send_CatInfo : MonoBehaviour
{
    private Cats_List Cats;
    
    public string cat;
    public string background;
    public int num;
    private bool collect;
    
    void Start()
    {
        Cats = GameObject.Find("Cats List").GetComponent<Cats_List>();
    }

    public void SendCatInfo()
    {
        Cats.CombineCatInfo(cat, background, num);
        collect = true;
        Cats.SaveCats();
    }
}
