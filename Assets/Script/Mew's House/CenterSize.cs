﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CenterSize : MonoBehaviour
{
    private bool nearFalseEnabled = false;
    private bool nearTrueEnabled = false;

    private GameObject center;
    private float distance;

    public float[] x = new float[2];
    public float[] y = new float[2];
    public float size = 10f;
    private float[] childX = new float[2];
    private float[] childY = new float[2];
    private bool vib;

    private RectTransform rect;

    private Button button;
    private ButtonAction info;

    private ScrollRectSnap snap;

    public bool isCollect = false;

    private void Awake()
    {
        rect = GetComponent<RectTransform>();
        button = GetComponent<Button>();
        info = GetComponent<ButtonAction>();
        snap = FindObjectOfType<ScrollRectSnap>();
    }
    // Use this for initialization
    void Start()
    {
        float i_width = Screen.width;
        x[0] = (300f * i_width) / 2220f;
        x[1] = 2 * x[0];
        y = x;

        vib = false;
        center = GameObject.Find("CenterToCompare");
        if (transform.parent.GetChild(0).gameObject == gameObject)
        {
            GetComponent<RectTransform>().sizeDelta = new Vector2(x[1], x[1]);
            transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(y[1], y[1]);
        }
        StartCoroutine(Size());
    }

    private IEnumerator Size()
    {
        RectTransform childRect = transform.GetChild(0).GetComponent<RectTransform>();
        while (true)
        {
            distance = center.GetComponent<RectTransform>().position.x - rect.position.x;
            distance = Mathf.Abs(distance);

            if (distance < 200)
            {
                if (!isCollect)
                {
                    button.enabled = false;
                }
                SetBtn(true);
                nearFalseEnabled = true;
                rect.sizeDelta = new Vector2(rect.sizeDelta.x + distance * size*2.5f, rect.sizeDelta.y + distance * size * 2.5f);
                childRect.sizeDelta = new Vector2(rect.sizeDelta.x + distance * size * 2.5f, rect.sizeDelta.y + distance * size * 2.5f);
                if(rect.sizeDelta.x > x[1])
                {
                    rect.sizeDelta = new Vector2(rect.sizeDelta.x + size * 2.5f, rect.sizeDelta.y + size * 2.5f);
                    childRect.sizeDelta = new Vector2(rect.sizeDelta.x + size * 2.5f, rect.sizeDelta.y + size * 2.5f);
                }
            }

            else
            {
                button.enabled = true;
                    SetBtn(false);
                nearTrueEnabled = true;
                rect.sizeDelta = new Vector2(rect.sizeDelta.x - distance * size, rect.sizeDelta.y - distance * size);
                childRect.sizeDelta = new Vector2(rect.sizeDelta.x + distance * size, rect.sizeDelta.y + distance * size);
            }
            float clampX = Mathf.Clamp(childRect.sizeDelta.x, childX[0], childX[1]);
            float clampY = Mathf.Clamp(childRect.sizeDelta.y, childY[0], childY[1]);
            childRect.sizeDelta = new Vector2(clampX, clampY);              
            
            rect.sizeDelta = new Vector2(Mathf.Clamp(rect.sizeDelta.x, x[0], x[1]), Mathf.Clamp(rect.sizeDelta.y, y[0], y[1]));
            yield return null;
        }
    }

    private void SetBtn(bool near)
    {
        if(near)
        {
            button.onClick.SetPersistentListenerState(0, UnityEngine.Events.UnityEventCallState.RuntimeOnly);
            button.onClick.SetPersistentListenerState(1, UnityEngine.Events.UnityEventCallState.RuntimeOnly);
            button.onClick.SetPersistentListenerState(2, UnityEngine.Events.UnityEventCallState.Off);
        }

        else
        {
            button.onClick.SetPersistentListenerState(0, UnityEngine.Events.UnityEventCallState.Off);
            button.onClick.SetPersistentListenerState(1, UnityEngine.Events.UnityEventCallState.Off);
            button.onClick.SetPersistentListenerState(2, UnityEngine.Events.UnityEventCallState.RuntimeOnly);
        }
    }

    private void BtnSetting()
    {
        button.onClick.AddListener(() => { snap.MoveCenter(-rect.anchoredPosition.x); });
        Debug.Log(button.onClick.GetPersistentEventCount());
    }
}
