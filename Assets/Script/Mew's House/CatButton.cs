﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatButton : MonoBehaviour
{
    LoadCatList_Show loadCats;
    Canvas canvas;
    private SpriteRenderer cat;
    private SpriteRenderer roomWall;
    public string catName;
    public int catNumber;
    public int i;
    // Use this for initialization
    void Start()
    {
        //loadCats = GameObject.Find("GameController").GetComponent<LoadCatList_Show>();
        //catName = loadCats.SendCatName(i);
        //catBackground = loadCats.SendCatBackground(i);
        //catNumber = loadCats.SendCatNumber(i);
    }
    /*리스트에 고양이 추가 한뒤 작동 확인 바람*/

    private void ShowCatRoom()
    {
        // 원래 있던 ui를 꺼줌
        GameObject.Find("Canvas").GetComponent<Canvas>().enabled = false;

        cat = GameObject.Find("Cat").GetComponent<SpriteRenderer>();
        roomWall = GameObject.Find("RoomWall").GetComponent<SpriteRenderer>();

        // 버튼에 맞는 고양이를 화면에 띄어주고 배경도 띄어줌 추후 애니메이션 추가해서 생성
        cat.enabled = true;
        cat.sprite = (Sprite)Resources.Load("Cats'House/Cats/" + catName, typeof(Sprite));
        //roomWall.sprite = (Sprite)Resources.Load("Cats'House/RoomWall/" + catBackground, typeof(Sprite));
        roomWall.color = new Color(roomWall.color.r, roomWall.color.g, roomWall.color.b, 1.0f);
    }
}
