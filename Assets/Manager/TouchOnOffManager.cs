﻿using BitBenderGames;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-01-19
 * 터치를 막아주는 매니져(싱글톤 사용)
 * 인자 받아서 ONOFF 해줌
 */

public class TouchOnOffManager : Singleton<TouchOnOffManager>
{
    public TouchInputController touchMove;
    //public TouchInterationCam touchInteration;
    public TouchManager touchManager;

    private bool touchMoveEnable = true;
    private bool touchInterationEnable = true;

    private void Start()
    {
        StartCoroutine(BoolChecker());
    }
    public void TouchOnOff(bool value)
    {
        touchMoveEnable = value;
        touchInterationEnable = value;
    }

    private IEnumerator BoolChecker()
    {
        WaitForFixedUpdate waitForFixedUpdate = new WaitForFixedUpdate();
        while (true)
        {
            touchMove.enabled = touchMoveEnable;
            //touchInteration.enabled = touchInterationEnable;
            touchManager.enabled = touchInterationEnable;
            yield return waitForFixedUpdate;
        }
    }
}
