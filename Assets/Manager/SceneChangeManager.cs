﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChangeManager : Singleton<SceneChangeManager>
{
    [SerializeField]
    [Header("페이드 인아웃")]
    private FadeInOutManager fadeManager;

    bool bChangeScene = false;
    protected override void Awake()
    {
        base.Awake();
        fadeManager = FindObjectOfType<FadeInOutManager>();
    }

    public void GoToAnimalHouse()
    {
        PlayerPrefs.SetString("PrevScene", SceneManager.GetActiveScene().name);
        ChangeScene("Cat'sRoom");
    }

    public void GetOutAimalHouse()
    {
        string PrevScene = PlayerPrefs.GetString("PrevScene");
        ChangeScene(PrevScene);
    }

    public void ChangeScene(string sceneName)
    {
        if(bChangeScene)
        {
            return;
        }
        StartCoroutine(_LoadScene(sceneName));
    }

    public IEnumerator _LoadScene(string sceneName)
    {
        bChangeScene = true;
        AsyncOperation op = SceneManager.LoadSceneAsync(sceneName);
        op.allowSceneActivation = false;
        fadeManager.FadeOut();
        yield return new WaitForSeconds(fadeManager.fadeTime);
        op.allowSceneActivation = true;
        yield break;
    }
}
