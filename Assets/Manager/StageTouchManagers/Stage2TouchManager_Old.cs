﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage2TouchManager_Old : TouchManager
{
    [Header("슬라이드 오브젝트를 위한 부울값")]
    private bool slide;

    protected override void PlayTouchDown()
    {
        base.PlayTouchDown();
        if (hit.collider != null)
        {
            interObject = hit.collider.gameObject;
            if (interObject.tag == "SlideObject")
            {
                interObject.GetComponent<Stage2SlideObject>().touch = true;
                slide = true;
                return;
            }
            else if (interObject.tag == "UpSlide")
            {
                interObject.GetComponent<Upslide>().touch = true;
                slide = true;
                return;
            }
        }
    }
    protected override void PlayTouchEvent()
    {
        if (slide)
        {
            slide = false;
            return;
        }

        if (hit.collider == null)
        {
            CreateDust(touchPos);
        }

        else if (hit.collider.tag == "Car")
        {
            chanceNum.GetComponent<ChanceNumImage>().MinusChance();
            backgound.Touch();
            VibratorControllor.TouchVib();
            coinCatCounter.MinCounter();
        }

        else if (hit.collider.tag == "SlideObject" || hit.collider.tag == "UpSlide")
            return;

        else if (hit.collider.tag == "Tree" || hit.collider.tag == "Tree2")
            TouchTreeEffectCreate(hit.collider.tag, touchPos);

        else
        {
            VibratorControllor.ObjTouchVib();
            interObject = hit.collider.gameObject;
            Debug.Log(interObject.name);
            if (interObject.tag == "bird")
            {
                interObject.GetComponent<I_NonColObject>().OnClick();
            }

            else if (interObject.tag == "fieldObject")
            {
                if (interObject.name == "CatAilen")
                    interObject.transform.DOPause();
                interObject.GetComponent<SampleButtonScript>().OnClick();
                StartCoroutine("Jelly", interObject);
            }

            else if (interObject.tag == "fieldCat")
            {
                TouchCollectCat();
                return;
            }

            else if (interObject.tag == "hiddenCat")
            {
                TouchHiddenCat();
                return;
            }

            else if (interObject.tag == "otherCat")
            {
                    interObject.GetComponent<I_OtherStageAnimal>().OhterStageAnimalOnClick();
            }

            // 인터페이스로 묶음
            // 아마 인터페이스 명령어도 곧 한번 바꿔야 될거 같음
            // 이름을 좀더 직관적이게
            if (interObject.tag == "NotColObject")
                interObject.GetComponent<I_NonColObject>().OnClick();

            else if (interObject.name == "CatFO")
            {
                interObject.GetComponent<CatFOMotion>().OnClick();
                PlayJellyMotion(interObject.transform.GetChild(0).gameObject);
            }
            else if (interObject.name == "MadCat")
            {
                interObject.GetComponent<CoinCatMotion>().OnClick();

                if (!interObject.GetComponent<MadCatCheck>().collect)
                {
                    photo.TakePicture(interObject);
                }
                else
                    return;

                List<CatSaveLoad.JsonCat> jsonCatData = jsonCat.LoadCats();
                jsonCatData[interObject.GetComponent<MadCatCheck>().no - 1].Collect = true;
                jsonCat.SaveCats(jsonCatData, jsonCat.key);
            }
        }
    }

    private void TouchCollectCat()
    {
        CatCheck catCheck = interObject.GetComponent<CatCheck>();
        FieldCat cat = interObject.GetComponent<FieldCat>();
        StartCoroutine("Jelly", interObject);
        if (catCheck.effectSound != null)
            AudioManager.Instance.PlaySound(catCheck.effectSound, interObject.transform.position, null, Random.Range(0.8f, 1.0f));

        if (!cat.CanCollect) return;

        if (!catCheck.loadCollect)
        {
            houseNewMark.SetData(true);
            photo.TakePicture(interObject);
        }

        
        SaveFoundNoramlCat(interObject, catCheck);
    }
    private void TouchHiddenCat()
    {
        CatCheck catCheck = interObject.GetComponent<CatCheck>();
        if (interObject.name == "Gomaymmee")
        {
            interObject.GetComponent<SquareCatMotion>().OnClick();
        }

        if (!interObject.GetComponent<CatCheck>().loadCollect)
        {
            photo.GetComponent<PhotoShot>().TakePicture(interObject);
        }

        
        SaveFoundHiddenCat(interObject, catCheck);
        StartCoroutine("Jelly", interObject);
    }
}

//List<CatSaveLoad.JsonCat> jsonCatData = jsonCat.LoadCats();
//jsonCatData[interObject.GetComponent<CatCheck>().no - 1].Collect = true;
//jsonCat.SaveCats(jsonCatData, jsonCat.key);
//
//for (int i = 0; i < catPanelContent.childCount; i++)
//{
//    SampleCatButton fieldCatScript = catPanelContent.GetChild(i).GetComponent<SampleCatButton>();
//    if (interObject == fieldCatScript.fieldCat)
//    {
//        fieldCatScript.OnClick();
//        interObject.GetComponent<CatCheck>().CollectObject();
//        interObject.GetComponent<CatCheck>().CheckCollect();
//        break;
//    }
//}


//List<CatSaveLoad.JsonCat> jsonCatData = jsonCat.LoadCats();
//jsonCatData[interObject.GetComponent<CatCheck>().no - 1].Collect = true;
//jsonCat.SaveCats(jsonCatData, jsonCat.key);
//
//for (int i = 0; i < hiddenCatContent.childCount; i++)
//{
//    SampleCatButton hiddenCatScript = hiddenCatContent.GetChild(0).GetComponent<SampleCatButton>();
//    if (interObject == hiddenCatScript.fieldCat)
//    {
//        hiddenCatScript.OnClick();
//        interObject.GetComponent<CatCheck>().CollectObject();
//        interObject.GetComponent<CatCheck>().CheckCollect();
//        break;
//    }
//}