﻿using BitBenderGames;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/*
 * 이걸 태그로 검사를 해야 좋을까
 * 아니면 그냥 함수 실행하는걸로 냅두는게 좋을까
 * 음....
 * 상관은 없어 보이는데
 * 만약 여기서 창문하고 나무 터치했을때 할 
 */

/// <summary>
/// 터치 매니져 베이스
/// 터치 담당 
/// </summary>
public class StageTouchManagerBase : Singleton<StageTouchManagerBase>, ITouchManger
{
    #region 터치 Update문에서 작동할 변수들
    [SerializeField]
    [Header("원하는 터치 시간")]
    private float touchTime;
    private float nowTouchTime;

    [SerializeField]
    [Header("어느정도 드래그 한거는 터치로 인정 안하겠다")]
    private float dragDistance = 0.3f;
    #endregion

    #region 터치 했을때 사용할 클래스 변수
    [SerializeField]
    [Header("터치 입력받을 레이어")]
    private LayerMask touchLayerMask;
    [SerializeField]
    [Header("터치 찬스 매니져")]
    private StageChanceManager chanceManager;
    [SerializeField]
    [Header("꽝 먼지")]
    private GameObject touchDust;

    [SerializeField]
    [Header("먼지 사운드 이름")]
    private string[] dustSoundName;

    [SerializeField]
    [Header("힌트 숨기기위한 컨트롤러")]
    private StageAnimalHintSetting hintController;
    #endregion

    [SerializeField]
    [Header("터치 막을때 사용할 인풋 컨트롤러")]
    private TouchInputController touchInputController;

    [SerializeField]
    [Header("카메라 무브(줌 설정때문에 필요함)")]
    private MobileTouchCamera mobileTouchCamere;

    private bool canTouch = true;
    /// <summary>
    /// 지금 터치 가능한지 확인하는 함수
    /// </summary>
    public bool CanTouch { get => canTouch; set => canTouch = value; }

    private float minZoomValue;

    private bool touchDownObj = true;

    private Vector3 downTouchPose;

    [SerializeField]
    [Header("슬라이드 오브젝트 레이어")]
    protected LayerMask slideLayer;

    [SerializeField]
    private int coinTouchCount = 20;
    
    private int currentTouchCount = 0;

    [SerializeField]
    private GameObject coinObj;

    protected override void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        StartCoroutine(SetZoomValue());
    }

    /// <summary>
    /// 처음 줌 땡긴 상태로 시작하는 함수
    /// </summary>
    private IEnumerator SetZoomValue()
    {
        //minZoomValue = mobileTouchCamere.CamZoomMin;
        //mobileTouchCamere.CamZoomMin = mobileTouchCamere.CamZoomMax;
        //yield return new WaitForSeconds(0.01f);
        //mobileTouchCamere.CamZoomMin = minZoomValue;
        mobileTouchCamere.CamZoom = mobileTouchCamere.CamZoomMax;
        yield break;
    }

    private void Update()
    {
#if UNITY_EDITOR_WIN
        // UI터치 막음
        if (EventSystem.current.IsPointerOverGameObject())
            return;
#endif

#if UNITY_ANDROID
        if (Input.touchCount > 0 && EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
            return;
#endif
        if (!CanTouch)
            return;

        CheckTouch();
    }

    /// <summary>
    /// 터치했는지 아니면 슬라이드 했는지 확인
    /// </summary>
    private void CheckTouch()
    {
        if(Input.GetMouseButtonDown(0))
        {
            hintController.TurnOffPanel();
            touchDownObj = DoStageTouchDown();
            downTouchPose = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (!DoStageTouchDown())
            {
                return;
            }
        }

        if(Input.GetMouseButton(0))
        {
            nowTouchTime += Time.deltaTime;
        }

        else if(Input.GetMouseButtonUp(0))
        {
            if (!touchDownObj)
            {
                nowTouchTime = 0;
                return;
            }

            if((nowTouchTime <= touchTime))
            {
                DoTouch();
            }
            nowTouchTime = 0;
        }
    }

    /// <summary>
    /// 터치 했을시 실행 할 함수
    /// </summary>
    private void DoTouch()
    {
        Vector3 touchPose  = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if(Vector3.Distance(downTouchPose, touchPose) > dragDistance)
        {
            return;
        }

        RaycastHit2D hit = Physics2D.Raycast(touchPose, Vector2.zero, 0.0f, touchLayerMask);

        if (hit.transform != null)
        {
            if (GetCanTouch())
            {
                // TouchableObj 이라는 인터페이스를 전부 갖고 있어야함
                // DoTouch를 실행시켜 터치 카운트를 줄이냐 안줄이냐 판단 뒤 행동
                TouchableObj touchObj = hit.transform.GetComponent<TouchableObj>();
                // 여기서 에러 나면 그 오브젝트에 이 인터페이스가 없는거임 예언 하나 한다.
                if(touchObj != null)
                {
                    bool minChance = touchObj.DoTouch(touchPose);
                    if (minChance)
                    {
                        MinusChance();
                    }
                }
            }
        }

        // 터치 했을때 아무것도 없으면 먼지 만들고 기회 하나 차감
        else
        {
            if(MinusChance())
            {
                CreateDustEffect(touchPose);
                GetCoin(touchPose);
            }
        }
        DoStageTouch();
    }

    /// <summary>
    /// 스테이지마다 다른 터치 이펙트
    /// 혹시 몰라서 만들어 넣음
    /// </summary>
    protected virtual void DoStageTouch()
    {
        
    }

    /// <summary>
    /// 터치 시작했을 시 호출
    /// </summary>
    /// <returns>true면 기회 차감 false면 기회 안차감</returns>
    protected virtual bool DoStageTouchDown()
    {
        Vector3 touchPose = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(touchPose, Vector2.zero, 0.0f, slideLayer);

        bool isChanceMin = true;

        if (hit.transform)
        {
            isChanceMin = hit.transform.GetComponent<TouchDownableObj>().DoTouchDown(touchPose);
        }

        return isChanceMin;
    }

    /// <summary>
    /// 찬스 매니저에 있는 지금 찬스 카운트에서 1을 뺌
    /// </summary>
    protected virtual bool MinusChance()
    {
        // TODO : 
        //chanceManager.NowChanceCount--;
        chanceManager.NowChanceCount = chanceManager.maxChanceCount;
        return GetCanTouch();
    }

    private void GetCoin(Vector2 touchPose)
    {
        currentTouchCount++;

        GameObject[] createCoin = new GameObject[5];
        if (currentTouchCount >= coinTouchCount)
        {
            for (int i = 0; i < createCoin.Length; i++)
            {
                createCoin[i] = PoolingManager.Instance.GetPool("Coin");
                if (createCoin[i] == null)
                    createCoin[i] = Instantiate(coinObj);

                createCoin[i].transform.position = touchPose;
                Vector3 currentPose = createCoin[i].transform.position;
                createCoin[i].transform.DOJump(new Vector3(currentPose.x + Random.Range(-2f, 2f), currentPose.y + Random.Range(-0.5f, 0.5f)), Random.Range(1, 2), Random.Range(1, 4), Random.Range(0.5f, 1.0f));
                createCoin[i].SetActive(true);
                createCoin[i].GetComponent<StageCoinCatCoinMotion>().CoinAnimation();
            }
            AudioManager.Instance.PlaySound("coin_1", transform.position);
            currentTouchCount = 0;
            SaveDataManager.Instance.EditCoinCount(SaveDataManager.Instance.Data.Player.CoinCount + 1);
        }
    }

    /// <summary>
    /// 지금 터치할 수 있는지 확인하는 함수
    /// </summary>
    /// <returns></returns>
    private bool GetCanTouch()
    {
        return chanceManager.NowChanceCount > 0;
    }

    /// <summary>
    /// 먼지 만드는 함수 (먼지가 먼지)
    /// </summary>
    /// <param name="touchPose">터치 했을때 좌표</param>
    private void CreateDustEffect(Vector2 touchPose)
    {
        GameObject dust = PoolingManager.Instance.GetPool("Dust");  
        if(dust ==null)
            dust = Instantiate(touchDust);

        dust.transform.position = touchPose;

        AudioManager.Instance.PlaySound(dustSoundName, transform.position);
    }

    /// <summary>
    /// 카메라 움직임 제어하는 함수
    /// </summary>
    /// <param name="can">참이면 움직이고 거짓이면 안움직임</param>
    public void ForbitTouch(bool can)
    {
        touchInputController.CanMove = can;
        CanTouch = can;
    }
}
