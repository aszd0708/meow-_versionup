﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage1TouchManager : TouchManager
{
    protected override void PlayTouchEvent()
    {
        if (hit.collider == null)
            CreateDust(touchPos);

        else if (hit.collider.tag == "Tree" || hit.collider.tag == "Tree2")
            TouchTreeEffectCreate(hit.collider.tag, touchPos);

        else
        {
            VibratorControllor.ObjTouchVib();
            interObject = hit.collider.gameObject;
            if (interObject.tag == "bird")
                interObject.GetComponent<TouchBird>().Touch();

            else if (interObject.tag == "fieldObject")
            {
                interObject.GetComponent<SampleButtonScript>().OnClick();
                PlayJellyMotion(interObject);
            }
    
            else if (interObject.tag == "fieldCat")
            {
                CatCheck catCheck = interObject.GetComponent<CatCheck>();
                FieldCat cat = interObject.GetComponent<FieldCat>();
                PlayJellyMotion(interObject);
                if (catCheck.effectSound != null)
                    AudioManager.Instance.PlaySound(catCheck.effectSound, interObject.transform.position, null, Random.Range(0.8f, 1.0f));
               
                if (!cat.CanCollect) return;

                if (!catCheck.loadCollect)
                {
                    houseNewMark.SetData(true);
                    photo.TakePicture(interObject);
                }

                SaveFoundNoramlCat(interObject, catCheck);
                return;
            }

            else if (interObject.tag == "hiddenCat")
            {
                CatCheck catCheck = interObject.GetComponent<CatCheck>();

                if (catCheck.effectSound != null)
                    AudioManager.Instance.PlaySound(catCheck.effectSound, interObject.transform.position, null, Random.Range(0.8f, 1.0f));
                if (!catCheck.loadCollect)
                {
                    FindObjectOfType<CatHouseNewMark>().SetData(true);
                    photo.TakePicture(interObject);
                }

                SaveFoundNoramlCat(interObject, catCheck);
                StartCoroutine("Jelly", interObject);
                return;
            }

            // 이건 인테페이스로 묶으려고 하면 좀 많이 고쳐야 하므로 마지막에 최적화 할 때 고치는거

            else if (interObject.tag == "NotColObject")
                interObject.GetComponent<I_NonColObject>().OnClick();

            //else if (interItem.name == "SpecialCloud")
            //    interItem.GetComponent<AngelCatCloudMotion>().CloudTouch();

            else if (interObject.name == "MadCat")
            {
                interObject.GetComponent<CoinCatMotion>().OnClick();
                if (!interObject.GetComponent<MadCatCheck>().collect)
                {
                    photo.TakePicture(interObject);
                }
                else
                    return;
                List<CatSaveLoad.JsonCat> jsonCatData = jsonCat.LoadCats();
                jsonCatData[interObject.GetComponent<MadCatCheck>().no - 1].Collect = true;
                jsonCat.SaveCats(jsonCatData, jsonCat.key);
            }

            if (interObject.GetComponent<EffectSound>() != null && interObject.GetComponent<EffectSound>().effectSound != null)
            {
                EffectSound sound = interObject.GetComponent<EffectSound>();
                AudioManager.Instance.PlaySound(sound.effectSound, interObject.transform.position, null, Random.Range(0.8f, 1.0f));
            }
            // 위에 코루틴은 다음에 하란 말 나오면 그때 하기.
        }
    }
}
