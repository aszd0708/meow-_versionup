﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 페이드 인아웃(Image, BGM) 매니져
/// </summary>
public class FadeInOutManager : MonoBehaviour
{
    [SerializeField]
    [Header("페이드 인아웃 이미지")]
    private Image fadeImage;

    [SerializeField]
    private AudioSource BGM;

    [SerializeField]
    [Header("페이드 시간")]
    public float fadeTime;

    private float outStart = 0.0f;
    private float outEnd = 1.0f;
    private float inStart = 1.0f;
    private float inEnd = 0.0f;
    private float time = 0.0f;

    public void Start()
    {
        FadeIn();
    }

    public void FadeIn()
    {
        StartCoroutine(_FadeIn());
        if (BGM != null)
            StartCoroutine(_BGMFadeOut(fadeTime, 0, PlayerPrefs.GetFloat("backVol", 1.0f)));
    }

    public void FadeOut()
    {
        StartCoroutine(_FadeOut());
        if (BGM != null)
            StartCoroutine(_BGMFadeIn(fadeTime, PlayerPrefs.GetFloat("backVol", 1.0f), 0));
    }

    private IEnumerator _FadeIn(float fadeTime = 1)
    {
        Color color = fadeImage.color;
        time = 0.0f;
        color.a = Mathf.Lerp(inStart, inEnd, time);

        while (color.a > 0.0f)
        {
            time += Time.deltaTime / fadeTime;

            color.a = Mathf.Lerp(inStart, inEnd, time);

            fadeImage.color = color;

            if (fadeImage.color.a <= 0) break;
            yield return null;
        }
        fadeImage.enabled = false;
        yield break;
    }

    private IEnumerator _FadeOut(float fadeTime = 1)
    {
        fadeImage.enabled = true;
        Color color = fadeImage.color;
        time = 0.0f;
        color.a = Mathf.Lerp(outStart, outEnd, time);

        while (color.a < 1.0f)
        {
            time += Time.deltaTime / fadeTime;

            color.a = Mathf.Lerp(outStart, outEnd, time);

            fadeImage.color = color;

            yield return null;
        }
    }

    private IEnumerator _BGMFadeIn(float time, float inStart = 1, float inEnd = 0, float fadeTime = 1)
    {
        float bgmVolume = BGM.volume;
        time = 0.0f;
        bgmVolume = Mathf.Lerp(inStart, inEnd, time);

        while (bgmVolume > 0.0f)
        {
            time += Time.deltaTime / fadeTime;

            bgmVolume = Mathf.Lerp(inStart, inEnd, time);

            BGM.volume = bgmVolume;
            yield return null;
        }

        BGM.enabled = false;
        yield return null;
    }

    private IEnumerator _BGMFadeOut(float time, float outStart = 0, float outEnd = 1, float fadeTime = 1)
    {
        BGM.enabled = true;
        float prefVolume = PlayerPrefs.GetFloat("backVol", 1.0f);
        float bgmVolume = BGM.volume;
        time = 0.0f;
        bgmVolume = Mathf.Lerp(outStart, outEnd, time);

        while (bgmVolume < prefVolume)
        {
            time += Time.deltaTime / fadeTime;

            bgmVolume = Mathf.Lerp(outStart, outEnd, time);

            BGM.volume = bgmVolume;

            yield return null;
        }
        yield return null;
    }
}
