﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JsonDataLoadManager<T> : MonoBehaviour
{
    // <summary>
    // 제이슨 파일 불러오는 함수
    // 리스트 O 배열 X
    // </summary>
    public List<T> LoadJson(TextAsset jsonFile)
    {
        string jsonData = jsonFile.ToString();
        List<T> data = JsonConvert.DeserializeObject<List<T>>(jsonData);
        return data;
    }

    private void SaveJson(List<T> _jsonData, string key, string fileName, string path = null)
    {
        if (path == null)
            path = Application.persistentDataPath;

        string jsonData = JsonDataManager_Dummy.ObjectToJson(_jsonData);

        JsonDataManager_Dummy.CreateJsonFile(path, fileName, jsonData);
    }

    private List<T> LoadJsonUsedAES(TextAsset jsonFile)
    {
        string jsonData = jsonFile.ToString();
        List<T> data = JsonConvert.DeserializeObject<List<T>>(jsonData);
        return data;
    }
}
