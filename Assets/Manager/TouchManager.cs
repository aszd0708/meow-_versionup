﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Collections;
using DG.Tweening;

public abstract class TouchManager : MonoBehaviour
{
    /* public */
    public Text ChanceNum;
    public int Chance;

    [Header("터치하고 때는 순간의 딜레이 캐치 시간")]
    public float checkTime = -0.5f;

    public ChanceNumImage chanceNum;
    public BackgoundTouch backgound;
    public CatHint catHint;
    public CoinCatTimer coinCatCounter;
    public GameObject treeTouchEffect;
    public PhotoShot photo;
    public CatSaveLoad jsonCat;
    public CatHouseNewMark houseNewMark;

    /* private */
    private new Camera camera;
    private float timeSpan = 0.5f;
    private bool jelly;
    private Vector3 camPos;
    private bool noChance = false;

    /* protected */
    protected GameObject interObject;
    protected Vector3 touchPos;

    [Header("일반 고양이 컨텐츠 판넬")]
    public Transform catPanelContent;

    [Header("히든 고양이 판넬")]
    public Transform hiddenCatContent;
    private readonly string[] touchSound = { "dgtouch1", "dgtouch2", "dgtouch3" };

    private TouchCreateCoin touchCreateCoin;
    private bool cameraTouch = true;

    public bool NoChance { get => noChance; set => noChance = value; }

    private int noChanceTouchCount;
    public int NoChanceTouchCount { get => noChanceTouchCount; set => noChanceTouchCount = value; }
    public bool CameraTouch { get => cameraTouch; set => cameraTouch = value; }

    protected RaycastHit2D hit;

    private void Awake()
    {
        touchCreateCoin = FindObjectOfType<TouchCreateCoin>();
        camera = Camera.main;
    }

    // Start is called before the first frame update
    void Start()
    {
        jelly = true;
        timeSpan = Time.time;
        ChanceNum = GameObject.Find("ChanceNum").GetComponent<Text>();
        Chance = System.Convert.ToInt32(ChanceNum.text);
        if (Chance <= 0)
            NoChance = true;
        NoChanceTouchCount = -1;
    }

    // Update is called once per frame
    void Update()
    {
        if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            return;

        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                if (EventSystem.current.IsPointerOverGameObject(i))
                    return;
            }
        }

        if (!CameraTouch)
            return;

        if (NoChance)
            NoChanceTouch();

        DoTouch();
    }

    protected abstract void PlayTouchEvent();

    protected virtual void DoTouch()
    {
        if (Input.GetMouseButtonDown(0))
        {
            PlayTouchDown();
        }

        else if (Input.GetMouseButtonUp(0))
        {
            PlayTouchUp();
        }
    }

    protected virtual void PlayTouchDown()
    {
        timeSpan = Time.time;
        Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        hit = Physics2D.Raycast(touchPos, Vector2.zero, 0.0f);
        catHint.GoOriginPos();
        camPos = camera.transform.position;
    }

    protected void PlayTouchUp()
    {
        Vector2 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        hit = Physics2D.Raycast(touchPos, Vector2.zero, 0.0f);
        timeSpan -= Time.time;

        if (Mathf.Abs(camPos.x - camera.transform.position.x) > 0.05f || Mathf.Abs(camPos.y - camera.transform.position.y) > 0.05f)
            return;

        if (timeSpan >= checkTime)
        {
            if (hit.collider == null)
                CreateDust(touchPos);
            else
            {
                PlayTouchEvent();
            }
        }
    }

    protected void SaveFoundHiddenCat(GameObject catObj, CatCheck catCheck)
    {
        List<CatSaveLoad.JsonCat> jsonCatData = jsonCat.LoadCats();
        jsonCatData[catCheck.no - 1].Collect = true;
        jsonCat.SaveCats(jsonCatData, jsonCat.key);

        for (int i = 0; i < hiddenCatContent.childCount; i++)
        {
            SampleCatButton hiddenCatScript = hiddenCatContent.GetChild(0).GetComponent<SampleCatButton>();
            if (catObj == hiddenCatScript.fieldCat)
            {
                hiddenCatScript.OnClick();
                catCheck.CollectObject();
                catCheck.CheckCollect();
                break;
            }
        }
    }
    protected void SaveFoundNoramlCat(GameObject catObj, CatCheck catCheck)
    {
        List<CatSaveLoad.JsonCat> jsonCatData = jsonCat.LoadCats();
        jsonCatData[catCheck.no - 1].Collect = true;
        jsonCat.SaveCats(jsonCatData, jsonCat.key);

        for (int i = 0; i < catPanelContent.childCount; i++)
        {
            SampleCatButton fieldCatScript = catPanelContent.GetChild(0).GetComponent<SampleCatButton>();
            if (catObj == fieldCatScript.fieldCat)
            {
                fieldCatScript.OnClick();
                catCheck.CollectObject();
                catCheck.CheckCollect();
                break;
            }
        }
    }

    protected void PlayJellyMotion(GameObject hitObject)
    {
        StartCoroutine(Jelly(hitObject));
    }

    private IEnumerator Jelly(GameObject hitObject)
    {
        if (jelly)
        {
            jelly = false;
            Vector3 origin = hitObject.transform.localScale;

            hitObject.transform.DOScale(new Vector3(origin.x - 0.1f, origin.y - 0.1f, origin.z), 0.1f);
            yield return new WaitForSeconds(0.1f);
            hitObject.transform.DOScale(new Vector3(origin.x, origin.y, origin.z), 0.8f).SetEase(Ease.OutElastic);
            yield return new WaitForSeconds(0.3f);
            jelly = true;
        }
        else
        {
            yield return null;
        }
    }

    protected void TouchTreeEffectCreate(string tagName, Vector3 touchPos)
    {
        if (tagName == "Tree" || tagName == "Tree2")
        {
            chanceNum.MinusChance();
            VibratorControllor.TouchVib();
            coinCatCounter.MinCounter();
            GameObject treeEffect;

            treeEffect = PoolingManager.Instance.GetPool(treeTouchEffect.GetComponent<TreeTouchEffect>().poolingName);

            if (treeEffect == null)
                treeEffect = Instantiate(treeTouchEffect);
            treeEffect.transform.position = touchPos;
        }
    }

    protected void CreateDust(Vector3 touchPos)
    {
        chanceNum.MinusChance();
        backgound.Touch();
        AudioManager.Instance.PlaySound(touchSound, touchPos, null, Random.Range(0.8f, 1.0f));
        VibratorControllor.TouchVib();
        coinCatCounter.MinCounter();

        touchCreateCoin.StartCreateCoin(touchPos);
    }

    private void NoChanceTouch()
    {
        if (Input.GetMouseButtonDown(0))
        {
            timeSpan = Time.time;
            catHint.GoOriginPos();
        }
        else if (Input.GetMouseButtonUp(0))
        {
            timeSpan -= Time.time;
            if (timeSpan >= checkTime)
            {
                if (NoChanceTouchCount == -1)
                    NoChanceTouchCount = 2;

                else if (NoChanceTouchCount == 1)
                {
                    FindObjectOfType<ChanceEmpty>().SendMessage("ShowPopup", true, SendMessageOptions.DontRequireReceiver);
                    NoChanceTouchCount = -1;
                }

                else NoChanceTouchCount--;
            }
        }
    }

    public void SetCamTouch()
    {
        CameraTouch = true;
    }
}
