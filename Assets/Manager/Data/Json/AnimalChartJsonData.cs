﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class AnimalChart
{
    public int No;
    public string Info1;
    public float Value1;
    public string Info2;
    public float Value2;
    public string Info3;
    public float Value3;
    public string Info4;
    public float Value4;
    public string Info5;
    public float Value5;

    public AnimalChart(int _No, string _Info1, float _Value1, string _Info2, float _Value2, string _Info3, float _Value3, string _Info4, float _Value4, string _Info5, float _Value5)
    {
        No = _No;
        Info1 = _Info1;
        Value1 = _Value1;
        Info2 = _Info2;
        Value2 = _Value2;
        Info3 = _Info3;
        Value3 = _Value3;
        Info4 = _Info4;
        Value4 = _Value4;
        Info5 = _Info5;
        Value5 = _Value5;
    }
    public void Print()
    {
        Debug.Log("No : " + No +
            "\n정보1 : " + Info1 + " 값 : " + Value1 +
            "\n정보2 : " + Info2 + " 값 : " + Value2 +
            "\n정보3" + Info3 + " 값 : " + Value3 +
            "\n정보4" + Info4 + " 값 : " + Value4 +
            "\n정보5" + Info5 + " 값 : " + Value5);
    }

    public string SentInfo(int a)
    {
        switch (a)
        {
            case 1: return Info1;
            case 2: return Info2;
            case 3: return Info3;
            case 4: return Info4;
            case 5: return Info5;
            default:
                Debug.Log("에러에러");
                break;
        }
        return null;
    }

    public float SendValue(int a)
    {
        switch (a)
        {
            case 1: return Value1;
            case 2: return Value2;
            case 3: return Value3;
            case 4: return Value4;
            case 5: return Value5;
            default:
                Debug.Log("에러에러");
                break;
        }
        return 0;
    }
}

public class AnimalChartJsonData : JsonDataManager<AnimalChart>
{
    public static AnimalChartJsonData Instance;

    protected override void Awake()
    {
        base.Awake();

        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);
    }

    //싱글톤 인스턴스가 사라졌을 때
    protected virtual void OnDestroy()
    {
        //유일한 Instance가 자신이면, Instance값을 비워줌 (다른 인스턴스가 들어갈 수 있도록)
        if (Instance == this)
            Instance = null;
    }
}
