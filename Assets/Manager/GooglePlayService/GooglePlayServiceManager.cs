﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;

public class GooglePlayServiceManager : Singleton<GooglePlayServiceManager>
{
    protected override void Awake()
    {
        base.Awake();

        DontDestroyOnLoad(gameObject);

        // Build Google Play
        {
            PlayGamesPlatform.InitializeInstance(new PlayGamesClientConfiguration.Builder().EnableSavedGames().Build());
            PlayGamesPlatform.DebugLogEnabled = true;
            PlayGamesPlatform.Activate();
        }
    }

    #region Log in out
    public void OnLogin()
    {
        if (!Social.localUser.authenticated)
        {
            Social.localUser.Authenticate((bool bSuccess) =>
            {
                if (bSuccess)
                {
                    Debug.Log("Success : " + Social.localUser.userName);
                }
                else
                {
                    Debug.Log("Fall");
                }
            });
        }

        else
        {
            Debug.Log("로그인 되어있음");
        }
    }

    public void OnLogOut()
    {
        ((PlayGamesPlatform)Social.Active).SignOut();
    }
    #endregion

    #region Cloud Data
    #region Save
    public void SaveToCloud()
    {
        SaveCloud();
        //Debug.LogError("로그인 에러");
    }
    ISavedGameClient SavedGame()
    {
        return PlayGamesPlatform.Instance.SavedGame;
    }

    public void SaveCloud()
    {
        SavedGame().OpenWithAutomaticConflictResolution("FindAnimal",
            DataSource.ReadCacheOrNetwork, ConflictResolutionStrategy.UseLastKnownGood, SaveGame);
    }

    private void SaveGame(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            var update = new SavedGameMetadataUpdate.Builder().Build();
            byte[] data = SaveDataManager.Instance.GetDataToByte();
            if (data != null)
            {

                Debug.Log("데이터 받아옴");
            }
            SavedGame().CommitUpdate(game, update, data, SaveData);
        }

        else
        {
            Debug.LogError("저장 에러");
        }
    }

    private void SaveData(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            Debug.LogError("저장 성공");
        }
        else
        {
            Debug.LogError("로그인 실패 멈춰!!!");
        }
    }
    #endregion
    #region Load
    public void LoadFromCloud()
    {
        LoadData();
        //Debug.LogError("로그인 ㄴㄴ");
    }

    public void LoadData()
    {
        SavedGame().OpenWithAutomaticConflictResolution("FindAnimal",
            DataSource.ReadCacheOrNetwork, ConflictResolutionStrategy.UseLastKnownGood, LoadGame);
    }

    void LoadGame(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            SavedGame().ReadBinaryData(game, LoadData);
        }
        else
        {
            Debug.LogError("LoadGame 실패");
        }
    }

    void LoadData(SavedGameRequestStatus status, byte[] LoadedData)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            string data = System.Text.Encoding.UTF8.GetString(LoadedData);
            //SaveDataManager.Instance.SaveDataFromToString(ref data);
            SaveDataManager.Instance.SaveDataFromByte(ref LoadedData);
            SceneChangeManager.Instance.ChangeScene("GameLogo");
            Debug.Log("로드 성공");
        }
        else
        {
            Debug.LogError("로드 실패");
        }
    }
    #endregion
    #endregion
}
