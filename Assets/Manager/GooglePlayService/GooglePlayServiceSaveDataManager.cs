﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;

public class GooglePlayServiceSaveDataManager : MonoBehaviour
{
    public void LogIn()
    {
        GooglePlayServiceManager.Instance.OnLogin();
    }

    public void LogOut()
    {
        GooglePlayServiceManager.Instance.OnLogOut();
    }

    public void SaveToCloud()
    {
        GooglePlayServiceManager.Instance.SaveToCloud();
    }

    public void LoadFromCloud()
    {
        GooglePlayServiceManager.Instance.LoadFromCloud();
    }
}
