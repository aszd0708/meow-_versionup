﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[Serializable]
public struct Test
{
    public int id;
    public bool boolean;
}


public class MergeSorting : MonoBehaviour
{
    public List<Test> t;

    private void Start()
    {
        Divide(ref t, 0, t.Count-1);
        for(int i = 0; i < t.Count; i++)
        {
            Debug.Log(t[i].id + " " + t[i].boolean);
        }
    }

    private void Divide(ref List<Test> t, int startIndex, int endIndex)
    {
        int middleIndex = (startIndex + endIndex) / 2;
        
        if(startIndex == endIndex)
        {
            return;
        }

        else
        {
            Divide(ref t, startIndex, middleIndex);
            Divide(ref t, middleIndex + 1, endIndex);
            Merge(ref t, startIndex, endIndex);
        }
    }

    private void Merge(ref List<Test> t, int startIndex, int endIndex)
    {
        int start = startIndex;
        int middleIndex = (startIndex + endIndex) / 2 + 1;

        List<Test> temp = new List<Test>();

        while (start <= (startIndex + endIndex) / 2 && middleIndex <= endIndex)
        {
            if (!t[start].boolean && t[middleIndex].boolean)
            {
                temp.Add(t[middleIndex++]);
            }

            else
            {
                temp.Add(t[start++]);
            }
        }

        if (middleIndex >= endIndex)
        {
            for (int i = start; i <= (startIndex + endIndex) / 2; i++)
            {
                temp.Add(t[i]);
            }
        }

        if (start > (startIndex + endIndex) / 2)
        {
            for(int i = middleIndex; i < endIndex; i++)
            {
                temp.Add(t[i]);
            }
        }

        for (int i = startIndex, j = 0; j < temp.Count; i++, j++)
        {
            t[i] = temp[j];
        }
    }
}
