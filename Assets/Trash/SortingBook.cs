﻿using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class SortingBook : MonoBehaviour
{
    private string path;
    public TextAsset json;

    public class JsonBook
    {
        public string Name;
        public string Book;

        public JsonBook(string name, string book)
        {
            Name = name;
            Book = book;
        }
    }

    public class SortingResult
    {
        public string Name;
        public string Book1;
        public string Book2;
        public string Book3;
        public string Book4;
        public string Book5;
        public string Book6;
        public string Book7;
        public string Book8;
        public string Book9;
        public string Book10;
        public string Book11;
        public string Book12;
        public string Book13;
        public string Book14, Book15, Book16, Book17, Book18, Book19, Book20, Book21, Book22, Book23, Book24, Book25;

        public SortingResult(string name, string[] book)
        {
            Name = name;
            Book1 = book[0];
            Book2 = book[1];
            Book3 = book[2];
            Book4 = book[3];
            Book5 = book[4];
            Book6 = book[5];
            Book7 = book[6];
            Book8 = book[7];
            Book9 = book[8];
            Book10 = book[9];
            Book11 = book[10];
            Book12 = book[11];
            Book13 = book[12];
            Book14 = book[13];
            Book15 = book[14];
            Book16 = book[15];
            Book17 = book[16];
            Book18 = book[17];
            Book19 = book[18];
            Book20 = book[19];
            Book21 = book[20];
            Book22 = book[21];
            Book23 = book[22];
            Book24 = book[23];
            Book25 = book[24];
        }
    }

    public List<JsonBook> jsonBook = new List<JsonBook>();
    public List<SortingResult> sortingBook = new List<SortingResult>();
    private string[] book = new string[25];

    private void Start()
    {
        path = Application.persistentDataPath;
        jsonBook = Load();
        Sorting();
    }

    private void Sorting()
    {
        int bookCounter = 0;
        string name = "";
        ClearBook();
        name = jsonBook[0].Name;
        for (int i = 0; i < jsonBook.Count; i++)
        {
            Debug.Log(i);
            Debug.Log(jsonBook[i].Book);
            Debug.Log(book[bookCounter]);
            if(name == jsonBook[i].Name)
            {
                book[bookCounter] = (jsonBook[i].Book);
                bookCounter++;
            }

            else
            {
                sortingBook.Add(new SortingResult(name, book));
                name = jsonBook[i].Name;
                ClearBook();
                bookCounter = 0;
                book[bookCounter] = (jsonBook[i].Book);
            }
            name = jsonBook[i].Name;
        }
        Save(sortingBook);
    }

    void ClearBook()
    {
        for (int i = 0; i < book.Length; i++)
            book[i] = "";
    }

    public List<JsonBook> Load()
    {
            string jsonData = json.ToString();
            List<JsonBook> jsonCat = JsonConvert.DeserializeObject<List<JsonBook>>(jsonData);
            return jsonCat;
    }

    public void Save(List<SortingResult> jsonCat)
    {
        string jsonData = JsonDataManager_Dummy.ObjectToJson(jsonCat);
        Debug.Log(jsonData);
        JsonDataManager_Dummy.CreateJsonFile(path, "aaa", jsonData);
    }
}
